#!/bin/bash

set -e

echo "Extraction des compendiums VO et construction des fichiers de traduction"
./prepare.py

echo "Mise à jour des fichiers status et dictionnaires"
./update-status.py

if [ $CI_DEPLOY_MODULE != "true" ]
then
    echo "Commit et push des fichiers compendiums"
    git add ../archive ../data
    git commit -m "Mise à jour des fichiers compendiums"
    git push -o ci.skip https://root:$ACCESS_TOKEN@gitlab.com/pathfinder-fr/foundryvtt-pathfinder2-fr.git HEAD:master
fi

echo "Done"
