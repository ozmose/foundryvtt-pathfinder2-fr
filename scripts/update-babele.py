#!/usr/bin/python3
# -*- coding: utf-8 -*-

import json
import os
import datetime

from libdata import *

BABELE = "../babele/vf/fr/"
BABELE_VF_VO = "../babele/vf-vo/fr/"
BABELE_VO_VF = "../babele/vo-vf/fr/"
BABELE_VO = "../babele/vo/fr/"

packs = getPacks()


#
# cette fonction ajoute dynamiquement les champs de listes
#
def addLists(pack, data, entry, mapping_prefix):
    if "lists" in pack and "listsFR" in data:
        for k in pack["lists"]:
            if len(data["listsFR"][k]) > 0:
                if k == "Prereq":
                    json_array = []
                    for item in data["listsFR"][k]:
                        json_array += [{"value": item}]
                    entry[mapping_prefix + k.lower()] = json_array
                else:
                    entry[mapping_prefix + k.lower()] = data["listsFR"][k]

    if "extract" in pack:
        for k in pack["extract"]:
            if k in data["dataFR"] and len(data["dataFR"][k].strip()) > 0:
                entry[mapping_prefix + k.lower()] = data["dataFR"][k]


def handlePack(p, path, key, babele, babeleVfVo, babeleVoVf, babeleVo, isItems=False, isPages=False):
    all_files = os.listdir(path)

    count = {"aucune": 0, "libre": 0, "officielle": 0, "changé": 0, "doublon": 0,
             "auto-trad": 0, "auto-googtrad": 0, "vide": 0}

    # This in case of items, to prefix mappings
    mapping_prefix = "items-" if isItems else ""

    # add mappings (not for pages)
    if not isPages and 'desc' in p['paths']:
        babele['mapping'][mapping_prefix + "description"] = p['paths']['desc']
        babeleVfVo['mapping'][mapping_prefix + "description"] = p['paths']['desc']
        babeleVoVf['mapping'][mapping_prefix + "description"] = p['paths']['desc']
        babeleVo['mapping'][mapping_prefix + "description"] = p['paths']['desc']

    if "lists" in p:
        for k in p["lists"]:
            babele['mapping'][mapping_prefix + k.lower()] = p["lists"][k]
            babeleVfVo['mapping'][mapping_prefix + k.lower()] = p["lists"][k]
            babeleVoVf['mapping'][mapping_prefix + k.lower()] = p["lists"][k]
            babeleVo['mapping'][mapping_prefix + k.lower()] = p["lists"][k]

    if "extract" in p:
        for k in p["extract"]:
            babele['mapping'][mapping_prefix + k.lower()] = p["extract"][k]
            babeleVfVo['mapping'][mapping_prefix + k.lower()] = p["extract"][k]
            babeleVoVf['mapping'][mapping_prefix + k.lower()] = p["extract"][k]
            babeleVo['mapping'][mapping_prefix + k.lower()] = p["extract"][k]

    # Ajout des converters Babele
    if isItems:
        babele['mapping']["items"] = {"path": "items", "converter": "translateActorItems"}
        babeleVfVo['mapping']["items"] = {"path": "items", "converter": "translateActorItems"}
        babeleVoVf['mapping']["items"] = {"path": "items", "converter": "translateActorItems"}
        babeleVo['mapping']["items"] = {"path": "items", "converter": "translateActorItems"}

    if "id" in p and p["id"] == "equipment":
        babele['mapping']["name"] = {"path": "name", "converter": "translateEquipmentName"}
        babeleVfVo['mapping']["name"] = {"path": "name", "converter": "translateEquipmentName"}
        babeleVoVf['mapping']["name"] = {"path": "name", "converter": "translateEquipmentName"}
        babeleVo['mapping']["name"] = {"path": "name", "converter": "translateEquipmentName"}

    # read all files in folder
    for fpath in all_files:
        data = fileToData(path + fpath)
        if data['status'] == "auto-trad":
            data['descFr'] = ""
        count[data['status']] += 1

        if data['status'] == 'aucune' or data['status'] == "auto-trad" \
                or data['status'] == "auto-googtrad" or data['status'] == "vide":
            continue
        elif not isValid(data):
            print("Skipping invalid entry %s" % path + fpath)
            continue

        # We use IDs for items
        if isItems:
            entry_id = data["id"]
        else:
            entry_id = data['nameEN']

        if not isPages:
            if isItems and data['nameEN'] in SKILLS:  # Pour les variants de compétences, ne pas traduire le nom
                entryVF = {}
                entryVFVO = {}
                entryVOVF = {}
                entryVO = {}
            else:
                entryVF = {'name': data['nameFR']}
                entryVO = {'name': data['nameEN']}
                entryVFVO = {'name': "%s" % data['nameEN'] if data['nameEN'] == data['nameFR']
                    else ("%s (%s)" % (data['nameFR'], data['nameEN'].split("|")[0]))}
                entryVOVF = {'name': "%s" % data['nameEN'] if data['nameEN'] == data['nameFR']
                    else ("%s (%s)" % (data['nameEN'], data['nameFR'].split("|")[0]))}
                if "descrFR" in data and data["descrFR"] is not None and data["descrFR"] != "":
                    entryVF[mapping_prefix + "description"] = data['descrFR']
                    entryVO[mapping_prefix + "description"] = data['descrFR']
                    entryVFVO[mapping_prefix + "description"] = data['descrFR']
                    entryVOVF[mapping_prefix + "description"] = data['descrFR']
            # Initialise page dictionary if the compendium has pages (is a journal)
            if "pages" in p:
                entryVF['pages'] = {}
                entryVFVO['pages'] = {}
                entryVOVF['pages'] = {}
                entryVO['pages'] = {}
            addLists(p, data, entryVF, mapping_prefix)
            babele['entries'][entry_id] = entryVF
            addLists(p, data, entryVFVO, mapping_prefix)
            babeleVfVo['entries'][entry_id] = entryVFVO
            addLists(p, data, entryVOVF, mapping_prefix)
            babeleVoVf['entries'][entry_id] = entryVOVF
            addLists(p, data, entryVO, mapping_prefix)
            babeleVo['entries'][entry_id] = entryVO
        else:
            babele['entries'][data['journal']]["pages"][entry_id] = {"name": data['nameFR'], "text": data["descrFR"]}
            babeleVo['entries'][data['journal']]["pages"][entry_id] = {"name": data['nameEN'], "text": data["descrFR"]}
            babeleVfVo['entries'][data['journal']]["pages"][entry_id] = {
                "name": "%s" % data['nameEN'] if data['nameEN'] == data['nameFR']
                else ("%s (%s)" % (data['nameFR'], data['nameEN'])), "text": data["descrFR"]}
            babeleVoVf['entries'][data['journal']]["pages"][entry_id] = {
                "name": "%s" % data['nameEN'] if data['nameEN'] == data['nameFR']
                else ("%s (%s)" % (data['nameEN'], data['nameFR'])), "text": data["descrFR"]}

    print("Statistiques: " + path[8:][:-1])
    print(" - Traduits: %d (officielle) %d (libre)" % (count["officielle"], count["libre"]))
    print(" - Changé: %d" % count["changé"])
    print(" - Non-traduits: %d - Auto-générés: %d" % (count["aucune"], count['auto-trad'] + count['auto-googtrad']))
    print(" - Vides: %d" % count["vide"])


for p in packs:
    module = p["module"] if "module" in p else "pf2e"
    key = "%s.%s" % (module, p["name"])
    path = "../data/%s/" % p["id"]
    packName = p["transl"]
    references = []
    if "reference" in p:
        if isinstance(p["reference"], list):
            for reference in p["reference"]:
                references.append("pf2e.%s" % reference)
        else:
            references = "pf2e.%s" % p["reference"]

    # Fichiers de destination
    babele = {'label': packName, 'reference': references, 'entries': {}, 'mapping': {}}
    babeleVfVo = {'label': packName, 'reference': references, 'entries': {}, 'mapping': {}}
    babeleVoVf = {'label': packName, 'reference': references, 'entries': {}, 'mapping': {}}
    babeleVo = {'label': packName, 'reference': references, 'entries': {}, 'mapping': {}}

    # Generate main pack JSON
    handlePack(p, path, key, babele, babeleVfVo, babeleVoVf, babeleVo)

    # Traitement des items (ajout au fichier)
    if "items" in p:
        path = "../data/%s-items/" % p["id"]
        handlePack(p["items"], path, key, babele, babeleVfVo, babeleVoVf, babeleVo,
                                                      True)

    # Traitement des pages (ajout au fichier)
    if "pages" in p:
        path = "../data/%s-pages/" % p["id"]
        handlePack(p["pages"], path, key, babele, babeleVfVo, babeleVoVf, babeleVo,
                                                      False, True)

    print(BABELE + key + ".json")
    with open(BABELE + key + ".json", 'w', encoding='utf-8') as f:
        json.dump(babele, f, ensure_ascii=False, indent=4)
    with open(BABELE_VF_VO + key + ".json", 'w', encoding='utf-8') as f:
        json.dump(babeleVfVo, f, ensure_ascii=False, indent=4)
    with open(BABELE_VO_VF + key + ".json", 'w', encoding='utf-8') as f:
        json.dump(babeleVoVf, f, ensure_ascii=False, indent=4)
    with open(BABELE_VO + key + ".json", 'w', encoding='utf-8') as f:
        json.dump(babeleVo, f, ensure_ascii=False, indent=4)
