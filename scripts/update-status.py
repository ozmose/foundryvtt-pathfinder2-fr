#!/usr/bin/python3
# -*- coding: utf-8 -*-

import json
import yaml
import os
import datetime

from libdata import *

ROOT = ".."
packs = getPacks()
translations = {}


def handlePack(pack_id, key):
    preparedTranslations = []
    dirpath = "%s/data/%s" % (ROOT, pack_id)

    statusContentOK = "| Fichier   | Nom (EN)    | Nom (FR)    | État |\n" + "|-----------|-------------|-------------|:----:|\n"
    statusContentChanged = "| Fichier   | Nom (EN)    | Nom (FR)    | État |\n" + "|-----------|-------------|-------------|:----:|\n"
    statusContentEmpty = "| Fichier   | Nom (EN)    | État |\n" + "|-----------|-------------|:----:|\n"
    statusContentAT = "| Fichier   | Nom (EN)    | État |\n" + "|-----------|-------------|:----:|\n"
    statusContentNOK = "| Fichier   | Nom (EN)    |\n" + "|-----------|-------------|\n"

    longueur_initiale = len(statusContentNOK)

    files = [f for f in os.listdir(dirpath) if os.path.isfile(os.path.join(dirpath, f))]
    files = sorted(files, key=str.casefold)
    stats = {}

    for f in files:
        data = fileToData(os.path.join(dirpath, f))

        preparedTranslations.append({
            'file': f,
            'name': data['nameEN'],
            'nom': data['nameFR'] if 'nameFR' in data else "-",
            'link': "@UUID[Compendium.%s.%s]" % (key, data['id'])
        })

        if data['status'] in stats:
            stats[data['status']] += 1
        else:
            stats[data['status']] = 1

        if data['status'] == "aucune" or (data['status'] == "changé" and len(data['nameFR']) == 0):
            statusContentNOK += "|[%s](%s/%s)|%s|\n" % (f, pack_id, f, data['nameEN'])
        elif data['status'] == "auto-trad" or data['status'] == "auto-googtrad":
            statusContentAT += "|[%s](%s/%s)|%s|%s|\n" % (f, pack_id, f, data['nameEN'], data['status'])
        elif data['status'] == "changé":
            statusContentChanged += "|[%s](%s/%s)|%s|%s|%s|\n" % (
                f, pack_id, f, data['nameEN'], data['nameFR'], data['status'])
        elif data['status'] == "vide":
            statusContentEmpty += "|[%s](%s/%s)|%s|%s|\n" % (f, pack_id, f, data['nameEN'], data['status'])
        else:
            statusContentOK += "|[%s](%s/%s)|%s|%s|%s|\n" % (
                f, pack_id, f, data['nameEN'], data['nameFR'], data['status'])

    content = "# État de la traduction (%s)\n\n" % pack_id
    for s in stats:
        content += " * **%s**: %d\n" % (s, stats[s])

    content += "\n\nDernière mise à jour: %s *(heure de Canada/Montréal)*" % datetime.datetime.now().strftime(
        '%Y-%m-%d %H:%M')
    content += "\n\nCe fichier est généré automatiquement. NE PAS MODIFIER!"
    if "aucune" in stats and stats["aucune"] > 0 or len(statusContentNOK) != longueur_initiale:
        content += "\n## Liste des traductions à faire\n\n"
        content += statusContentNOK
    if "auto-trad" in stats and stats["auto-trad"] > 0 or "auto-googtrad" in stats and stats["auto-googtrad"] > 0:
        content += "\n## Liste des traductions automatiques à corriger/retraduire\n\n"
        content += statusContentAT
    if "changé" in stats and stats["changé"] > 0:
        content += "\n## Liste des éléments changés en VO et devant être vérifiés\n\n"
        content += statusContentChanged
    if "vide" in stats and stats["vide"] > 0:
        content += "\n## Liste des éléments vides dont le nom doit être traduit\n\n"
        content += statusContentEmpty
    content += "\n## Liste des traductions complétées\n\n"
    content += statusContentOK

    with open("%s/data/status-%s.md" % (ROOT, pack_id), 'w', encoding='utf-8') as s:
        s.write(content)

    return preparedTranslations

for p in packs:
    module = p["module"] if "module" in p else "pf2e"
    key = "%s.%s.%s" % (module, p["name"], p["type"])
    translations[p["id"]] = handlePack(p["id"], key)

    if 'items' in p:
        translations[p["id"] + "-items"] = handlePack(p["id"] + "-items", key)

    if 'pages' in p:
        translations[p["id"] + "-pages"] = handlePack(p["id"] + "-pages", key)

# ===============================
# génération du dictionnaire (EN)
# ===============================
content = "# Bibliothèque\n\n"
content += "\n\nDernière mise à jour: %s *(heure de Canada/Montréal)*" % datetime.datetime.now().strftime(
    '%Y-%m-%d %H:%M')
content += "\n\nCe fichier est généré automatiquement. NE PAS MODIFIER!\n\n"

packs = sorted(packs, key=lambda k: k['transl'])

for p in packs:
    packName = p["transl"]
    content += " * [%s](#%s)\n" % (packName, packName.lower().replace(' ', '-').replace('(', '').replace(')', ''))

for p in packs:
    packName = p["transl"]
    content += "\n\n### %s\n\n" % packName
    content += "| Nom (EN)   | Nom (FR)    | Lien compendium |\n"
    content += "|------------|-------------|-----------------|\n"

    sortedList = sorted(translations[p["id"]], key=lambda k: k['name'])
    for el in sortedList:
        content += "|[%s](%s/%s)|%s|`%s`|\n" % (el['name'], p['id'], el['file'], el['nom'], el['link'])

with open("../data/dictionnaire.md", 'w', encoding='utf-8') as f:
    f.write(content)

# ===============================
# génération du dictionnaire (FR)
# ===============================

content = "# Bibliothèque\n\n"
content += "\n\nDernière mise à jour: %s *(heure de Canada/Montréal)*" % datetime.datetime.now().strftime(
    '%Y-%m-%d %H:%M')
content += "\n\nCe fichier est généré automatiquement. NE PAS MODIFIER!\n\n"

packs = sorted(packs, key=lambda k: k['transl'])

for p in packs:
    packName = p["transl"]
    content += " * [%s](#%s)\n" % (packName, packName.lower().replace(' ', '-').replace('(', '').replace(')', ''))

for p in packs:
    packName = p["transl"]
    content += "\n\n### %s\n\n" % packName
    content += "| Nom (FR)   | Nom (EN)    | Lien compendium |\n"
    content += "|------------|-------------|-----------------|\n"

    sortedList = sorted(translations[p["id"]], key=lambda k: k['nom'])
    for el in sortedList:
        content += "|[%s](%s/%s)|%s|`%s`|\n" % (el['nom'], p['id'], el['file'], el['name'], el['link'])

with open("../data/dictionnaire-fr.md", 'w', encoding='utf-8') as f:
    f.write(content)
