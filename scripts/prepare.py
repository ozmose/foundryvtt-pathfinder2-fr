#!/usr/bin/python3
# -*- coding: utf-8 -*-

print('Starting imports')
import json
import yaml
import os
import re
import logging

from libdata import readFolder, dataToFile, getPacks, getValue, getList, equals, print_error, print_warning, SKILLS, \
    SKILLSFR

# à réactiver pour autotrad
# from libselenium import translator_driver, full_trad

print('Preparing translation')
logging.basicConfig(filename='translation.log', level=logging.INFO)

ROOT = "../"

def checkItems(entries, pack_id, referencePaths=None, isItems=False, isPages=False):
    folderPath = "%sdata/%s/" % (ROOT, pack_id)
    # ==========================
    # read all available entries
    # ==========================
    if not os.path.isdir(folderPath):
        os.mkdir(folderPath)

    folderData = readFolder(folderPath)
    existing = folderData[0]
    existingByName = folderData[1]
    pack_has_errors = folderData[2]

    existingReference = {}
    existingItemsByName = {}
    if isItems:
        existingItemsByName = {**existingItemsByName, **readFolder("%sdata/spells/" % ROOT)[1]}
        existingItemsByName = {**existingItemsByName, **readFolder("%sdata/equipment/" % ROOT)[1]}
        if referencePaths is not None:
            if isinstance(referencePaths, list):
                for referencePath in referencePaths:
                    existingReference = {**existingReference, **readFolder(referencePath)[0]}
            else:
                existingReference = readFolder(referencePaths)[0]

    # ========================
    # create or update entries
    # ========================
    for id in entries:
        source = entries[id]
        if not source:
            continue

        # build filename
        filename = "%s.htm" % id
        if source['type2']:
            filename = "%s-%s-%s" % (source['type1'], source['type2'], filename)
        elif source['type1']:
            filename = "%s-%s" % (source['type1'], filename)
        filepath = "%s/%s" % (folderPath, filename)

        # data exists in reference directories => ignore it
        if id in existingReference or source["name"] in existingItemsByName:
            continue

        # data exists for id
        elif id in existing:

            # rename file if filepath not the same
            if existing[id]['filename'] != filename and not os.path.isfile("%s/%s" % (folderPath, filename)):
                pathFrom = "%s/%s" % (folderPath, existing[id]['filename'])
                pathTo = "%s/%s" % (folderPath, filename)
                os.rename(pathFrom, pathTo)

            # check status from existing file
            if not existing[id]["status"] in (
            "libre", "officielle", "doublon", "aucune", "changé", "auto-trad", "auto-googtrad", "vide"):
                print_error("Status error for : %s" % filepath)
                has_errors = True
                continue

            if (isItems and not equals(existing[id]['parentName'], source['parentName'])) \
                    or (isPages and not equals(existing[id]['journal'], source['journal'])) \
                    or not equals(existing[id]['nameEN'], source['name']) \
                    or not equals(existing[id]['descrEN'], source['desc']) \
                    or not equals(existing[id]['listsEN'], source['lists']) \
                    or not equals(existing[id]['dataEN'], source['data']):
                if ('parentName' in source):
                    existing[id]['parentName'] = source['parentName']
                if ('journal' in source):
                    existing[id]['journal'] = source['journal']
                existing[id]['nameEN'] = source['name']
                existing[id]['descrEN'] = source['desc']
                existing[id]['listsEN'] = source['lists']
                existing[id]['dataEN'] = source['data']

                if existing[id]['status'] != "aucune" and existing[id]['status'] != "changé":
                    existing[id]['oldstatus'] = existing[id]['status']
                    existing[id]['status'] = "changé"

                dataToFile(existing[id], filepath)

            elif 'oldstatus' in existing[id] and existing[id]['status'] != 'changé':
                del existing[id]['oldstatus']
                dataToFile(existing[id], filepath)

        # file doesn't exist
        else:

            # check if other entry exists with same name => means that ID has changed for the same element
            # not applicable to items (monsters have entries with the same name but different descriptions, we target on ID)
            if not isItems \
                    and not isPages \
                    and source['name'] in existingByName \
                    and not source['name'] in ("Shattering Strike", "Chilling Spray"):
                oldEntry = existingByName[source['name']]
                # rename file
                pathFrom = "%s/%s" % (folderPath, oldEntry['filename'])
                pathTo = "%s/%s" % (folderPath, filename)

                if oldEntry['id'] in existing:
                    del existing[oldEntry['id']]
                os.rename(pathFrom, pathTo)

            # create new
            else:
                name = source["name"]
                if len(source['desc']) > 0:
                    # Automatic translation
                    logging.info("Translating %s" % name)
                    # print("Translating %s" % name)
                    # translation_data = full_trad(driver, source['desc'])
                    # tradDesc = translation_data.data
                    # status = translation_data.status
                    # FIX : auto-trad trop longue pour le bestiaire 3
                    status = "aucune"
                    tradDesc = ""
                else:
                    tradDesc = ""
                    status = "vide"

                data = {
                    'parentName': source["parentName"] if isItems else None,
                    'journal': source["journal"] if isPages else None,
                    'nameEN': name,
                    'nameFR': SKILLSFR[name] if name in SKILLS else "",
                    'status': status,
                    'descrEN': source['desc'],
                    'descrFR': tradDesc,
                    'listsEN': source['lists'],
                    'dataEN': source['data'],
                    'listsFR': {},
                    'dataFR': {}
                }

                dataToFile(data, filepath)
                # si le pack contient au moins une erreur à la lecture, on arrête de l'examiner

    if pack_has_errors == True:
        print_warning("Invalid data in pack %s, skipping" % (folderPath))
        return True

    # =======================
    # search deleted elements
    # =======================
    if not os.path.exists("%sarchive" % (ROOT)):
        os.makedirs("%sarchive" % (ROOT))
    if not os.path.exists("%sarchive/%s/" % (ROOT, pack_id)):
        os.makedirs("%sarchive/%s/" % (ROOT, pack_id))

    for id in existing:
        if not id in entries:
            filename = "%s/%s" % (folderPath, existing[id]['filename'])
            if existing[id]['status'] != 'aucune':
                print("Archiving %s" % (existing[id]['filename']))
                os.replace("%s/%s" % (folderPath, existing[id]['filename']),
                           "%sarchive/%s/%s" % (ROOT, pack_id, existing[id]['filename']))
            else:
                os.remove(filename)

    return False


# à réactiver pour autotrad
# # ouverture de la connexion a DeepL Translator
# print('Opening DeepL Translator connection')
# driver = translator_driver()

print('Loading packs...')
packs = getPacks()
has_errors = False

for p in packs:
    # Ignorer prepare pour compendium AC, à désactiver quand mis à jour
    if "pack" in p:
        print('Ignoring %s pack' % (p["id"]))
        continue

    print('Preparing %s pack' % (p["id"]))
    FILE = p["pack"] + "/" + p["id"] + ".db" if "pack" in p else ROOT + "packs/" + p["id"] + ".json"
    entries = {}
    obj_items = {}
    obj_pages = {}

    # =================================
    # read pack files and generate dict
    # =================================
    with open(FILE, 'r', encoding='utf8') as f:
        count = 0
        for obj in json.load(f):
            count += 1

            if '$$deleted' in obj:
                continue

            entries[obj['_id']] = {
                'parentName': None,
                'name': getValue(obj, p['paths']['name']),
                'desc': getValue(obj, p['paths']['desc'], False, "") if 'desc' in p['paths'] else "NE PAS TRADUIRE",
                'type1': getValue(obj, p['paths']['type1']) if 'type1' in p['paths'] else None,
                'type2': getValue(obj, p['paths']['type2'], False) if 'type2' in p['paths'] else None,
                'lists': {},
                'data': {}
            }

            ## additional lists
            if "lists" in p:
                for key in p["lists"]:
                    list = getList(obj, p["lists"][key], False)
                    if len(list) == 0:
                        list = getList(obj, p["lists"][key] + ".value", False)
                    entries[obj['_id']]['lists'][key] = list

            ## other extractions
            if "extract" in p:
                for key in p["extract"]:
                    value = getValue(obj, p["extract"][key], False, None, False)
                    if value and len(value) > 0:
                        entries[obj['_id']]['data'][key] = value

            ## items
            if "items" in p:
                items = getList(obj, "items", False)
                for item in items:
                    # Attaques, Passifs, Actions
                    if item['type'] in ["melee", "action"]:
                        obj_items[item['_id']] = {
                            "_id": getValue(item, '_id', False),
                            "parentName": getValue(obj, p['paths']['name']),
                            "name": getValue(item, p['items']['paths']['name'], False),
                            "desc": getValue(item, p['items']['paths']['desc'], False, "") if 'desc' in p['items'][
                                'paths'] else "NE PAS TRADUIRE",
                            'type1': None,
                            'type2': None,
                            'lists': {},
                            'data': {},
                        }
                    # Armes : augmenter le nom avec les runes présentes sur l'arme
                    if item['type'] in ["weapon"]:
                        weaponName = getValue(item, p['items']['paths']['name'], False)
                        nameAdditions = []
                        if addition := getValue(item, "system.potencyRune.value", False, None, False):
                            nameAdditions.append("+" + str(addition))
                        for rune in [
                            "system.strikingRune.value",
                            "system.preciousMaterial.value",
                            "system.propertyRune1.value",
                            "system.propertyRune2.value",
                            "system.propertyRune3.value",
                            "system.propertyRune4.value",
                        ]:
                            if addition := getValue(item, rune, False):
                                nameAdditions.append(addition)
                        if len(nameAdditions) > 0:
                            formattedAdditions = ",".join(nameAdditions)
                            weaponName += "|" + formattedAdditions
                        obj_items[item['_id']] = {
                            "_id": getValue(item, '_id', False),
                            "parentName": getValue(obj, p['paths']['name']),
                            "name": weaponName,
                            "desc": getValue(item, p['items']['paths']['desc'], False, "")
                                if getValue(item, "flags.core.sourceId", False) is None
                                and 'desc' in p['items']['paths'] else "NE PAS TRADUIRE",
                            'type1': None,
                            'type2': None,
                            'lists': {},
                            'data': {},
                        }
                    # Entrées d'incantations, sorts, équipement, on extrait le nom, et si c'est un objet spécifique
                    if item['type'] in ["spellcastingEntry", "spell", "armor", "treasure", "consumable", "equipment",
                                        "kit"]:
                        obj_items[item['_id']] = {
                            "_id": getValue(item, '_id', False),
                            "parentName": getValue(obj, p['paths']['name']),
                            "name": getValue(item, p['items']['paths']['name'], False),
                            "desc": getValue(item, p['items']['paths']['desc'], False, "")
                                if getValue(item, "flags.core.sourceId", False) is None
                                and 'desc' in p['items']['paths'] else "NE PAS TRADUIRE",
                            'type1': None,
                            'type2': None,
                            'lists': {},
                            'data': {},
                        }
                    if item['type'] in ["lore"] \
                            and (item['name'] not in SKILLS  # Lore skills
                                 or ("variants" in item["system"] is not None and len(
                                item["system"]["variants"]) > 0)):  # Variants
                        obj_items[item['_id']] = {
                            "_id": getValue(item, '_id', False),
                            "parentName": getValue(obj, p['paths']['name']),
                            "name": getValue(item, p['items']['paths']['name'], False),
                            "desc": getValue(item, p['items']['paths']['desc'], False, "")
                                if 'desc' in p['items']['paths'] else "NE PAS TRADUIRE",
                            'type1': None,
                            'type2': None,
                            'lists': {},
                            'data': {}
                        }
                    if "extract" in p["items"]:
                        for key in p["items"]["extract"]:
                            value = getValue(item, p["items"]["extract"][key], False, None, False)
                            if value and len(value) > 0:
                                obj_items[item['_id']]['data'][key] = value

            ## pages
            if "pages" in p:
                pages = getList(obj, "pages", False)
                journal = getValue(obj, p['paths']['name'])
                for page in pages:
                    obj_pages[page['_id']] = {
                        "_id": getValue(page, '_id', False),
                        "journal": journal,
                        "name": getValue(page, p['pages']['paths']['name'], False),
                        "desc": getValue(page, p['pages']['paths']['desc'], False, "")
                            if 'desc' in p['pages']['paths'] else "NE PAS TRADUIRE",
                        'type1': None,
                        'type2': None,
                        'lists': {},
                        'data': {}
                    }

    # ==============================
    # search for duplicates in names
    # ==============================
    duplic = {}
    for id in entries:
        if entries[id]['name'] in duplic:
            print_warning("Duplicated name: %s (%s)\e[0m" % (entries[id]['name'], id))
        else:
            duplic[entries[id]['name']] = id

    # ==========================
    # read all available entries
    # ==========================
    has_errors = checkItems(entries, p["id"])

    # =============
    # process items
    # =============
    if len(obj_items) > 0:
        refs = None
        if "reference" in p:
            if isinstance(p["reference"], list):
                refs = []
                for reference in p["reference"]:
                    refs.append("%sdata/%s/" % (ROOT, reference + "-items"))
            else:
                refs = "%sdata/%s/" % (ROOT, p["reference"] + "-items")
        has_errors = has_errors or checkItems(obj_items, p["id"] + "-items", refs, True, False)

    # =============
    # process pages
    # =============
    if len(obj_pages) > 0:
        has_errors = has_errors or checkItems(obj_pages, p["id"] + "-pages", None, False, True)

    if has_errors:
        print_error("Au moins une erreur survenue durant la préparation, échec")
        exit(1)

# à réactiver pour autotrad
# # fermeture de la connexion a DeepL Translator
# if driver:
#   driver.quit()
