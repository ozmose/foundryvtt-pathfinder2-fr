# Module de traduction de Pathfinder 2 à destination de Foundry VTT

Ce module est destiné à être utilisé avec la plateforme virtuelle foundry VTT (https://foundryvtt.com/).  

Ce module permet de disposer de la traduction en français des données des objets contenus dans les packs du système Foundry et de la traduction en français de l'interface utilisateur du système Pathfinder 2e édition.

Pathfinder 2e édition est un jeu de rôle développé et commercialisé par Paizo. 

Le module permet de disposer :
- de l'interface utilisateur du système Pathfinder 2e en français, c'est à dire des explications dans les fenêtres de conversation, sur les feuilles de personnage, sur les monstres, etc.
- de la description des "items/objets" figurant dans les packs des compendiums anglophones également traduits en français. 

Le module permet de pouvoir disposer d'un affichage du nom de chaque objet de manière alternative, en vo, en vf, en vo-vf ou en vf-vo, la description restant en français. L'existence d'options dans le nommage facilite également le jeu sur des tables qui utilisent le français et l'anglais. Vous retrouverez les différentes options dans la partie "Utilisation sous Foundry" de ce document.

L'utilisation des noms doublés facilite la recherche et la prise en compte des noms qui peuvent être utilisés comme prédicats (mots-clefs servant de référence) par des modules qui permettent une automatisation.

Ledit module rend indispensable l'utilisation des modules suivants : 
1. Babele, ce module et ses propres dépendances sont nécessaires pour assurer la prise en charge de la traduction et la substitution lors de l'affichage en jeu. Il est développé par _Simone Riccisi_
2. PF2e system, ce module est celui qui assure la prise en charge par Foundry du système de jeu en lui-même avec ses règles, ses automatisations. Il est nécessaire pour charger les packs des compendiums de données du système de jeu qui sont traduites par le présent module
3. PF2e companion compendia, ce module est nécessaire pour charger les packs des compendiums de données propres aux différents compagnons (la créature de l'inventeur, le compagnon animal du druide ou du rôdeur, l'eidolon du conjurateur, les compagnons morts-vivants...). Il a été développé à part et sera probablement un jour mangé par le système 


**En aucun cas, Paizo ou Black Book Editions ne reconnaîssent, ne sont à l'origine ou ne sont les mécènes de ce module ou de ce projet.**

La gamme Pathfinder est une création de **Paizo Publishing**. Elle fait l'objet d'une publication officielle en français par **Black Book Editions**.

Le présent module est réalisé en application des licences Open Game License (ogl) et Pathfinder Community Use Policy (PCUP). 

Il est destiné à l'utilisation du système Pathfinder 2e avec le logiciel Foundry VTT 

L'utilisation de ce module pour permettre de jouer avec un produit commercialisé est strictement interdite. 

Ce module ne peut en aucun cas être une dépendance d'un autre module, à moins qu'il ne soit lui-même gratuit.

Vous ne pouvez notamment pas déclarer ce module dans la section 'dependencies' (ou toute section qui viendrait à lui être substituée) du fichier foundry module.json ou system.json de votre module, à moins qu'il ne soit lui même gratuit.

Par ailleurs, si vous entendez extraire ou utiliser les traductions des objets qui ont été réalisées par des fans à tout autre fin que l'utilisation de ce module et qui portent l'indicateur libre, vous devez **impérativement** :
- respecter les licences ogl et PCUP,
- avoir obtenu l'autorisation des traducteurs (Rectulo et Njini) et les créditer à ce titre.

Actuellement :
- Cheps a obtenu cette permission pour la création d'un [wiki](https://pf2e-srd.cheps.ovh/)  
- Fykk a obtenu cette permission pour le développement d'une application permettant de générer une feuille de personnage interactive

Vous ne pouvez en aucun cas utiliser les traductions libres réalisées gratuitement par des fans pour servir des fins commerciales.

- La traduction du Livre de base est, pour l'essentiel, la reprise de la traduction officielle qui a été réalisée en français par Black Book Edition avec application des errata parus depuis lors et non encore portés en vf.
- La description des monstres des Bestiaires 1 et 2 sont également issues de la traduction officielle réalisée par BBE.
- La traduction des objets du compendium du Guide du Maître est également issue de la traduction officielle réalisée par BBE.

Les traductions officielles utilisées ont été l'objet d'un travail complémentaire des contributeurs du module visant à supprimer les références des pages, mettre en forme les tables, insérer les liens nécessaires pour naviguer dans le logiciel Foundry entre les objets, réaliser les automatisations permises par le système. Les fichiers ont également fait l'objet de l'insertion des erratas qui ont été publiés par Paizo, et ce, au fur et à mesure de leur intégration dans Foundry.

Certains textes ont aussi été légèrement repris par souci de cohérence et d'uniformisation dans l'utilisation de certains mots, lorsque la traduction n'a pas parue la mieux adaptée au concept ou que la traduction a paru erronée. 

Les erratas sont appliqués au fur et à mesure de leur parution en vo, ce qui peut conduire à un décalage avec la version officielle en français.

Le reste de la traduction des fichiers anglophones qui vous est proposée dans ce module est le fruit du travail passionné de membres de la communauté (Njini et Rectulo depuis l'origine, Elsinin depuis la traduction de Treasure Vault). 

Ils se sont donnés pour objectif d'assurer la traduction des ouvrages Paizo au rythme de leur parution en anglais pour permettre de jouer avec les campagnes parues en version originale et de prendre en charge tout less règles non traduites en français.

## Note aux traducteurs 

* Les fichiers à traduire ou à corriger se trouvent dans le répertoire [data](data/).

* Chaque fichier correspond à une entrée traduite ou à traduire
  * Pour chaque fichier, vous disposez de plusieurs champs en anglais et en français,
  * Les textes d'origine en anglais doivent **rester inchangés** ! (ils permettent à un programme d'extraction des données à partir des fichiers anglophones d'effectuer automatiquement une comparaison entre le contenu de l'extraction précédente et une éventuelle mise à jour anglophone. Cette comparaison permet de signaler tous les changements, qu'il s'agisse d'évolutions ou dans l'automatisation, mais aussi les oublis ou typos et les éventuels erratas appliqués au fur et à mesure),
  * Voir [data/instructions.md](data/instructions.md) pour plus d'instructions concernant les instructions de traduction et de modifications pour ceux qui veulent contribuer.

* Pour voir l'état d'avancement des différentes traductions, vous pouvez consulter les fichiers qui se terminent par **.md** dans le répertoire data classés par thèmes.

* À la différence des compendiums, les fichiers concernant l'UI/UX du système et les effets de règles qui sont rangés dans le dossier lang sont maintenus en utilisant **uniquement** le dépôt constitué sous [Weblate](https://weblate.foundryvtt-hub.com/projects/pf2-fr/). Weblate dispose de fonctionnalités avancées pour faire apparaître les changements entre deux versions des fichiers anglophones qui lui sont propres. Qu'il s'agisse de nouvelles clés, de changements. Vous pouvez contribuer en passant par weblate si vous le souhaitez mais il ne faut pas opérer de changements en passant par le dépôt Gitlab.

## Utilisation sous Foundry

Pour appliquer les traductions dans votre système, il existe un prérequis logiciel. Il vous faut installer le module intitulé [Babele](https://gitlab.com/riccisi/foundryvtt-babele) qui a été créé par *Simone Riccisi*. Ce module réalise une cartogaphie des fichiers, repère les parties anglaises et permet ensuite de leur substituer les traductions proposées en français.

Une fois ce module et Babele installés et activés, il vous faut demander l'affichage en français dans les options. 

Instructions détaillées 
* Lorsque vous ouvrez le logiciel de VTT Foundry, allez dans l'onglet Modules et cliquez sur Installer un module
* Chercher Babele, puis **Installer** pour l'installer automatiquement
* Revenir dans l'onglet Module
* Chercher le module français [Pathfinder 2e] (fan made) ou bien entrer manuellement l'adresse du manifeste du module dans la barre d'adresse : `https://gitlab.com/pathfinder-fr/foundryvtt-pathfinder2-fr/-/raw/master/module.json`

Puis dans les options du module faire votre choix parmi les options
  * `vf(vo)`: noms au format "nom fr (nom en)" avec le contenu traduit en français
  * `vo(vf)`: noms au format "nom en (nom fr)" avec le contenu traduit en français
  * `vo`: noms conservés dans leur version originale (en) mais avec le contenu en français
  * `vf`: nom et description uniquement en vf
* Il ne vous reste qu'à en profiter !

L'utilisation vo(vf) et vf(vo) peut parfois amener des décalages inesthétiques dans l'affichage du système qui n'empêchent pas de jouer. 

Il vaut mieux utiliser les versions avec les noms dans les deux langues car plusieurs modules non localisés qui sont régulièrement utilisés codent des automatisations en utilisant des mots clés codés le plus souvent en langue anglaise. 

Si vous n'utilisez que la vf, vous ne profiterez pas de tous leurs effets.

Vous êtes invités à remonter les erreurs de traduction, les typos, les erratas sur le discord du projet. Une capture d'écran est la bienvenue.

## Ressources utiles

À partir des traductions, l'extraction complète au fil du temps et des publications a permis de disposer d'un [Glossaire](data/dictionnaire.md) qui vous donne le nom utilisé en anglais, le nom utilisé en français avec l'ID (l'identifiant du fichier qui permet d'insérer ailleurs un lien réutilisable pour faciliter la navigation). 

Un dictionnaire est aussi disponible en partant du français pour disposer de la traduction en anglais.

Compte tenu du "poids" des fichiers désormais, il faut les télécharger.

## Bugs de la vo
Les erreurs que vous trouvez dans les textes en vo peuvent être remontées sur le projet anglophone en créant une "issue" sur le système en décrivant la phrase erronée et la phrase que vous proposez. Le mieux est de donner le numéro du fichier, suivie de sa traduction et indiquer en quoi il est incorrect, le tout dans la langue anglaise. Au pire, remontez l'information sur le discord du projet où un contributeur du système pourra vous relayer auprès des développeurs anglophones.

## Ajout de données à extraire pour traduction

S'il est décidé qu'un autre champ du JSON anglais du fichier d'origine doit faire l'objet d'une traduction, c'est le fichier libdata.py qui doit être modifié, et en particulier les packs (variable SUPPORTED)

- si le champ JSON correspond à une liste, il faut ajouter le chemin de l'élément JSON tableau dans ̀`lists` (cf. prérequis des dons).
- si le champ JSON correspond à une valeur unique, il faut ajouter le chemin de l'élément JSON dans `extract` (cf. sorts).

## État

NOTE : Le pack des actions a été scindé avec la version 3.10 du système pour mettre les actions spécifiques à une aventure donnée dans un pack à part. Le pack actions ne contient plus que les seules actions communes aux règles du système. 

Les bestiaires des aventures ne sont pas pris en charge par les traducteurs (Njini et Rectulo). Ils peuvent néanmoins l'être du fait de l'ogl et de la PCUP. Les parties reprises ne contiennent que les données mécaniques permettant le jeu sans les éléments d'histoire qui appartiennent aux éditeurs, notamment les historiques des PNJ concernés par exemple.

L'équipe en charge de la traduction des règles ne le fera pas nécessairement mais d'autres membres peuvent le faire.

Actuellement, les Bestiaires de l'Âge des Cendres, de Sentence d'extinction, d'Agent d'Absalom, de la Boîte d'Initiation et d'Otira en difficulté ont été portés dans le module par Dwim. Njini a commencé à prendre en charge ceux de la campagne Kingmaker.

À jour au 12 mars 2023
