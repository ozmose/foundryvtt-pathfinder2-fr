# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [4.68.0] - 2023-06-07
Update the scripts to extract english name for the translations, as they disappeared from the source files
Update the Build process to be able to refresh the compendium without releasing the versions.

## [4.33.0] - 2023-02-13
Update the scripts to extract & handle skill variants & lores for Bestiaries

## [4.26.0] - 2023-01-15
Auto-archive obsolete items on preparing items

## [4.14.0] - 2022-10-23
Journals support

## [4.1.0] - 2022-09-11
V10 support

## [1.71.0] - 2022-08-03
Update bestiaries to handle item references to other compendiums

## [1.48.0] - 2022-04-22
Update to the scripts to handle items for Bestiaries

## [1.16.0] - 2022-01-17
Correct update from english data (went from old branch of the english project - Fix it to get the newest of the PF2 system)
Migration of heritages - extract heritages.db

## [1.14.0] - 2022-01-15 
- New release - More automations - proofing of the compendium

## [1.11.0] - 2022-01-02 
- New release of translations including G&G and Grand Bazaar (equipment, feats, spells) 

## [1.10.0] - 2021-12-09 
- New release of translations including G&G and Grand Bazaar (consumables) - first translation of critical deck

## [0.29.0] - 2020-06-22 
### Added
- First official release on FoundryVTT.com

## Previous
### Changed
- Updates based on latest translations
