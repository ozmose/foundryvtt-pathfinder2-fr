# État de la traduction (impossible-lands-bestiary)

 * **libre**: 16


Dernière mise à jour: 2023-07-12 20:28 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[1MxubcO4SB8PwmKT.htm](impossible-lands-bestiary/1MxubcO4SB8PwmKT.htm)|Benthic Reaver|Castor benthique|libre|
|[1SIx3wcRvplbfwk3.htm](impossible-lands-bestiary/1SIx3wcRvplbfwk3.htm)|Stone Sister|Soeur de pierre|libre|
|[4DQotKcUZebLfeeL.htm](impossible-lands-bestiary/4DQotKcUZebLfeeL.htm)|Cursed King|Roi maudit|libre|
|[AIQMB0ysHNeodKG4.htm](impossible-lands-bestiary/AIQMB0ysHNeodKG4.htm)|Spellscar Fext|Fexte cicatrisort|libre|
|[fGZeQOarR6Im7Lnk.htm](impossible-lands-bestiary/fGZeQOarR6Im7Lnk.htm)|Kashrishi Evaluator|Évaluateur Kashrishi|libre|
|[gZ2qX5vbWg7otMVT.htm](impossible-lands-bestiary/gZ2qX5vbWg7otMVT.htm)|Quantium Golem (Acid)|Golem de Quantium (acide)|libre|
|[JmGKKXdAxUkJtdTm.htm](impossible-lands-bestiary/JmGKKXdAxUkJtdTm.htm)|Ugvashi|Ugvashi|libre|
|[K2AOcLMDOVNbgPXp.htm](impossible-lands-bestiary/K2AOcLMDOVNbgPXp.htm)|Fleshforged Conformer|Conformateur forgé de chair|libre|
|[KTabPRN489yjTvek.htm](impossible-lands-bestiary/KTabPRN489yjTvek.htm)|Fleshforged Dreadnought|Cuirassé des forges de chair|libre|
|[lhr2fjewILo4nyUZ.htm](impossible-lands-bestiary/lhr2fjewILo4nyUZ.htm)|Mutant Gnoll Hulk|Mastodonte gnoll mutant|libre|
|[LVY9JAhTnBC2SeqZ.htm](impossible-lands-bestiary/LVY9JAhTnBC2SeqZ.htm)|Skinskitter|Dermine|libre|
|[qbfMDAa3RXvwyG7k.htm](impossible-lands-bestiary/qbfMDAa3RXvwyG7k.htm)|Gunpowder Ooze|Vase poudrière|libre|
|[qmw82W0WhPkn9UTW.htm](impossible-lands-bestiary/qmw82W0WhPkn9UTW.htm)|Clockwork Cannoneer|Canonnier mécanique|libre|
|[UgGyHJ39EHztCV5m.htm](impossible-lands-bestiary/UgGyHJ39EHztCV5m.htm)|Kasesh (Stone)|Kasesh (pierre)|libre|
|[wjNbuzeqoVSR0Wwm.htm](impossible-lands-bestiary/wjNbuzeqoVSR0Wwm.htm)|Quantium Golem (Electricity)|Golem de Quantium (Électricité)|libre|
|[xl2UxOFVSDGqaVS5.htm](impossible-lands-bestiary/xl2UxOFVSDGqaVS5.htm)|Ratajin Mastermind|Cerveau ratajin|libre|
