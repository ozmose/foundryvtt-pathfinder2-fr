# État de la traduction (impossible-lands-bestiary-items)

 * **libre**: 98


Dernière mise à jour: 2023-07-12 20:28 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0eOrNdC2RxxbKNH3.htm](impossible-lands-bestiary-items/0eOrNdC2RxxbKNH3.htm)|Fleshwarp Lore|Connaissance de la distorsion de la chair|libre|
|[1HLmpNiFbeCNAiK6.htm](impossible-lands-bestiary-items/1HLmpNiFbeCNAiK6.htm)|Lifesense 120 feet|Perception de la vie 36 mètres|libre|
|[20sqDR0ZVwbHaDQH.htm](impossible-lands-bestiary-items/20sqDR0ZVwbHaDQH.htm)|Unstable Feedback|Rétroaction instable|libre|
|[2vjvjlmw4lJ6Kx0X.htm](impossible-lands-bestiary-items/2vjvjlmw4lJ6Kx0X.htm)|Outsmart|Plus malin|libre|
|[2Zb90teqdj9IhFCP.htm](impossible-lands-bestiary-items/2Zb90teqdj9IhFCP.htm)|Low-Light Vision|vision nocturne|libre|
|[2zlQ8fhc8QeehktC.htm](impossible-lands-bestiary-items/2zlQ8fhc8QeehktC.htm)|Evaluate Discordance|Évaluer la discordance|libre|
|[3P9XjG1L99lgKXLy.htm](impossible-lands-bestiary-items/3P9XjG1L99lgKXLy.htm)|Eye Beam|Rayon oculaire|libre|
|[40NDv4xTfgTjOeUn.htm](impossible-lands-bestiary-items/40NDv4xTfgTjOeUn.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[5fWkOgtgxvhy51Ix.htm](impossible-lands-bestiary-items/5fWkOgtgxvhy51Ix.htm)|Battle Axe|+1,striking|Hache d'arme de frappe +1|libre|
|[5MRsa1cMu7G30ZW2.htm](impossible-lands-bestiary-items/5MRsa1cMu7G30ZW2.htm)|Combust|Combustion|libre|
|[5NIXdaMyiuggG6gU.htm](impossible-lands-bestiary-items/5NIXdaMyiuggG6gU.htm)|Scaling Spikes|Pitons d'escalade|libre|
|[5qo8PwcNs0JaqVRw.htm](impossible-lands-bestiary-items/5qo8PwcNs0JaqVRw.htm)|Statue|Statue|libre|
|[5RBj47hcinDCgyle.htm](impossible-lands-bestiary-items/5RBj47hcinDCgyle.htm)|Vengeful Bite|Morsure vengeresse|libre|
|[78Y9kAXcHfyuSYCP.htm](impossible-lands-bestiary-items/78Y9kAXcHfyuSYCP.htm)|Constant Spells|Sorts constants|libre|
|[8JQu5UduC3IHlSsZ.htm](impossible-lands-bestiary-items/8JQu5UduC3IHlSsZ.htm)|Regeneration 10 (Deactivated by Glass)|Régénération 10 (Désactivé par le verre)|libre|
|[8oCLJUItWspU84SL.htm](impossible-lands-bestiary-items/8oCLJUItWspU84SL.htm)|Drown in Ichor|Noyé dans l'ichor|libre|
|[9O0kaWa3O5uUYJX1.htm](impossible-lands-bestiary-items/9O0kaWa3O5uUYJX1.htm)|A Taste for Skin|Un goût pour la peau|libre|
|[Ajb9KHStcuJ2ENqg.htm](impossible-lands-bestiary-items/Ajb9KHStcuJ2ENqg.htm)|Split|Scission|libre|
|[ampKTxuQeRMnCDFC.htm](impossible-lands-bestiary-items/ampKTxuQeRMnCDFC.htm)|Tail|Queue|libre|
|[AnEGInz7N3l1tbEq.htm](impossible-lands-bestiary-items/AnEGInz7N3l1tbEq.htm)|Negative Healing|Guérison négative|libre|
|[ATYdz5E54DuLSoxk.htm](impossible-lands-bestiary-items/ATYdz5E54DuLSoxk.htm)|Conformance|Conformation|libre|
|[B3WrLCJIWAcuAeYs.htm](impossible-lands-bestiary-items/B3WrLCJIWAcuAeYs.htm)|Claw|Griffe|libre|
|[bd2Bxzbndod4WaZQ.htm](impossible-lands-bestiary-items/bd2Bxzbndod4WaZQ.htm)|Bastard Sword|+3,greaterStriking|Épée bâtarde de frappe supérieure +3|libre|
|[ByG35Mm0ihKQR578.htm](impossible-lands-bestiary-items/ByG35Mm0ihKQR578.htm)|Architectural Lore|Connaissance de l'architecture|libre|
|[BzXwrTvCBxJHT9u6.htm](impossible-lands-bestiary-items/BzXwrTvCBxJHT9u6.htm)|Wind-Up|Remonter un automate|libre|
|[cJRISKgZ5VsnHdhi.htm](impossible-lands-bestiary-items/cJRISKgZ5VsnHdhi.htm)|Improved Push 15 feet|Bousculade améliorée 4,5 mètres|libre|
|[CKIVIdYktVxXXtw2.htm](impossible-lands-bestiary-items/CKIVIdYktVxXXtw2.htm)|Fanged-Maw Finger|Doigt gueule-crochue|libre|
|[cX6w047KDcTYZxse.htm](impossible-lands-bestiary-items/cX6w047KDcTYZxse.htm)|Regeneration 10 (Deactivated by Cold Iron)|Régénération 10 (Désactivée par le fer froid)|libre|
|[D5B2HG8wJ5TtWu8c.htm](impossible-lands-bestiary-items/D5B2HG8wJ5TtWu8c.htm)|Curse of Stone|Malédiction de pierre|libre|
|[db1nDSlbX73gPD9U.htm](impossible-lands-bestiary-items/db1nDSlbX73gPD9U.htm)|Twin Defenders|Défenseurs jumeaux|libre|
|[ddDvnd23sZWFzoxt.htm](impossible-lands-bestiary-items/ddDvnd23sZWFzoxt.htm)|Wear Flesh|Porter la chair|libre|
|[DlQCtMdmZTTVAxRv.htm](impossible-lands-bestiary-items/DlQCtMdmZTTVAxRv.htm)|Destructive Charge|Charge destructrice|libre|
|[DSdNdFbiF9GQYxuk.htm](impossible-lands-bestiary-items/DSdNdFbiF9GQYxuk.htm)|Jaws|Mâchoire|libre|
|[DVThmJrdhHWi62Yw.htm](impossible-lands-bestiary-items/DVThmJrdhHWi62Yw.htm)|Tail Sweep|Balayage avec la queue|libre|
|[e1fm0dEDzFsOAiME.htm](impossible-lands-bestiary-items/e1fm0dEDzFsOAiME.htm)|Stone Flesh|Peau en pierre|libre|
|[E3dHEpv6XhjYjVZy.htm](impossible-lands-bestiary-items/E3dHEpv6XhjYjVZy.htm)|Negative Healing|Guérison négative|libre|
|[eFDHvwqwltjwqJlQ.htm](impossible-lands-bestiary-items/eFDHvwqwltjwqJlQ.htm)|True Seeing (Constant)|Vision lucide (constant)|libre|
|[EHhuSsYROqEqswRC.htm](impossible-lands-bestiary-items/EHhuSsYROqEqswRC.htm)|Negative Healing|Guérison négative|libre|
|[EpEyfv8OLfwd06J6.htm](impossible-lands-bestiary-items/EpEyfv8OLfwd06J6.htm)|Trample|Piétinement|libre|
|[eZro5U6mdKafCnrR.htm](impossible-lands-bestiary-items/eZro5U6mdKafCnrR.htm)|Eye Beam|Rayon oculaire|libre|
|[fkcwHO1dnyPLc1x0.htm](impossible-lands-bestiary-items/fkcwHO1dnyPLc1x0.htm)|Motion Sense|Perception du mouvement|libre|
|[FkfXWMpE0nsQUZOW.htm](impossible-lands-bestiary-items/FkfXWMpE0nsQUZOW.htm)|Bombard|Bombarde|libre|
|[fM5TkztLOObStHW2.htm](impossible-lands-bestiary-items/fM5TkztLOObStHW2.htm)|Paired Reconstruction|Reconstruction appariée|libre|
|[fnpZ6rdjZO4giOMG.htm](impossible-lands-bestiary-items/fnpZ6rdjZO4giOMG.htm)|Powder Blast|Explosion de poudre|libre|
|[fUkXWkQ67PMtMhrU.htm](impossible-lands-bestiary-items/fUkXWkQ67PMtMhrU.htm)|False Authority|Fausse autorité|libre|
|[geTGlSERKHb2Jjiz.htm](impossible-lands-bestiary-items/geTGlSERKHb2Jjiz.htm)|Defensive Curl|Courbure défensive|libre|
|[gkXa9hD1dnoSxatR.htm](impossible-lands-bestiary-items/gkXa9hD1dnoSxatR.htm)|Defender's Link|Lien du défenseur|libre|
|[gvmQpjy5T16FCqIV.htm](impossible-lands-bestiary-items/gvmQpjy5T16FCqIV.htm)|Gestalt|Gestalt|libre|
|[HAUW8kierQRW9NNp.htm](impossible-lands-bestiary-items/HAUW8kierQRW9NNp.htm)|All-Around Vision|Vision panoramique|libre|
|[hOLCYiLG6vCZSfXj.htm](impossible-lands-bestiary-items/hOLCYiLG6vCZSfXj.htm)|Fin|Nageoire|libre|
|[HuJO5QXhMlFOavak.htm](impossible-lands-bestiary-items/HuJO5QXhMlFOavak.htm)|Regeneration 20 (Deactivated by Cold Iron)|Régénération 20 (Désactivée par le fer froid)|libre|
|[IwBguu4yMR2YlPne.htm](impossible-lands-bestiary-items/IwBguu4yMR2YlPne.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|libre|
|[j26Lepuho7qRRsJ8.htm](impossible-lands-bestiary-items/j26Lepuho7qRRsJ8.htm)|Arcane Innate Spells|Sorts arcaniques innés|libre|
|[JfKCalt1jPXqeWKU.htm](impossible-lands-bestiary-items/JfKCalt1jPXqeWKU.htm)|Tissue Siphon|Siphon de tissus|libre|
|[JRVNlfIPlDWOJtj4.htm](impossible-lands-bestiary-items/JRVNlfIPlDWOJtj4.htm)|Song of the Lost|Chant du perdu|libre|
|[JxWJ79KAoE1u5yxK.htm](impossible-lands-bestiary-items/JxWJ79KAoE1u5yxK.htm)|Bore|Tube de canon|libre|
|[k0DnGvKuOa0mXrCC.htm](impossible-lands-bestiary-items/k0DnGvKuOa0mXrCC.htm)|Earth Block|Blocage de la pierre|libre|
|[kiwfN8nqWl1wDvoB.htm](impossible-lands-bestiary-items/kiwfN8nqWl1wDvoB.htm)|Cannonball|Boulet de canon|libre|
|[KOpvNQxJ1AQE1wtx.htm](impossible-lands-bestiary-items/KOpvNQxJ1AQE1wtx.htm)|Rugged Travel|Tout terrain|libre|
|[kQRYLLy5FQP0r7YN.htm](impossible-lands-bestiary-items/kQRYLLy5FQP0r7YN.htm)|Deadly Pursuit|Poursuite mortelle|libre|
|[KVg8aQDdtGbbwMY8.htm](impossible-lands-bestiary-items/KVg8aQDdtGbbwMY8.htm)|Negative Healing|Guérison négative|libre|
|[l21tZ86jrnLJdnHQ.htm](impossible-lands-bestiary-items/l21tZ86jrnLJdnHQ.htm)|Blast|Déflagration|libre|
|[L6SWQmnHWcKt4MuR.htm](impossible-lands-bestiary-items/L6SWQmnHWcKt4MuR.htm)|Grasp Power|Poigne de puissance|libre|
|[Lu89daMaRaJYkzCs.htm](impossible-lands-bestiary-items/Lu89daMaRaJYkzCs.htm)|Grab|Empoignade/Agrippement|libre|
|[mDiUkroqGjfvpZMC.htm](impossible-lands-bestiary-items/mDiUkroqGjfvpZMC.htm)|+1 Status to All Saves vs. Magic|Bonus de statut de +1 aux jets de sauvegarde contre la magie|libre|
|[MgHEQlKl76Fh28UE.htm](impossible-lands-bestiary-items/MgHEQlKl76Fh28UE.htm)|Berserk|Berserker|libre|
|[mJN86SaFPTByONSb.htm](impossible-lands-bestiary-items/mJN86SaFPTByONSb.htm)|Claw|Griffe|libre|
|[o86qm3Mg0q1yymC7.htm](impossible-lands-bestiary-items/o86qm3Mg0q1yymC7.htm)|Gunpowder Residue|Résidu de poudrière|libre|
|[OjPUqmgDC3LTm57H.htm](impossible-lands-bestiary-items/OjPUqmgDC3LTm57H.htm)|Ballista|Balliste|libre|
|[p388FhVDIQJNBinE.htm](impossible-lands-bestiary-items/p388FhVDIQJNBinE.htm)|Grab|Empoignade/Agrippement|libre|
|[PtehHqk7jrGM6pXO.htm](impossible-lands-bestiary-items/PtehHqk7jrGM6pXO.htm)|Pseudopod|Pseudopode|libre|
|[q0VsEUr8eeQrnQcV.htm](impossible-lands-bestiary-items/q0VsEUr8eeQrnQcV.htm)|Improved Knockdown|Renversement amélioré|libre|
|[Q4Prh3oB51Pdb02x.htm](impossible-lands-bestiary-items/Q4Prh3oB51Pdb02x.htm)|Maw|Gueule|libre|
|[QLpS6QL7iaYN78dd.htm](impossible-lands-bestiary-items/QLpS6QL7iaYN78dd.htm)|Lambent Beam|Rayon lumineux|libre|
|[QNFAJ7JlgnNbGGWf.htm](impossible-lands-bestiary-items/QNFAJ7JlgnNbGGWf.htm)|Usurper's Curse|Malédiction de l'usurpateur|libre|
|[r9mk3iMPZnVsYyaH.htm](impossible-lands-bestiary-items/r9mk3iMPZnVsYyaH.htm)|Reconstitution|Reconstitution|libre|
|[raXABiXCuDEiZOMD.htm](impossible-lands-bestiary-items/raXABiXCuDEiZOMD.htm)|Serrated Plates|Écailles coupantes|libre|
|[s1hc2hOjKqwxRlnL.htm](impossible-lands-bestiary-items/s1hc2hOjKqwxRlnL.htm)|Jaws|Mâchoire|libre|
|[s9VkD48pnMfkx30o.htm](impossible-lands-bestiary-items/s9VkD48pnMfkx30o.htm)|Body|Corps|libre|
|[SX8d2G4CzdvaLK5t.htm](impossible-lands-bestiary-items/SX8d2G4CzdvaLK5t.htm)|Battering Ram|Bélier|libre|
|[sXjGsScjLQ7DuSAO.htm](impossible-lands-bestiary-items/sXjGsScjLQ7DuSAO.htm)|Shuddering Skitter|Frisson de la dermine|libre|
|[T67rfyPzxmU6ckF4.htm](impossible-lands-bestiary-items/T67rfyPzxmU6ckF4.htm)|Usurper's Curse|Malédiction de l'usurpateur|libre|
|[tXoMMmsGRgIihcEf.htm](impossible-lands-bestiary-items/tXoMMmsGRgIihcEf.htm)|Earthen Shard|Écharde de terre|libre|
|[TZfUr6m0adsvTIF5.htm](impossible-lands-bestiary-items/TZfUr6m0adsvTIF5.htm)|Magic Sense (Imprecise) 30 feet|Perception de la magie 9 m (imprécis)|libre|
|[u3CMszU8CMc4atfm.htm](impossible-lands-bestiary-items/u3CMszU8CMc4atfm.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[uCiVp2qb8CNPeikX.htm](impossible-lands-bestiary-items/uCiVp2qb8CNPeikX.htm)|Intense Chill|Froid intense|libre|
|[UDGHzHklMiuitqjk.htm](impossible-lands-bestiary-items/UDGHzHklMiuitqjk.htm)|Muzzleburn|Brûlure de la gueule|libre|
|[Vpcaqhg3BJT4qyTu.htm](impossible-lands-bestiary-items/Vpcaqhg3BJT4qyTu.htm)|Nail|Ongle|libre|
|[vvjOTuOMXpAw3Ixd.htm](impossible-lands-bestiary-items/vvjOTuOMXpAw3Ixd.htm)|Numbing Ice|Froid engourdissant|libre|
|[VwsS4RJG0PTpcKyP.htm](impossible-lands-bestiary-items/VwsS4RJG0PTpcKyP.htm)|Tremorsense (Imprecise) 60 feet|Perception des vibrations 18 mètres (imprécis)|libre|
|[WF36bi2QkqysgEuy.htm](impossible-lands-bestiary-items/WF36bi2QkqysgEuy.htm)|Caustic Leak|Fuite caustique|libre|
|[Wi73J9y2ljudFu0i.htm](impossible-lands-bestiary-items/Wi73J9y2ljudFu0i.htm)|Empathic Sense|Perception empathique|libre|
|[wLibJdF8sVsM43a8.htm](impossible-lands-bestiary-items/wLibJdF8sVsM43a8.htm)|Golem Antimagic|Antimagie du golem|libre|
|[Xe2GOMh7IIFQD8Uu.htm](impossible-lands-bestiary-items/Xe2GOMh7IIFQD8Uu.htm)|Tail Sweep|Balayage avec la queue|libre|
|[XlZ6tISKXPBfkK9U.htm](impossible-lands-bestiary-items/XlZ6tISKXPBfkK9U.htm)|Surgical Detachment|Détachement chirurgical|libre|
|[xqkprkuAXQV5nDla.htm](impossible-lands-bestiary-items/xqkprkuAXQV5nDla.htm)|Stealth|Discrétion|libre|
|[YzH31yZdhxR2eUCh.htm](impossible-lands-bestiary-items/YzH31yZdhxR2eUCh.htm)|Share the Skin|Partager la peau|libre|
|[ZdqlTV2GE4fHp7o7.htm](impossible-lands-bestiary-items/ZdqlTV2GE4fHp7o7.htm)|Vulnerable to Banishment|Vulnérable à Bannissement|libre|
