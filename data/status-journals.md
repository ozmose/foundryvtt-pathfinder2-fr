# État de la traduction (journals)

 * **libre**: 4
 * **officielle**: 1


Dernière mise à jour: 2023-07-12 20:28 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[BSp4LUSaOmUyjBko.htm](journals/BSp4LUSaOmUyjBko.htm)|Hero Point Deck|Cartes de points d'héroïsme|libre|
|[EEZvDB1Z7ezwaxIr.htm](journals/EEZvDB1Z7ezwaxIr.htm)|Domains|Domaines|libre|
|[S55aqwWIzpQRFhcq.htm](journals/S55aqwWIzpQRFhcq.htm)|GM Screen|Écran du MJ|libre|
|[vx5FGEG34AxI2dow.htm](journals/vx5FGEG34AxI2dow.htm)|Archetypes|Archétypes|officielle|
|[xtrW5GEtPPuXR6k2.htm](journals/xtrW5GEtPPuXR6k2.htm)|Deep Backgrounds|Backgrounds/Historiques approfondis|libre|
