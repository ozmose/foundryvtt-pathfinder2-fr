# État de la traduction (age-of-ashes-bestiary-items)

 * **officielle**: 1084
 * **libre**: 116


Dernière mise à jour: 2023-07-12 20:28 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[055E78h70itsjy7c.htm](age-of-ashes-bestiary-items/055E78h70itsjy7c.htm)|Grab|Empoignade|officielle|
|[06QX0MyzRFRMZtye.htm](age-of-ashes-bestiary-items/06QX0MyzRFRMZtye.htm)|Scimitar|Cimeterre|officielle|
|[09IvgWSDnJI3o0MQ.htm](age-of-ashes-bestiary-items/09IvgWSDnJI3o0MQ.htm)|Darkvision|Vision dans le noir|officielle|
|[0a45Roiy20vW4l7O.htm](age-of-ashes-bestiary-items/0a45Roiy20vW4l7O.htm)|Invisibility (At Will)|Invisibilité (À volonté)|officielle|
|[0AiUaTUE96AdfKo2.htm](age-of-ashes-bestiary-items/0AiUaTUE96AdfKo2.htm)|Hunt Prey|Chasser une proie|officielle|
|[0At2kB40YzOYoP4s.htm](age-of-ashes-bestiary-items/0At2kB40YzOYoP4s.htm)|Manly Left Hook|Viril crochet du droit|officielle|
|[0D5GZXOXBKSd2dFV.htm](age-of-ashes-bestiary-items/0D5GZXOXBKSd2dFV.htm)|Dagger|+1,cold-iron,disrupting|Dague en fer froid perturbatrice +1|libre|
|[0DQIaDtqyVpcJATv.htm](age-of-ashes-bestiary-items/0DQIaDtqyVpcJATv.htm)|+2 Greater Resilient Chain Shirt|Chemise de mailles de résilience supérieure +2|libre|
|[0dYMOi8x5GT5a1Yq.htm](age-of-ashes-bestiary-items/0dYMOi8x5GT5a1Yq.htm)|1-2 Acid Rain|1–2 Pluie acide|officielle|
|[0Eqa5Be8keCH6y7q.htm](age-of-ashes-bestiary-items/0Eqa5Be8keCH6y7q.htm)|Light Mace|Masse d'armes légères|officielle|
|[0ew9olScumzeKG3i.htm](age-of-ashes-bestiary-items/0ew9olScumzeKG3i.htm)|Dart|Fléchette|officielle|
|[0fMOfuFCNYpqOsRI.htm](age-of-ashes-bestiary-items/0fMOfuFCNYpqOsRI.htm)|Maul|Maillet|officielle|
|[0fpr7D6z7lRzEK6s.htm](age-of-ashes-bestiary-items/0fpr7D6z7lRzEK6s.htm)|Dagger|silver|Dague en argent|libre|
|[0J60fBJDJuBW0TbY.htm](age-of-ashes-bestiary-items/0J60fBJDJuBW0TbY.htm)|Dagger|Dague|officielle|
|[0JCPDdK7GNe3JLNR.htm](age-of-ashes-bestiary-items/0JCPDdK7GNe3JLNR.htm)|Tail|Queue|officielle|
|[0lgDXPlAG6oZy8ks.htm](age-of-ashes-bestiary-items/0lgDXPlAG6oZy8ks.htm)|Shortbow|+1|Arc court +1|libre|
|[0N1KAl4hWrBUvv2S.htm](age-of-ashes-bestiary-items/0N1KAl4hWrBUvv2S.htm)|Darkvision|Vision dans le noir|officielle|
|[0n2OnJwSpxVhh6zy.htm](age-of-ashes-bestiary-items/0n2OnJwSpxVhh6zy.htm)|+3 Status to All Saves vs. Divine Magic|+3 de statut aux sauvegardes contre la magie divine|officielle|
|[0pbel7KeTVy39RkF.htm](age-of-ashes-bestiary-items/0pbel7KeTVy39RkF.htm)|At-Will Spells|Sorts à volonté|officielle|
|[0rm1ESKWoiQPJPo1.htm](age-of-ashes-bestiary-items/0rm1ESKWoiQPJPo1.htm)|Shrieking Frenzy|Frénésie hurlante|libre|
|[0rU9AWHWD11ECFwB.htm](age-of-ashes-bestiary-items/0rU9AWHWD11ECFwB.htm)|Boundless Reprisals|Représailles illimitées|officielle|
|[0tHs3BRTN0UIM4j7.htm](age-of-ashes-bestiary-items/0tHs3BRTN0UIM4j7.htm)|Ash Web Spores|Spores de toile cendrée|officielle|
|[0tyhJYUBZLs3j7c9.htm](age-of-ashes-bestiary-items/0tyhJYUBZLs3j7c9.htm)|Low-Light Vision|Vision nocturne|officielle|
|[0vtEhVnugRtcZgdh.htm](age-of-ashes-bestiary-items/0vtEhVnugRtcZgdh.htm)|Attack of Opportunity (Special)|Attaque d'opportunité (Spéciale)|officielle|
|[0YBsTxxadKi2UTcv.htm](age-of-ashes-bestiary-items/0YBsTxxadKi2UTcv.htm)|Scoundrel|Scélérat|officielle|
|[0z7Fxpm5UuCECxe5.htm](age-of-ashes-bestiary-items/0z7Fxpm5UuCECxe5.htm)|Efficient Capture|Capture efficace|officielle|
|[117QFjXSLlaZ1zZl.htm](age-of-ashes-bestiary-items/117QFjXSLlaZ1zZl.htm)|Improved Knockdown|Renversement amélioré|officielle|
|[15vIK60zfhRa5WGH.htm](age-of-ashes-bestiary-items/15vIK60zfhRa5WGH.htm)|Uncanny Bombs|Bombes stupéfiantes|officielle|
|[15Yz15Mslx43Yuro.htm](age-of-ashes-bestiary-items/15Yz15Mslx43Yuro.htm)|Body Slam|Secousse du corps|officielle|
|[197Q2S7Pdx8jKLm7.htm](age-of-ashes-bestiary-items/197Q2S7Pdx8jKLm7.htm)|Darkvision|Vision dans le noir|officielle|
|[1AvPepGSMr92x8Jz.htm](age-of-ashes-bestiary-items/1AvPepGSMr92x8Jz.htm)|Fist|Poing|officielle|
|[1BZlxGhTFq59fpA6.htm](age-of-ashes-bestiary-items/1BZlxGhTFq59fpA6.htm)|Infused Items|Objets imprégnés|officielle|
|[1h7ULVYpJvCMccHA.htm](age-of-ashes-bestiary-items/1h7ULVYpJvCMccHA.htm)|Jaws|Mâchoires|officielle|
|[1mhGkpmF4fQNlduu.htm](age-of-ashes-bestiary-items/1mhGkpmF4fQNlduu.htm)|Rust|Rouille|officielle|
|[1o1raLwrZRGxcwrA.htm](age-of-ashes-bestiary-items/1o1raLwrZRGxcwrA.htm)|Destructive Frenzy|Frénésie destructrice|officielle|
|[1oZUGa8uGtIoemuM.htm](age-of-ashes-bestiary-items/1oZUGa8uGtIoemuM.htm)|Scholar's Robes|Vêtements d'érudit|officielle|
|[1pNOXTSqIPHQk8Lw.htm](age-of-ashes-bestiary-items/1pNOXTSqIPHQk8Lw.htm)|Scent (Imprecise) 120 feet|Odorat 36 m (imprécis)|officielle|
|[1pOgJ4dFgPo6yfdm.htm](age-of-ashes-bestiary-items/1pOgJ4dFgPo6yfdm.htm)|Dagger|Dague|officielle|
|[1QF8m8mUgLiyrAeW.htm](age-of-ashes-bestiary-items/1QF8m8mUgLiyrAeW.htm)|Swift Capture|Capture rapide|officielle|
|[1rRyEALEgAvhDMWc.htm](age-of-ashes-bestiary-items/1rRyEALEgAvhDMWc.htm)|Imitate Door|Apparence de porte|officielle|
|[1sYunHFicjStIbRN.htm](age-of-ashes-bestiary-items/1sYunHFicjStIbRN.htm)|Begin the Hunt|Lancer la chasse|officielle|
|[1WAPJkfVFJzVJi9o.htm](age-of-ashes-bestiary-items/1WAPJkfVFJzVJi9o.htm)|Claw|Griffe|officielle|
|[1wkVUb6UVwlesFiF.htm](age-of-ashes-bestiary-items/1wkVUb6UVwlesFiF.htm)|Darkvision|Vision dans le noir|officielle|
|[1WQFCxrBbvgqJCnn.htm](age-of-ashes-bestiary-items/1WQFCxrBbvgqJCnn.htm)|Claw|Griffe|officielle|
|[1wtSED5UzPWNuVfD.htm](age-of-ashes-bestiary-items/1wtSED5UzPWNuVfD.htm)|Negative Healing|Guérison négative|officielle|
|[1Zo0SRU37QPsakyi.htm](age-of-ashes-bestiary-items/1Zo0SRU37QPsakyi.htm)|Thrown Rock|Projection de rocher|officielle|
|[21SVgDPf6zOVZqq8.htm](age-of-ashes-bestiary-items/21SVgDPf6zOVZqq8.htm)|Silver Dagger|Dague en argent|officielle|
|[21tiRXk96SAqD3GL.htm](age-of-ashes-bestiary-items/21tiRXk96SAqD3GL.htm)|Divine Prepared Spells|Sorts divins préparés|libre|
|[23aybhsSs40twjme.htm](age-of-ashes-bestiary-items/23aybhsSs40twjme.htm)|Wall of Fire (At Will)|Mur de feu (À volonté)|officielle|
|[23MQbc0sTY6HeULo.htm](age-of-ashes-bestiary-items/23MQbc0sTY6HeULo.htm)|Rapier|Rapière|officielle|
|[29nZUXJwv5knWe9B.htm](age-of-ashes-bestiary-items/29nZUXJwv5knWe9B.htm)|Shoddy Blunderbuss|Tromblon de mauvaise qualité|officielle|
|[2AlP13lCWmjbH1ea.htm](age-of-ashes-bestiary-items/2AlP13lCWmjbH1ea.htm)|Staff|Bâton|officielle|
|[2j69CqOoQwIanSEG.htm](age-of-ashes-bestiary-items/2j69CqOoQwIanSEG.htm)|Rejuvenation|Reconstruction|officielle|
|[2md78psqZkdESFj7.htm](age-of-ashes-bestiary-items/2md78psqZkdESFj7.htm)|Dagger|Dague|officielle|
|[2mthLx7yReK93i3n.htm](age-of-ashes-bestiary-items/2mthLx7yReK93i3n.htm)|Grab|Empoignade|officielle|
|[2MUmPkWIDTgvArGY.htm](age-of-ashes-bestiary-items/2MUmPkWIDTgvArGY.htm)|Greater Alchemist's Fire|Feu grégeois supérieur imprégné|officielle|
|[2MzdlMtLJGxFzqB3.htm](age-of-ashes-bestiary-items/2MzdlMtLJGxFzqB3.htm)|Jaws|Mâchoires|officielle|
|[2nAk1Aknjdxt219t.htm](age-of-ashes-bestiary-items/2nAk1Aknjdxt219t.htm)|Darkvision|Vision dans le noir|officielle|
|[2OGTeZ56MmSCLl9P.htm](age-of-ashes-bestiary-items/2OGTeZ56MmSCLl9P.htm)|Terrifying Croak|Croassement terrifiant|officielle|
|[2shpOEob027noRHp.htm](age-of-ashes-bestiary-items/2shpOEob027noRHp.htm)|Darkvision|Vision dans le noir|officielle|
|[2tkYDAIULPFGgxZg.htm](age-of-ashes-bestiary-items/2tkYDAIULPFGgxZg.htm)|3-4 Freezing Wind|3–4 Vent glacé|officielle|
|[2Xd0tA3QaFBV8Nwk.htm](age-of-ashes-bestiary-items/2Xd0tA3QaFBV8Nwk.htm)|Silver Dagger|Dague en argent|officielle|
|[31OJENvCqE3md7mz.htm](age-of-ashes-bestiary-items/31OJENvCqE3md7mz.htm)|Portal Lore|Connaissance des portails|officielle|
|[31Y7LmRVIBxfyRRK.htm](age-of-ashes-bestiary-items/31Y7LmRVIBxfyRRK.htm)|Heavy Crossbow|+1,striking|Arbalète lourde de frappe +1|libre|
|[3Evl5c2LrCI0TdM8.htm](age-of-ashes-bestiary-items/3Evl5c2LrCI0TdM8.htm)|Darkvision|Vision dans le noir|officielle|
|[3f4ao6e0shuKxhhj.htm](age-of-ashes-bestiary-items/3f4ao6e0shuKxhhj.htm)|Low-Light Vision|Vision nocturne|officielle|
|[3fAiqgS5r4NDvcsP.htm](age-of-ashes-bestiary-items/3fAiqgS5r4NDvcsP.htm)|Low-Light Vision|Vision nocturne|officielle|
|[3h72FdcRfSluIGqq.htm](age-of-ashes-bestiary-items/3h72FdcRfSluIGqq.htm)|Jaws|Mâchoires|officielle|
|[3LczZ5phdTxD5h93.htm](age-of-ashes-bestiary-items/3LczZ5phdTxD5h93.htm)|Regeneration 20 (Deactivated by Cold)|Régénération 20 (désactivée par Froid)|officielle|
|[3nm3Uxd0Ev8aBu6h.htm](age-of-ashes-bestiary-items/3nm3Uxd0Ev8aBu6h.htm)|Fist|Poing|officielle|
|[3NMUDjuSV3mI03a1.htm](age-of-ashes-bestiary-items/3NMUDjuSV3mI03a1.htm)|Sack With 5 Rocks|Sac contenant 5 rochers|officielle|
|[3oioSefge5pvcqam.htm](age-of-ashes-bestiary-items/3oioSefge5pvcqam.htm)|Sneak Attack|Attaque sournoise|officielle|
|[3s5bvgFh0mAe2sgw.htm](age-of-ashes-bestiary-items/3s5bvgFh0mAe2sgw.htm)|Attack of Opportunity (Jaws only)|Attaque d'opportunité (mâchoires uniquement)|officielle|
|[3T41i9CjiNKKumzp.htm](age-of-ashes-bestiary-items/3T41i9CjiNKKumzp.htm)|Drain Life|Drain de vie|officielle|
|[3tkJJs6AuFOwEB2K.htm](age-of-ashes-bestiary-items/3tkJJs6AuFOwEB2K.htm)|Resanguinate|Resanguination|officielle|
|[3Tll1xyzUd3udZZZ.htm](age-of-ashes-bestiary-items/3Tll1xyzUd3udZZZ.htm)|Dragon Heat|Chaleur du dragon|officielle|
|[3U0dwrOeAIosNI9e.htm](age-of-ashes-bestiary-items/3U0dwrOeAIosNI9e.htm)|Swallow Whole|Gober|officielle|
|[3ueUOMbC3RvbqrJs.htm](age-of-ashes-bestiary-items/3ueUOMbC3RvbqrJs.htm)|Slice Legs|Taillader les jambes|officielle|
|[3xCCKuQS0ngUiwBo.htm](age-of-ashes-bestiary-items/3xCCKuQS0ngUiwBo.htm)|Draconic Momentum|Impulsion draconique|officielle|
|[3ZIUyyCzEz5jwled.htm](age-of-ashes-bestiary-items/3ZIUyyCzEz5jwled.htm)|Manacles (Average) (Marked with the Symbol of the Scarlet Triad)|Menottes (moyennes) (marquées du symbole de la Triade écarlate)|officielle|
|[40tzhYIHgsaEat6r.htm](age-of-ashes-bestiary-items/40tzhYIHgsaEat6r.htm)|Attack of Opportunity (Special)|Attaque d'opportunité (Spéciale)|officielle|
|[42HRKo2P9cQi4d4N.htm](age-of-ashes-bestiary-items/42HRKo2P9cQi4d4N.htm)|Heavy Crossbow|Arbalète lourde|officielle|
|[43THokQpj0pCe1oU.htm](age-of-ashes-bestiary-items/43THokQpj0pCe1oU.htm)|Contingency|Contingence|officielle|
|[454nSdnfQdhFZ4cJ.htm](age-of-ashes-bestiary-items/454nSdnfQdhFZ4cJ.htm)|Alchemical Formulas|Formules alchimiques|officielle|
|[49agY84bFYE0480h.htm](age-of-ashes-bestiary-items/49agY84bFYE0480h.htm)|Cold Iron Dagger|Dague en fer froid|officielle|
|[4IWeNCrddBnixyyR.htm](age-of-ashes-bestiary-items/4IWeNCrddBnixyyR.htm)|Dragonstorm Blade|Lame de tempête draconique|officielle|
|[4K4yW5oQfO6cRjez.htm](age-of-ashes-bestiary-items/4K4yW5oQfO6cRjez.htm)|Breath-Seared Sword|Épée à deux mains altérée par le Souffle|officielle|
|[4LjhH5zCnNYIijXd.htm](age-of-ashes-bestiary-items/4LjhH5zCnNYIijXd.htm)|Fist|Poing|officielle|
|[4RTJGyTlfqna9dXb.htm](age-of-ashes-bestiary-items/4RTJGyTlfqna9dXb.htm)|Sneak Attack|Attaque sournoise|officielle|
|[4So35zJiQ6sRy14O.htm](age-of-ashes-bestiary-items/4So35zJiQ6sRy14O.htm)|At-Will Spells|Sorts à volonté|officielle|
|[4tYBLforD5bIHqLq.htm](age-of-ashes-bestiary-items/4tYBLforD5bIHqLq.htm)|Illusory Disguise (At Will)|Déguisement illusoire (À volonté)|officielle|
|[4U9ib14fmpR2wh3D.htm](age-of-ashes-bestiary-items/4U9ib14fmpR2wh3D.htm)|Darkvision|Vision dans le noir|officielle|
|[4VBtm8sTSFf6jv0w.htm](age-of-ashes-bestiary-items/4VBtm8sTSFf6jv0w.htm)|Claw|Griffe|officielle|
|[4Wv4ON3tD5f4zi8r.htm](age-of-ashes-bestiary-items/4Wv4ON3tD5f4zi8r.htm)|Shortbow|Arc court|officielle|
|[4x2IFkW9CrMXOEC5.htm](age-of-ashes-bestiary-items/4x2IFkW9CrMXOEC5.htm)|Dual Abuse|Double sévices|officielle|
|[4YYh61RgLRJVFW9O.htm](age-of-ashes-bestiary-items/4YYh61RgLRJVFW9O.htm)|Elven Curve Blade|+3,majorStriking|Lame incurvée elfe de frappe majeure +3|libre|
|[50JvEJYUrD0euZsR.htm](age-of-ashes-bestiary-items/50JvEJYUrD0euZsR.htm)|Light Mace|+2,striking|Masse légère de frappe +2|libre|
|[53jaWddHn7SXaQse.htm](age-of-ashes-bestiary-items/53jaWddHn7SXaQse.htm)|Claw|Griffe|officielle|
|[578QS3EJCggOjkwq.htm](age-of-ashes-bestiary-items/578QS3EJCggOjkwq.htm)|Composite Longbow|+1,striking|Arc long composite de frappe +1|officielle|
|[5Fp56J3PmkRq827t.htm](age-of-ashes-bestiary-items/5Fp56J3PmkRq827t.htm)|Fast Swallow|Gober rapidement|officielle|
|[5G4MLloILn0eWN2G.htm](age-of-ashes-bestiary-items/5G4MLloILn0eWN2G.htm)|Divine Providence|Divine providence|officielle|
|[5i5r7EFzXOifPlnY.htm](age-of-ashes-bestiary-items/5i5r7EFzXOifPlnY.htm)|Invisibility (Self Only)|Invisibilité (soi uniquement)|officielle|
|[5MhFpMwgTDRkEcvj.htm](age-of-ashes-bestiary-items/5MhFpMwgTDRkEcvj.htm)|Heat Surge|Poussée de chaleur|officielle|
|[5PFC36aGHaF9Jyoe.htm](age-of-ashes-bestiary-items/5PFC36aGHaF9Jyoe.htm)|Hand Crossbow|Arbalète de poing|officielle|
|[5qZDSHwIQw5algfw.htm](age-of-ashes-bestiary-items/5qZDSHwIQw5algfw.htm)|Library Lore|Connaissance des bibliothèques|officielle|
|[5s7kaeGdKYRnxQlF.htm](age-of-ashes-bestiary-items/5s7kaeGdKYRnxQlF.htm)|Jaws|Mâchoires|officielle|
|[5SF4QUg5rtkR7Sy2.htm](age-of-ashes-bestiary-items/5SF4QUg5rtkR7Sy2.htm)|Mirror Shield|Bouclier miroir|officielle|
|[5UfaoE8p4Wo5eUod.htm](age-of-ashes-bestiary-items/5UfaoE8p4Wo5eUod.htm)|Dagger|Dague|officielle|
|[5uXkMRUCPP1LhCN7.htm](age-of-ashes-bestiary-items/5uXkMRUCPP1LhCN7.htm)|Darkvision|Vision dans le noir|officielle|
|[60TKTeYwUMmrQq1Q.htm](age-of-ashes-bestiary-items/60TKTeYwUMmrQq1Q.htm)|Engineering Lore|Connaissance de l'ingénierie|officielle|
|[61z76N54xAG9rCLF.htm](age-of-ashes-bestiary-items/61z76N54xAG9rCLF.htm)|Constant Spells|Sorts constants|officielle|
|[62jCcD7uSVwAG83f.htm](age-of-ashes-bestiary-items/62jCcD7uSVwAG83f.htm)|Mace|+1,striking|Masse d'armes de frappe +1|libre|
|[68LYg123W8ittlmS.htm](age-of-ashes-bestiary-items/68LYg123W8ittlmS.htm)|Command (At Will)|Injonction (À volonté)|officielle|
|[6CJRM2LT3a7O3Hhp.htm](age-of-ashes-bestiary-items/6CJRM2LT3a7O3Hhp.htm)|Constrict|Constriction|officielle|
|[6CWSsZ6ciRweK8gJ.htm](age-of-ashes-bestiary-items/6CWSsZ6ciRweK8gJ.htm)|Longsword|Épée longue|officielle|
|[6gQEeIYepYV659hU.htm](age-of-ashes-bestiary-items/6gQEeIYepYV659hU.htm)|Religious Symbol of Droskar|Symbole religieux de Droskar|officielle|
|[6gUaRoXq4cNuZm4W.htm](age-of-ashes-bestiary-items/6gUaRoXq4cNuZm4W.htm)|Athletics|Athlétisme|officielle|
|[6hxF1odU5xuRbrDX.htm](age-of-ashes-bestiary-items/6hxF1odU5xuRbrDX.htm)|Paralysis|Paralysie|officielle|
|[6js1QaHEpOIDiNH1.htm](age-of-ashes-bestiary-items/6js1QaHEpOIDiNH1.htm)|Fist|Poing|officielle|
|[6K2DndSSiWoNraQj.htm](age-of-ashes-bestiary-items/6K2DndSSiWoNraQj.htm)|Opportune Backstab|Coup de poignard opportuniste|officielle|
|[6kF6A19eKeAk7fOO.htm](age-of-ashes-bestiary-items/6kF6A19eKeAk7fOO.htm)|Jaws|Mâchoires|officielle|
|[6kjg8IiIh8GmUJa7.htm](age-of-ashes-bestiary-items/6kjg8IiIh8GmUJa7.htm)|Mobility|Mobilité|officielle|
|[6Lt3GJTTuXRvxjPp.htm](age-of-ashes-bestiary-items/6Lt3GJTTuXRvxjPp.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|officielle|
|[6m1q9eJjaPr21j44.htm](age-of-ashes-bestiary-items/6m1q9eJjaPr21j44.htm)|Redirect Energy|Rediriger l'énergie|officielle|
|[6mPULTjHntSqyOmv.htm](age-of-ashes-bestiary-items/6mPULTjHntSqyOmv.htm)|Javelin|Javeline|officielle|
|[6nqWpnR0S7DfLCp7.htm](age-of-ashes-bestiary-items/6nqWpnR0S7DfLCp7.htm)|Shortbow|+1|Arc court +1|libre|
|[6R4b8uNayGGrHJNm.htm](age-of-ashes-bestiary-items/6R4b8uNayGGrHJNm.htm)|Illusory Object (At Will)|Objet illusoire (À volonté)|officielle|
|[6RB1tHnIwHnJMvAR.htm](age-of-ashes-bestiary-items/6RB1tHnIwHnJMvAR.htm)|Composite Longbow|Arc long composite|officielle|
|[6SHvoR38wFsY94R8.htm](age-of-ashes-bestiary-items/6SHvoR38wFsY94R8.htm)|Negative Healing|Guérison négative|officielle|
|[6sPaytqHJB4OlRwk.htm](age-of-ashes-bestiary-items/6sPaytqHJB4OlRwk.htm)|Dagger|Dague|officielle|
|[6xNQdZfi7r9teJPg.htm](age-of-ashes-bestiary-items/6xNQdZfi7r9teJPg.htm)|Fist|Poing|officielle|
|[6XZ1oUx9X7nxnnLP.htm](age-of-ashes-bestiary-items/6XZ1oUx9X7nxnnLP.htm)|Breath Weapon|Souffle|officielle|
|[7067hk1ZShJt1wS4.htm](age-of-ashes-bestiary-items/7067hk1ZShJt1wS4.htm)|Thrown Weapon Mastery|Maîtrise des armes de jet|officielle|
|[70hKpMI6lW9A6okT.htm](age-of-ashes-bestiary-items/70hKpMI6lW9A6okT.htm)|Unnerving Gaze|Regard déstabilisant|officielle|
|[7FqmJPlXVaPfi1Q8.htm](age-of-ashes-bestiary-items/7FqmJPlXVaPfi1Q8.htm)|Bomb Barrage|Déluge de bombes|officielle|
|[7im3eC8rElLqJkOE.htm](age-of-ashes-bestiary-items/7im3eC8rElLqJkOE.htm)|Nimble Dodge|Esquive agile|officielle|
|[7imiZn40Mr9yePd1.htm](age-of-ashes-bestiary-items/7imiZn40Mr9yePd1.htm)|Hunted Shot|Tir du chasseur|officielle|
|[7LPUMs664rIwb4pr.htm](age-of-ashes-bestiary-items/7LPUMs664rIwb4pr.htm)|Terrifying Squeal|Couinement terrifiant|officielle|
|[7nBGTwZnc14evBxB.htm](age-of-ashes-bestiary-items/7nBGTwZnc14evBxB.htm)|Enternal Damnation|Damnation éternelle|officielle|
|[7nMzSCqgZtUZbgud.htm](age-of-ashes-bestiary-items/7nMzSCqgZtUZbgud.htm)|Frightful Presence|Présence terrifiante|officielle|
|[7OH0rFP8UnDTxfdD.htm](age-of-ashes-bestiary-items/7OH0rFP8UnDTxfdD.htm)|Throw Rock|Lancé de rochers|libre|
|[7P75nDnE4avWwiZu.htm](age-of-ashes-bestiary-items/7P75nDnE4avWwiZu.htm)|Breath Weapon|Souffle|officielle|
|[7s7604XlS2m6VCsl.htm](age-of-ashes-bestiary-items/7s7604XlS2m6VCsl.htm)|+3 Greater Resilient Full Plate|Harnois de résilience supérieure +3|officielle|
|[7sAyxHFuVZ6ydyvs.htm](age-of-ashes-bestiary-items/7sAyxHFuVZ6ydyvs.htm)|Regeneration 30 (Deactivated by Cold Iron)|Régénération 30 (désactivée par Fer froid)|officielle|
|[7SEc5hjoyVGbbNng.htm](age-of-ashes-bestiary-items/7SEc5hjoyVGbbNng.htm)|Intimidating Strike|Frappe intimidante|officielle|
|[7sEHksBXglrNJ2nG.htm](age-of-ashes-bestiary-items/7sEHksBXglrNJ2nG.htm)|Jaws|Mâchoires|officielle|
|[7wgXd0InH2pMtroD.htm](age-of-ashes-bestiary-items/7wgXd0InH2pMtroD.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|officielle|
|[7wokY8VtRk0KvDhV.htm](age-of-ashes-bestiary-items/7wokY8VtRk0KvDhV.htm)|Manifest|Se manifester|officielle|
|[7WZNigBilIpl5GhG.htm](age-of-ashes-bestiary-items/7WZNigBilIpl5GhG.htm)|Flurry of Daggers|Déluge de dagues|officielle|
|[7YRi5wuplNhSsBsZ.htm](age-of-ashes-bestiary-items/7YRi5wuplNhSsBsZ.htm)|Darkvision|Vision dans le noir|officielle|
|[7zonVNduNgQTkcUe.htm](age-of-ashes-bestiary-items/7zonVNduNgQTkcUe.htm)|Acid Flask (Major) [Infused]|Fiole d'acide majeure [imprégné]|officielle|
|[85LHfl8B6bj27rPs.htm](age-of-ashes-bestiary-items/85LHfl8B6bj27rPs.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[85uLuXBsMIG8xwb8.htm](age-of-ashes-bestiary-items/85uLuXBsMIG8xwb8.htm)|Fangs|Crocs|officielle|
|[8BX0w5bbGwyCsIQB.htm](age-of-ashes-bestiary-items/8BX0w5bbGwyCsIQB.htm)|Jaws|Mâchoires|officielle|
|[8BZpNNw0UfSHhtxH.htm](age-of-ashes-bestiary-items/8BZpNNw0UfSHhtxH.htm)|Mask Settlement|Masquer une communauté|officielle|
|[8dMHcMp0PP7ZO27V.htm](age-of-ashes-bestiary-items/8dMHcMp0PP7ZO27V.htm)|Deep Breath|Inspiration profonde|officielle|
|[8Hel9Nl2oYP8kVQc.htm](age-of-ashes-bestiary-items/8Hel9Nl2oYP8kVQc.htm)|Sickle|Serpe|officielle|
|[8hzvjvbfBWJpAFep.htm](age-of-ashes-bestiary-items/8hzvjvbfBWJpAFep.htm)|Jaws|Mâchoires|officielle|
|[8ImDtLzIZFTzCASM.htm](age-of-ashes-bestiary-items/8ImDtLzIZFTzCASM.htm)|+1 Resilient Breastplate|Cuirasse de résilience +1|officielle|
|[8KfKjg5fTmB7ZCal.htm](age-of-ashes-bestiary-items/8KfKjg5fTmB7ZCal.htm)|Darkvision|Vision dans le noir|officielle|
|[8l7ngHz7yzWzu3vM.htm](age-of-ashes-bestiary-items/8l7ngHz7yzWzu3vM.htm)|Elven Curve Blade|Lame courbe elfique|officielle|
|[8nd5VVeytqa77pD8.htm](age-of-ashes-bestiary-items/8nd5VVeytqa77pD8.htm)|Rapier|Rapière|officielle|
|[8NJr1zJWkswMN2qH.htm](age-of-ashes-bestiary-items/8NJr1zJWkswMN2qH.htm)|Reactive|Réactif|officielle|
|[8OAdLpknHQWsmvmE.htm](age-of-ashes-bestiary-items/8OAdLpknHQWsmvmE.htm)|Orange Eye Beam|Rayon oculaire orange|officielle|
|[8sgTj0l0kmAK5fpR.htm](age-of-ashes-bestiary-items/8sgTj0l0kmAK5fpR.htm)|Collapse Ceiling|Écroulement du toit|officielle|
|[8V6geW11QKk6SncS.htm](age-of-ashes-bestiary-items/8V6geW11QKk6SncS.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|officielle|
|[8vIB5BOiDuY5uD2a.htm](age-of-ashes-bestiary-items/8vIB5BOiDuY5uD2a.htm)|Regeneration 25 (Deactivated by Good or Silver)|Régénération 25 (désactivée par Bon ou Argent)|officielle|
|[8XB58su9CoZLqKH7.htm](age-of-ashes-bestiary-items/8XB58su9CoZLqKH7.htm)|Fist|Poing|officielle|
|[8XwJYwcx1a2Md13M.htm](age-of-ashes-bestiary-items/8XwJYwcx1a2Md13M.htm)|Tail|Queue|officielle|
|[91EDD4ZPxtMkhm0e.htm](age-of-ashes-bestiary-items/91EDD4ZPxtMkhm0e.htm)|Negative Healing|Guérison négative|officielle|
|[95BHm1GQDEedURfw.htm](age-of-ashes-bestiary-items/95BHm1GQDEedURfw.htm)|Scent (Imprecise) 60 feet|Odorat 18 m (imprécis)|officielle|
|[97RfnojA9doZCy1G.htm](age-of-ashes-bestiary-items/97RfnojA9doZCy1G.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|officielle|
|[9A3DxTCqkhPOFzSd.htm](age-of-ashes-bestiary-items/9A3DxTCqkhPOFzSd.htm)|Weapon Supremacy|Suprématie martiale|officielle|
|[9APnpN5PuTxG8v98.htm](age-of-ashes-bestiary-items/9APnpN5PuTxG8v98.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|officielle|
|[9b70qoAMc6GR4eCV.htm](age-of-ashes-bestiary-items/9b70qoAMc6GR4eCV.htm)|Negative Healing|Guérison négative|officielle|
|[9dzKJk6ywUvYJxGr.htm](age-of-ashes-bestiary-items/9dzKJk6ywUvYJxGr.htm)|Efficient Capture|Capture efficace|officielle|
|[9EtKLNxC3l6ph7LD.htm](age-of-ashes-bestiary-items/9EtKLNxC3l6ph7LD.htm)|Eight Coils|Huit anneaux|officielle|
|[9FXyozNfJ4cDMvMy.htm](age-of-ashes-bestiary-items/9FXyozNfJ4cDMvMy.htm)|Manacles (Average) (Marked with the Symbol of the Scarlet Triad)|Menottes (moyennes) (marquées du symbole de la Triade écarlate)|officielle|
|[9h6YoWxYy7gK1ukK.htm](age-of-ashes-bestiary-items/9h6YoWxYy7gK1ukK.htm)|Stealth|Discrétion|officielle|
|[9jVBK8J30sirxvYJ.htm](age-of-ashes-bestiary-items/9jVBK8J30sirxvYJ.htm)|Javelin|Javeline|officielle|
|[9kCNk71urnz8zzJ8.htm](age-of-ashes-bestiary-items/9kCNk71urnz8zzJ8.htm)|Frighten|Effrayer|officielle|
|[9Ogunu3AaCrgsxpU.htm](age-of-ashes-bestiary-items/9Ogunu3AaCrgsxpU.htm)|Telekinetic Crystalline Assault|Assaut télékinésique cristallin|officielle|
|[9RaYMbm66QxVktif.htm](age-of-ashes-bestiary-items/9RaYMbm66QxVktif.htm)|Shipboard Savvy|Savoir-faire maritime|officielle|
|[9SKMKDVUzRsDh3cL.htm](age-of-ashes-bestiary-items/9SKMKDVUzRsDh3cL.htm)|Darkvision|Vision dans le noir|officielle|
|[9uKEXs9NLIvxXisk.htm](age-of-ashes-bestiary-items/9uKEXs9NLIvxXisk.htm)|Green Eye Beam|Rayon oculaire vert|officielle|
|[9V1OCxFjilGcKV8s.htm](age-of-ashes-bestiary-items/9V1OCxFjilGcKV8s.htm)|Major Frost Vial|Fiole de givre majeure|officielle|
|[9x8nl6F6ULV0kslr.htm](age-of-ashes-bestiary-items/9x8nl6F6ULV0kslr.htm)|Tongue|Langue|officielle|
|[9yiCjxLdJV4KSZ7b.htm](age-of-ashes-bestiary-items/9yiCjxLdJV4KSZ7b.htm)|Giant Scorpion Venom (Applied to Bolts)|Venin de scorpion géant (appliqué aux carreaux)|officielle|
|[9yNmVUtApEo0Ky5V.htm](age-of-ashes-bestiary-items/9yNmVUtApEo0Ky5V.htm)|Dead Spells|Sorts morts|officielle|
|[A4k38xHP7GlSzTCt.htm](age-of-ashes-bestiary-items/A4k38xHP7GlSzTCt.htm)|Whip|Fouet|officielle|
|[A4w2Db2zI5g3QPOa.htm](age-of-ashes-bestiary-items/A4w2Db2zI5g3QPOa.htm)|Negative Healing|Guérison négative|officielle|
|[abiEy2lj68FImtR8.htm](age-of-ashes-bestiary-items/abiEy2lj68FImtR8.htm)|Enslave Soul|Esclavage d'âme|officielle|
|[AdsHpmWCgQto5myp.htm](age-of-ashes-bestiary-items/AdsHpmWCgQto5myp.htm)|Attack of Opportunity (Jaws Only)|Attaque d'opportunité (Mâchoires uniquement)|officielle|
|[Aebmgw6Y4cwVgvM0.htm](age-of-ashes-bestiary-items/Aebmgw6Y4cwVgvM0.htm)|Shield Spikes|+2,greaterStriking|Pointes de bouclier de frappe supérieure +2|officielle|
|[AGJUwsQFIjEtEsuB.htm](age-of-ashes-bestiary-items/AGJUwsQFIjEtEsuB.htm)|Natural Invisibility|Invisibilité naturelle|officielle|
|[aGM9Fk35YNBZ55lZ.htm](age-of-ashes-bestiary-items/aGM9Fk35YNBZ55lZ.htm)|Eternal Damnation|Damnation éternelle|officielle|
|[agUYP6p6kB6OegCM.htm](age-of-ashes-bestiary-items/agUYP6p6kB6OegCM.htm)|Verdant Staff|Bâton verdoyant|officielle|
|[agvYAJn23dKfPmcI.htm](age-of-ashes-bestiary-items/agvYAJn23dKfPmcI.htm)|Branch|Branche|officielle|
|[aGyOlzElbjBAx2ru.htm](age-of-ashes-bestiary-items/aGyOlzElbjBAx2ru.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|libre|
|[AHGxpzkTgjwABgBa.htm](age-of-ashes-bestiary-items/AHGxpzkTgjwABgBa.htm)|Pseudopod|Pseudopode|officielle|
|[AhUdp5npAh56MGtg.htm](age-of-ashes-bestiary-items/AhUdp5npAh56MGtg.htm)|Shell Spikes|Pointes de carapace|officielle|
|[ajoryTcPkySSPZU4.htm](age-of-ashes-bestiary-items/ajoryTcPkySSPZU4.htm)|Darkness (At Will)|Ténèbres (À volonté)|officielle|
|[AjstPqHuQD2eALaa.htm](age-of-ashes-bestiary-items/AjstPqHuQD2eALaa.htm)|Whip|+2,striking,corrosive|Fouet corrosif de frappe +2|officielle|
|[ajsXyQpKZDv0k3qJ.htm](age-of-ashes-bestiary-items/ajsXyQpKZDv0k3qJ.htm)|Ring of Fire Resistance|Anneau de résistance au feu|officielle|
|[AjWArPc1kXvPmWvW.htm](age-of-ashes-bestiary-items/AjWArPc1kXvPmWvW.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[AKFVNPYKCZ4jZLKw.htm](age-of-ashes-bestiary-items/AKFVNPYKCZ4jZLKw.htm)|Necrotic Pulse|Pulsation nécrotique|officielle|
|[AkuMvwXObEyEbsN4.htm](age-of-ashes-bestiary-items/AkuMvwXObEyEbsN4.htm)|Corrosive Whip|Fouet corrosif|officielle|
|[AlC9QRa4quIAs8NK.htm](age-of-ashes-bestiary-items/AlC9QRa4quIAs8NK.htm)|Cinderclaw Gauntlet|+1,striking|Gantelet de la Griffe des braises|officielle|
|[Am5pbCNVDkDBNz5Y.htm](age-of-ashes-bestiary-items/Am5pbCNVDkDBNz5Y.htm)|Stench|Puanteur|officielle|
|[aM8ZIzzGKo3LwAaz.htm](age-of-ashes-bestiary-items/aM8ZIzzGKo3LwAaz.htm)|Cleric Domain Spells|Sorts de domaine de prêtre|libre|
|[aMtBg34djNUQyLrP.htm](age-of-ashes-bestiary-items/aMtBg34djNUQyLrP.htm)|Telepathy 1 Mile|Télépathie à 1,5 km|officielle|
|[anEeBSbM8nCZzVZ4.htm](age-of-ashes-bestiary-items/anEeBSbM8nCZzVZ4.htm)|Scent (Imprecise) 120 feet|Odorat 36 m (imprécis)|officielle|
|[aocJaGIdE31itQEp.htm](age-of-ashes-bestiary-items/aocJaGIdE31itQEp.htm)|Swift Slash|Lacération rapide|officielle|
|[aod69CMkW6wAO7gp.htm](age-of-ashes-bestiary-items/aod69CMkW6wAO7gp.htm)|Spear|Lance|officielle|
|[AP6aPJLtb3i0qgVG.htm](age-of-ashes-bestiary-items/AP6aPJLtb3i0qgVG.htm)|Dagger|Dague|officielle|
|[Ari4IpvWQTIYV64K.htm](age-of-ashes-bestiary-items/Ari4IpvWQTIYV64K.htm)|Yellow Eye Beam|Rayon oculaire jaune|officielle|
|[aTEZKPVNhE0QO16t.htm](age-of-ashes-bestiary-items/aTEZKPVNhE0QO16t.htm)|Underworld Lore|Connaissance de la pègre|officielle|
|[aTXAOXeP9oRRcBfj.htm](age-of-ashes-bestiary-items/aTXAOXeP9oRRcBfj.htm)|Grasping Hands|Mains agrippeuses|officielle|
|[axG8Gqi3ZFCSb6l3.htm](age-of-ashes-bestiary-items/axG8Gqi3ZFCSb6l3.htm)|Staff|Bâton|officielle|
|[AXpLna1FjT83aiYJ.htm](age-of-ashes-bestiary-items/AXpLna1FjT83aiYJ.htm)|Tormenting Touch|Contact tortionnaire|officielle|
|[AxvNayOm3jws9vMf.htm](age-of-ashes-bestiary-items/AxvNayOm3jws9vMf.htm)|Constrict|Constriction|officielle|
|[ayGFqy96KRzgh1Jx.htm](age-of-ashes-bestiary-items/ayGFqy96KRzgh1Jx.htm)|Magma Splash|Éclaboussure de magma|officielle|
|[ayPIYrcz520FXkl7.htm](age-of-ashes-bestiary-items/ayPIYrcz520FXkl7.htm)|Sap|Matraque|officielle|
|[AyzlkA8BDbzOj0oI.htm](age-of-ashes-bestiary-items/AyzlkA8BDbzOj0oI.htm)|Longbow|+1,striking|Arc long de frappe +1|libre|
|[AzkWSZ1llpwDtMLX.htm](age-of-ashes-bestiary-items/AzkWSZ1llpwDtMLX.htm)|Spin Silk|Tissage de soie|officielle|
|[b0MtgA6YzYyKSJQr.htm](age-of-ashes-bestiary-items/b0MtgA6YzYyKSJQr.htm)|+1 Resilient Leather Armor|Armure en cuir de résilience +1|officielle|
|[B2pDIbpVOV0J4SPQ.htm](age-of-ashes-bestiary-items/B2pDIbpVOV0J4SPQ.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[B2pWjBsWDmisLrz0.htm](age-of-ashes-bestiary-items/B2pWjBsWDmisLrz0.htm)|Running Poison Strike|Frappe empoisonnée empressée|officielle|
|[b4gT3E9tko3GkAav.htm](age-of-ashes-bestiary-items/b4gT3E9tko3GkAav.htm)|Dagger|Dague|officielle|
|[B6yK16tyh4kEH9SU.htm](age-of-ashes-bestiary-items/B6yK16tyh4kEH9SU.htm)|Negative Healing|Guérison négative|officielle|
|[b8bCRfgf4RGqbIsZ.htm](age-of-ashes-bestiary-items/b8bCRfgf4RGqbIsZ.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[b9ku7PIEzSu9iFAq.htm](age-of-ashes-bestiary-items/b9ku7PIEzSu9iFAq.htm)|Breath Weapon|Souffle|officielle|
|[bbD52Yw4aCU9xjzy.htm](age-of-ashes-bestiary-items/bbD52Yw4aCU9xjzy.htm)|Composite Longbow|Arc long composite|officielle|
|[BbG4VRHLFcOdy1kI.htm](age-of-ashes-bestiary-items/BbG4VRHLFcOdy1kI.htm)|Darkvision|Vision dans le noir|officielle|
|[bbL3QbriTw1sy7so.htm](age-of-ashes-bestiary-items/bbL3QbriTw1sy7so.htm)|Belch Smoke|Rejet de fumée|officielle|
|[bc7DxJeQl2oX946w.htm](age-of-ashes-bestiary-items/bc7DxJeQl2oX946w.htm)|Woodland Stride|Déplacement facilité en forêt|officielle|
|[BCcVa7LcDbfMataD.htm](age-of-ashes-bestiary-items/BCcVa7LcDbfMataD.htm)|Rejuvenation|Reconstruction|officielle|
|[bD1gctOA2Ni82NSM.htm](age-of-ashes-bestiary-items/bD1gctOA2Ni82NSM.htm)|Breath-Seared Greatsword|Épée à deux mains altérée par le Souffle|officielle|
|[BDUlQffqQeFK7d1K.htm](age-of-ashes-bestiary-items/BDUlQffqQeFK7d1K.htm)|Focused Breath Weapon|Souffle focalisé|officielle|
|[BdxoNSpPKQa1FYSZ.htm](age-of-ashes-bestiary-items/BdxoNSpPKQa1FYSZ.htm)|Soul Chain|Chaîne d'âme|officielle|
|[BEdK4O8CNtdvGUvX.htm](age-of-ashes-bestiary-items/BEdK4O8CNtdvGUvX.htm)|Shadow Strike|Attaque des ombres|officielle|
|[bFgmfCe3rBGAWZK1.htm](age-of-ashes-bestiary-items/bFgmfCe3rBGAWZK1.htm)|Darkvision|Vision dans le noir|officielle|
|[bfmkqrbi3vcWnEQ8.htm](age-of-ashes-bestiary-items/bfmkqrbi3vcWnEQ8.htm)|Hunt Prey|Chasser une proie|officielle|
|[BFNXF7LIEPpdAngu.htm](age-of-ashes-bestiary-items/BFNXF7LIEPpdAngu.htm)|Composite Longbow|+1,striking|Arc long composite de frappe +1|officielle|
|[bFQ8s2UKCuHaBspm.htm](age-of-ashes-bestiary-items/bFQ8s2UKCuHaBspm.htm)|Noxious Breath|Souffle nocif|officielle|
|[BiJxTD0UCp5DwYqu.htm](age-of-ashes-bestiary-items/BiJxTD0UCp5DwYqu.htm)|Nimble Dodge|Esquive agile|officielle|
|[BIvhzgu4Bi6t171L.htm](age-of-ashes-bestiary-items/BIvhzgu4Bi6t171L.htm)|Deny Advantage|Refus d'avantage|officielle|
|[Bizy0PzkQAYT8X7H.htm](age-of-ashes-bestiary-items/Bizy0PzkQAYT8X7H.htm)|Heat Rock|Rocher incandescent|officielle|
|[BJZe244H2HjWtcpq.htm](age-of-ashes-bestiary-items/BJZe244H2HjWtcpq.htm)|Lava Bomb|Bombe de lave|officielle|
|[bK1xdYaxhyB8kJli.htm](age-of-ashes-bestiary-items/bK1xdYaxhyB8kJli.htm)|Sudden Charge|Charge soudaine|officielle|
|[BKBcAo4en90X1m11.htm](age-of-ashes-bestiary-items/BKBcAo4en90X1m11.htm)|Arcane Prepared Spells|Sorts arcaniques préparés|libre|
|[BKJk7pGIcKhoCKA6.htm](age-of-ashes-bestiary-items/BKJk7pGIcKhoCKA6.htm)|Forge Breath|Souffle de la forge|officielle|
|[bKMNN43L5lX1I6nY.htm](age-of-ashes-bestiary-items/bKMNN43L5lX1I6nY.htm)|+1 Chain Shirt|Chemise de mailles +1|officielle|
|[bLCC2BkHqAMGHsin.htm](age-of-ashes-bestiary-items/bLCC2BkHqAMGHsin.htm)|Kukri|+2,greaterStriking,thundering|Kukri de tonnerre de frappe supérieure +2|libre|
|[bLoaxjzxhJesDzcL.htm](age-of-ashes-bestiary-items/bLoaxjzxhJesDzcL.htm)|Golem Antimagic|Antimagie du golem|officielle|
|[BmogbygVHyhJ5dIR.htm](age-of-ashes-bestiary-items/BmogbygVHyhJ5dIR.htm)|Dagger|+2,striking|Dague de frappe +2|libre|
|[bnOBGha4PmmSNkZJ.htm](age-of-ashes-bestiary-items/bnOBGha4PmmSNkZJ.htm)|Stench|Puanteur|officielle|
|[bQGNTrxdCyY34M1f.htm](age-of-ashes-bestiary-items/bQGNTrxdCyY34M1f.htm)|Thunderstone (Greater) [Infused]|Pierre à tonnerre supérieure (imprégnée)|officielle|
|[brN9awmqYtvkfScZ.htm](age-of-ashes-bestiary-items/brN9awmqYtvkfScZ.htm)|Scroll of True Strike (Level 1)|Parchemin de Coup au but (niveau 1)|officielle|
|[Brr7VPPaAfdPuEoy.htm](age-of-ashes-bestiary-items/Brr7VPPaAfdPuEoy.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[BSFOYE4jxrwLzw75.htm](age-of-ashes-bestiary-items/BSFOYE4jxrwLzw75.htm)|Regeneration 25|Régénération 25|officielle|
|[bTTWJNcprDJ2oony.htm](age-of-ashes-bestiary-items/bTTWJNcprDJ2oony.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[BtXKfb6sra2e5xGW.htm](age-of-ashes-bestiary-items/BtXKfb6sra2e5xGW.htm)|Tail|Queue|officielle|
|[BuGYWvXzoxguBhaX.htm](age-of-ashes-bestiary-items/BuGYWvXzoxguBhaX.htm)|Quick Dervish Strike|Rapide frappe du derviche|officielle|
|[bUNqaCaQSkkB6se8.htm](age-of-ashes-bestiary-items/bUNqaCaQSkkB6se8.htm)|+1 Status to All Saves vs. Mental|bonus de statut de +1 aux JdS contre les effets mentaux|officielle|
|[BvK09Yczybu8kLG6.htm](age-of-ashes-bestiary-items/BvK09Yczybu8kLG6.htm)|Darkvision|Vision dans le noir|officielle|
|[bvXcRvh1dBNmRcSg.htm](age-of-ashes-bestiary-items/bvXcRvh1dBNmRcSg.htm)|Live a Thousand Lives|Vivre mille vies|officielle|
|[BWXMuY5UwGtZJiaG.htm](age-of-ashes-bestiary-items/BWXMuY5UwGtZJiaG.htm)|Knockdown|Renversement|officielle|
|[C18MqFsSOmOKFyGk.htm](age-of-ashes-bestiary-items/C18MqFsSOmOKFyGk.htm)|Divine Prepared Spells|Sorts divins préparés|libre|
|[c2BbfXmMmIl5wmhi.htm](age-of-ashes-bestiary-items/c2BbfXmMmIl5wmhi.htm)|Infrared Vision|Vision infrarouge|officielle|
|[C5HAmEH4wICnDXXA.htm](age-of-ashes-bestiary-items/C5HAmEH4wICnDXXA.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|libre|
|[c68riQyo1R1CLItH.htm](age-of-ashes-bestiary-items/c68riQyo1R1CLItH.htm)|+1 Leather Armor|Armure en cuir +1|officielle|
|[C9IfyXUudzvj3eop.htm](age-of-ashes-bestiary-items/C9IfyXUudzvj3eop.htm)|Deep Breath|Inspiration profonde|officielle|
|[CadRGJpqw2lsmgp4.htm](age-of-ashes-bestiary-items/CadRGJpqw2lsmgp4.htm)|Droskar Lore|Connaissance de Droskar|officielle|
|[CadzQEjQFp41MtkP.htm](age-of-ashes-bestiary-items/CadzQEjQFp41MtkP.htm)|Blowgun|Sarbacane|officielle|
|[cbNmcndriv0G3Pqu.htm](age-of-ashes-bestiary-items/cbNmcndriv0G3Pqu.htm)|Dagger|Dague|officielle|
|[cBPi7wQqwCZJ4GR7.htm](age-of-ashes-bestiary-items/cBPi7wQqwCZJ4GR7.htm)|Efficient Capture|Capture efficace|officielle|
|[CCddbUlZDpZVHPi9.htm](age-of-ashes-bestiary-items/CCddbUlZDpZVHPi9.htm)|Arcane Spells Known|Sorts arcaniques connus|libre|
|[cCgcYbkQbcYvPXDB.htm](age-of-ashes-bestiary-items/cCgcYbkQbcYvPXDB.htm)|+1 Clothing (Explorer's)|Vêtements d'explorateur +1|officielle|
|[CClklHku7hq4O9Iy.htm](age-of-ashes-bestiary-items/CClklHku7hq4O9Iy.htm)|Dragon Lore|Connaissance des dragons|officielle|
|[ceaQ24wBGNxFLxDc.htm](age-of-ashes-bestiary-items/ceaQ24wBGNxFLxDc.htm)|Dagger|Dague|officielle|
|[CFIFmdujP4DOl3KL.htm](age-of-ashes-bestiary-items/CFIFmdujP4DOl3KL.htm)|Thrown Weapon Mastery|Maîtrise des armes de jet|officielle|
|[CgQzFuFeDdZQSFQL.htm](age-of-ashes-bestiary-items/CgQzFuFeDdZQSFQL.htm)|Jaws|Mâchoires|officielle|
|[cH86pXBWXoCzfFuA.htm](age-of-ashes-bestiary-items/cH86pXBWXoCzfFuA.htm)|Rapier|+1,striking|Rapière de frappe +1|officielle|
|[CJe3a5GahNX20FL8.htm](age-of-ashes-bestiary-items/CJe3a5GahNX20FL8.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[cjGAqJPJESzxQoEi.htm](age-of-ashes-bestiary-items/cjGAqJPJESzxQoEi.htm)|Maul|+1,striking|Maillet de Frappe +1|libre|
|[CKxM9QsObYnbO5fx.htm](age-of-ashes-bestiary-items/CKxM9QsObYnbO5fx.htm)|Dagger|Dague|officielle|
|[CNMsxGsJc8C2klMn.htm](age-of-ashes-bestiary-items/CNMsxGsJc8C2klMn.htm)|Air Walk (constant)|Marche dans les airs (constant)|officielle|
|[CP15DfrCLkhY6tX0.htm](age-of-ashes-bestiary-items/CP15DfrCLkhY6tX0.htm)|Darkvision|Vision dans le noir|officielle|
|[cP2We7SRbI7YuRzt.htm](age-of-ashes-bestiary-items/cP2We7SRbI7YuRzt.htm)|Constrict|Constriction|officielle|
|[cpgcQN4JOvn67e7q.htm](age-of-ashes-bestiary-items/cpgcQN4JOvn67e7q.htm)|Jaws|Mâchoires|officielle|
|[cS1gFj0x5OkGUnhg.htm](age-of-ashes-bestiary-items/cS1gFj0x5OkGUnhg.htm)|Wyvern Poison [Applied to 4 Bolts]|Poison de vouivre [Appliqué à 4 carreaux]|officielle|
|[cstX7ITEtTu7WN4L.htm](age-of-ashes-bestiary-items/cstX7ITEtTu7WN4L.htm)|Vine Lash|Cinglement de liane|officielle|
|[cSY3pNZ6AkV4mhbe.htm](age-of-ashes-bestiary-items/cSY3pNZ6AkV4mhbe.htm)|Goblin Scuttle|Précipitation gobeline|officielle|
|[ct0XvSPn4vZOIhf3.htm](age-of-ashes-bestiary-items/ct0XvSPn4vZOIhf3.htm)|Punish Imperfection|Imperfection fatale|officielle|
|[CU86HVtetBBtNPBx.htm](age-of-ashes-bestiary-items/CU86HVtetBBtNPBx.htm)|Dragonstorm Strike|Frappe de tempête draconique|officielle|
|[cUPegejySeJGT7RK.htm](age-of-ashes-bestiary-items/cUPegejySeJGT7RK.htm)|Corpse Disguise|Camouflage macabre|officielle|
|[CVnZaJ028qTQ4fIE.htm](age-of-ashes-bestiary-items/CVnZaJ028qTQ4fIE.htm)|Darkvision|Vision dans le noir|officielle|
|[D1apG8hCM6fFYauw.htm](age-of-ashes-bestiary-items/D1apG8hCM6fFYauw.htm)|Breath Weapon|Souffle|officielle|
|[d42IPyLbN0f6KFtq.htm](age-of-ashes-bestiary-items/d42IPyLbN0f6KFtq.htm)|Whip|+1,striking|Fouet de frappe +1|officielle|
|[d5HPFb8SkWD4lJZS.htm](age-of-ashes-bestiary-items/d5HPFb8SkWD4lJZS.htm)|Dagger|Dague|officielle|
|[D72zZj9nlTG1c06S.htm](age-of-ashes-bestiary-items/D72zZj9nlTG1c06S.htm)|Horns|Corne|officielle|
|[d8AM5zYWUUZUAdDc.htm](age-of-ashes-bestiary-items/d8AM5zYWUUZUAdDc.htm)|Batter the Fallen|Rosser les gisants|officielle|
|[D8iQ4Gr9GPDsrYgl.htm](age-of-ashes-bestiary-items/D8iQ4Gr9GPDsrYgl.htm)|Searing Heat|Chaleur accablante|officielle|
|[D9pjDoEUMn7yYSQT.htm](age-of-ashes-bestiary-items/D9pjDoEUMn7yYSQT.htm)|Mudwalking|Marche dans la boue|officielle|
|[Da7jV5DGeFRpV5qb.htm](age-of-ashes-bestiary-items/Da7jV5DGeFRpV5qb.htm)|Unleash Fragments|Projection de fragments|officielle|
|[dBFmE3lCilk6yZX5.htm](age-of-ashes-bestiary-items/dBFmE3lCilk6yZX5.htm)|Dagger|+2,greaterStriking,corrosive|Dague corrosive de frappe supérieure +2|libre|
|[dcKVjyzEepP9jZIg.htm](age-of-ashes-bestiary-items/dcKVjyzEepP9jZIg.htm)|Light Hammer|+1,striking,returning|Marteau de guerre léger boomerang de frappe +1|libre|
|[DCLGBKaqRvqYPFZZ.htm](age-of-ashes-bestiary-items/DCLGBKaqRvqYPFZZ.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[DCw8NyXCQyGIT7dh.htm](age-of-ashes-bestiary-items/DCw8NyXCQyGIT7dh.htm)|Spear (Thrown)|Lance (lancer)|officielle|
|[dDFsE7xMeGqFnlPS.htm](age-of-ashes-bestiary-items/dDFsE7xMeGqFnlPS.htm)|Firebleed|Saignement de feu|officielle|
|[DdM0HuOCPDC9g1gC.htm](age-of-ashes-bestiary-items/DdM0HuOCPDC9g1gC.htm)|Rend|Éventration|officielle|
|[DDzd3ocX0kkjYD9e.htm](age-of-ashes-bestiary-items/DDzd3ocX0kkjYD9e.htm)|Trap Dodger|Esquive des pièges|officielle|
|[delWZwlGtrbwQofE.htm](age-of-ashes-bestiary-items/delWZwlGtrbwQofE.htm)|Shell Game|Passe-passe|officielle|
|[DEoIu6kGyGbwbSn3.htm](age-of-ashes-bestiary-items/DEoIu6kGyGbwbSn3.htm)|Consume Flesh|Dévorer la chair|officielle|
|[DF8C1D50NHzFNPco.htm](age-of-ashes-bestiary-items/DF8C1D50NHzFNPco.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[DFdEgb4naWOYCPXX.htm](age-of-ashes-bestiary-items/DFdEgb4naWOYCPXX.htm)|Site Bound|Lié à un site|officielle|
|[dfNLOS9TpYGudTfO.htm](age-of-ashes-bestiary-items/dfNLOS9TpYGudTfO.htm)|Improved Grab|Empoignade améliorée|officielle|
|[dgaMs1IhlvBQeUzA.htm](age-of-ashes-bestiary-items/dgaMs1IhlvBQeUzA.htm)|Gold Jewelry|Bijou en or|officielle|
|[DH9wyO9XNqPjz1gc.htm](age-of-ashes-bestiary-items/DH9wyO9XNqPjz1gc.htm)|Bonds of Iron|Liens de fer|officielle|
|[dI9RG9Y1v3nG3JkI.htm](age-of-ashes-bestiary-items/dI9RG9Y1v3nG3JkI.htm)|Longbow|Arc long|officielle|
|[DjmGUYM8SGPzUgRQ.htm](age-of-ashes-bestiary-items/DjmGUYM8SGPzUgRQ.htm)|Telekinetic Assault|Assaut télékinésique|officielle|
|[DJoAQhbwF6EFhlAL.htm](age-of-ashes-bestiary-items/DJoAQhbwF6EFhlAL.htm)|Swallow Whole|Gober|officielle|
|[dKp6yeEMkDRQTEq9.htm](age-of-ashes-bestiary-items/dKp6yeEMkDRQTEq9.htm)|Greatclub|Massue|officielle|
|[DlwhGb66IrdDlQFU.htm](age-of-ashes-bestiary-items/DlwhGb66IrdDlQFU.htm)|Hand|Main|officielle|
|[Dmc8CB0p5bleq65p.htm](age-of-ashes-bestiary-items/Dmc8CB0p5bleq65p.htm)|Soul Shriek|Hurlement de l'âme|officielle|
|[DMn2jw41YgSewTab.htm](age-of-ashes-bestiary-items/DMn2jw41YgSewTab.htm)|Leg|Patte|officielle|
|[Do1UK3MaHGPk6Raa.htm](age-of-ashes-bestiary-items/Do1UK3MaHGPk6Raa.htm)|Shrieking Frenzy|Frénésie hurlante|libre|
|[doKYunVCGEcZDNXb.htm](age-of-ashes-bestiary-items/doKYunVCGEcZDNXb.htm)|Light Blindness|Aveuglé par la lumière|officielle|
|[DOuyoqgTXzVADMKJ.htm](age-of-ashes-bestiary-items/DOuyoqgTXzVADMKJ.htm)|Attack of Opportunity (Special)|Attaque d'opportunité (Spéciale)|officielle|
|[DPwtg3OqirDYbsrI.htm](age-of-ashes-bestiary-items/DPwtg3OqirDYbsrI.htm)|Focus Gaze|Focaliser le regard|officielle|
|[dQFY1Er30mYSlRnj.htm](age-of-ashes-bestiary-items/dQFY1Er30mYSlRnj.htm)|Arcane Prepared Spells|Sorts arcaniques préparés|libre|
|[DqIf07lCKF6aemjq.htm](age-of-ashes-bestiary-items/DqIf07lCKF6aemjq.htm)|Jungle Stride|Déplacement facilité dans la jungle|officielle|
|[dqiO3lCiS7jAYB3f.htm](age-of-ashes-bestiary-items/dqiO3lCiS7jAYB3f.htm)|Improved Evasion|Évasion améliorée|officielle|
|[DqY7L33t5uJuYvA3.htm](age-of-ashes-bestiary-items/DqY7L33t5uJuYvA3.htm)|Key to Manacles|Clé des menottes|officielle|
|[DrODOsMSTXjJpF5S.htm](age-of-ashes-bestiary-items/DrODOsMSTXjJpF5S.htm)|Halberd|+1|Hallebarde +1|libre|
|[DtgWIHkl45Oz0I8c.htm](age-of-ashes-bestiary-items/DtgWIHkl45Oz0I8c.htm)|Telekinetic Maneuver (At Will)|Manoeuvre télékinésique (À volonté)|officielle|
|[dU7uiLZral2AnGbC.htm](age-of-ashes-bestiary-items/dU7uiLZral2AnGbC.htm)|Confuse|Confusion|officielle|
|[DuTAy3tlbbhX6YSj.htm](age-of-ashes-bestiary-items/DuTAy3tlbbhX6YSj.htm)|Darkvision|Vision dans le noir|officielle|
|[DW0ZzC1c6DBVQbus.htm](age-of-ashes-bestiary-items/DW0ZzC1c6DBVQbus.htm)|Blink (At Will)|Clignotement (À volonté)|officielle|
|[DxojGnsJOG0ItLxo.htm](age-of-ashes-bestiary-items/DxojGnsJOG0ItLxo.htm)|Darkvision|Vision dans le noir|officielle|
|[DyilSr4hYYAgoPFF.htm](age-of-ashes-bestiary-items/DyilSr4hYYAgoPFF.htm)|Reverberating Revenge|Vengeance réverbérante|officielle|
|[Dz4yG96ucUj1zePA.htm](age-of-ashes-bestiary-items/Dz4yG96ucUj1zePA.htm)|Draconic Frenzy|Frénésie draconique|officielle|
|[e0qpil2ll45f3L8R.htm](age-of-ashes-bestiary-items/e0qpil2ll45f3L8R.htm)|Greater Thunderstone|Pierre à tonnerre supérieure|officielle|
|[e2jWaDeB99NST1wo.htm](age-of-ashes-bestiary-items/e2jWaDeB99NST1wo.htm)|Negative Healing|Guérison négative|officielle|
|[e2Kf0nqsKQ0S7M4n.htm](age-of-ashes-bestiary-items/e2Kf0nqsKQ0S7M4n.htm)|Smoky Retreat|Repli au fumigène|officielle|
|[E6DkiwmjLTAKqArb.htm](age-of-ashes-bestiary-items/E6DkiwmjLTAKqArb.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[e73hhyixLdkOAGhD.htm](age-of-ashes-bestiary-items/e73hhyixLdkOAGhD.htm)|Javelin|Javeline|officielle|
|[e7C7CLUio6dT7UwM.htm](age-of-ashes-bestiary-items/e7C7CLUio6dT7UwM.htm)|Web|Toile|officielle|
|[E9FrySzNqUR0B1Hm.htm](age-of-ashes-bestiary-items/E9FrySzNqUR0B1Hm.htm)|Athletics|Athlétisme|officielle|
|[ebQ7QkhGrSSsXuy0.htm](age-of-ashes-bestiary-items/ebQ7QkhGrSSsXuy0.htm)|Drain Bonded Item|Drain d'objet lié|officielle|
|[EcbzD7TsltUXe4Ge.htm](age-of-ashes-bestiary-items/EcbzD7TsltUXe4Ge.htm)|Wing|Aile|officielle|
|[eCRAMtpF3k73LRFd.htm](age-of-ashes-bestiary-items/eCRAMtpF3k73LRFd.htm)|Paralysis|Paralysie|officielle|
|[EDnXO2OsX5YQGXQy.htm](age-of-ashes-bestiary-items/EDnXO2OsX5YQGXQy.htm)|Trample|Piétinement|officielle|
|[edOwB8VQ6gUQ1xnZ.htm](age-of-ashes-bestiary-items/edOwB8VQ6gUQ1xnZ.htm)|Telekinetic Maneuver (At Will)|Manoeuvre télékinésique (À volonté)|officielle|
|[EDSriTe4fC1hsq33.htm](age-of-ashes-bestiary-items/EDSriTe4fC1hsq33.htm)|Attack of Opportunity (Special)|Attaque d'opportunité (Spéciale)|officielle|
|[EenrewXIgDjYNZCt.htm](age-of-ashes-bestiary-items/EenrewXIgDjYNZCt.htm)|+2 Resilient Studded Leather Armor|Armure en cuir clouté de résilience +2|officielle|
|[EferdcZmtLTCxvjj.htm](age-of-ashes-bestiary-items/EferdcZmtLTCxvjj.htm)|Staff Mastery|Maîtrise du bâton|officielle|
|[efWcncs8XDaP6xQA.htm](age-of-ashes-bestiary-items/efWcncs8XDaP6xQA.htm)|Corrosive Dagger|Dague corrosive|officielle|
|[EFxhJV27viaJeJvR.htm](age-of-ashes-bestiary-items/EFxhJV27viaJeJvR.htm)|Longsword|+2,greaterStriking|Épée longue de frappe supérieure +2|libre|
|[Egs11D8l9LMkS7uI.htm](age-of-ashes-bestiary-items/Egs11D8l9LMkS7uI.htm)|Needle Rain|Pluie d'aiguilles|officielle|
|[EHn8GE3CrQAw7HfX.htm](age-of-ashes-bestiary-items/EHn8GE3CrQAw7HfX.htm)|Nightmare (see Dream Haunting)|Cauchemar (voir Hanter les rêves)|officielle|
|[EJfbSXCysRWwqSsI.htm](age-of-ashes-bestiary-items/EJfbSXCysRWwqSsI.htm)|Ventriloquism (At Will)|Ventriloquie (À volonté)|officielle|
|[ekJjxL6yjWuexk2l.htm](age-of-ashes-bestiary-items/ekJjxL6yjWuexk2l.htm)|Attack of Opportunity (Jaws Only)|Attaque d'opportunité (Mâchoires uniquement)|officielle|
|[Elbm7mEpf5b1tHsf.htm](age-of-ashes-bestiary-items/Elbm7mEpf5b1tHsf.htm)|+2 Status to All Saves vs. Magic|+2 de statut aux sauvegardes contre la magie|officielle|
|[EM1Qpnk3JXilo1Qb.htm](age-of-ashes-bestiary-items/EM1Qpnk3JXilo1Qb.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|officielle|
|[eM2vlUZYXujfEHUB.htm](age-of-ashes-bestiary-items/eM2vlUZYXujfEHUB.htm)|Bloodseeker Beak (Affixed to Rapier)|Trompe de cherchesang (fixée à la rapière)|officielle|
|[EMNCDSnJYrVEceeF.htm](age-of-ashes-bestiary-items/EMNCDSnJYrVEceeF.htm)|Fangs|Crocs|officielle|
|[eMRmzaCOQXRtZ69R.htm](age-of-ashes-bestiary-items/eMRmzaCOQXRtZ69R.htm)|Power Attack|Attaque en puissance|officielle|
|[EorcRyybCCQIkYR8.htm](age-of-ashes-bestiary-items/EorcRyybCCQIkYR8.htm)|Woodland Elf|Elfe des bois|officielle|
|[ePFuv9mcwpdpUPHc.htm](age-of-ashes-bestiary-items/ePFuv9mcwpdpUPHc.htm)|Claw|Griffe|officielle|
|[EPvcNjklx5lHjwSK.htm](age-of-ashes-bestiary-items/EPvcNjklx5lHjwSK.htm)|Counterspell|Contresort|officielle|
|[eQxLyu1oOuefKVbK.htm](age-of-ashes-bestiary-items/eQxLyu1oOuefKVbK.htm)|+2 Circumstance to All Saves to Disbelieve Illusions|+2 de circonstances aux sauvegardes pour discerner des illusions|officielle|
|[eTaUxizC6y1b7l3T.htm](age-of-ashes-bestiary-items/eTaUxizC6y1b7l3T.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|officielle|
|[ETzIyo5ZvX3NpC3J.htm](age-of-ashes-bestiary-items/ETzIyo5ZvX3NpC3J.htm)|Obliteration Beam|Rayon d'oblitération|officielle|
|[EuBw3avFZNz7BacT.htm](age-of-ashes-bestiary-items/EuBw3avFZNz7BacT.htm)|Dagger|Dague|officielle|
|[eV1zBAr9CgzQ2C5X.htm](age-of-ashes-bestiary-items/eV1zBAr9CgzQ2C5X.htm)|Spore Explosion|Explosion de spores|officielle|
|[eVfKn6MTgzYtBFUg.htm](age-of-ashes-bestiary-items/eVfKn6MTgzYtBFUg.htm)|Darkvision|Vision dans le noir|officielle|
|[EwJM5XNHsNxMYpSu.htm](age-of-ashes-bestiary-items/EwJM5XNHsNxMYpSu.htm)|Crafting|Artisanat|officielle|
|[eWRYW3sMrzor03KL.htm](age-of-ashes-bestiary-items/eWRYW3sMrzor03KL.htm)|Frightful Presence|Présence terrifiante|officielle|
|[eX2VLJ64ecfgdr3K.htm](age-of-ashes-bestiary-items/eX2VLJ64ecfgdr3K.htm)|Scimitar|Cimeterre|officielle|
|[EYE5NG1a4JuddG5h.htm](age-of-ashes-bestiary-items/EYE5NG1a4JuddG5h.htm)|Perception and Vision|Perception et vision|officielle|
|[F2trczQsM5ejlXEm.htm](age-of-ashes-bestiary-items/F2trczQsM5ejlXEm.htm)|Jaws|Mâchoires|officielle|
|[F3y7pLxdrd17fqdw.htm](age-of-ashes-bestiary-items/F3y7pLxdrd17fqdw.htm)|Death Throes|Agonie fatale|officielle|
|[FbmiYuEnbNstnXfj.htm](age-of-ashes-bestiary-items/FbmiYuEnbNstnXfj.htm)|Fleshroaster|+2,striking,flaming|Brûlepeau|officielle|
|[fBvRbrhTqjBsWGVa.htm](age-of-ashes-bestiary-items/fBvRbrhTqjBsWGVa.htm)|Draconic Frenzy|Frénésie draconique|officielle|
|[FDGrNkxmckSR32ou.htm](age-of-ashes-bestiary-items/FDGrNkxmckSR32ou.htm)|Forge Breath|Souffle de la forge|officielle|
|[FdjRwOKhKnzO4NIv.htm](age-of-ashes-bestiary-items/FdjRwOKhKnzO4NIv.htm)|Hermea Lore|Connaissance d'Herméa|officielle|
|[FGLOUpHNkmleuOIx.htm](age-of-ashes-bestiary-items/FGLOUpHNkmleuOIx.htm)|Frost Vial (Greater) [Infused]|Fiole de givre supérieure (imprégnée)|officielle|
|[FiaGipQmzzGWwPnw.htm](age-of-ashes-bestiary-items/FiaGipQmzzGWwPnw.htm)|Mind Reading (At Will)|Lecture des pensées (À volonté)|officielle|
|[fINveZMkSiZN9kMX.htm](age-of-ashes-bestiary-items/fINveZMkSiZN9kMX.htm)|Expunge|Oblitération|officielle|
|[Fj81D52uCvQB3Zp7.htm](age-of-ashes-bestiary-items/Fj81D52uCvQB3Zp7.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[fjgBPuKgD22yTRuQ.htm](age-of-ashes-bestiary-items/fjgBPuKgD22yTRuQ.htm)|Inject Unstable Mutagen|Injecter un mutagène instable|officielle|
|[fjxiygmwqMkFDeyM.htm](age-of-ashes-bestiary-items/fjxiygmwqMkFDeyM.htm)|Constant Spells|Sorts constants|officielle|
|[FLhJ8AT87HUFtG5w.htm](age-of-ashes-bestiary-items/FLhJ8AT87HUFtG5w.htm)|+1 Leather Armor|Armure en cuir +1|officielle|
|[FP0lfB9biTbzRAwD.htm](age-of-ashes-bestiary-items/FP0lfB9biTbzRAwD.htm)|Tail|Queue|officielle|
|[fQSHG7Wkydbvpqji.htm](age-of-ashes-bestiary-items/fQSHG7Wkydbvpqji.htm)|Alchemist's Fire (Major) [Infused]|Feu grégeois majeur [imprégné]|officielle|
|[FQumD5lxgm3RrE1g.htm](age-of-ashes-bestiary-items/FQumD5lxgm3RrE1g.htm)|Blindsight (Precise) 120 feet|Vision aveugle 36 m (précis)|officielle|
|[FrfAFUxOL2jRQG6y.htm](age-of-ashes-bestiary-items/FrfAFUxOL2jRQG6y.htm)|Darkvision|Vision dans le noir|officielle|
|[fRTC42KFOaC3bfxs.htm](age-of-ashes-bestiary-items/fRTC42KFOaC3bfxs.htm)|Disrupting Cold Iron Dagger|Dague en fer froid perturbatrice|officielle|
|[fS06Q6X2UX1yRC1w.htm](age-of-ashes-bestiary-items/fS06Q6X2UX1yRC1w.htm)|+1 Resilient Full Plate|Harnois de résilience +1|officielle|
|[fTtXaKs3sTtekS9Y.htm](age-of-ashes-bestiary-items/fTtXaKs3sTtekS9Y.htm)|Dimension Door (At Will)|Porte dimensionnelle (À volonté)|officielle|
|[FwcS5N0auHwePEe8.htm](age-of-ashes-bestiary-items/FwcS5N0auHwePEe8.htm)|Adroit Disarm|Désarmement habile|officielle|
|[fWPGUdYIRBw22fnz.htm](age-of-ashes-bestiary-items/fWPGUdYIRBw22fnz.htm)|Sneak Attack|Attaque sournoise|officielle|
|[fX2zoaLIfhwnjmIl.htm](age-of-ashes-bestiary-items/fX2zoaLIfhwnjmIl.htm)|Mercantile Lore|Connaissance commerciale|officielle|
|[fXz6WRXGipzaUAhE.htm](age-of-ashes-bestiary-items/fXz6WRXGipzaUAhE.htm)|Radiant Explosion|Explosion rayonnante|officielle|
|[fyWgrVIE0U0GZARU.htm](age-of-ashes-bestiary-items/fyWgrVIE0U0GZARU.htm)|Lifelike Scintillation|Scintillation réaliste|officielle|
|[fZeOgtuKuu2UdKeD.htm](age-of-ashes-bestiary-items/fZeOgtuKuu2UdKeD.htm)|Elixir of Life (True) [Infused]|Élixir de vie ultime (imprégné)|officielle|
|[Fzwm2tFZ9TfeKZxu.htm](age-of-ashes-bestiary-items/Fzwm2tFZ9TfeKZxu.htm)|Contingency|Contingence|officielle|
|[G04XQpPTsP0scmx5.htm](age-of-ashes-bestiary-items/G04XQpPTsP0scmx5.htm)|Shipboard Grace|Grâce maritime|officielle|
|[G0N1HOgAQ6sfApBK.htm](age-of-ashes-bestiary-items/G0N1HOgAQ6sfApBK.htm)|Darkvision|Vision dans le noir|officielle|
|[G2rX0J2oGGfLdB0F.htm](age-of-ashes-bestiary-items/G2rX0J2oGGfLdB0F.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[g3GoUN2RoHvK9OHE.htm](age-of-ashes-bestiary-items/g3GoUN2RoHvK9OHE.htm)|Breachill Lore|Connaissance de Brèchemont|officielle|
|[g6yamqCVKN9pYELp.htm](age-of-ashes-bestiary-items/g6yamqCVKN9pYELp.htm)|Inspiring Presence|Présence inspirante|officielle|
|[g9GO4tLTHh7ZeC4L.htm](age-of-ashes-bestiary-items/g9GO4tLTHh7ZeC4L.htm)|Soul Chain|+2,greaterStriking,adamantine,flaming|Chaîne des âmes|officielle|
|[GAMbl4nEeYZTf7VD.htm](age-of-ashes-bestiary-items/GAMbl4nEeYZTf7VD.htm)|Constant Spells|Sorts constants|officielle|
|[gca4Y5kOq7GHUaJp.htm](age-of-ashes-bestiary-items/gca4Y5kOq7GHUaJp.htm)|Bones of Stone|Ossements de pierre|officielle|
|[gDANDUYKwxqtpEpB.htm](age-of-ashes-bestiary-items/gDANDUYKwxqtpEpB.htm)|Graceful Double Slice|Double taille gracieuse|officielle|
|[GDrd8HWCNlKRmARz.htm](age-of-ashes-bestiary-items/GDrd8HWCNlKRmARz.htm)|Telekinetic Defense|Défense télékinésique|officielle|
|[GE9EMUIIHP8yDVMg.htm](age-of-ashes-bestiary-items/GE9EMUIIHP8yDVMg.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[gEAiXMZU4XPY9yTM.htm](age-of-ashes-bestiary-items/gEAiXMZU4XPY9yTM.htm)|Engulf|Engloutir|officielle|
|[GFSu7vcUlllwKJzY.htm](age-of-ashes-bestiary-items/GFSu7vcUlllwKJzY.htm)|Darkvision|Vision dans le noir|officielle|
|[gg05Cq52L76J5iG8.htm](age-of-ashes-bestiary-items/gg05Cq52L76J5iG8.htm)|Draconic Momentum|Impulsion draconique|officielle|
|[ggGBjbtHJJq44MAq.htm](age-of-ashes-bestiary-items/ggGBjbtHJJq44MAq.htm)|Claw|Griffe|officielle|
|[gGMQ3mV8xiQNHsV3.htm](age-of-ashes-bestiary-items/gGMQ3mV8xiQNHsV3.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[gHkFG2cVrCRf6Hui.htm](age-of-ashes-bestiary-items/gHkFG2cVrCRf6Hui.htm)|Maul|+2,striking,thundering|Maillet de Frappe de tonnerre|libre|
|[gIKh3WQBoiyrW9gn.htm](age-of-ashes-bestiary-items/gIKh3WQBoiyrW9gn.htm)|Manacles (Average) (Marked with the Symbol of the Scarlet Triad)|Menottes (moyennes) (marquées du symbole de la Triade écarlate)|officielle|
|[gJ5Si4cUxad6gAip.htm](age-of-ashes-bestiary-items/gJ5Si4cUxad6gAip.htm)|Eschew Materials|Dispense de composant matériel|officielle|
|[gjRN8wkee3LER65n.htm](age-of-ashes-bestiary-items/gjRN8wkee3LER65n.htm)|Formula Book|Livre de formules|officielle|
|[gKRDi4EHuz4qIfX0.htm](age-of-ashes-bestiary-items/gKRDi4EHuz4qIfX0.htm)|+1 Status to All Saves vs. Positive|bonus de statut de +1 aux JdS contre les effets positifs|officielle|
|[GLAGuOAJAsQFW841.htm](age-of-ashes-bestiary-items/GLAGuOAJAsQFW841.htm)|Major Alchemist's Fire|Feu grégeois majeur|officielle|
|[GLOrUOMtmv1uHTph.htm](age-of-ashes-bestiary-items/GLOrUOMtmv1uHTph.htm)|Change Shape|Changement de forme|officielle|
|[gncjMzeBqmNqD4vz.htm](age-of-ashes-bestiary-items/gncjMzeBqmNqD4vz.htm)|Mauler|Écharpeur|officielle|
|[go1LGZS91Af3mtP5.htm](age-of-ashes-bestiary-items/go1LGZS91Af3mtP5.htm)|Reactive|Réactif|officielle|
|[gOhhyt5jY5m4BCII.htm](age-of-ashes-bestiary-items/gOhhyt5jY5m4BCII.htm)|Motion Sense 60 feet|Perception du mouvement 18 m|officielle|
|[GpY52PWVwnKAAp0M.htm](age-of-ashes-bestiary-items/GpY52PWVwnKAAp0M.htm)|Quick Bomber|Artificier rapide|officielle|
|[gqBKUBOWGOGtavDi.htm](age-of-ashes-bestiary-items/gqBKUBOWGOGtavDi.htm)|Darkvision|Vision dans le noir|officielle|
|[GQHHw4yF36Vczq0u.htm](age-of-ashes-bestiary-items/GQHHw4yF36Vczq0u.htm)|Negative Healing|Guérison négative|officielle|
|[gQJ5aamwehy4Pgwd.htm](age-of-ashes-bestiary-items/gQJ5aamwehy4Pgwd.htm)|Master Smith|Maîtresse Forgeronne|officielle|
|[Gr4iC9v8WDwl1THc.htm](age-of-ashes-bestiary-items/Gr4iC9v8WDwl1THc.htm)|Dagger|+1|Dague +1|officielle|
|[grsWv0LAFcO3iohL.htm](age-of-ashes-bestiary-items/grsWv0LAFcO3iohL.htm)|Claw|Griffe|officielle|
|[gTfs4nOLCXGSkmgU.htm](age-of-ashes-bestiary-items/gTfs4nOLCXGSkmgU.htm)|Silver Dagger|Dague en argent|officielle|
|[guGMBVBsqTK7okFR.htm](age-of-ashes-bestiary-items/guGMBVBsqTK7okFR.htm)|Slam Doors|Claquement de portes|officielle|
|[gW2dzfKwJdsR6lTt.htm](age-of-ashes-bestiary-items/gW2dzfKwJdsR6lTt.htm)|Channel Smite|Châtiment canalisé|officielle|
|[gw4Ka0K9vOSo5rbh.htm](age-of-ashes-bestiary-items/gw4Ka0K9vOSo5rbh.htm)|Soul Shriek|Hurlement de l'âme|officielle|
|[GWB9Y5J1UIUrQmlk.htm](age-of-ashes-bestiary-items/GWB9Y5J1UIUrQmlk.htm)|Infused Reagents|Réactifs imprégnés|officielle|
|[GXl3eIHoy2IwAZsz.htm](age-of-ashes-bestiary-items/GXl3eIHoy2IwAZsz.htm)|Morningstar|+1,striking|Morgenstern de frappe +1|libre|
|[GynHyVx5cEZ0vb9p.htm](age-of-ashes-bestiary-items/GynHyVx5cEZ0vb9p.htm)|Chase Down|Poursuite|officielle|
|[gzeeloXOYQJ3ZCvu.htm](age-of-ashes-bestiary-items/gzeeloXOYQJ3ZCvu.htm)|Mental Erosion|Érosion mentale|officielle|
|[GZrlbqO60jo1adXW.htm](age-of-ashes-bestiary-items/GZrlbqO60jo1adXW.htm)|Darkvision|Vision dans le noir|officielle|
|[H1vAXye0WIg0VDxS.htm](age-of-ashes-bestiary-items/H1vAXye0WIg0VDxS.htm)|Pick|Pic de guerre|officielle|
|[H2vI4lzuGs1S1ljJ.htm](age-of-ashes-bestiary-items/H2vI4lzuGs1S1ljJ.htm)|True Debilitating Bomb|Bombe incapacitante ultime|officielle|
|[H2zLCRS1UX8XpweP.htm](age-of-ashes-bestiary-items/H2zLCRS1UX8XpweP.htm)|At-Will Spells|Sorts à volonté|officielle|
|[H4swqBH4RI4TQYpt.htm](age-of-ashes-bestiary-items/H4swqBH4RI4TQYpt.htm)|Cold Iron Dagger|Dague en fer froid|officielle|
|[h4y4kf4q85cEAF9x.htm](age-of-ashes-bestiary-items/h4y4kf4q85cEAF9x.htm)|At-Will Spells|Sorts à volonté|officielle|
|[H6cbKbRUjKF71Gko.htm](age-of-ashes-bestiary-items/H6cbKbRUjKF71Gko.htm)|Dagger|Dague|officielle|
|[h7AGKtRekFbuZp1Z.htm](age-of-ashes-bestiary-items/h7AGKtRekFbuZp1Z.htm)|Heavy Book|Livre lourd|officielle|
|[h7oremneSeWvOijy.htm](age-of-ashes-bestiary-items/h7oremneSeWvOijy.htm)|Change Shape|Changement de forme|officielle|
|[h7xMfnmPBbeB5lZh.htm](age-of-ashes-bestiary-items/h7xMfnmPBbeB5lZh.htm)|Soul Chain|+1,striking|Chaîne des âmes|officielle|
|[hCNYJ9tfRIOCl989.htm](age-of-ashes-bestiary-items/hCNYJ9tfRIOCl989.htm)|Greater Acid Flask|Fiole d'acide supérieure imprégnée|officielle|
|[HDdDDHhvuKuOWIXX.htm](age-of-ashes-bestiary-items/HDdDDHhvuKuOWIXX.htm)|Mind Blank (Constant)|Esprit impénétrable (constant)|officielle|
|[hdN9cUP88jTa2Ykb.htm](age-of-ashes-bestiary-items/hdN9cUP88jTa2Ykb.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|officielle|
|[hdRK1K0vud2hRJEt.htm](age-of-ashes-bestiary-items/hdRK1K0vud2hRJEt.htm)|Promise Guard Stance|Posture des gardes de Promesse|officielle|
|[HEnPvo9LJho9qb75.htm](age-of-ashes-bestiary-items/HEnPvo9LJho9qb75.htm)|Manipulate Energy|Manipuler l'énergie|officielle|
|[HezD1ffNIEmWNt12.htm](age-of-ashes-bestiary-items/HezD1ffNIEmWNt12.htm)|Tail|Queue|officielle|
|[Hfn2kPN8v4JuYF1l.htm](age-of-ashes-bestiary-items/Hfn2kPN8v4JuYF1l.htm)|Quick Bomber|Artificier rapide|officielle|
|[hgONc5BRFBZc4j73.htm](age-of-ashes-bestiary-items/hgONc5BRFBZc4j73.htm)|Darkvision|Vision dans le noir|officielle|
|[HH6SL5ftuMn3NSHu.htm](age-of-ashes-bestiary-items/HH6SL5ftuMn3NSHu.htm)|Echoing Cry|Cri résonnant|officielle|
|[Hh8TSJGN8yL5SyKZ.htm](age-of-ashes-bestiary-items/Hh8TSJGN8yL5SyKZ.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|officielle|
|[hhR7r91njgZuHH4x.htm](age-of-ashes-bestiary-items/hhR7r91njgZuHH4x.htm)|Brutal Cleave|Coup tranchant brutal|officielle|
|[hhRccIBA0AFYl4Su.htm](age-of-ashes-bestiary-items/hhRccIBA0AFYl4Su.htm)|Key Ring (Opens Locks in Summershade Quarry)|Trousseau de clés (ouvre les cadenas dans la carrière d'Ombrété)|officielle|
|[hI3zcHdUBaFLeoZX.htm](age-of-ashes-bestiary-items/hI3zcHdUBaFLeoZX.htm)|Jaws|Mâchoires|officielle|
|[hJc7HGNvwjdmOdfq.htm](age-of-ashes-bestiary-items/hJc7HGNvwjdmOdfq.htm)|Edifice|Édifice|officielle|
|[HJwVm2TXnBSmdy28.htm](age-of-ashes-bestiary-items/HJwVm2TXnBSmdy28.htm)|Fire Shield (Constant)|Bouclier de feu (constant)|officielle|
|[HlgR9oLdO3ZGqCry.htm](age-of-ashes-bestiary-items/HlgR9oLdO3ZGqCry.htm)|Reactive Breath|Souffle réactif|officielle|
|[hna2NNtT5gHtT3b2.htm](age-of-ashes-bestiary-items/hna2NNtT5gHtT3b2.htm)|True Seeing (Constant)|Vision lucide (constant)|officielle|
|[HoXBI3O9X2kkeeC1.htm](age-of-ashes-bestiary-items/HoXBI3O9X2kkeeC1.htm)|Darkvision|Vision dans le noir|officielle|
|[hPc0H3LlS5oAeiST.htm](age-of-ashes-bestiary-items/hPc0H3LlS5oAeiST.htm)|Frightful Presence|Présence terrifiante|officielle|
|[hPj1RAdI4c2AUrOO.htm](age-of-ashes-bestiary-items/hPj1RAdI4c2AUrOO.htm)|Bestial Mutagen (Major) [Infused]|Mutagène bestial majeur [imprégné]|officielle|
|[HPLwJZH8RwLW9c13.htm](age-of-ashes-bestiary-items/HPLwJZH8RwLW9c13.htm)|Black Eye Beam|Rayon oculaire noir|officielle|
|[hRrW3wtTTKj4Kv6s.htm](age-of-ashes-bestiary-items/hRrW3wtTTKj4Kv6s.htm)|Longbow|Arc long|officielle|
|[hrt10usZTV3s3uKQ.htm](age-of-ashes-bestiary-items/hrt10usZTV3s3uKQ.htm)|Haunted Form|Forme d'apparition|officielle|
|[HSs8knjTJuMzsAQr.htm](age-of-ashes-bestiary-items/HSs8knjTJuMzsAQr.htm)|Recognize Ally|Reconnaître les alliés|officielle|
|[hUz1Bt93sajJTnQg.htm](age-of-ashes-bestiary-items/hUz1Bt93sajJTnQg.htm)|Bottled Lightning (Major) [Infused]|Foudre en bouteille majeur [imprégné]|officielle|
|[hVAcTBzqhXEsus66.htm](age-of-ashes-bestiary-items/hVAcTBzqhXEsus66.htm)|Wendigo Torment|Tourment du wendigo|officielle|
|[HVINdiV8c3uYz4or.htm](age-of-ashes-bestiary-items/HVINdiV8c3uYz4or.htm)|Darkvision|Vision dans le noir|officielle|
|[hWFsgyTptyHOBJtD.htm](age-of-ashes-bestiary-items/hWFsgyTptyHOBJtD.htm)|Arcane Prepared Spells|Sorts arcaniques préparés|libre|
|[HXiycpYZMsyFqs9l.htm](age-of-ashes-bestiary-items/HXiycpYZMsyFqs9l.htm)|Shortbow|Arc court|officielle|
|[hZNyCnxdujLVMKqb.htm](age-of-ashes-bestiary-items/hZNyCnxdujLVMKqb.htm)|Identify an Opening|Identifier une ouverture|officielle|
|[HzVWIyzzAWGlhGDC.htm](age-of-ashes-bestiary-items/HzVWIyzzAWGlhGDC.htm)|Claw|Griffe|officielle|
|[i2cs4dR5xnohX9E6.htm](age-of-ashes-bestiary-items/i2cs4dR5xnohX9E6.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[i7B8yhwnUu48ZFdz.htm](age-of-ashes-bestiary-items/i7B8yhwnUu48ZFdz.htm)|Dragon Pillar Glance|Regard de la colonne draconique|officielle|
|[i7bZ6f6zTCdQwhiN.htm](age-of-ashes-bestiary-items/i7bZ6f6zTCdQwhiN.htm)|Low-Light Vision|Vision nocturne|officielle|
|[i8XuwjgGCvTaFORz.htm](age-of-ashes-bestiary-items/i8XuwjgGCvTaFORz.htm)|Frost Vial (Major) [Infused]|Fiole de givre majeure (imprégnée)|officielle|
|[IBkN8sEe9mFN0Uf4.htm](age-of-ashes-bestiary-items/IBkN8sEe9mFN0Uf4.htm)|Pseudopod|Pseudopode|officielle|
|[ICaSdqQPFGE4xmmc.htm](age-of-ashes-bestiary-items/ICaSdqQPFGE4xmmc.htm)|Dagger|Dague|officielle|
|[IcCigf17UR8tbG7v.htm](age-of-ashes-bestiary-items/IcCigf17UR8tbG7v.htm)|Reflect|Renvoyer|officielle|
|[ICOl05YuWjz7C6GN.htm](age-of-ashes-bestiary-items/ICOl05YuWjz7C6GN.htm)|Paragon's Guard Stance|Posture de protection de parangon|officielle|
|[IcprrivigaRsacAR.htm](age-of-ashes-bestiary-items/IcprrivigaRsacAR.htm)|Swallow Whole|Gober|officielle|
|[IDbHyzOZv9b2OyKQ.htm](age-of-ashes-bestiary-items/IDbHyzOZv9b2OyKQ.htm)|Impede|Faire obstacle|officielle|
|[IDc6afCxHUwfqrkX.htm](age-of-ashes-bestiary-items/IDc6afCxHUwfqrkX.htm)|At-Will Spells|Sorts à volonté|officielle|
|[Idy0MtsKPxYUxKqC.htm](age-of-ashes-bestiary-items/Idy0MtsKPxYUxKqC.htm)|Ghostly Hand|Main spectrale|officielle|
|[ieB3sq8pLvjePur7.htm](age-of-ashes-bestiary-items/ieB3sq8pLvjePur7.htm)|Ghast Fever|Fièvre des blêmes|officielle|
|[iF9jSPAE3daPYnue.htm](age-of-ashes-bestiary-items/iF9jSPAE3daPYnue.htm)|Wand of Crushing Despair (Level 5)|Baguette de Désespoir foudroyant (Niveau 5)|officielle|
|[IFl5ilnFtCXtwx8I.htm](age-of-ashes-bestiary-items/IFl5ilnFtCXtwx8I.htm)|Pack Attack|Attaque en meute|officielle|
|[iFQmsWh8PXGOmX3E.htm](age-of-ashes-bestiary-items/iFQmsWh8PXGOmX3E.htm)|Spell Turning (At Will)|Renvoi de sort (À volonté)|officielle|
|[Ig3nqmVhvxcS8az0.htm](age-of-ashes-bestiary-items/Ig3nqmVhvxcS8az0.htm)|Shoddy Blunderbuss|Tromblon de mauvaise qualité|officielle|
|[IHKQdP1f65ZJ8Oqu.htm](age-of-ashes-bestiary-items/IHKQdP1f65ZJ8Oqu.htm)|Poison Weapon|Arme empoisonnée|officielle|
|[IHmGGY9udr3P8p7m.htm](age-of-ashes-bestiary-items/IHmGGY9udr3P8p7m.htm)|Light Blindness|Aveuglé par la lumière|officielle|
|[IidItzJQhC8vx430.htm](age-of-ashes-bestiary-items/IidItzJQhC8vx430.htm)|Torture Lore|Connaissance de la torture|officielle|
|[IJhw9ETWROv5Y9rt.htm](age-of-ashes-bestiary-items/IJhw9ETWROv5Y9rt.htm)|Spikes|Pics|officielle|
|[ikFTzuigWjVB9prY.htm](age-of-ashes-bestiary-items/ikFTzuigWjVB9prY.htm)|Hatchet|+1,striking|Hachette de frappe +1|libre|
|[iLD58rCFSuSX1HF6.htm](age-of-ashes-bestiary-items/iLD58rCFSuSX1HF6.htm)|Thundering Kukri|Kukri de tonnerre|officielle|
|[ILyJiCshei4yjRby.htm](age-of-ashes-bestiary-items/ILyJiCshei4yjRby.htm)|Aiudara Lore|Connaissance des aiudara|officielle|
|[iqsG9i0M9rfRDnFG.htm](age-of-ashes-bestiary-items/iqsG9i0M9rfRDnFG.htm)|Hermea Lore|Connaissance d'Herméa|officielle|
|[IrffLfsr4F6AQ70K.htm](age-of-ashes-bestiary-items/IrffLfsr4F6AQ70K.htm)|Subduing Strikes|Frappe de soumission|officielle|
|[iTmWT27s6I0W1gVm.htm](age-of-ashes-bestiary-items/iTmWT27s6I0W1gVm.htm)|Manifest Lesser Dragonstorm|Déclencher une tempête draconique mineure|officielle|
|[iVkBxitUPibUUdxV.htm](age-of-ashes-bestiary-items/iVkBxitUPibUUdxV.htm)|Shield Breaker|Brise-bouclier|officielle|
|[iVzCadSFlB0zTY05.htm](age-of-ashes-bestiary-items/iVzCadSFlB0zTY05.htm)|Shortsword|Épée courte|officielle|
|[iVZygV9OkEH28o2Q.htm](age-of-ashes-bestiary-items/iVZygV9OkEH28o2Q.htm)|Black Eye Beam|Rayon oculaire noir|officielle|
|[iwjhcDyX2SKcHon0.htm](age-of-ashes-bestiary-items/iwjhcDyX2SKcHon0.htm)|Blowgun|+1|Sarbacane +1|libre|
|[IwqiOEcQaE9YK1Sa.htm](age-of-ashes-bestiary-items/IwqiOEcQaE9YK1Sa.htm)|Spell Strike|Frappe magique|officielle|
|[IZI87xvq3Ze64hGf.htm](age-of-ashes-bestiary-items/IZI87xvq3Ze64hGf.htm)|Sailing Lore|Connaissance de la navigation maritime|officielle|
|[j0tisNS7G4pqIgVz.htm](age-of-ashes-bestiary-items/j0tisNS7G4pqIgVz.htm)|Shield Block|Blocage au bouclier|officielle|
|[j3NCaHUgoOUiHpye.htm](age-of-ashes-bestiary-items/j3NCaHUgoOUiHpye.htm)|Ghast Fever|Fièvre des blêmes|officielle|
|[j4MfbH7vW63xSuRI.htm](age-of-ashes-bestiary-items/j4MfbH7vW63xSuRI.htm)|Shield Boss|Ombon|officielle|
|[j5LDyAGkf9FeoBAt.htm](age-of-ashes-bestiary-items/j5LDyAGkf9FeoBAt.htm)|Rapier|Rapière|officielle|
|[J7B6WzdiD0YTP0qQ.htm](age-of-ashes-bestiary-items/J7B6WzdiD0YTP0qQ.htm)|Light Blindness|Aveuglé par la lumière|officielle|
|[j7hDiUXLpry6mkqN.htm](age-of-ashes-bestiary-items/j7hDiUXLpry6mkqN.htm)|Darkvision|Vision dans le noir|officielle|
|[j95Y0cxi70Mb6UMQ.htm](age-of-ashes-bestiary-items/j95Y0cxi70Mb6UMQ.htm)|Rugged Travel|Tout terrain|libre|
|[j9OR4GAOwpFolkP9.htm](age-of-ashes-bestiary-items/j9OR4GAOwpFolkP9.htm)|Plane Shift (To and From the Vazgorlu's Demiplane Only)|Changement de plan (de et vers le demi-plan du Vazgorlu uniquement)|officielle|
|[Janx9PZwNEn44igV.htm](age-of-ashes-bestiary-items/Janx9PZwNEn44igV.htm)|Fleshroaster|Brûlepeau|officielle|
|[jBkyceYP8fzH2x9P.htm](age-of-ashes-bestiary-items/jBkyceYP8fzH2x9P.htm)|Rock|Rocher|officielle|
|[JD2ON040dRP85QHM.htm](age-of-ashes-bestiary-items/JD2ON040dRP85QHM.htm)|Levitate (At Will)|Lévitation (À volonté)|officielle|
|[jEFmMCQkUOlH3g51.htm](age-of-ashes-bestiary-items/jEFmMCQkUOlH3g51.htm)|Ruby and Sapphire ring|Anneau de rubis et sapphire|officielle|
|[jfBUSjcjhlewvG1J.htm](age-of-ashes-bestiary-items/jfBUSjcjhlewvG1J.htm)|Longsword|Épée longue|officielle|
|[jFY95i89pS7nXrQl.htm](age-of-ashes-bestiary-items/jFY95i89pS7nXrQl.htm)|Darkvision|Vision dans le noir|officielle|
|[jHCCY0ZIpf7axn62.htm](age-of-ashes-bestiary-items/jHCCY0ZIpf7axn62.htm)|Channel Dragonstorm|Canaliser la tempête draconique|officielle|
|[jI9GCbJaB0b3F4Kw.htm](age-of-ashes-bestiary-items/jI9GCbJaB0b3F4Kw.htm)|Indigo Eye Beam|Rayon oculaire indigo|officielle|
|[jL6Ovx8CLeSJ0K2S.htm](age-of-ashes-bestiary-items/jL6Ovx8CLeSJ0K2S.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[jNcUglLORusRcA4f.htm](age-of-ashes-bestiary-items/jNcUglLORusRcA4f.htm)|Red Eye Beam|Rayon oculaire rouge|officielle|
|[jPZb7g1lsQyPQQw3.htm](age-of-ashes-bestiary-items/jPZb7g1lsQyPQQw3.htm)|Crossbow|+1,striking|Arbalète de frappe +1|libre|
|[jQUqBzYMtuQRpcmS.htm](age-of-ashes-bestiary-items/jQUqBzYMtuQRpcmS.htm)|Darkvision|Vision dans le noir|officielle|
|[JRd0vnYlb4G88YJ6.htm](age-of-ashes-bestiary-items/JRd0vnYlb4G88YJ6.htm)|Crossbow|Arbalète|officielle|
|[JrQMzt6NgfnCAaOv.htm](age-of-ashes-bestiary-items/JrQMzt6NgfnCAaOv.htm)|Composite Shortbow|+2,striking|Arc cout composite de Frappe +2|libre|
|[jVPlrYrdfUoKsCFZ.htm](age-of-ashes-bestiary-items/jVPlrYrdfUoKsCFZ.htm)|Tremorsense|Perception des vibrations|officielle|
|[jwRUzgElIiQUtmhR.htm](age-of-ashes-bestiary-items/jwRUzgElIiQUtmhR.htm)|Dimensional Tether|Laisse dimensionnelle|officielle|
|[jx7FmzYu8clgoANu.htm](age-of-ashes-bestiary-items/jx7FmzYu8clgoANu.htm)|Dimension Door (At Will)|Porte dimensionnelle (À volonté)|officielle|
|[JZbnTWFBrM3l78qS.htm](age-of-ashes-bestiary-items/JZbnTWFBrM3l78qS.htm)|Volcanic Eruption|Éruption volcanique|officielle|
|[jZFyZg8r1I1EWoBY.htm](age-of-ashes-bestiary-items/jZFyZg8r1I1EWoBY.htm)|Timely Distraction|Distraction opportune|officielle|
|[jZsVGuNQlvV5TkQ9.htm](age-of-ashes-bestiary-items/jZsVGuNQlvV5TkQ9.htm)|Perception and Vision|Perception et Vision|officielle|
|[jZUi2l7sM6u3fmPW.htm](age-of-ashes-bestiary-items/jZUi2l7sM6u3fmPW.htm)|Dagger|+2,striking,returning|Dague boomerang de frappe +2|libre|
|[K28NAtboAXcpvklg.htm](age-of-ashes-bestiary-items/K28NAtboAXcpvklg.htm)|Dahak's Glance|Regard de Dahak|officielle|
|[K5dIa5gCC7aePzQf.htm](age-of-ashes-bestiary-items/K5dIa5gCC7aePzQf.htm)|Soul Siphon|Siphon d'âme|officielle|
|[k7a5PsosLu8FXsTk.htm](age-of-ashes-bestiary-items/k7a5PsosLu8FXsTk.htm)|Speed Scimitar|Cimeterre rapide|officielle|
|[k8bgECKZjROUF5gA.htm](age-of-ashes-bestiary-items/k8bgECKZjROUF5gA.htm)|Staff of Fire|Bâton de feu|officielle|
|[kCTibCnGM3LjnVUm.htm](age-of-ashes-bestiary-items/kCTibCnGM3LjnVUm.htm)|Scimitar|+3,majorStriking,speed|Cimeterre de rapidité de frappe majeure +3|libre|
|[kD6AcE8oSOr3UKnh.htm](age-of-ashes-bestiary-items/kD6AcE8oSOr3UKnh.htm)|Scroll of Discern Location (Level 8)|Parchemin de Localisation suprême (Niveau 8)|officielle|
|[kedOBfOiNpFOHZa0.htm](age-of-ashes-bestiary-items/kedOBfOiNpFOHZa0.htm)|Jaws|Mâchoires|officielle|
|[keOsYquWeSxZCihV.htm](age-of-ashes-bestiary-items/keOsYquWeSxZCihV.htm)|Broken Greatsword|Épée à deux mains brisée|officielle|
|[kg205YMOcrEz6XQw.htm](age-of-ashes-bestiary-items/kg205YMOcrEz6XQw.htm)|Ranged Legerdemain|Prestidigitation à distance|officielle|
|[kGULFoB74ac0dOIO.htm](age-of-ashes-bestiary-items/kGULFoB74ac0dOIO.htm)|Scroll of Sleep (Level 1)|Parchemin de Sommeil (Niveau 1)|officielle|
|[KhFyp3oQ1nmWpdA9.htm](age-of-ashes-bestiary-items/KhFyp3oQ1nmWpdA9.htm)|Darkvision|Vision dans le noir|officielle|
|[KHiVxpvVx4edKn9M.htm](age-of-ashes-bestiary-items/KHiVxpvVx4edKn9M.htm)|Horns|Corne|officielle|
|[KiltiTEGGXxRaLvr.htm](age-of-ashes-bestiary-items/KiltiTEGGXxRaLvr.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[kJlV6Vzu6KZumb6u.htm](age-of-ashes-bestiary-items/kJlV6Vzu6KZumb6u.htm)|Volcanic Heat|Chaleur volcanique|officielle|
|[KJxjnogCSiWsIs3n.htm](age-of-ashes-bestiary-items/KJxjnogCSiWsIs3n.htm)|Magma Swim|Nage dans le magma.|officielle|
|[kJz2LXDyAG4vASIf.htm](age-of-ashes-bestiary-items/kJz2LXDyAG4vASIf.htm)|Draconic Frenzy|Frénésie draconique|officielle|
|[KKaF35HWVDgg10gX.htm](age-of-ashes-bestiary-items/KKaF35HWVDgg10gX.htm)|Scimitar|Cimeterre|officielle|
|[kn7uZpzyYXn8aUO7.htm](age-of-ashes-bestiary-items/kn7uZpzyYXn8aUO7.htm)|Key to Manacles|Clé des menottes|officielle|
|[KOEGii7EkPZsq9Iz.htm](age-of-ashes-bestiary-items/KOEGii7EkPZsq9Iz.htm)|Glare of Rage|Regard furieux|officielle|
|[KoIGP6ravPoUypu2.htm](age-of-ashes-bestiary-items/KoIGP6ravPoUypu2.htm)|Constant Spells|Sorts constants|officielle|
|[koqHCbsGHHME7p6J.htm](age-of-ashes-bestiary-items/koqHCbsGHHME7p6J.htm)|Claw|Griffe|officielle|
|[Kp4oqpyn0RuzJfDQ.htm](age-of-ashes-bestiary-items/Kp4oqpyn0RuzJfDQ.htm)|Staff|Bâton|officielle|
|[kPGNXeFYyzZHqKiv.htm](age-of-ashes-bestiary-items/kPGNXeFYyzZHqKiv.htm)|Return Fire|Tir de riposte|officielle|
|[kpp6uAfQZcHA99Ot.htm](age-of-ashes-bestiary-items/kpp6uAfQZcHA99Ot.htm)|Acid Flask (Greater) [Infused]|Fiole d'acide supérieure [imprégné]|officielle|
|[kqgeNf8SyOkbJ0qi.htm](age-of-ashes-bestiary-items/kqgeNf8SyOkbJ0qi.htm)|Greatsword|Épée à deux mains|officielle|
|[KqSNLofqd5sNL9bp.htm](age-of-ashes-bestiary-items/KqSNLofqd5sNL9bp.htm)|Arcane Prepared Spells|Sorts arcaniques préparés|libre|
|[KqV4dBWVzfi3DXyl.htm](age-of-ashes-bestiary-items/KqV4dBWVzfi3DXyl.htm)|Efficient Capture|Capture efficace|officielle|
|[KrTgWTUW9uB20sKT.htm](age-of-ashes-bestiary-items/KrTgWTUW9uB20sKT.htm)|Frightful Presence|Présence terrifiante|officielle|
|[kSkZEEGE3SBLK5M1.htm](age-of-ashes-bestiary-items/kSkZEEGE3SBLK5M1.htm)|Detect Alignment (Constant) (Good Only)|Détection de l'alignement (constant, bien uniquement)|officielle|
|[kT5280SVobphMNJn.htm](age-of-ashes-bestiary-items/kT5280SVobphMNJn.htm)|Sneak Attack|Attaque sournoise|officielle|
|[kuz9wy1VxfKf0baT.htm](age-of-ashes-bestiary-items/kuz9wy1VxfKf0baT.htm)|+1 Resilient Scale Mail|Armure d'écailles de résilience +1|officielle|
|[Kvh0b9Gxe3FzqXsg.htm](age-of-ashes-bestiary-items/Kvh0b9Gxe3FzqXsg.htm)|At-Will Spells|Sorts à volonté|officielle|
|[kVU7cb7Ts2sMfPyu.htm](age-of-ashes-bestiary-items/kVU7cb7Ts2sMfPyu.htm)|Whip|Fouet|officielle|
|[kW1j0YXBTuxfEsOM.htm](age-of-ashes-bestiary-items/kW1j0YXBTuxfEsOM.htm)|+1 Resilient Leather Armor|Armure en cuir de résilience +1|officielle|
|[kwP6P4BpQ8TfjZTO.htm](age-of-ashes-bestiary-items/kwP6P4BpQ8TfjZTO.htm)|Heavy Book|Livre lourd|officielle|
|[kwZwMAb5UWqN9UlE.htm](age-of-ashes-bestiary-items/kwZwMAb5UWqN9UlE.htm)|Synesthesia (At Will)|Synesthésie (À volonté)|officielle|
|[kx9XRnPu3GxZYqST.htm](age-of-ashes-bestiary-items/kx9XRnPu3GxZYqST.htm)|Darkvision|Vision dans le noir|officielle|
|[kyJU0dp0AuLccYHV.htm](age-of-ashes-bestiary-items/kyJU0dp0AuLccYHV.htm)|Breath Weapon|Souffle|officielle|
|[KYpQ8TgZcmUrKnjC.htm](age-of-ashes-bestiary-items/KYpQ8TgZcmUrKnjC.htm)|Independent Limbs|Membres indépendants|libre|
|[Kz3r31N7mvhlPQc8.htm](age-of-ashes-bestiary-items/Kz3r31N7mvhlPQc8.htm)|Occult Innate Spellss|Sorts occultes innés|libre|
|[kZ6DWvbDyy1GyJ6D.htm](age-of-ashes-bestiary-items/kZ6DWvbDyy1GyJ6D.htm)|Personal Quake|Tremblements corporels|officielle|
|[Kz6JoF3brQdpyel6.htm](age-of-ashes-bestiary-items/Kz6JoF3brQdpyel6.htm)|Wand of Vampiric Exsanguination (Level 6)|Baguette de Saignée vampirique (Niveau 6)|officielle|
|[kZcHhT2dlbVHF7AS.htm](age-of-ashes-bestiary-items/kZcHhT2dlbVHF7AS.htm)|Kobold Explosives|Explosifs kobolds|officielle|
|[L0ZHkQFYtz2JK3hH.htm](age-of-ashes-bestiary-items/L0ZHkQFYtz2JK3hH.htm)|Composite Longbow|Arc long composite|officielle|
|[L10IjFT5OxF7Vdtw.htm](age-of-ashes-bestiary-items/L10IjFT5OxF7Vdtw.htm)|Steady Spellcasting|Incantation fiable|officielle|
|[L1rIkFNagYO7XkgM.htm](age-of-ashes-bestiary-items/L1rIkFNagYO7XkgM.htm)|Wing Deflection|Déviation de l'aile|officielle|
|[l1U1HsRzfUA7agTu.htm](age-of-ashes-bestiary-items/l1U1HsRzfUA7agTu.htm)|Walk in Shadow|Déplacement dans les ombres|officielle|
|[l4sEHZaqyz1kUQ95.htm](age-of-ashes-bestiary-items/l4sEHZaqyz1kUQ95.htm)|Dagger|+3,majorStriking|Dague de frappe majeure +3|libre|
|[l69fOTyMIBdJNWTY.htm](age-of-ashes-bestiary-items/l69fOTyMIBdJNWTY.htm)|Guardian Sense|Sens de gardien|officielle|
|[L6oDlNintmXHhjAv.htm](age-of-ashes-bestiary-items/L6oDlNintmXHhjAv.htm)|Precision|Précision|officielle|
|[l7LvpPXTQL4jhPb5.htm](age-of-ashes-bestiary-items/l7LvpPXTQL4jhPb5.htm)|Low-Light Vision|Vision nocturne|officielle|
|[LB86IJopeELMnAze.htm](age-of-ashes-bestiary-items/LB86IJopeELMnAze.htm)|Frightful Moan|Lamentation d'épouvante|officielle|
|[lBHMoim0EVa3GvjV.htm](age-of-ashes-bestiary-items/lBHMoim0EVa3GvjV.htm)|Detect Magic (Constant)|Détection de la magie (constant)|officielle|
|[lBp2RqOAK3CORaeJ.htm](age-of-ashes-bestiary-items/lBp2RqOAK3CORaeJ.htm)|Spine|Épine|officielle|
|[lbSqT3JsKYmywHiq.htm](age-of-ashes-bestiary-items/lbSqT3JsKYmywHiq.htm)|Jaws|Mâchoires|officielle|
|[LbVTFpHTRiZGGVE9.htm](age-of-ashes-bestiary-items/LbVTFpHTRiZGGVE9.htm)|Longsword|Épée longue|officielle|
|[lcdFqkdanjf6Jdom.htm](age-of-ashes-bestiary-items/lcdFqkdanjf6Jdom.htm)|Caustic Nightmare Vapor|Vapeurs de cauchemar corrosives|officielle|
|[ld8xSt4F2q5m6vRe.htm](age-of-ashes-bestiary-items/ld8xSt4F2q5m6vRe.htm)|Paralyzing Force|Force paralysante|officielle|
|[LDF2M6FvzjfuWrGp.htm](age-of-ashes-bestiary-items/LDF2M6FvzjfuWrGp.htm)|Mind Reading (At Will)|Lecture des pensées (À volonté)|officielle|
|[LePEQxJOrm3uEIPP.htm](age-of-ashes-bestiary-items/LePEQxJOrm3uEIPP.htm)|Arcane Innate Spells|Sorts arcaniques innés|libre|
|[lff494GXiIjHCMDA.htm](age-of-ashes-bestiary-items/lff494GXiIjHCMDA.htm)|Weeping Midnight (applied to one arrow)|Poison lamentations de minuit (appliqué à une flèche)|officielle|
|[lfmWRoj9CXxKLkCs.htm](age-of-ashes-bestiary-items/lfmWRoj9CXxKLkCs.htm)|Red Eye Beam|Rayon oculaire rouge|officielle|
|[lJ6fQwHU2UpZ6h6F.htm](age-of-ashes-bestiary-items/lJ6fQwHU2UpZ6h6F.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|officielle|
|[lk1R5BJiKisYShQK.htm](age-of-ashes-bestiary-items/lk1R5BJiKisYShQK.htm)|See Invisibility (Constant)|Détection de l'invisibilité (constant)|officielle|
|[lKe6LQN6pD3r55Wq.htm](age-of-ashes-bestiary-items/lKe6LQN6pD3r55Wq.htm)|Rapier|+1,striking|Rapière de frappe +1|officielle|
|[LLzxyBASOUnyUmZk.htm](age-of-ashes-bestiary-items/LLzxyBASOUnyUmZk.htm)|Green Empathy|Empathie végétale|officielle|
|[LM8A2eiEIyuB9hk2.htm](age-of-ashes-bestiary-items/LM8A2eiEIyuB9hk2.htm)|Shortbow|Arc court|officielle|
|[LnwQpnNyyd6h5Xx0.htm](age-of-ashes-bestiary-items/LnwQpnNyyd6h5Xx0.htm)|Face of the Fatal Divine|Visage fatal du Divin|officielle|
|[lpDLQW5YfXIYXiJS.htm](age-of-ashes-bestiary-items/lpDLQW5YfXIYXiJS.htm)|Claw|Griffe|officielle|
|[lpORbE7ZJjtwviYL.htm](age-of-ashes-bestiary-items/lpORbE7ZJjtwviYL.htm)|Darkvision|Vision dans le noir|officielle|
|[lPxb0HqwCjMCigsv.htm](age-of-ashes-bestiary-items/lPxb0HqwCjMCigsv.htm)|Returning Light Hammer|Marteau de guerre léger boomerang|officielle|
|[LQ20lTeaoccMicH8.htm](age-of-ashes-bestiary-items/LQ20lTeaoccMicH8.htm)|Psychic Screech|Hurlement psychique|officielle|
|[lRGKk5oP862ZMOI3.htm](age-of-ashes-bestiary-items/lRGKk5oP862ZMOI3.htm)|Change Shape|Changement de forme|officielle|
|[lSLgvPqHIBXWMrhx.htm](age-of-ashes-bestiary-items/lSLgvPqHIBXWMrhx.htm)|Plane of Fire Lore|Connaissance du plan du feu|officielle|
|[LsLTSWuEmgszvIJo.htm](age-of-ashes-bestiary-items/LsLTSWuEmgszvIJo.htm)|Shortbow|Arc court|officielle|
|[lSxI33XT0yIaAARS.htm](age-of-ashes-bestiary-items/lSxI33XT0yIaAARS.htm)|Darkvision|Vision dans le noir|officielle|
|[LT73ykCcsBulHrcy.htm](age-of-ashes-bestiary-items/LT73ykCcsBulHrcy.htm)|Jaws|Mâchoires|officielle|
|[LTrdnkawIh2aRpXo.htm](age-of-ashes-bestiary-items/LTrdnkawIh2aRpXo.htm)|Katapesh Lore|Connaissance de Katapesh|officielle|
|[lup461lEK9zazSYF.htm](age-of-ashes-bestiary-items/lup461lEK9zazSYF.htm)|Arcane Innate Spells|Sorts arcaniques innés|libre|
|[lw6P5HJ72vY1Ef6K.htm](age-of-ashes-bestiary-items/lw6P5HJ72vY1Ef6K.htm)|Composite Shortbow|Arc court composite|officielle|
|[lWDO2icn4khYwlgs.htm](age-of-ashes-bestiary-items/lWDO2icn4khYwlgs.htm)|Jaws|Mâchoires|officielle|
|[LwLNUmtRRcLH4yWo.htm](age-of-ashes-bestiary-items/LwLNUmtRRcLH4yWo.htm)|Ghostly Hand|Main spectrale|officielle|
|[lxqSAaKp7jMekQke.htm](age-of-ashes-bestiary-items/lxqSAaKp7jMekQke.htm)|Inflate Bellows|Gonfler le soufflet|officielle|
|[M0hdta5U4NIGRZAD.htm](age-of-ashes-bestiary-items/M0hdta5U4NIGRZAD.htm)|Terrifying Visions|Visions terrifiantes|officielle|
|[M2OxBMZbqLpIs5U3.htm](age-of-ashes-bestiary-items/M2OxBMZbqLpIs5U3.htm)|+2 Resilient Leather Armor|Armure en cuir de résilience +2|officielle|
|[M4OptJYHoUkyQh2f.htm](age-of-ashes-bestiary-items/M4OptJYHoUkyQh2f.htm)|Trample|Piétinement|officielle|
|[M4S8LvGZQeUkoq88.htm](age-of-ashes-bestiary-items/M4S8LvGZQeUkoq88.htm)|Horns|Corne|officielle|
|[m69H7bbz05FeE4YE.htm](age-of-ashes-bestiary-items/m69H7bbz05FeE4YE.htm)|+1 Leather Armor|Armure en cuir +1|officielle|
|[m8n5v2B0saXm9zp1.htm](age-of-ashes-bestiary-items/m8n5v2B0saXm9zp1.htm)|Major Bottled Lightning|Foudre en bouteille majeure|officielle|
|[m8WR1jKIWEJbbaKO.htm](age-of-ashes-bestiary-items/m8WR1jKIWEJbbaKO.htm)|Phantom Pain (At Will)|Douleur fantôme (À volonté)|officielle|
|[m8X8IWzzFl0HzmIE.htm](age-of-ashes-bestiary-items/m8X8IWzzFl0HzmIE.htm)|Golden Luck|Chance dorée|officielle|
|[MaPDXpGVOauOwVc8.htm](age-of-ashes-bestiary-items/MaPDXpGVOauOwVc8.htm)|Rock|Rocher|officielle|
|[mEFkqvuYvc9WzXcr.htm](age-of-ashes-bestiary-items/mEFkqvuYvc9WzXcr.htm)|Reactive Shield|Bouclier réactif|officielle|
|[mgUFs2wbhtnPQXLr.htm](age-of-ashes-bestiary-items/mgUFs2wbhtnPQXLr.htm)|Fist|Poing|officielle|
|[MhYvDcxQITUxb0tw.htm](age-of-ashes-bestiary-items/MhYvDcxQITUxb0tw.htm)|Sneak Attack|Attaque sournoise|officielle|
|[MJg2MnY8wKpQte4q.htm](age-of-ashes-bestiary-items/MJg2MnY8wKpQte4q.htm)|Attack of Opportunity (Special)|Attaque d'opportunité (Spéciale)|officielle|
|[mjhchEu4tZZFuxhA.htm](age-of-ashes-bestiary-items/mjhchEu4tZZFuxhA.htm)|+2 Status to All Saves vs. Magic|+2 de statut aux sauvegardes contre la magie|officielle|
|[mJhd5iLsmLx74Qjo.htm](age-of-ashes-bestiary-items/mJhd5iLsmLx74Qjo.htm)|Spontaneous Divine Spells|Sorts divins spontanés|libre|
|[mkKQLOSUvNml05qi.htm](age-of-ashes-bestiary-items/mkKQLOSUvNml05qi.htm)|Composite Shortbow|Arc court composite|officielle|
|[mKqF9CDVTOOPFVm6.htm](age-of-ashes-bestiary-items/mKqF9CDVTOOPFVm6.htm)|Surprise Attack|Attaque surprise|officielle|
|[MLHgQjjeyBRDloTd.htm](age-of-ashes-bestiary-items/MLHgQjjeyBRDloTd.htm)|Entangling Residue|Résidu entravant|officielle|
|[MmnbEXy9FWwuhWIU.htm](age-of-ashes-bestiary-items/MmnbEXy9FWwuhWIU.htm)|Dagger|+2,greaterStriking|Dague de frappe supérieure +2|libre|
|[MMtAAek31GnnT3PK.htm](age-of-ashes-bestiary-items/MMtAAek31GnnT3PK.htm)|Warhammer|Marteau de guerre|officielle|
|[mMwtoDsUUn0ZCGvL.htm](age-of-ashes-bestiary-items/mMwtoDsUUn0ZCGvL.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[MNIeEFHVbRZ8b5Ln.htm](age-of-ashes-bestiary-items/MNIeEFHVbRZ8b5Ln.htm)|Hostile Juxtaposition|Juxtaposition hostile|officielle|
|[mnMWLmMnVuCl04IC.htm](age-of-ashes-bestiary-items/mnMWLmMnVuCl04IC.htm)|Fling Sand in the Eyes|Projection de sable dans les yeux|officielle|
|[Mo8pDcRqUWKoQzPF.htm](age-of-ashes-bestiary-items/Mo8pDcRqUWKoQzPF.htm)|Jaws|Mâchoires|officielle|
|[moQZVLDgyoG4xYuR.htm](age-of-ashes-bestiary-items/moQZVLDgyoG4xYuR.htm)|Volcanic Purge|Purge volcanique|officielle|
|[Mp6f5v7pvptmPQVp.htm](age-of-ashes-bestiary-items/Mp6f5v7pvptmPQVp.htm)|+2 Greater Resilient Full Plate|Harnois de résilience supérieure +2|officielle|
|[MpCCxEfa5YHRcVVR.htm](age-of-ashes-bestiary-items/MpCCxEfa5YHRcVVR.htm)|Mace|Masse d'armes|officielle|
|[MQvALn3TuAb3iI1B.htm](age-of-ashes-bestiary-items/MQvALn3TuAb3iI1B.htm)|Innate Arcane Spells|Sorts arcaniques innés|libre|
|[Mrf2WIIKXTByxA81.htm](age-of-ashes-bestiary-items/Mrf2WIIKXTByxA81.htm)|Intimidating Attack of Opportunity|Attaque d'opportunité intimidante|officielle|
|[mrHQQjwSTfXCnZch.htm](age-of-ashes-bestiary-items/mrHQQjwSTfXCnZch.htm)|Chained Dagger|Chaîne-dague|officielle|
|[mSAR7TLZJBzuU46P.htm](age-of-ashes-bestiary-items/mSAR7TLZJBzuU46P.htm)|Thrown Rock|Projection de rocher|officielle|
|[msk25zy4vClQBl9Q.htm](age-of-ashes-bestiary-items/msk25zy4vClQBl9Q.htm)|Inflate Bellows|Gonfler le soufflet|officielle|
|[MTe5oaN8qxo4KYFm.htm](age-of-ashes-bestiary-items/MTe5oaN8qxo4KYFm.htm)|Demiplane Lair|Demi-plan en guise d'antre|officielle|
|[mTgeFQ9F1VC2Jckr.htm](age-of-ashes-bestiary-items/mTgeFQ9F1VC2Jckr.htm)|Wizard School Spells|Sorts de l'école de magicien|libre|
|[MuK4kIpfXEUbojeB.htm](age-of-ashes-bestiary-items/MuK4kIpfXEUbojeB.htm)|HP is 72 for each 5 foot patch|72 PV par plaque de 1,5 m|officielle|
|[Mvo72kTvELk6g3ZD.htm](age-of-ashes-bestiary-items/Mvo72kTvELk6g3ZD.htm)|Shadow Plane Lore|Connaissance du plan de l'ombre|officielle|
|[MW0LxdUJZd9ozWtO.htm](age-of-ashes-bestiary-items/MW0LxdUJZd9ozWtO.htm)|Fast Healing 20|Guérison accélérée 20|officielle|
|[mwEc6YhRZUrr9Qkb.htm](age-of-ashes-bestiary-items/mwEc6YhRZUrr9Qkb.htm)|Kick|Coup de pied|officielle|
|[mXRRlIf5fS8NO5I1.htm](age-of-ashes-bestiary-items/mXRRlIf5fS8NO5I1.htm)|Woodland Stride|Déplacement facilité en forêt|officielle|
|[MYgT9dmiyGDI48Hx.htm](age-of-ashes-bestiary-items/MYgT9dmiyGDI48Hx.htm)|Superlative Summoner|Convocateur remarquable|officielle|
|[MyJ5pEba9K3unoCm.htm](age-of-ashes-bestiary-items/MyJ5pEba9K3unoCm.htm)|Archery Experience|Expertise en archerie|officielle|
|[MZs5uxec71OZQSzk.htm](age-of-ashes-bestiary-items/MZs5uxec71OZQSzk.htm)|Disrupting Cold Iron Dagger|Dague en fer froid perturbatrice|officielle|
|[MZVij16ep6tHatWC.htm](age-of-ashes-bestiary-items/MZVij16ep6tHatWC.htm)|+1 Striking Staff of Necromancy (Greater)|+1,striking|Bâton de nécromancie supérieur de frappe +1|officielle|
|[N01NwkU6GvutbIp7.htm](age-of-ashes-bestiary-items/N01NwkU6GvutbIp7.htm)|Shell Block|Blocage avec la carapace|officielle|
|[N096jcv77bKpi9xA.htm](age-of-ashes-bestiary-items/N096jcv77bKpi9xA.htm)|Backshot|Contre-tir|officielle|
|[n0JJ4L7jPOWqDejM.htm](age-of-ashes-bestiary-items/n0JJ4L7jPOWqDejM.htm)|Matriarch's Caress|Caresse de la matriarche|officielle|
|[n2I1TMJLTQ65exGf.htm](age-of-ashes-bestiary-items/n2I1TMJLTQ65exGf.htm)|Arcane Innate Spells|Sorts arcaniques innés|libre|
|[n327lTMbR7EaIPxV.htm](age-of-ashes-bestiary-items/n327lTMbR7EaIPxV.htm)|Darting Shot|Tir véloce|officielle|
|[n32H8SDG3hQfZVNB.htm](age-of-ashes-bestiary-items/n32H8SDG3hQfZVNB.htm)|Instantaneous Movement|Déplacement instantané|officielle|
|[n4acrtFiDu5TwYGe.htm](age-of-ashes-bestiary-items/n4acrtFiDu5TwYGe.htm)|Hammer the Chained|Battre les prisonniers|officielle|
|[N62QvxPwdPCrIlP1.htm](age-of-ashes-bestiary-items/N62QvxPwdPCrIlP1.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|libre|
|[n6SPErVyS2S3b3lv.htm](age-of-ashes-bestiary-items/n6SPErVyS2S3b3lv.htm)|Horn Snare|Piège cornu|officielle|
|[N9ItpLBOPJW5l9DI.htm](age-of-ashes-bestiary-items/N9ItpLBOPJW5l9DI.htm)|Extra Reaction|Surcoît de réaction|officielle|
|[nA6MKkodX1XzMSli.htm](age-of-ashes-bestiary-items/nA6MKkodX1XzMSli.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|officielle|
|[naVs815EnCcnoaW1.htm](age-of-ashes-bestiary-items/naVs815EnCcnoaW1.htm)|Drowning Drone|Bourdonnement parasite|officielle|
|[nf1iMpVleTsR2jEV.htm](age-of-ashes-bestiary-items/nf1iMpVleTsR2jEV.htm)|Internal Furnace|Fournaise interne|officielle|
|[NfPbxqhBdeyeKfyS.htm](age-of-ashes-bestiary-items/NfPbxqhBdeyeKfyS.htm)|Weapon Master|Maître d'armes|officielle|
|[NgedLFfO7OJlxvqT.htm](age-of-ashes-bestiary-items/NgedLFfO7OJlxvqT.htm)|Frightful Presence|Présence terrifiante|officielle|
|[NgHUvhpolU7B4yNd.htm](age-of-ashes-bestiary-items/NgHUvhpolU7B4yNd.htm)|Dazzling Display|Parade éblouissante|officielle|
|[NI8nqs0x537N5LvI.htm](age-of-ashes-bestiary-items/NI8nqs0x537N5LvI.htm)|Tail|Queue|officielle|
|[NioStPlx80WxmDvR.htm](age-of-ashes-bestiary-items/NioStPlx80WxmDvR.htm)|Searing Heat|Chaleur accablante|officielle|
|[nkAN9Dt2tljmS96M.htm](age-of-ashes-bestiary-items/nkAN9Dt2tljmS96M.htm)|Site Bound (Jewelgate Way Station)|Lié à un site (Zone relais du portail des Joyaux)|officielle|
|[nKfHeaA4JlYimGVv.htm](age-of-ashes-bestiary-items/nKfHeaA4JlYimGVv.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[NKkENtFd0bUL6yx7.htm](age-of-ashes-bestiary-items/NKkENtFd0bUL6yx7.htm)|Woodland Stride|Déplacement facilité en forêt|officielle|
|[NKKJNwEod8IO6M9D.htm](age-of-ashes-bestiary-items/NKKJNwEod8IO6M9D.htm)|Sterling Blacksmith's Tools|Outils de forgeron exceptionnels|officielle|
|[NMhI8XzKo8AYJRpE.htm](age-of-ashes-bestiary-items/NMhI8XzKo8AYJRpE.htm)|Keys to Manacles|Clé des menottes|officielle|
|[nMOYUQ60Uw7g4k0q.htm](age-of-ashes-bestiary-items/nMOYUQ60Uw7g4k0q.htm)|Flurry|Déluge|officielle|
|[NNiuCix0bI7jl6FB.htm](age-of-ashes-bestiary-items/NNiuCix0bI7jl6FB.htm)|Shortbow|+2,striking|Arc court de frappe +2|libre|
|[nnJ4kOGHM4Qct2PR.htm](age-of-ashes-bestiary-items/nnJ4kOGHM4Qct2PR.htm)|Steady Spellcasting|Incantation fiable|officielle|
|[NNXU0f8NQLPkYMva.htm](age-of-ashes-bestiary-items/NNXU0f8NQLPkYMva.htm)|Painsight|Vision de la douleur|officielle|
|[nPeM50gZBykCKDxe.htm](age-of-ashes-bestiary-items/nPeM50gZBykCKDxe.htm)|Distant Ringing|Tintement lointain|officielle|
|[nPeylmjWQ7slkCXr.htm](age-of-ashes-bestiary-items/nPeylmjWQ7slkCXr.htm)|Teleportation Attachment|Téléportation jointe|officielle|
|[nSj9cVczzhejjp9x.htm](age-of-ashes-bestiary-items/nSj9cVczzhejjp9x.htm)|Saggorak Lore|Connaissance de Saggorak|officielle|
|[NtNZz3pZ90BCanve.htm](age-of-ashes-bestiary-items/NtNZz3pZ90BCanve.htm)|Burning Hands (At Will)|Mains brûlantes (À volonté)|officielle|
|[nTtoBqYHxSoaPWuC.htm](age-of-ashes-bestiary-items/nTtoBqYHxSoaPWuC.htm)|Site Bound|Lié à un site|officielle|
|[NttUPgbz0pGvaOnd.htm](age-of-ashes-bestiary-items/NttUPgbz0pGvaOnd.htm)|Low-Light Vision|Vision nocturne|officielle|
|[Nu8j6VpdlorVM75S.htm](age-of-ashes-bestiary-items/Nu8j6VpdlorVM75S.htm)|Designate Bellflower Crop|Désigner une récolte de Campanule|officielle|
|[nurvAKW6FmusUHRh.htm](age-of-ashes-bestiary-items/nurvAKW6FmusUHRh.htm)|Dagger|Dague|officielle|
|[nw4MbhXR5zCwPJBV.htm](age-of-ashes-bestiary-items/nw4MbhXR5zCwPJBV.htm)|Dagger|Dague|officielle|
|[nxnCWI2x0Zy4iWjR.htm](age-of-ashes-bestiary-items/nxnCWI2x0Zy4iWjR.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[NXVmpsr0ZCjlPFnB.htm](age-of-ashes-bestiary-items/NXVmpsr0ZCjlPFnB.htm)|+2 Resilient Scale Mail|Armure d'écailles de résilience +2|officielle|
|[nycjIkuU99HnHnf9.htm](age-of-ashes-bestiary-items/nycjIkuU99HnHnf9.htm)|Scent (Imprecise) 60 feet|Odorat 18 m (imprécis)|officielle|
|[NYttSUeq7rE5Lwlt.htm](age-of-ashes-bestiary-items/NYttSUeq7rE5Lwlt.htm)|Rapier|Rapière|officielle|
|[Nyz0P3ikAyq0ud3H.htm](age-of-ashes-bestiary-items/Nyz0P3ikAyq0ud3H.htm)|Thrown Weapon Mastery|Maîtrise des armes de jet|officielle|
|[NZ4xdM6bKvntdTEa.htm](age-of-ashes-bestiary-items/NZ4xdM6bKvntdTEa.htm)|Fearsome Brute|Brute effrayante|officielle|
|[o0njxNAMbjPeC9AS.htm](age-of-ashes-bestiary-items/o0njxNAMbjPeC9AS.htm)|Cataclysmic Rain|Déluge cataclysmique|officielle|
|[o10Yz3qVKjeI4Fxz.htm](age-of-ashes-bestiary-items/o10Yz3qVKjeI4Fxz.htm)|Breath Weapon|Souffle|officielle|
|[o1edk89NwTsVdvC1.htm](age-of-ashes-bestiary-items/o1edk89NwTsVdvC1.htm)|Rejuvenation|Reconstruction|officielle|
|[o4fl6Wqm7Pupv1R8.htm](age-of-ashes-bestiary-items/o4fl6Wqm7Pupv1R8.htm)|Tail|Queue|officielle|
|[O4Vu0lAef9YMzn23.htm](age-of-ashes-bestiary-items/O4Vu0lAef9YMzn23.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|officielle|
|[O5uoqD3Lzqcdx708.htm](age-of-ashes-bestiary-items/O5uoqD3Lzqcdx708.htm)|Wizard School Spells|Sorts de l'école de magicien|libre|
|[O6DYo04qm4tluUeG.htm](age-of-ashes-bestiary-items/O6DYo04qm4tluUeG.htm)|Spine Volley|Volée d'épines|officielle|
|[o8XSgCFe5Tgc8VwW.htm](age-of-ashes-bestiary-items/o8XSgCFe5Tgc8VwW.htm)|Vulnerable to Stone to Flesh|Vulnérabilité à Pierre en chair|officielle|
|[o9eWNluFGesUiB9v.htm](age-of-ashes-bestiary-items/o9eWNluFGesUiB9v.htm)|Draconic Frenzy|Frénésie draconique|officielle|
|[O9V5OSgRwAX3gEBH.htm](age-of-ashes-bestiary-items/O9V5OSgRwAX3gEBH.htm)|Grab|Empoignade|officielle|
|[OBpAWIEWPW9DSQMx.htm](age-of-ashes-bestiary-items/OBpAWIEWPW9DSQMx.htm)|Longbow|Arc long|officielle|
|[OBr4iMQYSp8jHCDH.htm](age-of-ashes-bestiary-items/OBr4iMQYSp8jHCDH.htm)|+2 Greater Resilient Full Plate|Harnois de résilience supérieure +2|officielle|
|[oBtQcDrceotbFbrk.htm](age-of-ashes-bestiary-items/oBtQcDrceotbFbrk.htm)|Constant Spells|Sorts constants|officielle|
|[oBv9CmgE9lFftp0E.htm](age-of-ashes-bestiary-items/oBv9CmgE9lFftp0E.htm)|Swiftness|Célérité|officielle|
|[obvyw8sjJyDFGLDy.htm](age-of-ashes-bestiary-items/obvyw8sjJyDFGLDy.htm)|Infused Items|Objets imprégnés|officielle|
|[OC3Cenu8w3vfySOp.htm](age-of-ashes-bestiary-items/OC3Cenu8w3vfySOp.htm)|Twisting Tail|Queue sinueuse|officielle|
|[OCml3BrX8mx0m7zq.htm](age-of-ashes-bestiary-items/OCml3BrX8mx0m7zq.htm)|True Seeing (Constant)|Vision lucide (constant)|officielle|
|[Od7nphkGvQM8Iv5L.htm](age-of-ashes-bestiary-items/Od7nphkGvQM8Iv5L.htm)|Blue Eye Beam|Rayon oculaire bleu|officielle|
|[oDodI8lhFK3QYtHx.htm](age-of-ashes-bestiary-items/oDodI8lhFK3QYtHx.htm)|Indigo Eye Beam|Rayon oculaire indigo|officielle|
|[ODRNG6vG9zjC3Etf.htm](age-of-ashes-bestiary-items/ODRNG6vG9zjC3Etf.htm)|Rend|Éventration|officielle|
|[oeNy6QRmjjdgIkfT.htm](age-of-ashes-bestiary-items/oeNy6QRmjjdgIkfT.htm)|+2 Resilient Full Plate|Harnois de résilience +2|officielle|
|[OfIb5QtIPTTftNlq.htm](age-of-ashes-bestiary-items/OfIb5QtIPTTftNlq.htm)|Low-Light Vision|Vision nocturne|officielle|
|[oFwQEUiMJeXE9xX7.htm](age-of-ashes-bestiary-items/oFwQEUiMJeXE9xX7.htm)|Fly (Constant)|Vol (constant)|officielle|
|[oh8XLsynAdQBhHEF.htm](age-of-ashes-bestiary-items/oh8XLsynAdQBhHEF.htm)|Azlanti Lore|Connaissance de l'Azlant|officielle|
|[OHnvdksMNZmChe4c.htm](age-of-ashes-bestiary-items/OHnvdksMNZmChe4c.htm)|Light Hammer|Marteau de guerre léger|officielle|
|[OHsCxCSp0K0AQCsz.htm](age-of-ashes-bestiary-items/OHsCxCSp0K0AQCsz.htm)|Enlarge (Self Only)|Agrandissement (soi uniquement)|officielle|
|[oIJ6ppXsMXZMBfSi.htm](age-of-ashes-bestiary-items/oIJ6ppXsMXZMBfSi.htm)|Liberation Vulnerability|Vulnérabilité à la libération|officielle|
|[Okk4imbIUOwkYs3E.htm](age-of-ashes-bestiary-items/Okk4imbIUOwkYs3E.htm)|Hunter's Aim|Visée du chasseur|officielle|
|[oktku6eQTO0fa63B.htm](age-of-ashes-bestiary-items/oktku6eQTO0fa63B.htm)|Scimitar|+2,striking|Cimeterre de frappe +2|libre|
|[OLb44HyPmPQ1A0Sm.htm](age-of-ashes-bestiary-items/OLb44HyPmPQ1A0Sm.htm)|+1 Status to All Saves vs. Positive|bonus de statut de +1 aux JdS contre les effets positifs|officielle|
|[OlZeNmmphsIzWVz5.htm](age-of-ashes-bestiary-items/OlZeNmmphsIzWVz5.htm)|Composite Shortbow|Arc court composite|officielle|
|[OM9iuNqyLKsh2ZjP.htm](age-of-ashes-bestiary-items/OM9iuNqyLKsh2ZjP.htm)|Darkvision|Vision dans le noir|officielle|
|[OmjmOPy2aa488z2d.htm](age-of-ashes-bestiary-items/OmjmOPy2aa488z2d.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|officielle|
|[OMliTjyRbQBsIAzC.htm](age-of-ashes-bestiary-items/OMliTjyRbQBsIAzC.htm)|Dimension Door (At Will)|Porte dimensionnelle (À volonté)|officielle|
|[omPyHCBiVsSmjHtN.htm](age-of-ashes-bestiary-items/omPyHCBiVsSmjHtN.htm)|Keys to Each Dormitory Door and Slave Manacles|Clés des dortoirs et des menottes des esclaves|officielle|
|[Omu620MVyXotE5Nc.htm](age-of-ashes-bestiary-items/Omu620MVyXotE5Nc.htm)|Breath Weapon|Souffle|officielle|
|[OOmkcTVG03cJ0cT3.htm](age-of-ashes-bestiary-items/OOmkcTVG03cJ0cT3.htm)|Shield Block|Blocage au bouclier|officielle|
|[oPh7RYiHA3OguL9Z.htm](age-of-ashes-bestiary-items/oPh7RYiHA3OguL9Z.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[oqM1d6hyqJXDeB6F.htm](age-of-ashes-bestiary-items/oqM1d6hyqJXDeB6F.htm)|Wasp Trapped in Amber|Guêpe prise dans l’ambre|officielle|
|[OqoxUu62vH7mcrPs.htm](age-of-ashes-bestiary-items/OqoxUu62vH7mcrPs.htm)|Terrifying Stare|Regard terrifiant|officielle|
|[oqVJRy53SS1ih6ly.htm](age-of-ashes-bestiary-items/oqVJRy53SS1ih6ly.htm)|Goblin Lore|Connaissance des gobelins|officielle|
|[Oqy1IXCABemD4A9g.htm](age-of-ashes-bestiary-items/Oqy1IXCABemD4A9g.htm)|Sneak Attack|Attaque sournoise|officielle|
|[OSgjqd6Z5NewzIw4.htm](age-of-ashes-bestiary-items/OSgjqd6Z5NewzIw4.htm)|Focus Spells|Sorts focalisés|libre|
|[ou3LZoiUQhZ13e1n.htm](age-of-ashes-bestiary-items/ou3LZoiUQhZ13e1n.htm)|Telekinetic Haul (At Will)|Transport télékinésique (À volonté)|officielle|
|[ou4yHaSTmuMZhC5y.htm](age-of-ashes-bestiary-items/ou4yHaSTmuMZhC5y.htm)|Scimitar|+1,striking|Cimeterre de frappe +1|libre|
|[oUAmM1vrjuPEVqvT.htm](age-of-ashes-bestiary-items/oUAmM1vrjuPEVqvT.htm)|Longsword|+2,greaterStriking|Épée longue de frappe supérieure +2|libre|
|[ouAmQ8tGIKalQy5V.htm](age-of-ashes-bestiary-items/ouAmQ8tGIKalQy5V.htm)|Juggernaut Mutagen (Major) [Infused]|Mutagène du juggernaut majeur [imprégné]|officielle|
|[oUbNiZbQMyGjNTIj.htm](age-of-ashes-bestiary-items/oUbNiZbQMyGjNTIj.htm)|Reactionary|Réactif|officielle|
|[OuGn9uqYP90nZvNX.htm](age-of-ashes-bestiary-items/OuGn9uqYP90nZvNX.htm)|Summon Fiend (Augur Velstrac Only)|Convocation de fiélon (velstrac augure uniquement)|officielle|
|[ouzInD92zLt5EjxO.htm](age-of-ashes-bestiary-items/ouzInD92zLt5EjxO.htm)|Underworld Lore|Connaissance de la pègre|officielle|
|[OvKpoweI2x9CioBO.htm](age-of-ashes-bestiary-items/OvKpoweI2x9CioBO.htm)|Overwhelming Energy|Énergie écrasante|officielle|
|[OVyCsNi5aandXp9w.htm](age-of-ashes-bestiary-items/OVyCsNi5aandXp9w.htm)|Hunt Prey|Chasser une proie|officielle|
|[owejjJWuMlBx46tR.htm](age-of-ashes-bestiary-items/owejjJWuMlBx46tR.htm)|Whiplash|Coup de fouet|officielle|
|[OWhmwuZtv80QG0wy.htm](age-of-ashes-bestiary-items/OWhmwuZtv80QG0wy.htm)|Surprise Attack|Attaque surprise|officielle|
|[OwoH6FLZ94ihNvEv.htm](age-of-ashes-bestiary-items/OwoH6FLZ94ihNvEv.htm)|Returning Light Hammer|Marteau de guerre léger boomerang|officielle|
|[oy3YXIlxGdlgBh3N.htm](age-of-ashes-bestiary-items/oy3YXIlxGdlgBh3N.htm)|+1 Resilient Studded Leather Armor|Armure en cuir clouté de résilience +1|officielle|
|[OzVIHvIPV38rnIVv.htm](age-of-ashes-bestiary-items/OzVIHvIPV38rnIVv.htm)|Darkvision|Vision dans le noir|officielle|
|[ozZqtI8i4QAUvyoA.htm](age-of-ashes-bestiary-items/ozZqtI8i4QAUvyoA.htm)|Primal Prepared Spells|Sorts primordiaux préparés|libre|
|[p1bvrj2qMZFdHpsv.htm](age-of-ashes-bestiary-items/p1bvrj2qMZFdHpsv.htm)|Self-Repair|Rénovation|officielle|
|[P54JJ58qynLo2URI.htm](age-of-ashes-bestiary-items/P54JJ58qynLo2URI.htm)|Breath Weapon|Souffle|officielle|
|[p5rguZk07i9usCOe.htm](age-of-ashes-bestiary-items/p5rguZk07i9usCOe.htm)|Dagger|Dague|officielle|
|[P8LB7aKoS0qhsbne.htm](age-of-ashes-bestiary-items/P8LB7aKoS0qhsbne.htm)|Ancient History Lore|Connaissance de l'histoire ancienne|officielle|
|[p9DHIoccyeBea4Yd.htm](age-of-ashes-bestiary-items/p9DHIoccyeBea4Yd.htm)|Twin Shot|Tirs jumeaux|officielle|
|[PA0WqIGLWwPFkdAP.htm](age-of-ashes-bestiary-items/PA0WqIGLWwPFkdAP.htm)|Telekinetic Maneuver (At Will)|Manoeuvre télékinésique (À volonté)|officielle|
|[pb3yDpk6Rl0BuY7j.htm](age-of-ashes-bestiary-items/pb3yDpk6Rl0BuY7j.htm)|Arcane Innate Spells|Sorts arcaniques innés|libre|
|[PcDH3sH6RU74yewe.htm](age-of-ashes-bestiary-items/PcDH3sH6RU74yewe.htm)|Liberating Trick|Astuce libératrice|officielle|
|[PcLLTu6aeVnY2Ogg.htm](age-of-ashes-bestiary-items/PcLLTu6aeVnY2Ogg.htm)|Claw|Griffe|officielle|
|[pcMdAOs6T1huhf1k.htm](age-of-ashes-bestiary-items/pcMdAOs6T1huhf1k.htm)|Greatsword|+2,greaterStriking|Épée à deux mains de frappe supérieure +2|libre|
|[pDkq3pcVeqspt9o5.htm](age-of-ashes-bestiary-items/pDkq3pcVeqspt9o5.htm)|Darkvision|Vision dans le noir|officielle|
|[PdMypFZGmGTQBUO7.htm](age-of-ashes-bestiary-items/PdMypFZGmGTQBUO7.htm)|Door|Porte|officielle|
|[pdp1XYZ1mtcsTvM3.htm](age-of-ashes-bestiary-items/pdp1XYZ1mtcsTvM3.htm)|Prepared Arcane Spells|Sorts arcaniques préparés|libre|
|[peAxpqZ3KlwaPcjL.htm](age-of-ashes-bestiary-items/peAxpqZ3KlwaPcjL.htm)|Paralyzing Force|Force paralysante|officielle|
|[PemrcXjJFtKQ8192.htm](age-of-ashes-bestiary-items/PemrcXjJFtKQ8192.htm)|Shortsword|Épée courte|officielle|
|[peSYmPiuqBy9OU2r.htm](age-of-ashes-bestiary-items/peSYmPiuqBy9OU2r.htm)|Change Shape|Changement de forme|officielle|
|[pF8fcF4d3JB5eI7V.htm](age-of-ashes-bestiary-items/pF8fcF4d3JB5eI7V.htm)|Darkvision|Vision dans le noir|officielle|
|[pfkbWfIp6yg9hS9F.htm](age-of-ashes-bestiary-items/pfkbWfIp6yg9hS9F.htm)|Shortsword|Épée courte|officielle|
|[pFweIlOkcH38fuxf.htm](age-of-ashes-bestiary-items/pFweIlOkcH38fuxf.htm)|Spikes|Pics|officielle|
|[pHsMHzt7Z1sM5ScM.htm](age-of-ashes-bestiary-items/pHsMHzt7Z1sM5ScM.htm)|Tongue|Langue|officielle|
|[PhUe9NLaMWI1oIUJ.htm](age-of-ashes-bestiary-items/PhUe9NLaMWI1oIUJ.htm)|Halberd|Hallebarde|officielle|
|[pIKbKs8nTXF2nUAm.htm](age-of-ashes-bestiary-items/pIKbKs8nTXF2nUAm.htm)|Fiend Summoner|Convocateur de fiélons|officielle|
|[pjPtT8ePki7wgMaO.htm](age-of-ashes-bestiary-items/pjPtT8ePki7wgMaO.htm)|Swift Leap|Bond rapide|officielle|
|[plcsZ25Y6jlruAMn.htm](age-of-ashes-bestiary-items/plcsZ25Y6jlruAMn.htm)|Twin Parry|Parade jumelée|officielle|
|[pLURCvEu8rYktiZE.htm](age-of-ashes-bestiary-items/pLURCvEu8rYktiZE.htm)|Paralyzing Touch|Toucher paralysant|officielle|
|[pmGTuXbF38zbcznu.htm](age-of-ashes-bestiary-items/pmGTuXbF38zbcznu.htm)|Claw|Griffe|officielle|
|[PmP7kVHHTCVo9E2y.htm](age-of-ashes-bestiary-items/PmP7kVHHTCVo9E2y.htm)|Longsword|Épée longue|officielle|
|[PMvxB0zhxiZNDxxW.htm](age-of-ashes-bestiary-items/PMvxB0zhxiZNDxxW.htm)|Necrotic Field|Champ nécrotique|officielle|
|[ppHf9s7R9QhMXyxE.htm](age-of-ashes-bestiary-items/ppHf9s7R9QhMXyxE.htm)|Draconic Momentum|Impulsion draconique|officielle|
|[ppTxZD2ni4N12liM.htm](age-of-ashes-bestiary-items/ppTxZD2ni4N12liM.htm)|Fangs|Crocs|officielle|
|[PPVreyhaPRMpOiLh.htm](age-of-ashes-bestiary-items/PPVreyhaPRMpOiLh.htm)|Key to Chiselrock's Prison|Clé de la prison de Cisèlepierre|officielle|
|[pqI4LFJJ8zEZDSlf.htm](age-of-ashes-bestiary-items/pqI4LFJJ8zEZDSlf.htm)|Javelin|+2,greaterStriking|Javelot de frappe supérieure +2|libre|
|[pqOaIKfZFro1QcD2.htm](age-of-ashes-bestiary-items/pqOaIKfZFro1QcD2.htm)|Shrieking Frenzy|Frénésie hurlante|officielle|
|[PrMrxrT6XDC0RiYK.htm](age-of-ashes-bestiary-items/PrMrxrT6XDC0RiYK.htm)|Thrown Weapon Mastery|Maîtrise des armes de jet|officielle|
|[Ps0fTafFUadvtLAi.htm](age-of-ashes-bestiary-items/Ps0fTafFUadvtLAi.htm)|Efficient Capture|Capture efficace|officielle|
|[PT2MUTRJtcohFtP9.htm](age-of-ashes-bestiary-items/PT2MUTRJtcohFtP9.htm)|Lava Bomb|Bombe de lave|officielle|
|[pTHZcKbaFZmIwmOo.htm](age-of-ashes-bestiary-items/pTHZcKbaFZmIwmOo.htm)|Occult Prepared Spells|Sorts occultes préparés|libre|
|[PTLfrf6aBtRzqBS1.htm](age-of-ashes-bestiary-items/PTLfrf6aBtRzqBS1.htm)|Nolly's Hoe|Houe de Nolly|officielle|
|[PUIN2KruDbWQEf29.htm](age-of-ashes-bestiary-items/PUIN2KruDbWQEf29.htm)|Scimitar|+2,greaterStriking|Cimeterre de frappe supérieure +2|libre|
|[pvaF5cf0XkH3cCbK.htm](age-of-ashes-bestiary-items/pvaF5cf0XkH3cCbK.htm)|Claw|Griffe|officielle|
|[Pw6xjk3Ocpe5x0m4.htm](age-of-ashes-bestiary-items/Pw6xjk3Ocpe5x0m4.htm)|Composite Shortbow|+2,greaterStriking|Arc court composite de Frappe supérieure +2|libre|
|[pWNu3N2Ovu0m978Z.htm](age-of-ashes-bestiary-items/pWNu3N2Ovu0m978Z.htm)|Blood Quarry|Proie sanglante|officielle|
|[PWOiCJ1IBh9T12ba.htm](age-of-ashes-bestiary-items/PWOiCJ1IBh9T12ba.htm)|Darkvision|Vision dans le noir|officielle|
|[Py6UVHQQNzpI6Lww.htm](age-of-ashes-bestiary-items/Py6UVHQQNzpI6Lww.htm)|Soul Chain|Chaîne d'âme|officielle|
|[py7MzheoXU7EEjNA.htm](age-of-ashes-bestiary-items/py7MzheoXU7EEjNA.htm)|Negative Healing|Guérison négative|officielle|
|[pYNPdghXNuSzUUUS.htm](age-of-ashes-bestiary-items/pYNPdghXNuSzUUUS.htm)|Light Blindness|Aveuglé par la lumière|officielle|
|[q1GTtW7ypFaAaLpl.htm](age-of-ashes-bestiary-items/q1GTtW7ypFaAaLpl.htm)|Shield Block|Blocage au bouclier|officielle|
|[Q2kYqdtBur9KmjY3.htm](age-of-ashes-bestiary-items/Q2kYqdtBur9KmjY3.htm)|Jaws|Mâchoires|officielle|
|[Q6x7Lrplu7fSsiYf.htm](age-of-ashes-bestiary-items/Q6x7Lrplu7fSsiYf.htm)|Darkvision|Vision dans le noir|officielle|
|[q90PCZCTBqoxOX62.htm](age-of-ashes-bestiary-items/q90PCZCTBqoxOX62.htm)|Jaws|Mâchoires|officielle|
|[Qbd7q1jTcWs61OsF.htm](age-of-ashes-bestiary-items/Qbd7q1jTcWs61OsF.htm)|Hail of Arrows|Volée de flèches|officielle|
|[qCH0ksRH4qTOdKil.htm](age-of-ashes-bestiary-items/qCH0ksRH4qTOdKil.htm)|Warhammer|+2,striking,adamantine|Marteau de guerre en adamantium de frappe +2|officielle|
|[QD3U7kcL3GCOpa3y.htm](age-of-ashes-bestiary-items/QD3U7kcL3GCOpa3y.htm)|Smoke Vision|Vision malgré la fumée|officielle|
|[qdRHgb9PmpqQ2jTG.htm](age-of-ashes-bestiary-items/qdRHgb9PmpqQ2jTG.htm)|Quick Draw|Arme en main|officielle|
|[qfAZ12H9DKd8pEad.htm](age-of-ashes-bestiary-items/qfAZ12H9DKd8pEad.htm)|Shell Defense|Défense de l'enveloppe|officielle|
|[QFVHYWU00lctwBwf.htm](age-of-ashes-bestiary-items/QFVHYWU00lctwBwf.htm)|Change Shape|Changer de forme|officielle|
|[QhwSqyQ7YKPRITDw.htm](age-of-ashes-bestiary-items/QhwSqyQ7YKPRITDw.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[qi5FtOzBWTiiCStB.htm](age-of-ashes-bestiary-items/qi5FtOzBWTiiCStB.htm)|Explosion|Déflagration|officielle|
|[qIJ5KclBfbHjmuzk.htm](age-of-ashes-bestiary-items/qIJ5KclBfbHjmuzk.htm)|The Extraplanar Registry|Le registre extraplanaire|officielle|
|[QKmae867GS9N8IKK.htm](age-of-ashes-bestiary-items/QKmae867GS9N8IKK.htm)|Darkvision|Vision dans le noir|officielle|
|[QKwduCpw7Bm9RQv2.htm](age-of-ashes-bestiary-items/QKwduCpw7Bm9RQv2.htm)|Hand Crossbow|+1|Arbalète de poing +1|libre|
|[qLKyo9iiDXWWqXvs.htm](age-of-ashes-bestiary-items/qLKyo9iiDXWWqXvs.htm)|Grab|Empoignade|officielle|
|[QmrJQN5wGkBpsqMo.htm](age-of-ashes-bestiary-items/QmrJQN5wGkBpsqMo.htm)|Bard Composition Spells|Sorts de composition de barde|libre|
|[qnM0VhMof6zWC5w1.htm](age-of-ashes-bestiary-items/qnM0VhMof6zWC5w1.htm)|Claw|Griffe|officielle|
|[QPPzshzEbERB3TmQ.htm](age-of-ashes-bestiary-items/QPPzshzEbERB3TmQ.htm)|Divine Prepared Spells|Sorts divins préparés|libre|
|[Qqwiu5T6ag0OjmLs.htm](age-of-ashes-bestiary-items/Qqwiu5T6ag0OjmLs.htm)|Darkvision|Vision dans le noir|officielle|
|[QStNE8d3elauT8lt.htm](age-of-ashes-bestiary-items/QStNE8d3elauT8lt.htm)|Silver Dagger|Dague en argent|officielle|
|[QsuzBxkK6bf7oS8S.htm](age-of-ashes-bestiary-items/QsuzBxkK6bf7oS8S.htm)|Captivate|Fasciner|officielle|
|[QtbetvkizVrPlsdb.htm](age-of-ashes-bestiary-items/QtbetvkizVrPlsdb.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|officielle|
|[qtiwr8Rp5Y4kIa7O.htm](age-of-ashes-bestiary-items/qtiwr8Rp5Y4kIa7O.htm)|Quick Alchemy|Alchimie rapide|officielle|
|[QVKGD3jLsVMeTZzu.htm](age-of-ashes-bestiary-items/QVKGD3jLsVMeTZzu.htm)|Surprise Strike|Frappe surprise|officielle|
|[Qw4pImBH1KPofsS1.htm](age-of-ashes-bestiary-items/Qw4pImBH1KPofsS1.htm)|Assemble Choir|Étoffer le choeur|officielle|
|[qWDx8yQWBVM8oIuO.htm](age-of-ashes-bestiary-items/qWDx8yQWBVM8oIuO.htm)|Shortsword|+2,striking|Épée courte de frappe +2|officielle|
|[QWZ2jVcqi9CN4U6I.htm](age-of-ashes-bestiary-items/QWZ2jVcqi9CN4U6I.htm)|Sneak Attack|Attaque sournoise|officielle|
|[r0yOc789JUXP2zpN.htm](age-of-ashes-bestiary-items/r0yOc789JUXP2zpN.htm)|Aluum Antimagic|Antimagie aluum|officielle|
|[R9ndNOt0KSVZBrCG.htm](age-of-ashes-bestiary-items/R9ndNOt0KSVZBrCG.htm)|Nimble Dodge|Esquive agile|officielle|
|[RBix0BOrbHGTntW9.htm](age-of-ashes-bestiary-items/RBix0BOrbHGTntW9.htm)|Dragonstorm Aura|Aura de tempête draconique|officielle|
|[RF1LobyvELbvZKdf.htm](age-of-ashes-bestiary-items/RF1LobyvELbvZKdf.htm)|Wings|Aile|officielle|
|[Rg3ygnHjRloP7FoD.htm](age-of-ashes-bestiary-items/Rg3ygnHjRloP7FoD.htm)|Arcane Innate Spells|Sorts arcaniques innés|libre|
|[rGBruApiTZDc1HuB.htm](age-of-ashes-bestiary-items/rGBruApiTZDc1HuB.htm)|Redirect Energy|Rediriger l'énergie|officielle|
|[RgYma4kYCK90TSJD.htm](age-of-ashes-bestiary-items/RgYma4kYCK90TSJD.htm)|Imprisoned|Emprisonnée|officielle|
|[rHJhscZyJ7wDsGiW.htm](age-of-ashes-bestiary-items/rHJhscZyJ7wDsGiW.htm)|Freedom of Movement (Constant)|Liberté de mouvement (constant)|officielle|
|[RhOO7QphVzTN8Y2j.htm](age-of-ashes-bestiary-items/RhOO7QphVzTN8Y2j.htm)|Furious Claws|Furie de griffes|officielle|
|[rI6uxFg6LJxzLDta.htm](age-of-ashes-bestiary-items/rI6uxFg6LJxzLDta.htm)|Sneak Attack|Attaque sournoise|officielle|
|[RIE5Ws1CBYpHQtCZ.htm](age-of-ashes-bestiary-items/RIE5Ws1CBYpHQtCZ.htm)|Staff|+3,majorStriking|Bâton de frappe majeure +3|officielle|
|[riLQEjTb79Im6cTV.htm](age-of-ashes-bestiary-items/riLQEjTb79Im6cTV.htm)|Tongue Pull|Traction avec la langue|officielle|
|[RiXHSOTCKfKIONvP.htm](age-of-ashes-bestiary-items/RiXHSOTCKfKIONvP.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|officielle|
|[RjIX3v1EU84c920i.htm](age-of-ashes-bestiary-items/RjIX3v1EU84c920i.htm)|Breath Snatcher|Voleur de souffle|officielle|
|[rJLMQhvHJTZBujHg.htm](age-of-ashes-bestiary-items/rJLMQhvHJTZBujHg.htm)|Frightful Presence|Présence terrifiante|officielle|
|[RJtBHxR2U04jtD7I.htm](age-of-ashes-bestiary-items/RJtBHxR2U04jtD7I.htm)|Destructive Explosion|Explosion destructrice|officielle|
|[RKUjuuHr7izDXZqd.htm](age-of-ashes-bestiary-items/RKUjuuHr7izDXZqd.htm)|Reach Spell|Sort éloigné|officielle|
|[RmNG2smIjmn2isvO.htm](age-of-ashes-bestiary-items/RmNG2smIjmn2isvO.htm)|Deny Advantage|Refus d'avantage|officielle|
|[RNHnYYn5s0onufID.htm](age-of-ashes-bestiary-items/RNHnYYn5s0onufID.htm)|Crafting|Artisanat|officielle|
|[rNS9IXUZDALDQoiJ.htm](age-of-ashes-bestiary-items/rNS9IXUZDALDQoiJ.htm)|Dagger|cold-iron|Dague en fer froid|libre|
|[rOBeT7cNsBIJSzT7.htm](age-of-ashes-bestiary-items/rOBeT7cNsBIJSzT7.htm)|Cleric Domain Spells|Sorts de domaine de prêtre|libre|
|[rqq8NjCeDPoInPTq.htm](age-of-ashes-bestiary-items/rqq8NjCeDPoInPTq.htm)|Arcane Innate Spells|Sorts arcaniques innés|libre|
|[rRrTo5REtGDduBlv.htm](age-of-ashes-bestiary-items/rRrTo5REtGDduBlv.htm)|Fangs|Crocs|officielle|
|[rU9gIiMETSDDnipL.htm](age-of-ashes-bestiary-items/rU9gIiMETSDDnipL.htm)|Tail|Queue|officielle|
|[rUMDjgQAaue8rA2w.htm](age-of-ashes-bestiary-items/rUMDjgQAaue8rA2w.htm)|Regeneration 15 (Deactivated by Good)|Régénération 15 (désactivée par Bon)|officielle|
|[rVWeTEhRpSWDcpVJ.htm](age-of-ashes-bestiary-items/rVWeTEhRpSWDcpVJ.htm)|Spellbook|Grimoire|officielle|
|[RwCa647AJFm8BWJR.htm](age-of-ashes-bestiary-items/RwCa647AJFm8BWJR.htm)|Darkvision|Vision dans le noir|officielle|
|[rWg63VpAt6JzaV7z.htm](age-of-ashes-bestiary-items/rWg63VpAt6JzaV7z.htm)|Wing|Aile|officielle|
|[RwOAEo9R3fFzTJY7.htm](age-of-ashes-bestiary-items/RwOAEo9R3fFzTJY7.htm)|Surprise Attack|Attaque surprise|officielle|
|[ryLB6NeDkc2PFBmb.htm](age-of-ashes-bestiary-items/ryLB6NeDkc2PFBmb.htm)|Carapace|Carapace|officielle|
|[rZ3NKdEarhgXFXmc.htm](age-of-ashes-bestiary-items/rZ3NKdEarhgXFXmc.htm)|Catch Rock|Interception de rocher|officielle|
|[rzanS9lRHYe4hZQt.htm](age-of-ashes-bestiary-items/rzanS9lRHYe4hZQt.htm)|Sailing Lore|Connaissance de la navigation maritime|officielle|
|[RzjCNg6HpRoCBHoz.htm](age-of-ashes-bestiary-items/RzjCNg6HpRoCBHoz.htm)|Jungle Stride|Déplacement facilité dans la jungle|officielle|
|[rzMtRbnW5Hj2wyGz.htm](age-of-ashes-bestiary-items/rzMtRbnW5Hj2wyGz.htm)|Manacles (Good) (Marked with the Symbol of the Scarlet Triad)|Menottes (bonnes) (marquées du symbole de la Triade écarlate)|officielle|
|[S16umnQkXhv237qK.htm](age-of-ashes-bestiary-items/S16umnQkXhv237qK.htm)|Key to Manacles|Clé des menottes|officielle|
|[s2CoTcNYzppH89ej.htm](age-of-ashes-bestiary-items/s2CoTcNYzppH89ej.htm)|Cinderclaw Gauntlet|Gantelet de griffe de braise|officielle|
|[S5Inn9EP3p8RwITB.htm](age-of-ashes-bestiary-items/S5Inn9EP3p8RwITB.htm)|Warded Casting|Incantation protégée|officielle|
|[s5L6PEZRqvHlBdWg.htm](age-of-ashes-bestiary-items/s5L6PEZRqvHlBdWg.htm)|Rejuvenation|Reconstruction|officielle|
|[S7uX0GFBE6GEwURU.htm](age-of-ashes-bestiary-items/S7uX0GFBE6GEwURU.htm)|Dragon Lore|Connaissance des dragons|officielle|
|[s9Tsu9PDPlPpfeYQ.htm](age-of-ashes-bestiary-items/s9Tsu9PDPlPpfeYQ.htm)|Torture Lore|Connaissance de la torture|officielle|
|[sbEfGLC90nUBe7FX.htm](age-of-ashes-bestiary-items/sbEfGLC90nUBe7FX.htm)|Weapon of Judgement|Arme du jugement|officielle|
|[sBHiaipaEIdoNG9q.htm](age-of-ashes-bestiary-items/sBHiaipaEIdoNG9q.htm)|Telekinetic Storm|Tempête télékinésique|officielle|
|[SBQifbHhjnwomydG.htm](age-of-ashes-bestiary-items/SBQifbHhjnwomydG.htm)|Darkvision|Vision dans le noir|officielle|
|[sc3daf0IgUPTb2rM.htm](age-of-ashes-bestiary-items/sc3daf0IgUPTb2rM.htm)|Greater Frost Vial|Fiole de givre supérieure imprégnée|officielle|
|[SdtmvPS7LAUlE41O.htm](age-of-ashes-bestiary-items/SdtmvPS7LAUlE41O.htm)|Swamp Stride|Déplacement facilité dans les marais|officielle|
|[SduZ57vCDQWA59rW.htm](age-of-ashes-bestiary-items/SduZ57vCDQWA59rW.htm)|Athletics|Athlétisme|officielle|
|[seHBkvEFgGTlfAtt.htm](age-of-ashes-bestiary-items/seHBkvEFgGTlfAtt.htm)|Dagger|Dague|officielle|
|[sek6swVu6eFYssLv.htm](age-of-ashes-bestiary-items/sek6swVu6eFYssLv.htm)|Key to Chest|Clé du coffre|officielle|
|[sEn2RRTxyiFjpqfu.htm](age-of-ashes-bestiary-items/sEn2RRTxyiFjpqfu.htm)|Resonant Wail|Plainte résonnante|officielle|
|[SH0nk4BfgI1ur3oJ.htm](age-of-ashes-bestiary-items/SH0nk4BfgI1ur3oJ.htm)|Unbalancing Blow|Coup déséquilibrant|officielle|
|[SH5975wJdafMupdy.htm](age-of-ashes-bestiary-items/SH5975wJdafMupdy.htm)|Rejuvenation|Reconstruction|officielle|
|[shtsWYCyenHOUURJ.htm](age-of-ashes-bestiary-items/shtsWYCyenHOUURJ.htm)|Primal Rituals|Rituels primordiaux|libre|
|[SHZi101TqFOYWlIL.htm](age-of-ashes-bestiary-items/SHZi101TqFOYWlIL.htm)|Green Eye Beam|Rayon oculaire vert|officielle|
|[SJg4gN7fByvyyHi4.htm](age-of-ashes-bestiary-items/SJg4gN7fByvyyHi4.htm)|Sneak Attack|Attaque sournoise|officielle|
|[sjs5I4Em4t7kFOwK.htm](age-of-ashes-bestiary-items/sjs5I4Em4t7kFOwK.htm)|Violet Eye Beam|Rayon oculaire violet|officielle|
|[SJvjp5tv9lMwcTyw.htm](age-of-ashes-bestiary-items/SJvjp5tv9lMwcTyw.htm)|Weapon Trick|Astuces martiales|officielle|
|[sKH86qpUBKLIW8ls.htm](age-of-ashes-bestiary-items/sKH86qpUBKLIW8ls.htm)|Efficient Capture|Capture efficace|officielle|
|[skQ2ydydslD2F8Br.htm](age-of-ashes-bestiary-items/skQ2ydydslD2F8Br.htm)|Fist|Poing|officielle|
|[SkzgfQVojElPeAh6.htm](age-of-ashes-bestiary-items/SkzgfQVojElPeAh6.htm)|Breath Weapon|Souffle|officielle|
|[sLKipq0eV2QIdP0P.htm](age-of-ashes-bestiary-items/sLKipq0eV2QIdP0P.htm)|Consume Flesh|Dévorer la chair|officielle|
|[sLQqwa6hoaw69U93.htm](age-of-ashes-bestiary-items/sLQqwa6hoaw69U93.htm)|Drain Bonded Item|Drain d'objet lié|officielle|
|[SlVqql8huNGs1yPM.htm](age-of-ashes-bestiary-items/SlVqql8huNGs1yPM.htm)|Horns|Corne|officielle|
|[SmEfn9FnR2kxlXyr.htm](age-of-ashes-bestiary-items/SmEfn9FnR2kxlXyr.htm)|Sneak Attack|Attaque sournoise|officielle|
|[SNejKUlOFhtJWGTZ.htm](age-of-ashes-bestiary-items/SNejKUlOFhtJWGTZ.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[Sohj5I5HF2aiJ17e.htm](age-of-ashes-bestiary-items/Sohj5I5HF2aiJ17e.htm)|Yellow Eye Beam|Rayon oculaire jaune|officielle|
|[sQoyJLg3FlAvbzAa.htm](age-of-ashes-bestiary-items/sQoyJLg3FlAvbzAa.htm)|+1 Leather Armor|Armure en cuir +1|officielle|
|[sQsoukNeadSMEKWS.htm](age-of-ashes-bestiary-items/sQsoukNeadSMEKWS.htm)|Beak|Bec|officielle|
|[sRUKk7rcYO0y4CrH.htm](age-of-ashes-bestiary-items/sRUKk7rcYO0y4CrH.htm)|Razor Sharp|Coupant comme un rasoir|officielle|
|[Supu8qO5sZ5gfG4U.htm](age-of-ashes-bestiary-items/Supu8qO5sZ5gfG4U.htm)|Claw|Griffe|officielle|
|[SV6WrRtaq5OJtKUq.htm](age-of-ashes-bestiary-items/SV6WrRtaq5OJtKUq.htm)|Hampering Shot|Tir ralentissant|officielle|
|[SW6CqLFVtIpxlZcy.htm](age-of-ashes-bestiary-items/SW6CqLFVtIpxlZcy.htm)|Major Acid Flask|Fiole d'acide majeure|officielle|
|[SxjIbhY3Md3bS3dx.htm](age-of-ashes-bestiary-items/SxjIbhY3Md3bS3dx.htm)|Morningstar|Morgenstern|officielle|
|[sXWZxvJlU7LZrpHC.htm](age-of-ashes-bestiary-items/sXWZxvJlU7LZrpHC.htm)|Orange Eye Beam|Rayon oculaire orange|officielle|
|[t00h5GlGA9LELf56.htm](age-of-ashes-bestiary-items/t00h5GlGA9LELf56.htm)|Soul Chain|Chaîne d'âme|officielle|
|[T1MUgKrMJnC4CnSS.htm](age-of-ashes-bestiary-items/T1MUgKrMJnC4CnSS.htm)|Crafting|Artisanat|officielle|
|[T1sHFc7JoaRRC0iK.htm](age-of-ashes-bestiary-items/T1sHFc7JoaRRC0iK.htm)|Claw|Griffe|officielle|
|[T3t1bWcAoYCVuiiE.htm](age-of-ashes-bestiary-items/T3t1bWcAoYCVuiiE.htm)|Drain Soul Cage|Canaliser le phylactère|officielle|
|[t5AqMVMcvrvoUCcN.htm](age-of-ashes-bestiary-items/t5AqMVMcvrvoUCcN.htm)|Necrotic Affliction|Affliction nécrotique|officielle|
|[T82SzULUe8LH0NIZ.htm](age-of-ashes-bestiary-items/T82SzULUe8LH0NIZ.htm)|Split|Scission|officielle|
|[TBCoGqPy0fhFmpMq.htm](age-of-ashes-bestiary-items/TBCoGqPy0fhFmpMq.htm)|Twin Takedown|Agression jumelée|officielle|
|[tBIBeHhWxRinsTO5.htm](age-of-ashes-bestiary-items/tBIBeHhWxRinsTO5.htm)|Soul Binder|Emprisonnement d'âme|officielle|
|[TbMZhJ1evBpaauR7.htm](age-of-ashes-bestiary-items/TbMZhJ1evBpaauR7.htm)|Create Shadow Sanctuary|Sanctuaire des ombres|officielle|
|[tCcBQS2TNgo7EIvB.htm](age-of-ashes-bestiary-items/tCcBQS2TNgo7EIvB.htm)|Efficient Capture|Capture efficace|officielle|
|[tdbB9sBzDpFxgzUz.htm](age-of-ashes-bestiary-items/tdbB9sBzDpFxgzUz.htm)|Darkvision|Vision dans le noir|officielle|
|[TF94bqW7ak7QdxKX.htm](age-of-ashes-bestiary-items/TF94bqW7ak7QdxKX.htm)|Efficient Capture|Capture efficace|officielle|
|[tfAQ1l4sV2c1psLD.htm](age-of-ashes-bestiary-items/tfAQ1l4sV2c1psLD.htm)|Precise Debilitations|Handicaps précis|officielle|
|[Thxxg6IpvoOEOFZu.htm](age-of-ashes-bestiary-items/Thxxg6IpvoOEOFZu.htm)|Scoff at the Divine|Gausserie du divin|officielle|
|[tHzRquAwQcwKEsgJ.htm](age-of-ashes-bestiary-items/tHzRquAwQcwKEsgJ.htm)|Superior Pack Attack|Attaque de meute supérieure|officielle|
|[TIhbTdeTcedCd2mp.htm](age-of-ashes-bestiary-items/TIhbTdeTcedCd2mp.htm)|Low-Light Vision|Vision nocturne|officielle|
|[TJEWisVqlHMJM2xw.htm](age-of-ashes-bestiary-items/TJEWisVqlHMJM2xw.htm)|Telekinetic Haul (At Will)|Transport télékinésique (À volonté)|officielle|
|[TJWwRY8dDZB5iZvQ.htm](age-of-ashes-bestiary-items/TJWwRY8dDZB5iZvQ.htm)|Eerie Flexibility|Souplesse surnaturelle|officielle|
|[tKTardJEDrHVNygR.htm](age-of-ashes-bestiary-items/tKTardJEDrHVNygR.htm)|Darkvision|Vision dans le noir|officielle|
|[tl2R3rnbspKfWaNX.htm](age-of-ashes-bestiary-items/tl2R3rnbspKfWaNX.htm)|Darkvision|Vision dans le noir|officielle|
|[tmioSSIeBsfcu9eY.htm](age-of-ashes-bestiary-items/tmioSSIeBsfcu9eY.htm)|5-6 Bolts of Lightning|5–6 Éclairs|officielle|
|[To3ttdceko61OtLl.htm](age-of-ashes-bestiary-items/To3ttdceko61OtLl.htm)|Breath Weapon|Souffle|officielle|
|[TOo0Vq8jXzgZHSGq.htm](age-of-ashes-bestiary-items/TOo0Vq8jXzgZHSGq.htm)|At-Will Spells|Sorts à volonté|officielle|
|[TPKOsROi9hAsKuHY.htm](age-of-ashes-bestiary-items/TPKOsROi9hAsKuHY.htm)|Composite Shortbow|+1|Arc court composite +1|officielle|
|[TpYZfRAOyWTWC5du.htm](age-of-ashes-bestiary-items/TpYZfRAOyWTWC5du.htm)|Quick Movements|Déplacements rapides|officielle|
|[TsPUS6z43vjlNNkp.htm](age-of-ashes-bestiary-items/TsPUS6z43vjlNNkp.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[tSQ2I9ViRu4ocrSQ.htm](age-of-ashes-bestiary-items/tSQ2I9ViRu4ocrSQ.htm)|Summon Fiend (Hell Hound Only)|Convocation de fiélon (molosse infernal uniquement)|officielle|
|[TSSV8a0oKTioJrDT.htm](age-of-ashes-bestiary-items/TSSV8a0oKTioJrDT.htm)|Spectral Fist|Poing spectral|officielle|
|[TTHC9vPRacUfSJco.htm](age-of-ashes-bestiary-items/TTHC9vPRacUfSJco.htm)|Crystallize|Cristalliser|officielle|
|[TU1DbG3ubOh1bDcw.htm](age-of-ashes-bestiary-items/TU1DbG3ubOh1bDcw.htm)|Composite Shortbow|Arc court composite|officielle|
|[TupueOehWDuKS2G5.htm](age-of-ashes-bestiary-items/TupueOehWDuKS2G5.htm)|Shortbow|Arc court|officielle|
|[tvo7UaALlpq5xWzP.htm](age-of-ashes-bestiary-items/tvo7UaALlpq5xWzP.htm)|Halfling Luck|Chance halfeline|officielle|
|[tvWQ8FsujbpIgBJg.htm](age-of-ashes-bestiary-items/tvWQ8FsujbpIgBJg.htm)|Mining Lore|Connaissance minière|officielle|
|[TW4r3ip5ObjvYZMs.htm](age-of-ashes-bestiary-items/TW4r3ip5ObjvYZMs.htm)|Shield Spikes|Pointes de bouclier|officielle|
|[txRGabDLLfoByLm2.htm](age-of-ashes-bestiary-items/txRGabDLLfoByLm2.htm)|Thundering Maul|Maillet de tonnerre|officielle|
|[TZaSvXMYX3TRNNtO.htm](age-of-ashes-bestiary-items/TZaSvXMYX3TRNNtO.htm)|Warhammer|+2,greaterStriking,greaterFlaming|Marteau de guerre enflammé supérieur de frappe supérieure +2|officielle|
|[u3F1k0BPoP05Xfzn.htm](age-of-ashes-bestiary-items/u3F1k0BPoP05Xfzn.htm)|Scent (Imprecise) 120 feet|Odorat 36 m (imprécis)|officielle|
|[U62hbZDWfFSaAj64.htm](age-of-ashes-bestiary-items/U62hbZDWfFSaAj64.htm)|Smoke Vision|Vision malgré la fumée|officielle|
|[U6Wwg69lSYJAW46L.htm](age-of-ashes-bestiary-items/U6Wwg69lSYJAW46L.htm)|Constant Spells|Sorts constants|officielle|
|[uapfpdQSjUz13nBn.htm](age-of-ashes-bestiary-items/uapfpdQSjUz13nBn.htm)|Eye Beam|Rayon oculaire|officielle|
|[UasVzRT5NmsYorFO.htm](age-of-ashes-bestiary-items/UasVzRT5NmsYorFO.htm)|Invisibility (Self Only)|Invisibilité (soi uniquement)|officielle|
|[uAtJgY4Z2GeiZYOi.htm](age-of-ashes-bestiary-items/uAtJgY4Z2GeiZYOi.htm)|No MAP|Pas de PAM|libre|
|[uBfa75DofLNXMiso.htm](age-of-ashes-bestiary-items/uBfa75DofLNXMiso.htm)|Manacles (Average) (Marked with the Symbol of the Scarlet Triad)|Menottes (moyennes) (marquées du symbole de la Triade écarlate)|officielle|
|[ubsRZqqQfGfMpXdi.htm](age-of-ashes-bestiary-items/ubsRZqqQfGfMpXdi.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[UCFuIxxWMMjN84Rx.htm](age-of-ashes-bestiary-items/UCFuIxxWMMjN84Rx.htm)|Kovlar Lore|Connaissance de Kovlar|officielle|
|[UcUzuyj7o64QhvCg.htm](age-of-ashes-bestiary-items/UcUzuyj7o64QhvCg.htm)|Dagger|Dague|officielle|
|[UDRLkUU6I38waiLi.htm](age-of-ashes-bestiary-items/UDRLkUU6I38waiLi.htm)|Tail Swipe|Coup de queue|officielle|
|[UDx53YecyYB7KIrJ.htm](age-of-ashes-bestiary-items/UDx53YecyYB7KIrJ.htm)|Anadi Venom|Venin d'anadi|officielle|
|[uf0REXRCA5Os0He4.htm](age-of-ashes-bestiary-items/uf0REXRCA5Os0He4.htm)|Claw|Griffe|officielle|
|[Uf6zmBkqRIkAQTiY.htm](age-of-ashes-bestiary-items/Uf6zmBkqRIkAQTiY.htm)|Claw|Griffe|officielle|
|[UfanUBQ8Zs0ulv4e.htm](age-of-ashes-bestiary-items/UfanUBQ8Zs0ulv4e.htm)|Frightful Presence|Présence terrifiante|officielle|
|[UGsIrubFW2RiOSNY.htm](age-of-ashes-bestiary-items/UGsIrubFW2RiOSNY.htm)|Composite Longbow|+2,striking|Arc long composite de frappe +2|libre|
|[UhafuYzXLYPCHxPD.htm](age-of-ashes-bestiary-items/UhafuYzXLYPCHxPD.htm)|Low-Light Vision|Vision nocturne|officielle|
|[UicWUJ68tR3WgGLD.htm](age-of-ashes-bestiary-items/UicWUJ68tR3WgGLD.htm)|Grapeshot|Plombs|officielle|
|[uIFB2aU5gYZC7XXL.htm](age-of-ashes-bestiary-items/uIFB2aU5gYZC7XXL.htm)|Innate Arcane Spells|Sorts arcaniques innés|libre|
|[uiKPE6MQz8FAQvOx.htm](age-of-ashes-bestiary-items/uiKPE6MQz8FAQvOx.htm)|Shield Warden|Gardien au bouclier|officielle|
|[UIvPyDXGpNjhmqBZ.htm](age-of-ashes-bestiary-items/UIvPyDXGpNjhmqBZ.htm)|Hatchet|Hachette|officielle|
|[UKJ4sJuhWqcOOeYg.htm](age-of-ashes-bestiary-items/UKJ4sJuhWqcOOeYg.htm)|+2 Greater Resilient Chain Mail|Cotte de mailles de résilience supérieure +2|officielle|
|[ulyeBuv2PkM39FKm.htm](age-of-ashes-bestiary-items/ulyeBuv2PkM39FKm.htm)|Arcane Innate Spells|Sorts arcaniques innés|libre|
|[umJV0GGKKxt5cyGw.htm](age-of-ashes-bestiary-items/umJV0GGKKxt5cyGw.htm)|Scent (Imprecise) 60 feet|Odorat 18 m (imprécis)|officielle|
|[UOfQIWygJYcqT1kQ.htm](age-of-ashes-bestiary-items/UOfQIWygJYcqT1kQ.htm)|Mobility|Mobilité|officielle|
|[UQaty56jSFbPq0pG.htm](age-of-ashes-bestiary-items/UQaty56jSFbPq0pG.htm)|Bite|Morsure|officielle|
|[UQInVEZyWsihDZJI.htm](age-of-ashes-bestiary-items/UQInVEZyWsihDZJI.htm)|Drain Bonded Item|Drain d'objet lié|officielle|
|[UQnCKGq6QIQn8xKq.htm](age-of-ashes-bestiary-items/UQnCKGq6QIQn8xKq.htm)|Greatsword|Épée à deux mains|officielle|
|[ur5ordRthB6Bqyei.htm](age-of-ashes-bestiary-items/ur5ordRthB6Bqyei.htm)|Grab|Empoignade|officielle|
|[USQ16Z86qZYqJDkV.htm](age-of-ashes-bestiary-items/USQ16Z86qZYqJDkV.htm)|Persistent Bleed Critical|Saignement sur coup critique|officielle|
|[Ut7DWly2y0Fghbxh.htm](age-of-ashes-bestiary-items/Ut7DWly2y0Fghbxh.htm)|Corrupt Ally|Corruption d'allié|officielle|
|[UuBYVwcGmnkDWnT3.htm](age-of-ashes-bestiary-items/UuBYVwcGmnkDWnT3.htm)|+1 Dagger (Bonded Item)|+1|Dague +1 (objet lié)|officielle|
|[uufsa9Y9z4IZhCZb.htm](age-of-ashes-bestiary-items/uufsa9Y9z4IZhCZb.htm)|Keen Eyes|Yeux perçants|officielle|
|[uUTQ7Z3rao6lhQYz.htm](age-of-ashes-bestiary-items/uUTQ7Z3rao6lhQYz.htm)|History of the Bumblebrashers in Isger|Histoire des Impétueux-bafouilleurs en Isger|officielle|
|[UwdvzOviNTex0Xqb.htm](age-of-ashes-bestiary-items/UwdvzOviNTex0Xqb.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[UxtBtayqxXB5mZip.htm](age-of-ashes-bestiary-items/UxtBtayqxXB5mZip.htm)|Bully's Push|Bousculade de brute|officielle|
|[uZATYiIAsLS32uLU.htm](age-of-ashes-bestiary-items/uZATYiIAsLS32uLU.htm)|+2 Resilient Adamantine Breastplate|Cuirasse en adamantium de résilience +2|officielle|
|[V02rKZLk4zOrx65r.htm](age-of-ashes-bestiary-items/V02rKZLk4zOrx65r.htm)|Thrown Debris|Projection de débris|officielle|
|[v0Bn1s1wRHKj8MGA.htm](age-of-ashes-bestiary-items/v0Bn1s1wRHKj8MGA.htm)|Lazurite-Infused Flesh|Chair imprégnée de lazurite|officielle|
|[v1DFg4W547LdWp1p.htm](age-of-ashes-bestiary-items/v1DFg4W547LdWp1p.htm)|Pummeling Flurry|Rouer de coups|officielle|
|[V2OOVzLbdPKRulIf.htm](age-of-ashes-bestiary-items/V2OOVzLbdPKRulIf.htm)|Adhesive Body|Corps adhésif|officielle|
|[v2P0c4rmwI5BoEBT.htm](age-of-ashes-bestiary-items/v2P0c4rmwI5BoEBT.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[v3tty7UlOB215KUf.htm](age-of-ashes-bestiary-items/v3tty7UlOB215KUf.htm)|Enlarge (Self Only)|Agrandissement (soi uniquement)|officielle|
|[v5JDDS5HftErPvPh.htm](age-of-ashes-bestiary-items/v5JDDS5HftErPvPh.htm)|Opened Sluices|Ouverture des écluses|officielle|
|[V6HMMbCm0RtvxhNC.htm](age-of-ashes-bestiary-items/V6HMMbCm0RtvxhNC.htm)|Claw|Griffe|officielle|
|[V7VvhfarrGzmaDnf.htm](age-of-ashes-bestiary-items/V7VvhfarrGzmaDnf.htm)|Bite|Morsure|officielle|
|[v8QaZXPq7Yj9K2v1.htm](age-of-ashes-bestiary-items/v8QaZXPq7Yj9K2v1.htm)|Constant Spells|Sorts constants|officielle|
|[vaRLfCxd4TnmCYFX.htm](age-of-ashes-bestiary-items/vaRLfCxd4TnmCYFX.htm)|Droskar Lore|Connaissance de Droskar|officielle|
|[VbtHhiKtz4qjoR6z.htm](age-of-ashes-bestiary-items/VbtHhiKtz4qjoR6z.htm)|Stealth|Discrétion|officielle|
|[vCxz30plLPffDrlE.htm](age-of-ashes-bestiary-items/vCxz30plLPffDrlE.htm)|Wavesense (Imprecise) 30 feet|Perception des ondes 9 m (imprécis)|officielle|
|[VDCmacyFJeeLLjxL.htm](age-of-ashes-bestiary-items/VDCmacyFJeeLLjxL.htm)|Incredible Initiative|Initiative extraordinaire|officielle|
|[veIehkJyX5KsiJzn.htm](age-of-ashes-bestiary-items/veIehkJyX5KsiJzn.htm)|Hunter's Flurry|Déluge du chasseur|officielle|
|[veigzkgmikNe9iEL.htm](age-of-ashes-bestiary-items/veigzkgmikNe9iEL.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[vEOKWwRMGGUetHuk.htm](age-of-ashes-bestiary-items/vEOKWwRMGGUetHuk.htm)|Telepathy 100 feet|Télépathie 30 m|officielle|
|[vevNfOyFN6QRJtR2.htm](age-of-ashes-bestiary-items/vevNfOyFN6QRJtR2.htm)|At-Will Spells|Sorts à volonté|officielle|
|[vfDhFBTrynIBoAmF.htm](age-of-ashes-bestiary-items/vfDhFBTrynIBoAmF.htm)|Prepared Divine Spells|Sorts divins préparés|libre|
|[VGLGaYgY8xICa8nV.htm](age-of-ashes-bestiary-items/VGLGaYgY8xICa8nV.htm)|Improved Grab|Empoignade améliorée|officielle|
|[VHTjtjDkxGqmFbdd.htm](age-of-ashes-bestiary-items/VHTjtjDkxGqmFbdd.htm)|Dagger|Dague|officielle|
|[VihElpjLJNASMTWo.htm](age-of-ashes-bestiary-items/VihElpjLJNASMTWo.htm)|Nolly's Hoe (+1 Striking Anarchic Halfling Sling Staff)|+1,striking,anarchic|Houe de Nolly (fustibale halfeline anarchique de frappe +1)|officielle|
|[vJ1d2ivm8JgAi3ET.htm](age-of-ashes-bestiary-items/vJ1d2ivm8JgAi3ET.htm)|Hatchet|Hachette|officielle|
|[VJbm5TGMwUCws6K1.htm](age-of-ashes-bestiary-items/VJbm5TGMwUCws6K1.htm)|Longsword|Épée longue|officielle|
|[VkjtpvreAQ1v0h8Z.htm](age-of-ashes-bestiary-items/VkjtpvreAQ1v0h8Z.htm)|Hatchet|Hachette|officielle|
|[VlCgM24yW7fJGoEX.htm](age-of-ashes-bestiary-items/VlCgM24yW7fJGoEX.htm)|+2 Status to All Saves vs. Magic|+2 de statut aux sauvegardes contre la magie|officielle|
|[vlFbh7Px7CtFWTTe.htm](age-of-ashes-bestiary-items/vlFbh7Px7CtFWTTe.htm)|Divine Prepared Spells|Sorts divins préparés|libre|
|[vM66vkQqjowc47rC.htm](age-of-ashes-bestiary-items/vM66vkQqjowc47rC.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[vmOhdTHRiDnoF4LK.htm](age-of-ashes-bestiary-items/vmOhdTHRiDnoF4LK.htm)|Eye Beam|Rayon oculaire|officielle|
|[vMtSSRTHSKwtSssM.htm](age-of-ashes-bestiary-items/vMtSSRTHSKwtSssM.htm)|Ekujae Lore|Connaissance des Ekujaes|officielle|
|[VNd49RdCdrZvopZ3.htm](age-of-ashes-bestiary-items/VNd49RdCdrZvopZ3.htm)|Motion Sense|Perception du mouvement|officielle|
|[vnegbgEyYoSvmXKq.htm](age-of-ashes-bestiary-items/vnegbgEyYoSvmXKq.htm)|Religious Symbol of Dahak (Gold)|Symbole religieux en or de Dahak|officielle|
|[VO094UobV7Jjl6sN.htm](age-of-ashes-bestiary-items/VO094UobV7Jjl6sN.htm)|Longbow|+1|Arc long +1|libre|
|[vPx7Kv8CDnL5rd3e.htm](age-of-ashes-bestiary-items/vPx7Kv8CDnL5rd3e.htm)|Warhammer|Marteau de guerre|officielle|
|[VqMcIUne6jfbwvel.htm](age-of-ashes-bestiary-items/VqMcIUne6jfbwvel.htm)|Blood Magic|Magie du sang|officielle|
|[vreeTrUJqAchFZUM.htm](age-of-ashes-bestiary-items/vreeTrUJqAchFZUM.htm)|+2 Circumstance to All Saves vs. Shove, Trip, or Being Knocked Prone|+2 de circonstances aux sauvegardes contre Pousser, Croc-en-jambe ou jeter à terre|officielle|
|[vt4zVYNHBbCrRw9A.htm](age-of-ashes-bestiary-items/vt4zVYNHBbCrRw9A.htm)|At-Will Spells|Sorts à volonté|officielle|
|[VTvR3zWnwVcxzIwl.htm](age-of-ashes-bestiary-items/VTvR3zWnwVcxzIwl.htm)|Improved Grab|Empoignade améliorée|officielle|
|[vxAKcJqC8kzmxb4t.htm](age-of-ashes-bestiary-items/vxAKcJqC8kzmxb4t.htm)|Darkvision|Vision dans le noir|officielle|
|[vXOWe8El9JVygflC.htm](age-of-ashes-bestiary-items/vXOWe8El9JVygflC.htm)|Greatclub|+1,striking|Massue de frappe +1|libre|
|[vYOEAucH84uYzG3z.htm](age-of-ashes-bestiary-items/vYOEAucH84uYzG3z.htm)|Darkvision|Vision dans le noir|officielle|
|[VzUnnud6z8Zj4DTC.htm](age-of-ashes-bestiary-items/VzUnnud6z8Zj4DTC.htm)|Scimitar|Cimeterre|officielle|
|[W0905p3wQrOz7E2x.htm](age-of-ashes-bestiary-items/W0905p3wQrOz7E2x.htm)|Shield Block|Blocage au bouclier|officielle|
|[w1d8C43oMLZdCVjG.htm](age-of-ashes-bestiary-items/w1d8C43oMLZdCVjG.htm)|Rapier|+1|Rapière +1|libre|
|[W24hKhRcyY9H8AQZ.htm](age-of-ashes-bestiary-items/W24hKhRcyY9H8AQZ.htm)|Formula Book|Livre de formules|officielle|
|[w40QDydlY3kEtlQE.htm](age-of-ashes-bestiary-items/w40QDydlY3kEtlQE.htm)|Rain of Arrows|Déluge de flèches|officielle|
|[w481TRk2bURCXIxx.htm](age-of-ashes-bestiary-items/w481TRk2bURCXIxx.htm)|Coven|Cercle|officielle|
|[w5Yk889pfRGklcLm.htm](age-of-ashes-bestiary-items/w5Yk889pfRGklcLm.htm)|Violet Eye Beam|Rayon oculaire violet|officielle|
|[W6f4zhENM79wqemW.htm](age-of-ashes-bestiary-items/W6f4zhENM79wqemW.htm)|Tail|Queue|officielle|
|[wb5jOSkiZA5lYAX9.htm](age-of-ashes-bestiary-items/wb5jOSkiZA5lYAX9.htm)|Destructive Strikes|Frappes destructrices|officielle|
|[wBZt1NPsZgzDeD2m.htm](age-of-ashes-bestiary-items/wBZt1NPsZgzDeD2m.htm)|Expanded Splash|Éclaboussure élargie|officielle|
|[WDdm6RT885xA9FVZ.htm](age-of-ashes-bestiary-items/WDdm6RT885xA9FVZ.htm)|Kintargo Lore|Connaissance de Kintargo|officielle|
|[wDnuOjox5cjI817l.htm](age-of-ashes-bestiary-items/wDnuOjox5cjI817l.htm)|Immortality|Immortalité|officielle|
|[wDV5VP9FUeUWN0DC.htm](age-of-ashes-bestiary-items/wDV5VP9FUeUWN0DC.htm)|Shortsword|+1|Épée courte +1|officielle|
|[wHfHFuiq0qNw7Q0J.htm](age-of-ashes-bestiary-items/wHfHFuiq0qNw7Q0J.htm)|Vulnerability to Exorcism|Vulnérabilité aux exorcismes|officielle|
|[whQM5kvewGUXnnLe.htm](age-of-ashes-bestiary-items/whQM5kvewGUXnnLe.htm)|Anguished Shriek|Hurlement d'angoisse|officielle|
|[wHrD91zmg0bTBIc1.htm](age-of-ashes-bestiary-items/wHrD91zmg0bTBIc1.htm)|Command (At Will)|Injonction (À volonté)|officielle|
|[WIq9iFyHE2YJoFwA.htm](age-of-ashes-bestiary-items/WIq9iFyHE2YJoFwA.htm)|Claw|Griffe|officielle|
|[wkS0gm33Vs1iiRST.htm](age-of-ashes-bestiary-items/wkS0gm33Vs1iiRST.htm)|Dream Haunting|Hanter les rêves|officielle|
|[wlSd81ltH4W9B3X7.htm](age-of-ashes-bestiary-items/wlSd81ltH4W9B3X7.htm)|Slaver Lore|Connaissance de l'esclavagisme|officielle|
|[wmpfvEE1W9Sses2l.htm](age-of-ashes-bestiary-items/wmpfvEE1W9Sses2l.htm)|Bark Orders|Aboyer des ordres|officielle|
|[wNbZrLNPqxS1Uudq.htm](age-of-ashes-bestiary-items/wNbZrLNPqxS1Uudq.htm)|Sap|+1,striking|Matraque de frappe +1|libre|
|[wnE8aQGkQtI87toJ.htm](age-of-ashes-bestiary-items/wnE8aQGkQtI87toJ.htm)|Darkvision|Vision dans le noir|officielle|
|[Wo5Z8Jix5ohz1Urx.htm](age-of-ashes-bestiary-items/Wo5Z8Jix5ohz1Urx.htm)|Rock Dwarf|Nain des roches|officielle|
|[wOCajMqUzhODb2NB.htm](age-of-ashes-bestiary-items/wOCajMqUzhODb2NB.htm)|Steady Spellcasting|Incantation fiable|officielle|
|[WonIEfwUndYFXxyq.htm](age-of-ashes-bestiary-items/WonIEfwUndYFXxyq.htm)|Thrown Debris|Projection de débris|officielle|
|[WpLmYPgmK0qG58K8.htm](age-of-ashes-bestiary-items/WpLmYPgmK0qG58K8.htm)|Pseudopod|Pseudopode|officielle|
|[wqr4H9kTRu8PWkfc.htm](age-of-ashes-bestiary-items/wqr4H9kTRu8PWkfc.htm)|Tongue Grab|Agrippement avec la langue|libre|
|[wrMmoUVK16gf58er.htm](age-of-ashes-bestiary-items/wrMmoUVK16gf58er.htm)|Acrobatics|Acrobaties|officielle|
|[wSkREkhq1FagvrHD.htm](age-of-ashes-bestiary-items/wSkREkhq1FagvrHD.htm)|Arcane Prepared Spells|Sorts arcaniques préparés|libre|
|[WslAyAlavrAXyhAt.htm](age-of-ashes-bestiary-items/WslAyAlavrAXyhAt.htm)|Syringe Filled with Unstable Mutagen|Seringue remplie de mutagène instable|officielle|
|[WsUJjMncNN9DWybk.htm](age-of-ashes-bestiary-items/WsUJjMncNN9DWybk.htm)|Frightful Presence|Présence terrifiante|officielle|
|[wt7Ds9r5d3F2v34T.htm](age-of-ashes-bestiary-items/wt7Ds9r5d3F2v34T.htm)|Draconic Momentum|Impulsion draconique|officielle|
|[wTrhi9PyRoPxh06i.htm](age-of-ashes-bestiary-items/wTrhi9PyRoPxh06i.htm)|Burning Mace|Masse enflammée|officielle|
|[WugSxYfeO8nxDg3a.htm](age-of-ashes-bestiary-items/WugSxYfeO8nxDg3a.htm)|Religious Symbol of Desna|Symbole religieux de Desna|officielle|
|[WuZqaLIPaZyuIDMD.htm](age-of-ashes-bestiary-items/WuZqaLIPaZyuIDMD.htm)|Key to Manacles|Clé des menottes|officielle|
|[WvSXMmKbLjsbglaD.htm](age-of-ashes-bestiary-items/WvSXMmKbLjsbglaD.htm)|Glimpse of Redemption|Lueur de rédemption|officielle|
|[wwajiyf5MX0aRcgm.htm](age-of-ashes-bestiary-items/wwajiyf5MX0aRcgm.htm)|Tail|Queue|officielle|
|[wXHpR0xMnyNfcooq.htm](age-of-ashes-bestiary-items/wXHpR0xMnyNfcooq.htm)|Darkvision|Vision dans le noir|officielle|
|[wXqVDCpWmbgHRsvv.htm](age-of-ashes-bestiary-items/wXqVDCpWmbgHRsvv.htm)|Prismatic Beam|Faisceau prismatique|officielle|
|[x15lr82vvdm9m35m.htm](age-of-ashes-bestiary-items/x15lr82vvdm9m35m.htm)|Warhammer|Marteau de guerre|officielle|
|[x41htpR9Ou0sBLB3.htm](age-of-ashes-bestiary-items/x41htpR9Ou0sBLB3.htm)|+2 Greater Resilient Hide Armor|Armure de peau de résilience supérieure +2|officielle|
|[x4yLU5eVHfGlI7wG.htm](age-of-ashes-bestiary-items/x4yLU5eVHfGlI7wG.htm)|Telekinetic Object|Objet télékinésique|officielle|
|[X5l0KEVyuHvwriD3.htm](age-of-ashes-bestiary-items/X5l0KEVyuHvwriD3.htm)|Manacles (Good) (Marked with the Symbol of the Scarlet Triad)|Menottes (bonnes) (marquées du symbole de la Triade écarlate)|officielle|
|[x6TyhZ97JVNfZqqb.htm](age-of-ashes-bestiary-items/x6TyhZ97JVNfZqqb.htm)|+1 Status to All Saves vs. Positive|bonus de statut de +1 aux JdS contre positif|officielle|
|[x7eL2M3jSBwA2oka.htm](age-of-ashes-bestiary-items/x7eL2M3jSBwA2oka.htm)|Frightful Presence|Présence terrifiante|officielle|
|[x7Maw7sCvZ5bEGPz.htm](age-of-ashes-bestiary-items/x7Maw7sCvZ5bEGPz.htm)|Hermea Lore|Connaissance d'Herméa|officielle|
|[x94Pe7R5UrLKdRbG.htm](age-of-ashes-bestiary-items/x94Pe7R5UrLKdRbG.htm)|Invoke Reckoning|Convoquer le jugement|officielle|
|[x95HHOmRVl9H3tZ3.htm](age-of-ashes-bestiary-items/x95HHOmRVl9H3tZ3.htm)|Catch Rock|Interception de rocher|officielle|
|[x9FRA1b0TaNaQQ1p.htm](age-of-ashes-bestiary-items/x9FRA1b0TaNaQQ1p.htm)|Pervasive Attacks|Attaques insidieuses|officielle|
|[xCHDeiUPDjUNFsyM.htm](age-of-ashes-bestiary-items/xCHDeiUPDjUNFsyM.htm)|Charm (At Will)|Charme (À volonté)|officielle|
|[xCWXBbQXE3SoIRlC.htm](age-of-ashes-bestiary-items/xCWXBbQXE3SoIRlC.htm)|Spectral Hand|Main spectrale|officielle|
|[XD9BdYMcx2Thd1zB.htm](age-of-ashes-bestiary-items/XD9BdYMcx2Thd1zB.htm)|Shield Shove|Pousser au bouclier|officielle|
|[XDuhN8g49cd7hcxi.htm](age-of-ashes-bestiary-items/XDuhN8g49cd7hcxi.htm)|Lifesense 60 feet|Perception de la vie 18 m|officielle|
|[Xf6HIqF2ZwvI5TDe.htm](age-of-ashes-bestiary-items/Xf6HIqF2ZwvI5TDe.htm)|Flail|+1|Fléau +1|libre|
|[XfTtTfEahCIBTcwm.htm](age-of-ashes-bestiary-items/XfTtTfEahCIBTcwm.htm)|Flail|Fléau d'armes|officielle|
|[xfV7KvKayKVA2xs0.htm](age-of-ashes-bestiary-items/xfV7KvKayKVA2xs0.htm)|Explosive Breath|Souffle explosif|officielle|
|[xGyMivb2Rzai1kKv.htm](age-of-ashes-bestiary-items/xGyMivb2Rzai1kKv.htm)|Dagger|+1,striking,silver|Dague en argent de frappe +1|libre|
|[XIHvMvJwIJxCpNA7.htm](age-of-ashes-bestiary-items/XIHvMvJwIJxCpNA7.htm)|Arcane Innate Spells|Sorts arcaniques innés|libre|
|[XiZUzjxIJiMTuUq9.htm](age-of-ashes-bestiary-items/XiZUzjxIJiMTuUq9.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[XJmutb6gsFNsubD3.htm](age-of-ashes-bestiary-items/XJmutb6gsFNsubD3.htm)|Jaws|Mâchoires|officielle|
|[xJSY7tPzY307UaTz.htm](age-of-ashes-bestiary-items/xJSY7tPzY307UaTz.htm)|Pick|+1|Pic +1|libre|
|[XLA0uMn7KxLm7iN7.htm](age-of-ashes-bestiary-items/XLA0uMn7KxLm7iN7.htm)|Tail Lash|Coup de queue fouetté|officielle|
|[XLCaqjJ5OC0PqgJp.htm](age-of-ashes-bestiary-items/XLCaqjJ5OC0PqgJp.htm)|Dagger|Dague|officielle|
|[xMoISwQsrQGkhK7q.htm](age-of-ashes-bestiary-items/xMoISwQsrQGkhK7q.htm)|Dimensional Shunt|Poussée dimensionnelle|officielle|
|[xN7xuZd6V7GrLGii.htm](age-of-ashes-bestiary-items/xN7xuZd6V7GrLGii.htm)|7-8 Flaming Vortex|7–8 Vortex de feu|officielle|
|[Xnas07kWC3ZCJmY4.htm](age-of-ashes-bestiary-items/Xnas07kWC3ZCJmY4.htm)|Dragonstorm Breath|Souffle de tempête draconique|officielle|
|[xnnq7Jjs1DHgYSUC.htm](age-of-ashes-bestiary-items/xnnq7Jjs1DHgYSUC.htm)|Web Sense (Imprecise) 60 feet|Perception sur les toiles 18 m (imprécis)|officielle|
|[XO1T3uBgfNZ2aJks.htm](age-of-ashes-bestiary-items/XO1T3uBgfNZ2aJks.htm)|Throw Rock|Lancé de rochers|libre|
|[XqfuKJG5gZuzeCDr.htm](age-of-ashes-bestiary-items/XqfuKJG5gZuzeCDr.htm)|Frightful Presence|Présence terrifiante|officielle|
|[XrDITDvengsQOcjL.htm](age-of-ashes-bestiary-items/XrDITDvengsQOcjL.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[xSd36hFNsd1RqBw7.htm](age-of-ashes-bestiary-items/xSd36hFNsd1RqBw7.htm)|Ferocity|Férocité|officielle|
|[XSkEHa3VqTvwMo3i.htm](age-of-ashes-bestiary-items/XSkEHa3VqTvwMo3i.htm)|+1 Resilient Breastplate|Cuirasse de résilience +1|officielle|
|[XsSwjx894G8tLV3p.htm](age-of-ashes-bestiary-items/XsSwjx894G8tLV3p.htm)|+2 Status to All Saves vs. Arcane Magic|+2 de statut aux sauvegardes contre la magie arcanique|officielle|
|[XteT6RS3CQMyiNDq.htm](age-of-ashes-bestiary-items/XteT6RS3CQMyiNDq.htm)|At-Will Spells|Sorts à volonté|officielle|
|[XTtyz95mOu6Xrre4.htm](age-of-ashes-bestiary-items/XTtyz95mOu6Xrre4.htm)|Sneak Attack|Attaque sournoise|officielle|
|[XUqvBZPxNzGJ4RIm.htm](age-of-ashes-bestiary-items/XUqvBZPxNzGJ4RIm.htm)|Vulnerable to Dispelling|Vulnérabilité à la dissipation|officielle|
|[xvs14t3klUSEYCct.htm](age-of-ashes-bestiary-items/xvs14t3klUSEYCct.htm)|Prepared Arcane Spells|Sorts arcaniques préparés|libre|
|[XXHOTNV0xQRjxtwV.htm](age-of-ashes-bestiary-items/XXHOTNV0xQRjxtwV.htm)|Stunning Retort|Riposte étourdissante|officielle|
|[XxjjAD0GTCEaspdx.htm](age-of-ashes-bestiary-items/XxjjAD0GTCEaspdx.htm)|Perfect Will|Volonté parfaite|officielle|
|[Xy8CYVbA7GcoN8SC.htm](age-of-ashes-bestiary-items/Xy8CYVbA7GcoN8SC.htm)|Whip Swing|Se hisser à l'aide du fouet|officielle|
|[xyPbW6KkYnYYyzU1.htm](age-of-ashes-bestiary-items/xyPbW6KkYnYYyzU1.htm)|At-Will Spells|Sorts à volonté|officielle|
|[XzC7Tpe9v9HjnSor.htm](age-of-ashes-bestiary-items/XzC7Tpe9v9HjnSor.htm)|Localized Dragonstorm|Tempête draconique localisée|officielle|
|[XZIP4xIGRqQ4M88R.htm](age-of-ashes-bestiary-items/XZIP4xIGRqQ4M88R.htm)|Inexorable|Inexorable|officielle|
|[Y1AIIIjE3wSGI9AA.htm](age-of-ashes-bestiary-items/Y1AIIIjE3wSGI9AA.htm)|Engulf|Engloutir|officielle|
|[y1Ce9dDq4YBhhTY5.htm](age-of-ashes-bestiary-items/y1Ce9dDq4YBhhTY5.htm)|Arcane Innate Spells|Sorts arcaniques innés|libre|
|[y1qzP8PTqVnXarTq.htm](age-of-ashes-bestiary-items/y1qzP8PTqVnXarTq.htm)|+2 Greater Resilient Full Plate|Harnois de résilience supérieure +2|officielle|
|[Y2PZ0DuMi8XMN6hp.htm](age-of-ashes-bestiary-items/Y2PZ0DuMi8XMN6hp.htm)|Fly (Constant)|Vol (constant)|officielle|
|[Y4NlUcT2NJz7cDOb.htm](age-of-ashes-bestiary-items/Y4NlUcT2NJz7cDOb.htm)|True Seeing (constant)|Vision lucide (constant)|officielle|
|[y6J0cFsDLtTzzYwe.htm](age-of-ashes-bestiary-items/y6J0cFsDLtTzzYwe.htm)|Negative Healing|Guérison négative|officielle|
|[Y6pLT1gObD3hTlFj.htm](age-of-ashes-bestiary-items/Y6pLT1gObD3hTlFj.htm)|Champion Devotion Spells|Sorts de dévotion de champion|libre|
|[Y8FacHkJWvG6NwDu.htm](age-of-ashes-bestiary-items/Y8FacHkJWvG6NwDu.htm)|Rend|Éventration|officielle|
|[yA62c4LqjlDWm2Wb.htm](age-of-ashes-bestiary-items/yA62c4LqjlDWm2Wb.htm)|Plane Shift (At Will) (Self Only) (to the Material or Shadow Plane only)|Changement de plan (à volonté, soi uniquement, vers le plan Matériel ou de l'Ombre uniquement)|officielle|
|[YC7WTdAOh21NEeNx.htm](age-of-ashes-bestiary-items/YC7WTdAOh21NEeNx.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[yCCEm7HEqedgAAhD.htm](age-of-ashes-bestiary-items/yCCEm7HEqedgAAhD.htm)|+1 Status to All Saves vs. Poison|bonus de statut de +1 aux JdS contre poison|officielle|
|[yCOLPsjbm44QUBqX.htm](age-of-ashes-bestiary-items/yCOLPsjbm44QUBqX.htm)|At-Will Spells|Sorts à volonté|officielle|
|[YCQ2DWy0a4bzK0Hb.htm](age-of-ashes-bestiary-items/YCQ2DWy0a4bzK0Hb.htm)|Crafting|Artisanat|officielle|
|[YdZT3Ed6Vo7vzA6W.htm](age-of-ashes-bestiary-items/YdZT3Ed6Vo7vzA6W.htm)|Gunpower|Poudre noire|officielle|
|[YeFEaw3Ja6zgrf9o.htm](age-of-ashes-bestiary-items/YeFEaw3Ja6zgrf9o.htm)|Double Slice|Double taille|officielle|
|[yfiUAGMbYu5GpkVc.htm](age-of-ashes-bestiary-items/yfiUAGMbYu5GpkVc.htm)|Constant Spells|Sorts constants|officielle|
|[yggAbFpFmUKjj7Bz.htm](age-of-ashes-bestiary-items/yggAbFpFmUKjj7Bz.htm)|Drain Bonded Item|Drain d'objet lié|officielle|
|[yGOn243Z3mSuZbji.htm](age-of-ashes-bestiary-items/yGOn243Z3mSuZbji.htm)|Freedom of Movement (Constant)|Liberté de mouvement (constant)|officielle|
|[yh0jnKztqB0nQlYV.htm](age-of-ashes-bestiary-items/yh0jnKztqB0nQlYV.htm)|Nail|Clou|officielle|
|[YhBi7QJI7xdnQU10.htm](age-of-ashes-bestiary-items/YhBi7QJI7xdnQU10.htm)|Acidic Needle|Aiguille acide|officielle|
|[YhdTXGv6J7fkl4wd.htm](age-of-ashes-bestiary-items/YhdTXGv6J7fkl4wd.htm)|Inexorable March|Marche inexorable|officielle|
|[Yimb91yYaAqRGR3x.htm](age-of-ashes-bestiary-items/Yimb91yYaAqRGR3x.htm)|Seismic Spear|Lance sismique|officielle|
|[yJtHwSdZ0Gj2LGAH.htm](age-of-ashes-bestiary-items/yJtHwSdZ0Gj2LGAH.htm)|Shrieking Frenzy|Frénésie hurlante|libre|
|[YlxLNSbifo76lEgK.htm](age-of-ashes-bestiary-items/YlxLNSbifo76lEgK.htm)|Dragon Lore|Connaissance des dragons|officielle|
|[YMJhZThbEkA9mBqf.htm](age-of-ashes-bestiary-items/YMJhZThbEkA9mBqf.htm)|Darkvision|Vision dans le noir|officielle|
|[yo9I3mhk7rIUPwWa.htm](age-of-ashes-bestiary-items/yo9I3mhk7rIUPwWa.htm)|Daemonic Trap Making|Création de piège daémonique|officielle|
|[YoSJ0Xce0DnEuH7Z.htm](age-of-ashes-bestiary-items/YoSJ0Xce0DnEuH7Z.htm)|Composite Longbow|Arc long composite|officielle|
|[yqO9SHE3AFcvwy0R.htm](age-of-ashes-bestiary-items/yqO9SHE3AFcvwy0R.htm)|Disturbing Vision|Vision troublante|officielle|
|[yQWrahMGXUIRdJBO.htm](age-of-ashes-bestiary-items/yQWrahMGXUIRdJBO.htm)|Sound Burst (At Will)|Cacophonie (À volonté)|officielle|
|[YQZTNPJv0qGnH7F6.htm](age-of-ashes-bestiary-items/YQZTNPJv0qGnH7F6.htm)|Focus Spells|Sorts focalisés|libre|
|[yrnHfAodzInJxTRU.htm](age-of-ashes-bestiary-items/yrnHfAodzInJxTRU.htm)|Constant Spells|Sorts constants|officielle|
|[yRzCy4eBcJG8ydmJ.htm](age-of-ashes-bestiary-items/yRzCy4eBcJG8ydmJ.htm)|9-10 Poison Miasma|9–10 Miasmes toxiques|officielle|
|[YsvmoSYwOoP0KhQx.htm](age-of-ashes-bestiary-items/YsvmoSYwOoP0KhQx.htm)|Blue Eye Beam|Rayon oculaire bleu|officielle|
|[yt0OzxOSDhYO0Nle.htm](age-of-ashes-bestiary-items/yt0OzxOSDhYO0Nle.htm)|Composite Shortbow|Arc court composite|officielle|
|[YT5Tkw6exXuHF2ph.htm](age-of-ashes-bestiary-items/YT5Tkw6exXuHF2ph.htm)|Soul Chain|Chaîne d'âmes|officielle|
|[yTHVYVd7pjTLwWpu.htm](age-of-ashes-bestiary-items/yTHVYVd7pjTLwWpu.htm)|Syringe|Seringue|officielle|
|[Yuh3IpCAUHQMRRoE.htm](age-of-ashes-bestiary-items/Yuh3IpCAUHQMRRoE.htm)|Deny Advantage|Refus d'avantage|officielle|
|[YW4QH2QsfjdJXFo8.htm](age-of-ashes-bestiary-items/YW4QH2QsfjdJXFo8.htm)|Aluum Antimagic|Antimagie aluum|officielle|
|[YW9ZoJ6fNOB1slcu.htm](age-of-ashes-bestiary-items/YW9ZoJ6fNOB1slcu.htm)|Dragon Lore|Connaissance des dragons|officielle|
|[yXL2GLDidWh51HPb.htm](age-of-ashes-bestiary-items/yXL2GLDidWh51HPb.htm)|Dagger|Dague|officielle|
|[yyARzCq3i2SQxTFI.htm](age-of-ashes-bestiary-items/yyARzCq3i2SQxTFI.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|officielle|
|[Z28y4lP04fvgUiy2.htm](age-of-ashes-bestiary-items/Z28y4lP04fvgUiy2.htm)|Mutagenic Adaptation|Accoutumance aux mutagènes|officielle|
|[z4fETZWZ1URqOEw5.htm](age-of-ashes-bestiary-items/z4fETZWZ1URqOEw5.htm)|Subsonic Hum|Bourdonnement subsonique|officielle|
|[Z6mjzInaKKCOyvbM.htm](age-of-ashes-bestiary-items/Z6mjzInaKKCOyvbM.htm)|Glowing Spores|Spores luminescentes|officielle|
|[Z7DJdh6dNliHnh4s.htm](age-of-ashes-bestiary-items/Z7DJdh6dNliHnh4s.htm)|Crossbow|Arbalète|officielle|
|[Z875mlZme0MfKkqS.htm](age-of-ashes-bestiary-items/Z875mlZme0MfKkqS.htm)|Redirect Portal|Rediriger un portail|officielle|
|[z89EX9zy3v9pZxJc.htm](age-of-ashes-bestiary-items/z89EX9zy3v9pZxJc.htm)|Command Skeletons|Commander des squelettes|officielle|
|[ZB91njeJdhCxI5DS.htm](age-of-ashes-bestiary-items/ZB91njeJdhCxI5DS.htm)|Rock|Rocher|officielle|
|[ZcyYi8CE2APPPjbe.htm](age-of-ashes-bestiary-items/ZcyYi8CE2APPPjbe.htm)|Rapier|+1,striking|Rapière de frappe +1|officielle|
|[ZDJ1U0JvyJNUofit.htm](age-of-ashes-bestiary-items/ZDJ1U0JvyJNUofit.htm)|Air Walk (Constant)|Marche dans les airs (constant)|officielle|
|[zFmXqrFJBmQGs6z5.htm](age-of-ashes-bestiary-items/zFmXqrFJBmQGs6z5.htm)|Create Haunt|Création d'apparition|officielle|
|[ZfxZgIiinmttVMGy.htm](age-of-ashes-bestiary-items/ZfxZgIiinmttVMGy.htm)|Light Hammer|+1,striking|Marteau de guerre léger de frappe +1|libre|
|[zgALCNYNRMkV6Bhv.htm](age-of-ashes-bestiary-items/zgALCNYNRMkV6Bhv.htm)|Dagger|Dague|officielle|
|[ZgtDywSfiOFQD0r7.htm](age-of-ashes-bestiary-items/ZgtDywSfiOFQD0r7.htm)|Regeneration 50|Régénération 50|officielle|
|[zhwIQhGc33778Khq.htm](age-of-ashes-bestiary-items/zhwIQhGc33778Khq.htm)|Improved Push|Bousculade améliorée|officielle|
|[Zj2BDpm3BaAvGpbd.htm](age-of-ashes-bestiary-items/Zj2BDpm3BaAvGpbd.htm)|Arborean Arm|Bras arboricole|officielle|
|[ZJpZacKciS27DjVJ.htm](age-of-ashes-bestiary-items/ZJpZacKciS27DjVJ.htm)|Monocle|Monocle|officielle|
|[ZkM3S4OabItFRUIq.htm](age-of-ashes-bestiary-items/ZkM3S4OabItFRUIq.htm)|Negative Healing|Guérison négative|officielle|
|[zkT4pDnggpJK1FkR.htm](age-of-ashes-bestiary-items/zkT4pDnggpJK1FkR.htm)|Key to the Temple of All Gods|Clé du Temple de tous les dieux|officielle|
|[ZmNv9H5jTJd7onVz.htm](age-of-ashes-bestiary-items/ZmNv9H5jTJd7onVz.htm)|Spear|+3,majorStriking,returning|Lance boomerang de frappe majeure +3|officielle|
|[znA23DnIxfbHuPHh.htm](age-of-ashes-bestiary-items/znA23DnIxfbHuPHh.htm)|Draconic Frenzy|Frénésie draconique|officielle|
|[ZpFKjHuyvoYiqXhM.htm](age-of-ashes-bestiary-items/ZpFKjHuyvoYiqXhM.htm)|Crossbow|+1|Arbalète +1|libre|
|[ZqyK0k31aaaiua44.htm](age-of-ashes-bestiary-items/ZqyK0k31aaaiua44.htm)|Longsword|+1,striking|Épée longue de frappe +1|libre|
|[ZRoLOOZe4nN8DGRf.htm](age-of-ashes-bestiary-items/ZRoLOOZe4nN8DGRf.htm)|Mercantile Lore|Connaissance commerciale|officielle|
|[zRPB5eZcHteseFIt.htm](age-of-ashes-bestiary-items/zRPB5eZcHteseFIt.htm)|Nul-Acrumi Vazghul Ritual|Rituel du nul-acrumi vazghul|officielle|
|[ztAvaU3ULkuadpMc.htm](age-of-ashes-bestiary-items/ztAvaU3ULkuadpMc.htm)|Slash and Slam|Lacération et percussion|officielle|
|[zuDdECTRYKz8JuqE.htm](age-of-ashes-bestiary-items/zuDdECTRYKz8JuqE.htm)|Darkvision|Vision dans le noir|officielle|
|[zVCalXk0BtUnOMfM.htm](age-of-ashes-bestiary-items/zVCalXk0BtUnOMfM.htm)|Sharpened Fragment|Fragments aiguisés|officielle|
|[ZvcUmXAhxPGfnsNc.htm](age-of-ashes-bestiary-items/ZvcUmXAhxPGfnsNc.htm)|Spider Lore|Connaissance des araignées|officielle|
|[ZWaPe5FQtP4LDiEM.htm](age-of-ashes-bestiary-items/ZWaPe5FQtP4LDiEM.htm)|Bleeding Nail|Clou barbelé|officielle|
|[zWiqjRZ48bqk1oCo.htm](age-of-ashes-bestiary-items/zWiqjRZ48bqk1oCo.htm)|Composite Longbow|+1,striking,flaming|Arc long composite enflammé de frappe +1|libre|
|[zWMWo1FSBvdX4mhf.htm](age-of-ashes-bestiary-items/zWMWo1FSBvdX4mhf.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[zWqQgevLJLJ7zv0x.htm](age-of-ashes-bestiary-items/zWqQgevLJLJ7zv0x.htm)|Mercantile Lore|Connaissance commerciale|officielle|
|[ZXC6yvdBWQpHBJIb.htm](age-of-ashes-bestiary-items/ZXC6yvdBWQpHBJIb.htm)|Constrict|Constriction|officielle|
|[ZXkVB1alDTTOm35s.htm](age-of-ashes-bestiary-items/ZXkVB1alDTTOm35s.htm)|Freeze|Immobilisation|officielle|
|[ZyKpCc2am8rFqQ93.htm](age-of-ashes-bestiary-items/ZyKpCc2am8rFqQ93.htm)|Manifest Dagger|Convoquer une dague|officielle|
|[ZYT8B7LGfrmxllL4.htm](age-of-ashes-bestiary-items/ZYT8B7LGfrmxllL4.htm)|Bloodsense|Perception du sang|officielle|
|[zZnaIccm0OeZf8hn.htm](age-of-ashes-bestiary-items/zZnaIccm0OeZf8hn.htm)|Hermea Lore|Connaissance d'Herméa|officielle|
