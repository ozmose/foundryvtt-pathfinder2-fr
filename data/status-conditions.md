# État de la traduction (conditions)

 * **libre**: 10
 * **officielle**: 32


Dernière mise à jour: 2023-07-12 20:28 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[1wQY3JYyhMYeeV2G.htm](conditions/1wQY3JYyhMYeeV2G.htm)|Observed|Observé|libre|
|[3uh1r86TzbQvosxv.htm](conditions/3uh1r86TzbQvosxv.htm)|Doomed|Condamné|officielle|
|[4D2KBtexWXa6oUMR.htm](conditions/4D2KBtexWXa6oUMR.htm)|Drained|Drainé|officielle|
|[6dNUvdb1dhToNDj3.htm](conditions/6dNUvdb1dhToNDj3.htm)|Broken|Brisé|officielle|
|[6uEgoh53GbXuHpTF.htm](conditions/6uEgoh53GbXuHpTF.htm)|Paralyzed|Paralysé|officielle|
|[9evPzg9E6muFcoSk.htm](conditions/9evPzg9E6muFcoSk.htm)|Unnoticed|Inaperçu|officielle|
|[9PR9y0bi4JPKnHPR.htm](conditions/9PR9y0bi4JPKnHPR.htm)|Deafened|Sourd|officielle|
|[9qGBRpbX9NEwtAAr.htm](conditions/9qGBRpbX9NEwtAAr.htm)|Controlled|Contrôlé|officielle|
|[AdPVz7rbaVSRxHFg.htm](conditions/AdPVz7rbaVSRxHFg.htm)|Fascinated|Fasciné|officielle|
|[AJh5ex99aV6VTggg.htm](conditions/AJh5ex99aV6VTggg.htm)|Flat-Footed|Pris au dépourvu|officielle|
|[D5mg6Tc7Jzrj6ro7.htm](conditions/D5mg6Tc7Jzrj6ro7.htm)|Encumbered|Surchargé|officielle|
|[dfCMdR4wnpbYNTix.htm](conditions/dfCMdR4wnpbYNTix.htm)|Stunned|Étourdi|officielle|
|[DmAIPqOBomZ7H95W.htm](conditions/DmAIPqOBomZ7H95W.htm)|Concealed|Masqué|officielle|
|[dTwPJuKgBQCMxixg.htm](conditions/dTwPJuKgBQCMxixg.htm)|Petrified|Pétrifié|officielle|
|[e1XGnhKNSQIm5IXg.htm](conditions/e1XGnhKNSQIm5IXg.htm)|Stupefied|Stupéfié|officielle|
|[eIcWbB5o3pP6OIMe.htm](conditions/eIcWbB5o3pP6OIMe.htm)|Immobilized|Immobilisé|officielle|
|[fBnFDH2MTzgFijKf.htm](conditions/fBnFDH2MTzgFijKf.htm)|Unconscious|Inconscient|officielle|
|[fesd1n5eVhpCSS18.htm](conditions/fesd1n5eVhpCSS18.htm)|Sickened|Malade/nauséeux|libre|
|[fuG8dgthlDWfWjIA.htm](conditions/fuG8dgthlDWfWjIA.htm)|Indifferent|Indifférent|libre|
|[HL2l2VRSaQHu9lUw.htm](conditions/HL2l2VRSaQHu9lUw.htm)|Fatigued|Fatigué|officielle|
|[I1ffBVISxLr2gC4u.htm](conditions/I1ffBVISxLr2gC4u.htm)|Unfriendly|Inamical|officielle|
|[i3OJZU2nk64Df3xm.htm](conditions/i3OJZU2nk64Df3xm.htm)|Clumsy|Maladroit|officielle|
|[iU0fEDdBp3rXpTMC.htm](conditions/iU0fEDdBp3rXpTMC.htm)|Hidden|Caché|officielle|
|[j91X7x0XSomq8d60.htm](conditions/j91X7x0XSomq8d60.htm)|Prone|À terre|officielle|
|[kWc1fhmv9LBiTuei.htm](conditions/kWc1fhmv9LBiTuei.htm)|Grabbed|Agrippé/empoigné|libre|
|[lDVqvLKA6eF3Df60.htm](conditions/lDVqvLKA6eF3Df60.htm)|Persistent Damage|Dégâts persistants|officielle|
|[MIRkyAjyBeXivMa7.htm](conditions/MIRkyAjyBeXivMa7.htm)|Enfeebled|Affaibli|officielle|
|[nlCjDvLMf2EkV2dl.htm](conditions/nlCjDvLMf2EkV2dl.htm)|Quickened|Accéléré|officielle|
|[sDPxOjQ9kx2RZE8D.htm](conditions/sDPxOjQ9kx2RZE8D.htm)|Fleeing|En fuite|officielle|
|[TBSHQspnbcqxsmjL.htm](conditions/TBSHQspnbcqxsmjL.htm)|Frightened|Effrayé|officielle|
|[TkIyaNPgTZFBCCuh.htm](conditions/TkIyaNPgTZFBCCuh.htm)|Dazzled|Ébloui|officielle|
|[ud7gTLwPeklzYSXG.htm](conditions/ud7gTLwPeklzYSXG.htm)|Hostile|Hostile|libre|
|[v44P3WUcU1j0115l.htm](conditions/v44P3WUcU1j0115l.htm)|Helpful|Serviable|libre|
|[v66R7FdOf11l94im.htm](conditions/v66R7FdOf11l94im.htm)|Friendly|Amical|libre|
|[VcDeM8A5oI6VqhbM.htm](conditions/VcDeM8A5oI6VqhbM.htm)|Restrained|Entravé|libre|
|[VRSef5y1LmL2Hkjf.htm](conditions/VRSef5y1LmL2Hkjf.htm)|Undetected|Non détecté|officielle|
|[XgEqL1kFApUbl5Z2.htm](conditions/XgEqL1kFApUbl5Z2.htm)|Blinded|Aveuglé|officielle|
|[xYTAsEpcJE1Ccni3.htm](conditions/xYTAsEpcJE1Ccni3.htm)|Slowed|Ralenti|libre|
|[yblD8fOR1J8rDwEQ.htm](conditions/yblD8fOR1J8rDwEQ.htm)|Confused|Confus|officielle|
|[Yl48xTdMh3aeQYL2.htm](conditions/Yl48xTdMh3aeQYL2.htm)|Wounded|Blessé|libre|
|[yZRUzMqrMmfLu0V1.htm](conditions/yZRUzMqrMmfLu0V1.htm)|Dying|Mourant|officielle|
|[zJxUflt9np0q4yML.htm](conditions/zJxUflt9np0q4yML.htm)|Invisible|Invisible|officielle|
