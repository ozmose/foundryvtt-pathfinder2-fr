# État de la traduction (kingmaker-bestiary)

 * **libre**: 235


Dernière mise à jour: 2023-07-12 20:28 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0EO5vP2gCliAxLrF.htm](kingmaker-bestiary/0EO5vP2gCliAxLrF.htm)|Nok-Nok (Level 5)|Nok-Nok (niveau 5)|libre|
|[0EQySWHT7D68yrrx.htm](kingmaker-bestiary/0EQySWHT7D68yrrx.htm)|Jubilost (Level 8)|Jubilost (niveau 8)|libre|
|[0qc6h3jgLFNhX1tG.htm](kingmaker-bestiary/0qc6h3jgLFNhX1tG.htm)|Ballista Defense|Baliste de défense|libre|
|[1SEyDYO9l6mcFhoy.htm](kingmaker-bestiary/1SEyDYO9l6mcFhoy.htm)|Ekundayo (Level 1)|Ekundayo (niveau 1)|libre|
|[1upVC05DY7YzxNsr.htm](kingmaker-bestiary/1upVC05DY7YzxNsr.htm)|Chew Spider|Araignées mâchouilleuses|libre|
|[1UWbR2WkeP0kl1nQ.htm](kingmaker-bestiary/1UWbR2WkeP0kl1nQ.htm)|Giant Trapdoor Spider|Araignée géante piégeuse|libre|
|[1vsQlCVwa9kVmgRi.htm](kingmaker-bestiary/1vsQlCVwa9kVmgRi.htm)|Avatar of the Lantern King|Avatar du Roi lanterne|libre|
|[1ZKOTqqzeorIv7BB.htm](kingmaker-bestiary/1ZKOTqqzeorIv7BB.htm)|Trapdoor Ogre Spider|Araignée ogre piégeuse|libre|
|[2deePNLiJcnSZQhd.htm](kingmaker-bestiary/2deePNLiJcnSZQhd.htm)|Test of Strength|Test de Force|libre|
|[2mkfF43tP8IGVfBz.htm](kingmaker-bestiary/2mkfF43tP8IGVfBz.htm)|Foras|Foras|libre|
|[3gtQv6Mkr7CUlG7W.htm](kingmaker-bestiary/3gtQv6Mkr7CUlG7W.htm)|Wild Hunt Archer|Archer de la Chasse sauvage|libre|
|[3HnIluPWsKm3eEYB.htm](kingmaker-bestiary/3HnIluPWsKm3eEYB.htm)|Flooding Room|Salle submergée|libre|
|[3kyS4KzEmG8Eyl4N.htm](kingmaker-bestiary/3kyS4KzEmG8Eyl4N.htm)|Thresholder Hermeticist|Hermétiste du seuil|libre|
|[3QkgiJJ0IS6oTT0t.htm](kingmaker-bestiary/3QkgiJJ0IS6oTT0t.htm)|River Elasmosaurus|Élasmosaure de rivière|libre|
|[3Tb42wMuwjtP3iYN.htm](kingmaker-bestiary/3Tb42wMuwjtP3iYN.htm)|The Lonely Warrior|Le guerrier solitaire|libre|
|[3xHHKI7PGQN218aL.htm](kingmaker-bestiary/3xHHKI7PGQN218aL.htm)|Eobald|Éobald|libre|
|[48o307dry03xazvd.htm](kingmaker-bestiary/48o307dry03xazvd.htm)|Corax|Corax|libre|
|[554zNFkbYXBdmgAy.htm](kingmaker-bestiary/554zNFkbYXBdmgAy.htm)|Sister of the Bloodshot Eye|Soeur de l'Oeil ensanglanté|libre|
|[5npzwOJSQLSPhmx2.htm](kingmaker-bestiary/5npzwOJSQLSPhmx2.htm)|Pavetta Stroon-Drelev|Pavetta Stroon-Drélev|libre|
|[5Xslhvyh3MSzgIXN.htm](kingmaker-bestiary/5Xslhvyh3MSzgIXN.htm)|Volodmyra|Volodmyra|libre|
|[6BFUxkEBKPr89SNt.htm](kingmaker-bestiary/6BFUxkEBKPr89SNt.htm)|Mandragora Swarm|Nuée de mandragores|libre|
|[6zWcjvPya0wWDZv2.htm](kingmaker-bestiary/6zWcjvPya0wWDZv2.htm)|Risen Fetch|Fetch réssuscité|libre|
|[7bWjM5e9sKxaYbOw.htm](kingmaker-bestiary/7bWjM5e9sKxaYbOw.htm)|Thorn River Bandit|Bandit du gué de l'Épine|libre|
|[7os3GIj69tIqXTGl.htm](kingmaker-bestiary/7os3GIj69tIqXTGl.htm)|Wild Hunt Horse|Cheval de la Chasse sauvage|libre|
|[7PkMXEkBrxhTCw4s.htm](kingmaker-bestiary/7PkMXEkBrxhTCw4s.htm)|Horagnamon|Horagnamon|libre|
|[8cRXmQ9vd93T0m2N.htm](kingmaker-bestiary/8cRXmQ9vd93T0m2N.htm)|Cyclops Zombie|Cyclope zombie|libre|
|[8D3xvMiuOLJ3p6FU.htm](kingmaker-bestiary/8D3xvMiuOLJ3p6FU.htm)|Hooktongue Hydra|Hydre Langue-crochet|libre|
|[8eTfnYg0xmcgmDVQ.htm](kingmaker-bestiary/8eTfnYg0xmcgmDVQ.htm)|Irovetti's Fetch|Fetch d'Irovetti|libre|
|[8IYynuqZxDpXZwBE.htm](kingmaker-bestiary/8IYynuqZxDpXZwBE.htm)|Tartuccio|Tartuccio|libre|
|[8YJiyhWSWxMdGJV6.htm](kingmaker-bestiary/8YJiyhWSWxMdGJV6.htm)|The Power of Faith|Le pouvoir de la foi|libre|
|[9nGhRYarE5KkwOwc.htm](kingmaker-bestiary/9nGhRYarE5KkwOwc.htm)|Nilak|Nilak|libre|
|[AA3hdVlJhALthghl.htm](kingmaker-bestiary/AA3hdVlJhALthghl.htm)|Enormous Flame Drake|Énorme drake des flammes|libre|
|[aEz68WBHT9F5YlXz.htm](kingmaker-bestiary/aEz68WBHT9F5YlXz.htm)|Castruccio Irovetti|Castruccio Irovetti|libre|
|[AGNRcnrftglAtlbN.htm](kingmaker-bestiary/AGNRcnrftglAtlbN.htm)|Gromog|Gromog|libre|
|[AjpJNvt4nwzDAhqT.htm](kingmaker-bestiary/AjpJNvt4nwzDAhqT.htm)|Villamor Koth|Villarmor Koth|libre|
|[aoICPc2KqxIP19m2.htm](kingmaker-bestiary/aoICPc2KqxIP19m2.htm)|Valerie (Level 9)|Valérie (niveau 9)|libre|
|[Awlsx9BtgkQ39x6N.htm](kingmaker-bestiary/Awlsx9BtgkQ39x6N.htm)|Aldori Sister|Soeur Aldori|libre|
|[AXmQ8rUKsJFZUKb6.htm](kingmaker-bestiary/AXmQ8rUKsJFZUKb6.htm)|Collapsing Bridge|Effondrement de pont|libre|
|[B0EZMtvXsIE3SYiu.htm](kingmaker-bestiary/B0EZMtvXsIE3SYiu.htm)|Locking Alarm|Alarme de verrouillage|libre|
|[BEo6xNEhEKPsUzIn.htm](kingmaker-bestiary/BEo6xNEhEKPsUzIn.htm)|Hooktongue|Langue-crochet|libre|
|[bF0FHdZMWl1OuRae.htm](kingmaker-bestiary/bF0FHdZMWl1OuRae.htm)|Hill Giant Butcher|Boucher géant des collines|libre|
|[bHnGsHHymPUC1XGc.htm](kingmaker-bestiary/bHnGsHHymPUC1XGc.htm)|Auchs|Auchs|libre|
|[BJbe6BzLSIx8Jsix.htm](kingmaker-bestiary/BJbe6BzLSIx8Jsix.htm)|Storm-Struck Arboreal|Arboréen foudroyé|libre|
|[bjMn6cRNFK9jgfe0.htm](kingmaker-bestiary/bjMn6cRNFK9jgfe0.htm)|Bog Mummy Cultist|Cultiste momie des tourbières|libre|
|[Bq2eST0Ke4MnKXgR.htm](kingmaker-bestiary/Bq2eST0Ke4MnKXgR.htm)|Grigori|Grigori|libre|
|[bwEYizrdHs8lmvxE.htm](kingmaker-bestiary/bwEYizrdHs8lmvxE.htm)|Leng Envoy|Envoyé de Leng|libre|
|[BWI2CbtRo2lUcraX.htm](kingmaker-bestiary/BWI2CbtRo2lUcraX.htm)|Bloom Cultist|Cultiste de la Fleur|libre|
|[ce5vC049lfuXnPSy.htm](kingmaker-bestiary/ce5vC049lfuXnPSy.htm)|Orb Blast Trap|Piège de l'orbe explosive|libre|
|[Cj2zn7JazTB9ZnIq.htm](kingmaker-bestiary/Cj2zn7JazTB9ZnIq.htm)|Tiger Lord Hill Giant (TL2)|Seigneur Tigre géant des collines (TL2)|libre|
|[cjQVmbjqONihI9SR.htm](kingmaker-bestiary/cjQVmbjqONihI9SR.htm)|Fen Pudding|Vase des marais|libre|
|[cPcregujkG0LUHiG.htm](kingmaker-bestiary/cPcregujkG0LUHiG.htm)|Evindra|Évindra|libre|
|[defXhBIK4TtoZXGK.htm](kingmaker-bestiary/defXhBIK4TtoZXGK.htm)|The Stag Lord|Seigneur Cerf|libre|
|[Dxn6t9aoWUwPn6CE.htm](kingmaker-bestiary/Dxn6t9aoWUwPn6CE.htm)|Virthad|Virthad|libre|
|[dZEl1W8zV3rj5D9O.htm](kingmaker-bestiary/dZEl1W8zV3rj5D9O.htm)|Old Crackjaw|Vieux Claque-bec|libre|
|[E70Drr6CmSaJQ01v.htm](kingmaker-bestiary/E70Drr6CmSaJQ01v.htm)|Spiral Seal|Sceau en spirale|libre|
|[Ea0Edd9XNA17yj9n.htm](kingmaker-bestiary/Ea0Edd9XNA17yj9n.htm)|Stag Lord Bandit|Bandit du Seigneur Cerf|libre|
|[eAu4SOvMaNISD3RZ.htm](kingmaker-bestiary/eAu4SOvMaNISD3RZ.htm)|Vordakai|Vordakai|libre|
|[EI8wQc9kzooDHQoJ.htm](kingmaker-bestiary/EI8wQc9kzooDHQoJ.htm)|Weakened Floor|Sol fragilisé|libre|
|[EM2mwJzZeu5rWIQS.htm](kingmaker-bestiary/EM2mwJzZeu5rWIQS.htm)|Ekundayo (Level 6)|Ekundayo (niveau 6)|libre|
|[etIP2Mdv3Xnr0wto.htm](kingmaker-bestiary/etIP2Mdv3Xnr0wto.htm)|Ancient Wisp|Ancient feu follet|libre|
|[etrlNmJRUWDKbKwG.htm](kingmaker-bestiary/etrlNmJRUWDKbKwG.htm)|Terrion Numesti|Terrion Numesti|libre|
|[eVHkWtrdGJMVMob7.htm](kingmaker-bestiary/eVHkWtrdGJMVMob7.htm)|Dovan from Nisroch|Dovan de Nisroch|libre|
|[EvNCuVyY5sEGM4ZL.htm](kingmaker-bestiary/EvNCuVyY5sEGM4ZL.htm)|Hannis Drelev|Hannis Drélev|libre|
|[F0uz9YV8ILAM8fIg.htm](kingmaker-bestiary/F0uz9YV8ILAM8fIg.htm)|Windchaser|Coursevent|libre|
|[fbsy6zPV4HjHiis7.htm](kingmaker-bestiary/fbsy6zPV4HjHiis7.htm)|Bloomborn Athach|Athach né du Bourgeon|libre|
|[fiBhPqM2lomswplt.htm](kingmaker-bestiary/fiBhPqM2lomswplt.htm)|Kereek|Kereek|libre|
|[FkSxn7QSbVqA3dMy.htm](kingmaker-bestiary/FkSxn7QSbVqA3dMy.htm)|Test of Endurance|Test de l'Endurance|libre|
|[fQ9FuovHuRt6vtcq.htm](kingmaker-bestiary/fQ9FuovHuRt6vtcq.htm)|Wild Hunt Scout|Éclaireur de la Chasse sauvage|libre|
|[fs1iFoZmJF1iUWwX.htm](kingmaker-bestiary/fs1iFoZmJF1iUWwX.htm)|Davik Nettles|Davik Nettles|libre|
|[GDBEHLicn4kKggis.htm](kingmaker-bestiary/GDBEHLicn4kKggis.htm)|Murder of Crows|Vol de corbeaux assassins|libre|
|[gdTJOwXUwwhKAzlR.htm](kingmaker-bestiary/gdTJOwXUwwhKAzlR.htm)|Nishkiv the Knife|Nishkiv le Couteau|libre|
|[GiRckUiMamrdgjXQ.htm](kingmaker-bestiary/GiRckUiMamrdgjXQ.htm)|Cutthroat Haunt|Apparition coupe-gorge|libre|
|[gNtXGquzueNJLvFJ.htm](kingmaker-bestiary/gNtXGquzueNJLvFJ.htm)|Dread Aura|Aura effroyable|libre|
|[GviFe34FuTpo8AT0.htm](kingmaker-bestiary/GviFe34FuTpo8AT0.htm)|Scalding Tar Lake|Lac de goudron enflammé|libre|
|[gXbDaY9ci2u22ptT.htm](kingmaker-bestiary/gXbDaY9ci2u22ptT.htm)|Ghostly Guard|Gardes fantomatiques|libre|
|[h73Up6EQZqtgh6gP.htm](kingmaker-bestiary/h73Up6EQZqtgh6gP.htm)|Jamandi Aldori|Jamandi Aldori|libre|
|[hdFT5WIarw2Do3Sy.htm](kingmaker-bestiary/hdFT5WIarw2Do3Sy.htm)|Tristian (Level 1)|Tristian (niveau 1)|libre|
|[hFHdGOMRuhXIuAJo.htm](kingmaker-bestiary/hFHdGOMRuhXIuAJo.htm)|Test of Agility|Test de l'Agilité|libre|
|[hGLp8mkvx8J8DDL8.htm](kingmaker-bestiary/hGLp8mkvx8J8DDL8.htm)|Malgorzata Niska|Malgorzata Niska|libre|
|[hGQ4uxhxwtrnGfj0.htm](kingmaker-bestiary/hGQ4uxhxwtrnGfj0.htm)|Rickety Bridge|Pont branlant|libre|
|[HkdsqQLNb9XwzYIH.htm](kingmaker-bestiary/HkdsqQLNb9XwzYIH.htm)|Tiger Lord|Seigneur tigre|libre|
|[hLBHFloWuXLjCQYH.htm](kingmaker-bestiary/hLBHFloWuXLjCQYH.htm)|Primal Bandersnatch|Bandersnatch primordial|libre|
|[hLvxvLere6DruDLJ.htm](kingmaker-bestiary/hLvxvLere6DruDLJ.htm)|Annamede Belavarah|Annamède Belavarah|libre|
|[hQ0aR4oXug0yoTbT.htm](kingmaker-bestiary/hQ0aR4oXug0yoTbT.htm)|Unstable Pit|Fosse instable|libre|
|[HRLmXOCMwyw5HAfw.htm](kingmaker-bestiary/HRLmXOCMwyw5HAfw.htm)|Camouflaged Spiked Pit|Fosse camouflée hérissée de pieux|libre|
|[htHgsx1COWOfhE3D.htm](kingmaker-bestiary/htHgsx1COWOfhE3D.htm)|Thresholder Mystic|Mystique du seuil|libre|
|[hW40C78kV4MBDs4v.htm](kingmaker-bestiary/hW40C78kV4MBDs4v.htm)|Teleport Trap|Piège de téléportation|libre|
|[hX3uMf6KxgObJ9ec.htm](kingmaker-bestiary/hX3uMf6KxgObJ9ec.htm)|Valerie (Level 1)|Valérie (niveau 1)|libre|
|[hywUu3wVDuXePx3e.htm](kingmaker-bestiary/hywUu3wVDuXePx3e.htm)|Centaur Scout|Éclaireur centaure|libre|
|[I8IPTHEU1zF5KmAB.htm](kingmaker-bestiary/I8IPTHEU1zF5KmAB.htm)|Boggard Warden|Gardien bourbiérin|libre|
|[I8pvGB7SiXQ6SnGn.htm](kingmaker-bestiary/I8pvGB7SiXQ6SnGn.htm)|Stygian Fires|Feux stygiens|libre|
|[IGVqtFsNIyghuVsD.htm](kingmaker-bestiary/IGVqtFsNIyghuVsD.htm)|Agai|Agai|libre|
|[j7fPCy71EfQL1KmU.htm](kingmaker-bestiary/j7fPCy71EfQL1KmU.htm)|Nyrissa|Nyrissa|libre|
|[jFA0q3g3MsFxS3xO.htm](kingmaker-bestiary/jFA0q3g3MsFxS3xO.htm)|Goblin Bat-Dog|Chien chauve-souris goblin|libre|
|[JLfy3tVKEhLqT2j5.htm](kingmaker-bestiary/JLfy3tVKEhLqT2j5.htm)|Shelyn's Shame|La honte de Shélyn|libre|
|[jmH2nbWRJSYgzx5z.htm](kingmaker-bestiary/jmH2nbWRJSYgzx5z.htm)|Jin Durwhimmer|Jin Durwhimmer|libre|
|[jUNnWUuiVueOTvHt.htm](kingmaker-bestiary/jUNnWUuiVueOTvHt.htm)|The Misbegotten Troll|Troll Bâtard|libre|
|[K61HTqmdHRPHQz1x.htm](kingmaker-bestiary/K61HTqmdHRPHQz1x.htm)|Tulvak|Tulvak|libre|
|[k9eR5UwCSrfpPPAv.htm](kingmaker-bestiary/k9eR5UwCSrfpPPAv.htm)|Hateful Hermit|Ermite détestable|libre|
|[kiUlJn4FzWMzgkbW.htm](kingmaker-bestiary/kiUlJn4FzWMzgkbW.htm)|Talon Peak Roc|Roc du Pic des serres|libre|
|[Kmr8s4sEMn365d5M.htm](kingmaker-bestiary/Kmr8s4sEMn365d5M.htm)|Prank Workshop Mitflit|Mitlit de l'atelier à farces|libre|
|[KP7uf1E8CQgzTuQy.htm](kingmaker-bestiary/KP7uf1E8CQgzTuQy.htm)|Nugrah|Nugrah|libre|
|[KqWZZBucIAA1MzjF.htm](kingmaker-bestiary/KqWZZBucIAA1MzjF.htm)|Jubilost (Level 1)|Jubilost (niveau 1)|libre|
|[kSR0D8dLXTlw09NT.htm](kingmaker-bestiary/kSR0D8dLXTlw09NT.htm)|Gedovius|Gédovius|libre|
|[L3q7yQ0jKqH2IWy7.htm](kingmaker-bestiary/L3q7yQ0jKqH2IWy7.htm)|Nightmare Rook|Freux de cauchemard|libre|
|[L6ANJQoCyk5dWcdH.htm](kingmaker-bestiary/L6ANJQoCyk5dWcdH.htm)|Elder Elemental Tsunami|Tsunami élémentaire ancien|libre|
|[LdIVntI4ho9eKTVt.htm](kingmaker-bestiary/LdIVntI4ho9eKTVt.htm)|The Dancing Lady|La Dame dansante|libre|
|[LF67gTrSqSb7h4KZ.htm](kingmaker-bestiary/LF67gTrSqSb7h4KZ.htm)|Boggard Cultist|Cultiste bourbiérin|libre|
|[Lm30gxMpkRJ2Y43d.htm](kingmaker-bestiary/Lm30gxMpkRJ2Y43d.htm)|Satinder Morne|Satindre Morne|libre|
|[lRxISnNmcfUm7AfG.htm](kingmaker-bestiary/lRxISnNmcfUm7AfG.htm)|Grabbles|Tâtons|libre|
|[lutSSPcXOzDDqIGj.htm](kingmaker-bestiary/lutSSPcXOzDDqIGj.htm)|Gurija|Gurija|libre|
|[lZtkMlyix3kaTO0j.htm](kingmaker-bestiary/lZtkMlyix3kaTO0j.htm)|The Beast|La bête|libre|
|[M7hMPdEbaC1RNwfY.htm](kingmaker-bestiary/M7hMPdEbaC1RNwfY.htm)|Kob Moleg|Kob Moleg|libre|
|[m8kwG6NskYDlBSCy.htm](kingmaker-bestiary/m8kwG6NskYDlBSCy.htm)|Ankou Assassin|Assassin Ankou|libre|
|[MABh0eh1VKh3izdf.htm](kingmaker-bestiary/MABh0eh1VKh3izdf.htm)|Eldritch Echoes|Échos mystiques|libre|
|[mbAhAq5OyZAv6lq5.htm](kingmaker-bestiary/mbAhAq5OyZAv6lq5.htm)|Darivan|Darivan|libre|
|[mgh7E2Mh0ZRaniCc.htm](kingmaker-bestiary/mgh7E2Mh0ZRaniCc.htm)|King Vesket|Roi Vesket|libre|
|[MgUBst46K9Hv0qsJ.htm](kingmaker-bestiary/MgUBst46K9Hv0qsJ.htm)|Amiri (Level 1, Kingmaker)|Amiri (niveau 1, Kingmaker)|libre|
|[MlWXttLN4MjyzTGr.htm](kingmaker-bestiary/MlWXttLN4MjyzTGr.htm)|Dropping Web Trap|Piège du pont araignée|libre|
|[mMfMs5PlNYkwe55s.htm](kingmaker-bestiary/mMfMs5PlNYkwe55s.htm)|Alasen|Alasen|libre|
|[mNKAaSBWbZHQRdo9.htm](kingmaker-bestiary/mNKAaSBWbZHQRdo9.htm)|Endless Struggle|Lutte sans fin|libre|
|[MqxA7COGMXc5CNsZ.htm](kingmaker-bestiary/MqxA7COGMXc5CNsZ.htm)|Black Tear Cutthroat|Égorgeur des Larmes noires|libre|
|[MUuXMpUGEnqmElgT.htm](kingmaker-bestiary/MUuXMpUGEnqmElgT.htm)|Vicious Army Ant Swarm|Nuée de fourmis soldats vicieux|libre|
|[myNEeBzXVmWbHk2X.htm](kingmaker-bestiary/myNEeBzXVmWbHk2X.htm)|Azure Lilies|Lys azur|libre|
|[n82GZhM6joceE91v.htm](kingmaker-bestiary/n82GZhM6joceE91v.htm)|Ilthuliak|Ilthuliak|libre|
|[nDFogS2qQJomjfmR.htm](kingmaker-bestiary/nDFogS2qQJomjfmR.htm)|Fetch Stalker|Harceleur fetch|libre|
|[ndoXVn6MPPxSJvcC.htm](kingmaker-bestiary/ndoXVn6MPPxSJvcC.htm)|Black Smilodon|Smilodon noir|libre|
|[ni0RSuVeUgs5WmlY.htm](kingmaker-bestiary/ni0RSuVeUgs5WmlY.htm)|Hillstomper|Écrasecolline|libre|
|[NMCee869vhJjP5Ri.htm](kingmaker-bestiary/NMCee869vhJjP5Ri.htm)|The Wriggling Man|L'Homme grouillant|libre|
|[NSY6VAGs9VrKzyRX.htm](kingmaker-bestiary/NSY6VAGs9VrKzyRX.htm)|Armag Twice-Born|Armag Deux fois né|libre|
|[Nx6vcagWXhYToIdC.htm](kingmaker-bestiary/Nx6vcagWXhYToIdC.htm)|Jurgrindor|Jurgrindor|libre|
|[NxVi4ot2bzNOk6Zj.htm](kingmaker-bestiary/NxVi4ot2bzNOk6Zj.htm)|Aecora Silverfire|Aecora Feuargenté|libre|
|[O5EEnXdrKPcODuwh.htm](kingmaker-bestiary/O5EEnXdrKPcODuwh.htm)|Sir Fredero Sinnet|Sieur Frédéro Sinnet|libre|
|[oBPdL0icCG5zmknB.htm](kingmaker-bestiary/oBPdL0icCG5zmknB.htm)|Glyph of Warding (Kingmaker)|Glyphe de garde (Kingmaker)|libre|
|[obT2KQ8YYlRxvSWr.htm](kingmaker-bestiary/obT2KQ8YYlRxvSWr.htm)|Tree that Weeps|Arbre qui pleure|libre|
|[OhuPV1g5LfejhtAz.htm](kingmaker-bestiary/OhuPV1g5LfejhtAz.htm)|The Horned Hunter|Le Chasseur cornu|libre|
|[oM1AvORITfhzwrDk.htm](kingmaker-bestiary/oM1AvORITfhzwrDk.htm)|Barbtongued Wyvern|Vouivre à langue crochue|libre|
|[OnHIutiVLt1czwWL.htm](kingmaker-bestiary/OnHIutiVLt1czwWL.htm)|Wild Hunt Hound|Molosse de la Chasse sauvage|libre|
|[oOPf7VG4tuMvzrgA.htm](kingmaker-bestiary/oOPf7VG4tuMvzrgA.htm)|Dog (Ekundayo's Companion)|Chien (compagnon d'Ékundayo)|libre|
|[oqlZNnsV8XMGn7JN.htm](kingmaker-bestiary/oqlZNnsV8XMGn7JN.htm)|Praise of Yog-Sothoth|Louange de Yog-Sothoth|libre|
|[ovjnD3aiPgRi2C7u.htm](kingmaker-bestiary/ovjnD3aiPgRi2C7u.htm)|The Gardener|Le jardinier|libre|
|[P1kWLRlEPTcZ3uzD.htm](kingmaker-bestiary/P1kWLRlEPTcZ3uzD.htm)|Kellid Graveknight|Chevalier sépulcre kellide|libre|
|[pcct13qdrriJf3OL.htm](kingmaker-bestiary/pcct13qdrriJf3OL.htm)|Akuzhail|Akuzhail|libre|
|[pD5Y7gJtqlr2A4a2.htm](kingmaker-bestiary/pD5Y7gJtqlr2A4a2.htm)|Hargulka|Hargulka|libre|
|[pfvUhfy0VHNlTyvN.htm](kingmaker-bestiary/pfvUhfy0VHNlTyvN.htm)|Immense Mandragora|Immense mandragore|libre|
|[PK8yBANFyMqFZ3IY.htm](kingmaker-bestiary/PK8yBANFyMqFZ3IY.htm)|Zorek|Zorek|libre|
|[pT5hfxcsG7eV5oxh.htm](kingmaker-bestiary/pT5hfxcsG7eV5oxh.htm)|Niodrhast|Niodrhast|libre|
|[pZosztihhMtCLinT.htm](kingmaker-bestiary/pZosztihhMtCLinT.htm)|Wild Hunt Monarch|Monarque de la Chasse sauvage|libre|
|[QF2AIby1vQRq5b9E.htm](kingmaker-bestiary/QF2AIby1vQRq5b9E.htm)|Ameon Trask|Améon Trask|libre|
|[QkGk4GMq3pCtBbLS.htm](kingmaker-bestiary/QkGk4GMq3pCtBbLS.htm)|Troll Guard|Garde Troll|libre|
|[qLfHY6uNUQ99NZei.htm](kingmaker-bestiary/qLfHY6uNUQ99NZei.htm)|Werendegar|Wérendegar|libre|
|[qpkpPFlN0dSKJxaR.htm](kingmaker-bestiary/qpkpPFlN0dSKJxaR.htm)|Breeg's Traps|Pièges de Breeg|libre|
|[qS7JwIPqsjNKKALK.htm](kingmaker-bestiary/qS7JwIPqsjNKKALK.htm)|The First Faithful|Le Premier fidèle|libre|
|[RfIipAkVucpR0f0x.htm](kingmaker-bestiary/RfIipAkVucpR0f0x.htm)|Oleg|Oleg|libre|
|[RL6cxasbeQMtCDvV.htm](kingmaker-bestiary/RL6cxasbeQMtCDvV.htm)|Chief Sootscale|Chef Écaille de suie|libre|
|[RN3Fiz9AZzUuqb9z.htm](kingmaker-bestiary/RN3Fiz9AZzUuqb9z.htm)|Smoke-Filled Hallway|Couloir enfumé|libre|
|[rnFjLc6xYYPtXS6a.htm](kingmaker-bestiary/rnFjLc6xYYPtXS6a.htm)|Defaced Naiad Queen|Naïade souveraine défigurée|libre|
|[rsm5ZSX6oKJWQRvf.htm](kingmaker-bestiary/rsm5ZSX6oKJWQRvf.htm)|Nyrissa's Tempest|Tempête de Nyrissa|libre|
|[RTd4FwqGq8gBjdAO.htm](kingmaker-bestiary/RTd4FwqGq8gBjdAO.htm)|Melianse|Mélianse|libre|
|[rUmPNDqvptyp5Ob4.htm](kingmaker-bestiary/rUmPNDqvptyp5Ob4.htm)|Test of Tactics|Test de Stratégie|libre|
|[s0BfmFWAhLQkQEbg.htm](kingmaker-bestiary/s0BfmFWAhLQkQEbg.htm)|Kargstaad's Giant|Les géants de Kargstaad|libre|
|[S3jESLRaGeoaHG7t.htm](kingmaker-bestiary/S3jESLRaGeoaHG7t.htm)|Exploding Bloom Pods|Gousses explosives|libre|
|[S4oIMaVPzQuRaTpK.htm](kingmaker-bestiary/S4oIMaVPzQuRaTpK.htm)|Phomandala|Phomandale|libre|
|[SaSXYUpSWIFHIzET.htm](kingmaker-bestiary/SaSXYUpSWIFHIzET.htm)|Pitax Warden|Gardien du Pitax|libre|
|[sedubjznhIbVfCkD.htm](kingmaker-bestiary/sedubjznhIbVfCkD.htm)|Sepoko|Sépoko|libre|
|[sF7JAULkMpi1QMz2.htm](kingmaker-bestiary/sF7JAULkMpi1QMz2.htm)|Amiri (Level 11, Kingmaker)|Amiri (Level 11, Kingmaker)|libre|
|[SfFMqKTUQ1Dwu5lT.htm](kingmaker-bestiary/SfFMqKTUQ1Dwu5lT.htm)|Whimwyrm|Dragon fantasque|libre|
|[SjU0oB6pOk0XY8VN.htm](kingmaker-bestiary/SjU0oB6pOk0XY8VN.htm)|Minognos-Ushad|Minognos-Ushad|libre|
|[sMCEMlNngFINMX8y.htm](kingmaker-bestiary/sMCEMlNngFINMX8y.htm)|Elga Verniex|Elga Verniex|libre|
|[sNqAajzeDA9BUkfa.htm](kingmaker-bestiary/sNqAajzeDA9BUkfa.htm)|Explosion Bear|Ours explosif|libre|
|[so1XCkNLe2tuNbzW.htm](kingmaker-bestiary/so1XCkNLe2tuNbzW.htm)|Tristian (Level 10)|Tristian (niveau 10)|libre|
|[SVUjDSZHYmwbQgnq.htm](kingmaker-bestiary/SVUjDSZHYmwbQgnq.htm)|The Knurly Witch|La Sorcière noueuse|libre|
|[sw32QdZlsWnmWaVY.htm](kingmaker-bestiary/sw32QdZlsWnmWaVY.htm)|Mastiff Of Tindalos|Dogue de Tindalos|libre|
|[sY8owbk9TFeygFL9.htm](kingmaker-bestiary/sY8owbk9TFeygFL9.htm)|Nok-Nok (Level 1)|Nok-Nok (niveau 1)|libre|
|[SZVTHAwTVXDwIOqC.htm](kingmaker-bestiary/SZVTHAwTVXDwIOqC.htm)|Gaetane|Gaétane|libre|
|[TAEgPgivuUOyuEU5.htm](kingmaker-bestiary/TAEgPgivuUOyuEU5.htm)|Falling Portcullis|Piège de la herse tombante|libre|
|[TfwvPnETjUhEUQ82.htm](kingmaker-bestiary/TfwvPnETjUhEUQ82.htm)|Trapped Portcullis|Herse piégée|libre|
|[tjq87ghubOcPAXjj.htm](kingmaker-bestiary/tjq87ghubOcPAXjj.htm)|Fetch Behemoth|Fetch béhémoth|libre|
|[TLoNfIIhS7YGdV54.htm](kingmaker-bestiary/TLoNfIIhS7YGdV54.htm)|Thylacine|Thylacine|libre|
|[TOW9azHYIoaNSavI.htm](kingmaker-bestiary/TOW9azHYIoaNSavI.htm)|Hidden Pressure Plate|Plaque de pression cachée|libre|
|[tq87VRZjkGBmW8kf.htm](kingmaker-bestiary/tq87VRZjkGBmW8kf.htm)|Rezatha|Rézatha|libre|
|[UAlHSl6Cpujld1dx.htm](kingmaker-bestiary/UAlHSl6Cpujld1dx.htm)|Logger|Bûcheron|libre|
|[UBwmJpIyIV65U7R2.htm](kingmaker-bestiary/UBwmJpIyIV65U7R2.htm)|Kressle|Kressle|libre|
|[UfB3NfSgZIkN5Rjx.htm](kingmaker-bestiary/UfB3NfSgZIkN5Rjx.htm)|Cleansed Cultist|Cultistes purifiés|libre|
|[ugzdSsP9U0gGLZ3v.htm](kingmaker-bestiary/ugzdSsP9U0gGLZ3v.htm)|Rigg Gargadilly|Rigg Gargadilly|libre|
|[uLzD70CB7Bh2XxQf.htm](kingmaker-bestiary/uLzD70CB7Bh2XxQf.htm)|Shambler|Tertre errant|libre|
|[UPESZZbXchcuqI1r.htm](kingmaker-bestiary/UPESZZbXchcuqI1r.htm)|Engelidis|Engélidis|libre|
|[uQbzVX7DWDbxLONd.htm](kingmaker-bestiary/uQbzVX7DWDbxLONd.htm)|Paranoia Well|Puits de paranoïa|libre|
|[UXXEOnvp2MDaS9Sc.htm](kingmaker-bestiary/UXXEOnvp2MDaS9Sc.htm)|Vilderavn Herald|Héraut vilderavn|libre|
|[v2cJC9tdjRHexMwa.htm](kingmaker-bestiary/v2cJC9tdjRHexMwa.htm)|Irahkatu|Irahkatu|libre|
|[V7FoP8iIcehuiF20.htm](kingmaker-bestiary/V7FoP8iIcehuiF20.htm)|Cursed Guardian|Gardien maudit|libre|
|[vBugpZnpxQcrrWoo.htm](kingmaker-bestiary/vBugpZnpxQcrrWoo.htm)|Bloom of Lamashtu|Fleur de Lamashtu|libre|
|[VEWrS5u71szMrhs4.htm](kingmaker-bestiary/VEWrS5u71szMrhs4.htm)|Quintessa Maray|Quintessa Maray|libre|
|[vff5VzjJpRMmg4Hx.htm](kingmaker-bestiary/vff5VzjJpRMmg4Hx.htm)|Cephal Lorentus|Céphal Lorentus|libre|
|[vONZlReozVCabXhq.htm](kingmaker-bestiary/vONZlReozVCabXhq.htm)|Collapsing Floor|Effondrement du plancher|libre|
|[vrZrha0Gz14Zd4tA.htm](kingmaker-bestiary/vrZrha0Gz14Zd4tA.htm)|Korog|Korog|libre|
|[vs8QT4LYEcQfA6Us.htm](kingmaker-bestiary/vs8QT4LYEcQfA6Us.htm)|Skeletal Tiger Lord|Seigneur tigre squelette|libre|
|[VSffsyt5RONB4k2U.htm](kingmaker-bestiary/VSffsyt5RONB4k2U.htm)|General Avinash Jurrg|Général Avinash Jurrg|libre|
|[vTzbfxtvhhmS7KWr.htm](kingmaker-bestiary/vTzbfxtvhhmS7KWr.htm)|Bloom Wyvern|Vouivre de la Fleur|libre|
|[w8jUzPPGLQECT4j7.htm](kingmaker-bestiary/w8jUzPPGLQECT4j7.htm)|Overgrown Viper Vine|Liane-serpent évolué|libre|
|[wD6nctHffSaMdyag.htm](kingmaker-bestiary/wD6nctHffSaMdyag.htm)|Lizardfolk Warrior|Homme-lézard guerrier|libre|
|[wdoRpYImTQuVGZSQ.htm](kingmaker-bestiary/wdoRpYImTQuVGZSQ.htm)|Fionn|Fionn|libre|
|[WGHV95WkNnlY70Sn.htm](kingmaker-bestiary/WGHV95WkNnlY70Sn.htm)|Lintwerth|Lintwerth|libre|
|[wHQmyXnG4Yax4KcK.htm](kingmaker-bestiary/wHQmyXnG4Yax4KcK.htm)|Enormous Dragonfly|Libellule géante évoluée|libre|
|[WJ0bMCZUHJVwKYG1.htm](kingmaker-bestiary/WJ0bMCZUHJVwKYG1.htm)|Freshly Bloomed Basilisk|Nouvelle fleur de Basilic|libre|
|[wJDtBtvRtyxtyqHS.htm](kingmaker-bestiary/wJDtBtvRtyxtyqHS.htm)|Kargstaad|Kargstaad|libre|
|[wMfnwJoLvxdZZAwr.htm](kingmaker-bestiary/wMfnwJoLvxdZZAwr.htm)|Drelev Guards|Garde de Drélev|libre|
|[WmSZELnHCZ9g9Nq2.htm](kingmaker-bestiary/WmSZELnHCZ9g9Nq2.htm)|Svetlana|Svetlana|libre|
|[Wyqsf3qDt7PqQ8OM.htm](kingmaker-bestiary/Wyqsf3qDt7PqQ8OM.htm)|Brush Thylacine|Thylacine du maquis|libre|
|[xMuLJmx51eBv9FcE.htm](kingmaker-bestiary/xMuLJmx51eBv9FcE.htm)|Jewel|Bijou|libre|
|[XOiNuunTFGbDYeu2.htm](kingmaker-bestiary/XOiNuunTFGbDYeu2.htm)|Lights of the Lost|La lumière des Perdus|libre|
|[xqTVUqUs5UsGcSuH.htm](kingmaker-bestiary/xqTVUqUs5UsGcSuH.htm)|Winged Owlbear|Hibours ailé|libre|
|[XQyXoOe7FCpZilaF.htm](kingmaker-bestiary/XQyXoOe7FCpZilaF.htm)|Lesser Jabberwock|Jabberwocky mineur|libre|
|[XydYK1C7RFDqKine.htm](kingmaker-bestiary/XydYK1C7RFDqKine.htm)|Imeckus Stroon|Imeckus Stroon|libre|
|[XzFifthQr0V5nEJe.htm](kingmaker-bestiary/XzFifthQr0V5nEJe.htm)|Ilora Nuski|Ilora Nuski|libre|
|[y5W2rrHzQmeSE6LU.htm](kingmaker-bestiary/y5W2rrHzQmeSE6LU.htm)|Happs Bydon|Happs Bydon|libre|
|[y68kqNXmr1BnjZtc.htm](kingmaker-bestiary/y68kqNXmr1BnjZtc.htm)|Oversized Chimera|Chimère évoluée|libre|
|[Y9N8MaTYupFhCUuN.htm](kingmaker-bestiary/Y9N8MaTYupFhCUuN.htm)|Jaggedbriar Hag|Guenaude Jaggedbriar|libre|
|[YFnWOu9edWhj6vV6.htm](kingmaker-bestiary/YFnWOu9edWhj6vV6.htm)|Akiros Ismort|Akiros Ismort|libre|
|[ylMXDs2f7C0YKdZY.htm](kingmaker-bestiary/ylMXDs2f7C0YKdZY.htm)|Xae|Xaë|libre|
|[Ypxj2FdUPQqpWPf3.htm](kingmaker-bestiary/Ypxj2FdUPQqpWPf3.htm)|Ntavi|Ntavi|libre|
|[YyUVzTucO99JFDnm.htm](kingmaker-bestiary/YyUVzTucO99JFDnm.htm)|Rigged Climbing Loops|Poignées d'escalade piégées|libre|
|[yzlu5YX7Oxo7TVvK.htm](kingmaker-bestiary/yzlu5YX7Oxo7TVvK.htm)|Prazil|Prazil|libre|
|[z1Hk6z6RU4sF9aJU.htm](kingmaker-bestiary/z1Hk6z6RU4sF9aJU.htm)|Thresholder Disciple|Disciple du Seuil|libre|
|[ZaA9oQXOWne0IXSG.htm](kingmaker-bestiary/ZaA9oQXOWne0IXSG.htm)|Linzi (Level 1)|Linzi (Niveau 1)|libre|
|[ZbgFOoDCOXqhJWhz.htm](kingmaker-bestiary/ZbgFOoDCOXqhJWhz.htm)|Ngara|Ngara|libre|
|[ZDt1rLh1VUFnHj5S.htm](kingmaker-bestiary/ZDt1rLh1VUFnHj5S.htm)|Lickweed|Lèche-feuille|libre|
|[ZEpByav3dMCyvJJu.htm](kingmaker-bestiary/ZEpByav3dMCyvJJu.htm)|Void Pit|Puits du néant|libre|
|[ZGVQcWl03kBIStS0.htm](kingmaker-bestiary/ZGVQcWl03kBIStS0.htm)|Kundal|Kundal|libre|
|[ZiNVsXL5DJ4Ekd5v.htm](kingmaker-bestiary/ZiNVsXL5DJ4Ekd5v.htm)|Breath of Despair|Piège du souffle de désespoir|libre|
|[ZlEhOqdwPDpU3jvO.htm](kingmaker-bestiary/ZlEhOqdwPDpU3jvO.htm)|Stinging Nettles|Piège des orties|libre|
|[ZoUIXzygFDuHKebr.htm](kingmaker-bestiary/ZoUIXzygFDuHKebr.htm)|Spirit of Stisshak|Esprit de Stisshak|libre|
|[ZPm7WqgQTjysuTiT.htm](kingmaker-bestiary/ZPm7WqgQTjysuTiT.htm)|False Priestess|Fausse prêtresse|libre|
|[zvhw5Qx6gU7e39he.htm](kingmaker-bestiary/zvhw5Qx6gU7e39he.htm)|Linzi (Level 7)|Linzi (Niveau 7)|libre|
|[zZrKk6wh7av4nU1z.htm](kingmaker-bestiary/zZrKk6wh7av4nU1z.htm)|Phantasmagoric Fog Trap|Piège de brouillard fantasmagorique|libre|
