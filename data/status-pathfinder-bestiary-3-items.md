# État de la traduction (pathfinder-bestiary-3-items)

 * **libre**: 3080
 * **officielle**: 235
 * **changé**: 89


Dernière mise à jour: 2023-07-12 20:28 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[1Nzt9YANcjiUjxZu.htm](pathfinder-bestiary-3-items/1Nzt9YANcjiUjxZu.htm)|Liquefy|Liquéfier|changé|
|[2cVkSactyUiKGzdD.htm](pathfinder-bestiary-3-items/2cVkSactyUiKGzdD.htm)|Backdrop|Décor|changé|
|[40daZI7TkA0gZGz3.htm](pathfinder-bestiary-3-items/40daZI7TkA0gZGz3.htm)|Divine Innate Spells|Sorts divins innés|changé|
|[4XdLVNceTaTeWXxs.htm](pathfinder-bestiary-3-items/4XdLVNceTaTeWXxs.htm)|Weak Acid|Acide faible|changé|
|[5OmSIpE30Qir6OKb.htm](pathfinder-bestiary-3-items/5OmSIpE30Qir6OKb.htm)|Tremorsense (Imprecise) 60 feet|Perception des vibrations 18 m (imprécis)|changé|
|[6Xmm3pc3idD2x94u.htm](pathfinder-bestiary-3-items/6Xmm3pc3idD2x94u.htm)|Create Water (At Will)|Création d'eau (À volonté)|changé|
|[A7emqw5XPEaVatWu.htm](pathfinder-bestiary-3-items/A7emqw5XPEaVatWu.htm)|Constant Spells|Sorts constants|changé|
|[af7dgcRuc5gWm1ej.htm](pathfinder-bestiary-3-items/af7dgcRuc5gWm1ej.htm)|At-Will Spells|Sorts à volonté|changé|
|[aK9UmOvlCoa0e4uF.htm](pathfinder-bestiary-3-items/aK9UmOvlCoa0e4uF.htm)|Arcane Innate Spells|Sorts arcaniques innés|changé|
|[aT8anQIUKNtDMO8Y.htm](pathfinder-bestiary-3-items/aT8anQIUKNtDMO8Y.htm)|Arcane Innate Spells|Sorts arcaniques innés|changé|
|[atierys9eTCKzPyz.htm](pathfinder-bestiary-3-items/atierys9eTCKzPyz.htm)|Greater Constrict|Constriction supérieure|changé|
|[Bpok96twh3ZtuzEH.htm](pathfinder-bestiary-3-items/Bpok96twh3ZtuzEH.htm)|Wavesense (Imprecise) 60 feet|Perception des ondes 18 m (imprécis)|changé|
|[CFh42XjjfBF8zZRE.htm](pathfinder-bestiary-3-items/CFh42XjjfBF8zZRE.htm)|Scent (Imprecise) 60 feet|Odorat 18 m (imprécis)|changé|
|[cPRyXJDPZFJ7bHKq.htm](pathfinder-bestiary-3-items/cPRyXJDPZFJ7bHKq.htm)|Sweltering Heat|Chaleur étouffante|changé|
|[CZrjwV0LG9oK1IpV.htm](pathfinder-bestiary-3-items/CZrjwV0LG9oK1IpV.htm)|Countered by Fire|Contré par le feu|changé|
|[dahd3jKunJZarMSO.htm](pathfinder-bestiary-3-items/dahd3jKunJZarMSO.htm)|Frightful Presence|Présence terrifiante|changé|
|[DjRAEGwWJTNcjr6z.htm](pathfinder-bestiary-3-items/DjRAEGwWJTNcjr6z.htm)|Draining Blight|Dégradation drainante|changé|
|[DLKqB3QVep4jRNGS.htm](pathfinder-bestiary-3-items/DLKqB3QVep4jRNGS.htm)|Truescript|Pictomorphie|changé|
|[Dy4ItmxMvWZNtvIJ.htm](pathfinder-bestiary-3-items/Dy4ItmxMvWZNtvIJ.htm)|Surface-Bound|Lié à la surface|changé|
|[E3tmyHUT7v0BSgAP.htm](pathfinder-bestiary-3-items/E3tmyHUT7v0BSgAP.htm)|Greater Constrict|Constriction supérieure|changé|
|[EHAajCEP6flvt7fx.htm](pathfinder-bestiary-3-items/EHAajCEP6flvt7fx.htm)|Lignifying Bite|Morsure lignifiante|changé|
|[eogxSxYx5HC0X9Tc.htm](pathfinder-bestiary-3-items/eogxSxYx5HC0X9Tc.htm)|Jaws|Mâchoires|changé|
|[eqx6f68rVSfojXpJ.htm](pathfinder-bestiary-3-items/eqx6f68rVSfojXpJ.htm)|Hyponatremia|Hyponatrémie|changé|
|[eT3PtWf4uiMjHI7j.htm](pathfinder-bestiary-3-items/eT3PtWf4uiMjHI7j.htm)|Perfected Flight|Vol parfait|changé|
|[eTWugVatqxe8rLdZ.htm](pathfinder-bestiary-3-items/eTWugVatqxe8rLdZ.htm)|Scent (Imprecise) 60 feet|Odorat 18 m (imprécis)|changé|
|[f44ALHTlp3xIkYpz.htm](pathfinder-bestiary-3-items/f44ALHTlp3xIkYpz.htm)|Tree Shape (See Forest Shape)|Morphologie d'arbre (voir Morphologie de forêt)|changé|
|[fbcWIR3VTHaRmvLw.htm](pathfinder-bestiary-3-items/fbcWIR3VTHaRmvLw.htm)|Swarming Slither|Nuée rampante|changé|
|[flo5oC8zWtFBUsvt.htm](pathfinder-bestiary-3-items/flo5oC8zWtFBUsvt.htm)|Jaws|Mâchoires|changé|
|[FPfOOgSVW55hbKb0.htm](pathfinder-bestiary-3-items/FPfOOgSVW55hbKb0.htm)|Splatter|Éclaboussure|changé|
|[GPZIIPDmUwYRcaYF.htm](pathfinder-bestiary-3-items/GPZIIPDmUwYRcaYF.htm)|Grab|Empoignade/Agrippement|changé|
|[GZIzUrSeCXXcCvhZ.htm](pathfinder-bestiary-3-items/GZIzUrSeCXXcCvhZ.htm)|At-Will Spells|Sorts à volonté|changé|
|[h2v7VpyXTPjTzVX1.htm](pathfinder-bestiary-3-items/h2v7VpyXTPjTzVX1.htm)|Smoke Vision|Vision malgré la fumée|changé|
|[h4zO0kK45DmIdXa1.htm](pathfinder-bestiary-3-items/h4zO0kK45DmIdXa1.htm)|Grab|Empoignade/Agrippement|changé|
|[Hi0AFeSVBBw7vuPF.htm](pathfinder-bestiary-3-items/Hi0AFeSVBBw7vuPF.htm)|Suction|Succion|changé|
|[hJYdbtet0tqbLviV.htm](pathfinder-bestiary-3-items/hJYdbtet0tqbLviV.htm)|Fed by Metal|Renforcé par le métal|changé|
|[HKJDIUhv4yxNRcOz.htm](pathfinder-bestiary-3-items/HKJDIUhv4yxNRcOz.htm)|Grab|Empoignade/Agrippement|changé|
|[HofMxPk6h7wuflyA.htm](pathfinder-bestiary-3-items/HofMxPk6h7wuflyA.htm)|Stealth|Discrétion|changé|
|[iewmwrowaGzoIyTr.htm](pathfinder-bestiary-3-items/iewmwrowaGzoIyTr.htm)|Woodland Stride|Déplacement facilité en forêt|changé|
|[IjkPvQ204dD2FWaQ.htm](pathfinder-bestiary-3-items/IjkPvQ204dD2FWaQ.htm)|Primal Innate Spells|Sorts primordiaux innés|changé|
|[IlJzVorvcYKLmyZM.htm](pathfinder-bestiary-3-items/IlJzVorvcYKLmyZM.htm)|Backdrop|Décor|changé|
|[J8ElHZzPwIKugFCK.htm](pathfinder-bestiary-3-items/J8ElHZzPwIKugFCK.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|changé|
|[jhyOsq6KuZehFYax.htm](pathfinder-bestiary-3-items/jhyOsq6KuZehFYax.htm)|Pass Without Trace (Constant) (Forest Terrain Only)|Passage sans trace (constant) (en forêt seulement)|changé|
|[KPwsQn8bO8tcLOgw.htm](pathfinder-bestiary-3-items/KPwsQn8bO8tcLOgw.htm)|Frightful Presence|Présence terrifiante|changé|
|[kry7QWW5PONsNRlw.htm](pathfinder-bestiary-3-items/kry7QWW5PONsNRlw.htm)|Stealth|Discrétion|changé|
|[LFIyH1YUMUoGUmPX.htm](pathfinder-bestiary-3-items/LFIyH1YUMUoGUmPX.htm)|Countered by Metal|Contré par le métal|changé|
|[ljmD4eOFLneTqsb9.htm](pathfinder-bestiary-3-items/ljmD4eOFLneTqsb9.htm)|At-Will Spells|Sorts à volonté|changé|
|[LorLNjRUhElv6hVC.htm](pathfinder-bestiary-3-items/LorLNjRUhElv6hVC.htm)|Tail|Queue|changé|
|[lQ9VDly9F5WovfxG.htm](pathfinder-bestiary-3-items/lQ9VDly9F5WovfxG.htm)|Tail|Queue|changé|
|[Lscs4sD2vIZEzZIX.htm](pathfinder-bestiary-3-items/Lscs4sD2vIZEzZIX.htm)|Tail|Queue|changé|
|[LupnDs04lfZkJ52r.htm](pathfinder-bestiary-3-items/LupnDs04lfZkJ52r.htm)|Scent (Imprecise) 60 feet|Odorat 18 m (imprécis)|changé|
|[mFrFiI59EQmtyVk8.htm](pathfinder-bestiary-3-items/mFrFiI59EQmtyVk8.htm)|Scorch Earth|Terre brûlée|changé|
|[Mn6J9sGKxgEPsSFM.htm](pathfinder-bestiary-3-items/Mn6J9sGKxgEPsSFM.htm)|Breath Weapon|Arme de souffle|changé|
|[mqyNB4TXgzDtKJ3Q.htm](pathfinder-bestiary-3-items/mqyNB4TXgzDtKJ3Q.htm)|Fed by Earth|Renforcé par la terre|changé|
|[MTzVT8iMpCahyDa9.htm](pathfinder-bestiary-3-items/MTzVT8iMpCahyDa9.htm)|Stealth|Discrétion|changé|
|[O9kHIQhPaga6ZnIu.htm](pathfinder-bestiary-3-items/O9kHIQhPaga6ZnIu.htm)|Speak with Plants (At Will)|Communication avec les animaux (À volonté)|changé|
|[OazlOifRnQFSPc2g.htm](pathfinder-bestiary-3-items/OazlOifRnQFSPc2g.htm)|Fire Healing|Guérison par le feu|changé|
|[OGIF0iSMoUfXZTlW.htm](pathfinder-bestiary-3-items/OGIF0iSMoUfXZTlW.htm)|Breath Weapon|Arme de souffle|changé|
|[OLXpGW8u6XmpJXAp.htm](pathfinder-bestiary-3-items/OLXpGW8u6XmpJXAp.htm)|Swarming Bites|Morsures de nuée|changé|
|[Oo1ff4ERhvPS7Bhw.htm](pathfinder-bestiary-3-items/Oo1ff4ERhvPS7Bhw.htm)|Frightful Presence|Présence terrifiante|changé|
|[OshjLmAWIWTiJZoR.htm](pathfinder-bestiary-3-items/OshjLmAWIWTiJZoR.htm)|Adamantine Claws|Griffes en adamantium|changé|
|[PO5OyKjxeT0rYaYl.htm](pathfinder-bestiary-3-items/PO5OyKjxeT0rYaYl.htm)|Jaws|Mâchoires|changé|
|[PuvbyIbJk14Uho2a.htm](pathfinder-bestiary-3-items/PuvbyIbJk14Uho2a.htm)|Tremorsense 60 feet|Perception des vibrations 18 m|changé|
|[Qew18sYA4NJJ2n7y.htm](pathfinder-bestiary-3-items/Qew18sYA4NJJ2n7y.htm)|Fed by Water|Renforcé par l'eau|changé|
|[r5rP9a4eNdlmkjiO.htm](pathfinder-bestiary-3-items/r5rP9a4eNdlmkjiO.htm)|Claw|Griffe|changé|
|[Ram97nW6kaJ9IBc0.htm](pathfinder-bestiary-3-items/Ram97nW6kaJ9IBc0.htm)|Constant Spells|Sorts constants|changé|
|[RdiP9Lesck92HsdO.htm](pathfinder-bestiary-3-items/RdiP9Lesck92HsdO.htm)|Forest Shape|Morphologie de forêt|changé|
|[rvv40f4QmFMFlF5L.htm](pathfinder-bestiary-3-items/rvv40f4QmFMFlF5L.htm)|Jaws|Mâchoires|changé|
|[rYubtl6wVH1V3yQM.htm](pathfinder-bestiary-3-items/rYubtl6wVH1V3yQM.htm)|Surface-Bound|Lié à la surface|changé|
|[Sa5PkHx5wTuKIBts.htm](pathfinder-bestiary-3-items/Sa5PkHx5wTuKIBts.htm)|Constrict|Constriction|changé|
|[sm8dNJYFVglgLjGA.htm](pathfinder-bestiary-3-items/sm8dNJYFVglgLjGA.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|changé|
|[Tp8OXXguuWM7iKK6.htm](pathfinder-bestiary-3-items/Tp8OXXguuWM7iKK6.htm)|Electrical Blast|Explosion électrique|changé|
|[uLElA6gc5JAsMKBq.htm](pathfinder-bestiary-3-items/uLElA6gc5JAsMKBq.htm)|Swarm Mind|Esprit de la nuée|changé|
|[uN7b49VuSYz5eKNd.htm](pathfinder-bestiary-3-items/uN7b49VuSYz5eKNd.htm)|Water Walk (Constant)|Marche sur l'eau (constant)|changé|
|[uuZFA4waTTmjGuf7.htm](pathfinder-bestiary-3-items/uuZFA4waTTmjGuf7.htm)|Claw|Griffe|changé|
|[vDVTFesUl1YqnWqr.htm](pathfinder-bestiary-3-items/vDVTFesUl1YqnWqr.htm)|Claw|Griffe|changé|
|[vfThatLsAEQPDKBL.htm](pathfinder-bestiary-3-items/vfThatLsAEQPDKBL.htm)|Claw|Griffe|changé|
|[Vvhg9eA8gPTjQD32.htm](pathfinder-bestiary-3-items/Vvhg9eA8gPTjQD32.htm)|Grab|Empoignade/Agrippement|changé|
|[VW8DMx7itpBNELJa.htm](pathfinder-bestiary-3-items/VW8DMx7itpBNELJa.htm)|Tail|Queue|changé|
|[VxrL8PMJq1Vfet9z.htm](pathfinder-bestiary-3-items/VxrL8PMJq1Vfet9z.htm)|Countered by Earth|Contré par la terre|changé|
|[WaigNlDjE6reTIhN.htm](pathfinder-bestiary-3-items/WaigNlDjE6reTIhN.htm)|Mist Vision|Vision malgré la brume|changé|
|[WMiSz943Mp34MPxS.htm](pathfinder-bestiary-3-items/WMiSz943Mp34MPxS.htm)|Countered by Water|Contré par l'eau|changé|
|[Y9Hwv9W7YTv5qXrI.htm](pathfinder-bestiary-3-items/Y9Hwv9W7YTv5qXrI.htm)|Endure Elements (Self Only)|Endurance aux éléments (Soi uniquement)|changé|
|[YBLgLADsWKQWYwXJ.htm](pathfinder-bestiary-3-items/YBLgLADsWKQWYwXJ.htm)|Breath Weapon|Arme de souffle|changé|
|[yiRwJtMI4udvjxuF.htm](pathfinder-bestiary-3-items/yiRwJtMI4udvjxuF.htm)|Frightful Presence|Présence terrifiante|changé|
|[YRKU8F7zH6qq03Om.htm](pathfinder-bestiary-3-items/YRKU8F7zH6qq03Om.htm)|Constant Spells|Sorts constants|changé|
|[YwO8ViIiTT0pBxLd.htm](pathfinder-bestiary-3-items/YwO8ViIiTT0pBxLd.htm)|Splatter|Frappe éclaboussante|changé|
|[z19VI7TL2KhjANdt.htm](pathfinder-bestiary-3-items/z19VI7TL2KhjANdt.htm)|Constrict|Constriction|changé|
|[ZFhHkksQnaNOI9Rk.htm](pathfinder-bestiary-3-items/ZFhHkksQnaNOI9Rk.htm)|Fed by Wood|Renforcé par le bois|changé|
|[zZ9i2cbCfyyfJXBh.htm](pathfinder-bestiary-3-items/zZ9i2cbCfyyfJXBh.htm)|Breath Weapon|Arme de souffle|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[01e3LF3IBDnqhKXi.htm](pathfinder-bestiary-3-items/01e3LF3IBDnqhKXi.htm)|Magic Aura (Constant) (Shaukeen and its Items Only)|Aura magique (constant) (shaukine et ses objets uniquement)|libre|
|[025oERI6JXgOoPWL.htm](pathfinder-bestiary-3-items/025oERI6JXgOoPWL.htm)|Pass Without Trace (Constant)|Passage sans trace (constant)|officielle|
|[04WUnuVaMwggkQxr.htm](pathfinder-bestiary-3-items/04WUnuVaMwggkQxr.htm)|Plaque Burst|Éclat de plaque dentaire|libre|
|[06gks6jfRaUwycln.htm](pathfinder-bestiary-3-items/06gks6jfRaUwycln.htm)|Gift of Knowledge|Don de la Connaissance|libre|
|[06mT1ZKXqT7oAL0x.htm](pathfinder-bestiary-3-items/06mT1ZKXqT7oAL0x.htm)|Talon|Serre|libre|
|[06rO4SWWCzcZfK0D.htm](pathfinder-bestiary-3-items/06rO4SWWCzcZfK0D.htm)|Spiny Eurypterid Venom|Venin d'euryptéride|libre|
|[08t9rJT8yGrqWVpw.htm](pathfinder-bestiary-3-items/08t9rJT8yGrqWVpw.htm)|Darkvision|Vision dans le noir|libre|
|[08TcEyoA4GxnmNBv.htm](pathfinder-bestiary-3-items/08TcEyoA4GxnmNBv.htm)|Raise Grain Cloud|Soulever un nuage de grains|libre|
|[0a3piFjb7YtsWrsN.htm](pathfinder-bestiary-3-items/0a3piFjb7YtsWrsN.htm)|Uncanny Pounce|Bond troublant|libre|
|[0AymbzLegfs9ibC8.htm](pathfinder-bestiary-3-items/0AymbzLegfs9ibC8.htm)|Tied to the Land|Liée à la région|libre|
|[0BvqGo4aw3xH9SrP.htm](pathfinder-bestiary-3-items/0BvqGo4aw3xH9SrP.htm)|Fan the Flames|Attiser les flammes|libre|
|[0CCLws9uCZ7vqR3l.htm](pathfinder-bestiary-3-items/0CCLws9uCZ7vqR3l.htm)|Air Walk (At Will)|Marche dans les airs (À volonté)|officielle|
|[0cGHRE1RTMbDwp5J.htm](pathfinder-bestiary-3-items/0cGHRE1RTMbDwp5J.htm)|Tremorsense (Imprecise) 15 feet|Perception des vibrations (imprecise) 4,5 m|libre|
|[0cUUUVCjDDYMeTyQ.htm](pathfinder-bestiary-3-items/0cUUUVCjDDYMeTyQ.htm)|Darkvision|Vision dans le noir|libre|
|[0eBQQnVqxlDicgcI.htm](pathfinder-bestiary-3-items/0eBQQnVqxlDicgcI.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[0eEbWDjEXskh1gvK.htm](pathfinder-bestiary-3-items/0eEbWDjEXskh1gvK.htm)|Cold Adaptation|Adaptation au froid|libre|
|[0eyOInV05vjoOU3X.htm](pathfinder-bestiary-3-items/0eyOInV05vjoOU3X.htm)|Blinding Sulfur|Soufre aveuglant|libre|
|[0F9P2pX66pZEF0GL.htm](pathfinder-bestiary-3-items/0F9P2pX66pZEF0GL.htm)|Ruins Lore (Applies Only to Their Home Ruins)|Connaissance des ruines (ne s'applique que sur leurs ruines)|libre|
|[0fDCOqKLdjm45yXu.htm](pathfinder-bestiary-3-items/0fDCOqKLdjm45yXu.htm)|Resonance|Résonance|libre|
|[0fEFXQi4HqLkc4EL.htm](pathfinder-bestiary-3-items/0fEFXQi4HqLkc4EL.htm)|Raise a Shield|Lever un bouclier|libre|
|[0fGH8UJJm4ccTubw.htm](pathfinder-bestiary-3-items/0fGH8UJJm4ccTubw.htm)|Accord Essence|Essence de résonance|libre|
|[0fyQTAFgkuG0R7JH.htm](pathfinder-bestiary-3-items/0fyQTAFgkuG0R7JH.htm)|Stinger|Dard|libre|
|[0FzHP2R1UYaIKxeF.htm](pathfinder-bestiary-3-items/0FzHP2R1UYaIKxeF.htm)|Clairvoyance (At Will)|Clairvoyance (À volonté)|libre|
|[0GjO0CZOnFyUsbIr.htm](pathfinder-bestiary-3-items/0GjO0CZOnFyUsbIr.htm)|Fast Healing 15|Guérison accélérée 15|libre|
|[0Himurdx3RL3D4gk.htm](pathfinder-bestiary-3-items/0Himurdx3RL3D4gk.htm)|Broken Thorns|Épines brisées|libre|
|[0hiycK0RHpWLyxZG.htm](pathfinder-bestiary-3-items/0hiycK0RHpWLyxZG.htm)|No Breath|Ne respire pas|libre|
|[0Hotofas3viOSSly.htm](pathfinder-bestiary-3-items/0Hotofas3viOSSly.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[0hSL8u2ImY5BTyJo.htm](pathfinder-bestiary-3-items/0hSL8u2ImY5BTyJo.htm)|Drain Life|Drain de vie|libre|
|[0Iag5l05kpRuaYEj.htm](pathfinder-bestiary-3-items/0Iag5l05kpRuaYEj.htm)|Philanthropic Bile|Bile philantropique|libre|
|[0IiLvdj1EuziMc4L.htm](pathfinder-bestiary-3-items/0IiLvdj1EuziMc4L.htm)|Tongues (Constant)|Don des langues (constant)|officielle|
|[0IOuDjFm6iLT2q5x.htm](pathfinder-bestiary-3-items/0IOuDjFm6iLT2q5x.htm)|Vanish into the Wilds|Disparaît dans la nature|libre|
|[0jNl0jg5W1N5NrTS.htm](pathfinder-bestiary-3-items/0jNl0jg5W1N5NrTS.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[0k3Jmkc79tf6HAi0.htm](pathfinder-bestiary-3-items/0k3Jmkc79tf6HAi0.htm)|All-Around Vision|Vision panoramique|libre|
|[0kBV6c4sgKTQp33O.htm](pathfinder-bestiary-3-items/0kBV6c4sgKTQp33O.htm)|Talon|Serre|libre|
|[0LkFUNpvQRTipNKI.htm](pathfinder-bestiary-3-items/0LkFUNpvQRTipNKI.htm)|Constrict|Constriction|libre|
|[0LnVcBoOwSUOZfMh.htm](pathfinder-bestiary-3-items/0LnVcBoOwSUOZfMh.htm)|Scent (Imprecise) 60 feet|Odorat 18 m (imprécis)|libre|
|[0lXyiooxNtOuTiyT.htm](pathfinder-bestiary-3-items/0lXyiooxNtOuTiyT.htm)|Dimension Door (At Will)|Porte dimensionnelle (À volonté)|officielle|
|[0nLSNrzLY7to9xEI.htm](pathfinder-bestiary-3-items/0nLSNrzLY7to9xEI.htm)|Form Up|Se reformer|libre|
|[0NvY0SLrHGDIxxdQ.htm](pathfinder-bestiary-3-items/0NvY0SLrHGDIxxdQ.htm)|Darkvision|Vision dans le noir|libre|
|[0oYv3Dg6IHZomTdm.htm](pathfinder-bestiary-3-items/0oYv3Dg6IHZomTdm.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|libre|
|[0PhWUNCIDMNJ1T2x.htm](pathfinder-bestiary-3-items/0PhWUNCIDMNJ1T2x.htm)|Telepathy (Touch)|Télépathie (Contact)|libre|
|[0PU6xyU0Ewf6vUVD.htm](pathfinder-bestiary-3-items/0PU6xyU0Ewf6vUVD.htm)|Huntblade Brutality|Lame de chasse brutale|libre|
|[0QWcjdtxCTYgd8Bm.htm](pathfinder-bestiary-3-items/0QWcjdtxCTYgd8Bm.htm)|Autonomous Spell|Sort autonome|libre|
|[0R9aK9TliKd2z99X.htm](pathfinder-bestiary-3-items/0R9aK9TliKd2z99X.htm)|Bardic Lore|Connaissance bardique|officielle|
|[0Rn8CE4JcrTxdKqN.htm](pathfinder-bestiary-3-items/0Rn8CE4JcrTxdKqN.htm)|Easy to Call|Facile à invoquer|libre|
|[0taZfsX99OZREL4H.htm](pathfinder-bestiary-3-items/0taZfsX99OZREL4H.htm)|At-Will Spells|Sorts à volonté|libre|
|[0wqFlm8L407f7apV.htm](pathfinder-bestiary-3-items/0wqFlm8L407f7apV.htm)|Grab|Empoignade/Agrippement|libre|
|[0XhFSYYoNSo9Uab7.htm](pathfinder-bestiary-3-items/0XhFSYYoNSo9Uab7.htm)|Claw|Griffe|libre|
|[0yTcCw02hLLf5qMi.htm](pathfinder-bestiary-3-items/0yTcCw02hLLf5qMi.htm)|Change Shape|Changement de forme|libre|
|[0zFvRc93Gl6eANug.htm](pathfinder-bestiary-3-items/0zFvRc93Gl6eANug.htm)|Halberd|+1|Hallebarde +1|libre|
|[0zMlEvkv5OFS1XI2.htm](pathfinder-bestiary-3-items/0zMlEvkv5OFS1XI2.htm)|Low-Light Vision|Vision nocturne|libre|
|[10hI8m7tJ9cKdeoE.htm](pathfinder-bestiary-3-items/10hI8m7tJ9cKdeoE.htm)|Hoof|Sabot|libre|
|[10pooBpkF2WtrqUt.htm](pathfinder-bestiary-3-items/10pooBpkF2WtrqUt.htm)|Consume Organ|Consommation d'organe|libre|
|[135ZjsQICC0n6hMK.htm](pathfinder-bestiary-3-items/135ZjsQICC0n6hMK.htm)|Hoof|Sabot|libre|
|[13p4CNlMthrJMobp.htm](pathfinder-bestiary-3-items/13p4CNlMthrJMobp.htm)|Negative Healing|Guérison négative|libre|
|[18UAUscmHJhznFH9.htm](pathfinder-bestiary-3-items/18UAUscmHJhznFH9.htm)|Society|Société|libre|
|[19AKAkXuEUEtoqkr.htm](pathfinder-bestiary-3-items/19AKAkXuEUEtoqkr.htm)|Recall the Fallen|Rappel des défunts|libre|
|[1a2EGaGWMPDZQqkX.htm](pathfinder-bestiary-3-items/1a2EGaGWMPDZQqkX.htm)|Harvester Poison|Poison récolté|libre|
|[1AH3QAMA4BbIHNxh.htm](pathfinder-bestiary-3-items/1AH3QAMA4BbIHNxh.htm)|Stinger|Dard|libre|
|[1b748L6U33YBgL1V.htm](pathfinder-bestiary-3-items/1b748L6U33YBgL1V.htm)|Undetectable Alignment (Constant)|Alignement indétectable (constant)|libre|
|[1bmJGFD6zAmHJap5.htm](pathfinder-bestiary-3-items/1bmJGFD6zAmHJap5.htm)|Leng Ghoul Fever|Fièvre de goule de Leng|libre|
|[1BVkuiVh37hRA4y8.htm](pathfinder-bestiary-3-items/1BVkuiVh37hRA4y8.htm)|Vent Energy|Évacuation d'énergie|libre|
|[1BzSQkOqOPqFvz2u.htm](pathfinder-bestiary-3-items/1BzSQkOqOPqFvz2u.htm)|Dominate|Domination|libre|
|[1CAm301u2swnFr8b.htm](pathfinder-bestiary-3-items/1CAm301u2swnFr8b.htm)|Horn|Corne|libre|
|[1cGNt3AXx2z6VszS.htm](pathfinder-bestiary-3-items/1cGNt3AXx2z6VszS.htm)|Constant Spells|Sorts constants|libre|
|[1CvWXq8Ii6oimfQW.htm](pathfinder-bestiary-3-items/1CvWXq8Ii6oimfQW.htm)|Stealth|Discrétion|libre|
|[1DAzbaSEKgnFKzqV.htm](pathfinder-bestiary-3-items/1DAzbaSEKgnFKzqV.htm)|True Seeing (Constant)|Vision lucide (constant)|officielle|
|[1EJsPFGk71lyNvRf.htm](pathfinder-bestiary-3-items/1EJsPFGk71lyNvRf.htm)|Mauler|Écharpeur|officielle|
|[1elKyf9JIeMq5jrZ.htm](pathfinder-bestiary-3-items/1elKyf9JIeMq5jrZ.htm)|Fan Bolts|Carreau éventail|libre|
|[1emEy5ty5iwJ1bDG.htm](pathfinder-bestiary-3-items/1emEy5ty5iwJ1bDG.htm)|Constant Spells|Sorts constants|libre|
|[1EYcnayZrJgCFXca.htm](pathfinder-bestiary-3-items/1EYcnayZrJgCFXca.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[1F1dP9JtIaewb9ib.htm](pathfinder-bestiary-3-items/1F1dP9JtIaewb9ib.htm)|Catch Rock|Interception de rochers|libre|
|[1F2GCLISmzNTXa9u.htm](pathfinder-bestiary-3-items/1F2GCLISmzNTXa9u.htm)|Extend Vines|Extension des vrilles|libre|
|[1GpBs3jPWwtTzZjf.htm](pathfinder-bestiary-3-items/1GpBs3jPWwtTzZjf.htm)|Impossible Stature|Stature imposante|libre|
|[1hdOnHb2baGiZGTs.htm](pathfinder-bestiary-3-items/1hdOnHb2baGiZGTs.htm)|Darkvision|Vision dans le noir|libre|
|[1hmYrhxS6td9Cs3g.htm](pathfinder-bestiary-3-items/1hmYrhxS6td9Cs3g.htm)|Heartsong|Mélodie du coeur|libre|
|[1i0yksL3nMzWjZLz.htm](pathfinder-bestiary-3-items/1i0yksL3nMzWjZLz.htm)|Tickle|Chatouilles|libre|
|[1IBSMp5wMRvadNtl.htm](pathfinder-bestiary-3-items/1IBSMp5wMRvadNtl.htm)|Hyponatremia|Hyponatrémie|libre|
|[1iD6khc2Ev2LOLO7.htm](pathfinder-bestiary-3-items/1iD6khc2Ev2LOLO7.htm)|Perfect Camouflage|Camouflage parfait|libre|
|[1jFj68yQe0moSwH1.htm](pathfinder-bestiary-3-items/1jFj68yQe0moSwH1.htm)|Circle of Protection (Constant)|Cercle de protection (Constant)|libre|
|[1Ld61yL8VIWBvakX.htm](pathfinder-bestiary-3-items/1Ld61yL8VIWBvakX.htm)|Swarm Shape|Forme de nuée|libre|
|[1LUodDO3qpr7CYqM.htm](pathfinder-bestiary-3-items/1LUodDO3qpr7CYqM.htm)|Violent Retort|Riposte violente|libre|
|[1lYiwUfYhh9bsuiW.htm](pathfinder-bestiary-3-items/1lYiwUfYhh9bsuiW.htm)|Buck|Désarçonner|libre|
|[1mdGEjHdiIWq524G.htm](pathfinder-bestiary-3-items/1mdGEjHdiIWq524G.htm)|Air Walk (Constant)|Marche dans les airs (constant)|officielle|
|[1oCth6jpnhdrJCOk.htm](pathfinder-bestiary-3-items/1oCth6jpnhdrJCOk.htm)|Walk the Ethereal Line|En équilibre sur la frontière de l'Éthéré|libre|
|[1pEzu9hLGS2XL7cf.htm](pathfinder-bestiary-3-items/1pEzu9hLGS2XL7cf.htm)|Verdant Burst|Explosion verdoyante|libre|
|[1qHeEusR10OOW67e.htm](pathfinder-bestiary-3-items/1qHeEusR10OOW67e.htm)|Sneak Attack|Attaque sournoise|libre|
|[1qvnCAYxCtqLHNWB.htm](pathfinder-bestiary-3-items/1qvnCAYxCtqLHNWB.htm)|Extend Limbs|Extension des membres|libre|
|[1RfKpuWxU1cMc4p7.htm](pathfinder-bestiary-3-items/1RfKpuWxU1cMc4p7.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[1rv8rr7V1SPtaWM4.htm](pathfinder-bestiary-3-items/1rv8rr7V1SPtaWM4.htm)|True Seeing (Constant)|Vision lucide (constant)|officielle|
|[1RYNKTCRimaI46EV.htm](pathfinder-bestiary-3-items/1RYNKTCRimaI46EV.htm)|Sand Stride|Déplacement facilité dans le sable|libre|
|[1Skzxnout1K9TVNP.htm](pathfinder-bestiary-3-items/1Skzxnout1K9TVNP.htm)|Tied to the Land|Liée à la région|libre|
|[1Ssl4YAndiCjS1sF.htm](pathfinder-bestiary-3-items/1Ssl4YAndiCjS1sF.htm)|At-Will Spells|Sorts à volonté|libre|
|[1sZ0LFXxfqkedyyb.htm](pathfinder-bestiary-3-items/1sZ0LFXxfqkedyyb.htm)|Swallow Whole|Gober|libre|
|[1TjWSXYs7BlJtfxX.htm](pathfinder-bestiary-3-items/1TjWSXYs7BlJtfxX.htm)|Opening Statement|Réquisitoire d'ouverture|libre|
|[1UaZoefxmocVHBRY.htm](pathfinder-bestiary-3-items/1UaZoefxmocVHBRY.htm)|Darkvision|Vision dans le noir|libre|
|[1URQxVXYnS7mCVrp.htm](pathfinder-bestiary-3-items/1URQxVXYnS7mCVrp.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|libre|
|[1UZfCsTCiCQdrzdC.htm](pathfinder-bestiary-3-items/1UZfCsTCiCQdrzdC.htm)|Dream Message (At Will)|Message onirique (À volonté)|libre|
|[1vK61e5tiUHGEF7i.htm](pathfinder-bestiary-3-items/1vK61e5tiUHGEF7i.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[1VZg8BVuvg7BFe9c.htm](pathfinder-bestiary-3-items/1VZg8BVuvg7BFe9c.htm)|Earthbind (At Will)|Cloué à terre (À volonté)|libre|
|[1WtEtbp6ITuVjGUO.htm](pathfinder-bestiary-3-items/1WtEtbp6ITuVjGUO.htm)|Water Walk (Constant)|Marche sur l'eau (constant)|officielle|
|[1xKmwhrUAlCPlAwz.htm](pathfinder-bestiary-3-items/1xKmwhrUAlCPlAwz.htm)|Darkvision|Vision dans le noir|libre|
|[1YPP0PyKjErYFPk7.htm](pathfinder-bestiary-3-items/1YPP0PyKjErYFPk7.htm)|Paralytic Fear|Peur paralysante|libre|
|[1YwW47mP6ygWjXlh.htm](pathfinder-bestiary-3-items/1YwW47mP6ygWjXlh.htm)|+2 Resilient Glamered Full Plate|Harnois de mimétisme résilient +2|libre|
|[1zABoyA2x54PjRbW.htm](pathfinder-bestiary-3-items/1zABoyA2x54PjRbW.htm)|Staff|Bâton|libre|
|[1zDo0dT2WRDJ2XqD.htm](pathfinder-bestiary-3-items/1zDo0dT2WRDJ2XqD.htm)|Mist Vision|Vision malgré la brume|libre|
|[20T7ujLS0ToWyxkl.htm](pathfinder-bestiary-3-items/20T7ujLS0ToWyxkl.htm)|Inhabit Object|Occuper un objet|libre|
|[276uGZ3JI9bINWVh.htm](pathfinder-bestiary-3-items/276uGZ3JI9bINWVh.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[2815rJTOaYW8FTZW.htm](pathfinder-bestiary-3-items/2815rJTOaYW8FTZW.htm)|At-Will Spells|Sorts à volonté|libre|
|[2aPAtYqGlSZ40pDT.htm](pathfinder-bestiary-3-items/2aPAtYqGlSZ40pDT.htm)|Reflect Spell|Sort renvoyé|libre|
|[2bcr0iC80HhF1QZo.htm](pathfinder-bestiary-3-items/2bcr0iC80HhF1QZo.htm)|Inhabit Vessel|Possession du récipient|libre|
|[2BRmODyR3yH6ev3Q.htm](pathfinder-bestiary-3-items/2BRmODyR3yH6ev3Q.htm)|Swamp Stride|Déplacement facilité dans les marais|officielle|
|[2CGRxGWozrsdSvIi.htm](pathfinder-bestiary-3-items/2CGRxGWozrsdSvIi.htm)|Low-Light Vision|Vision nocturne|libre|
|[2Cym04NuN3V52Lkm.htm](pathfinder-bestiary-3-items/2Cym04NuN3V52Lkm.htm)|Performance|Représentation|libre|
|[2DJRrjRKoxQStF9N.htm](pathfinder-bestiary-3-items/2DJRrjRKoxQStF9N.htm)|Kukri|Kukri|libre|
|[2dwXilE1hqrwdBdk.htm](pathfinder-bestiary-3-items/2dwXilE1hqrwdBdk.htm)|Spear|+1,striking|Lance de frappe +1|officielle|
|[2EhHblW8AgWdPhB8.htm](pathfinder-bestiary-3-items/2EhHblW8AgWdPhB8.htm)|Detect Alignment (At Will) (Evil Only)|Détection de l'alignement (à volonté, mauvais uniquement)|officielle|
|[2epurBlrSyaqWetH.htm](pathfinder-bestiary-3-items/2epurBlrSyaqWetH.htm)|Darkvision|Vision dans le noir|libre|
|[2ERFu4hiXliGhhKj.htm](pathfinder-bestiary-3-items/2ERFu4hiXliGhhKj.htm)|All-Around Vision|Vision panoramique|libre|
|[2etpUEez5sWOqlMb.htm](pathfinder-bestiary-3-items/2etpUEez5sWOqlMb.htm)|True Seeing (Constant)|Vision lucide (constant)|officielle|
|[2GzvaeDl0Dl1kjjw.htm](pathfinder-bestiary-3-items/2GzvaeDl0Dl1kjjw.htm)|Drink Blood|Boire le sang|libre|
|[2if951NcJCXU3XXv.htm](pathfinder-bestiary-3-items/2if951NcJCXU3XXv.htm)|Resurrect (Doesn't Require Secondary Casters)|Résurrection (Ne nécessite pas des incantateurs secondaires)|libre|
|[2iPW0FxKyAPkbkDp.htm](pathfinder-bestiary-3-items/2iPW0FxKyAPkbkDp.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|libre|
|[2iSSH3y2Kuv4MNqO.htm](pathfinder-bestiary-3-items/2iSSH3y2Kuv4MNqO.htm)|Emotional Bolt|Carreau d'émotion|libre|
|[2IybDR9dBLK8bfWV.htm](pathfinder-bestiary-3-items/2IybDR9dBLK8bfWV.htm)|Agent of Fate|Agent du destin|libre|
|[2JP7ZbSsTYsLb6ux.htm](pathfinder-bestiary-3-items/2JP7ZbSsTYsLb6ux.htm)|Aquatic Drag|Emporter sous l'eau|libre|
|[2JQEQa6cUhcYoUM4.htm](pathfinder-bestiary-3-items/2JQEQa6cUhcYoUM4.htm)|Tentacle|Tentacule|libre|
|[2kcFz29KySrfv5cW.htm](pathfinder-bestiary-3-items/2kcFz29KySrfv5cW.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[2kFQsJv3fMYJ1RND.htm](pathfinder-bestiary-3-items/2kFQsJv3fMYJ1RND.htm)|Beak|Bec|libre|
|[2kSrspDPmUktGvBB.htm](pathfinder-bestiary-3-items/2kSrspDPmUktGvBB.htm)|Tooth Tug|Tire-dent|libre|
|[2KYuE80DPoFkgFGT.htm](pathfinder-bestiary-3-items/2KYuE80DPoFkgFGT.htm)|Spiked Chain|Chaîne cloutée|libre|
|[2LhHLzXgOoyNmS7p.htm](pathfinder-bestiary-3-items/2LhHLzXgOoyNmS7p.htm)|Darkvision|Vision dans le noir|libre|
|[2lp8axHkLWnfErrb.htm](pathfinder-bestiary-3-items/2lp8axHkLWnfErrb.htm)|Lower Spears!|Lances en avant !|libre|
|[2LqNyl4yrY4R0XPU.htm](pathfinder-bestiary-3-items/2LqNyl4yrY4R0XPU.htm)|Forest Lore (applies to the arboreal archive's territory)|Connaissance de la forêt (s'applique au territoire de l'archiviste arboréen)|libre|
|[2m8xZRm2hISAn5U0.htm](pathfinder-bestiary-3-items/2m8xZRm2hISAn5U0.htm)|Charm (Undead Only)|Charme (mort-vivant uniquement)|libre|
|[2mSXXYPRnq3gIcpj.htm](pathfinder-bestiary-3-items/2mSXXYPRnq3gIcpj.htm)|Grab|Empoignade/Agrippement|libre|
|[2nEKM6ZrGJImSDZh.htm](pathfinder-bestiary-3-items/2nEKM6ZrGJImSDZh.htm)|Grab|Empoignade/Agrippement|libre|
|[2nQjq9UXbBGNp21e.htm](pathfinder-bestiary-3-items/2nQjq9UXbBGNp21e.htm)|Detect Alignment (At Will)|Détection de l'alignement (À volonté)|libre|
|[2NUCE7ph3hld0mgZ.htm](pathfinder-bestiary-3-items/2NUCE7ph3hld0mgZ.htm)|Darkvision|Vision dans le noir|libre|
|[2oNUku2hW5pSquMS.htm](pathfinder-bestiary-3-items/2oNUku2hW5pSquMS.htm)|Sap Mind|Miner l'esprit|libre|
|[2Ou5MlYuSq3KB2Rn.htm](pathfinder-bestiary-3-items/2Ou5MlYuSq3KB2Rn.htm)|At-Will Spells|Sorts à volonté|libre|
|[2OyUOHJ2HoOkk9ki.htm](pathfinder-bestiary-3-items/2OyUOHJ2HoOkk9ki.htm)|Light Blindness|Aveuglé par la lumière|officielle|
|[2PoHFE6FWRaWarJD.htm](pathfinder-bestiary-3-items/2PoHFE6FWRaWarJD.htm)|Breath Weapon|Arme de souffle|libre|
|[2pZ54UBlAD1KlL3E.htm](pathfinder-bestiary-3-items/2pZ54UBlAD1KlL3E.htm)|Flame|Flamme|libre|
|[2Qt36u2pXWig3wKR.htm](pathfinder-bestiary-3-items/2Qt36u2pXWig3wKR.htm)|Shell Block|Blocage de carapace|libre|
|[2rRPRh9T9ed91v7l.htm](pathfinder-bestiary-3-items/2rRPRh9T9ed91v7l.htm)|Cacophony|Cacophonie|libre|
|[2rT3ehZEN8ZxCrCM.htm](pathfinder-bestiary-3-items/2rT3ehZEN8ZxCrCM.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|libre|
|[2Rzw5Pms3YBG0FNZ.htm](pathfinder-bestiary-3-items/2Rzw5Pms3YBG0FNZ.htm)|Viper Swarm Venom|Venin de nuée de vipères|libre|
|[2tww80yq8tBSw5KT.htm](pathfinder-bestiary-3-items/2tww80yq8tBSw5KT.htm)|Blowgun Darts with Harvester Poison|Fléchettes imprégnées du poison récolté|libre|
|[2tYrB09YxVQESLhH.htm](pathfinder-bestiary-3-items/2tYrB09YxVQESLhH.htm)|Darkvision|Vision dans le noir|libre|
|[2vb8hf7ikquLEd9z.htm](pathfinder-bestiary-3-items/2vb8hf7ikquLEd9z.htm)|Aquatic Echolocation (Precise) 120 feet|Écholocalisation aquatique 36 m (précis)|libre|
|[2Vj9sjJhfJuSzBn1.htm](pathfinder-bestiary-3-items/2Vj9sjJhfJuSzBn1.htm)|Grab|Empoignade/Agrippement|libre|
|[2xn05F1PZDFa3Es4.htm](pathfinder-bestiary-3-items/2xn05F1PZDFa3Es4.htm)|Jaws|Mâchoires|libre|
|[2yxDzeKlqEJcTu27.htm](pathfinder-bestiary-3-items/2yxDzeKlqEJcTu27.htm)|Uncanny Pounce|Bond troublant|libre|
|[302qi3pb2HGvsM6E.htm](pathfinder-bestiary-3-items/302qi3pb2HGvsM6E.htm)|Shield Spikes|Pointes de bouclier|libre|
|[333AoGB4COJFtWLx.htm](pathfinder-bestiary-3-items/333AoGB4COJFtWLx.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[34s5p7VALREkL8Sh.htm](pathfinder-bestiary-3-items/34s5p7VALREkL8Sh.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[34yAb411T3yPWYAp.htm](pathfinder-bestiary-3-items/34yAb411T3yPWYAp.htm)|Darting Legs|Éperon de pattes|libre|
|[37C5tkSFoAf7HJd2.htm](pathfinder-bestiary-3-items/37C5tkSFoAf7HJd2.htm)|Fist|Poing|libre|
|[3b4YyISk8knkUFuM.htm](pathfinder-bestiary-3-items/3b4YyISk8knkUFuM.htm)|Falchion|Cimeterre à deux mains|libre|
|[3BM49h9dJSP6UcHN.htm](pathfinder-bestiary-3-items/3BM49h9dJSP6UcHN.htm)|Enraged Home (Piercing)|Maison enragée (Perforant)|libre|
|[3BpK7a2mX5UCDLmB.htm](pathfinder-bestiary-3-items/3BpK7a2mX5UCDLmB.htm)|Stick a Fork in It|Coup de fourchette|libre|
|[3bqSjZ8OFBahcmGk.htm](pathfinder-bestiary-3-items/3bqSjZ8OFBahcmGk.htm)|Darkvision|Vision dans le noir|libre|
|[3Epd1MoTcY2zIoJQ.htm](pathfinder-bestiary-3-items/3Epd1MoTcY2zIoJQ.htm)|Darkvision|Vision dans le noir|libre|
|[3eyaSs1hwilHo39q.htm](pathfinder-bestiary-3-items/3eyaSs1hwilHo39q.htm)|Frightful Presence|Présence terrifiante|libre|
|[3F0FPP20eSpY3u1Q.htm](pathfinder-bestiary-3-items/3F0FPP20eSpY3u1Q.htm)|Darkvision|Vision dans le noir|libre|
|[3FHM8X9p0VGx9jpe.htm](pathfinder-bestiary-3-items/3FHM8X9p0VGx9jpe.htm)|Rock|Rocher|libre|
|[3FtOenHRAd3nreJ2.htm](pathfinder-bestiary-3-items/3FtOenHRAd3nreJ2.htm)|Jaws|Mâchoires|libre|
|[3gpkwgTnI0kfqxxS.htm](pathfinder-bestiary-3-items/3gpkwgTnI0kfqxxS.htm)|Missile Volley|Volée de missiles|libre|
|[3gYN9UrAQK8utoKH.htm](pathfinder-bestiary-3-items/3gYN9UrAQK8utoKH.htm)|Scent (Imprecise) 60 feet|Odorat 18 m (imprécis)|libre|
|[3ISG9gfCmesBzeYt.htm](pathfinder-bestiary-3-items/3ISG9gfCmesBzeYt.htm)|Nosferatu Vulnerabilities|Vulnérabilités du nosferatu|libre|
|[3IZdVb2IEp23wALM.htm](pathfinder-bestiary-3-items/3IZdVb2IEp23wALM.htm)|Lifesense (Imprecise) 60 feet|Perception de la vie 18 m (imprécis)|libre|
|[3jrrGptpZSe7SNzt.htm](pathfinder-bestiary-3-items/3jrrGptpZSe7SNzt.htm)|Constant Spells|Sorts constants|libre|
|[3keJLwwlQMlCUtDM.htm](pathfinder-bestiary-3-items/3keJLwwlQMlCUtDM.htm)|Frightful Presence|Présence terrifiante|libre|
|[3ltFeU7t0v6ZbQqL.htm](pathfinder-bestiary-3-items/3ltFeU7t0v6ZbQqL.htm)|Sudden Destruction|Destruction soudaine|libre|
|[3Lzyr6PVBTQSPZzD.htm](pathfinder-bestiary-3-items/3Lzyr6PVBTQSPZzD.htm)|Flail|+1,striking|Fléau de frappe +1|libre|
|[3MP8S4relPgvIRf2.htm](pathfinder-bestiary-3-items/3MP8S4relPgvIRf2.htm)|Hat Toss|Lancement de chapeau|libre|
|[3nAIvklO9ro6F88p.htm](pathfinder-bestiary-3-items/3nAIvklO9ro6F88p.htm)|Big Claw|Grande pince|libre|
|[3OQIsBgxhUgao5MD.htm](pathfinder-bestiary-3-items/3OQIsBgxhUgao5MD.htm)|Nourishing Feast|Repas nourrissant|libre|
|[3p5BUgo3SACdAkGj.htm](pathfinder-bestiary-3-items/3p5BUgo3SACdAkGj.htm)|Leaping Pounce|Bond agressif|libre|
|[3r5kNijs14DNKRqh.htm](pathfinder-bestiary-3-items/3r5kNijs14DNKRqh.htm)|Ice Climb|Escalade de la glace|officielle|
|[3SCP38lxFNM5HyPh.htm](pathfinder-bestiary-3-items/3SCP38lxFNM5HyPh.htm)|At-Will Spells|Sorts à volonté|libre|
|[3sFxSnysZjKDkccp.htm](pathfinder-bestiary-3-items/3sFxSnysZjKDkccp.htm)|Jaws|Mâchoires|libre|
|[3SJ0K1VlKcITVMvi.htm](pathfinder-bestiary-3-items/3SJ0K1VlKcITVMvi.htm)|Twisting Thrash|Maltraitance renversante|libre|
|[3UNbqS2zBuQO6NaW.htm](pathfinder-bestiary-3-items/3UNbqS2zBuQO6NaW.htm)|Constant Spells|Sorts constants|libre|
|[3vdP680iRFJVfvwV.htm](pathfinder-bestiary-3-items/3vdP680iRFJVfvwV.htm)|Sudden Charge|Charge soudaine|libre|
|[3VhaqjXhDYgbAlz2.htm](pathfinder-bestiary-3-items/3VhaqjXhDYgbAlz2.htm)|Smoke Vision|Vision malgré la fumée|libre|
|[3VjRp3T0kmoqaYH2.htm](pathfinder-bestiary-3-items/3VjRp3T0kmoqaYH2.htm)|Break Swell|Houle fracassante|libre|
|[3VJSS90TtdqUONTz.htm](pathfinder-bestiary-3-items/3VJSS90TtdqUONTz.htm)|Constant Spells|Sorts constants|libre|
|[3wLOdV2RXEY5pnF5.htm](pathfinder-bestiary-3-items/3wLOdV2RXEY5pnF5.htm)|Mutilating Bite|Morsure estropiante|libre|
|[3Wnn8u8Vi6jbroLI.htm](pathfinder-bestiary-3-items/3Wnn8u8Vi6jbroLI.htm)|Scent (Imprecise) 60 feet|Odorat (Imprecis) 18 m|libre|
|[3xQc4jPhxkh2EbIF.htm](pathfinder-bestiary-3-items/3xQc4jPhxkh2EbIF.htm)|Claw|Griffe|libre|
|[3YK02GIFVfg4gqYs.htm](pathfinder-bestiary-3-items/3YK02GIFVfg4gqYs.htm)|Plane Shift (At Will)|Changement de plan (À volonté)|officielle|
|[3z3CSczhmYzfNCxB.htm](pathfinder-bestiary-3-items/3z3CSczhmYzfNCxB.htm)|Ward|Pupille|libre|
|[3ZfdDyLTXwOFpx2V.htm](pathfinder-bestiary-3-items/3ZfdDyLTXwOFpx2V.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[3zY8sXqA8CTtAm4D.htm](pathfinder-bestiary-3-items/3zY8sXqA8CTtAm4D.htm)|Lingering Enmity|Rancune persistante|libre|
|[3ZZyaQ6uTOW9lyxY.htm](pathfinder-bestiary-3-items/3ZZyaQ6uTOW9lyxY.htm)|Tail|Queue|libre|
|[40G9Tq0fb0vTHD9L.htm](pathfinder-bestiary-3-items/40G9Tq0fb0vTHD9L.htm)|Negative Healing|Guérison négative|libre|
|[41sWFM6OF3ytKfyk.htm](pathfinder-bestiary-3-items/41sWFM6OF3ytKfyk.htm)|Drink Abundance|Boire en abondance|libre|
|[41XCv8P4PnillpYy.htm](pathfinder-bestiary-3-items/41XCv8P4PnillpYy.htm)|Longsword|+1,striking|Épée longue de frappe +1|libre|
|[425lZ6UBU1Pd31FP.htm](pathfinder-bestiary-3-items/425lZ6UBU1Pd31FP.htm)|Change Shape|Changement de forme|libre|
|[42qxpi5EYfZ17E0X.htm](pathfinder-bestiary-3-items/42qxpi5EYfZ17E0X.htm)|Darkvision|Vision dans le noir|libre|
|[43AUXc5WoGyh1pKT.htm](pathfinder-bestiary-3-items/43AUXc5WoGyh1pKT.htm)|Drench|Tremper|libre|
|[43VfyQa8GnQbb1JR.htm](pathfinder-bestiary-3-items/43VfyQa8GnQbb1JR.htm)|Team Attack|Attaque de groupe|libre|
|[45zsYQgswZpSa14E.htm](pathfinder-bestiary-3-items/45zsYQgswZpSa14E.htm)|Resonance|Résonance|libre|
|[46fsaAZvo5Pu9usU.htm](pathfinder-bestiary-3-items/46fsaAZvo5Pu9usU.htm)|Troop Movement|Mouvement de troupe|libre|
|[46M4PewZUpmrWn09.htm](pathfinder-bestiary-3-items/46M4PewZUpmrWn09.htm)|Longsword|Épée longue|libre|
|[48uJqrNPzQyywSb2.htm](pathfinder-bestiary-3-items/48uJqrNPzQyywSb2.htm)|Claw|Griffe|libre|
|[4923v7qhGbPOnzkA.htm](pathfinder-bestiary-3-items/4923v7qhGbPOnzkA.htm)|Xiuh Couatl Venom|Venin de xiuh couatl|libre|
|[49AEhw1auYxOww4q.htm](pathfinder-bestiary-3-items/49AEhw1auYxOww4q.htm)|Feast|Repas de fête|libre|
|[49TebdoZrVR7bJei.htm](pathfinder-bestiary-3-items/49TebdoZrVR7bJei.htm)|Art Lore|Connaissance des arts|libre|
|[4ajY6AeHzOgwoB3H.htm](pathfinder-bestiary-3-items/4ajY6AeHzOgwoB3H.htm)|Slip|Glissade|libre|
|[4bIGF4lDjFiCWkiL.htm](pathfinder-bestiary-3-items/4bIGF4lDjFiCWkiL.htm)|Strafing Chomp|Morsure éclair|libre|
|[4csEUCYiZOJmhWRo.htm](pathfinder-bestiary-3-items/4csEUCYiZOJmhWRo.htm)|Improved Knockdown|Renversement amélioré|libre|
|[4cx1d9efogHuWe26.htm](pathfinder-bestiary-3-items/4cx1d9efogHuWe26.htm)|Wreck|Anéantir|libre|
|[4DUpOE1nhjDUloIt.htm](pathfinder-bestiary-3-items/4DUpOE1nhjDUloIt.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[4DxK55znXhBy05Je.htm](pathfinder-bestiary-3-items/4DxK55znXhBy05Je.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[4FIPIf24n9DntnSj.htm](pathfinder-bestiary-3-items/4FIPIf24n9DntnSj.htm)|Rend|Éventration|libre|
|[4FtT2fJlh9Q0McYv.htm](pathfinder-bestiary-3-items/4FtT2fJlh9Q0McYv.htm)|Tackle|Plaquer|libre|
|[4gjSVNhiTon9n0Xd.htm](pathfinder-bestiary-3-items/4gjSVNhiTon9n0Xd.htm)|Swarming Strikes|Frappe de nuée|libre|
|[4j8tGzWuHcBuRSsZ.htm](pathfinder-bestiary-3-items/4j8tGzWuHcBuRSsZ.htm)|Frightful Presence|Présence terrifiante|libre|
|[4JqTaWMRh2PWQLJX.htm](pathfinder-bestiary-3-items/4JqTaWMRh2PWQLJX.htm)|Foot|Pied|libre|
|[4JSskrFw05x7ggm1.htm](pathfinder-bestiary-3-items/4JSskrFw05x7ggm1.htm)|Kick Back|Ruade|libre|
|[4k6Uvj17HBcW7ylM.htm](pathfinder-bestiary-3-items/4k6Uvj17HBcW7ylM.htm)|Energy Conversion|Conversion de l'énergie|libre|
|[4KGlzeHqmtsJnLv9.htm](pathfinder-bestiary-3-items/4KGlzeHqmtsJnLv9.htm)|Spectral Hand|Main spectrale|libre|
|[4nSekhH06OdsuVpK.htm](pathfinder-bestiary-3-items/4nSekhH06OdsuVpK.htm)|Dominate|Domination|libre|
|[4o2YdJckFmrO6XVf.htm](pathfinder-bestiary-3-items/4o2YdJckFmrO6XVf.htm)|Plane of Fire Lore|Connaissance du Plan du feu|officielle|
|[4q3DAdCBSdwFd8Ge.htm](pathfinder-bestiary-3-items/4q3DAdCBSdwFd8Ge.htm)|Claw|Griffe|libre|
|[4Q80XDMMBCj2Mihv.htm](pathfinder-bestiary-3-items/4Q80XDMMBCj2Mihv.htm)|Low-Light Vision|Vision nocturne|libre|
|[4QajQWtHxBUlKsgo.htm](pathfinder-bestiary-3-items/4QajQWtHxBUlKsgo.htm)|Reconstitution|Reconstitution|libre|
|[4QFJBrjb8mR8VE6I.htm](pathfinder-bestiary-3-items/4QFJBrjb8mR8VE6I.htm)|Stormcalling|Appel à la tempête|libre|
|[4QS3vuiF6NBVHzYN.htm](pathfinder-bestiary-3-items/4QS3vuiF6NBVHzYN.htm)|Blood Wake|Sillage de sang|libre|
|[4r3fZEvFKQpzQMJ0.htm](pathfinder-bestiary-3-items/4r3fZEvFKQpzQMJ0.htm)|Dagger|Dague|libre|
|[4rjTham5fRzKLGA2.htm](pathfinder-bestiary-3-items/4rjTham5fRzKLGA2.htm)|Low-Light Vision|Vision nocturne|libre|
|[4rONi7rqC9gJLQZT.htm](pathfinder-bestiary-3-items/4rONi7rqC9gJLQZT.htm)|+2 Greater Resilient Full Plate|Harnois de résilience supérieure +2|officielle|
|[4SUjs1Fnnil2m2Qt.htm](pathfinder-bestiary-3-items/4SUjs1Fnnil2m2Qt.htm)|Cursed Gaze|Regard de malédiction|libre|
|[4t55o9zeccODBur0.htm](pathfinder-bestiary-3-items/4t55o9zeccODBur0.htm)|Stitch Skin|Coudre la peau|libre|
|[4t6lOiLhCYAlFpnf.htm](pathfinder-bestiary-3-items/4t6lOiLhCYAlFpnf.htm)|Greatsword|Épée à deux mains|libre|
|[4TqIsk44LcZm61Ws.htm](pathfinder-bestiary-3-items/4TqIsk44LcZm61Ws.htm)|Rend|Éventration|libre|
|[4uqNBCMxlug85vdT.htm](pathfinder-bestiary-3-items/4uqNBCMxlug85vdT.htm)|Stealth|Discrétion|libre|
|[4v1IFAFpIxXq9Uzv.htm](pathfinder-bestiary-3-items/4v1IFAFpIxXq9Uzv.htm)|Remove Curse (At Will)|Délivrance des malédictions (À volonté)|libre|
|[4vjdVbvQ2cPvkxA1.htm](pathfinder-bestiary-3-items/4vjdVbvQ2cPvkxA1.htm)|Seek Quarry|Recherche d'une cible|libre|
|[4W60RWwqP4LPF4Ve.htm](pathfinder-bestiary-3-items/4W60RWwqP4LPF4Ve.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[4w6cni5pDSonPMZW.htm](pathfinder-bestiary-3-items/4w6cni5pDSonPMZW.htm)|Shortsword|Épée courte|libre|
|[4W98qLPXYtwytrBj.htm](pathfinder-bestiary-3-items/4W98qLPXYtwytrBj.htm)|Low-Light Vision|Vision nocturne|libre|
|[4wEJM0uffDwyN4QS.htm](pathfinder-bestiary-3-items/4wEJM0uffDwyN4QS.htm)|Coiling Frenzy|Frénésie torsadée|libre|
|[4x1dSgcNIbL7YtTa.htm](pathfinder-bestiary-3-items/4x1dSgcNIbL7YtTa.htm)|Frightening Howl|Hurlement effrayant|libre|
|[4y0Im0eAbj4YFOII.htm](pathfinder-bestiary-3-items/4y0Im0eAbj4YFOII.htm)|Troop Movement|Mouvement de troupe|libre|
|[4yxB753affbJoFUo.htm](pathfinder-bestiary-3-items/4yxB753affbJoFUo.htm)|Constant Spells|Sorts constants|libre|
|[4ZK6UkKCERwU9jVd.htm](pathfinder-bestiary-3-items/4ZK6UkKCERwU9jVd.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[50TREly1W5yrRm7R.htm](pathfinder-bestiary-3-items/50TREly1W5yrRm7R.htm)|Thoughtsense (Imprecise) 60 feet|Perception des pensées 18 m (imprécis)|libre|
|[51mhuepTTAtKvDL3.htm](pathfinder-bestiary-3-items/51mhuepTTAtKvDL3.htm)|Diseased Pustules|Pustules infectées|libre|
|[53ZHZQrxzfyMUm3s.htm](pathfinder-bestiary-3-items/53ZHZQrxzfyMUm3s.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[54ARy8tIUzIr76GH.htm](pathfinder-bestiary-3-items/54ARy8tIUzIr76GH.htm)|Fed by Metal|Renforcé par le métal|libre|
|[55pDhEb6m82JCtfR.htm](pathfinder-bestiary-3-items/55pDhEb6m82JCtfR.htm)|Abrogation of Consequences|Abrogation des conséquences|libre|
|[56iLpxxqpM1gYATc.htm](pathfinder-bestiary-3-items/56iLpxxqpM1gYATc.htm)|Death Gaze|Regard de mort|libre|
|[56KGSD8qAWdFbvdP.htm](pathfinder-bestiary-3-items/56KGSD8qAWdFbvdP.htm)|Divine Innate Spells (See Ganzi Spells)|Sorts divins innés (voir sorts ganzis)|libre|
|[57if0qj4jc4HcQGo.htm](pathfinder-bestiary-3-items/57if0qj4jc4HcQGo.htm)|Nondetection (Constant) (Self Only)|Antidétection (constant)(soi uniquement)|libre|
|[58QNGdCWXAUWVAti.htm](pathfinder-bestiary-3-items/58QNGdCWXAUWVAti.htm)|Darkvision|Vision dans le noir|libre|
|[5AumaQTWt3WVkDej.htm](pathfinder-bestiary-3-items/5AumaQTWt3WVkDej.htm)|At-Will Spells|Sorts à volonté|libre|
|[5auWJrdx79CUpWa2.htm](pathfinder-bestiary-3-items/5auWJrdx79CUpWa2.htm)|Attack of Opportunity (Tail Only)|Attaque d'opportunité (Queue uniquement)|libre|
|[5B3uRzeJSlXUyJPR.htm](pathfinder-bestiary-3-items/5B3uRzeJSlXUyJPR.htm)|Darkvision|Vision dans le noir|libre|
|[5d0jyVv6vjBbVxdc.htm](pathfinder-bestiary-3-items/5d0jyVv6vjBbVxdc.htm)|Sure Stride|Marche assurée|libre|
|[5e9nAAnEPwtzAoiR.htm](pathfinder-bestiary-3-items/5e9nAAnEPwtzAoiR.htm)|Darkvision|Vision dans le noir|libre|
|[5eqtzIqWpDscyCIh.htm](pathfinder-bestiary-3-items/5eqtzIqWpDscyCIh.htm)|Breath Weapon|Arme de souffle|libre|
|[5FoNMqcRkDgmHGWx.htm](pathfinder-bestiary-3-items/5FoNMqcRkDgmHGWx.htm)|Darkvision|Vision dans le noir|libre|
|[5FqnH65aSIz1yYu4.htm](pathfinder-bestiary-3-items/5FqnH65aSIz1yYu4.htm)|Share Pain|Partage de la douleur|libre|
|[5gfJon4GXyqVHTz0.htm](pathfinder-bestiary-3-items/5gfJon4GXyqVHTz0.htm)|Negative Healing|Guérison négative|libre|
|[5gY8gk28J2srHl1t.htm](pathfinder-bestiary-3-items/5gY8gk28J2srHl1t.htm)|Skip Between|Alterner|libre|
|[5h2xMj0CQYOauzxy.htm](pathfinder-bestiary-3-items/5h2xMj0CQYOauzxy.htm)|Monk Ki Spells|Sorts Ki de moine|libre|
|[5h9QlGG2MnMtfSRm.htm](pathfinder-bestiary-3-items/5h9QlGG2MnMtfSRm.htm)|Boneyard Lore|Connaissance du Cimetière|officielle|
|[5hGIIhdbBibMGMJ3.htm](pathfinder-bestiary-3-items/5hGIIhdbBibMGMJ3.htm)|Cat's Grace|Élégance du chat|libre|
|[5hNT46xUSUSqA2x1.htm](pathfinder-bestiary-3-items/5hNT46xUSUSqA2x1.htm)|Musk|Musc|libre|
|[5I1UyAH3ppy0Pi6l.htm](pathfinder-bestiary-3-items/5I1UyAH3ppy0Pi6l.htm)|Rib Skewer|Côte embrocheuse|libre|
|[5iBIN1PlI1tshlnI.htm](pathfinder-bestiary-3-items/5iBIN1PlI1tshlnI.htm)|Knockdown|Renversement|libre|
|[5iH51KTA8CfAOS08.htm](pathfinder-bestiary-3-items/5iH51KTA8CfAOS08.htm)|Light Wisp|Volute lumineuse|libre|
|[5IHuVnedjxI2lxzF.htm](pathfinder-bestiary-3-items/5IHuVnedjxI2lxzF.htm)|Distracting Gaze|Regard accaparant|libre|
|[5iw21DmwOYFPkVbC.htm](pathfinder-bestiary-3-items/5iw21DmwOYFPkVbC.htm)|Prickly Burst|Explosion épineuse|libre|
|[5j0da1f0NNKtJ9uB.htm](pathfinder-bestiary-3-items/5j0da1f0NNKtJ9uB.htm)|Pulse of Rage|Pulsion de rage|libre|
|[5JFwqj1O27XBnI7T.htm](pathfinder-bestiary-3-items/5JFwqj1O27XBnI7T.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[5JGaGCjUurVrjoyX.htm](pathfinder-bestiary-3-items/5JGaGCjUurVrjoyX.htm)|Unfathomable Infinity|Infinité insondable|libre|
|[5JjMeb6t3MgQy8HQ.htm](pathfinder-bestiary-3-items/5JjMeb6t3MgQy8HQ.htm)|Rock|Rocher|libre|
|[5KlhepR2kBxgHDeS.htm](pathfinder-bestiary-3-items/5KlhepR2kBxgHDeS.htm)|Clairaudience (At Will)|Clairaudience (À volonté)|libre|
|[5mZBnyI8jN8hRDpC.htm](pathfinder-bestiary-3-items/5mZBnyI8jN8hRDpC.htm)|Barbed Net|Filet barbelé|libre|
|[5NPHDSBtMnU44c52.htm](pathfinder-bestiary-3-items/5NPHDSBtMnU44c52.htm)|Rally|Rallier|libre|
|[5OeW9Mm1qaPRCaYt.htm](pathfinder-bestiary-3-items/5OeW9Mm1qaPRCaYt.htm)|+2 to All Saves vs. Poison|bonus de +2 aux JdS contre le poison|libre|
|[5OPP2h29MKUEtig6.htm](pathfinder-bestiary-3-items/5OPP2h29MKUEtig6.htm)|Ectoplasmic Secretions|Sécrétions ectoplasmiques|libre|
|[5OTunhc4CEJ5XrAr.htm](pathfinder-bestiary-3-items/5OTunhc4CEJ5XrAr.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[5pRHBAOx7kPRJDBR.htm](pathfinder-bestiary-3-items/5pRHBAOx7kPRJDBR.htm)|Negative Healing|Guérison négative|libre|
|[5PRYxLiEoiOScKcc.htm](pathfinder-bestiary-3-items/5PRYxLiEoiOScKcc.htm)|Swift Steps|Pas véloces|libre|
|[5qFwYj9ry9CVIKY7.htm](pathfinder-bestiary-3-items/5qFwYj9ry9CVIKY7.htm)|Telepathy 60 feet|Télépathie 18 m|libre|
|[5rmiy80ibEEvlvIL.htm](pathfinder-bestiary-3-items/5rmiy80ibEEvlvIL.htm)|Consecration Vulnerability|Vulnérable à la consécration|libre|
|[5RtC2ASqDYa1fPfS.htm](pathfinder-bestiary-3-items/5RtC2ASqDYa1fPfS.htm)|Constant Spells|Sorts constants|libre|
|[5SsaserwdJ4my9cj.htm](pathfinder-bestiary-3-items/5SsaserwdJ4my9cj.htm)|Darkvision|Vision dans le noir|libre|
|[5SZG9rr8f11whg0x.htm](pathfinder-bestiary-3-items/5SZG9rr8f11whg0x.htm)|Speak with Bats|Communication avec les chauve-souris|libre|
|[5TY3CK7BZ3ZrKA1C.htm](pathfinder-bestiary-3-items/5TY3CK7BZ3ZrKA1C.htm)|Occult Prepared Spells|Sorts occultes préparés|libre|
|[5u8Fh5gvFK0DreBo.htm](pathfinder-bestiary-3-items/5u8Fh5gvFK0DreBo.htm)|Handspring Kick|Saut de mains frappé|libre|
|[5VVtnGIh8V9iyhaS.htm](pathfinder-bestiary-3-items/5VVtnGIh8V9iyhaS.htm)|Sense Apostate 500 feet|Perception des apostats 150 m|libre|
|[5WhjDHH8kb9e6f8l.htm](pathfinder-bestiary-3-items/5WhjDHH8kb9e6f8l.htm)|Shadowplay|Jeux d'ombres|libre|
|[5wlVfCVGmDq3fRKw.htm](pathfinder-bestiary-3-items/5wlVfCVGmDq3fRKw.htm)|Malodorous Smoke|Odeur malodorante|libre|
|[5XfUKnzADDIB9IN0.htm](pathfinder-bestiary-3-items/5XfUKnzADDIB9IN0.htm)|Create Spawn|Création de rejetons|libre|
|[5xKUaQZJHSM8chyv.htm](pathfinder-bestiary-3-items/5xKUaQZJHSM8chyv.htm)|Intimidation|Intimidation|libre|
|[5YGPeCSsTq71DxAb.htm](pathfinder-bestiary-3-items/5YGPeCSsTq71DxAb.htm)|Souleater|Mangeur d'âmes|libre|
|[5YK8hJa0XyWYTPW1.htm](pathfinder-bestiary-3-items/5YK8hJa0XyWYTPW1.htm)|Darkvision|Vision dans le noir|libre|
|[5zASRad3bigyGHMo.htm](pathfinder-bestiary-3-items/5zASRad3bigyGHMo.htm)|Coiling Frenzy|Frénésie torsadée|libre|
|[5ZYqE0wvrRymU2Bg.htm](pathfinder-bestiary-3-items/5ZYqE0wvrRymU2Bg.htm)|Darkvision|Vision dans le noir|libre|
|[60CCB5Edf3n6TRxV.htm](pathfinder-bestiary-3-items/60CCB5Edf3n6TRxV.htm)|Tongue|Langue|libre|
|[60deOo2mPqEopbvy.htm](pathfinder-bestiary-3-items/60deOo2mPqEopbvy.htm)|Arcane Innate Spells|Sorts arcaniques innés|libre|
|[61RVU0BzmvnLofFj.htm](pathfinder-bestiary-3-items/61RVU0BzmvnLofFj.htm)|Absorb Evocation|Absorption d'évocation|libre|
|[62PNJhmFdbtT0TrS.htm](pathfinder-bestiary-3-items/62PNJhmFdbtT0TrS.htm)|Voice Imitation|Imitation de la voix|libre|
|[62tSPn9Q3DkS4DA9.htm](pathfinder-bestiary-3-items/62tSPn9Q3DkS4DA9.htm)|Sandwalking|Marche sur le sable|libre|
|[64CXDHwnDpHKKZpp.htm](pathfinder-bestiary-3-items/64CXDHwnDpHKKZpp.htm)|At-Will Spells|Sorts à volonté|libre|
|[64fFy6rN4KqZRePB.htm](pathfinder-bestiary-3-items/64fFy6rN4KqZRePB.htm)|Feign Death|Feindre la mort|libre|
|[66fDZvUWClHrgbKL.htm](pathfinder-bestiary-3-items/66fDZvUWClHrgbKL.htm)|Breath Weapon|Arme de souffle|libre|
|[66GzinTfrKKU8xfg.htm](pathfinder-bestiary-3-items/66GzinTfrKKU8xfg.htm)|Darkvision|Vision dans le noir|libre|
|[66q0s4rjQzYyXMxq.htm](pathfinder-bestiary-3-items/66q0s4rjQzYyXMxq.htm)|Snake Search|Serpent chercheur|libre|
|[69BcgWueS6EljWeK.htm](pathfinder-bestiary-3-items/69BcgWueS6EljWeK.htm)|Benthic Wave|Vague benthique|libre|
|[6cqbtL8P73BAKLlb.htm](pathfinder-bestiary-3-items/6cqbtL8P73BAKLlb.htm)|Void Weapon|Arme du Néant|libre|
|[6CxEvaX1CFDQ7rOR.htm](pathfinder-bestiary-3-items/6CxEvaX1CFDQ7rOR.htm)|Deep Breath|Inspiration profonde|libre|
|[6d6cIadAucEYZpne.htm](pathfinder-bestiary-3-items/6d6cIadAucEYZpne.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|libre|
|[6D8hv3P04IeQWepE.htm](pathfinder-bestiary-3-items/6D8hv3P04IeQWepE.htm)|Alien Vestment|Tenue étrange|libre|
|[6DJ5UGod5sVQIgQ8.htm](pathfinder-bestiary-3-items/6DJ5UGod5sVQIgQ8.htm)|Sunset Ribbon|Ruban du crépuscule|libre|
|[6fc7mDScSYrAh4aK.htm](pathfinder-bestiary-3-items/6fc7mDScSYrAh4aK.htm)|Broom|Balai|officielle|
|[6FLKit13w08Xyhqy.htm](pathfinder-bestiary-3-items/6FLKit13w08Xyhqy.htm)|Bond with Ward|Lié au porteur|libre|
|[6frIjAjjbonxZDls.htm](pathfinder-bestiary-3-items/6frIjAjjbonxZDls.htm)|Endure Elements (Constant)|Endurance aux éléments (constant)|libre|
|[6gxrdqOXjumJ5L6w.htm](pathfinder-bestiary-3-items/6gxrdqOXjumJ5L6w.htm)|Sound Burst (At Will)|Cacophonie (À volonté)|officielle|
|[6hgXjCxuZTwCxmFa.htm](pathfinder-bestiary-3-items/6hgXjCxuZTwCxmFa.htm)|Darkvision|Vision dans le noir|libre|
|[6HIVko7t9eQdURP8.htm](pathfinder-bestiary-3-items/6HIVko7t9eQdURP8.htm)|Tendril|Vrille|libre|
|[6jf1IhNb2PBN2aXV.htm](pathfinder-bestiary-3-items/6jf1IhNb2PBN2aXV.htm)|Attack of Opportunity (Special)|Attaque d'opportunité (Spéciale)|libre|
|[6Jhrih75LDb7Tovl.htm](pathfinder-bestiary-3-items/6Jhrih75LDb7Tovl.htm)|Warping Ray|Rayon de distortion|libre|
|[6jmiViG6aADCKgpM.htm](pathfinder-bestiary-3-items/6jmiViG6aADCKgpM.htm)|Wing|Aile|libre|
|[6jXWp7OM7AukkRT3.htm](pathfinder-bestiary-3-items/6jXWp7OM7AukkRT3.htm)|Bite|Morsure|officielle|
|[6kF88w6XCeKwvBaF.htm](pathfinder-bestiary-3-items/6kF88w6XCeKwvBaF.htm)|Stealth|Discrétion|libre|
|[6kZm8XnYELAQFppU.htm](pathfinder-bestiary-3-items/6kZm8XnYELAQFppU.htm)|Bat Empathy|Empathie avec les chauves-souris|libre|
|[6LlX67R81S5J80Ws.htm](pathfinder-bestiary-3-items/6LlX67R81S5J80Ws.htm)|All-Around Vision|Vision panoramique|libre|
|[6LN4eVSW1LF6YGqK.htm](pathfinder-bestiary-3-items/6LN4eVSW1LF6YGqK.htm)|Scent (Imprecise) 100 feet|Odorat (imprécis) 30 m|libre|
|[6ludWZkd4jUcg8tv.htm](pathfinder-bestiary-3-items/6ludWZkd4jUcg8tv.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[6LVQDNRocR70kvVD.htm](pathfinder-bestiary-3-items/6LVQDNRocR70kvVD.htm)|Darkvision|Vision dans le noir|libre|
|[6m81W89YrbQ1gHjG.htm](pathfinder-bestiary-3-items/6m81W89YrbQ1gHjG.htm)|Elongate Tongue|Élongation de la langue|libre|
|[6meS6jDvt73ZAOZV.htm](pathfinder-bestiary-3-items/6meS6jDvt73ZAOZV.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[6mHmfXwMk0MX37HC.htm](pathfinder-bestiary-3-items/6mHmfXwMk0MX37HC.htm)|Greater Constrict|Constriction supérieure|libre|
|[6OrGVyunvCJATyUD.htm](pathfinder-bestiary-3-items/6OrGVyunvCJATyUD.htm)|Self-Absorbed|Centré sur lui-même|libre|
|[6OT1Cj34QvB0zv3Q.htm](pathfinder-bestiary-3-items/6OT1Cj34QvB0zv3Q.htm)|Tail|Queue|libre|
|[6PLzHEkZC8lh9I6a.htm](pathfinder-bestiary-3-items/6PLzHEkZC8lh9I6a.htm)|At-Will Spells|Sorts à volonté|libre|
|[6PZisICkQg9iEoQs.htm](pathfinder-bestiary-3-items/6PZisICkQg9iEoQs.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|libre|
|[6qxKmQ1NJ2m3caTx.htm](pathfinder-bestiary-3-items/6qxKmQ1NJ2m3caTx.htm)|No Breath|Ne respire pas|libre|
|[6QzeJqibXMpIAXDj.htm](pathfinder-bestiary-3-items/6QzeJqibXMpIAXDj.htm)|Low-Light Vision|Vision nocturne|libre|
|[6r3WJI32FjeNNZlp.htm](pathfinder-bestiary-3-items/6r3WJI32FjeNNZlp.htm)|Echolocation 20 feet|Écholocalisation 6 m|libre|
|[6ruimm3IigTOGGP3.htm](pathfinder-bestiary-3-items/6ruimm3IigTOGGP3.htm)|Tongues (Constant)|Don des langues (constant)|officielle|
|[6S6IDhHyRogz5bB7.htm](pathfinder-bestiary-3-items/6S6IDhHyRogz5bB7.htm)|Illusory Object (At Will)|Objet illusoire (À volonté)|officielle|
|[6SDloGsttpr7ZAJq.htm](pathfinder-bestiary-3-items/6SDloGsttpr7ZAJq.htm)|Hatred of Beauty|Haine de la beauté|libre|
|[6ThLzzOnr19ZeVm4.htm](pathfinder-bestiary-3-items/6ThLzzOnr19ZeVm4.htm)|Darkvision|Vision dans le noir|libre|
|[6twRlVuFcy8KOm6s.htm](pathfinder-bestiary-3-items/6twRlVuFcy8KOm6s.htm)|Farming Lore|Connaissance agricole|officielle|
|[6tZvLirOPTqzo6of.htm](pathfinder-bestiary-3-items/6tZvLirOPTqzo6of.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[6u4WiGSgJcSBMIVQ.htm](pathfinder-bestiary-3-items/6u4WiGSgJcSBMIVQ.htm)|Universal Language|Langage universel|libre|
|[6U6A8ryVDLXX3rmv.htm](pathfinder-bestiary-3-items/6U6A8ryVDLXX3rmv.htm)|+1 Status to All Saves vs. Positive|bonus de statut de +1 aux JdS contre les effets positifs|libre|
|[6UlwNI4X3CPg9JEG.htm](pathfinder-bestiary-3-items/6UlwNI4X3CPg9JEG.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[6utF0bwgFoQkxHUD.htm](pathfinder-bestiary-3-items/6utF0bwgFoQkxHUD.htm)|True Seeing (Constant)|Vision lucide (constant)|officielle|
|[6wmw4xZcc4sGgnNR.htm](pathfinder-bestiary-3-items/6wmw4xZcc4sGgnNR.htm)|Carrion Fever|Fièvre du charognard|libre|
|[6WZJcN2ygA1eIjQE.htm](pathfinder-bestiary-3-items/6WZJcN2ygA1eIjQE.htm)|Arcane Innate Spells|Sorts arcaniques innés|libre|
|[6XahuYsNr6LHrkAs.htm](pathfinder-bestiary-3-items/6XahuYsNr6LHrkAs.htm)|Sudden Dive|Plongeon soudain|libre|
|[6xVdg4wmRT0c4mXW.htm](pathfinder-bestiary-3-items/6xVdg4wmRT0c4mXW.htm)|Sugar Rush|Frénésie de sucre|libre|
|[6yq9O6WHEsvMhrT8.htm](pathfinder-bestiary-3-items/6yq9O6WHEsvMhrT8.htm)|Change Shape|Changement de forme|libre|
|[6ZdU1MvTTYu6fH0E.htm](pathfinder-bestiary-3-items/6ZdU1MvTTYu6fH0E.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[6zHIzorHFZDIXyyR.htm](pathfinder-bestiary-3-items/6zHIzorHFZDIXyyR.htm)|Coiling Frenzy|Frénésie torsadée|libre|
|[6ZOExAAHG9cE1G7V.htm](pathfinder-bestiary-3-items/6ZOExAAHG9cE1G7V.htm)|Jaws|Mâchoires|libre|
|[7101NEoU5qf7eU5R.htm](pathfinder-bestiary-3-items/7101NEoU5qf7eU5R.htm)|Greater Constrict|Constriction supérieure|libre|
|[71GJwFaE95Ae7SXJ.htm](pathfinder-bestiary-3-items/71GJwFaE95Ae7SXJ.htm)|Mage Bond|Lié au mage|libre|
|[71vyIq4WtorjpuSI.htm](pathfinder-bestiary-3-items/71vyIq4WtorjpuSI.htm)|Pseudopod|Pseudopode|libre|
|[71ZISy5sK6KLCTyg.htm](pathfinder-bestiary-3-items/71ZISy5sK6KLCTyg.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[72K68WtjLjkNXxWr.htm](pathfinder-bestiary-3-items/72K68WtjLjkNXxWr.htm)|Invigorating Feast|Festin revigorant|libre|
|[73Q1gfTlvsbrJIOV.htm](pathfinder-bestiary-3-items/73Q1gfTlvsbrJIOV.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[74cvbuceLeJciQVI.htm](pathfinder-bestiary-3-items/74cvbuceLeJciQVI.htm)|Flesh Grafting|Greffer la chair|libre|
|[74QNWqV1T1TsPKzT.htm](pathfinder-bestiary-3-items/74QNWqV1T1TsPKzT.htm)|Rend|Éventration|libre|
|[75r7SZRmZYqQiKfH.htm](pathfinder-bestiary-3-items/75r7SZRmZYqQiKfH.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|libre|
|[76GyDdxf5paIWLH7.htm](pathfinder-bestiary-3-items/76GyDdxf5paIWLH7.htm)|Air Walk (Constant)|Marche dans les airs (constant)|officielle|
|[76utl0ImcsGHY3qX.htm](pathfinder-bestiary-3-items/76utl0ImcsGHY3qX.htm)|Freedom of Movement (At Will)|Liberté de mouvement (À volonté)|libre|
|[77tPlIEwFc6nptCm.htm](pathfinder-bestiary-3-items/77tPlIEwFc6nptCm.htm)|Wildfire Storm|Tourbillon de feu sauvage|libre|
|[78eiTtsU1ddUvLm1.htm](pathfinder-bestiary-3-items/78eiTtsU1ddUvLm1.htm)|Dromophobia|Dromophobie|libre|
|[7ASsgG1v1611ihJD.htm](pathfinder-bestiary-3-items/7ASsgG1v1611ihJD.htm)|Bile|Bile|libre|
|[7bHEWVYytDW7lLet.htm](pathfinder-bestiary-3-items/7bHEWVYytDW7lLet.htm)|At-Will Spells|Sorts à volonté|libre|
|[7bvs9Qjgwf9G0s4w.htm](pathfinder-bestiary-3-items/7bvs9Qjgwf9G0s4w.htm)|Darkvision|Vision dans le noir|libre|
|[7c1InSV9VXV4cB8Z.htm](pathfinder-bestiary-3-items/7c1InSV9VXV4cB8Z.htm)|Grasping Tail|Queue saisissante|libre|
|[7cb3300PlOTebHcM.htm](pathfinder-bestiary-3-items/7cb3300PlOTebHcM.htm)|Troop Spellcasting|Incantation de troupe|libre|
|[7cU2Ki9ZUte4XeOY.htm](pathfinder-bestiary-3-items/7cU2Ki9ZUte4XeOY.htm)|Claw|Griffe|libre|
|[7F01r7qfkds4bfEo.htm](pathfinder-bestiary-3-items/7F01r7qfkds4bfEo.htm)|Claw|Griffe|libre|
|[7fBfykPx1yG8CYGz.htm](pathfinder-bestiary-3-items/7fBfykPx1yG8CYGz.htm)|Atrophic Plague|Fléau atrophique|libre|
|[7FCmN4UL4psU0tDh.htm](pathfinder-bestiary-3-items/7FCmN4UL4psU0tDh.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[7FMwxdPwaQDAu4Bt.htm](pathfinder-bestiary-3-items/7FMwxdPwaQDAu4Bt.htm)|Invisibility (At Will) (Self Only)|Invisibilité (À volonté) (soi uniquement)|officielle|
|[7FPtqs0896vW7FwI.htm](pathfinder-bestiary-3-items/7FPtqs0896vW7FwI.htm)|Titanic Charge|Charge titanesque|libre|
|[7GA62FWHgOMnOoir.htm](pathfinder-bestiary-3-items/7GA62FWHgOMnOoir.htm)|Pounce|Bond|officielle|
|[7H5EOw27GyoL0zDR.htm](pathfinder-bestiary-3-items/7H5EOw27GyoL0zDR.htm)|Nauseating Slap|Baffe nauséabonde|libre|
|[7hmsuLy1J8DJxM6F.htm](pathfinder-bestiary-3-items/7hmsuLy1J8DJxM6F.htm)|Fast Healing 15|Guérison accélérée 15|libre|
|[7i9LTQqlhq2Hhy7j.htm](pathfinder-bestiary-3-items/7i9LTQqlhq2Hhy7j.htm)|Constant Spells|Sorts constants|libre|
|[7Ik8WbV7r4dXszxh.htm](pathfinder-bestiary-3-items/7Ik8WbV7r4dXszxh.htm)|Acrobatics|Acrobaties|libre|
|[7iPW2qJv28sNIMlG.htm](pathfinder-bestiary-3-items/7iPW2qJv28sNIMlG.htm)|Tongues (Constant)|Don des langues (constant)|officielle|
|[7Ja558AxHimR0DNe.htm](pathfinder-bestiary-3-items/7Ja558AxHimR0DNe.htm)|No Breath|Ne respire pas|libre|
|[7je9N9tvy1YfaYoe.htm](pathfinder-bestiary-3-items/7je9N9tvy1YfaYoe.htm)|Create Water (At Will)|Création d'eau (À volonté)|libre|
|[7JznoiaHsY4zQn0z.htm](pathfinder-bestiary-3-items/7JznoiaHsY4zQn0z.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[7KrbqcGEJ1GaWuQc.htm](pathfinder-bestiary-3-items/7KrbqcGEJ1GaWuQc.htm)|Attack of Opportunity (Special)|Attaque d'opportunité (Spéciale)|libre|
|[7kWnGY8jWtyZFMYQ.htm](pathfinder-bestiary-3-items/7kWnGY8jWtyZFMYQ.htm)|Detect Alignment (At Will) (Evil Only)|Détection de l'alignement (à volonté, mauvais uniquement)|officielle|
|[7lG8jBDPOKE6KGt2.htm](pathfinder-bestiary-3-items/7lG8jBDPOKE6KGt2.htm)|Green Caress|Caresse végétale|libre|
|[7M6TofwZ6Yvd0iw4.htm](pathfinder-bestiary-3-items/7M6TofwZ6Yvd0iw4.htm)|Change Shape|Changement de forme|libre|
|[7N5JvxeRQzcnB5rG.htm](pathfinder-bestiary-3-items/7N5JvxeRQzcnB5rG.htm)|Create Water (At Will)|Création d'eau (À volonté)|libre|
|[7NvFdNeBt7AssuX2.htm](pathfinder-bestiary-3-items/7NvFdNeBt7AssuX2.htm)|Faithful Weapon|Arme de la foi|libre|
|[7O5f2VrYGu8KYH37.htm](pathfinder-bestiary-3-items/7O5f2VrYGu8KYH37.htm)|Emit Spores|Émission de spores|libre|
|[7OHhyz8S9jaICp6G.htm](pathfinder-bestiary-3-items/7OHhyz8S9jaICp6G.htm)|Root In Place|Enracinement|libre|
|[7oiohGkxeOfZceVR.htm](pathfinder-bestiary-3-items/7oiohGkxeOfZceVR.htm)|Low-Light Vision|Vision nocturne|libre|
|[7osAd2va2Xd7Jqmw.htm](pathfinder-bestiary-3-items/7osAd2va2Xd7Jqmw.htm)|Ectoplasmic Form|Forme ectoplasmique|libre|
|[7q0sH9B8Zn1uCrNj.htm](pathfinder-bestiary-3-items/7q0sH9B8Zn1uCrNj.htm)|Cleric Domain Spells|Sorts de domaine de prêtre|libre|
|[7qBjkooMOFnRW7Uy.htm](pathfinder-bestiary-3-items/7qBjkooMOFnRW7Uy.htm)|Jaws|Mâchoires|libre|
|[7qC64GqGP34ClPu4.htm](pathfinder-bestiary-3-items/7qC64GqGP34ClPu4.htm)|True Seeing (Constant)|Vision lucide (constant)|officielle|
|[7SJO477OusJy7wpB.htm](pathfinder-bestiary-3-items/7SJO477OusJy7wpB.htm)|Jaws|Mâchoires|libre|
|[7SyelFSQ8Pix3kVJ.htm](pathfinder-bestiary-3-items/7SyelFSQ8Pix3kVJ.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[7u9nInfiCammnrKi.htm](pathfinder-bestiary-3-items/7u9nInfiCammnrKi.htm)|Jaws|Mâchoires|libre|
|[7ueu1lLVg5YdZ0pi.htm](pathfinder-bestiary-3-items/7ueu1lLVg5YdZ0pi.htm)|Create Undead (Doesn't Require Secondary Casters)|Création de mort-vivant (ne requiert pas d'incantateurs secondaires)|libre|
|[7UK0F7yfKDuLMdqq.htm](pathfinder-bestiary-3-items/7UK0F7yfKDuLMdqq.htm)|Coiling Frenzy|Frénésie torsadée|libre|
|[7ulY5fkbW89xf55O.htm](pathfinder-bestiary-3-items/7ulY5fkbW89xf55O.htm)|Pest Haven|Havre de vermine|libre|
|[7V9CRphWnj5Wbphk.htm](pathfinder-bestiary-3-items/7V9CRphWnj5Wbphk.htm)|Constant Spells|Sorts constants|libre|
|[7VXwGJWTdkaP6Ch0.htm](pathfinder-bestiary-3-items/7VXwGJWTdkaP6Ch0.htm)|At-Will Spells|Sorts à volonté|libre|
|[7wr42RamnztEcqpc.htm](pathfinder-bestiary-3-items/7wr42RamnztEcqpc.htm)|Send Beyond|Envoi dans l'au-delà|libre|
|[7WVvb1W4IfVQZzSv.htm](pathfinder-bestiary-3-items/7WVvb1W4IfVQZzSv.htm)|Malevolent Possession|Possession malveillante|libre|
|[7wxTRall7vLvrVdI.htm](pathfinder-bestiary-3-items/7wxTRall7vLvrVdI.htm)|Sharp Riposte|Riposte tranchante|libre|
|[7xqB2gVMxUHI1QMC.htm](pathfinder-bestiary-3-items/7xqB2gVMxUHI1QMC.htm)|Skeleton Crew|Équipage de squelettes|libre|
|[81AZ7vz3WmQESZ0c.htm](pathfinder-bestiary-3-items/81AZ7vz3WmQESZ0c.htm)|Rock|Rocher|libre|
|[82eOb078T13bwHGH.htm](pathfinder-bestiary-3-items/82eOb078T13bwHGH.htm)|Shape Stone (See Idols of Stone)|Façonnage de la pierre (Voir Idoles de pierre)|libre|
|[82PUYYf4hPvhXKja.htm](pathfinder-bestiary-3-items/82PUYYf4hPvhXKja.htm)|Fear (At Will)|Terreur (À volonté)|officielle|
|[831DXsppKlNomlSq.htm](pathfinder-bestiary-3-items/831DXsppKlNomlSq.htm)|Thievery|Vol|libre|
|[847XuFlQNdsrsK3G.htm](pathfinder-bestiary-3-items/847XuFlQNdsrsK3G.htm)|Roll Up|S'enrouler|libre|
|[85a0vfvP7BEWaC7o.htm](pathfinder-bestiary-3-items/85a0vfvP7BEWaC7o.htm)|At-Will Spells|Sorts à volonté|libre|
|[85CNz1N6H7AEa4kU.htm](pathfinder-bestiary-3-items/85CNz1N6H7AEa4kU.htm)|Scimitar|Cimeterre|libre|
|[87If4txGT9U4ihPf.htm](pathfinder-bestiary-3-items/87If4txGT9U4ihPf.htm)|Scrabbling Swarm|Bousculade de nuée|libre|
|[88a6l4wOhZKuqey0.htm](pathfinder-bestiary-3-items/88a6l4wOhZKuqey0.htm)|Flight Commander of Dis|Commandant d'esquadre de Dis|libre|
|[88wfIYYh2caypx0G.htm](pathfinder-bestiary-3-items/88wfIYYh2caypx0G.htm)|Wavesense (Imprecise) 100 feet|Perception des ondes (imprécis) 30 m|libre|
|[89pRruq95QsvuZYX.htm](pathfinder-bestiary-3-items/89pRruq95QsvuZYX.htm)|Sensory Fever|Fièvre des sens|libre|
|[89ujLImiUz00Zomc.htm](pathfinder-bestiary-3-items/89ujLImiUz00Zomc.htm)|Lance Charge|Charge à la lance d'arçon|libre|
|[8AK5AzyX03GXrikY.htm](pathfinder-bestiary-3-items/8AK5AzyX03GXrikY.htm)|Fist|Poing|libre|
|[8Amc9UddnS7QBYOL.htm](pathfinder-bestiary-3-items/8Amc9UddnS7QBYOL.htm)|Cryptomnesia|Cryptomnésie|libre|
|[8B31oTS6yoGOkPve.htm](pathfinder-bestiary-3-items/8B31oTS6yoGOkPve.htm)|Flurry of Blows|Déluge de coups|libre|
|[8BmcD1iEjqhQRthD.htm](pathfinder-bestiary-3-items/8BmcD1iEjqhQRthD.htm)|Tail|Queue|libre|
|[8cr5UQhOU7Fs4x6B.htm](pathfinder-bestiary-3-items/8cr5UQhOU7Fs4x6B.htm)|Scent (Imprecise) 100 feet|Odorat (imprécis) 30 m|libre|
|[8cXuUewtlaWaE5MT.htm](pathfinder-bestiary-3-items/8cXuUewtlaWaE5MT.htm)|Tendril|Vrille|libre|
|[8d5s5kWfxsTt1E7x.htm](pathfinder-bestiary-3-items/8d5s5kWfxsTt1E7x.htm)|Scythe|+1,striking|Faux de frappe +1|libre|
|[8e4jbz96UsZDTcka.htm](pathfinder-bestiary-3-items/8e4jbz96UsZDTcka.htm)|Retaliate|Riposte|libre|
|[8EzoTBKzzz2kcPsk.htm](pathfinder-bestiary-3-items/8EzoTBKzzz2kcPsk.htm)|Darkvision|Vision dans le noir|libre|
|[8FgEIFUzEgLTHjHc.htm](pathfinder-bestiary-3-items/8FgEIFUzEgLTHjHc.htm)|Fearsense (Precise) 60 feet|Perception de la peur 18 m (précis)|libre|
|[8fuYbarODhWt1FXZ.htm](pathfinder-bestiary-3-items/8fuYbarODhWt1FXZ.htm)|Invisibility (At Will) (Self Only)|Invisibilité (À volonté) (soi uniquement)|officielle|
|[8GbsrAKrtoDroA9Y.htm](pathfinder-bestiary-3-items/8GbsrAKrtoDroA9Y.htm)|Snout|Museau|libre|
|[8geadWeP6EV4w18U.htm](pathfinder-bestiary-3-items/8geadWeP6EV4w18U.htm)|Charm (At Will)|Charme (À volonté)|officielle|
|[8GmTiza6VxQxjlFA.htm](pathfinder-bestiary-3-items/8GmTiza6VxQxjlFA.htm)|Arcane Prepared Spells|Sorts arcaniques préparés|libre|
|[8GuVI0hREIK9sjQW.htm](pathfinder-bestiary-3-items/8GuVI0hREIK9sjQW.htm)|Coiling Frenzy|Frénésie torsadée|libre|
|[8hoPxayirM6ubTyC.htm](pathfinder-bestiary-3-items/8hoPxayirM6ubTyC.htm)|Longspear|Pique|libre|
|[8HtO6GuiGguIFsmV.htm](pathfinder-bestiary-3-items/8HtO6GuiGguIFsmV.htm)|Spine|Épines|libre|
|[8IAg57ZRzV51urlh.htm](pathfinder-bestiary-3-items/8IAg57ZRzV51urlh.htm)|Warhammer|Marteau de guerre|officielle|
|[8IQ2N3VKsykBfWvw.htm](pathfinder-bestiary-3-items/8IQ2N3VKsykBfWvw.htm)|Pincer|Pince|libre|
|[8isKGe0HwKWmQDEg.htm](pathfinder-bestiary-3-items/8isKGe0HwKWmQDEg.htm)|Focus Beauty|Focaliser la beauté|libre|
|[8jAtUvdgKdyaNzHN.htm](pathfinder-bestiary-3-items/8jAtUvdgKdyaNzHN.htm)|Fiery Wake|Sillage de feu|libre|
|[8JO7dr59w8qgOjKx.htm](pathfinder-bestiary-3-items/8JO7dr59w8qgOjKx.htm)|Sling|Fronde|libre|
|[8jSa46Rumwn8HF3e.htm](pathfinder-bestiary-3-items/8jSa46Rumwn8HF3e.htm)|Dominate (At Will) (See Doninate)|Domination (À volonté)|libre|
|[8k94OQXOqEJ2FUbE.htm](pathfinder-bestiary-3-items/8k94OQXOqEJ2FUbE.htm)|Grab|Empoignade/Agrippement|libre|
|[8KGUlBcPtdSEm805.htm](pathfinder-bestiary-3-items/8KGUlBcPtdSEm805.htm)|Fist|Poing|libre|
|[8kIjzzH6mYzHjhor.htm](pathfinder-bestiary-3-items/8kIjzzH6mYzHjhor.htm)|Darkvision|Vision dans le noir|libre|
|[8kKowoHm6gXmxhb1.htm](pathfinder-bestiary-3-items/8kKowoHm6gXmxhb1.htm)|-1 to All Saves vs. Emotion Effects|Malus de -1 à tous les jets de sauvegarde contre les effets d'émotion|libre|
|[8LHwhivJZ4n6E2Gd.htm](pathfinder-bestiary-3-items/8LHwhivJZ4n6E2Gd.htm)|Mass Wriggle|Masse tortillante|libre|
|[8lKDQyCeoSuhotl9.htm](pathfinder-bestiary-3-items/8lKDQyCeoSuhotl9.htm)|Regeneration 50 (Deactivated by Evil, Mental, or Orichalcum)|Régénération 50 (désactivée par Mauvais, Mental ou Orichalque)|libre|
|[8N2A9BvddYZu0qKO.htm](pathfinder-bestiary-3-items/8N2A9BvddYZu0qKO.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[8n2FSC3RM16gF3r7.htm](pathfinder-bestiary-3-items/8n2FSC3RM16gF3r7.htm)|Falchion|Cimeterre à deux mains|libre|
|[8N8GpSFs6h26Jfs5.htm](pathfinder-bestiary-3-items/8N8GpSFs6h26Jfs5.htm)|Prehensile Tail|Queue préhensile|libre|
|[8NQPS5MLiZHxcRCO.htm](pathfinder-bestiary-3-items/8NQPS5MLiZHxcRCO.htm)|Ward|Pupille|libre|
|[8NXS92A64sYRn2Rv.htm](pathfinder-bestiary-3-items/8NXS92A64sYRn2Rv.htm)|Bully the Departed|Harcèlement des défunts|libre|
|[8oZMFFQs6aQ2T29D.htm](pathfinder-bestiary-3-items/8oZMFFQs6aQ2T29D.htm)|Knockdown|Renversement|libre|
|[8pagn6n2lyrn1sVZ.htm](pathfinder-bestiary-3-items/8pagn6n2lyrn1sVZ.htm)|Scroll of Confusion (Level 4)|Parchemin de Confusion (Niveau 4)|libre|
|[8pbf8Jlvt2FtaVXg.htm](pathfinder-bestiary-3-items/8pbf8Jlvt2FtaVXg.htm)|Spear|+1,striking,returning|Lance boomerang de Frappe +1|libre|
|[8rkyOZr5JbW7dVMr.htm](pathfinder-bestiary-3-items/8rkyOZr5JbW7dVMr.htm)|Nanite Surge|Déferlement nanites|libre|
|[8So4CPwp508RfAuQ.htm](pathfinder-bestiary-3-items/8So4CPwp508RfAuQ.htm)|Hoof|Sabot|libre|
|[8SPxajKwnuThr56n.htm](pathfinder-bestiary-3-items/8SPxajKwnuThr56n.htm)|Darkvision|Vision dans le noir|libre|
|[8tOUu5ISAj7PMRRU.htm](pathfinder-bestiary-3-items/8tOUu5ISAj7PMRRU.htm)|Absorb Blood|Absorber de sang|libre|
|[8Ukd831yvoAA6YEz.htm](pathfinder-bestiary-3-items/8Ukd831yvoAA6YEz.htm)|Hull|Quille|libre|
|[8UV5F66KioAedVVP.htm](pathfinder-bestiary-3-items/8UV5F66KioAedVVP.htm)|Challenge Foe|Défier l'ennemi|libre|
|[8VqNl9kqJUEBBRW1.htm](pathfinder-bestiary-3-items/8VqNl9kqJUEBBRW1.htm)|Ferocious Will|Volonté farouche|libre|
|[8wjQ8NlAQiJtzns1.htm](pathfinder-bestiary-3-items/8wjQ8NlAQiJtzns1.htm)|Mindbound|Relié par l'esprit|libre|
|[8XnKrSfP40tWixA8.htm](pathfinder-bestiary-3-items/8XnKrSfP40tWixA8.htm)|Knockdown|Renversement|libre|
|[8YB1Ghz9CBsNGNNe.htm](pathfinder-bestiary-3-items/8YB1Ghz9CBsNGNNe.htm)|Invisibility (At Will) (Self Only)|Invisibilité (À volonté) (soi uniquement)|officielle|
|[8YPBBSxz9NwSDdqE.htm](pathfinder-bestiary-3-items/8YPBBSxz9NwSDdqE.htm)|Expel Wave|Déchaînement de vague|libre|
|[8ZmjmY8SXUuq9HVi.htm](pathfinder-bestiary-3-items/8ZmjmY8SXUuq9HVi.htm)|Dreamlands Lore|Connaissance de la Dimension des rêves|libre|
|[8znZn6HZQJY5Ryah.htm](pathfinder-bestiary-3-items/8znZn6HZQJY5Ryah.htm)|Coiling Frenzy|Frénésie torsadée|libre|
|[8zVt6SNpE1sJ6ntx.htm](pathfinder-bestiary-3-items/8zVt6SNpE1sJ6ntx.htm)|Hydraulic Deflection|Déviation hydraulique|libre|
|[90h9CZlrzmq29yPp.htm](pathfinder-bestiary-3-items/90h9CZlrzmq29yPp.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[926QjL85SoOXZHaD.htm](pathfinder-bestiary-3-items/926QjL85SoOXZHaD.htm)|Clever Disguises|Déguisements astucieux|libre|
|[92aGlrWvxAQQzu5H.htm](pathfinder-bestiary-3-items/92aGlrWvxAQQzu5H.htm)|Cheaters Never Prosper|Malheur aux tricheurs|libre|
|[983mBG3qwmzQKGS2.htm](pathfinder-bestiary-3-items/983mBG3qwmzQKGS2.htm)|Hatred of Mirrors|Haine des miroirs|libre|
|[98oLnVVl0tvsGdHl.htm](pathfinder-bestiary-3-items/98oLnVVl0tvsGdHl.htm)|Darkvision|Vision dans le noir|libre|
|[9aVqnZyByrnNoi0O.htm](pathfinder-bestiary-3-items/9aVqnZyByrnNoi0O.htm)|Plagued Coffin Restoration|Cercueil épidémique restaurateur|libre|
|[9azjuUmcCUGNaceC.htm](pathfinder-bestiary-3-items/9azjuUmcCUGNaceC.htm)|Ventriloquism (At Will)|Ventriloquie (À volonté)|officielle|
|[9bnT3kvF7tmt4OdX.htm](pathfinder-bestiary-3-items/9bnT3kvF7tmt4OdX.htm)|Draconic Frenzy|Frénésie draconique|libre|
|[9bZvu1KELcz1AAxo.htm](pathfinder-bestiary-3-items/9bZvu1KELcz1AAxo.htm)|Dimension Door (At Will)|Porte dimensionnelle (À volonté)|officielle|
|[9Cf5HlYqqu2sA7jZ.htm](pathfinder-bestiary-3-items/9Cf5HlYqqu2sA7jZ.htm)|Vortex|Vortex|officielle|
|[9DknzWoTx4dGllLO.htm](pathfinder-bestiary-3-items/9DknzWoTx4dGllLO.htm)|Pressgang Soul|Rejoindre les âmes|libre|
|[9eI0RiQpFdTMAoty.htm](pathfinder-bestiary-3-items/9eI0RiQpFdTMAoty.htm)|Darkvision|Vision dans le noir|libre|
|[9f0C9k9fO0tU9t1k.htm](pathfinder-bestiary-3-items/9f0C9k9fO0tU9t1k.htm)|Detect Alignment (At Will) (Evil Only)|Détection de l'alignement (à volonté, mauvais uniquement)|officielle|
|[9FHqdE5S2AJGokrP.htm](pathfinder-bestiary-3-items/9FHqdE5S2AJGokrP.htm)|Am I Pretty?|Suis-je jolie ?|libre|
|[9FrKdx9Hv7Nqbxqh.htm](pathfinder-bestiary-3-items/9FrKdx9Hv7Nqbxqh.htm)|Snow Vision|Vision malgré la neige|libre|
|[9FS7AxQLqZNLvTdp.htm](pathfinder-bestiary-3-items/9FS7AxQLqZNLvTdp.htm)|At-Will Spells|Sorts à volonté|libre|
|[9h6KJeGxzm8rEPaD.htm](pathfinder-bestiary-3-items/9h6KJeGxzm8rEPaD.htm)|Primal Prepared Spells|Sorts primordiaux préparés|libre|
|[9hTzL7fC49Gu7r0d.htm](pathfinder-bestiary-3-items/9hTzL7fC49Gu7r0d.htm)|Jaws|Mâchoires|libre|
|[9Io2XFFvCWC7aHPp.htm](pathfinder-bestiary-3-items/9Io2XFFvCWC7aHPp.htm)|Darkvision|Vision dans le noir|libre|
|[9iPQQuORbP2KlVOO.htm](pathfinder-bestiary-3-items/9iPQQuORbP2KlVOO.htm)|Ultrasonic Thrust|Poussée ultrasonique|libre|
|[9jBzC5INoea9yhwL.htm](pathfinder-bestiary-3-items/9jBzC5INoea9yhwL.htm)|Arcane Innate Spells|Sorts arcaniques innés|libre|
|[9Jcz4Yr2wdXvPymK.htm](pathfinder-bestiary-3-items/9Jcz4Yr2wdXvPymK.htm)|Believe the Lie|Croit au mensonge|libre|
|[9jiKLswJEKTZnpYW.htm](pathfinder-bestiary-3-items/9jiKLswJEKTZnpYW.htm)|Charm (Animals Only)|Charme (animaux uniquement)|officielle|
|[9kiCgRcrTLqrvzAj.htm](pathfinder-bestiary-3-items/9kiCgRcrTLqrvzAj.htm)|Negative Healing|Guérison négative|libre|
|[9ksWonUvUDnlpZJ9.htm](pathfinder-bestiary-3-items/9ksWonUvUDnlpZJ9.htm)|Darkvision|Vision dans le noir|libre|
|[9LhBHOk8wBt80kaJ.htm](pathfinder-bestiary-3-items/9LhBHOk8wBt80kaJ.htm)|Darkvision|Vision dans le noir|libre|
|[9MHMx6sPv3PI8rHT.htm](pathfinder-bestiary-3-items/9MHMx6sPv3PI8rHT.htm)|Rock|Rocher|libre|
|[9nNxDGgy23r1ETVB.htm](pathfinder-bestiary-3-items/9nNxDGgy23r1ETVB.htm)|Fed by Water|Renforcé par l'eau|libre|
|[9NuzaAkDhBBdhrxI.htm](pathfinder-bestiary-3-items/9NuzaAkDhBBdhrxI.htm)|Constrict|Constriction|libre|
|[9om3JGlrd1NB7cuF.htm](pathfinder-bestiary-3-items/9om3JGlrd1NB7cuF.htm)|Burning Cold Fusillade|Fusillade de Froid brûlant|libre|
|[9Pg0o9SqvEh925xr.htm](pathfinder-bestiary-3-items/9Pg0o9SqvEh925xr.htm)|In Concert|En harmonie|libre|
|[9RHcmhuS5shME3T2.htm](pathfinder-bestiary-3-items/9RHcmhuS5shME3T2.htm)|Greatclub|+3,majorStriking|Massue de frappe majeure +3|libre|
|[9RLSccx0GSuMZYk2.htm](pathfinder-bestiary-3-items/9RLSccx0GSuMZYk2.htm)|Plane of Air Lore|Connaissance du Plan de l'air|libre|
|[9RNFc8HCHuW9XJgm.htm](pathfinder-bestiary-3-items/9RNFc8HCHuW9XJgm.htm)|Lithe|Souplesse|libre|
|[9RSHzC11L764mlf4.htm](pathfinder-bestiary-3-items/9RSHzC11L764mlf4.htm)|Liquefy|Liquéfier|libre|
|[9RW0VcibxF9cA8uA.htm](pathfinder-bestiary-3-items/9RW0VcibxF9cA8uA.htm)|All This Will Happen Again|Cela se produira à nouveau|libre|
|[9rWukglRKNo6c7Jk.htm](pathfinder-bestiary-3-items/9rWukglRKNo6c7Jk.htm)|Claw|Griffe|libre|
|[9sOhZcHSPREufoxA.htm](pathfinder-bestiary-3-items/9sOhZcHSPREufoxA.htm)|Arcane Innate Spells|Sorts arcaniques innés|libre|
|[9sVRsQeqp2NqPZKy.htm](pathfinder-bestiary-3-items/9sVRsQeqp2NqPZKy.htm)|Darkvision|Vision dans le noir|libre|
|[9u2lwbCkserddtx7.htm](pathfinder-bestiary-3-items/9u2lwbCkserddtx7.htm)|Lore (any three)|Connaissance (3 au choix)|libre|
|[9uc04LSVh0aJFRbE.htm](pathfinder-bestiary-3-items/9uc04LSVh0aJFRbE.htm)|At-Will Spells|Sorts à volonté|libre|
|[9uczAsAAuGtsnZP2.htm](pathfinder-bestiary-3-items/9uczAsAAuGtsnZP2.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[9uDPkhvDHQvs18xG.htm](pathfinder-bestiary-3-items/9uDPkhvDHQvs18xG.htm)|Viviparous Birth|Naissance vivipare|libre|
|[9uhb8pW1o8nRmwPD.htm](pathfinder-bestiary-3-items/9uhb8pW1o8nRmwPD.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|libre|
|[9uoAAHOpjHhrvzWl.htm](pathfinder-bestiary-3-items/9uoAAHOpjHhrvzWl.htm)|Darkvision|Vision dans le noir|libre|
|[9UrYZXifKee6nnBC.htm](pathfinder-bestiary-3-items/9UrYZXifKee6nnBC.htm)|Retributive Suplex|Suplex punitive|libre|
|[9utEXpSfzcSAzWEq.htm](pathfinder-bestiary-3-items/9utEXpSfzcSAzWEq.htm)|Paranoia (At Will)|Paranoïa (À volonté)|libre|
|[9wegz19U5LREiL9x.htm](pathfinder-bestiary-3-items/9wegz19U5LREiL9x.htm)|Strix Vengeance|Vengeance Strix|libre|
|[9Wm3gZTBJ0G9sk1w.htm](pathfinder-bestiary-3-items/9Wm3gZTBJ0G9sk1w.htm)|Glimpse of Redemption|Lueur de rédemption|libre|
|[9WqbsuRExPiFqTnh.htm](pathfinder-bestiary-3-items/9WqbsuRExPiFqTnh.htm)|Flameheart Weapon|Arme coeur de flammes|libre|
|[9WYmk5gm1kpKey4j.htm](pathfinder-bestiary-3-items/9WYmk5gm1kpKey4j.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[9x6UoyWK1LPpP2M1.htm](pathfinder-bestiary-3-items/9x6UoyWK1LPpP2M1.htm)|Arcane Innate Spells|Sorts arcaniques innés|libre|
|[9X9mEo92lODvZP6d.htm](pathfinder-bestiary-3-items/9X9mEo92lODvZP6d.htm)|Troop Defenses|Défenses des troupes|libre|
|[9xFBrkHYg5Q7QAn6.htm](pathfinder-bestiary-3-items/9xFBrkHYg5Q7QAn6.htm)|Earthbind (At Will)|Cloué à terre (À volonté)|libre|
|[9xiLnsl12cVesxyN.htm](pathfinder-bestiary-3-items/9xiLnsl12cVesxyN.htm)|Crafting|Artisanat|libre|
|[9xVKiyYkmLvWLsPN.htm](pathfinder-bestiary-3-items/9xVKiyYkmLvWLsPN.htm)|Countered by Earth|Contré par la terre|libre|
|[a0XndmlcRchhe4Sl.htm](pathfinder-bestiary-3-items/a0XndmlcRchhe4Sl.htm)|Horn|Corne|libre|
|[A1bXxv71EPG8m8nv.htm](pathfinder-bestiary-3-items/A1bXxv71EPG8m8nv.htm)|Tongues (Constant)|Don des langues (constant)|officielle|
|[a2FJlFGOGuTMTLOI.htm](pathfinder-bestiary-3-items/a2FJlFGOGuTMTLOI.htm)|Fist|Poing|libre|
|[a2IB9CvFYzHYpXPK.htm](pathfinder-bestiary-3-items/a2IB9CvFYzHYpXPK.htm)|Athletics|Athlétisme|libre|
|[A2z7JUAPZ4320zP5.htm](pathfinder-bestiary-3-items/A2z7JUAPZ4320zP5.htm)|Negative Healing|Guérison négative|libre|
|[A3POhpGYovQJDozd.htm](pathfinder-bestiary-3-items/A3POhpGYovQJDozd.htm)|Bonded Vessel|Récipient lié|libre|
|[a4OTeLXz850yN3NZ.htm](pathfinder-bestiary-3-items/a4OTeLXz850yN3NZ.htm)|Axe Vulnerability|Vulnérabilité à la hache|libre|
|[a6e5k1in4L7W0ieF.htm](pathfinder-bestiary-3-items/a6e5k1in4L7W0ieF.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[a77can8I0pBhmKe0.htm](pathfinder-bestiary-3-items/a77can8I0pBhmKe0.htm)|+2 Circumstance to All Saves vs. Shove and Trip|bonus de circonstances de +2 aux JdS contre Pousser et Croc-en-jambe|libre|
|[a7ntWl311o1OfxCR.htm](pathfinder-bestiary-3-items/a7ntWl311o1OfxCR.htm)|Vulnerable to Sunlight|Vulnérabilité à la lumière du soleil|libre|
|[a83gShMg9yb9LGhv.htm](pathfinder-bestiary-3-items/a83gShMg9yb9LGhv.htm)|At-Will Spells|Sorts à volonté|libre|
|[a8Oke93EjcLb3CyD.htm](pathfinder-bestiary-3-items/a8Oke93EjcLb3CyD.htm)|Trident|Trident|libre|
|[a9aLzghe9uCANIDN.htm](pathfinder-bestiary-3-items/a9aLzghe9uCANIDN.htm)|Jaws|Mâchoires|libre|
|[a9pJTuCtvZCUpnCc.htm](pathfinder-bestiary-3-items/a9pJTuCtvZCUpnCc.htm)|Deception|Duperie|libre|
|[aA0Li6f8IO68qqXw.htm](pathfinder-bestiary-3-items/aA0Li6f8IO68qqXw.htm)|Draining Blight|Dégradation drainante|libre|
|[ab11N1FmMkMVAP9Q.htm](pathfinder-bestiary-3-items/ab11N1FmMkMVAP9Q.htm)|Greater Constrict|Constriction supérieure|libre|
|[aBGHzzN9TzDAz9kf.htm](pathfinder-bestiary-3-items/aBGHzzN9TzDAz9kf.htm)|Divine Lightning|Foudre divine|libre|
|[AbzlxSNMB7g4nTJc.htm](pathfinder-bestiary-3-items/AbzlxSNMB7g4nTJc.htm)|Passive Points|Pointes passives|libre|
|[aCOs4ab0FqX7Nup4.htm](pathfinder-bestiary-3-items/aCOs4ab0FqX7Nup4.htm)|Champion Focus Spells|Sorts focalisés de champion|libre|
|[acq6pZafGJRNM5Um.htm](pathfinder-bestiary-3-items/acq6pZafGJRNM5Um.htm)|+2 Status Bonus on Saves vs. Linguistic Effects|bonus de statut de +2 auxw JdS contre les effets linguistiques|libre|
|[Ad7v2ldIjRYpmP6V.htm](pathfinder-bestiary-3-items/Ad7v2ldIjRYpmP6V.htm)|Swallow Whole|Gober|libre|
|[ADj1D97ZuGgEgAMr.htm](pathfinder-bestiary-3-items/ADj1D97ZuGgEgAMr.htm)|At-Will Spells|Sorts à volonté|libre|
|[adMwYHLlDzh2bKaz.htm](pathfinder-bestiary-3-items/adMwYHLlDzh2bKaz.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[adUdgeL10QH11zJu.htm](pathfinder-bestiary-3-items/adUdgeL10QH11zJu.htm)|Mortal Shield|Bouclier mortel|libre|
|[Ae1LTM5lLFv32i3l.htm](pathfinder-bestiary-3-items/Ae1LTM5lLFv32i3l.htm)|Change Shape|Changement de forme|libre|
|[Ae2lLfIjacCnLEfD.htm](pathfinder-bestiary-3-items/Ae2lLfIjacCnLEfD.htm)|Floating Disk (see Disk Rider)|Disque flottant (voir chevaucheur de disque)|libre|
|[AELQ9eZZYSwimA7P.htm](pathfinder-bestiary-3-items/AELQ9eZZYSwimA7P.htm)|Bide|Patience|libre|
|[AelQuSvbNDOMD6xd.htm](pathfinder-bestiary-3-items/AelQuSvbNDOMD6xd.htm)|Humanoid Form (At Will)|Forme humanoïde (À volonté)|officielle|
|[aEnTHY8PwlCJWqzG.htm](pathfinder-bestiary-3-items/aEnTHY8PwlCJWqzG.htm)|Foot|Pied|libre|
|[AErcLCe3qBLb8HU0.htm](pathfinder-bestiary-3-items/AErcLCe3qBLb8HU0.htm)|No Breath|Ne respire pas|libre|
|[AEz7KLpjP8KnKGjx.htm](pathfinder-bestiary-3-items/AEz7KLpjP8KnKGjx.htm)|Liquid Leap|Saut liquide|libre|
|[AF48h2X3hAjthWZX.htm](pathfinder-bestiary-3-items/AF48h2X3hAjthWZX.htm)|Jaws|Mâchoires|libre|
|[AG5uVa2pIKuc9bfQ.htm](pathfinder-bestiary-3-items/AG5uVa2pIKuc9bfQ.htm)|Flailing Thrash|Coups aux agrippés|libre|
|[AGsJjbwv43AZYfPx.htm](pathfinder-bestiary-3-items/AGsJjbwv43AZYfPx.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[AHL4n7RnueMnqqXS.htm](pathfinder-bestiary-3-items/AHL4n7RnueMnqqXS.htm)|Form a Phalanx|Formation en phalange|libre|
|[aHOLCHiRhu5ipc7L.htm](pathfinder-bestiary-3-items/aHOLCHiRhu5ipc7L.htm)|Ectoplasmic Shield|Bouclier ectoplasmique|libre|
|[Ahyotq9GfFcfraAt.htm](pathfinder-bestiary-3-items/Ahyotq9GfFcfraAt.htm)|Signet Ring|Chevalière|libre|
|[aHZC7D82nkogCiRc.htm](pathfinder-bestiary-3-items/aHZC7D82nkogCiRc.htm)|Air Walk (Constant) (Self Only)|Marche dans les airs (constant, soi uniquement)|libre|
|[Ai9DSqNliIuPOhxP.htm](pathfinder-bestiary-3-items/Ai9DSqNliIuPOhxP.htm)|Interplanar Lifesense|Perception de la vie interplanaire|libre|
|[AiejWhSgj1tzGlOW.htm](pathfinder-bestiary-3-items/AiejWhSgj1tzGlOW.htm)|True Seeing (Constant)|Vision lucide (constant)|officielle|
|[AijMBZl9gTqlCcb0.htm](pathfinder-bestiary-3-items/AijMBZl9gTqlCcb0.htm)|Tormented Snarl|Tourment de la souffrance|libre|
|[AirzEgJQdbzrmlyJ.htm](pathfinder-bestiary-3-items/AirzEgJQdbzrmlyJ.htm)|Light Wisp|Volute lumineuse|libre|
|[aJ9nob7nAtJIDXwG.htm](pathfinder-bestiary-3-items/aJ9nob7nAtJIDXwG.htm)|Arcane Innate Spells|Sorts arcaniques innés|libre|
|[ajC29c9gmHsNUgs1.htm](pathfinder-bestiary-3-items/ajC29c9gmHsNUgs1.htm)|Nymph's Beauty|Beauté de la nymphe|libre|
|[AJK2FzoU6wIUxE43.htm](pathfinder-bestiary-3-items/AJK2FzoU6wIUxE43.htm)|Flaming Shroud|Suaire de flammes|libre|
|[AJLCWM50AXEWFDYT.htm](pathfinder-bestiary-3-items/AJLCWM50AXEWFDYT.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[aK7xyvWTJe4GyQ42.htm](pathfinder-bestiary-3-items/aK7xyvWTJe4GyQ42.htm)|Darkvision|Vision dans le noir|libre|
|[AKn4Oke8VMoU85yk.htm](pathfinder-bestiary-3-items/AKn4Oke8VMoU85yk.htm)|Indomitable Oration|Oraison indomptable|libre|
|[AKUv4ErgbncGAS63.htm](pathfinder-bestiary-3-items/AKUv4ErgbncGAS63.htm)|On All Fours|Quadrupède|libre|
|[AkvUF9WUqlp4ytHG.htm](pathfinder-bestiary-3-items/AkvUF9WUqlp4ytHG.htm)|Toxic Body|Corps toxique|libre|
|[aKXQLekTbiAqj1Id.htm](pathfinder-bestiary-3-items/aKXQLekTbiAqj1Id.htm)|Troop Movement|Mouvement de troupe|libre|
|[alAwSazDkJZWiv6r.htm](pathfinder-bestiary-3-items/alAwSazDkJZWiv6r.htm)|Cooperative Scrying|Scrutation coopérative|libre|
|[AlDSrVGyweTmAY0N.htm](pathfinder-bestiary-3-items/AlDSrVGyweTmAY0N.htm)|Hoof|Sabot|libre|
|[aLEUgg9lhbABSHFk.htm](pathfinder-bestiary-3-items/aLEUgg9lhbABSHFk.htm)|Fear (Animals, Fungi, And Plants Only)|Terreur (animaux, moisissures et plantes uniquement)|libre|
|[alp4ZDrGM2zotNDt.htm](pathfinder-bestiary-3-items/alp4ZDrGM2zotNDt.htm)|Slash the Suffering|Lacération des souffrants|libre|
|[aLsS2G1pU06ABZHI.htm](pathfinder-bestiary-3-items/aLsS2G1pU06ABZHI.htm)|Powerful Scimitars|Cimeterres puissants|libre|
|[aMgKVhdJKyEfpoK9.htm](pathfinder-bestiary-3-items/aMgKVhdJKyEfpoK9.htm)|Low-Light Vision|Vision nocturne|libre|
|[amsXIUz7b4eRuZRR.htm](pathfinder-bestiary-3-items/amsXIUz7b4eRuZRR.htm)|Dagger|+1,striking,returning|Dague boomerang de frappe +1|libre|
|[AmUgwUNSujKkZSco.htm](pathfinder-bestiary-3-items/AmUgwUNSujKkZSco.htm)|Forehead|Coup de tête|libre|
|[aMxPxph18d6Elee9.htm](pathfinder-bestiary-3-items/aMxPxph18d6Elee9.htm)|Echoblade Flurry|Déluge d'écholame|libre|
|[An7ZmOxkCPkGUuBN.htm](pathfinder-bestiary-3-items/An7ZmOxkCPkGUuBN.htm)|Claw|Griffe|libre|
|[anJNKDz3R8ApuLb2.htm](pathfinder-bestiary-3-items/anJNKDz3R8ApuLb2.htm)|Floating Disk (At Will)|Disque flottant (À volonté)|libre|
|[aNoJsFxQdiX8ZDqx.htm](pathfinder-bestiary-3-items/aNoJsFxQdiX8ZDqx.htm)|Halberd|Hallebarde|libre|
|[anyFEiqXXoGS63Gs.htm](pathfinder-bestiary-3-items/anyFEiqXXoGS63Gs.htm)|Grasping Tail|Queue saisissante|libre|
|[aO0RDgMo7pF6pa0h.htm](pathfinder-bestiary-3-items/aO0RDgMo7pF6pa0h.htm)|Snake Fangs|Crocs de serpent|libre|
|[aOaPtLmoTvz46pJw.htm](pathfinder-bestiary-3-items/aOaPtLmoTvz46pJw.htm)|Divine Prepared Spells|Sorts divins préparés|libre|
|[aOeCz4c09QMbgoTG.htm](pathfinder-bestiary-3-items/aOeCz4c09QMbgoTG.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[aox1nvPhhyKKaTi9.htm](pathfinder-bestiary-3-items/aox1nvPhhyKKaTi9.htm)|Scroll of Fly (Level 4)|Parchemin de Vol (Niveau 4)|libre|
|[apuiQK82CCi4s2wU.htm](pathfinder-bestiary-3-items/apuiQK82CCi4s2wU.htm)|Echoblade|Écholame|libre|
|[APy2XnP43Ct9hDjp.htm](pathfinder-bestiary-3-items/APy2XnP43Ct9hDjp.htm)|True Seeing (Constant)|Vision lucide (constant)|officielle|
|[AQ7Uf21qvFcnJL1X.htm](pathfinder-bestiary-3-items/AQ7Uf21qvFcnJL1X.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[aqpkkRpk5KmwHfhG.htm](pathfinder-bestiary-3-items/aqpkkRpk5KmwHfhG.htm)|Knockdown|Renversement|libre|
|[AQxSKJnCPtcDDqGB.htm](pathfinder-bestiary-3-items/AQxSKJnCPtcDDqGB.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[AR1CzPoZmsKpCqkD.htm](pathfinder-bestiary-3-items/AR1CzPoZmsKpCqkD.htm)|Plane Shift (To Or From The Shadow Plane Only)|Changement de plan (de ou vers le plan de l'Ombre uniquement)|libre|
|[arx5pjwY2S5WLBAA.htm](pathfinder-bestiary-3-items/arx5pjwY2S5WLBAA.htm)|Mounted Troop|Troupe montée|libre|
|[AsbIVQDcJZfJhU4N.htm](pathfinder-bestiary-3-items/AsbIVQDcJZfJhU4N.htm)|Troop Defenses|Défenses de troupes|libre|
|[ASdIdSgTDc6bN48E.htm](pathfinder-bestiary-3-items/ASdIdSgTDc6bN48E.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[AsRS8u2z4hfaeLCO.htm](pathfinder-bestiary-3-items/AsRS8u2z4hfaeLCO.htm)|Telepathy 50 feet|Télépathie 15 m|libre|
|[At4UFNUZDqPnIhl7.htm](pathfinder-bestiary-3-items/At4UFNUZDqPnIhl7.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|libre|
|[aT8INKRo18qw8cTl.htm](pathfinder-bestiary-3-items/aT8INKRo18qw8cTl.htm)|Breath Weapon|Arme de souffle|libre|
|[atf7XFGfAWXeI1TN.htm](pathfinder-bestiary-3-items/atf7XFGfAWXeI1TN.htm)|Tremorsense (Imprecise) 30 feet|Perception des vibrations 9 m (imprécis)|libre|
|[atgtDLkpO7KEpGOE.htm](pathfinder-bestiary-3-items/atgtDLkpO7KEpGOE.htm)|Darkvision|Vision dans le noir|libre|
|[ATj9bVkYk7iDTGyM.htm](pathfinder-bestiary-3-items/ATj9bVkYk7iDTGyM.htm)|Dagger|Dague|libre|
|[AtSxBEaHS4ljnAyv.htm](pathfinder-bestiary-3-items/AtSxBEaHS4ljnAyv.htm)|Coven|Cercle|libre|
|[AuA52N8pGsULYcjw.htm](pathfinder-bestiary-3-items/AuA52N8pGsULYcjw.htm)|Tendril|Vrille|libre|
|[AuPR3E2MyY05z9N2.htm](pathfinder-bestiary-3-items/AuPR3E2MyY05z9N2.htm)|Low-Light Vision|Vision nocturne|libre|
|[AUSkGV3ZjHHnyIrW.htm](pathfinder-bestiary-3-items/AUSkGV3ZjHHnyIrW.htm)|Skitter Away|Déguerpir|libre|
|[aVCE3zDK0cQohDQM.htm](pathfinder-bestiary-3-items/aVCE3zDK0cQohDQM.htm)|Monk Ki Spells|Sorts de ki de moine|libre|
|[AvNNZdhRs6SKQiy3.htm](pathfinder-bestiary-3-items/AvNNZdhRs6SKQiy3.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[AW7uAsCknGCb4PDf.htm](pathfinder-bestiary-3-items/AW7uAsCknGCb4PDf.htm)|Read Omens (At Will)|Lire les présages (À volonté)|libre|
|[aw8UyWcvmu5VncsT.htm](pathfinder-bestiary-3-items/aw8UyWcvmu5VncsT.htm)|Mist Vision|Vision malgré la brume|libre|
|[AWOczN1NAKx6tjD8.htm](pathfinder-bestiary-3-items/AWOczN1NAKx6tjD8.htm)|Kukri|Kukri|libre|
|[AwRCPMRzo9dIymmM.htm](pathfinder-bestiary-3-items/AwRCPMRzo9dIymmM.htm)|Susceptible to Death|Exposé à la mort|libre|
|[AXuWafDpkZy5SX8V.htm](pathfinder-bestiary-3-items/AXuWafDpkZy5SX8V.htm)|Incite Violence|Incitation à la violence|libre|
|[aYd4v0mDwmgHckIH.htm](pathfinder-bestiary-3-items/aYd4v0mDwmgHckIH.htm)|Bonded Strike|Frappe liée|libre|
|[AyDallashGfDWvMX.htm](pathfinder-bestiary-3-items/AyDallashGfDWvMX.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[ayEWmTQnDQXk54zc.htm](pathfinder-bestiary-3-items/ayEWmTQnDQXk54zc.htm)|Immortal Flesh|Chair immortelle|libre|
|[aYv6mdSXvzzYWgRA.htm](pathfinder-bestiary-3-items/aYv6mdSXvzzYWgRA.htm)|Capuchin's Curse|Malédiction du capucin|libre|
|[aZTKSdmtywAvr7TJ.htm](pathfinder-bestiary-3-items/aZTKSdmtywAvr7TJ.htm)|Claw|Griffe|libre|
|[AzWuaOagwffOzhLP.htm](pathfinder-bestiary-3-items/AzWuaOagwffOzhLP.htm)|Sneak Attack|Attaque sournoise|libre|
|[aZYbkerDQjojnR8G.htm](pathfinder-bestiary-3-items/aZYbkerDQjojnR8G.htm)|Shield Block|Blocage au bouclier|libre|
|[B0CIGI7Er3AYCaem.htm](pathfinder-bestiary-3-items/B0CIGI7Er3AYCaem.htm)|Darkvision|Vision dans le noir|libre|
|[B0U6YmeB8UGglyGa.htm](pathfinder-bestiary-3-items/B0U6YmeB8UGglyGa.htm)|Tremorsense (Imprecise) 60 feet|Perception des vibrations 18 m (imprécis)|libre|
|[B2rF45WAMrowNHio.htm](pathfinder-bestiary-3-items/B2rF45WAMrowNHio.htm)|Darkvision|Vision dans le noir|libre|
|[b2yf3FTVhb4cL3mI.htm](pathfinder-bestiary-3-items/b2yf3FTVhb4cL3mI.htm)|Pose a Riddle|Poser une énigme|libre|
|[b3L5EEZVfcjwleqc.htm](pathfinder-bestiary-3-items/b3L5EEZVfcjwleqc.htm)|Low-Light Vision|Vision nocturne|libre|
|[B3NBi21pYXb87QM2.htm](pathfinder-bestiary-3-items/B3NBi21pYXb87QM2.htm)|Nosferatu Vulnerabilities|Vulnérabilités du nosferatu|libre|
|[b4LBZAjcRm8KzaWR.htm](pathfinder-bestiary-3-items/b4LBZAjcRm8KzaWR.htm)|Darkvision|Vision dans le noir|libre|
|[B5cFXl5CIucCzoy0.htm](pathfinder-bestiary-3-items/B5cFXl5CIucCzoy0.htm)|Launched Blade|Projection de lame|libre|
|[b5tNOyXUMi9maaal.htm](pathfinder-bestiary-3-items/b5tNOyXUMi9maaal.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[B5UmGa9iTcz4fPFG.htm](pathfinder-bestiary-3-items/B5UmGa9iTcz4fPFG.htm)|Clairaudience (At Will)|Clairaudience (À volonté)|libre|
|[B6M6fTBtJwAQDYCs.htm](pathfinder-bestiary-3-items/B6M6fTBtJwAQDYCs.htm)|Swarm Mind|Esprit de la nuée|libre|
|[b70iU5hV08AtqcDw.htm](pathfinder-bestiary-3-items/b70iU5hV08AtqcDw.htm)|Coven Spells|Sorts de cercle|libre|
|[b7j8M7xzLStzldNC.htm](pathfinder-bestiary-3-items/b7j8M7xzLStzldNC.htm)|Fast Healing 5|Guérison accélérée 5|libre|
|[b8889HDEEpdiqYFf.htm](pathfinder-bestiary-3-items/b8889HDEEpdiqYFf.htm)|Constant Spells|Sorts constants|libre|
|[b8MhSEnrYG0hWLqw.htm](pathfinder-bestiary-3-items/b8MhSEnrYG0hWLqw.htm)|+2 Status to All Saves vs. Divine Magic|bonus de statut de +2 aux JdS contre la magie divine|libre|
|[B8Uf7QHdJ2D0Fs74.htm](pathfinder-bestiary-3-items/B8Uf7QHdJ2D0Fs74.htm)|Capture|Capture|libre|
|[B9EouOshvYsQf0Td.htm](pathfinder-bestiary-3-items/B9EouOshvYsQf0Td.htm)|Low-Light Vision|Vision nocturne|libre|
|[B9hUiPWhzaBpL0E4.htm](pathfinder-bestiary-3-items/B9hUiPWhzaBpL0E4.htm)|Water Walk (Constant)|Marche sur l'eau (constant)|officielle|
|[baOID7dh8Ikettqn.htm](pathfinder-bestiary-3-items/baOID7dh8Ikettqn.htm)|Magical Broth|Bouillon magique|libre|
|[BaTacjtQvnaLoVc5.htm](pathfinder-bestiary-3-items/BaTacjtQvnaLoVc5.htm)|Wind-Up|Remonter un dispositif|libre|
|[bAYPijLnxouZs1Ea.htm](pathfinder-bestiary-3-items/bAYPijLnxouZs1Ea.htm)|Grab|Empoignade/Agrippement|libre|
|[bBRh5P56Wfgd2pT1.htm](pathfinder-bestiary-3-items/bBRh5P56Wfgd2pT1.htm)|Scroll of Fly (Level 4)|Parchemin de Vol (Niveau 4)|libre|
|[bcflm5XlNEDnnGZ3.htm](pathfinder-bestiary-3-items/bcflm5XlNEDnnGZ3.htm)|Low-Light Vision|Vision nocturne|libre|
|[BCIdFtWYKRXP0vLE.htm](pathfinder-bestiary-3-items/BCIdFtWYKRXP0vLE.htm)|Typhoon Dive|Plongeon typhon|libre|
|[BCsGqHil1Dziu3FX.htm](pathfinder-bestiary-3-items/BCsGqHil1Dziu3FX.htm)|Lower Halberds!|Hallebardes en position !|libre|
|[BcWStlMljvf4ap7v.htm](pathfinder-bestiary-3-items/BcWStlMljvf4ap7v.htm)|Wide Cleave|Affliction élargie|libre|
|[BD35N4Wa7tdNg5y4.htm](pathfinder-bestiary-3-items/BD35N4Wa7tdNg5y4.htm)|Jaws|Mâchoires|libre|
|[BD81Ab7NPFAacuhc.htm](pathfinder-bestiary-3-items/BD81Ab7NPFAacuhc.htm)|Attack of Opportunity (Special)|Attaque d'opportunité (Spéciale)|libre|
|[BDbSH109hF8RleEa.htm](pathfinder-bestiary-3-items/BDbSH109hF8RleEa.htm)|Hatred of Red|Haine du rouge|libre|
|[bDZ6kq6HqfFLK2PU.htm](pathfinder-bestiary-3-items/bDZ6kq6HqfFLK2PU.htm)|Embed|Implanté|libre|
|[BE8uoWMEnODt0Xw7.htm](pathfinder-bestiary-3-items/BE8uoWMEnODt0Xw7.htm)|Swallow Whole|Gober|libre|
|[BEcAt0SdsvIa6qp9.htm](pathfinder-bestiary-3-items/BEcAt0SdsvIa6qp9.htm)|Befuddling Lash|Queue déroutante|libre|
|[befWMYmJQKeMMpzD.htm](pathfinder-bestiary-3-items/befWMYmJQKeMMpzD.htm)|Club|Gourdin|libre|
|[BFhitQjxtXx7cQKS.htm](pathfinder-bestiary-3-items/BFhitQjxtXx7cQKS.htm)|Sting Shot|Projection de dard|libre|
|[BFS5mQ3ITub7ZdJL.htm](pathfinder-bestiary-3-items/BFS5mQ3ITub7ZdJL.htm)|Claw|Griffe|libre|
|[BG9o62cI16MBV3Qy.htm](pathfinder-bestiary-3-items/BG9o62cI16MBV3Qy.htm)|Darkvision|Vision dans le noir|libre|
|[BgiZcqILhEbxEVG7.htm](pathfinder-bestiary-3-items/BgiZcqILhEbxEVG7.htm)|In Concert|En harmonie|libre|
|[BgqCS9ExYivBMHU2.htm](pathfinder-bestiary-3-items/BgqCS9ExYivBMHU2.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[bgr9Qyvp6pX1W3KQ.htm](pathfinder-bestiary-3-items/bgr9Qyvp6pX1W3KQ.htm)|Raktavarna Venom|Venin de raktavarna|libre|
|[bgwyucC0EpyEmxl7.htm](pathfinder-bestiary-3-items/bgwyucC0EpyEmxl7.htm)|Change Shape|Changement de forme|libre|
|[Bh4tXIFDTCdh3Vzs.htm](pathfinder-bestiary-3-items/Bh4tXIFDTCdh3Vzs.htm)|Breath Weapon|Arme de souffle|libre|
|[BhejT6zPZ9fehFxC.htm](pathfinder-bestiary-3-items/BhejT6zPZ9fehFxC.htm)|Tree Shape (See Forest Shape)|Morphologie d'arbre (voir Morphologie de forêt)|libre|
|[BHN2n3G77QRNLHhM.htm](pathfinder-bestiary-3-items/BHN2n3G77QRNLHhM.htm)|Stinger|Dard|libre|
|[BHNm9PtnZ9Hrj6fQ.htm](pathfinder-bestiary-3-items/BHNm9PtnZ9Hrj6fQ.htm)|Unsettled Mind|Esprit agité|libre|
|[bhqneDVB7PGBIMMh.htm](pathfinder-bestiary-3-items/bhqneDVB7PGBIMMh.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[BI9PPwdfEfP8sJv5.htm](pathfinder-bestiary-3-items/BI9PPwdfEfP8sJv5.htm)|Constant Spells|Sorts constants|libre|
|[BIKoqqYwjx9IZ1th.htm](pathfinder-bestiary-3-items/BIKoqqYwjx9IZ1th.htm)|Fox's Cunning|Ruse du renard|libre|
|[bIOojNVEElRivlmd.htm](pathfinder-bestiary-3-items/bIOojNVEElRivlmd.htm)|Claw|Griffe|libre|
|[BISNEn6kmVke1CIR.htm](pathfinder-bestiary-3-items/BISNEn6kmVke1CIR.htm)|Forest Shape|Morphologie de forêt|libre|
|[bjqB60qqpfDFqKNU.htm](pathfinder-bestiary-3-items/bjqB60qqpfDFqKNU.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[bJtDTWk5J19kyrIU.htm](pathfinder-bestiary-3-items/bJtDTWk5J19kyrIU.htm)|Dimension of Time Lore|Connaissance de la Dimension du Temps|libre|
|[bJUdfd279JTmWONI.htm](pathfinder-bestiary-3-items/bJUdfd279JTmWONI.htm)|Door|Porte|libre|
|[bkajUVm6XDKbIO89.htm](pathfinder-bestiary-3-items/bkajUVm6XDKbIO89.htm)|Spike|Pique|libre|
|[BKsoNNVMI5d9mEhL.htm](pathfinder-bestiary-3-items/BKsoNNVMI5d9mEhL.htm)|All-Around Vision|Vision panoramique|libre|
|[BlH8W4GE6xpZoZM2.htm](pathfinder-bestiary-3-items/BlH8W4GE6xpZoZM2.htm)|Troop Defenses|Défenses des troupes|libre|
|[BLkAKMlADR3rQxEw.htm](pathfinder-bestiary-3-items/BLkAKMlADR3rQxEw.htm)|Troop Defenses|Défenses des troupes|libre|
|[bLo1G95EZjpM4Zf6.htm](pathfinder-bestiary-3-items/bLo1G95EZjpM4Zf6.htm)|Arcane Innate Spells|Sorts arcaniques innés|libre|
|[blSV9HiaoSCWQT9w.htm](pathfinder-bestiary-3-items/blSV9HiaoSCWQT9w.htm)|Grab|Empoignade/Agrippement|libre|
|[BlSwWRr9dRFacb9z.htm](pathfinder-bestiary-3-items/BlSwWRr9dRFacb9z.htm)|Warfare Lore|Connaissance de la guerre|officielle|
|[bLxMh4Pt4sU0x8Pp.htm](pathfinder-bestiary-3-items/bLxMh4Pt4sU0x8Pp.htm)|Telepathic Singer|Chanteur télépathique|libre|
|[bMrJDUmLpocEes2w.htm](pathfinder-bestiary-3-items/bMrJDUmLpocEes2w.htm)|Draconic Momentum|Impulsion draconique|libre|
|[bMrmRT314fkcPeuk.htm](pathfinder-bestiary-3-items/bMrmRT314fkcPeuk.htm)|Jaws|Mâchoires|libre|
|[bnBa5XcA2wHzP5KM.htm](pathfinder-bestiary-3-items/bnBa5XcA2wHzP5KM.htm)|Champion Devotion Spells|Sorts de dévotion de champion|libre|
|[bnDqqEqGa8k1Und0.htm](pathfinder-bestiary-3-items/bnDqqEqGa8k1Und0.htm)|Plane Shift (Self Only)|Changement de plan (soi uniquement)|libre|
|[bnU4poD2v5S9mLDT.htm](pathfinder-bestiary-3-items/bnU4poD2v5S9mLDT.htm)|Eternal Fear|Peur éternelle|libre|
|[Bo9liMM0Qn69gKdi.htm](pathfinder-bestiary-3-items/Bo9liMM0Qn69gKdi.htm)|Grab|Empoignade/Agrippement|libre|
|[bpNYXau9AAUwSo12.htm](pathfinder-bestiary-3-items/bpNYXau9AAUwSo12.htm)|Invisibility (At Will) (Self Only)|Invisibilité (À volonté) (soi uniquement)|officielle|
|[bpvnWZi8sukcfy9H.htm](pathfinder-bestiary-3-items/bpvnWZi8sukcfy9H.htm)|Displace|Modification de position|libre|
|[bqDyC6IKtXktariv.htm](pathfinder-bestiary-3-items/bqDyC6IKtXktariv.htm)|Needle|Aiguille|libre|
|[BqST74R7dRaivdDQ.htm](pathfinder-bestiary-3-items/BqST74R7dRaivdDQ.htm)|Beak|Bec|libre|
|[bQxZThiBIxPwNuDH.htm](pathfinder-bestiary-3-items/bQxZThiBIxPwNuDH.htm)|Sea Spray|Geyser d'eau salée|libre|
|[bQZIEB7cmxSGgulV.htm](pathfinder-bestiary-3-items/bQZIEB7cmxSGgulV.htm)|Jaws|Mâchoires|libre|
|[br8FCwfVCLAXisnU.htm](pathfinder-bestiary-3-items/br8FCwfVCLAXisnU.htm)|Dragon's Wisdom|Sagesse du dragon|libre|
|[brAwbDIfmKeRHjjo.htm](pathfinder-bestiary-3-items/brAwbDIfmKeRHjjo.htm)|Verdant Burst|Explosion verdoyante|libre|
|[bRWkzuZ2ByRvUEXx.htm](pathfinder-bestiary-3-items/bRWkzuZ2ByRvUEXx.htm)|Negative Healing|Guérison négative|libre|
|[BRY6lAS6ePaNJsIO.htm](pathfinder-bestiary-3-items/BRY6lAS6ePaNJsIO.htm)|Disk Rider|Chevaucheur de disque|libre|
|[BrYFJCgLri32rKt1.htm](pathfinder-bestiary-3-items/BrYFJCgLri32rKt1.htm)|Tongues (Constant)|Don des langues (constant)|officielle|
|[bScgZ7J4SQWXIPfp.htm](pathfinder-bestiary-3-items/bScgZ7J4SQWXIPfp.htm)|Void Transmission|Transmission par le vide|libre|
|[bsKbGLCEij4kqAPY.htm](pathfinder-bestiary-3-items/bsKbGLCEij4kqAPY.htm)|Ice Staff|Bâton de glace|libre|
|[BskkanKxSINfDBr9.htm](pathfinder-bestiary-3-items/BskkanKxSINfDBr9.htm)|Bittersweet Dreams|Rêves aigres-doux|libre|
|[BsVgKelZGZWfZaYs.htm](pathfinder-bestiary-3-items/BsVgKelZGZWfZaYs.htm)|Greatclub|Massue|libre|
|[bTNygOkcj07sIda5.htm](pathfinder-bestiary-3-items/bTNygOkcj07sIda5.htm)|Improved Grab|Empoignade/Agrippement améliorés|libre|
|[btVPiH1rJHK8iziH.htm](pathfinder-bestiary-3-items/btVPiH1rJHK8iziH.htm)|Elven Curve Blade|Lame courbe elfique|libre|
|[BTxgwCFuROqASlPz.htm](pathfinder-bestiary-3-items/BTxgwCFuROqASlPz.htm)|Darkvision|Vision dans le noir|libre|
|[bUFMLfwRNZ1zBmqa.htm](pathfinder-bestiary-3-items/bUFMLfwRNZ1zBmqa.htm)|Swarm Mind|Esprit de la nuée|libre|
|[BurQSnXk9enXhVZH.htm](pathfinder-bestiary-3-items/BurQSnXk9enXhVZH.htm)|Steal Breath|Vol de souffle|libre|
|[BVccjoWLx8QgvIDh.htm](pathfinder-bestiary-3-items/BVccjoWLx8QgvIDh.htm)|Thoughtsense (Imprecise) 60 feet|Perception des pensées 18 m (imprécis)|libre|
|[BVGtxqRvaiXmxNDG.htm](pathfinder-bestiary-3-items/BVGtxqRvaiXmxNDG.htm)|Rage of Spirits|Rage des esprits liés|libre|
|[BvmTCIQKaeHE9MSd.htm](pathfinder-bestiary-3-items/BvmTCIQKaeHE9MSd.htm)|Shadow Plane Lore|Connaissance du plan de l'Ombre|officielle|
|[BVZS9T28bimG6NBU.htm](pathfinder-bestiary-3-items/BVZS9T28bimG6NBU.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[BW9Qv2EOWjmgeEgL.htm](pathfinder-bestiary-3-items/BW9Qv2EOWjmgeEgL.htm)|Inspiration|Inspiration|libre|
|[BwimiLeXJKeEigfN.htm](pathfinder-bestiary-3-items/BwimiLeXJKeEigfN.htm)|Tail|Queue|libre|
|[BWPZgNFWXwjeS6rr.htm](pathfinder-bestiary-3-items/BWPZgNFWXwjeS6rr.htm)|Clairaudience (At Will)|Clairaudience (À volonté)|libre|
|[bwScFju6YTBM7feQ.htm](pathfinder-bestiary-3-items/bwScFju6YTBM7feQ.htm)|Horn|Corne|libre|
|[BwU8clOdeUsnJX4w.htm](pathfinder-bestiary-3-items/BwU8clOdeUsnJX4w.htm)|Push|Bousculade|officielle|
|[BX7sT7k5GNZuI9hc.htm](pathfinder-bestiary-3-items/BX7sT7k5GNZuI9hc.htm)|Silver Scissors|Ciseaux en argent|libre|
|[bXIGmBzDzq3zSjQh.htm](pathfinder-bestiary-3-items/bXIGmBzDzq3zSjQh.htm)|Sailing Lore|Connaissance de la navigation maritime|officielle|
|[bxnrpUx8pN5QerLc.htm](pathfinder-bestiary-3-items/bxnrpUx8pN5QerLc.htm)|Divine Prepared Spells|Sorts divins préparés|libre|
|[BXslGLJsOIB3mptW.htm](pathfinder-bestiary-3-items/BXslGLJsOIB3mptW.htm)|Diplomacy|Diplomatie|libre|
|[bYiotnUBfPu81i3V.htm](pathfinder-bestiary-3-items/bYiotnUBfPu81i3V.htm)|Swarm Mind|Esprit de la nuée|libre|
|[BySVNQG4P1lbNbyJ.htm](pathfinder-bestiary-3-items/BySVNQG4P1lbNbyJ.htm)|Mortic Ferocity|Férocité de mortifié|libre|
|[BZPtJSauua8fLt2G.htm](pathfinder-bestiary-3-items/BZPtJSauua8fLt2G.htm)|At-Will Spells|Sorts à volonté|libre|
|[C0NEkV1zwoV5vogY.htm](pathfinder-bestiary-3-items/C0NEkV1zwoV5vogY.htm)|Reconstitution|Reconstitution|libre|
|[C10wjHeJK4PRbQiQ.htm](pathfinder-bestiary-3-items/C10wjHeJK4PRbQiQ.htm)|Jaws|Mâchoires|libre|
|[C1CsUuSohUDsve6I.htm](pathfinder-bestiary-3-items/C1CsUuSohUDsve6I.htm)|Air Walk (Constant)|Marche dans les airs (constant)|officielle|
|[C22ZfqwOAI1G9r1O.htm](pathfinder-bestiary-3-items/C22ZfqwOAI1G9r1O.htm)|Maul|Maillet|libre|
|[c2bv8VJPrEoXworT.htm](pathfinder-bestiary-3-items/c2bv8VJPrEoXworT.htm)|Ventriloquism (At Will)|Ventriloquie (À volonté)|officielle|
|[C2fTIwqzj871lbht.htm](pathfinder-bestiary-3-items/C2fTIwqzj871lbht.htm)|Fear (At Will)|Terreur (À volonté)|officielle|
|[c2jgSAE0FH6AmDJ2.htm](pathfinder-bestiary-3-items/c2jgSAE0FH6AmDJ2.htm)|Jaws|Mâchoires|libre|
|[c2zqPCnz9E8UlKRw.htm](pathfinder-bestiary-3-items/c2zqPCnz9E8UlKRw.htm)|Arcane Innate Spells|Sorts arcaniques innés|libre|
|[c3sGdZtwuCDBGlig.htm](pathfinder-bestiary-3-items/c3sGdZtwuCDBGlig.htm)|Shortbow|Arc court|libre|
|[C6Fl3KvNV5ml8FIL.htm](pathfinder-bestiary-3-items/C6Fl3KvNV5ml8FIL.htm)|Stealth|Discrétion|libre|
|[c85RY0nsV1xyDA3t.htm](pathfinder-bestiary-3-items/c85RY0nsV1xyDA3t.htm)|Improved Grab|Empoignade/Agrippement améliorés|libre|
|[C8Ncqnw55IXOEyme.htm](pathfinder-bestiary-3-items/C8Ncqnw55IXOEyme.htm)|Pest Form (Monkey Only)|Forme de nuisible (uniquement singe)|libre|
|[C8pZwyrtELK6lVYw.htm](pathfinder-bestiary-3-items/C8pZwyrtELK6lVYw.htm)|Arrow of Despair|Flèche de désespoir|libre|
|[c95y554eFZ79003o.htm](pathfinder-bestiary-3-items/c95y554eFZ79003o.htm)|Grab|Empoignade/Agrippement|libre|
|[caAfIPQy8oLw6CGa.htm](pathfinder-bestiary-3-items/caAfIPQy8oLw6CGa.htm)|At-Will Spells|Sorts à volonté|libre|
|[CAgNpEik2Ky3XXu4.htm](pathfinder-bestiary-3-items/CAgNpEik2Ky3XXu4.htm)|Retract|Se rétracter|libre|
|[caybfvEpmrHbdtN6.htm](pathfinder-bestiary-3-items/caybfvEpmrHbdtN6.htm)|Return Arrow|Renvoi de flèche|libre|
|[cb5dMTXksovLZMVP.htm](pathfinder-bestiary-3-items/cb5dMTXksovLZMVP.htm)|Claw|Griffe|libre|
|[cBLNcI8XKhBuOFqE.htm](pathfinder-bestiary-3-items/cBLNcI8XKhBuOFqE.htm)|Claw|Griffe|libre|
|[CC7MztEx8rEoNgaL.htm](pathfinder-bestiary-3-items/CC7MztEx8rEoNgaL.htm)|Longsword|Épée longue|libre|
|[CChXCrOyQxz6h0X9.htm](pathfinder-bestiary-3-items/CChXCrOyQxz6h0X9.htm)|Abeyance Rift|Rupture mémorielle|libre|
|[CcOaWq1ZikuzfZ88.htm](pathfinder-bestiary-3-items/CcOaWq1ZikuzfZ88.htm)|Tenebral Form|Forme ombrale|libre|
|[CCUY44whvoSSEz3I.htm](pathfinder-bestiary-3-items/CCUY44whvoSSEz3I.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[Cdh7brQkfnpWbZDJ.htm](pathfinder-bestiary-3-items/Cdh7brQkfnpWbZDJ.htm)|Mandibles|Mandibules|libre|
|[cdmwwMzpk0Bsc4Jg.htm](pathfinder-bestiary-3-items/cdmwwMzpk0Bsc4Jg.htm)|Jaws|Mâchoires|libre|
|[cdrifaen6W903klB.htm](pathfinder-bestiary-3-items/cdrifaen6W903klB.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[ce63eYi0AotvHaz2.htm](pathfinder-bestiary-3-items/ce63eYi0AotvHaz2.htm)|Constant Spells|Sorts constants|libre|
|[cE6juVvy0O4iJMgY.htm](pathfinder-bestiary-3-items/cE6juVvy0O4iJMgY.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[CEHvST4zuzVjG0oO.htm](pathfinder-bestiary-3-items/CEHvST4zuzVjG0oO.htm)|Scent (Imprecise) 60 feet|Odorat 18 m (imprécis)|libre|
|[ceUAQwTriMohyVjk.htm](pathfinder-bestiary-3-items/ceUAQwTriMohyVjk.htm)|Swooping Dive|Plongeon plané|libre|
|[CFiD5D2XvZ2N67x1.htm](pathfinder-bestiary-3-items/CFiD5D2XvZ2N67x1.htm)|Swarming Gnaw|Rongement de nuée|libre|
|[cfPdfe58QE4eOHM0.htm](pathfinder-bestiary-3-items/cfPdfe58QE4eOHM0.htm)|Fear (At Will)|Terreur (À volonté)|officielle|
|[cFSlH4qPRNdzcxY6.htm](pathfinder-bestiary-3-items/cFSlH4qPRNdzcxY6.htm)|Tide of Creation|Marée de création|libre|
|[CG0ROboEmdpQKFaJ.htm](pathfinder-bestiary-3-items/CG0ROboEmdpQKFaJ.htm)|Stealth|Discrétion|libre|
|[cg8fgSm6sGAJE6bt.htm](pathfinder-bestiary-3-items/cg8fgSm6sGAJE6bt.htm)|Fast Healing 10|Guérison accélérée 10|libre|
|[CGIXYmLYYlMz2iRx.htm](pathfinder-bestiary-3-items/CGIXYmLYYlMz2iRx.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[cgNs8rXo0upiL2I7.htm](pathfinder-bestiary-3-items/cgNs8rXo0upiL2I7.htm)|Invisibility (Self Only) (At Will)|Invisibilité (soi uniquement)(À volonté)|libre|
|[CgQW6ZFOrGsrgkHZ.htm](pathfinder-bestiary-3-items/CgQW6ZFOrGsrgkHZ.htm)|Throw Rock|Projection de rocher|libre|
|[CGyzuG0hwimkA0im.htm](pathfinder-bestiary-3-items/CGyzuG0hwimkA0im.htm)|Champion Devotion Spells|Sorts de dévotion de champion|libre|
|[CHKrjbjG2iuM6dGa.htm](pathfinder-bestiary-3-items/CHKrjbjG2iuM6dGa.htm)|Black Apoxia|Hypoxie noire|libre|
|[cIdzhTylzbVeNykJ.htm](pathfinder-bestiary-3-items/cIdzhTylzbVeNykJ.htm)|Arcane Innate Spells|Sorts arcaniques innés|libre|
|[CjNF1hyzoK7u7txT.htm](pathfinder-bestiary-3-items/CjNF1hyzoK7u7txT.htm)|Wind Blast|Déferlement de vent|libre|
|[ck37LN0pcQmPLf0a.htm](pathfinder-bestiary-3-items/ck37LN0pcQmPLf0a.htm)|Troop Movement|Mouvement de troupe|libre|
|[CK5NsrPLZx91r8av.htm](pathfinder-bestiary-3-items/CK5NsrPLZx91r8av.htm)|Constant Spells|Sorts constants|libre|
|[CKhA0wknMwjoGOGJ.htm](pathfinder-bestiary-3-items/CKhA0wknMwjoGOGJ.htm)|Low-Light Vision|Vision nocturne|libre|
|[cLFteTTZ1b4O3AoI.htm](pathfinder-bestiary-3-items/cLFteTTZ1b4O3AoI.htm)|Fire Crossbows!|Volée de carreaux|libre|
|[cm8kh6khpDzrtqm7.htm](pathfinder-bestiary-3-items/cm8kh6khpDzrtqm7.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[CMbpDKN5GasPKdEy.htm](pathfinder-bestiary-3-items/CMbpDKN5GasPKdEy.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[cNgD6mqVpe7o2GOL.htm](pathfinder-bestiary-3-items/cNgD6mqVpe7o2GOL.htm)|Dungeon Lore|Connaissance des donjons|libre|
|[cp5UE666Pg5Pz1gR.htm](pathfinder-bestiary-3-items/cp5UE666Pg5Pz1gR.htm)|Shadow Step|Pas d'ombre|libre|
|[cpVCCJ4aUToJ2wyU.htm](pathfinder-bestiary-3-items/cpVCCJ4aUToJ2wyU.htm)|Beak|Bec|libre|
|[cpZes8po87QoRYMi.htm](pathfinder-bestiary-3-items/cpZes8po87QoRYMi.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[cQCs1PH16u35vCzT.htm](pathfinder-bestiary-3-items/cQCs1PH16u35vCzT.htm)|Countered by Fire|Contré par le feu|libre|
|[cQMEu0nbq6cPn2sD.htm](pathfinder-bestiary-3-items/cQMEu0nbq6cPn2sD.htm)|Hellstrider|Arpenteur des enfers|libre|
|[CqrOCl7qaEx1GVuB.htm](pathfinder-bestiary-3-items/CqrOCl7qaEx1GVuB.htm)|Powerful Charge|Charge puissante|libre|
|[CR2hQQQ9hlXe7miP.htm](pathfinder-bestiary-3-items/CR2hQQQ9hlXe7miP.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[CrApsTeWiiQiyew0.htm](pathfinder-bestiary-3-items/CrApsTeWiiQiyew0.htm)|Living Machine|Machine vivante|libre|
|[CRN9VNs8XxxFho4z.htm](pathfinder-bestiary-3-items/CRN9VNs8XxxFho4z.htm)|Low-Light Vision|Vision nocturne|libre|
|[cRVYRqrBJjmjPGU5.htm](pathfinder-bestiary-3-items/cRVYRqrBJjmjPGU5.htm)|Augury (At Will)|Augure (À volonté)|officielle|
|[Cs1rBxjb2dOVjN5V.htm](pathfinder-bestiary-3-items/Cs1rBxjb2dOVjN5V.htm)|Rock|Rocher|libre|
|[CsRpTNQIfspNNH9f.htm](pathfinder-bestiary-3-items/CsRpTNQIfspNNH9f.htm)|Darkvision|Vision dans le noir|libre|
|[ct5v7Ozu8ADoqRMV.htm](pathfinder-bestiary-3-items/ct5v7Ozu8ADoqRMV.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[ctbxOCAPmqkVJNSg.htm](pathfinder-bestiary-3-items/ctbxOCAPmqkVJNSg.htm)|Detect Alignment (At Will) (Evil Only)|Détection de l'alignement (à volonté, mauvais uniquement)|officielle|
|[CTcOXdYuenSJsDQC.htm](pathfinder-bestiary-3-items/CTcOXdYuenSJsDQC.htm)|Suggestion (At Will)|Suggestion (À volonté)|officielle|
|[CTfA2dy8dT55Ygfa.htm](pathfinder-bestiary-3-items/CTfA2dy8dT55Ygfa.htm)|Knockdown|Renversement|libre|
|[cTfE4TeDdxDyNmZt.htm](pathfinder-bestiary-3-items/cTfE4TeDdxDyNmZt.htm)|Darkvision|Vision dans le noir|libre|
|[ctFijrOvAkTERCu9.htm](pathfinder-bestiary-3-items/ctFijrOvAkTERCu9.htm)|Water Breathing (Constant)|Respiration aquatique (constant)|libre|
|[CU8RCoSIPpjOMZoO.htm](pathfinder-bestiary-3-items/CU8RCoSIPpjOMZoO.htm)|Greataxe|+1,striking|Grande hache de frappe +1|libre|
|[cUAlKL5D2aom7SjG.htm](pathfinder-bestiary-3-items/cUAlKL5D2aom7SjG.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[cuBDxCCJtk4iMr0A.htm](pathfinder-bestiary-3-items/cuBDxCCJtk4iMr0A.htm)|Trailblazing Stride|Marche des baroudeurs|libre|
|[CucaKzBXWexDGgV3.htm](pathfinder-bestiary-3-items/CucaKzBXWexDGgV3.htm)|Swing Back|Pivotement|libre|
|[cUQLbIojBehGDQn1.htm](pathfinder-bestiary-3-items/cUQLbIojBehGDQn1.htm)|Frightful Presence|Présence terrifiante|libre|
|[cur7nFf5cRCA5ccx.htm](pathfinder-bestiary-3-items/cur7nFf5cRCA5ccx.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[CuvZkG5iAgBSIV6w.htm](pathfinder-bestiary-3-items/CuvZkG5iAgBSIV6w.htm)|Darkvision|Vision dans le noir|libre|
|[cUvzo0ln0c4WgCh2.htm](pathfinder-bestiary-3-items/cUvzo0ln0c4WgCh2.htm)|Mind Swap|Échange d'esprit|libre|
|[Cv7lau7bBNhJQwti.htm](pathfinder-bestiary-3-items/Cv7lau7bBNhJQwti.htm)|Fist|Poing|libre|
|[CvF5ABLW3yZL4Pya.htm](pathfinder-bestiary-3-items/CvF5ABLW3yZL4Pya.htm)|Dagger|Dague|libre|
|[cVJFpIMkobfBe9gI.htm](pathfinder-bestiary-3-items/cVJFpIMkobfBe9gI.htm)|Sailing Lore|Connaissance de la navigation|officielle|
|[CVVLZgcOZxdjSrnH.htm](pathfinder-bestiary-3-items/CVVLZgcOZxdjSrnH.htm)|Wasting Curse|Malédiction débilitante|libre|
|[Cw4UJYMkN1OxnJo9.htm](pathfinder-bestiary-3-items/Cw4UJYMkN1OxnJo9.htm)|Grab|Empoignade/Agrippement|libre|
|[cwfUBFIbuyqydNEj.htm](pathfinder-bestiary-3-items/cwfUBFIbuyqydNEj.htm)|Darkvision|Vision dans le noir|libre|
|[CWqtP7800C9YheCY.htm](pathfinder-bestiary-3-items/CWqtP7800C9YheCY.htm)|Constant Spells|Sorts constants|libre|
|[cwvyDjJgUkCmj3aI.htm](pathfinder-bestiary-3-items/cwvyDjJgUkCmj3aI.htm)|Adamantine Claws|Griffes en adamantium|libre|
|[cwzm6hgGDNWyBDjU.htm](pathfinder-bestiary-3-items/cwzm6hgGDNWyBDjU.htm)|Draconic Momentum|Impulsion draconique|libre|
|[cXcE5HUwD2KD7fgp.htm](pathfinder-bestiary-3-items/cXcE5HUwD2KD7fgp.htm)|Regeneration 30 (Deactivated by Acid, Cold, or Fire)|Régénération 30 (Désactivée par Acide, Froid ou Feu)|libre|
|[cXE4SYOLUHsl6waW.htm](pathfinder-bestiary-3-items/cXE4SYOLUHsl6waW.htm)|Darkvision|Vision dans le noir|libre|
|[cxN4u9cT3UrjnDry.htm](pathfinder-bestiary-3-items/cxN4u9cT3UrjnDry.htm)|Water Jet|Projection d'eau|libre|
|[CybU0X1fwtJZX5W4.htm](pathfinder-bestiary-3-items/CybU0X1fwtJZX5W4.htm)|Telepathy 60 feet|Télépathie 18 m|libre|
|[CYFB6ARFi1FTJRUe.htm](pathfinder-bestiary-3-items/CYFB6ARFi1FTJRUe.htm)|Low-Light Vision|Vision nocturne|libre|
|[cyqkRqwp35f8CtAJ.htm](pathfinder-bestiary-3-items/cyqkRqwp35f8CtAJ.htm)|Whispers of Discord|Murmures de dispute|libre|
|[Cz9TDLxidHRU8bVI.htm](pathfinder-bestiary-3-items/Cz9TDLxidHRU8bVI.htm)|Psychic Shard|Éclat psychique|libre|
|[CzKSMmeRg2rmeWlU.htm](pathfinder-bestiary-3-items/CzKSMmeRg2rmeWlU.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[CZmwCcrjPndjeWl3.htm](pathfinder-bestiary-3-items/CZmwCcrjPndjeWl3.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[CZTZt5TtBNcJtjOp.htm](pathfinder-bestiary-3-items/CZTZt5TtBNcJtjOp.htm)|Uncanny Pounce|Bond troublant|libre|
|[czupLRtK5lXMuEaI.htm](pathfinder-bestiary-3-items/czupLRtK5lXMuEaI.htm)|Telepathy 80 feet|Télépathie 15 m|libre|
|[D0ZfsdnVuEVXFyqB.htm](pathfinder-bestiary-3-items/D0ZfsdnVuEVXFyqB.htm)|Focused Slam|Claque focalisée|libre|
|[D1C3re9l8xuzleoq.htm](pathfinder-bestiary-3-items/D1C3re9l8xuzleoq.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[d1cyC0vptbTtXFE2.htm](pathfinder-bestiary-3-items/d1cyC0vptbTtXFE2.htm)|Dagger|Dague|libre|
|[D1Nl8ZLF8I2wRwxv.htm](pathfinder-bestiary-3-items/D1Nl8ZLF8I2wRwxv.htm)|Hoof|Sabot|libre|
|[d26LgNmNMpMAAoX3.htm](pathfinder-bestiary-3-items/d26LgNmNMpMAAoX3.htm)|Calculated Reload|Réarmement calculé|libre|
|[d2g5vbtRBlZsAPu4.htm](pathfinder-bestiary-3-items/d2g5vbtRBlZsAPu4.htm)|Wrathful Misfortune|Infortune vengeresse|libre|
|[d3CPZluPoEOBmUDr.htm](pathfinder-bestiary-3-items/d3CPZluPoEOBmUDr.htm)|Arcane Innate Spells|Sorts arcaniques innés|libre|
|[D4ExwJPbq9vP2fZU.htm](pathfinder-bestiary-3-items/D4ExwJPbq9vP2fZU.htm)|Shy|Timide|libre|
|[D4noCa8oJeJbAEEn.htm](pathfinder-bestiary-3-items/D4noCa8oJeJbAEEn.htm)|Change Shape|Changement de forme|libre|
|[d5EqozxY8XEY3NIw.htm](pathfinder-bestiary-3-items/d5EqozxY8XEY3NIw.htm)|Telekinetic Maneuver (At Will)|Manoeuvre télékinésique (À volonté)|officielle|
|[D5k9m1BTcXvhogT6.htm](pathfinder-bestiary-3-items/D5k9m1BTcXvhogT6.htm)|Composite Shortbow|Arc court composite|libre|
|[d5pg9c64JL7EqHVV.htm](pathfinder-bestiary-3-items/d5pg9c64JL7EqHVV.htm)|Earth Glide|Glisse à travers la terre|officielle|
|[D6GffrGRbXblVQ49.htm](pathfinder-bestiary-3-items/D6GffrGRbXblVQ49.htm)|Transparent|Transparence|libre|
|[D6XunANSlrJ1WOJS.htm](pathfinder-bestiary-3-items/D6XunANSlrJ1WOJS.htm)|Tilt Scales|Écailles mobiles|libre|
|[d7MLmQG9ZkBIstUb.htm](pathfinder-bestiary-3-items/d7MLmQG9ZkBIstUb.htm)|Compulsive Counting|Comptage compulsif|libre|
|[d85usCDnVlCRnJzP.htm](pathfinder-bestiary-3-items/d85usCDnVlCRnJzP.htm)|Darkvision|Vision dans le noir|libre|
|[d8kaihBUHhOfdpSO.htm](pathfinder-bestiary-3-items/d8kaihBUHhOfdpSO.htm)|Coiling Frenzy|Frénésie torsadée|libre|
|[D8VyX86E4dNG3pvk.htm](pathfinder-bestiary-3-items/D8VyX86E4dNG3pvk.htm)|Katana|Katana|officielle|
|[d8xFQfk53fDdc7Ge.htm](pathfinder-bestiary-3-items/d8xFQfk53fDdc7Ge.htm)|Telepathy (Touch)|Télépathie (Contact)|libre|
|[d9VRUtPpmcA0zUfF.htm](pathfinder-bestiary-3-items/d9VRUtPpmcA0zUfF.htm)|Darkvision|Vision dans le noir|libre|
|[DA63Y6dqt1pn07ex.htm](pathfinder-bestiary-3-items/DA63Y6dqt1pn07ex.htm)|Sludge Tendril|Tentacule de vase|libre|
|[DADYHBK8ittlLsj9.htm](pathfinder-bestiary-3-items/DADYHBK8ittlLsj9.htm)|At-Will Spells|Sorts à volonté|libre|
|[daFMLxLznj0FiDbZ.htm](pathfinder-bestiary-3-items/daFMLxLznj0FiDbZ.htm)|Nirvana Lore|Connaissance du Nirvana|libre|
|[dAiQDFnLDhubSBkJ.htm](pathfinder-bestiary-3-items/dAiQDFnLDhubSBkJ.htm)|Stealth|Discrétion|libre|
|[DAwhqp01jQeK2AM2.htm](pathfinder-bestiary-3-items/DAwhqp01jQeK2AM2.htm)|Wolfstorm|Brume du loup|libre|
|[dbA4U6OCx4Nn4dOe.htm](pathfinder-bestiary-3-items/dbA4U6OCx4Nn4dOe.htm)|Seaweed Strand|Brin d'algue|libre|
|[dBTUlLnCE86ceUMS.htm](pathfinder-bestiary-3-items/dBTUlLnCE86ceUMS.htm)|Low-Light Vision|Vision nocturne|libre|
|[DbyMn4FBjCER4ikN.htm](pathfinder-bestiary-3-items/DbyMn4FBjCER4ikN.htm)|Dance of Destruction|Danse de destruction|libre|
|[DCeikz6Fl0ryJW3v.htm](pathfinder-bestiary-3-items/DCeikz6Fl0ryJW3v.htm)|No Escape|Pas d'échappatoire|libre|
|[DCeV72u09INkg49k.htm](pathfinder-bestiary-3-items/DCeV72u09INkg49k.htm)|Trident|Trident|libre|
|[dCFKo5jQVRwny9tZ.htm](pathfinder-bestiary-3-items/dCFKo5jQVRwny9tZ.htm)|Control Water (At Will)|Contrôle de l'eau (À volonté)|libre|
|[DCLIjWlO5fWrsiw7.htm](pathfinder-bestiary-3-items/DCLIjWlO5fWrsiw7.htm)|Shining Blaze|Emanation de flamboyance|libre|
|[dcxy9KeoPzEN2Fpd.htm](pathfinder-bestiary-3-items/dcxy9KeoPzEN2Fpd.htm)|Lore (any one)|Connaissance (n'importe laquelle)|libre|
|[dd90VYwacTBPrupI.htm](pathfinder-bestiary-3-items/dd90VYwacTBPrupI.htm)|Grioth Venom|Venin grioth|libre|
|[DDcodZwZoVe2IQw6.htm](pathfinder-bestiary-3-items/DDcodZwZoVe2IQw6.htm)|Constant Spells|Sorts constants|libre|
|[dDtA4Al0gFgC9jck.htm](pathfinder-bestiary-3-items/dDtA4Al0gFgC9jck.htm)|Darkvision|Vision dans le noir|libre|
|[dEG5crSMOZ8MheG9.htm](pathfinder-bestiary-3-items/dEG5crSMOZ8MheG9.htm)|Darkvision|Vision dans le noir|libre|
|[DElua88KAjGxlJiI.htm](pathfinder-bestiary-3-items/DElua88KAjGxlJiI.htm)|Cold Adaptation|Adaptation au froid|libre|
|[dEqNcyLplxjymRI4.htm](pathfinder-bestiary-3-items/dEqNcyLplxjymRI4.htm)|Athletics|Athlétisme|libre|
|[deXYiHJyBjDSNFF5.htm](pathfinder-bestiary-3-items/deXYiHJyBjDSNFF5.htm)|Grasping Bites|Morsures agrippantes|libre|
|[df6x7z2JqZ325lY2.htm](pathfinder-bestiary-3-items/df6x7z2JqZ325lY2.htm)|Bite|Morsure|officielle|
|[DF8EHlGsG4hDnHIf.htm](pathfinder-bestiary-3-items/DF8EHlGsG4hDnHIf.htm)|Countered by Metal|Contré par le métal|libre|
|[dGghVG22K6EF1IOo.htm](pathfinder-bestiary-3-items/dGghVG22K6EF1IOo.htm)|Twin Bites|Morsures jumelles|libre|
|[DHbKIfHAeaazMHgH.htm](pathfinder-bestiary-3-items/DHbKIfHAeaazMHgH.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|libre|
|[dHCnMadBsh7HsABT.htm](pathfinder-bestiary-3-items/dHCnMadBsh7HsABT.htm)|Polymorphic Appendage|Appendice polymorphe|libre|
|[dhMEgZcYzpqH15gY.htm](pathfinder-bestiary-3-items/dhMEgZcYzpqH15gY.htm)|Chattering Teeth|Claquements de dents|libre|
|[DhZVlrSd475Ljs1k.htm](pathfinder-bestiary-3-items/DhZVlrSd475Ljs1k.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[DJ5RgiOgDCBnlgH3.htm](pathfinder-bestiary-3-items/DJ5RgiOgDCBnlgH3.htm)|Low-Light Vision|Vision nocturne|libre|
|[dJBsPpWCcZqoQuz1.htm](pathfinder-bestiary-3-items/dJBsPpWCcZqoQuz1.htm)|Bardic Lore|Connaissance bardique|officielle|
|[DjcXxE3rldN09uxu.htm](pathfinder-bestiary-3-items/DjcXxE3rldN09uxu.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[dJT018LZV3FEJvJO.htm](pathfinder-bestiary-3-items/dJT018LZV3FEJvJO.htm)|Knockdown|Renversement|libre|
|[DkdUj7zo7tI7BCpF.htm](pathfinder-bestiary-3-items/DkdUj7zo7tI7BCpF.htm)|Jaws|Mâchoires|libre|
|[DKeP0NHGZX49S9dE.htm](pathfinder-bestiary-3-items/DKeP0NHGZX49S9dE.htm)|Grab|Empoignade/Agrippement|libre|
|[dKid8PGYfgX6asC5.htm](pathfinder-bestiary-3-items/dKid8PGYfgX6asC5.htm)|Detect Alignment (Constant)|Détection de l'alignement (constant)|officielle|
|[DkRuMYAHXbZdXuqZ.htm](pathfinder-bestiary-3-items/DkRuMYAHXbZdXuqZ.htm)|Threatening Visage|Expression menaçante|libre|
|[Dl0UbAiU402HXgL9.htm](pathfinder-bestiary-3-items/Dl0UbAiU402HXgL9.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[DL9RtsdSy9Gulbqd.htm](pathfinder-bestiary-3-items/DL9RtsdSy9Gulbqd.htm)|Greatsword|Épée à deux mains|libre|
|[dLMAFMqNUxlmTN4X.htm](pathfinder-bestiary-3-items/dLMAFMqNUxlmTN4X.htm)|Dagger|Dague|libre|
|[DNA5DHqZmcle3dXg.htm](pathfinder-bestiary-3-items/DNA5DHqZmcle3dXg.htm)|Shriek|Hurlement|libre|
|[DNDY9jZtwm6GdoBP.htm](pathfinder-bestiary-3-items/DNDY9jZtwm6GdoBP.htm)|Improved Grab|Empoignade/Agrippement améliorés|libre|
|[DNkqMNI50CBgRZbS.htm](pathfinder-bestiary-3-items/DNkqMNI50CBgRZbS.htm)|Ice Stride|Marche sur la glace|libre|
|[DNMBuy0G69bflxTr.htm](pathfinder-bestiary-3-items/DNMBuy0G69bflxTr.htm)|Tendril|Vrille|libre|
|[doTDspe65aJOvJFL.htm](pathfinder-bestiary-3-items/doTDspe65aJOvJFL.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[DotZCkt036LONE1t.htm](pathfinder-bestiary-3-items/DotZCkt036LONE1t.htm)|Claw|Griffe|libre|
|[DP0QpHqfJo5budhm.htm](pathfinder-bestiary-3-items/DP0QpHqfJo5budhm.htm)|Swallow Whole|Gober|libre|
|[dp14tTXhxb0SZSZq.htm](pathfinder-bestiary-3-items/dp14tTXhxb0SZSZq.htm)|Project Echoblade|Projection de d'écholame|libre|
|[DPf0kYMfi2748Y1L.htm](pathfinder-bestiary-3-items/DPf0kYMfi2748Y1L.htm)|Improved Grab|Empoignade/Agrippement améliorés|libre|
|[Dpji6q4R6gQMLALe.htm](pathfinder-bestiary-3-items/Dpji6q4R6gQMLALe.htm)|Earthen Fist|Poing de terre|libre|
|[dpMfwwbltlZ55vSZ.htm](pathfinder-bestiary-3-items/dpMfwwbltlZ55vSZ.htm)|Final Blasphemy|Ultime blasphème|libre|
|[DpQ8esg6GOdnxHkt.htm](pathfinder-bestiary-3-items/DpQ8esg6GOdnxHkt.htm)|Claw|Griffe|libre|
|[DqllMZa6Ja60FhmJ.htm](pathfinder-bestiary-3-items/DqllMZa6Ja60FhmJ.htm)|Arcane Innate Spells|Sorts arcaniques innés|libre|
|[dQW2DvGO2PSM5XBY.htm](pathfinder-bestiary-3-items/dQW2DvGO2PSM5XBY.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[Dr1ESmEvHchNQHYd.htm](pathfinder-bestiary-3-items/Dr1ESmEvHchNQHYd.htm)|Grab|Empoignade/Agrippement|libre|
|[DRfoCntho3tTjU8x.htm](pathfinder-bestiary-3-items/DRfoCntho3tTjU8x.htm)|Constrict|Constriction|libre|
|[DrR0GXOBDUWyiyEd.htm](pathfinder-bestiary-3-items/DrR0GXOBDUWyiyEd.htm)|Frightful Presence|Présence terrifiante|libre|
|[dRWLZGZySawLWgLz.htm](pathfinder-bestiary-3-items/dRWLZGZySawLWgLz.htm)|Winning Smile|Sourire vainqueur|libre|
|[dS2bG5NB6KkMXMXH.htm](pathfinder-bestiary-3-items/dS2bG5NB6KkMXMXH.htm)|Jaws|Mâchoires|libre|
|[ds6T4axImpnQYao4.htm](pathfinder-bestiary-3-items/ds6T4axImpnQYao4.htm)|Break Ground|Briser le sol|libre|
|[DSDbtNz99LPKAHnV.htm](pathfinder-bestiary-3-items/DSDbtNz99LPKAHnV.htm)|Darkvision|Vision dans le noir|libre|
|[DSdWvRkowqLA8hig.htm](pathfinder-bestiary-3-items/DSdWvRkowqLA8hig.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[dSIKXmZhpwoDtoh3.htm](pathfinder-bestiary-3-items/dSIKXmZhpwoDtoh3.htm)|Beak|Bec|libre|
|[dStg3v3luFQKIonn.htm](pathfinder-bestiary-3-items/dStg3v3luFQKIonn.htm)|Darkvision|Vision dans le noir|libre|
|[DTNOBV0vRtCJQIK8.htm](pathfinder-bestiary-3-items/DTNOBV0vRtCJQIK8.htm)|+1 Resilient Hide Armor|Armure de peau de résilience +1|libre|
|[DToLcWr6RKiExkcA.htm](pathfinder-bestiary-3-items/DToLcWr6RKiExkcA.htm)|Arcane Prepared Spells|Sorts arcaniques préparés|libre|
|[dUtAAFoA9eQnE0J5.htm](pathfinder-bestiary-3-items/dUtAAFoA9eQnE0J5.htm)|Disrupting Staff|Bâton perturbateur|libre|
|[dvHLzTpiciD6p1Yb.htm](pathfinder-bestiary-3-items/dvHLzTpiciD6p1Yb.htm)|Astral Recoil|Rejet astral|libre|
|[DVNHPkNXh0og7150.htm](pathfinder-bestiary-3-items/DVNHPkNXh0og7150.htm)|Dimension Door (At Will)|Porte dimensionnelle (À volonté)|officielle|
|[dW7bkWhXvNcwfUCh.htm](pathfinder-bestiary-3-items/dW7bkWhXvNcwfUCh.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|libre|
|[DW8Cqi1XpUWPpQyd.htm](pathfinder-bestiary-3-items/DW8Cqi1XpUWPpQyd.htm)|Stinger|Dard|libre|
|[dWa7XGI4buL9uDdZ.htm](pathfinder-bestiary-3-items/dWa7XGI4buL9uDdZ.htm)|Feed on Fear|Renforcé par la peur|libre|
|[DwbOG3M8gGZfp6CI.htm](pathfinder-bestiary-3-items/DwbOG3M8gGZfp6CI.htm)|Pry|Arrachage|libre|
|[dwH6nJY0Yb3I4jsA.htm](pathfinder-bestiary-3-items/dwH6nJY0Yb3I4jsA.htm)|Reposition|Repositionnement|libre|
|[dwi92HM5kiYhFz8W.htm](pathfinder-bestiary-3-items/dwi92HM5kiYhFz8W.htm)|Fist|Poing|libre|
|[dwMummZWtYk7b2LJ.htm](pathfinder-bestiary-3-items/dwMummZWtYk7b2LJ.htm)|Final Jape|Farce finale|libre|
|[dWphYFgVjZP3oKOy.htm](pathfinder-bestiary-3-items/dWphYFgVjZP3oKOy.htm)|Negative Healing|Guérison négative|libre|
|[dWtpFgBDu68MZFgx.htm](pathfinder-bestiary-3-items/dWtpFgBDu68MZFgx.htm)|Fist|Poing|libre|
|[DWxhZZHfxKIg0dak.htm](pathfinder-bestiary-3-items/DWxhZZHfxKIg0dak.htm)|Fist|Poing|libre|
|[dX486NKkzEeXKpLt.htm](pathfinder-bestiary-3-items/dX486NKkzEeXKpLt.htm)|Composite Longbow|Arc long composite|libre|
|[dXeWudwXB4aN3eTx.htm](pathfinder-bestiary-3-items/dXeWudwXB4aN3eTx.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[dXTDQTPpz6jKxLYH.htm](pathfinder-bestiary-3-items/dXTDQTPpz6jKxLYH.htm)|Improved Grab|Empoignade/Agrippement améliorés|libre|
|[DY5764x6bU3jfnwu.htm](pathfinder-bestiary-3-items/DY5764x6bU3jfnwu.htm)|Slow|Lent|libre|
|[dYaztoeE08mMvpAK.htm](pathfinder-bestiary-3-items/dYaztoeE08mMvpAK.htm)|Dimension of Time Lore|Connaissance de la Dimension du Temps|libre|
|[dyHPOgwE2Bso99pQ.htm](pathfinder-bestiary-3-items/dyHPOgwE2Bso99pQ.htm)|Antler|Bois|libre|
|[DyOzh90VV1d6GTKS.htm](pathfinder-bestiary-3-items/DyOzh90VV1d6GTKS.htm)|Dimension of Time Lore|Connaissance de la Dimension du Temps|libre|
|[DYPC5uZCfiJo895S.htm](pathfinder-bestiary-3-items/DYPC5uZCfiJo895S.htm)|Manifest|Se manifester|libre|
|[dyqAZBHswn1148hK.htm](pathfinder-bestiary-3-items/dyqAZBHswn1148hK.htm)|Knockdown|Renversement|libre|
|[dyrhkvCX2H3pZBp9.htm](pathfinder-bestiary-3-items/dyrhkvCX2H3pZBp9.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[dYuJ14oybPHNyAOW.htm](pathfinder-bestiary-3-items/dYuJ14oybPHNyAOW.htm)|Divine Prepared Spells|Sorts divins préparés|libre|
|[dyWftDFNWQBORQLX.htm](pathfinder-bestiary-3-items/dyWftDFNWQBORQLX.htm)|Grab Weapon|Agripper l'arme|libre|
|[dZb5yXk5MS02kU1G.htm](pathfinder-bestiary-3-items/dZb5yXk5MS02kU1G.htm)|Brutal Blows|Coup brutal|libre|
|[DzdpbPovIBcXuiRi.htm](pathfinder-bestiary-3-items/DzdpbPovIBcXuiRi.htm)|Claw|Griffe|libre|
|[dzHyKmRIrVao4ke0.htm](pathfinder-bestiary-3-items/dzHyKmRIrVao4ke0.htm)|Darkvision|Vision dans le noir|libre|
|[E06rANySjyXiIjDH.htm](pathfinder-bestiary-3-items/E06rANySjyXiIjDH.htm)|Boiling Blood|Sang bouillant|libre|
|[E1tUKsZc3ldzya9y.htm](pathfinder-bestiary-3-items/E1tUKsZc3ldzya9y.htm)|At-Will Spells|Sorts à volonté|libre|
|[e2fOY59UxAgnqdHF.htm](pathfinder-bestiary-3-items/e2fOY59UxAgnqdHF.htm)|Command Thrall|Diriger l'esclave|libre|
|[e3kt6jZa3bspNiSF.htm](pathfinder-bestiary-3-items/e3kt6jZa3bspNiSF.htm)|Anchored Soul|Âme ancrée|libre|
|[e3ozogxqOi4jcWiU.htm](pathfinder-bestiary-3-items/e3ozogxqOi4jcWiU.htm)|Pungent Aura|Odeur âcre|libre|
|[E3RAvTSsOhNonHz1.htm](pathfinder-bestiary-3-items/E3RAvTSsOhNonHz1.htm)|Constant Spells|Sorts constants|libre|
|[e4gcYFnxnq6FmPws.htm](pathfinder-bestiary-3-items/e4gcYFnxnq6FmPws.htm)|Deadly Spark|Étincelle mortelle|libre|
|[E4Nclckgv1bSz78W.htm](pathfinder-bestiary-3-items/E4Nclckgv1bSz78W.htm)|Blood Scent|Perception du sang|officielle|
|[e4q0ijGMv88voluX.htm](pathfinder-bestiary-3-items/e4q0ijGMv88voluX.htm)|Grab|Empoignade/Agrippement|libre|
|[E5jGyO2W780NMvYV.htm](pathfinder-bestiary-3-items/E5jGyO2W780NMvYV.htm)|Antler|Bois|libre|
|[e6Ag02OjJBtEzB5f.htm](pathfinder-bestiary-3-items/e6Ag02OjJBtEzB5f.htm)|Acrobatics|Acrobaties|libre|
|[E6dBhZRBf4SXzGl7.htm](pathfinder-bestiary-3-items/E6dBhZRBf4SXzGl7.htm)|Resize Plant|Redimensionner les plantes|libre|
|[e6ZkEbkFGX8LfATR.htm](pathfinder-bestiary-3-items/e6ZkEbkFGX8LfATR.htm)|Rock|Rochers|libre|
|[e81mjeZGp9mKBDKv.htm](pathfinder-bestiary-3-items/e81mjeZGp9mKBDKv.htm)|Change Shape|Changement de forme|libre|
|[EAC12Ob2QRDH7V3R.htm](pathfinder-bestiary-3-items/EAC12Ob2QRDH7V3R.htm)|Breath Weapon|Arme de souffle|libre|
|[EaFbQFfNsYFJLrSK.htm](pathfinder-bestiary-3-items/EaFbQFfNsYFJLrSK.htm)|Fangs|Crocs|libre|
|[eaYQr6jbgAf1fd2l.htm](pathfinder-bestiary-3-items/eaYQr6jbgAf1fd2l.htm)|Jaws|Mâchoires|libre|
|[eb3vh43tEB1z2vtJ.htm](pathfinder-bestiary-3-items/eb3vh43tEB1z2vtJ.htm)|At-Will Spells|Sorts à volonté|libre|
|[ebO4IFQGFnQQfnSq.htm](pathfinder-bestiary-3-items/ebO4IFQGFnQQfnSq.htm)|Scent (Imprecise) 60 feet|Odorat 18 m (imprécis)|libre|
|[EbsFN4gbz3zpDwft.htm](pathfinder-bestiary-3-items/EbsFN4gbz3zpDwft.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[ebuEFfG89GEGr5RZ.htm](pathfinder-bestiary-3-items/ebuEFfG89GEGr5RZ.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[eBzYlEBFycddJLAk.htm](pathfinder-bestiary-3-items/eBzYlEBFycddJLAk.htm)|Hunt Prey|Chasser sa proie|libre|
|[ECl6ybDr5qUy0KF6.htm](pathfinder-bestiary-3-items/ECl6ybDr5qUy0KF6.htm)|Fed by Wood|Renforcé par le bois|libre|
|[EcU0YXprTtXbRRmY.htm](pathfinder-bestiary-3-items/EcU0YXprTtXbRRmY.htm)|Bloodbird|Oiseau sanguinaire|libre|
|[ECVcMDSSzJXSESDi.htm](pathfinder-bestiary-3-items/ECVcMDSSzJXSESDi.htm)|Claw|Griffe|libre|
|[eD9aNvigXSOpv46o.htm](pathfinder-bestiary-3-items/eD9aNvigXSOpv46o.htm)|Ultrasonic Pulse|Pulsation ultrasonique|libre|
|[EDn6PkdwmLETtSlD.htm](pathfinder-bestiary-3-items/EDn6PkdwmLETtSlD.htm)|Phase Lurch|Marche déphasée|libre|
|[edVyMro5viX5rgD9.htm](pathfinder-bestiary-3-items/edVyMro5viX5rgD9.htm)|Wind-Up|Remonter un dispositif|libre|
|[EdX3IAMq8CXGNxOu.htm](pathfinder-bestiary-3-items/EdX3IAMq8CXGNxOu.htm)|Arcane Prepared Spells|Sorts arcaniques préparés|libre|
|[ee90NjJ1Ze4Ghwsk.htm](pathfinder-bestiary-3-items/ee90NjJ1Ze4Ghwsk.htm)|At-Will Spells|Sorts à volonté|libre|
|[EFidHtj7QBgjYsOb.htm](pathfinder-bestiary-3-items/EFidHtj7QBgjYsOb.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[EFkfk6hbU9iqt1ja.htm](pathfinder-bestiary-3-items/EFkfk6hbU9iqt1ja.htm)|Frightful Presence|Présence terrifiante|libre|
|[EGyTn2mFKrYKKJvX.htm](pathfinder-bestiary-3-items/EGyTn2mFKrYKKJvX.htm)|Nature's Chosen|Élu de la Nature|libre|
|[EhA1NamwzAZrJMj2.htm](pathfinder-bestiary-3-items/EhA1NamwzAZrJMj2.htm)|Bite|Morsure|officielle|
|[EhcLe86zdmbslOZG.htm](pathfinder-bestiary-3-items/EhcLe86zdmbslOZG.htm)|Wave|Vague|libre|
|[ehlp9AWnS6V5OAuB.htm](pathfinder-bestiary-3-items/ehlp9AWnS6V5OAuB.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[EHsKUjzbFonBGt9N.htm](pathfinder-bestiary-3-items/EHsKUjzbFonBGt9N.htm)|Envenom|Empoisonner|libre|
|[eI8RLbyITDSsCGtu.htm](pathfinder-bestiary-3-items/eI8RLbyITDSsCGtu.htm)|Bound Spirits|Esprits liés|libre|
|[eiBZvzNac6ZHZQDi.htm](pathfinder-bestiary-3-items/eiBZvzNac6ZHZQDi.htm)|Hatchet|Hachette|libre|
|[EIopSecYGMv2HxGV.htm](pathfinder-bestiary-3-items/EIopSecYGMv2HxGV.htm)|Embed Quill|Pointes enfoncées|libre|
|[eIrU8U3KMyv0flIO.htm](pathfinder-bestiary-3-items/eIrU8U3KMyv0flIO.htm)|Deception|Duperie|libre|
|[eiuG3n39AiXSGDav.htm](pathfinder-bestiary-3-items/eiuG3n39AiXSGDav.htm)|Dimensional Pit|Puits dimensionnel|libre|
|[EJ2Xgd9VGb4Rby3n.htm](pathfinder-bestiary-3-items/EJ2Xgd9VGb4Rby3n.htm)|Isolating Lash|Fouet d'isolement|libre|
|[eJPyB6k2nbixpAxn.htm](pathfinder-bestiary-3-items/eJPyB6k2nbixpAxn.htm)|At-Will Spells|Sorts à volonté|libre|
|[EknlSoHchVPvAGPA.htm](pathfinder-bestiary-3-items/EknlSoHchVPvAGPA.htm)|Grasping Bites|Morsures agrippantes|libre|
|[eKQzWhQFHFwAwUYf.htm](pathfinder-bestiary-3-items/eKQzWhQFHFwAwUYf.htm)|Perfected Flight|Vol parfait|libre|
|[EKRq00ExnMsBTNP4.htm](pathfinder-bestiary-3-items/EKRq00ExnMsBTNP4.htm)|Hurtful Critique|Critique acerbe|libre|
|[EkUXODIcewWOk6qH.htm](pathfinder-bestiary-3-items/EkUXODIcewWOk6qH.htm)|Freeze Shell|Geler la carapace|libre|
|[EKw1H7sHPZrfrDL9.htm](pathfinder-bestiary-3-items/EKw1H7sHPZrfrDL9.htm)|Composite Longbow|+1,striking|Arc long composite de frappe +1|officielle|
|[elcu7DQCtC0dX7Q4.htm](pathfinder-bestiary-3-items/elcu7DQCtC0dX7Q4.htm)|Negative Healing|Guérison négative|libre|
|[eLKeINPUnuoZeD9D.htm](pathfinder-bestiary-3-items/eLKeINPUnuoZeD9D.htm)|Swarm Mind|Esprit de la nuée|libre|
|[eLmaLQ51OWOQK6bj.htm](pathfinder-bestiary-3-items/eLmaLQ51OWOQK6bj.htm)|Divine Decree (Evil Only)|Décret divin (Mauvais uniquement)|libre|
|[ELmguzDIW19zBvG2.htm](pathfinder-bestiary-3-items/ELmguzDIW19zBvG2.htm)|Constant Spells|Sorts constants|libre|
|[ELpuKrFNyvwzCapj.htm](pathfinder-bestiary-3-items/ELpuKrFNyvwzCapj.htm)|True Strike (At Will)|Coup au but (À volonté)|libre|
|[emdMVqKxpviFTtN7.htm](pathfinder-bestiary-3-items/emdMVqKxpviFTtN7.htm)|Regeneration 40 (Deactivated by Chaotic, Mental or Orichalcum)|Régénération 40 (désactivée par Chaos, Mental ou Orichalque)|libre|
|[eMH8aLOsO2dW2SPL.htm](pathfinder-bestiary-3-items/eMH8aLOsO2dW2SPL.htm)|Low-Light Vision|Vision nocturne|libre|
|[eMLzHgxK6gQ67wx6.htm](pathfinder-bestiary-3-items/eMLzHgxK6gQ67wx6.htm)|Household Lore|Connaissance domestique|officielle|
|[eNkQHj53Zq1tG2jD.htm](pathfinder-bestiary-3-items/eNkQHj53Zq1tG2jD.htm)|Eclipse|Éclipse|libre|
|[eno8CnD1EnlY1yIZ.htm](pathfinder-bestiary-3-items/eno8CnD1EnlY1yIZ.htm)|Zone of Truth (At Will)|Zone de vérité (À volonté)|officielle|
|[ENpP2o2H3ia9c6NJ.htm](pathfinder-bestiary-3-items/ENpP2o2H3ia9c6NJ.htm)|Surreal Anatomy|Anatomie suréelle|libre|
|[ENxpJknQm2f0aGri.htm](pathfinder-bestiary-3-items/ENxpJknQm2f0aGri.htm)|Speak with Arthropods|Communication avec les arthropodes|libre|
|[eoGlb3oqWZjUaNDi.htm](pathfinder-bestiary-3-items/eoGlb3oqWZjUaNDi.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[EoRz5vyqTCn8pIIk.htm](pathfinder-bestiary-3-items/EoRz5vyqTCn8pIIk.htm)|Low-Light Vision|Vision nocturne|libre|
|[EPLy8QbtiSRbEEme.htm](pathfinder-bestiary-3-items/EPLy8QbtiSRbEEme.htm)|Swarm Mind|Esprit de la nuée|libre|
|[eQjA75mkF3WdkNS8.htm](pathfinder-bestiary-3-items/eQjA75mkF3WdkNS8.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[erIKrXEzR8ADuFq8.htm](pathfinder-bestiary-3-items/erIKrXEzR8ADuFq8.htm)|Tail|Queue|libre|
|[ERS9rhuIGUTjxi8H.htm](pathfinder-bestiary-3-items/ERS9rhuIGUTjxi8H.htm)|At-Will Spells|Sorts à volonté|libre|
|[erx9pGmKEEP0jCHW.htm](pathfinder-bestiary-3-items/erx9pGmKEEP0jCHW.htm)|Festival Lore|Connaissance des festivals|libre|
|[ET0tToniC9uHpsbM.htm](pathfinder-bestiary-3-items/ET0tToniC9uHpsbM.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[EtHDwrRNzkfZ50vn.htm](pathfinder-bestiary-3-items/EtHDwrRNzkfZ50vn.htm)|Forsaken Patron|Protecteur Oublié|libre|
|[ETR9TOwQKjt3PBxR.htm](pathfinder-bestiary-3-items/ETR9TOwQKjt3PBxR.htm)|Sudden Blight (At Will)|Dégradation soudaine (À volonté)|libre|
|[ETZ0a2GwlpGrMxWM.htm](pathfinder-bestiary-3-items/ETZ0a2GwlpGrMxWM.htm)|Rock|Rocher|libre|
|[eU85Cgnz1ezRDCdF.htm](pathfinder-bestiary-3-items/eU85Cgnz1ezRDCdF.htm)|Hatchet|Hachette|libre|
|[EuQQ73V8hJuBn5EU.htm](pathfinder-bestiary-3-items/EuQQ73V8hJuBn5EU.htm)|Grab Item|Attraper un objet|libre|
|[EViJHho1D7U93moQ.htm](pathfinder-bestiary-3-items/EViJHho1D7U93moQ.htm)|Smoke Vision|Vision malgré la fumée|officielle|
|[eVqb0828602ykLqp.htm](pathfinder-bestiary-3-items/eVqb0828602ykLqp.htm)|Head Bowl|Cuvette de tête|libre|
|[Ew8oLYahqyGEluxo.htm](pathfinder-bestiary-3-items/Ew8oLYahqyGEluxo.htm)|Greater Constrict|Constriction supérieure|libre|
|[eWeqt47GwgkuwIQ8.htm](pathfinder-bestiary-3-items/eWeqt47GwgkuwIQ8.htm)|At-Will Spells|Sorts à volonté|libre|
|[eWV9eGMG3Ss6ZE78.htm](pathfinder-bestiary-3-items/eWV9eGMG3Ss6ZE78.htm)|+4 Status to All Saves vs. Mental or Divine|bonus de statut de +4 aux JdS contre les effets mentaux ou divins|libre|
|[EWX813DLZAePlSAY.htm](pathfinder-bestiary-3-items/EWX813DLZAePlSAY.htm)|Jaws|Mâchoires|libre|
|[EXdI3guudVr8XFFO.htm](pathfinder-bestiary-3-items/EXdI3guudVr8XFFO.htm)|Improved Knockdown|Renversement amélioré|libre|
|[eY479IPS7BQvkOTF.htm](pathfinder-bestiary-3-items/eY479IPS7BQvkOTF.htm)|Love's Impunity|Impunité de l'amour|libre|
|[EybKkcXQpisZym7b.htm](pathfinder-bestiary-3-items/EybKkcXQpisZym7b.htm)|Claim Trophy|Revendication de trophée|libre|
|[eYkYSZX390lDUFFK.htm](pathfinder-bestiary-3-items/eYkYSZX390lDUFFK.htm)|Pounce|Bond|officielle|
|[EYLtQQcjxfjr6MNe.htm](pathfinder-bestiary-3-items/EYLtQQcjxfjr6MNe.htm)|Regeneration 50 (Deactivated by Good, Mental, Orichalcum)|Régénération 50 (désactivée par Bon, Mental ou orichalque)|libre|
|[Eyuxywd5xyJvHMPE.htm](pathfinder-bestiary-3-items/Eyuxywd5xyJvHMPE.htm)|Mind Reading (At Will)|Lecture des pensées (À volonté)|officielle|
|[eZ5EIf8w0pc83e2W.htm](pathfinder-bestiary-3-items/eZ5EIf8w0pc83e2W.htm)|Shameful Loathing|Honteux dégoût|libre|
|[EZESCdF1dzv8z0rc.htm](pathfinder-bestiary-3-items/EZESCdF1dzv8z0rc.htm)|Darkvision|Vision dans le noir|libre|
|[EzfpvwnxObWcM5rn.htm](pathfinder-bestiary-3-items/EzfpvwnxObWcM5rn.htm)|Carver's Curse|Malédiction du sculpteur|libre|
|[eZiRahkdCT1SRMx1.htm](pathfinder-bestiary-3-items/eZiRahkdCT1SRMx1.htm)|Rock|Rocher|libre|
|[EzVhFWQhs6axH99B.htm](pathfinder-bestiary-3-items/EzVhFWQhs6axH99B.htm)|Spear|Lance|libre|
|[F0RaqdAbEfogkX2c.htm](pathfinder-bestiary-3-items/F0RaqdAbEfogkX2c.htm)|Fan Bolt|Carreau éventail|libre|
|[F0su6gl7ieMAuCn8.htm](pathfinder-bestiary-3-items/F0su6gl7ieMAuCn8.htm)|Raise Guard|Garde haute|libre|
|[f0YkO5SMtEfB3r39.htm](pathfinder-bestiary-3-items/f0YkO5SMtEfB3r39.htm)|Scent (Imprecise) 60 feet|Odorat 18 m (imprécis)|libre|
|[F1BUx1q2oTY8sFpj.htm](pathfinder-bestiary-3-items/F1BUx1q2oTY8sFpj.htm)|Clutching Cold|Froid saisissant|libre|
|[f27aQS5iwwcDr82I.htm](pathfinder-bestiary-3-items/f27aQS5iwwcDr82I.htm)|Full Plate (see Death Blaze)|Harnois (voir Brasier fatal)|libre|
|[f4MFrfJdBTJTN23S.htm](pathfinder-bestiary-3-items/f4MFrfJdBTJTN23S.htm)|Command Thrall|Diriger l'esclave|libre|
|[f4Mkjn4uAW6nabJR.htm](pathfinder-bestiary-3-items/f4Mkjn4uAW6nabJR.htm)|Rearrange Possessions|Réarranger les possessions|libre|
|[F5nrBH7jkTIQsJqp.htm](pathfinder-bestiary-3-items/F5nrBH7jkTIQsJqp.htm)|Spirit Body|Corps spirituel|libre|
|[f661MaVKOhxZZMUI.htm](pathfinder-bestiary-3-items/f661MaVKOhxZZMUI.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[f6S96FxE504I9tBq.htm](pathfinder-bestiary-3-items/f6S96FxE504I9tBq.htm)|Lifesense 120 feet|Perception de la vie 36 m|libre|
|[f6v0xX9B6TCjk14f.htm](pathfinder-bestiary-3-items/f6v0xX9B6TCjk14f.htm)|Arcane Prepared Spells|Sorts arcaniques préparés|libre|
|[f714vqweH1u5q1ZG.htm](pathfinder-bestiary-3-items/f714vqweH1u5q1ZG.htm)|Low-Light Vision|Vision nocturne|libre|
|[F7XOjtJspPrYwCzy.htm](pathfinder-bestiary-3-items/F7XOjtJspPrYwCzy.htm)|Darkvision|Vision dans le noir|libre|
|[f8ucclfLXdX89h8k.htm](pathfinder-bestiary-3-items/f8ucclfLXdX89h8k.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[F98RBByZ9IcrIL7x.htm](pathfinder-bestiary-3-items/F98RBByZ9IcrIL7x.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[f9gqhbFaPyj77i0e.htm](pathfinder-bestiary-3-items/f9gqhbFaPyj77i0e.htm)|Light Hammer|Marteau de guerre léger|officielle|
|[F9hcxqTd9pv3rmpZ.htm](pathfinder-bestiary-3-items/F9hcxqTd9pv3rmpZ.htm)|Skip Between|Alterner|libre|
|[F9OvUrIH2XqsqnGm.htm](pathfinder-bestiary-3-items/F9OvUrIH2XqsqnGm.htm)|Root|Racine|libre|
|[f9xJOejUBSfPwnLt.htm](pathfinder-bestiary-3-items/f9xJOejUBSfPwnLt.htm)|Draconic Momentum|Impulsion draconique|libre|
|[F9z5eqsEuY9xP7v7.htm](pathfinder-bestiary-3-items/F9z5eqsEuY9xP7v7.htm)|Frightful Presence|Présence terrifiante|libre|
|[FAGZiwfUhhTFHxxn.htm](pathfinder-bestiary-3-items/FAGZiwfUhhTFHxxn.htm)|Claw|Griffe|libre|
|[faqkx0QvzDNLMR1M.htm](pathfinder-bestiary-3-items/faqkx0QvzDNLMR1M.htm)|Constant Spells|Sorts constants|libre|
|[fAtIew8TlThwaKzS.htm](pathfinder-bestiary-3-items/fAtIew8TlThwaKzS.htm)|Dagger|Dague|libre|
|[fCe9kl0ZVqmf44Qi.htm](pathfinder-bestiary-3-items/fCe9kl0ZVqmf44Qi.htm)|Midwifery Lore|Connaissance du métier de sages-femmes|libre|
|[fcF7yPdFg6xTRRlu.htm](pathfinder-bestiary-3-items/fcF7yPdFg6xTRRlu.htm)|Hunter's Triumph|Triomphe du chasseur|libre|
|[fCrkAswBNGcLQ2S5.htm](pathfinder-bestiary-3-items/fCrkAswBNGcLQ2S5.htm)|Detect Alignment (At Will) (Evil Only)|Détection de l'alignement (à volonté, mauvais uniquement)|officielle|
|[fCSVGUKsmrepwWjP.htm](pathfinder-bestiary-3-items/fCSVGUKsmrepwWjP.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[fcx7u3nEY1i0o8JF.htm](pathfinder-bestiary-3-items/fcx7u3nEY1i0o8JF.htm)|Pungent|Odeur puissante|libre|
|[fDBEEgdrhbSzqYYM.htm](pathfinder-bestiary-3-items/fDBEEgdrhbSzqYYM.htm)|Delicious|Délicieux|libre|
|[fDOGJqfBeWIT7IOl.htm](pathfinder-bestiary-3-items/fDOGJqfBeWIT7IOl.htm)|Scent (Imprecise) 60 feet|Odorat 18 m (imprécis)|libre|
|[fE3RaY5ixs8crh6p.htm](pathfinder-bestiary-3-items/fE3RaY5ixs8crh6p.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[FeIulhzHCcKLFyNH.htm](pathfinder-bestiary-3-items/FeIulhzHCcKLFyNH.htm)|Claw|Griffe|libre|
|[FEmYP0SV4WLWDH9k.htm](pathfinder-bestiary-3-items/FEmYP0SV4WLWDH9k.htm)|Divine Spontaneous Spells|Sorts divins spontanés|libre|
|[fFOSRHY0ACjbHmk0.htm](pathfinder-bestiary-3-items/fFOSRHY0ACjbHmk0.htm)|Strix Camaraderie|Camaraderie strix|libre|
|[fg1os9FHkiVGPkuh.htm](pathfinder-bestiary-3-items/fg1os9FHkiVGPkuh.htm)|Limited Immortality|Immortalité limitée|libre|
|[FGcmZG66TcZc2r8o.htm](pathfinder-bestiary-3-items/FGcmZG66TcZc2r8o.htm)|Constrict|Constriction|libre|
|[fgDDQdQN0mgFgxVm.htm](pathfinder-bestiary-3-items/fgDDQdQN0mgFgxVm.htm)|Raccoon's Whimsy|Fantaisie du raton-laveur|libre|
|[Fgyso5whxxzTMpVE.htm](pathfinder-bestiary-3-items/Fgyso5whxxzTMpVE.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[fH4zN6EufktPqbW4.htm](pathfinder-bestiary-3-items/fH4zN6EufktPqbW4.htm)|Clockwork Wand|Baguette mécanique|libre|
|[FH98KC2pUcRXdN35.htm](pathfinder-bestiary-3-items/FH98KC2pUcRXdN35.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[FHCDHU9Avy8uYlDu.htm](pathfinder-bestiary-3-items/FHCDHU9Avy8uYlDu.htm)|Telepathy 60 feet|Télépathie 18 m|libre|
|[fhIh70SWeQm18cfv.htm](pathfinder-bestiary-3-items/fhIh70SWeQm18cfv.htm)|Low-Light Vision|Vision nocturne|libre|
|[fhK47KtCqw1hqre0.htm](pathfinder-bestiary-3-items/fhK47KtCqw1hqre0.htm)|At-Will Spells|Sorts à volonté|libre|
|[FHVxImDGF7MB4tzs.htm](pathfinder-bestiary-3-items/FHVxImDGF7MB4tzs.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[FHWDKbm2r5ltPmmK.htm](pathfinder-bestiary-3-items/FHWDKbm2r5ltPmmK.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[Fil9NBsfO3iADreb.htm](pathfinder-bestiary-3-items/Fil9NBsfO3iADreb.htm)|Sandstorm|Tempête de sable|libre|
|[fj2rLzo717u5F5Ya.htm](pathfinder-bestiary-3-items/fj2rLzo717u5F5Ya.htm)|Positive Energy Plane Lore|Connaissance du Plan de l'énergie positive|libre|
|[FJEaQqHttC6Z3MP3.htm](pathfinder-bestiary-3-items/FJEaQqHttC6Z3MP3.htm)|Sneak Attack|Attaque sournoise|libre|
|[FJGG1xzdcXWDtz4x.htm](pathfinder-bestiary-3-items/FJGG1xzdcXWDtz4x.htm)|Bard Composition Spells|Sorts de composition de barde|libre|
|[fJlI13pn1Y8ntWFP.htm](pathfinder-bestiary-3-items/fJlI13pn1Y8ntWFP.htm)|Heartless Furnace|Fournaise impitoyable|libre|
|[Fk0okw5jh318HfC0.htm](pathfinder-bestiary-3-items/Fk0okw5jh318HfC0.htm)|Boundless Reach|Allonge illimitée|libre|
|[Fk6KpPG5r84Cxf3v.htm](pathfinder-bestiary-3-items/Fk6KpPG5r84Cxf3v.htm)|Constant Spells|Sorts constants|libre|
|[fKdYArB4XGbeZO0e.htm](pathfinder-bestiary-3-items/fKdYArB4XGbeZO0e.htm)|Staff|Bâton|libre|
|[FkGm83vCSFYej5yy.htm](pathfinder-bestiary-3-items/FkGm83vCSFYej5yy.htm)|Darkvision|Vision dans le noir|libre|
|[fkJeFr0LCaDT6DYo.htm](pathfinder-bestiary-3-items/fkJeFr0LCaDT6DYo.htm)|Air Walk (Constant)|Marche dans les airs (constant)|officielle|
|[FkwYpWuwVTFS02Jn.htm](pathfinder-bestiary-3-items/FkwYpWuwVTFS02Jn.htm)|All-Around Vision|Vision panoramique|libre|
|[fLCQ4ieE5VzQA9NP.htm](pathfinder-bestiary-3-items/fLCQ4ieE5VzQA9NP.htm)|Borrowed Skin|Emprunt de peau|libre|
|[fLXcaCWjl1dAjO33.htm](pathfinder-bestiary-3-items/fLXcaCWjl1dAjO33.htm)|Bite|Morsure|officielle|
|[FLY1h37SM1s6HQ3K.htm](pathfinder-bestiary-3-items/FLY1h37SM1s6HQ3K.htm)|Web Trap|Piège de toile|libre|
|[FmabDHqtzgl9JIl4.htm](pathfinder-bestiary-3-items/FmabDHqtzgl9JIl4.htm)|Jaws|Mâchoires|libre|
|[fmxx7smV9KEkfRN8.htm](pathfinder-bestiary-3-items/fmxx7smV9KEkfRN8.htm)|Isolate Foes|Isoler les ennemis|libre|
|[fN2P498vq870hq1W.htm](pathfinder-bestiary-3-items/fN2P498vq870hq1W.htm)|All-Around Vision|Vision panoramique|libre|
|[FnIxsS3RE81lmBvi.htm](pathfinder-bestiary-3-items/FnIxsS3RE81lmBvi.htm)|Kukri|Kukri|libre|
|[fNOL0XY0wCqc5rUq.htm](pathfinder-bestiary-3-items/fNOL0XY0wCqc5rUq.htm)|+3 Status to All Saves vs. Divine Magic|bonus de statut de +3 aux JdS contre la magie divine|libre|
|[fOaemwLydAbI9Tdb.htm](pathfinder-bestiary-3-items/fOaemwLydAbI9Tdb.htm)|Grab|Empoignade/Agrippement|libre|
|[FodKNeEvBm6FqwvE.htm](pathfinder-bestiary-3-items/FodKNeEvBm6FqwvE.htm)|Impaling Charge|Charge empaleuse|libre|
|[foFqU5DE5kDifheg.htm](pathfinder-bestiary-3-items/foFqU5DE5kDifheg.htm)|Smoke Vision|Vision malgré la fumée|officielle|
|[foR7mgXEL0XMHJ6J.htm](pathfinder-bestiary-3-items/foR7mgXEL0XMHJ6J.htm)|Flurry of Pods|Déluge de pseudopodes|libre|
|[FoWz6fNCdey5pLcj.htm](pathfinder-bestiary-3-items/FoWz6fNCdey5pLcj.htm)|Dispel Magic (At Will)|Dissipation de la magie (À volonté)|officielle|
|[fp6MDEvJKxcE7hE3.htm](pathfinder-bestiary-3-items/fp6MDEvJKxcE7hE3.htm)|Low-Light Vision|Vision nocturne|libre|
|[fpKOIfEsphtEaCbR.htm](pathfinder-bestiary-3-items/fpKOIfEsphtEaCbR.htm)|Longsword|+1,striking,disrupting|Épée longue perturbatrice de frappe +1|libre|
|[FpyBx0T91Ob8bRXz.htm](pathfinder-bestiary-3-items/FpyBx0T91Ob8bRXz.htm)|Mindlink (At Will)|Lien mental (À volonté)|officielle|
|[fPZelv8NqLRyuQ1p.htm](pathfinder-bestiary-3-items/fPZelv8NqLRyuQ1p.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[fQAp6FTjVozJr8XU.htm](pathfinder-bestiary-3-items/fQAp6FTjVozJr8XU.htm)|Attack of Opportunity (Special)|Attaque d'opportunité (Spéciale)|libre|
|[fQuNqFErQHkBJLE8.htm](pathfinder-bestiary-3-items/fQuNqFErQHkBJLE8.htm)|Claw|Griffe|libre|
|[FqY5LFjb311dDEv1.htm](pathfinder-bestiary-3-items/FqY5LFjb311dDEv1.htm)|Darkvision|Vision dans le noir|libre|
|[FR0NE1tkGwNuwmOZ.htm](pathfinder-bestiary-3-items/FR0NE1tkGwNuwmOZ.htm)|Improved Grab|Empoignade/Agrippement améliorés|libre|
|[fr7zLtKzGQVPqqKL.htm](pathfinder-bestiary-3-items/fr7zLtKzGQVPqqKL.htm)|Spit|Crachat|libre|
|[fRSODlNG63LciyB1.htm](pathfinder-bestiary-3-items/fRSODlNG63LciyB1.htm)|Constant Spells|Sorts constants|libre|
|[fRYKciZWxC0TiEcw.htm](pathfinder-bestiary-3-items/fRYKciZWxC0TiEcw.htm)|Darkvision|Vision dans le noir|libre|
|[fS0jOjwX16xkNJ2N.htm](pathfinder-bestiary-3-items/fS0jOjwX16xkNJ2N.htm)|Pitchfork|Fourche|libre|
|[fSDTMDmYkmPrgbRM.htm](pathfinder-bestiary-3-items/fSDTMDmYkmPrgbRM.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[fSFM5tsKzCAUyEVt.htm](pathfinder-bestiary-3-items/fSFM5tsKzCAUyEVt.htm)|Sneak Attack|Attaque d'opportunité|libre|
|[FsndhUfvxvaK2vMV.htm](pathfinder-bestiary-3-items/FsndhUfvxvaK2vMV.htm)|Grab|Empoigner/Agripper|libre|
|[FTFBPXIVH7AJW2HL.htm](pathfinder-bestiary-3-items/FTFBPXIVH7AJW2HL.htm)|Rejuvenation|Reconstruction|libre|
|[FtnpYBZenPEqBKZT.htm](pathfinder-bestiary-3-items/FtnpYBZenPEqBKZT.htm)|Vishkanyan Venom|Venin vishkanya|libre|
|[FV9xFHPHkv8KNSub.htm](pathfinder-bestiary-3-items/FV9xFHPHkv8KNSub.htm)|Inspire Envoy|Inspiration de l'émissaire|libre|
|[fv9ZIBX07mdzmfwt.htm](pathfinder-bestiary-3-items/fv9ZIBX07mdzmfwt.htm)|Change Shape|Changement de forme|libre|
|[fvbnbBzVNf3FpASj.htm](pathfinder-bestiary-3-items/fvbnbBzVNf3FpASj.htm)|Scent (Imprecise) 60 feet|Odorat 18 m (imprécis)|libre|
|[FVNiKshCldd6k244.htm](pathfinder-bestiary-3-items/FVNiKshCldd6k244.htm)|Hand of Despair|Main de désespoir|libre|
|[fvVvVEF0pK4ATbmC.htm](pathfinder-bestiary-3-items/fvVvVEF0pK4ATbmC.htm)|Luck Osmosis|Osmose de chance|libre|
|[FWr6Ex98OyC3DSds.htm](pathfinder-bestiary-3-items/FWr6Ex98OyC3DSds.htm)|At-Will Spells|Sorts à volonté|libre|
|[FWYR69TIoTEaL4Sc.htm](pathfinder-bestiary-3-items/FWYR69TIoTEaL4Sc.htm)|Speak with Animals (Constant)|Communication avec les animaux (constant)|officielle|
|[Fx2DEwhdjtpLUXBj.htm](pathfinder-bestiary-3-items/Fx2DEwhdjtpLUXBj.htm)|Sack for Holding Rocks|Sac contenant des rochers|libre|
|[fXjJYcC6HHWIClZe.htm](pathfinder-bestiary-3-items/fXjJYcC6HHWIClZe.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|libre|
|[fXKjy8wUG1Jgkyga.htm](pathfinder-bestiary-3-items/fXKjy8wUG1Jgkyga.htm)|Inspiration|Inspiration|libre|
|[FYOsSxnsDAEzO8SZ.htm](pathfinder-bestiary-3-items/FYOsSxnsDAEzO8SZ.htm)|Claw|Griffe|libre|
|[fYpLZdqHoH2EpmhI.htm](pathfinder-bestiary-3-items/fYpLZdqHoH2EpmhI.htm)|Swift Steps|Foulées rapides|libre|
|[FYQBhz6UyHtF4h3Q.htm](pathfinder-bestiary-3-items/FYQBhz6UyHtF4h3Q.htm)|Claws|Griffes|libre|
|[FyrrgVjaMyZCtRfk.htm](pathfinder-bestiary-3-items/FyrrgVjaMyZCtRfk.htm)|Darkvision|Vision dans le noir|libre|
|[fysOeh3zCgDpX1IC.htm](pathfinder-bestiary-3-items/fysOeh3zCgDpX1IC.htm)|Antler|Bois|libre|
|[Fz5eA42qzHRa526p.htm](pathfinder-bestiary-3-items/Fz5eA42qzHRa526p.htm)|Death Gasp|Soupir de mort|libre|
|[fzjCg0wFBhRYlKEh.htm](pathfinder-bestiary-3-items/fzjCg0wFBhRYlKEh.htm)|Dragon's Salvation|Sauvetage du dragon|libre|
|[fzkFHUIw06YZhQU3.htm](pathfinder-bestiary-3-items/fzkFHUIw06YZhQU3.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[fZUt506WBOAg0Mrm.htm](pathfinder-bestiary-3-items/fZUt506WBOAg0Mrm.htm)|Spearing Tail|Queue acérée|libre|
|[fzv99kNrktzJJd55.htm](pathfinder-bestiary-3-items/fzv99kNrktzJJd55.htm)|Overpowering Healing|Surcharge curative|libre|
|[g078MCPSCGCFoMNJ.htm](pathfinder-bestiary-3-items/g078MCPSCGCFoMNJ.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[g2ws0uSei8FpMGsN.htm](pathfinder-bestiary-3-items/g2ws0uSei8FpMGsN.htm)|Negative Healing|Guérison négative|libre|
|[g4EAOBnH3VEiBpbj.htm](pathfinder-bestiary-3-items/g4EAOBnH3VEiBpbj.htm)|Divine Focus Spells|Sorts divins focalisés|libre|
|[G4lup29lJteGQEx7.htm](pathfinder-bestiary-3-items/G4lup29lJteGQEx7.htm)|Divine Prepared Spells|Sorts divins préparés|libre|
|[g4OWJGRw7uHmBmrk.htm](pathfinder-bestiary-3-items/g4OWJGRw7uHmBmrk.htm)|Axe Vulnerability|Vulnérable à la hache|libre|
|[g5jByzHTDqydOMis.htm](pathfinder-bestiary-3-items/g5jByzHTDqydOMis.htm)|Relentless|Acharné|libre|
|[G5wnFqnf2owuuqKY.htm](pathfinder-bestiary-3-items/G5wnFqnf2owuuqKY.htm)|Bonded Vessel|Récipient lié|libre|
|[g5zUhW3HyLCDaXR1.htm](pathfinder-bestiary-3-items/g5zUhW3HyLCDaXR1.htm)|Fist|Poing|libre|
|[g6t9K95If99AfQfM.htm](pathfinder-bestiary-3-items/g6t9K95If99AfQfM.htm)|Change Shape|Changement de forme|libre|
|[g7u85rySCuCvovJ1.htm](pathfinder-bestiary-3-items/g7u85rySCuCvovJ1.htm)|Tail|Queue|libre|
|[G9pbNjrUbv6UwBPm.htm](pathfinder-bestiary-3-items/G9pbNjrUbv6UwBPm.htm)|Feed on Quintessence|Se nourrir de la quintessence|libre|
|[ga5JXiw8tZ2VnkLB.htm](pathfinder-bestiary-3-items/ga5JXiw8tZ2VnkLB.htm)|Low-Light Vision|Vision nocturne|libre|
|[gaAm3rT0oo2DoWFh.htm](pathfinder-bestiary-3-items/gaAm3rT0oo2DoWFh.htm)|Nymph's Beauty|Beauté de la nymphe|libre|
|[gacO7J9Bw3Sc7Hnf.htm](pathfinder-bestiary-3-items/gacO7J9Bw3Sc7Hnf.htm)|Hop On|Monter à bord|libre|
|[gbeM9uQuXvgenm6R.htm](pathfinder-bestiary-3-items/gbeM9uQuXvgenm6R.htm)|At-Will Spells|Sorts à volonté|libre|
|[gbGFrdjx00klWwUk.htm](pathfinder-bestiary-3-items/gbGFrdjx00klWwUk.htm)|Coven|Cercle|libre|
|[GBOycFh1m1Y5Jb9h.htm](pathfinder-bestiary-3-items/GBOycFh1m1Y5Jb9h.htm)|Change Shape|Changement de forme|libre|
|[gbX1GThYkUe0eSAF.htm](pathfinder-bestiary-3-items/gbX1GThYkUe0eSAF.htm)|Darkvision|Vision dans le noir|libre|
|[GC4dnaKqE0PPSLwc.htm](pathfinder-bestiary-3-items/GC4dnaKqE0PPSLwc.htm)|Blinding Spittle|Crachat aveuglant|libre|
|[gcmEV9uoqo5w9B3u.htm](pathfinder-bestiary-3-items/gcmEV9uoqo5w9B3u.htm)|Throw Rock|Projection de rocher|libre|
|[gdbnGMdSRW0Nzev8.htm](pathfinder-bestiary-3-items/gdbnGMdSRW0Nzev8.htm)|Spit|Crachat|libre|
|[GdnX1i87yKmvFsjB.htm](pathfinder-bestiary-3-items/GdnX1i87yKmvFsjB.htm)|Fume|Fumée|libre|
|[GDsko6VNw9V3q3Fn.htm](pathfinder-bestiary-3-items/GDsko6VNw9V3q3Fn.htm)|Nature's Chosen|Élu de la nature|libre|
|[GeAdyeLnGrb8jf9c.htm](pathfinder-bestiary-3-items/GeAdyeLnGrb8jf9c.htm)|Coiling Frenzy|Frénésie torsadée|libre|
|[GeCGs1bKTNRXa8oP.htm](pathfinder-bestiary-3-items/GeCGs1bKTNRXa8oP.htm)|Comprehend Language (Self Only)|Compréhension des langues (soi uniquement)|libre|
|[gemxvf3zXMNV7oqP.htm](pathfinder-bestiary-3-items/gemxvf3zXMNV7oqP.htm)|Sack for Holding Rocks|Sac contenant des rochers|libre|
|[gEPUwy8WYR35LnP8.htm](pathfinder-bestiary-3-items/gEPUwy8WYR35LnP8.htm)|Maul|+1|Maillet +1|libre|
|[geu1krmMeuI55Tx7.htm](pathfinder-bestiary-3-items/geu1krmMeuI55Tx7.htm)|At-Will Spells|Sorts à volonté|libre|
|[GeuKn3hCssJl4FAm.htm](pathfinder-bestiary-3-items/GeuKn3hCssJl4FAm.htm)|Plane of Earth Lore|Connaissance du Plan de la terre|libre|
|[GFFvPm7EbrqTqDNh.htm](pathfinder-bestiary-3-items/GFFvPm7EbrqTqDNh.htm)|Borer|Foreur|libre|
|[GfHEx2NMpfuQjhkS.htm](pathfinder-bestiary-3-items/GfHEx2NMpfuQjhkS.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[GFv0unGRcJq78oa1.htm](pathfinder-bestiary-3-items/GFv0unGRcJq78oa1.htm)|Negative Healing|Guérison négative|libre|
|[gfVREKT4YiAXf0Sh.htm](pathfinder-bestiary-3-items/gfVREKT4YiAXf0Sh.htm)|Mind Reading (At Will)|Lecture des pensées (À volonté)|officielle|
|[gg1skd2GL6lN70Op.htm](pathfinder-bestiary-3-items/gg1skd2GL6lN70Op.htm)|Foresight (Constant) (Self Only)|Prémonition (constant) (soi uniquement)|libre|
|[gGHq0sBBHPVuZhDJ.htm](pathfinder-bestiary-3-items/gGHq0sBBHPVuZhDJ.htm)|Constrict|Constriction|libre|
|[gGpB7QLHdHm3LW5f.htm](pathfinder-bestiary-3-items/gGpB7QLHdHm3LW5f.htm)|Pufferfish Venom|Venin de diodon|libre|
|[gGq6qmIUe3LgDalo.htm](pathfinder-bestiary-3-items/gGq6qmIUe3LgDalo.htm)|Illusory Creature (At Will)|Créature illusoire (À volonté)|officielle|
|[ggqzAen8OXMFadmR.htm](pathfinder-bestiary-3-items/ggqzAen8OXMFadmR.htm)|Low-Light Vision|Vision nocturne|libre|
|[Gh301Rw6jmBr5P8s.htm](pathfinder-bestiary-3-items/Gh301Rw6jmBr5P8s.htm)|Putrid Vomit|Vomi putride|libre|
|[ghavKYYcFvQxKQ3n.htm](pathfinder-bestiary-3-items/ghavKYYcFvQxKQ3n.htm)|Font of Death|Source de mort|libre|
|[gHkt9ZPmla4aDS6c.htm](pathfinder-bestiary-3-items/gHkt9ZPmla4aDS6c.htm)|Telepathy|Télépathie|libre|
|[GhM8SkeNK34K4KuM.htm](pathfinder-bestiary-3-items/GhM8SkeNK34K4KuM.htm)|Extend Neck|Extension du cou|libre|
|[gi7A2YM0065rtCEt.htm](pathfinder-bestiary-3-items/gi7A2YM0065rtCEt.htm)|Horn|Corne|libre|
|[GIMEaPqcCrWjsAx0.htm](pathfinder-bestiary-3-items/GIMEaPqcCrWjsAx0.htm)|Suggestion (At Will)|Suggestion (À volonté)|officielle|
|[gIPLLT7gHYLZoTe5.htm](pathfinder-bestiary-3-items/gIPLLT7gHYLZoTe5.htm)|Moon Frenzy|Frénésie lunaire|libre|
|[Gj7A4LTikw9f1cDC.htm](pathfinder-bestiary-3-items/Gj7A4LTikw9f1cDC.htm)|Jaws|Mâchoires|libre|
|[gJ7hdDvMBTlevMyx.htm](pathfinder-bestiary-3-items/gJ7hdDvMBTlevMyx.htm)|Maw|Gueule|libre|
|[gjjjiArVeBThq31t.htm](pathfinder-bestiary-3-items/gjjjiArVeBThq31t.htm)|Library Lore|Connaissance des bibliothèques|officielle|
|[gk6ccJsfW8SnQ1XT.htm](pathfinder-bestiary-3-items/gk6ccJsfW8SnQ1XT.htm)|Limited Flight|Vol limité|libre|
|[gKE1dU3eNHT5VhmQ.htm](pathfinder-bestiary-3-items/gKE1dU3eNHT5VhmQ.htm)|Darkvision|Vision dans le noir|libre|
|[GkEh5ET2hVXw3FKO.htm](pathfinder-bestiary-3-items/GkEh5ET2hVXw3FKO.htm)|Darkvision|Vision dans le noir|libre|
|[gKhFLhWvvDGY1Xbf.htm](pathfinder-bestiary-3-items/gKhFLhWvvDGY1Xbf.htm)|Trident|Trident|libre|
|[GKPDyYkNMI6xU6fY.htm](pathfinder-bestiary-3-items/GKPDyYkNMI6xU6fY.htm)|Trample|Piétinement|libre|
|[gKuOYzPHRi2J8prN.htm](pathfinder-bestiary-3-items/gKuOYzPHRi2J8prN.htm)|Woodland Stride|Déplacement facilité en forêt|libre|
|[gKxdmvPJZYi5W7YT.htm](pathfinder-bestiary-3-items/gKxdmvPJZYi5W7YT.htm)|Vicious Strafe|Double attaque féroce|libre|
|[gLEu7XWn7pQSVc6w.htm](pathfinder-bestiary-3-items/gLEu7XWn7pQSVc6w.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[gLK9FYslyozpAyWH.htm](pathfinder-bestiary-3-items/gLK9FYslyozpAyWH.htm)|Draconic Frenzy|Frénésie draconique|libre|
|[gll6P1NY192pZXCX.htm](pathfinder-bestiary-3-items/gll6P1NY192pZXCX.htm)|Zealous Conviction (Self Only)|Conviction zélée (soi uniquement)|libre|
|[gMnuejIYSlD0wrxg.htm](pathfinder-bestiary-3-items/gMnuejIYSlD0wrxg.htm)|Darkvision|Vision dans le noir|libre|
|[gmOQkMoumZBsdfpn.htm](pathfinder-bestiary-3-items/gmOQkMoumZBsdfpn.htm)|Jaws|Mâchoires|libre|
|[GNmpOlkmA2KqPmhK.htm](pathfinder-bestiary-3-items/GNmpOlkmA2KqPmhK.htm)|Darkvision|Vision dans le noir|libre|
|[gNwlqfCtYRIjrH6e.htm](pathfinder-bestiary-3-items/gNwlqfCtYRIjrH6e.htm)|Fed by Metal|Renforcé par le métal|libre|
|[go13nXjPXGhj1YCQ.htm](pathfinder-bestiary-3-items/go13nXjPXGhj1YCQ.htm)|Low-Light Vision|Vision nocturne|libre|
|[GOF0Ud2jC782jqa5.htm](pathfinder-bestiary-3-items/GOF0Ud2jC782jqa5.htm)|Lore Master|Maître des connaissances|libre|
|[gOiaDjo5WK8ESJHN.htm](pathfinder-bestiary-3-items/gOiaDjo5WK8ESJHN.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[gokPql4rMjZsUmFa.htm](pathfinder-bestiary-3-items/gokPql4rMjZsUmFa.htm)|Claw|Griffe|libre|
|[GOr0VDbwyouZE0as.htm](pathfinder-bestiary-3-items/GOr0VDbwyouZE0as.htm)|Guardian's Aegis|Égide du gardien|libre|
|[GosHB6QvTUOTDIVs.htm](pathfinder-bestiary-3-items/GosHB6QvTUOTDIVs.htm)|Hurl Corpse|Projection de cadavre|libre|
|[gPaWa8Zkt9jBAg2K.htm](pathfinder-bestiary-3-items/gPaWa8Zkt9jBAg2K.htm)|Forge Jewelry|Contrefaçon de bijoux|libre|
|[GPeTASJke20Pg69M.htm](pathfinder-bestiary-3-items/GPeTASJke20Pg69M.htm)|Negative Healing|Guérison négative|libre|
|[GPFQRaqbTvsHzgLv.htm](pathfinder-bestiary-3-items/GPFQRaqbTvsHzgLv.htm)|Speak with Animals (Constant) (Spiders Only)|Communication avec les animaux (constant, araignées uniquement)|libre|
|[GpjM5EevUR9J0dQv.htm](pathfinder-bestiary-3-items/GpjM5EevUR9J0dQv.htm)|Viscous Choke|Étouffement visqueux|libre|
|[gpsZOiwpAEi2TKTL.htm](pathfinder-bestiary-3-items/gpsZOiwpAEi2TKTL.htm)|Despoiler|Affaiblissement|libre|
|[GPxeevqqNhveOB8u.htm](pathfinder-bestiary-3-items/GPxeevqqNhveOB8u.htm)|Snowstep|Marche dans la neige|libre|
|[GpYSF4Qy9tOtd9Z4.htm](pathfinder-bestiary-3-items/GpYSF4Qy9tOtd9Z4.htm)|Slice and Dice|Hacher menu|libre|
|[gQmDkG34wRgRZDty.htm](pathfinder-bestiary-3-items/gQmDkG34wRgRZDty.htm)|Taste Anger (Imprecise) 1 mile|Goût de colère (imprécis) 1,5 km|libre|
|[GqtkVnERy0SmBtKA.htm](pathfinder-bestiary-3-items/GqtkVnERy0SmBtKA.htm)|Dart|Fléchette|libre|
|[gr15jMdsKQApNTr1.htm](pathfinder-bestiary-3-items/gr15jMdsKQApNTr1.htm)|Hatchet|Hachette|libre|
|[grdS9mKHqWcDMpuH.htm](pathfinder-bestiary-3-items/grdS9mKHqWcDMpuH.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[GrP9ijoNzD5cbT4X.htm](pathfinder-bestiary-3-items/GrP9ijoNzD5cbT4X.htm)|Composite Shortbow|Arc court composite|libre|
|[GRQsdgoPIcgExLRe.htm](pathfinder-bestiary-3-items/GRQsdgoPIcgExLRe.htm)|Darkvision|Vision dans le noir|libre|
|[GS25DV8nC7hEzR8I.htm](pathfinder-bestiary-3-items/GS25DV8nC7hEzR8I.htm)|Easy to Call|Facile à invoquer|libre|
|[gsORJTMYFRzNjUAn.htm](pathfinder-bestiary-3-items/gsORJTMYFRzNjUAn.htm)|Low-Light Vision|Vision nocturne|libre|
|[Gt37cWDUiwwAkjyI.htm](pathfinder-bestiary-3-items/Gt37cWDUiwwAkjyI.htm)|Troop Movement|Mouvement de troupe|libre|
|[gTGE8w5NItEtmFuG.htm](pathfinder-bestiary-3-items/gTGE8w5NItEtmFuG.htm)|Constant Spells|Sorts constants|libre|
|[gtLlO4bF2NESErTl.htm](pathfinder-bestiary-3-items/gtLlO4bF2NESErTl.htm)|Boat Breaker|Brise navire|libre|
|[gU4MP6UZdedQtspk.htm](pathfinder-bestiary-3-items/gU4MP6UZdedQtspk.htm)|Frightful Presence|Présence terrifiante|libre|
|[Gu762rdO8uTT9ixQ.htm](pathfinder-bestiary-3-items/Gu762rdO8uTT9ixQ.htm)|Attack of Opportunity (Stinger Only)|Attaque d'opportunité (Dard uniquement)|libre|
|[GugfZAMFY5fRgjN3.htm](pathfinder-bestiary-3-items/GugfZAMFY5fRgjN3.htm)|Longbow|Arc long|libre|
|[guqcEGLb6OHHFXDW.htm](pathfinder-bestiary-3-items/guqcEGLb6OHHFXDW.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[GUTBHqMY0oGnnUx0.htm](pathfinder-bestiary-3-items/GUTBHqMY0oGnnUx0.htm)|Plagued Coffin Restoration|Cercueil épidémique restaurateur|libre|
|[GVOl6lIUVxSW5gZK.htm](pathfinder-bestiary-3-items/GVOl6lIUVxSW5gZK.htm)|Rejuvenation|Reconstruction|libre|
|[GWdF6u5sHvS26nIZ.htm](pathfinder-bestiary-3-items/GWdF6u5sHvS26nIZ.htm)|Divine Prepared Spells|Sorts divins préparés|libre|
|[gWdFg8nl9k3xaERt.htm](pathfinder-bestiary-3-items/gWdFg8nl9k3xaERt.htm)|Shove into Stone|Poussée dans la pierre|libre|
|[GWKCP70pS87RXrFP.htm](pathfinder-bestiary-3-items/GWKCP70pS87RXrFP.htm)|Aura of Disquietude|Aura d'angoisse|libre|
|[gWmjAFdZNF6c51z1.htm](pathfinder-bestiary-3-items/gWmjAFdZNF6c51z1.htm)|Plane of Fire Lore|Connaissance du plan du feu|officielle|
|[Gwusjl0kDQRYPaID.htm](pathfinder-bestiary-3-items/Gwusjl0kDQRYPaID.htm)|Astral Shock|Choc astral|libre|
|[GWx7ppzGBq5TVLbL.htm](pathfinder-bestiary-3-items/GWx7ppzGBq5TVLbL.htm)|Constant Spells|Sorts constants|libre|
|[GWXJlKCvo60X8rp8.htm](pathfinder-bestiary-3-items/GWXJlKCvo60X8rp8.htm)|Hundred-Handed Whirlwind|Cyclone aux cent mains|libre|
|[gxBpGZrMdteLCa6c.htm](pathfinder-bestiary-3-items/gxBpGZrMdteLCa6c.htm)|Hand|Main|libre|
|[Gxd48LW0au3UyXRn.htm](pathfinder-bestiary-3-items/Gxd48LW0au3UyXRn.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[gxkIdHdPIrnUNavf.htm](pathfinder-bestiary-3-items/gxkIdHdPIrnUNavf.htm)|Transcribe|Transcription|libre|
|[GXP09LUUMpZjzZNK.htm](pathfinder-bestiary-3-items/GXP09LUUMpZjzZNK.htm)|Meld into Stone (At Will)|Fusion dans la pierre (À volonté)|officielle|
|[gXT2WwNt19Jqn8Lo.htm](pathfinder-bestiary-3-items/gXT2WwNt19Jqn8Lo.htm)|Proficient Poisoner|Empoisonneur qualifié|libre|
|[GyUxfyjtgA7EdiPY.htm](pathfinder-bestiary-3-items/GyUxfyjtgA7EdiPY.htm)|Darkvision|Vision dans le noir|libre|
|[gZ11aiTmjuFfS0D9.htm](pathfinder-bestiary-3-items/gZ11aiTmjuFfS0D9.htm)|Swarm Mind|Esprit de nuée|libre|
|[gzbNuojJ54XXTTe3.htm](pathfinder-bestiary-3-items/gzbNuojJ54XXTTe3.htm)|Arcane Innate Spells|Sorts arcaniques innés|libre|
|[gzGzxCPvcdZu8f4O.htm](pathfinder-bestiary-3-items/gzGzxCPvcdZu8f4O.htm)|Constrict|Constriction|libre|
|[gZVSvNNNodOSP8Js.htm](pathfinder-bestiary-3-items/gZVSvNNNodOSP8Js.htm)|At-Will Spells|Sorts à volonté|libre|
|[gzZaUElRwVXoDLly.htm](pathfinder-bestiary-3-items/gzZaUElRwVXoDLly.htm)|Sagebane|Fléau du sage|libre|
|[H00CJvnkXb56atN1.htm](pathfinder-bestiary-3-items/H00CJvnkXb56atN1.htm)|Jaws|Mâchoires|libre|
|[h1ZAFdNFnrnrUzal.htm](pathfinder-bestiary-3-items/h1ZAFdNFnrnrUzal.htm)|Wavesense (Imprecise) 120 feet|Perception des ondes 36 m (imprécis)|libre|
|[H2G6L0SJ45uHxDiE.htm](pathfinder-bestiary-3-items/H2G6L0SJ45uHxDiE.htm)|Jaws|Mâchoires|libre|
|[h2l4ytlaIH7Elzxp.htm](pathfinder-bestiary-3-items/h2l4ytlaIH7Elzxp.htm)|Fist|Poing|libre|
|[h3GtcNC8KADgxKzm.htm](pathfinder-bestiary-3-items/h3GtcNC8KADgxKzm.htm)|Tearing Clutch|Attaque déchirante|libre|
|[H3pa17KySBkdeqib.htm](pathfinder-bestiary-3-items/H3pa17KySBkdeqib.htm)|Moonlight's Kiss|Baiser du clair de lune|libre|
|[h3rEsPLD8eRRgdqm.htm](pathfinder-bestiary-3-items/h3rEsPLD8eRRgdqm.htm)|Freedom of Movement (Constant)|Liberté de mouvement (constant)|officielle|
|[H4TL62mthkiNmZPt.htm](pathfinder-bestiary-3-items/H4TL62mthkiNmZPt.htm)|Jaws|Mâchoires|libre|
|[h4X51BYbgu9tzFnJ.htm](pathfinder-bestiary-3-items/h4X51BYbgu9tzFnJ.htm)|Tremorsense (Imprecise) 60 feet|Perception des vibrations 18 m (imprécis)|libre|
|[H6bcCPSTnAt5zBu8.htm](pathfinder-bestiary-3-items/H6bcCPSTnAt5zBu8.htm)|Felling Assault|Assaut renversant|libre|
|[H6E4xmp7lHmRZXO2.htm](pathfinder-bestiary-3-items/H6E4xmp7lHmRZXO2.htm)|Hadalic Presence|Présence hadale|libre|
|[h6z492dnOVTr6NoI.htm](pathfinder-bestiary-3-items/h6z492dnOVTr6NoI.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[h7VjKWk9XbmBEL2M.htm](pathfinder-bestiary-3-items/h7VjKWk9XbmBEL2M.htm)|Low-Light Vision|Vision nocturne|libre|
|[h8caQJMnKj4F22zZ.htm](pathfinder-bestiary-3-items/h8caQJMnKj4F22zZ.htm)|Frenzied Slashes|Lacérations frénétiques|libre|
|[h8iiN0oJfHoKkkw6.htm](pathfinder-bestiary-3-items/h8iiN0oJfHoKkkw6.htm)|Darkvision|Vision dans le noir|libre|
|[h8Phu8ErJOaWCuUU.htm](pathfinder-bestiary-3-items/h8Phu8ErJOaWCuUU.htm)|Pincer|Pince|libre|
|[h8rEFtxwQFlx0MIz.htm](pathfinder-bestiary-3-items/h8rEFtxwQFlx0MIz.htm)|Venomous Fangs|Crocs empoisonnés|libre|
|[h8UYIVYJvZxviPCt.htm](pathfinder-bestiary-3-items/h8UYIVYJvZxviPCt.htm)|Web|Toile|libre|
|[H8WZz3DKIs1SCcFr.htm](pathfinder-bestiary-3-items/H8WZz3DKIs1SCcFr.htm)|Darkvision|Vision dans le noir|libre|
|[H9YGbRcmWQV6mrYy.htm](pathfinder-bestiary-3-items/H9YGbRcmWQV6mrYy.htm)|Grab|Empoignade/Agrippement|libre|
|[HAbopgX5BTCYokYc.htm](pathfinder-bestiary-3-items/HAbopgX5BTCYokYc.htm)|Regenerative Blood|Sang régénérateur|libre|
|[hagL5rqsb5F2Dib3.htm](pathfinder-bestiary-3-items/hagL5rqsb5F2Dib3.htm)|Detect Alignment (At Will) (Good or Evil Only)|Détection de l'alignement (à volonté, bon ou mauvais uniquement)|libre|
|[HayFvnvEzF4yeL8e.htm](pathfinder-bestiary-3-items/HayFvnvEzF4yeL8e.htm)|Cleanly Vulnerability|Vulnérabilité à la propreté|libre|
|[hbJLdmKm20p84vtq.htm](pathfinder-bestiary-3-items/hbJLdmKm20p84vtq.htm)|Hurled Debris|Projection de débris|libre|
|[HbNu5OQ0rlq7Yc5T.htm](pathfinder-bestiary-3-items/HbNu5OQ0rlq7Yc5T.htm)|Surface Skimmer|Écumeur de surface|libre|
|[HBWRwsQLwNl5P9GZ.htm](pathfinder-bestiary-3-items/HBWRwsQLwNl5P9GZ.htm)|Painful Bite|Morsure éprouvante|libre|
|[hCBIDqLoaomtVumJ.htm](pathfinder-bestiary-3-items/hCBIDqLoaomtVumJ.htm)|Tremorsense (Imprecise) within their entire bound yard|Perception des vibrations (imprécis) (A l'intérieur de l'espace lié)|libre|
|[hcf7RGBrhHKjGcG4.htm](pathfinder-bestiary-3-items/hcf7RGBrhHKjGcG4.htm)|Camel Spit|Crachat de chameau|libre|
|[hCqIQRbX94na2LJI.htm](pathfinder-bestiary-3-items/hCqIQRbX94na2LJI.htm)|Spit|Crachat|libre|
|[hcWQWEF9ku17m4ue.htm](pathfinder-bestiary-3-items/hcWQWEF9ku17m4ue.htm)|Claw|Griffe|libre|
|[HD1JVNGcHKCVtQRP.htm](pathfinder-bestiary-3-items/HD1JVNGcHKCVtQRP.htm)|Fed by Earth|Renforcé par la terre|libre|
|[hD5zZmaH7szmKu7h.htm](pathfinder-bestiary-3-items/hD5zZmaH7szmKu7h.htm)|Claw|Griffe|libre|
|[hD8oXqV8Wz5DBv2c.htm](pathfinder-bestiary-3-items/hD8oXqV8Wz5DBv2c.htm)|Claw|Griffe|libre|
|[HDgHNFoKqVXHIcGb.htm](pathfinder-bestiary-3-items/HDgHNFoKqVXHIcGb.htm)|Darkvision|Vision dans le noir|libre|
|[HdHa8fMuZx5zztbP.htm](pathfinder-bestiary-3-items/HdHa8fMuZx5zztbP.htm)|Voidglass Kukri|Kudri en verre du vide|libre|
|[hDrYmsPyJApRYUgu.htm](pathfinder-bestiary-3-items/hDrYmsPyJApRYUgu.htm)|Wrap in Coils|Enrouler les anneaux|libre|
|[He0zYHgzyYLryoo4.htm](pathfinder-bestiary-3-items/He0zYHgzyYLryoo4.htm)|Claw|Griffe|libre|
|[He1e2JVLL4LLFCXX.htm](pathfinder-bestiary-3-items/He1e2JVLL4LLFCXX.htm)|Protective Pinch|Pincement de protection|libre|
|[HeSYt5jeXQwluP8x.htm](pathfinder-bestiary-3-items/HeSYt5jeXQwluP8x.htm)|Fangs|Crocs|libre|
|[hfaAY55AoAQW59YR.htm](pathfinder-bestiary-3-items/hfaAY55AoAQW59YR.htm)|Inflict Misery|Infliction de souffrance|libre|
|[hfbHiFnMOX2vWG3b.htm](pathfinder-bestiary-3-items/hfbHiFnMOX2vWG3b.htm)|Engulf|Engloutir|libre|
|[hfMabMzyolY9p2Gw.htm](pathfinder-bestiary-3-items/hfMabMzyolY9p2Gw.htm)|Euphoric Spark|Étincelle d'euphorie|libre|
|[hFX9SvNVh26s4No7.htm](pathfinder-bestiary-3-items/hFX9SvNVh26s4No7.htm)|Fade Away|S'évanouir dans la nature|libre|
|[hGcK7acpYirlNAXQ.htm](pathfinder-bestiary-3-items/hGcK7acpYirlNAXQ.htm)|Jewelry Lore|Connaissance de la joaillerie|libre|
|[HHcUzm4oVPHPOsqI.htm](pathfinder-bestiary-3-items/HHcUzm4oVPHPOsqI.htm)|Suggestion (At Will)|Suggestion (À volonté)|officielle|
|[HHDECazmKhMUWZZF.htm](pathfinder-bestiary-3-items/HHDECazmKhMUWZZF.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[hHLSrHMm2augW6G7.htm](pathfinder-bestiary-3-items/hHLSrHMm2augW6G7.htm)|At-Will Spells|Sorts à volonté|libre|
|[HHS4CPEzSJeRK2TJ.htm](pathfinder-bestiary-3-items/HHS4CPEzSJeRK2TJ.htm)|Drain Life|Drain de vie|libre|
|[HHzuVZDMyhnuUwVS.htm](pathfinder-bestiary-3-items/HHzuVZDMyhnuUwVS.htm)|Speak with Animals (Constant)|Communication avec les animaux (constant)|officielle|
|[HiELCWTyhJvQaGUl.htm](pathfinder-bestiary-3-items/HiELCWTyhJvQaGUl.htm)|Low-Light Vision|Vision nocturne|libre|
|[HiTMvvga2hoAWC9Y.htm](pathfinder-bestiary-3-items/HiTMvvga2hoAWC9Y.htm)|Snatch Between|Alterner à deux|libre|
|[HItnjZpl8JZa8gGc.htm](pathfinder-bestiary-3-items/HItnjZpl8JZa8gGc.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[Hj7vX0JdYOvuiOk2.htm](pathfinder-bestiary-3-items/Hj7vX0JdYOvuiOk2.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[hJMhsrU4b1Xsyslz.htm](pathfinder-bestiary-3-items/hJMhsrU4b1Xsyslz.htm)|Low-Light Vision|Vision nocturne|libre|
|[hjpDAszGRHhxvxyq.htm](pathfinder-bestiary-3-items/hjpDAszGRHhxvxyq.htm)|Legal Lore|Connaissance juridique|officielle|
|[HJT5NvgiIrqOYp97.htm](pathfinder-bestiary-3-items/HJT5NvgiIrqOYp97.htm)|Scent (Imprecise) 60 feet|Odorat 18 m (imprécis)|libre|
|[hKMqAgh48onTAhpH.htm](pathfinder-bestiary-3-items/hKMqAgh48onTAhpH.htm)|Clinch Victory|Neutralisation de victoire|libre|
|[Hl6TVTFqmE0SOKoE.htm](pathfinder-bestiary-3-items/Hl6TVTFqmE0SOKoE.htm)|Storm of Battle|Tempête de bataille|libre|
|[hljpYgDUm3hDZfDm.htm](pathfinder-bestiary-3-items/hljpYgDUm3hDZfDm.htm)|Low-Light Vision|Vision nocturne|libre|
|[hlLjyEJFBxuPfIsH.htm](pathfinder-bestiary-3-items/hlLjyEJFBxuPfIsH.htm)|Spectral Claw|Griffe spectrale|libre|
|[HMAJFIpbicpFe1je.htm](pathfinder-bestiary-3-items/HMAJFIpbicpFe1je.htm)|Charm (Undead Only)|Charme (mort-vivant uniquement)|libre|
|[hNp02EzjCutL6cMN.htm](pathfinder-bestiary-3-items/hNp02EzjCutL6cMN.htm)|Jaws|Mâchoires|libre|
|[hoeCSNdGULGrdSYX.htm](pathfinder-bestiary-3-items/hoeCSNdGULGrdSYX.htm)|Shield Block|Blocage au bouclier|libre|
|[HOEKikyuy4nwWSmn.htm](pathfinder-bestiary-3-items/HOEKikyuy4nwWSmn.htm)|Inkstain|Tâche d'encre|libre|
|[hozqm7eiBZchD7T3.htm](pathfinder-bestiary-3-items/hozqm7eiBZchD7T3.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[hP7NqLHuZTlhjPtO.htm](pathfinder-bestiary-3-items/hP7NqLHuZTlhjPtO.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[hpbnFFNrcbDIDwkP.htm](pathfinder-bestiary-3-items/hpbnFFNrcbDIDwkP.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[hPBPkTGmnJVgZ22X.htm](pathfinder-bestiary-3-items/hPBPkTGmnJVgZ22X.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[HPHMTMivSSxm6A9g.htm](pathfinder-bestiary-3-items/HPHMTMivSSxm6A9g.htm)|Firebolt|Boule de feu|libre|
|[HpKFDo446U6j9P11.htm](pathfinder-bestiary-3-items/HpKFDo446U6j9P11.htm)|Composite Longbow|+1|Arc long composite +1|officielle|
|[hR5BG9TXTZY8EKQh.htm](pathfinder-bestiary-3-items/hR5BG9TXTZY8EKQh.htm)|Glyph of Warding (At Will)|Glyphe de garde (À volonté)|libre|
|[Hr6tT6rCEgL0bZVy.htm](pathfinder-bestiary-3-items/Hr6tT6rCEgL0bZVy.htm)|Speak with Animals (At Will)|Communication avec les animaux (À volonté)|libre|
|[hR97QpYRCioTid90.htm](pathfinder-bestiary-3-items/hR97QpYRCioTid90.htm)|Lore (Any one Celestial Plane)|Connaissance (N'importe quel Plan céleste)|libre|
|[HRwyZKqvUxP3jUSQ.htm](pathfinder-bestiary-3-items/HRwyZKqvUxP3jUSQ.htm)|Plane Shift (Self Only) (Astral or Material Plane Only)|Changement de plan (soi uniquement, plan Astral ou Matériel uniquement)|libre|
|[hT2TFquy1W3hmWXW.htm](pathfinder-bestiary-3-items/hT2TFquy1W3hmWXW.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[HTapFz8e3dL6LNJc.htm](pathfinder-bestiary-3-items/HTapFz8e3dL6LNJc.htm)|Smoke Vision|Vision malgré la fumée|libre|
|[hteKQpvnrGLuVGSS.htm](pathfinder-bestiary-3-items/hteKQpvnrGLuVGSS.htm)|Low-Light Vision|Vision nocturne|libre|
|[HTL4UczGmmOt7HUk.htm](pathfinder-bestiary-3-items/HTL4UczGmmOt7HUk.htm)|Persuasive Rebuttal|Réfutation persuasive|libre|
|[HTp6N4B1ohShQdxQ.htm](pathfinder-bestiary-3-items/HTp6N4B1ohShQdxQ.htm)|Spray Musk|Projection de musc|libre|
|[Hu6MV5KT0NEuooVU.htm](pathfinder-bestiary-3-items/Hu6MV5KT0NEuooVU.htm)|Acrobatics|Acrobaties|libre|
|[HulNPVFqPe3KCKvw.htm](pathfinder-bestiary-3-items/HulNPVFqPe3KCKvw.htm)|+1 Resilient Breastplate|Cuirasse de résilience +1|officielle|
|[hUmX5vToV3dXSRRK.htm](pathfinder-bestiary-3-items/hUmX5vToV3dXSRRK.htm)|Change Shape|Changement de forme|libre|
|[Hv7Blt8YRg7FuNu6.htm](pathfinder-bestiary-3-items/Hv7Blt8YRg7FuNu6.htm)|Deflect Aggression|Détournement de l'agression|libre|
|[HvbA2JrPJX04mXf4.htm](pathfinder-bestiary-3-items/HvbA2JrPJX04mXf4.htm)|Smoke Vision|Vision malgré la fumée|officielle|
|[HVELxWKZ2of1Q0F1.htm](pathfinder-bestiary-3-items/HVELxWKZ2of1Q0F1.htm)|Suspend Soul|Suspension de l'âme|libre|
|[HvrOYSndJdFfyuSN.htm](pathfinder-bestiary-3-items/HvrOYSndJdFfyuSN.htm)|Towering Stance|Grande prestance|libre|
|[hVTC4rrt8IWJq51F.htm](pathfinder-bestiary-3-items/hVTC4rrt8IWJq51F.htm)|Green Tongue|Langue végétale|libre|
|[HvYM5Z1ubUt120XB.htm](pathfinder-bestiary-3-items/HvYM5Z1ubUt120XB.htm)|Claw|Griffe|libre|
|[HwqKHt0yeIDL3Jjm.htm](pathfinder-bestiary-3-items/HwqKHt0yeIDL3Jjm.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[hww0iSmzvgv1CuVE.htm](pathfinder-bestiary-3-items/hww0iSmzvgv1CuVE.htm)|Sunset Ray|Rayon du crépuscule|libre|
|[HX5VU8Xvb5yPZ2aH.htm](pathfinder-bestiary-3-items/HX5VU8Xvb5yPZ2aH.htm)|Discorporate|Désincarnation|libre|
|[HX6LFkk4karuzyse.htm](pathfinder-bestiary-3-items/HX6LFkk4karuzyse.htm)|Constrict|Constriction|libre|
|[hXhEkQgvn1PZadRa.htm](pathfinder-bestiary-3-items/hXhEkQgvn1PZadRa.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[hyj6N5o5oSyNgbJd.htm](pathfinder-bestiary-3-items/hyj6N5o5oSyNgbJd.htm)|Vine|Vrille|libre|
|[hykuv0gwwDzF4GZW.htm](pathfinder-bestiary-3-items/hykuv0gwwDzF4GZW.htm)|Bonded Strike|Frappe liée|libre|
|[HYkzxImoz15eP5c0.htm](pathfinder-bestiary-3-items/HYkzxImoz15eP5c0.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[HYlZSPGidgo056wQ.htm](pathfinder-bestiary-3-items/HYlZSPGidgo056wQ.htm)|Constant Spells|Sorts constants|libre|
|[hYXNxWZ2lgrY2oNX.htm](pathfinder-bestiary-3-items/hYXNxWZ2lgrY2oNX.htm)|Primal Prepared Spells|Sorts primordiaux préparés|libre|
|[hzAD0q71nN1lkiVO.htm](pathfinder-bestiary-3-items/hzAD0q71nN1lkiVO.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[hZBLQC0oOFlrs0Tt.htm](pathfinder-bestiary-3-items/hZBLQC0oOFlrs0Tt.htm)|Fear (At Will)|Terreur (À volonté)|officielle|
|[HZoHUgBTn23rMgPJ.htm](pathfinder-bestiary-3-items/HZoHUgBTn23rMgPJ.htm)|Meteorology Lore|Connaissance de la météorologie|libre|
|[hZqDsZrM19xB27pn.htm](pathfinder-bestiary-3-items/hZqDsZrM19xB27pn.htm)|Blessed Strikes|Frappes bénies|libre|
|[i0CgRjDNx28m8d7r.htm](pathfinder-bestiary-3-items/i0CgRjDNx28m8d7r.htm)|Hundred-Dimension Grasp|Emprise sur les dimensions|libre|
|[i0GVDRjE4Mf3fPfW.htm](pathfinder-bestiary-3-items/i0GVDRjE4Mf3fPfW.htm)|Spear|Lance|libre|
|[i1KyfP7F6bj0FZS5.htm](pathfinder-bestiary-3-items/i1KyfP7F6bj0FZS5.htm)|Constrict|Constriction|libre|
|[I1xWawDf3yw69pnw.htm](pathfinder-bestiary-3-items/I1xWawDf3yw69pnw.htm)|Desert Stride|Déplacement facilité dans le désert|libre|
|[I239EJXsU4ME0R50.htm](pathfinder-bestiary-3-items/I239EJXsU4ME0R50.htm)|Shortsword|Épée courte|libre|
|[i2o1AoEa6G6aYbNZ.htm](pathfinder-bestiary-3-items/i2o1AoEa6G6aYbNZ.htm)|Low-Light Vision|Vision nocturne|libre|
|[i2xCkOeImkc5M5zz.htm](pathfinder-bestiary-3-items/i2xCkOeImkc5M5zz.htm)|Darkvision|Vision dans le noir|libre|
|[i2YbHX9ZBEKMZMOv.htm](pathfinder-bestiary-3-items/i2YbHX9ZBEKMZMOv.htm)|Darkvision|Vision dans le noir|libre|
|[I3LHE7CKMGHBy5aq.htm](pathfinder-bestiary-3-items/I3LHE7CKMGHBy5aq.htm)|Call to Arms|Appel aux armes|libre|
|[i49FXrRnhIZz63Ee.htm](pathfinder-bestiary-3-items/i49FXrRnhIZz63Ee.htm)|Recognize Hero|Identification des héros|libre|
|[i4jbUrRlPXHHy15s.htm](pathfinder-bestiary-3-items/i4jbUrRlPXHHy15s.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|libre|
|[I5elsFKNOWOCVOwx.htm](pathfinder-bestiary-3-items/I5elsFKNOWOCVOwx.htm)|Falchion|+2,striking|Cimeterre à deux mains de frappe +2|libre|
|[I5FrBDYXau3Iuild.htm](pathfinder-bestiary-3-items/I5FrBDYXau3Iuild.htm)|Telepathy 120 feet|Télépathie 36 m|libre|
|[I6ATQo2zBIuQQinX.htm](pathfinder-bestiary-3-items/I6ATQo2zBIuQQinX.htm)|Draconic Momentum|Impulsion draconique|libre|
|[I6HjzvdkLUHJLowS.htm](pathfinder-bestiary-3-items/I6HjzvdkLUHJLowS.htm)|Detect Alignment (At Will)|Détection de l'alignement (À volonté)|libre|
|[I7lMAhjCqMyxPu6y.htm](pathfinder-bestiary-3-items/I7lMAhjCqMyxPu6y.htm)|Inexorable|Inexorable|officielle|
|[i7YE4zH1GBMET2Uu.htm](pathfinder-bestiary-3-items/i7YE4zH1GBMET2Uu.htm)|Change Shape|Changement de forme|libre|
|[i8I92PVcpFQf4HBz.htm](pathfinder-bestiary-3-items/i8I92PVcpFQf4HBz.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[I9tVwoP0Z0dV0r5j.htm](pathfinder-bestiary-3-items/I9tVwoP0Z0dV0r5j.htm)|Amulet Relic|Amulette relique|libre|
|[ia7nfgIs3ERJytZy.htm](pathfinder-bestiary-3-items/ia7nfgIs3ERJytZy.htm)|Persistence of Memory|Persistance de souvenirs|libre|
|[IaFXrGv80HlNVl19.htm](pathfinder-bestiary-3-items/IaFXrGv80HlNVl19.htm)|Telepathy 150 feet|Télépathie 45 m|libre|
|[IAO39ERjCHDfcZlr.htm](pathfinder-bestiary-3-items/IAO39ERjCHDfcZlr.htm)|Hoof|Sabot|libre|
|[IbrrhH58fU066gLI.htm](pathfinder-bestiary-3-items/IbrrhH58fU066gLI.htm)|Nirvana Lore|Connaissance du Nirvana|libre|
|[IBxaJ1PsBPxvnN70.htm](pathfinder-bestiary-3-items/IBxaJ1PsBPxvnN70.htm)|Droning Distraction|Bourdonnement gênant|libre|
|[iC8kTbasCU08DFOc.htm](pathfinder-bestiary-3-items/iC8kTbasCU08DFOc.htm)|Loathing Garotte|Garrot de haine|libre|
|[ICHmpsSeRPd2QliC.htm](pathfinder-bestiary-3-items/ICHmpsSeRPd2QliC.htm)|Coiling Frenzy|Frénésie torsadée|libre|
|[iCM4V1WXNcTslhi7.htm](pathfinder-bestiary-3-items/iCM4V1WXNcTslhi7.htm)|Coven|Cercle|libre|
|[ICqXmQ1I3WhSNBF7.htm](pathfinder-bestiary-3-items/ICqXmQ1I3WhSNBF7.htm)|Green Rituals|Rituel végétal|libre|
|[icR13DUCw64iVgQ2.htm](pathfinder-bestiary-3-items/icR13DUCw64iVgQ2.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[icUrUnsfgvWuac89.htm](pathfinder-bestiary-3-items/icUrUnsfgvWuac89.htm)|Tail|Queue|libre|
|[IDk5Kln9p90yl0SF.htm](pathfinder-bestiary-3-items/IDk5Kln9p90yl0SF.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[Idr71bCK1gM6nuVc.htm](pathfinder-bestiary-3-items/Idr71bCK1gM6nuVc.htm)|Blight (Doesn't Require Secondary Casters)|Flétrissement végétal (ne requiert pas d'incantateurs secondaires)|libre|
|[Idu5EyPugITPjmRq.htm](pathfinder-bestiary-3-items/Idu5EyPugITPjmRq.htm)|Paralysis|Paralysie|libre|
|[IE75LW4gCgc5TOPJ.htm](pathfinder-bestiary-3-items/IE75LW4gCgc5TOPJ.htm)|Designate Master|Maître désigné|libre|
|[IegAA8jyEDik5QGH.htm](pathfinder-bestiary-3-items/IegAA8jyEDik5QGH.htm)|Darkness (At Will)|Ténèbres (À volonté)|officielle|
|[IELKs5btgBCSQgGs.htm](pathfinder-bestiary-3-items/IELKs5btgBCSQgGs.htm)|Misdirection (At Will) (Self Only)|Détection faussée (À volonté) (soi uniquement)|libre|
|[if2CB9IchjxMbiab.htm](pathfinder-bestiary-3-items/if2CB9IchjxMbiab.htm)|Coven Spells|Sorts de cercle|libre|
|[IFBwgiXBfRM7tF8Q.htm](pathfinder-bestiary-3-items/IFBwgiXBfRM7tF8Q.htm)|Eel Dart|Anguille fléchette|libre|
|[IfgRYpjy1kMxYZ7N.htm](pathfinder-bestiary-3-items/IfgRYpjy1kMxYZ7N.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[iFhR8LsLMNJ2v6O0.htm](pathfinder-bestiary-3-items/iFhR8LsLMNJ2v6O0.htm)|Absorb Magic|Absorbtion la magie|libre|
|[iFKv7PFNKtSVy9XA.htm](pathfinder-bestiary-3-items/iFKv7PFNKtSVy9XA.htm)|Slow|Lent|libre|
|[iFLHtzCCLkMeOw2v.htm](pathfinder-bestiary-3-items/iFLHtzCCLkMeOw2v.htm)|Longsword|+1,striking|Épée longue de frappe +1|libre|
|[IfQ2Ql0FDoKs4CXh.htm](pathfinder-bestiary-3-items/IfQ2Ql0FDoKs4CXh.htm)|Fist|Poing|libre|
|[ifv7iRcnxapPCh5S.htm](pathfinder-bestiary-3-items/ifv7iRcnxapPCh5S.htm)|Grab|Empoignade/Agrippement|libre|
|[Ig0zFPa6ddSOqkXY.htm](pathfinder-bestiary-3-items/Ig0zFPa6ddSOqkXY.htm)|Javelin|Javelot|libre|
|[iGBJDGQHVXz9hpgE.htm](pathfinder-bestiary-3-items/iGBJDGQHVXz9hpgE.htm)|Constant Spells|Sorts constants|libre|
|[igF8SYccIFVckGta.htm](pathfinder-bestiary-3-items/igF8SYccIFVckGta.htm)|Master of the Granary|Maître du grenier|libre|
|[iGgnvBVMpX59E7rh.htm](pathfinder-bestiary-3-items/iGgnvBVMpX59E7rh.htm)|Invisibility (Self Only)|Invisibilité (soi uniquement)|officielle|
|[IgNXM2vcFy0D1uCF.htm](pathfinder-bestiary-3-items/IgNXM2vcFy0D1uCF.htm)|Cudgel|Bâton matraque|libre|
|[Igt62T1pB0clDkfb.htm](pathfinder-bestiary-3-items/Igt62T1pB0clDkfb.htm)|All-Around Vision|Vision panoramique|libre|
|[iGx7eQKe0LJXzUAo.htm](pathfinder-bestiary-3-items/iGx7eQKe0LJXzUAo.htm)|Breath Weapon|Arme de souffle|libre|
|[igxHjmUJWlr6ULG7.htm](pathfinder-bestiary-3-items/igxHjmUJWlr6ULG7.htm)|Head Spin|Pivotement de la tête|libre|
|[IH1oaRD65ApAyt5k.htm](pathfinder-bestiary-3-items/IH1oaRD65ApAyt5k.htm)|Longsword|Épée longue|libre|
|[iHJPeHHphrdXidbV.htm](pathfinder-bestiary-3-items/iHJPeHHphrdXidbV.htm)|Enraged Home (Slashing)|Maison enragée (Tranchant)|libre|
|[iHPmzxNLB3Yqy1nG.htm](pathfinder-bestiary-3-items/iHPmzxNLB3Yqy1nG.htm)|Frightful Presence|Présence terrifiante|libre|
|[IhW67AKfQIjSFjgx.htm](pathfinder-bestiary-3-items/IhW67AKfQIjSFjgx.htm)|Elegy of the Faithless|Élégie de l'abandonné|libre|
|[iIIyNY3zJBmroqzM.htm](pathfinder-bestiary-3-items/iIIyNY3zJBmroqzM.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[iIkQe60YaPrjs5Wa.htm](pathfinder-bestiary-3-items/iIkQe60YaPrjs5Wa.htm)|Claw|Griffe|libre|
|[iinBWMkulBkpeUxH.htm](pathfinder-bestiary-3-items/iinBWMkulBkpeUxH.htm)|Dispel Magic (At Will)|Dissipation de la magie (À volonté)|officielle|
|[iJaSMrCi07AzcK4L.htm](pathfinder-bestiary-3-items/iJaSMrCi07AzcK4L.htm)|Share Defenses|Défenses partagées|libre|
|[Ijep1LR2jAGG5Xqz.htm](pathfinder-bestiary-3-items/Ijep1LR2jAGG5Xqz.htm)|Negative Healing|Guérison négative|libre|
|[IjHEwnv7xjIRPpYZ.htm](pathfinder-bestiary-3-items/IjHEwnv7xjIRPpYZ.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[ijtDf4mbj5lb5bvM.htm](pathfinder-bestiary-3-items/ijtDf4mbj5lb5bvM.htm)|Sneak Attack|Attaque sournoise|libre|
|[iJuVRVeuCbiPmn0M.htm](pathfinder-bestiary-3-items/iJuVRVeuCbiPmn0M.htm)|Grab|Empoignade/Agrippement|libre|
|[iK81tvrAddbR20xS.htm](pathfinder-bestiary-3-items/iK81tvrAddbR20xS.htm)|Pincer|Pince|libre|
|[IKBRGKTG4o0biJz9.htm](pathfinder-bestiary-3-items/IKBRGKTG4o0biJz9.htm)|Low-Light Vision|Vision nocturne|libre|
|[IKkbf0MnPy4EbhpO.htm](pathfinder-bestiary-3-items/IKkbf0MnPy4EbhpO.htm)|Read Omens (At Will)|Lire les présages (À volonté)|libre|
|[iKR8M3h9wCtVtDTe.htm](pathfinder-bestiary-3-items/iKR8M3h9wCtVtDTe.htm)|Swing from the Saddle|Attaque montée|libre|
|[ikvqvxMtrtqOwbWu.htm](pathfinder-bestiary-3-items/ikvqvxMtrtqOwbWu.htm)|Seize Prayer|Détournement de prière|libre|
|[IKYih1OQrUQy2d83.htm](pathfinder-bestiary-3-items/IKYih1OQrUQy2d83.htm)|Fear (At Will)|Terreur (À volonté)|officielle|
|[Il0NCYZxE3i4U1ud.htm](pathfinder-bestiary-3-items/Il0NCYZxE3i4U1ud.htm)|Echoblade|Écholame|libre|
|[iLC1r5LnzDRvTshg.htm](pathfinder-bestiary-3-items/iLC1r5LnzDRvTshg.htm)|Banished from the Ground|Banni du sol|libre|
|[ILDPyfeR9ijXko3B.htm](pathfinder-bestiary-3-items/ILDPyfeR9ijXko3B.htm)|Jaws|Mâchoires|libre|
|[ILOl1dY50IaR6WPN.htm](pathfinder-bestiary-3-items/ILOl1dY50IaR6WPN.htm)|Liquefy|Se liquéfier|libre|
|[iN9SAyDZsYVDzYfN.htm](pathfinder-bestiary-3-items/iN9SAyDZsYVDzYfN.htm)|Mind Reading (At Will)|Lecture des pensées (À volonté)|officielle|
|[inTqnHZGNmB3Nv4k.htm](pathfinder-bestiary-3-items/inTqnHZGNmB3Nv4k.htm)|Crafting|Artisanat|libre|
|[IO9iVIVpEfBjrcmw.htm](pathfinder-bestiary-3-items/IO9iVIVpEfBjrcmw.htm)|Divine Dispelling|Dissipation du divin|libre|
|[IOBHBhN2gfM5QQHO.htm](pathfinder-bestiary-3-items/IOBHBhN2gfM5QQHO.htm)|Greater Constrict|Constriction supérieure|libre|
|[iommO8SU4yNamwC1.htm](pathfinder-bestiary-3-items/iommO8SU4yNamwC1.htm)|Focus Gaze|Focaliser le regard|libre|
|[iov5t4k8TcxeShEp.htm](pathfinder-bestiary-3-items/iov5t4k8TcxeShEp.htm)|Shortbow|Arc court|libre|
|[IP7OFv62ex3QvHzS.htm](pathfinder-bestiary-3-items/IP7OFv62ex3QvHzS.htm)|Knockdown|Renversement|libre|
|[IpaUvWrHaznmdOGb.htm](pathfinder-bestiary-3-items/IpaUvWrHaznmdOGb.htm)|Darkvision|Vision dans le noir|libre|
|[iPdoOjHPIU1Zhkvd.htm](pathfinder-bestiary-3-items/iPdoOjHPIU1Zhkvd.htm)|Stepping Decoy|Illusion de pas|libre|
|[IPI8W2nuWEsuk2ti.htm](pathfinder-bestiary-3-items/IPI8W2nuWEsuk2ti.htm)|Despairing Weep|Pleurs désespérés|libre|
|[iq5hF08qzTQTeTBD.htm](pathfinder-bestiary-3-items/iq5hF08qzTQTeTBD.htm)|Trample|Piétinement|libre|
|[iQBwvNjlgULNrpOy.htm](pathfinder-bestiary-3-items/iQBwvNjlgULNrpOy.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[iQNybW2FjtzVHCOS.htm](pathfinder-bestiary-3-items/iQNybW2FjtzVHCOS.htm)|Self-Resurrection|Auto-résurrection|libre|
|[iqOncPekvnZawgEq.htm](pathfinder-bestiary-3-items/iqOncPekvnZawgEq.htm)|Meteorology Lore|Connaissance de la météorologie|libre|
|[iQvaTMNbSTahgvnU.htm](pathfinder-bestiary-3-items/iQvaTMNbSTahgvnU.htm)|Foot|Pied|libre|
|[iraYrEvANESYqSYz.htm](pathfinder-bestiary-3-items/iraYrEvANESYqSYz.htm)|Antler|Bois|libre|
|[IRdpsVF3pePDKcEF.htm](pathfinder-bestiary-3-items/IRdpsVF3pePDKcEF.htm)|Enraged Home (Bludgeoning)|Maison enragée (Contondants)|libre|
|[iRJk9tacSkOQcb2p.htm](pathfinder-bestiary-3-items/iRJk9tacSkOQcb2p.htm)|Create Golden Apple|Création d'une pomme dorée|libre|
|[IRV6fBqa828pUJvZ.htm](pathfinder-bestiary-3-items/IRV6fBqa828pUJvZ.htm)|Unnatural Leap|Bond surnaturel|libre|
|[IsKgw1GkBDUH68wl.htm](pathfinder-bestiary-3-items/IsKgw1GkBDUH68wl.htm)|Wavesense (Imprecise) 60 feet|Perception des ondes 18 m (imprécis)|officielle|
|[ISNwBWNpl1RrSuwh.htm](pathfinder-bestiary-3-items/ISNwBWNpl1RrSuwh.htm)|Invisibility (At Will) (Self Only)|Invisibilité (À volonté) (soi uniquement)|officielle|
|[ISW9dU1e7h1uA2Vu.htm](pathfinder-bestiary-3-items/ISW9dU1e7h1uA2Vu.htm)|In Concert|En harmonie|libre|
|[IT0DYOyhbNdwhEsc.htm](pathfinder-bestiary-3-items/IT0DYOyhbNdwhEsc.htm)|At-Will Spells|Sorts à volonté|libre|
|[It88LMNyaOfw3HMG.htm](pathfinder-bestiary-3-items/It88LMNyaOfw3HMG.htm)|Shadow's Swiftness|Célérité de l'ombre|libre|
|[it8V6trXRaEs8bg1.htm](pathfinder-bestiary-3-items/it8V6trXRaEs8bg1.htm)|Charm (At Will)|Charme (À volonté)|officielle|
|[iuA961D6giH9Ci1G.htm](pathfinder-bestiary-3-items/iuA961D6giH9Ci1G.htm)|Construct Armor (Hardness 10)|Armure de créature artificielle (solidité 10)|libre|
|[iUcqbwAXsz9BBt5U.htm](pathfinder-bestiary-3-items/iUcqbwAXsz9BBt5U.htm)|Change Shape|Changement de forme|libre|
|[IUp0cmk3iouIb4mc.htm](pathfinder-bestiary-3-items/IUp0cmk3iouIb4mc.htm)|Mundane Appearance|Apparence normale|libre|
|[IvCrd9B34OQupCPq.htm](pathfinder-bestiary-3-items/IvCrd9B34OQupCPq.htm)|Slime Ball|Boule de limon|libre|
|[IVnGWQzHYIx6BwIf.htm](pathfinder-bestiary-3-items/IVnGWQzHYIx6BwIf.htm)|Ride Corpse|Diriger un cadavre|libre|
|[ivTJbPtmjGlcioNL.htm](pathfinder-bestiary-3-items/ivTJbPtmjGlcioNL.htm)|Halberd|Hallebarde|libre|
|[IvUr6AnOtG8K2BSD.htm](pathfinder-bestiary-3-items/IvUr6AnOtG8K2BSD.htm)|Energize Clockwork Wand|Dynamiser une baguette mécanique|libre|
|[iVuxv6GFqvanqgOO.htm](pathfinder-bestiary-3-items/iVuxv6GFqvanqgOO.htm)|Rend|Éventration (Griffe)|libre|
|[iwCc7o3EXz0xMRTD.htm](pathfinder-bestiary-3-items/iwCc7o3EXz0xMRTD.htm)|Legal Ledgers|Registres judiciaires|libre|
|[iWhzjrN6V3SMvbjf.htm](pathfinder-bestiary-3-items/iWhzjrN6V3SMvbjf.htm)|Claw|Griffe|libre|
|[IwRzWoZPoZN3p2w4.htm](pathfinder-bestiary-3-items/IwRzWoZPoZN3p2w4.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[iXj6VKYDT9ao0fDy.htm](pathfinder-bestiary-3-items/iXj6VKYDT9ao0fDy.htm)|Darkvision|Vision dans le noir|libre|
|[IXlGwryzhB2V9aPV.htm](pathfinder-bestiary-3-items/IXlGwryzhB2V9aPV.htm)|Stealth|Discrétion|libre|
|[iXMM9LrLHQONVyO1.htm](pathfinder-bestiary-3-items/iXMM9LrLHQONVyO1.htm)|Throw Spirits|Projection d'esprits liés|libre|
|[IYB1MIKRfITclT4i.htm](pathfinder-bestiary-3-items/IYB1MIKRfITclT4i.htm)|Suggestion (At Will)|Suggestion (À volonté)|officielle|
|[IyoiZQ9XhzNia9nz.htm](pathfinder-bestiary-3-items/IyoiZQ9XhzNia9nz.htm)|Shortsword|Épée courte|libre|
|[IyQeFgzzdZuwWSrh.htm](pathfinder-bestiary-3-items/IyQeFgzzdZuwWSrh.htm)|Tremorsense (Imprecise) 60 feet|Perception des vibrations 18 m (imprécis)|libre|
|[iYWcxAxjnGp9gag8.htm](pathfinder-bestiary-3-items/iYWcxAxjnGp9gag8.htm)|Hatchet|Hachette|libre|
|[iZ48ihJeJ9DbfW5I.htm](pathfinder-bestiary-3-items/iZ48ihJeJ9DbfW5I.htm)|Jaws|Mâchoires|libre|
|[IzAYt0TOGDn03Iuj.htm](pathfinder-bestiary-3-items/IzAYt0TOGDn03Iuj.htm)|Fiery Explosion|Déflagration de feu|libre|
|[iZEu9jYTf7nhB7z5.htm](pathfinder-bestiary-3-items/iZEu9jYTf7nhB7z5.htm)|Composite Shortbow|+1,striking|Arc court composite +1 de Frappe|libre|
|[iZqo211s6bnAgJto.htm](pathfinder-bestiary-3-items/iZqo211s6bnAgJto.htm)|Arcane Innate Spells|Sorts arcaniques innés|libre|
|[j1Bwf8sEmKvjZNHK.htm](pathfinder-bestiary-3-items/j1Bwf8sEmKvjZNHK.htm)|Low-Light Vision|Vision nocturne|libre|
|[j1yTlYWclYVbVuCO.htm](pathfinder-bestiary-3-items/j1yTlYWclYVbVuCO.htm)|Composite Longbow|Arc long composite|libre|
|[j2EwWhmaQSWT0Ktw.htm](pathfinder-bestiary-3-items/j2EwWhmaQSWT0Ktw.htm)|Wrath of Spurned Hospitality|Courroux de l'hospitalité refusée|libre|
|[J2Wcbnb4u8polTU4.htm](pathfinder-bestiary-3-items/J2Wcbnb4u8polTU4.htm)|Grab|Empoignade/Agrippement|libre|
|[J3Ldbu2TKyrPVZBY.htm](pathfinder-bestiary-3-items/J3Ldbu2TKyrPVZBY.htm)|Instrument of Faith|Instrument de foi|libre|
|[j3teD40q6U6024pW.htm](pathfinder-bestiary-3-items/j3teD40q6U6024pW.htm)|Foot|Pied|libre|
|[j3YK3rYte4HUEXXX.htm](pathfinder-bestiary-3-items/j3YK3rYte4HUEXXX.htm)|Troop Defenses|Défenses des troupes|libre|
|[J5Is1PpCXNmzaN0y.htm](pathfinder-bestiary-3-items/J5Is1PpCXNmzaN0y.htm)|Maze (Once per Week)|Labyrinthe (Une fois par semaine)|libre|
|[J5VPK19qAf7MabPt.htm](pathfinder-bestiary-3-items/J5VPK19qAf7MabPt.htm)|Emerge From Undergrowth|Émerger des sous-bois|libre|
|[j67aVe855G3FeDEO.htm](pathfinder-bestiary-3-items/j67aVe855G3FeDEO.htm)|Improved Grab|Empoignade/Agrippement améliorés|libre|
|[j68N9UkahC1d6lhq.htm](pathfinder-bestiary-3-items/j68N9UkahC1d6lhq.htm)|-1 Status to All Saves vs. Death Effects|Malus de statut de -1 à tous les jets de sauvegarde contre les effets de mort|libre|
|[J6FVU37oPVKikGeY.htm](pathfinder-bestiary-3-items/J6FVU37oPVKikGeY.htm)|Darkvision|Vision dans le noir|libre|
|[j6jdXFHyW1BBy2iT.htm](pathfinder-bestiary-3-items/j6jdXFHyW1BBy2iT.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[j6z5lXtJI1JXtP6N.htm](pathfinder-bestiary-3-items/j6z5lXtJI1JXtP6N.htm)|Constant Spells|Sorts constants|libre|
|[j71uBba5yrrEslXS.htm](pathfinder-bestiary-3-items/j71uBba5yrrEslXS.htm)|Flexible|Flexible|libre|
|[J7axLPnjNBn2CYP9.htm](pathfinder-bestiary-3-items/J7axLPnjNBn2CYP9.htm)|Negative Healing|Guérison négative|libre|
|[J9DbeNCFrbq08rQy.htm](pathfinder-bestiary-3-items/J9DbeNCFrbq08rQy.htm)|Felt Shears|Cisailles de feutre|libre|
|[JA8W1EoSKmMH74zg.htm](pathfinder-bestiary-3-items/JA8W1EoSKmMH74zg.htm)|Illusory Weapon|Arme illusoire|libre|
|[JAaRcZ0xgXIgQi8D.htm](pathfinder-bestiary-3-items/JAaRcZ0xgXIgQi8D.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[jAcMhmLTK9Y34bhZ.htm](pathfinder-bestiary-3-items/jAcMhmLTK9Y34bhZ.htm)|Coven Spells|Sorts de cercle|libre|
|[jaJAvtkcuLs6oqUz.htm](pathfinder-bestiary-3-items/jaJAvtkcuLs6oqUz.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[jB6DPG2ZqtuA1Z3r.htm](pathfinder-bestiary-3-items/jB6DPG2ZqtuA1Z3r.htm)|Regeneration 15 (Deactivated by Acid)|Régénération 15 (désactivée par Acide)|libre|
|[jBnvJizQVNlbpByJ.htm](pathfinder-bestiary-3-items/jBnvJizQVNlbpByJ.htm)|Jaws|Mâchoires|libre|
|[jbOS76Gp3MzY4W9x.htm](pathfinder-bestiary-3-items/jbOS76Gp3MzY4W9x.htm)|Torturous Buzz|Bourdonnement insoutenable|libre|
|[jBu4ahdLBrGQ0cxJ.htm](pathfinder-bestiary-3-items/jBu4ahdLBrGQ0cxJ.htm)|Shuriken|Shuriken|libre|
|[jbVk6BJ2TRpBl0PN.htm](pathfinder-bestiary-3-items/jbVk6BJ2TRpBl0PN.htm)|Composite Longbow|+1,striking|Arc long composite de frappe +1|officielle|
|[jcu8xSbi5cUAAu26.htm](pathfinder-bestiary-3-items/jcu8xSbi5cUAAu26.htm)|+2 Greater Resilient Breastplate|Cuirasse de résilience supérieure +2|libre|
|[JCVCYsMSue62ekID.htm](pathfinder-bestiary-3-items/JCVCYsMSue62ekID.htm)|Project Terror|Projection de terreur|libre|
|[JcXJdmKDoEq5nqTp.htm](pathfinder-bestiary-3-items/JcXJdmKDoEq5nqTp.htm)|Covetous of Secrets|Avide de secrets|libre|
|[Jd2DYD67ChktNYVN.htm](pathfinder-bestiary-3-items/Jd2DYD67ChktNYVN.htm)|Tongues (Constant)|Don des langues (constant)|officielle|
|[jd3HtosBbbWGDYMZ.htm](pathfinder-bestiary-3-items/jd3HtosBbbWGDYMZ.htm)|Spider Legs|Pattes d'araignée|libre|
|[JD7dIGSb1yYwZOil.htm](pathfinder-bestiary-3-items/JD7dIGSb1yYwZOil.htm)|At-Will Spells|Sorts à volonté|libre|
|[jDheLY8hqIt7kVGR.htm](pathfinder-bestiary-3-items/jDheLY8hqIt7kVGR.htm)|Dispel Magic (At Will)|Dissipation de la magie (À volonté)|officielle|
|[jdhRSwRmm4AuXQt1.htm](pathfinder-bestiary-3-items/jdhRSwRmm4AuXQt1.htm)|Foot|Pied|libre|
|[JDNUanP8QQ5riRTG.htm](pathfinder-bestiary-3-items/JDNUanP8QQ5riRTG.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[jEfZwLub2SXZ9ApN.htm](pathfinder-bestiary-3-items/jEfZwLub2SXZ9ApN.htm)|Ominous Footsteps|Pas menaçants|libre|
|[jETzhtuReRYnPbPE.htm](pathfinder-bestiary-3-items/jETzhtuReRYnPbPE.htm)|False Foe|Faux ennemi|libre|
|[jeUZBWMJc0ynT78X.htm](pathfinder-bestiary-3-items/jeUZBWMJc0ynT78X.htm)|Darkvision|Vision dans le noir|libre|
|[JG2FJBq2rmJM7TqB.htm](pathfinder-bestiary-3-items/JG2FJBq2rmJM7TqB.htm)|+1 Status to All Saves vs. Darkness or Shadow|bonus de statut de +1 aux JdS contre les effets de ténèbres ou d'ombre|libre|
|[jgB5xsEwGDphGPM7.htm](pathfinder-bestiary-3-items/jgB5xsEwGDphGPM7.htm)|Golem Antimagic|Antimagie du golem|libre|
|[JgIL2Chquwv95iZZ.htm](pathfinder-bestiary-3-items/JgIL2Chquwv95iZZ.htm)|Hyponatremia|Hyponatrémie|libre|
|[JGruUFHuCKGitKYf.htm](pathfinder-bestiary-3-items/JGruUFHuCKGitKYf.htm)|Magic Aura (At Will)|Aura magique (À volonté)|libre|
|[JGSUP9GpkQinyXNh.htm](pathfinder-bestiary-3-items/JGSUP9GpkQinyXNh.htm)|Darkvision|Vision dans le noir|libre|
|[jGVjxRiLDypUaQCi.htm](pathfinder-bestiary-3-items/jGVjxRiLDypUaQCi.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[JHEhs3yuQkcfulWY.htm](pathfinder-bestiary-3-items/JHEhs3yuQkcfulWY.htm)|Anguished Cry|Plainte d'angoisse|libre|
|[jhi0sreme2yjolfI.htm](pathfinder-bestiary-3-items/jhi0sreme2yjolfI.htm)|Spear|Lance|libre|
|[jHVQHOSLTThAA8th.htm](pathfinder-bestiary-3-items/jHVQHOSLTThAA8th.htm)|Antler|Bois|libre|
|[ji4mZPdeTTXd5DD2.htm](pathfinder-bestiary-3-items/ji4mZPdeTTXd5DD2.htm)|Low-Light Vision|Vision nocturne|libre|
|[jidSgetwzkY6k5WH.htm](pathfinder-bestiary-3-items/jidSgetwzkY6k5WH.htm)|See Invisibility (Constant)|Détection de l'invisibilité (constant)|officielle|
|[JiieW7FHU7H9vlCH.htm](pathfinder-bestiary-3-items/JiieW7FHU7H9vlCH.htm)|Greater Constrict|Constriction supérieure|libre|
|[jJdhYFljJjyWjYuO.htm](pathfinder-bestiary-3-items/jJdhYFljJjyWjYuO.htm)|Light to Dark|De Lumière aux ténèbres|libre|
|[JjTVprpY8GeQPnTT.htm](pathfinder-bestiary-3-items/JjTVprpY8GeQPnTT.htm)|Thoughtsense (Imprecise) 60 feet|Perception des pensées 18 m (imprécis)|libre|
|[JK8lH0FZWQnphVLZ.htm](pathfinder-bestiary-3-items/JK8lH0FZWQnphVLZ.htm)|Farming Lore|Connaissance agricole|officielle|
|[jkOvDfJsGXDaYicW.htm](pathfinder-bestiary-3-items/jkOvDfJsGXDaYicW.htm)|Crocodile Empathy|Empathie avec les crocodiles|libre|
|[jKqS45yr5dqPtPAf.htm](pathfinder-bestiary-3-items/jKqS45yr5dqPtPAf.htm)|Claw|Griffe|libre|
|[jlBFW0dhslsLTtR1.htm](pathfinder-bestiary-3-items/jlBFW0dhslsLTtR1.htm)|Collective Sense|Perception collective|libre|
|[jlphhH3RnH4L3Qup.htm](pathfinder-bestiary-3-items/jlphhH3RnH4L3Qup.htm)|Grab|Empoignade/Agrippement|libre|
|[jLWpzWTqyo83F7Uy.htm](pathfinder-bestiary-3-items/jLWpzWTqyo83F7Uy.htm)|At-Will Spells|Sorts à volonté|libre|
|[JmeFujNsZiXeU7qA.htm](pathfinder-bestiary-3-items/JmeFujNsZiXeU7qA.htm)|Breath Weapon|Arme de souffle|libre|
|[JmiVjM3xyF1WaZae.htm](pathfinder-bestiary-3-items/JmiVjM3xyF1WaZae.htm)|Uncanny Pounce|Bond troublant|libre|
|[jMJ3VKTn3P4G9pne.htm](pathfinder-bestiary-3-items/jMJ3VKTn3P4G9pne.htm)|Instant Suspension|Suspension immédiate|libre|
|[jMQ5yo71IDbzIoiV.htm](pathfinder-bestiary-3-items/jMQ5yo71IDbzIoiV.htm)|Jaws|Mâchoires|libre|
|[JNAbSt0GireCV1x1.htm](pathfinder-bestiary-3-items/JNAbSt0GireCV1x1.htm)|Darkvision|Vision dans le noir|libre|
|[JNflvBLtzzPBlMKZ.htm](pathfinder-bestiary-3-items/JNflvBLtzzPBlMKZ.htm)|Heat of the Forge|Chaleur de la forge|libre|
|[JNHO9c4kCv8dEqkU.htm](pathfinder-bestiary-3-items/JNHO9c4kCv8dEqkU.htm)|Tongue|Langue|libre|
|[jnPwDZEoWvaBHihS.htm](pathfinder-bestiary-3-items/jnPwDZEoWvaBHihS.htm)|Jorogumo Venom|Venin de Jorogumo|libre|
|[JOnNHpqNRRfoZym8.htm](pathfinder-bestiary-3-items/JOnNHpqNRRfoZym8.htm)|At-Will Spells|Sorts à volonté|libre|
|[JOPv6XlxMrnEuMiK.htm](pathfinder-bestiary-3-items/JOPv6XlxMrnEuMiK.htm)|Negative Healing|Guérison négative|libre|
|[jpLpYja2kQc3ywu3.htm](pathfinder-bestiary-3-items/jpLpYja2kQc3ywu3.htm)|Telepathy 30 feet (Munavris Only)|Télépathie 9 mètres (seulement les munavris)|libre|
|[JpsnonKEA4ScV5n5.htm](pathfinder-bestiary-3-items/JpsnonKEA4ScV5n5.htm)|Tail|Queue|libre|
|[JQcN49zNIu4wTuSg.htm](pathfinder-bestiary-3-items/JQcN49zNIu4wTuSg.htm)|Defensive Quills|Épines défensives|libre|
|[jQCQmFdnjvvlQOeA.htm](pathfinder-bestiary-3-items/jQCQmFdnjvvlQOeA.htm)|Darkvision|Vision dans le noir|libre|
|[jQDDocLXx8D5KEwx.htm](pathfinder-bestiary-3-items/jQDDocLXx8D5KEwx.htm)|Catch Rock|Interception de rochers|libre|
|[jQpWelCVysebUl0R.htm](pathfinder-bestiary-3-items/jQpWelCVysebUl0R.htm)|Tail|Queue|libre|
|[jqQAuZsUGFs0m1uI.htm](pathfinder-bestiary-3-items/jqQAuZsUGFs0m1uI.htm)|Lifesense 120 feet|Perception de la vie|libre|
|[jrAbmB8HIKhZ8PFd.htm](pathfinder-bestiary-3-items/jrAbmB8HIKhZ8PFd.htm)|Jaws|Mâchoires|libre|
|[jRNANkCIgSnmDUWP.htm](pathfinder-bestiary-3-items/jRNANkCIgSnmDUWP.htm)|Low-Light Vision|Vision nocturne|libre|
|[Js5HPgVs02JYjPwr.htm](pathfinder-bestiary-3-items/Js5HPgVs02JYjPwr.htm)|Spray Pus|Projection de pus|libre|
|[JScL1xh1oN0V3BSZ.htm](pathfinder-bestiary-3-items/JScL1xh1oN0V3BSZ.htm)|Darkvision|Vision dans le noir|libre|
|[JSGbWepzGabICqtd.htm](pathfinder-bestiary-3-items/JSGbWepzGabICqtd.htm)|Spray Blinding Musk|Projection de musc aveuglant|libre|
|[jspQIwxqoVmeF37U.htm](pathfinder-bestiary-3-items/jspQIwxqoVmeF37U.htm)|Invoke Haunter of the Dark|Invoquer Celui qui hante les ténèbres|libre|
|[JSRXi7FpsI5stZOw.htm](pathfinder-bestiary-3-items/JSRXi7FpsI5stZOw.htm)|Fangs|Crocs|libre|
|[Jt5uw27Ef9hVZ8D5.htm](pathfinder-bestiary-3-items/Jt5uw27Ef9hVZ8D5.htm)|Thoughtsense (Precise) 60 feet|Perception des pensées 18 mètres (précis)|libre|
|[jTT3N3sXixsPE4b1.htm](pathfinder-bestiary-3-items/jTT3N3sXixsPE4b1.htm)|Fist|Poing|libre|
|[jTvAXL1UqsQrbQhO.htm](pathfinder-bestiary-3-items/jTvAXL1UqsQrbQhO.htm)|Telepathy (Touch)|Télépathie (Contact)|libre|
|[jUd95l5nba0bNy3I.htm](pathfinder-bestiary-3-items/jUd95l5nba0bNy3I.htm)|Gleaming Armor|Armure brillante|libre|
|[jveVL0lDMa0EIIx6.htm](pathfinder-bestiary-3-items/jveVL0lDMa0EIIx6.htm)|Constant Spells|Sorts constants|libre|
|[jvJQ52E2kygewU8J.htm](pathfinder-bestiary-3-items/jvJQ52E2kygewU8J.htm)|Spear|Lance|libre|
|[JVzWcWD2qbxHGbUb.htm](pathfinder-bestiary-3-items/JVzWcWD2qbxHGbUb.htm)|Athletics|Athlétisme|libre|
|[jw9ImZmmbzTSmeX5.htm](pathfinder-bestiary-3-items/jw9ImZmmbzTSmeX5.htm)|Threatening Lunge|Allonge brusque|libre|
|[JwmDTNMiGDZ6Kis3.htm](pathfinder-bestiary-3-items/JwmDTNMiGDZ6Kis3.htm)|Dagger|Dague|libre|
|[jwmn7WQty7oJo2L7.htm](pathfinder-bestiary-3-items/jwmn7WQty7oJo2L7.htm)|Construct Armor (Hardness 15)|Armure de créature artificielle (solidité 15)|libre|
|[JwRLhgIxYJX1wLQx.htm](pathfinder-bestiary-3-items/JwRLhgIxYJX1wLQx.htm)|Thundering Charge|Charge foudroyante|libre|
|[JxdXfCP6ayaEEZND.htm](pathfinder-bestiary-3-items/JxdXfCP6ayaEEZND.htm)|Jaws|Mâchoires|libre|
|[JxGDiOzvqKBoFWu5.htm](pathfinder-bestiary-3-items/JxGDiOzvqKBoFWu5.htm)|Plane Shift (Self Only)|Changement de plan (soi uniquement)|libre|
|[jXHs8IwojZHlZUSA.htm](pathfinder-bestiary-3-items/jXHs8IwojZHlZUSA.htm)|Wind Mastery|Maîtrise du vent|libre|
|[JxnXxf6brDnOqBXG.htm](pathfinder-bestiary-3-items/JxnXxf6brDnOqBXG.htm)|Sweltering Heat|Chaleur étouffante|libre|
|[JXy1hnyoxsc0xnCO.htm](pathfinder-bestiary-3-items/JXy1hnyoxsc0xnCO.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[JYnZKoZ4Immi2yaz.htm](pathfinder-bestiary-3-items/JYnZKoZ4Immi2yaz.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[Jyzm7OFRyidSiJk0.htm](pathfinder-bestiary-3-items/Jyzm7OFRyidSiJk0.htm)|Spore Domination|Domination des spores|libre|
|[Jzdce7Z64aM5gu70.htm](pathfinder-bestiary-3-items/Jzdce7Z64aM5gu70.htm)|Phantom Sword|Épée fantôme|libre|
|[JZeE5BqCANDTf6hq.htm](pathfinder-bestiary-3-items/JZeE5BqCANDTf6hq.htm)|Change Shape|Changement de forme|libre|
|[jZf3j2xK5YKD3uC0.htm](pathfinder-bestiary-3-items/jZf3j2xK5YKD3uC0.htm)|Activate Defenses|Activation des défenses|libre|
|[jzI14zm2om9KJeOM.htm](pathfinder-bestiary-3-items/jzI14zm2om9KJeOM.htm)|Devourer of Swarms|Dévoreur de nuées|libre|
|[JZJkUq4BEg1IAg4W.htm](pathfinder-bestiary-3-items/JZJkUq4BEg1IAg4W.htm)|Tongues (Constant)|Don des langues (constant)|officielle|
|[K0pcy2s3gTJZZK7a.htm](pathfinder-bestiary-3-items/K0pcy2s3gTJZZK7a.htm)|Stormsight|Vision malgré la tempête|libre|
|[k1fBI6krs8NLi3AR.htm](pathfinder-bestiary-3-items/k1fBI6krs8NLi3AR.htm)|Pitchfork|Fourche|libre|
|[k1FCB3wNkyv3rnly.htm](pathfinder-bestiary-3-items/k1FCB3wNkyv3rnly.htm)|Vortex|Vortex|libre|
|[K1MeCVjxSahZAhHQ.htm](pathfinder-bestiary-3-items/K1MeCVjxSahZAhHQ.htm)|Sack for Holding Rocks|Sac contenant des rochers|libre|
|[k2B8t1pzRzSkO2qC.htm](pathfinder-bestiary-3-items/k2B8t1pzRzSkO2qC.htm)|Talon|Serre|libre|
|[k2HBbsayvnQzyEIo.htm](pathfinder-bestiary-3-items/k2HBbsayvnQzyEIo.htm)|Death Umbra|Mort d'ombres|libre|
|[k2Num39uDHGiZwTm.htm](pathfinder-bestiary-3-items/k2Num39uDHGiZwTm.htm)|Breath Weapon|Arme de souffle|libre|
|[k2oujPtkWYkCjV8t.htm](pathfinder-bestiary-3-items/k2oujPtkWYkCjV8t.htm)|Jaws|Mâchoires|libre|
|[K32Ag7GpssjobCOz.htm](pathfinder-bestiary-3-items/K32Ag7GpssjobCOz.htm)|Darkvision|Vision dans le noir|libre|
|[k3a4ohAlfjdpm7Ck.htm](pathfinder-bestiary-3-items/k3a4ohAlfjdpm7Ck.htm)|Grab|Empoignade/Agrippement|libre|
|[K3dfOHkozmujfcG3.htm](pathfinder-bestiary-3-items/K3dfOHkozmujfcG3.htm)|Endure Elements (Self Only)|Endurance aux éléments (soi uniquement)|libre|
|[k3dgXH9CegWMvwQi.htm](pathfinder-bestiary-3-items/k3dgXH9CegWMvwQi.htm)|Consume Souls|Dévorer les âmes|libre|
|[K4gqGlmGETi7zB7J.htm](pathfinder-bestiary-3-items/K4gqGlmGETi7zB7J.htm)|Charm (Undead Only)|Charme (mort-vivant uniquement)|libre|
|[k4kguAJD9K2mg4zY.htm](pathfinder-bestiary-3-items/k4kguAJD9K2mg4zY.htm)|Impossible Stature|Stature imposante|libre|
|[k5a2kb9JPuGSAe7J.htm](pathfinder-bestiary-3-items/k5a2kb9JPuGSAe7J.htm)|Darkvision|Vision dans le noir|libre|
|[K5ozGM2drmKhGIoI.htm](pathfinder-bestiary-3-items/K5ozGM2drmKhGIoI.htm)|Aquatic Ambush|Embuscade aquatique|libre|
|[k5ozmTQNA7IyzbmH.htm](pathfinder-bestiary-3-items/k5ozmTQNA7IyzbmH.htm)|Mind Reading (At Will)|Lecture des pensées (À volonté)|officielle|
|[k6bhkrULfZctxZZa.htm](pathfinder-bestiary-3-items/k6bhkrULfZctxZZa.htm)|Landslide|Glissement de terrain|libre|
|[k6KXYRt0pYf0ds2B.htm](pathfinder-bestiary-3-items/k6KXYRt0pYf0ds2B.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[K6p90DP5TOgu1uHl.htm](pathfinder-bestiary-3-items/K6p90DP5TOgu1uHl.htm)|Tremorsense (Imprecise) 60 feet|Perception des vibrations 18 m (imprécis)|libre|
|[K8HY6MgNzIvDLuDT.htm](pathfinder-bestiary-3-items/K8HY6MgNzIvDLuDT.htm)|Bone Chariot|Char osseux|libre|
|[K9b86nLjHgbbzFnm.htm](pathfinder-bestiary-3-items/K9b86nLjHgbbzFnm.htm)|Fist|Poing|libre|
|[ka8ggPLmvKPNC19r.htm](pathfinder-bestiary-3-items/ka8ggPLmvKPNC19r.htm)|Wind Blast|Rafale de vent|libre|
|[kacehtkxmzv3j5Zn.htm](pathfinder-bestiary-3-items/kacehtkxmzv3j5Zn.htm)|Primal Prepared Spells|Sorts primordiaux préparés|libre|
|[kALPdWJYfV0Cg7Iu.htm](pathfinder-bestiary-3-items/kALPdWJYfV0Cg7Iu.htm)|Toenail Cutter|Coupe-ongles|libre|
|[kB4Y7lsPktJzaUtm.htm](pathfinder-bestiary-3-items/kB4Y7lsPktJzaUtm.htm)|Countered by Metal|Contré par le métal|libre|
|[kBmOKMaUjcEjTeia.htm](pathfinder-bestiary-3-items/kBmOKMaUjcEjTeia.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[KbRk42Ska9TyPynC.htm](pathfinder-bestiary-3-items/KbRk42Ska9TyPynC.htm)|Dab|Tamponner|libre|
|[kCoPTtj3Cu57Xebn.htm](pathfinder-bestiary-3-items/kCoPTtj3Cu57Xebn.htm)|Halberd|+3,greaterStriking|Hallebarde de frappe supérieure +3|libre|
|[KD9WyZyMHgs07nLa.htm](pathfinder-bestiary-3-items/KD9WyZyMHgs07nLa.htm)|Claw|Griffe|libre|
|[KdePPltFxHzCPJrm.htm](pathfinder-bestiary-3-items/KdePPltFxHzCPJrm.htm)|At-Will Spells|Sorts à volonté|libre|
|[KDhzo4KVYsJvSpXo.htm](pathfinder-bestiary-3-items/KDhzo4KVYsJvSpXo.htm)|Keelhaul|Passage sous la quille|libre|
|[KdJvX2wvTeXTeu5i.htm](pathfinder-bestiary-3-items/KdJvX2wvTeXTeu5i.htm)|Dimension Door (At Will)|Porte dimensionnelle (À volonté)|officielle|
|[KdYM2NV3yWc57PNc.htm](pathfinder-bestiary-3-items/KdYM2NV3yWc57PNc.htm)|Darkvision|Vision dans le noir|libre|
|[Ke8jPPnQzzzeKXWw.htm](pathfinder-bestiary-3-items/Ke8jPPnQzzzeKXWw.htm)|Darkvision|Vision dans le noir|libre|
|[KEJNkeF17Hd5RgpW.htm](pathfinder-bestiary-3-items/KEJNkeF17Hd5RgpW.htm)|Feral Trackers|Pistage féral|libre|
|[KewwLz9N8mWqhTpA.htm](pathfinder-bestiary-3-items/KewwLz9N8mWqhTpA.htm)|Frightful Presence|Présence terrifiante|libre|
|[Kf0yffdxraBrMmiJ.htm](pathfinder-bestiary-3-items/Kf0yffdxraBrMmiJ.htm)|Acrobatics|Acrobaties|libre|
|[KF8c1lT0YVj5lMG4.htm](pathfinder-bestiary-3-items/KF8c1lT0YVj5lMG4.htm)|Mind Reading (At Will)|Lecture des pensées (À volonté)|officielle|
|[kfBNKa5ltxDIa0qG.htm](pathfinder-bestiary-3-items/kfBNKa5ltxDIa0qG.htm)|At-Will Spells|Sorts à volonté|libre|
|[KftIJAHBIGp8Mnad.htm](pathfinder-bestiary-3-items/KftIJAHBIGp8Mnad.htm)|Camouflage|Camouflage|libre|
|[KGCeRo6qPcjltHnA.htm](pathfinder-bestiary-3-items/KGCeRo6qPcjltHnA.htm)|Wavesense (Imprecise) 10 feet|Perception des ondes 3 m (imprécis)|libre|
|[KgGTXxW5g0BAy1nV.htm](pathfinder-bestiary-3-items/KgGTXxW5g0BAy1nV.htm)|Spiked Chain|+1,striking,ghostTouch|Chaine cloutée spectrale de frappe +1|libre|
|[KgidUZZqAF6dfZdR.htm](pathfinder-bestiary-3-items/KgidUZZqAF6dfZdR.htm)|Bo Staff|Bô|libre|
|[KGyAqL8XCgBPKJwN.htm](pathfinder-bestiary-3-items/KGyAqL8XCgBPKJwN.htm)|Divine Lightning|Foudre divine|libre|
|[khSydk2fZucaWpwr.htm](pathfinder-bestiary-3-items/khSydk2fZucaWpwr.htm)|At-Will Spells|Sorts à volonté|libre|
|[KHXOl15PR5DH02Gu.htm](pathfinder-bestiary-3-items/KHXOl15PR5DH02Gu.htm)|Dispel Magic (At Will)|Dissipation de la magie (À volonté)|officielle|
|[KIEOH7Hzo0bgoAnC.htm](pathfinder-bestiary-3-items/KIEOH7Hzo0bgoAnC.htm)|Darkvision|Vision dans le noir|libre|
|[kIfdLdjbTbTKS03H.htm](pathfinder-bestiary-3-items/kIfdLdjbTbTKS03H.htm)|Comprehend Language (At Will) (Self Only)|Compréhension des langues (À volonté) (soi uniquement)|libre|
|[kimzS4sVtC1JuBK6.htm](pathfinder-bestiary-3-items/kimzS4sVtC1JuBK6.htm)|Tail|Queue|libre|
|[KjameIcuKwYkyTzJ.htm](pathfinder-bestiary-3-items/KjameIcuKwYkyTzJ.htm)|Cooperative Hunting|Chasse coopérative|libre|
|[kjCwpEZhQpTXIyZj.htm](pathfinder-bestiary-3-items/kjCwpEZhQpTXIyZj.htm)|Defensive Disarm|Désarmement défensif|libre|
|[KjLaF8NJBaPjsRAz.htm](pathfinder-bestiary-3-items/KjLaF8NJBaPjsRAz.htm)|Gnash|Acharnement|libre|
|[kjoPJjVHl7tsoAet.htm](pathfinder-bestiary-3-items/kjoPJjVHl7tsoAet.htm)|Breath Weapon|Arme de souffle|libre|
|[KJRA38LAe9tJK0hJ.htm](pathfinder-bestiary-3-items/KJRA38LAe9tJK0hJ.htm)|Heraldry Lore|Connaissance de l'héraldique|libre|
|[KJxwCEgKTrckplr8.htm](pathfinder-bestiary-3-items/KJxwCEgKTrckplr8.htm)|Hell Lore|Connaissance des Enfers|libre|
|[KkkIZP8XxpPJcECa.htm](pathfinder-bestiary-3-items/KkkIZP8XxpPJcECa.htm)|Flaming Greataxe|Grande hache enflammée|libre|
|[KkSN8aLpddZGTcZq.htm](pathfinder-bestiary-3-items/KkSN8aLpddZGTcZq.htm)|Greater Constrict|Constriction supérieure|libre|
|[klPNjA3PDnepkkn6.htm](pathfinder-bestiary-3-items/klPNjA3PDnepkkn6.htm)|At-Will Spells|Sorts à volonté|libre|
|[KluTBobaNmcJwryy.htm](pathfinder-bestiary-3-items/KluTBobaNmcJwryy.htm)|Darkvision|Vision dans le noir|libre|
|[KMdZUSARMPSywMsn.htm](pathfinder-bestiary-3-items/KMdZUSARMPSywMsn.htm)|Divine Aegis|Égide divine|libre|
|[kMfzeWG6rv2XM68m.htm](pathfinder-bestiary-3-items/kMfzeWG6rv2XM68m.htm)|Blade-Leg|Patte-lame|libre|
|[KmgwRHZExpyqDBij.htm](pathfinder-bestiary-3-items/KmgwRHZExpyqDBij.htm)|Scent (Imprecise) 60 feet|Odorat 18 m (imprécis)|libre|
|[knIdAZZkuVAcH33t.htm](pathfinder-bestiary-3-items/knIdAZZkuVAcH33t.htm)|Claw|Griffe|libre|
|[kNl1E67lBZGpSC5U.htm](pathfinder-bestiary-3-items/kNl1E67lBZGpSC5U.htm)|Gory Tendril|filament sanguinolent|libre|
|[KnlMKaajUHNW24mj.htm](pathfinder-bestiary-3-items/KnlMKaajUHNW24mj.htm)|Crossbow|Arbalète|libre|
|[KOan7i2UeawwyO2v.htm](pathfinder-bestiary-3-items/KOan7i2UeawwyO2v.htm)|Push|Bousculade|officielle|
|[koOLkiJRcE1XYMxS.htm](pathfinder-bestiary-3-items/koOLkiJRcE1XYMxS.htm)|Darkvision|Vision dans le noir|libre|
|[kpB9dRBV5LWoW0uG.htm](pathfinder-bestiary-3-items/kpB9dRBV5LWoW0uG.htm)|Darkvision|Vision dans le noir|libre|
|[kpvnHl1XmqHSX6rP.htm](pathfinder-bestiary-3-items/kpvnHl1XmqHSX6rP.htm)|At-Will Spells|Sorts à volonté|libre|
|[KpWuD5FV2RQBjAeo.htm](pathfinder-bestiary-3-items/KpWuD5FV2RQBjAeo.htm)|Primal Prepared Spells|Sorts primordiaux préparés|libre|
|[kpx9ES2ZOnjt0XMh.htm](pathfinder-bestiary-3-items/kpx9ES2ZOnjt0XMh.htm)|Troop Defenses|Défenses des troupes|libre|
|[kpySQfuxcXr6ztFA.htm](pathfinder-bestiary-3-items/kpySQfuxcXr6ztFA.htm)|Halberd|Hallebarde|libre|
|[kQaDhJaLxNJw4l8l.htm](pathfinder-bestiary-3-items/kQaDhJaLxNJw4l8l.htm)|Low-Light Vision|Vision nocturne|libre|
|[kqcQU0n1PhfM4fgW.htm](pathfinder-bestiary-3-items/kqcQU0n1PhfM4fgW.htm)|Swarm Mind|Esprit de la nuée|libre|
|[kQNlQmU6whlWK7M9.htm](pathfinder-bestiary-3-items/kQNlQmU6whlWK7M9.htm)|Telepathic Wail|Plainte télépathique|libre|
|[KqQvNC6PQXFL9bre.htm](pathfinder-bestiary-3-items/KqQvNC6PQXFL9bre.htm)|Occult Prepared Spells|Sorts occultes préparés|libre|
|[KRjbKjp9iT8REk4M.htm](pathfinder-bestiary-3-items/KRjbKjp9iT8REk4M.htm)|Ecstatic Hunger|Faim extatique|libre|
|[krWu51yBcWRllWxP.htm](pathfinder-bestiary-3-items/krWu51yBcWRllWxP.htm)|Sand Spin|Tourbillon de sable|libre|
|[ks2bGQlMCszTgN6c.htm](pathfinder-bestiary-3-items/ks2bGQlMCszTgN6c.htm)|Bardic Lore|Connaissance bardique|libre|
|[ks76ZS8gWSFgrdl9.htm](pathfinder-bestiary-3-items/ks76ZS8gWSFgrdl9.htm)|Catch Rock|Interception de rochers|libre|
|[kSMYzNRzwjJcLeCe.htm](pathfinder-bestiary-3-items/kSMYzNRzwjJcLeCe.htm)|Tail|Queue|libre|
|[kt9Kmearp1t75oDh.htm](pathfinder-bestiary-3-items/kt9Kmearp1t75oDh.htm)|Grab|Empoignade/Agrippement|libre|
|[KUErEL0JPCYaIeu0.htm](pathfinder-bestiary-3-items/KUErEL0JPCYaIeu0.htm)|Electrical Field|Champ électrique|libre|
|[KvEbx6NMqRfeeaD2.htm](pathfinder-bestiary-3-items/KvEbx6NMqRfeeaD2.htm)|Darkvision|Vision dans le noir|libre|
|[kVLMWbSRPeQyl3Oe.htm](pathfinder-bestiary-3-items/kVLMWbSRPeQyl3Oe.htm)|Spiritual Epidemic (At Will)|Épidémie spirituelle (À volonté)|libre|
|[kWARaiGoyPB0KHgj.htm](pathfinder-bestiary-3-items/kWARaiGoyPB0KHgj.htm)|Spirit Body|Corps d'esprit|libre|
|[kWiBTKt3h01RReit.htm](pathfinder-bestiary-3-items/kWiBTKt3h01RReit.htm)|Darkvision|Vision dans le noir|libre|
|[kX3JkP0HZPIQ7vWj.htm](pathfinder-bestiary-3-items/kX3JkP0HZPIQ7vWj.htm)|Light Blindness|Aveuglé par la lumière|officielle|
|[kXXLa1y6PCq4db69.htm](pathfinder-bestiary-3-items/kXXLa1y6PCq4db69.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|libre|
|[KXYQjUBjJFbx6t3Z.htm](pathfinder-bestiary-3-items/KXYQjUBjJFbx6t3Z.htm)|Claw|Griffe|libre|
|[kY9XyX74vUAFhali.htm](pathfinder-bestiary-3-items/kY9XyX74vUAFhali.htm)|Flay|Écorcher|libre|
|[kYmteRQa3LahCveN.htm](pathfinder-bestiary-3-items/kYmteRQa3LahCveN.htm)|Touch of Ages|Contact des âges|libre|
|[kZCjL41fVSpE8vkC.htm](pathfinder-bestiary-3-items/kZCjL41fVSpE8vkC.htm)|Skip Between|Alterner|libre|
|[KZhlld4EKQcCTBjw.htm](pathfinder-bestiary-3-items/KZhlld4EKQcCTBjw.htm)|Swarm Mind|Esprit de la nuée|libre|
|[kziqHkR0lOsYBKJz.htm](pathfinder-bestiary-3-items/kziqHkR0lOsYBKJz.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[kzOsIUUeSNgTP9xK.htm](pathfinder-bestiary-3-items/kzOsIUUeSNgTP9xK.htm)|At-Will Spells|Sorts à volonté|libre|
|[kzwDYGXsWl0wyTwN.htm](pathfinder-bestiary-3-items/kzwDYGXsWl0wyTwN.htm)|Light Hammer|Marteau de guerre léger|officielle|
|[KZzCxzwU4zROZ7As.htm](pathfinder-bestiary-3-items/KZzCxzwU4zROZ7As.htm)|Engineering Lore|Connaissance de l'ingénierie|officielle|
|[L0AaJkB1CVD61yGv.htm](pathfinder-bestiary-3-items/L0AaJkB1CVD61yGv.htm)|Master's Eyes|Yeux du maître|libre|
|[L2LnR1T2MiBlznWr.htm](pathfinder-bestiary-3-items/L2LnR1T2MiBlznWr.htm)|Draconic Momentum|Impulsion draconique|libre|
|[l2ymfoz6uzs9Q7vM.htm](pathfinder-bestiary-3-items/l2ymfoz6uzs9Q7vM.htm)|Constant Spells|Sorts constants|libre|
|[l2ZTQQTqrVwPvUY1.htm](pathfinder-bestiary-3-items/l2ZTQQTqrVwPvUY1.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[L3k41cRxvKs5ZH4L.htm](pathfinder-bestiary-3-items/L3k41cRxvKs5ZH4L.htm)|Morpheme Glyph|Glyphe morphème|libre|
|[l3QKE8kkrmNe5aZO.htm](pathfinder-bestiary-3-items/l3QKE8kkrmNe5aZO.htm)|Darkvision|Vision dans le noir|libre|
|[L4eZWrxPCscqkfv1.htm](pathfinder-bestiary-3-items/L4eZWrxPCscqkfv1.htm)|Hurl Javelins!|Projection de javeline !|libre|
|[l5mWQVGm4hAXrFYp.htm](pathfinder-bestiary-3-items/l5mWQVGm4hAXrFYp.htm)|Spiked Chain|+3,greaterStriking,cold-iron|Chaine cloutée en fer froid de frappe supérieure +3|libre|
|[l6dezchxG4CL8dny.htm](pathfinder-bestiary-3-items/l6dezchxG4CL8dny.htm)|Burning Wings|Ailes brûlantes|libre|
|[l7a80DGdIvJp8Pp7.htm](pathfinder-bestiary-3-items/l7a80DGdIvJp8Pp7.htm)|Dwelling Lore|Connaissance de l'habitat|libre|
|[L86Ap8gRiTU3o6rf.htm](pathfinder-bestiary-3-items/L86Ap8gRiTU3o6rf.htm)|Dimension Door (At Will)|Porte dimensionnelle (À volonté)|officielle|
|[L87Xx2Cd9z06I697.htm](pathfinder-bestiary-3-items/L87Xx2Cd9z06I697.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[l8Vwgjba06YNgLZg.htm](pathfinder-bestiary-3-items/l8Vwgjba06YNgLZg.htm)|Regeneration 20 (Deactivated by Evil)|Régénération 20 (désactivée par Mauvais)|libre|
|[L9XsoNHRPeOdO6cL.htm](pathfinder-bestiary-3-items/L9XsoNHRPeOdO6cL.htm)|Striking Koa|Cohue violente|libre|
|[LaB8UoWpNQqZKM30.htm](pathfinder-bestiary-3-items/LaB8UoWpNQqZKM30.htm)|Rolling Thunder|Tonnerre grondant|libre|
|[LAhnCuHlBH3zJBoP.htm](pathfinder-bestiary-3-items/LAhnCuHlBH3zJBoP.htm)|Golem Antimagic|Antimagie Golem|libre|
|[Lb8baU3vfeAtn1Nw.htm](pathfinder-bestiary-3-items/Lb8baU3vfeAtn1Nw.htm)|Cower|Se recroqueviller|libre|
|[lbG0r8knl6cp3RdW.htm](pathfinder-bestiary-3-items/lbG0r8knl6cp3RdW.htm)|Claw|Griffe|libre|
|[lbhWyawC0OJPtNR4.htm](pathfinder-bestiary-3-items/lbhWyawC0OJPtNR4.htm)|Geas (Doesn't Require Secondary Casters And Can Target a Willing Creature of Any Level)|Serment rituel (ne nécessite pas d'incantateur secondaire et peut cibler une créature consentante de n'importe quel niveau)|libre|
|[lBss0k9IK6iCcQEo.htm](pathfinder-bestiary-3-items/lBss0k9IK6iCcQEo.htm)|Excruciating Enzyme|Enzyme insoutenable|libre|
|[LcChlrMndc88kp30.htm](pathfinder-bestiary-3-items/LcChlrMndc88kp30.htm)|Sunlight Powerlessness|Impuissance solaire|libre|
|[LcdaZzKwze9fmC6D.htm](pathfinder-bestiary-3-items/LcdaZzKwze9fmC6D.htm)|Defensive Slam|Claque défensive|libre|
|[lcGPZQiY2GFPXOVQ.htm](pathfinder-bestiary-3-items/lcGPZQiY2GFPXOVQ.htm)|Stunning Flurry|Déluge étourdissant|libre|
|[lciayGn2ma9dSbcM.htm](pathfinder-bestiary-3-items/lciayGn2ma9dSbcM.htm)|Curse of Darkness|Malédiction de ténèbres|libre|
|[lcYKbUtg1W8w9cul.htm](pathfinder-bestiary-3-items/lcYKbUtg1W8w9cul.htm)|Wing|Aile|libre|
|[lcYudXUlkKY7E3iT.htm](pathfinder-bestiary-3-items/lcYudXUlkKY7E3iT.htm)|Trample|Piétinement|libre|
|[LCYVlQZcfJT8AY75.htm](pathfinder-bestiary-3-items/LCYVlQZcfJT8AY75.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[lD1oJbNSEUiWgWzZ.htm](pathfinder-bestiary-3-items/lD1oJbNSEUiWgWzZ.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[lD2k5BU1mLju5ewn.htm](pathfinder-bestiary-3-items/lD2k5BU1mLju5ewn.htm)|Blade|Lame|libre|
|[LD67mqtsXztjCTmJ.htm](pathfinder-bestiary-3-items/LD67mqtsXztjCTmJ.htm)|Regeneration 20 (Deactivated by Fire)|Régénération 20 (désactivée par Feu)|libre|
|[LdhQrUO09um9htY6.htm](pathfinder-bestiary-3-items/LdhQrUO09um9htY6.htm)|Drain Blood|Absorber le sang|libre|
|[lDtCFaHXdvj9JdET.htm](pathfinder-bestiary-3-items/lDtCFaHXdvj9JdET.htm)|Shortbow|Arc court|libre|
|[LDTFrdNHyvizP1mX.htm](pathfinder-bestiary-3-items/LDTFrdNHyvizP1mX.htm)|At-Will Spells|Sorts à volonté|libre|
|[le8d6e5N7Wjb7Rgo.htm](pathfinder-bestiary-3-items/le8d6e5N7Wjb7Rgo.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[LehQoRloBWVwpTHh.htm](pathfinder-bestiary-3-items/LehQoRloBWVwpTHh.htm)|Mangle|Secousse broyante|libre|
|[leLKKGPbDbUPHstg.htm](pathfinder-bestiary-3-items/leLKKGPbDbUPHstg.htm)|Darkvision|Vision dans le noir|libre|
|[lFFJEvkkHiFKVLEu.htm](pathfinder-bestiary-3-items/lFFJEvkkHiFKVLEu.htm)|Pinch|Pincement|libre|
|[LfpK0azkldAisbCY.htm](pathfinder-bestiary-3-items/LfpK0azkldAisbCY.htm)|Extend Limb|Extension des membres|libre|
|[lgdEGoF8XPhWshor.htm](pathfinder-bestiary-3-items/lgdEGoF8XPhWshor.htm)|At-Will Spells|Sorts à volonté|libre|
|[Lgf1CXruVEasnLms.htm](pathfinder-bestiary-3-items/Lgf1CXruVEasnLms.htm)|Tremorsense (Imprecise) within their entire bound home|Perception des vibrations (imprécis) au sein de leur maison liée)|libre|
|[lGFDmPhDxFNEVo95.htm](pathfinder-bestiary-3-items/lGFDmPhDxFNEVo95.htm)|Invisibility (At Will) (Self Only)|Invisibilité (À volonté) (soi uniquement)|officielle|
|[LH24pBPDHDAEH6TV.htm](pathfinder-bestiary-3-items/LH24pBPDHDAEH6TV.htm)|Create Golden Apple|Création de la pomme d'or|libre|
|[LhA2xtluaWsZrc1T.htm](pathfinder-bestiary-3-items/LhA2xtluaWsZrc1T.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[lHGpzeVpbkAh9PNL.htm](pathfinder-bestiary-3-items/lHGpzeVpbkAh9PNL.htm)|Bastard Sword|Épée bâtarde|libre|
|[liA5VGcqAKl36Vqh.htm](pathfinder-bestiary-3-items/liA5VGcqAKl36Vqh.htm)|Darkvision|Vision dans le noir|libre|
|[liadzf6LKysC2rK8.htm](pathfinder-bestiary-3-items/liadzf6LKysC2rK8.htm)|Low-Light Vision|Vision nocturne|libre|
|[liMrTZS1ECyEEh6I.htm](pathfinder-bestiary-3-items/liMrTZS1ECyEEh6I.htm)|At-Will Spells|Sorts à volonté|libre|
|[lIN8nRgTYzPOm90G.htm](pathfinder-bestiary-3-items/lIN8nRgTYzPOm90G.htm)|Incalculable Fangs|Crocs impondérables|libre|
|[LIQcObuGu1wqBnEw.htm](pathfinder-bestiary-3-items/LIQcObuGu1wqBnEw.htm)|Grab|Empoignade/Agrippement|libre|
|[lj6vRNNZwXQmeqG7.htm](pathfinder-bestiary-3-items/lj6vRNNZwXQmeqG7.htm)|Claw|Griffe|libre|
|[lJDxq1w6WvzVEghk.htm](pathfinder-bestiary-3-items/lJDxq1w6WvzVEghk.htm)|Sprint|Sprint|libre|
|[Ljg3kgAdR1sVGBeL.htm](pathfinder-bestiary-3-items/Ljg3kgAdR1sVGBeL.htm)|Echolocation 60 feet|Écholocalisation 9 m|libre|
|[LK5MP99wRhzbl0Py.htm](pathfinder-bestiary-3-items/LK5MP99wRhzbl0Py.htm)|Settlement Lore|Connaissance des communautés|libre|
|[lK75Yi29CDBjFNYu.htm](pathfinder-bestiary-3-items/lK75Yi29CDBjFNYu.htm)|Darkvision|Vision dans le noir|libre|
|[LKCha1KO0muUvUr0.htm](pathfinder-bestiary-3-items/LKCha1KO0muUvUr0.htm)|Hellwasp Venom|Venin de guêpe infernale|libre|
|[lkfuADlIxVnyt6EH.htm](pathfinder-bestiary-3-items/lkfuADlIxVnyt6EH.htm)|Scimitar|Cimeterre|libre|
|[lL49wJa4ig4V0ag1.htm](pathfinder-bestiary-3-items/lL49wJa4ig4V0ag1.htm)|Record Audio|Enregistreur audio|libre|
|[Ll9EKS0upQ66GUIJ.htm](pathfinder-bestiary-3-items/Ll9EKS0upQ66GUIJ.htm)|Low-Light Vision|Vision nocturne|libre|
|[LLArpiTAsI8pUto2.htm](pathfinder-bestiary-3-items/LLArpiTAsI8pUto2.htm)|Master of the Home|Maître de la maison|libre|
|[llRkvAv40JIHPwXk.htm](pathfinder-bestiary-3-items/llRkvAv40JIHPwXk.htm)|Horn|Corne|libre|
|[LMRK013cxClnSMlQ.htm](pathfinder-bestiary-3-items/LMRK013cxClnSMlQ.htm)|Tail|Queue|libre|
|[lmUd66np5PVIvPFr.htm](pathfinder-bestiary-3-items/lmUd66np5PVIvPFr.htm)|Scent (Imprecise) 60 feet|Odorat 18 m (imprécis)|libre|
|[Ln0qmde8wRjzuIos.htm](pathfinder-bestiary-3-items/Ln0qmde8wRjzuIos.htm)|Moisture Dependency|Dépendance à l'humidité|libre|
|[lnftcCa5nWLimB4r.htm](pathfinder-bestiary-3-items/lnftcCa5nWLimB4r.htm)|Wing Thrash|Balayage d'ailes|libre|
|[Lni9y4yXW9RhxwLM.htm](pathfinder-bestiary-3-items/Lni9y4yXW9RhxwLM.htm)|Ventriloquism (At Will)|Ventriloquie (À volonté)|officielle|
|[LNRtNrzHIseTefIq.htm](pathfinder-bestiary-3-items/LNRtNrzHIseTefIq.htm)|Clockwork Wand|Baguette mécanique|libre|
|[lNuMFP2gGs9IFNk7.htm](pathfinder-bestiary-3-items/lNuMFP2gGs9IFNk7.htm)|Telepathy 30 feet|Télépathie 9 m|libre|
|[lo1xAOTNNBx9CRHH.htm](pathfinder-bestiary-3-items/lo1xAOTNNBx9CRHH.htm)|Harp|Harpe|libre|
|[lO3tFaQKyvdDCf09.htm](pathfinder-bestiary-3-items/lO3tFaQKyvdDCf09.htm)|Low-Light Vision|Vision nocturne|libre|
|[LoE3dWpTJPk8g8Ei.htm](pathfinder-bestiary-3-items/LoE3dWpTJPk8g8Ei.htm)|Create Water (At Will)|Création d'eau (À volonté)|libre|
|[LoLS2Gw8HzHFPWld.htm](pathfinder-bestiary-3-items/LoLS2Gw8HzHFPWld.htm)|Languages|Don des langues|libre|
|[LOMkNEBVrbQNC9tu.htm](pathfinder-bestiary-3-items/LOMkNEBVrbQNC9tu.htm)|Acrobatics|Acrobaties|libre|
|[LP9djZsLKMnkBo3M.htm](pathfinder-bestiary-3-items/LP9djZsLKMnkBo3M.htm)|Draconic Momentum|Impulsion draconique|libre|
|[lp9zpRImtn8gAWuk.htm](pathfinder-bestiary-3-items/lp9zpRImtn8gAWuk.htm)|Bloodsense (Imprecise) 90 feet|Perception du sang 18 m (imprécis)|libre|
|[lpbIToo47rzzxQRz.htm](pathfinder-bestiary-3-items/lpbIToo47rzzxQRz.htm)|Darkvision|Vision dans le noir|libre|
|[lPHcKkQiBqbtQQ8z.htm](pathfinder-bestiary-3-items/lPHcKkQiBqbtQQ8z.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[lPUDxg0sCW4m6DZX.htm](pathfinder-bestiary-3-items/lPUDxg0sCW4m6DZX.htm)|Dagger|Dague|libre|
|[lpvW88Zc5DItTR6J.htm](pathfinder-bestiary-3-items/lpvW88Zc5DItTR6J.htm)|Water Walk (Constant)|Marche sur l'eau (constant)|officielle|
|[lq5shiJkFR0pdE6O.htm](pathfinder-bestiary-3-items/lq5shiJkFR0pdE6O.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[lqiFGbLGpuC1xc0l.htm](pathfinder-bestiary-3-items/lqiFGbLGpuC1xc0l.htm)|Grab|Empoignade/Agrippement|libre|
|[lQKNHnyXpn3nHnzE.htm](pathfinder-bestiary-3-items/lQKNHnyXpn3nHnzE.htm)|Swarm Mind|Esprit de la nuée|libre|
|[LRFB4rNU7O66JVlQ.htm](pathfinder-bestiary-3-items/LRFB4rNU7O66JVlQ.htm)|Grab|Empoignade/Agrippement|libre|
|[LrGPFAVrLLeOkRJj.htm](pathfinder-bestiary-3-items/LrGPFAVrLLeOkRJj.htm)|Whisper Earworm|Rengaines murmurées|libre|
|[lRiN69RyGchL1AiD.htm](pathfinder-bestiary-3-items/lRiN69RyGchL1AiD.htm)|Speak with Animals (Constant)|Communication avec les animaux (constant)|officielle|
|[LrtLY1p0wVwq0afd.htm](pathfinder-bestiary-3-items/LrtLY1p0wVwq0afd.htm)|Jaws|Mâchoires|libre|
|[lsECqEvllEKtOv5g.htm](pathfinder-bestiary-3-items/lsECqEvllEKtOv5g.htm)|Chain Shot|Tir de chaîne|libre|
|[Lt0Vd3S5Hji79WEW.htm](pathfinder-bestiary-3-items/Lt0Vd3S5Hji79WEW.htm)|Draconic Frenzy|Frénésie draconique|libre|
|[LtMnRFGdEZ9u0QFh.htm](pathfinder-bestiary-3-items/LtMnRFGdEZ9u0QFh.htm)|Pluck Dream|Cueillir le rêve|libre|
|[lTNuGoh4u8QMy0pk.htm](pathfinder-bestiary-3-items/lTNuGoh4u8QMy0pk.htm)|Tendril|Vrille|libre|
|[lTytlP2GFTlmWEkk.htm](pathfinder-bestiary-3-items/lTytlP2GFTlmWEkk.htm)|Haste (At Will) (Self Only)|Rapidité (À volonté) (soi uniquement)|libre|
|[lufhjv4m2WdjSTZs.htm](pathfinder-bestiary-3-items/lufhjv4m2WdjSTZs.htm)|Countered by Water|Contré par l'eau|libre|
|[lUKDtZBh5MBovcUd.htm](pathfinder-bestiary-3-items/lUKDtZBh5MBovcUd.htm)|Negative Healing|Guérison négative|libre|
|[LUWAzCWdvMJPK5Am.htm](pathfinder-bestiary-3-items/LUWAzCWdvMJPK5Am.htm)|Hide and Seek|Cache-cache|libre|
|[lV8lCapoUOFDMPz8.htm](pathfinder-bestiary-3-items/lV8lCapoUOFDMPz8.htm)|Pukwudgie Poison|Poison pukwudgie|libre|
|[LVNb1MU1A4uFDYSo.htm](pathfinder-bestiary-3-items/LVNb1MU1A4uFDYSo.htm)|Push|Bousculade|officielle|
|[LVzeZlyXzpgBSv5s.htm](pathfinder-bestiary-3-items/LVzeZlyXzpgBSv5s.htm)|Jaws|Mâchoires|libre|
|[lwLwA2EvXhOPYIuA.htm](pathfinder-bestiary-3-items/lwLwA2EvXhOPYIuA.htm)|At-Will Spells|Sorts à volonté|libre|
|[lwULACd9cg2TzXz5.htm](pathfinder-bestiary-3-items/lwULACd9cg2TzXz5.htm)|Deep Breath|Inspiration profonde|libre|
|[Lx5Q366mof1O0MPp.htm](pathfinder-bestiary-3-items/Lx5Q366mof1O0MPp.htm)|Form Up|Se reformer|libre|
|[lxcQL19jOkUey7nF.htm](pathfinder-bestiary-3-items/lxcQL19jOkUey7nF.htm)|Blowgun|Sarbacane|libre|
|[lxK1cONnIVJsnFVA.htm](pathfinder-bestiary-3-items/lxK1cONnIVJsnFVA.htm)|Sandals|Sandales|libre|
|[lXlQ3g7mZu8LDrvB.htm](pathfinder-bestiary-3-items/lXlQ3g7mZu8LDrvB.htm)|Woodland Stride|Déplacement facilité en forêt|officielle|
|[lxWOOSDzdS3gKtgq.htm](pathfinder-bestiary-3-items/lxWOOSDzdS3gKtgq.htm)|Swarm Mind|Esprit de la nuée|libre|
|[LY9m5BMsAb8g3SIx.htm](pathfinder-bestiary-3-items/LY9m5BMsAb8g3SIx.htm)|Negative Healing|Guérison négative|libre|
|[lYRZM8I1flblujiU.htm](pathfinder-bestiary-3-items/lYRZM8I1flblujiU.htm)|Negative Healing|Guérison négative|libre|
|[LzvPd4UTa6iofTzD.htm](pathfinder-bestiary-3-items/LzvPd4UTa6iofTzD.htm)|Hoof|Sabot|libre|
|[lZYIDbiemHOhYhm5.htm](pathfinder-bestiary-3-items/lZYIDbiemHOhYhm5.htm)|Abandon Corpse|Abandon de cadavre|libre|
|[m03bX0sD03E32bCM.htm](pathfinder-bestiary-3-items/m03bX0sD03E32bCM.htm)|Constant Spells|Sorts constants|libre|
|[m0gDkd2xvDLRZxHw.htm](pathfinder-bestiary-3-items/m0gDkd2xvDLRZxHw.htm)|Invisibility (Self Only)|Invisibilité (soi uniquement)|officielle|
|[m0pBEDByK2E42m1L.htm](pathfinder-bestiary-3-items/m0pBEDByK2E42m1L.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[m1kfe61fI2Y0NgZd.htm](pathfinder-bestiary-3-items/m1kfe61fI2Y0NgZd.htm)|Darkvision|Vision dans le noir|libre|
|[M1p2Iq57MOBT9II9.htm](pathfinder-bestiary-3-items/M1p2Iq57MOBT9II9.htm)|Darkvision|Vision dans le noir|libre|
|[M1rzEefuLXlWlYN8.htm](pathfinder-bestiary-3-items/M1rzEefuLXlWlYN8.htm)|Colossus's Grasp|Poigne du Colosse|libre|
|[m2GqanJ6fVKX39SQ.htm](pathfinder-bestiary-3-items/m2GqanJ6fVKX39SQ.htm)|Grab|Empoignade/Agrippement|libre|
|[m2xn36z0bizgQRSl.htm](pathfinder-bestiary-3-items/m2xn36z0bizgQRSl.htm)|Polyglot|Polyglotte|libre|
|[M3azf7Pp6YDlxgN3.htm](pathfinder-bestiary-3-items/M3azf7Pp6YDlxgN3.htm)|Quick Escape|Échappatoire rapide|libre|
|[M3e5MWLAt9COPusW.htm](pathfinder-bestiary-3-items/M3e5MWLAt9COPusW.htm)|Tendril|Vrille|libre|
|[m3ZfC7sl35Uue2KZ.htm](pathfinder-bestiary-3-items/m3ZfC7sl35Uue2KZ.htm)|Low-Light Vision|Vision nocturne|libre|
|[M4gK880IlR5fkws0.htm](pathfinder-bestiary-3-items/M4gK880IlR5fkws0.htm)|Darkvision|Vision dans le noir|libre|
|[M4yZRTxw1DkV1j2m.htm](pathfinder-bestiary-3-items/M4yZRTxw1DkV1j2m.htm)|Grab|Empoignade/Agrippement|libre|
|[M68tcWaPYgJDf7pp.htm](pathfinder-bestiary-3-items/M68tcWaPYgJDf7pp.htm)|Launch|Catapultage|libre|
|[M6bPLFIXKoFFgM2u.htm](pathfinder-bestiary-3-items/M6bPLFIXKoFFgM2u.htm)|Darkvision|Vision dans le noir|libre|
|[M6HiBc42J8BI5rZo.htm](pathfinder-bestiary-3-items/M6HiBc42J8BI5rZo.htm)|Unstable Magic|Magie instable|libre|
|[M6L8pe2IvyC5P2i4.htm](pathfinder-bestiary-3-items/M6L8pe2IvyC5P2i4.htm)|Longspear|+1,striking|Pique de frappe +1|libre|
|[M6VhLwSsBbyN4CsJ.htm](pathfinder-bestiary-3-items/M6VhLwSsBbyN4CsJ.htm)|Greatpick|Grand pic de guerre|libre|
|[M7bSwayItCZu1H43.htm](pathfinder-bestiary-3-items/M7bSwayItCZu1H43.htm)|Crush Chitin|Brise-chitine|libre|
|[m8Ycgo6ZGdfthZSw.htm](pathfinder-bestiary-3-items/m8Ycgo6ZGdfthZSw.htm)|Sanguine Spray|Giclée de sang|libre|
|[M9CMfdS3cQy6u8eS.htm](pathfinder-bestiary-3-items/M9CMfdS3cQy6u8eS.htm)|Create Undead (No Secondary Caster Required)|Création de mort-vivant (aucun incantateur secondaire requis)|libre|
|[M9Gq6cRX2w40Q3Yb.htm](pathfinder-bestiary-3-items/M9Gq6cRX2w40Q3Yb.htm)|Darkvision|Vision dans le noir|libre|
|[mAl92vhU2YdKG3bV.htm](pathfinder-bestiary-3-items/mAl92vhU2YdKG3bV.htm)|Breath Weapon|Arme de souffle|libre|
|[maMkCYAHEvq6tAqg.htm](pathfinder-bestiary-3-items/maMkCYAHEvq6tAqg.htm)|Invisibility (At Will)|Invisibilité (À volonté)|officielle|
|[maPEoGc6nv9lMP5R.htm](pathfinder-bestiary-3-items/maPEoGc6nv9lMP5R.htm)|Fist|Poing|libre|
|[MAu9spTENeddU6Zv.htm](pathfinder-bestiary-3-items/MAu9spTENeddU6Zv.htm)|Foot|Pied|libre|
|[maVqItGv6VD6w8M3.htm](pathfinder-bestiary-3-items/maVqItGv6VD6w8M3.htm)|Roiling Rebuke|Réprimande agacée|libre|
|[MB3A4i2cdRbTv3vg.htm](pathfinder-bestiary-3-items/MB3A4i2cdRbTv3vg.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[MbX8MuX8mUeGVwXv.htm](pathfinder-bestiary-3-items/MbX8MuX8mUeGVwXv.htm)|Stunning Electricity|Électricité étourdissante|libre|
|[MCvCX567cQVCilQv.htm](pathfinder-bestiary-3-items/MCvCX567cQVCilQv.htm)|Claw|Griffe|libre|
|[MdAaSnMbM5KOAlWl.htm](pathfinder-bestiary-3-items/MdAaSnMbM5KOAlWl.htm)|Pummeling Assault|Rouée de coups|libre|
|[MDEHgLAgesfgrGrt.htm](pathfinder-bestiary-3-items/MDEHgLAgesfgrGrt.htm)|Low-Light Vision|Vision nocturne|libre|
|[MDqLlKyAsgRUfism.htm](pathfinder-bestiary-3-items/MDqLlKyAsgRUfism.htm)|Constant Spells|Sorts constants|libre|
|[mEcsR2Hq0xwLS5m1.htm](pathfinder-bestiary-3-items/mEcsR2Hq0xwLS5m1.htm)|Shadow Whip|Fouet d'ombre|libre|
|[MEdTOOe1fbIYRy1N.htm](pathfinder-bestiary-3-items/MEdTOOe1fbIYRy1N.htm)|Jaws|Mâchoires|libre|
|[mep6FodvFU5OWGuk.htm](pathfinder-bestiary-3-items/mep6FodvFU5OWGuk.htm)|Consecration Vulnerability|Vulnérable à la consécration|libre|
|[Mf6HjC4fVjp3RLPm.htm](pathfinder-bestiary-3-items/Mf6HjC4fVjp3RLPm.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[mfooCk9alzoismP0.htm](pathfinder-bestiary-3-items/mfooCk9alzoismP0.htm)|Grab|Empoignade/Agrippement|libre|
|[mfQZq4ONY5lVM7IX.htm](pathfinder-bestiary-3-items/mfQZq4ONY5lVM7IX.htm)|Constant Spells|Sorts constants|libre|
|[MfS2aGB9sp6OgznC.htm](pathfinder-bestiary-3-items/MfS2aGB9sp6OgznC.htm)|Claw|Griffe|libre|
|[Mfyiq7MrkDXVAE1n.htm](pathfinder-bestiary-3-items/Mfyiq7MrkDXVAE1n.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[MGarveOJoEZEZRDb.htm](pathfinder-bestiary-3-items/MGarveOJoEZEZRDb.htm)|Vine Forest|Forêt de vrille|libre|
|[mGFhVUVLfuBvJdR9.htm](pathfinder-bestiary-3-items/mGFhVUVLfuBvJdR9.htm)|Fist|Poing|libre|
|[MGFv64yTRt7JOqf8.htm](pathfinder-bestiary-3-items/MGFv64yTRt7JOqf8.htm)|+1 Striking Felt Shears (as Dagger)|+1,striking|Ciseaux de couture de frappe +1 (comme la dague de frappe +1)|libre|
|[MgZCPERomWYyl1Nh.htm](pathfinder-bestiary-3-items/MgZCPERomWYyl1Nh.htm)|Inspire Envoy|Inspiration de l'émissaire|libre|
|[MhafIYjoiiUIlHV9.htm](pathfinder-bestiary-3-items/MhafIYjoiiUIlHV9.htm)|Darkvision|Vision dans le noir|libre|
|[MHEumHsZqQqW917y.htm](pathfinder-bestiary-3-items/MHEumHsZqQqW917y.htm)|Arcane Innate Spells|Sorts arcaniques innés|libre|
|[MhmAMaJ3NhpSRreS.htm](pathfinder-bestiary-3-items/MhmAMaJ3NhpSRreS.htm)|Girtablilu Venom|Venin de Girtablilu|libre|
|[MHmU7Zg92WbTJ7CF.htm](pathfinder-bestiary-3-items/MHmU7Zg92WbTJ7CF.htm)|Calm Emotions (At Will)|Apaisement des émotions (À volonté)|officielle|
|[MHWhouO72rbCG3vP.htm](pathfinder-bestiary-3-items/MHWhouO72rbCG3vP.htm)|Whip Tail|Claquement de queue|libre|
|[MIgppLmIY2AofKTC.htm](pathfinder-bestiary-3-items/MIgppLmIY2AofKTC.htm)|Swarming Chimes|Nuée de carillons|libre|
|[Mj3sDpLmMbVTOv59.htm](pathfinder-bestiary-3-items/Mj3sDpLmMbVTOv59.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[MjldvRpasuQZnNAa.htm](pathfinder-bestiary-3-items/MjldvRpasuQZnNAa.htm)|Absorbed Language|Langue absorbée|libre|
|[mjNu7tTMIDpyD02w.htm](pathfinder-bestiary-3-items/mjNu7tTMIDpyD02w.htm)|Foot|Pied|libre|
|[mjVHIWJqPBBMeBhy.htm](pathfinder-bestiary-3-items/mjVHIWJqPBBMeBhy.htm)|Talon|Serre|libre|
|[mk57CVzb2MTvBoCX.htm](pathfinder-bestiary-3-items/mk57CVzb2MTvBoCX.htm)|At-Will Spells|Sorts à volonté|libre|
|[MKArQIqeRcZmRwMf.htm](pathfinder-bestiary-3-items/MKArQIqeRcZmRwMf.htm)|Faith Bound|Lié à la foi|libre|
|[mKGOHDAfBouobqfT.htm](pathfinder-bestiary-3-items/mKGOHDAfBouobqfT.htm)|Darkness (At Will)|Ténèbres (À volonté)|officielle|
|[mkOmRandYut3CQzA.htm](pathfinder-bestiary-3-items/mkOmRandYut3CQzA.htm)|Spade|Pelle|libre|
|[mksu92xcObxdYZwg.htm](pathfinder-bestiary-3-items/mksu92xcObxdYZwg.htm)|Rend Faith|Déchirement de la foi|libre|
|[MLb6AX5Sc9kHDdik.htm](pathfinder-bestiary-3-items/MLb6AX5Sc9kHDdik.htm)|Ranseur|Corsèque|libre|
|[mLIM2js0SKePsRgt.htm](pathfinder-bestiary-3-items/mLIM2js0SKePsRgt.htm)|Shortsword|Épée courte|libre|
|[mLT5MDiW1VnsaeKk.htm](pathfinder-bestiary-3-items/mLT5MDiW1VnsaeKk.htm)|Breath Weapon|Arme de souffle|libre|
|[mM2VhVOnYEHZ9vNy.htm](pathfinder-bestiary-3-items/mM2VhVOnYEHZ9vNy.htm)|Ooze Globule|Bulle de vase|libre|
|[MN3PR10lW2gEDJPP.htm](pathfinder-bestiary-3-items/MN3PR10lW2gEDJPP.htm)|Swallow Whole|Gober|libre|
|[MnCWApQWsNq3LZGf.htm](pathfinder-bestiary-3-items/MnCWApQWsNq3LZGf.htm)|Rapid Evolution|Évolution rapide|libre|
|[mnF1BUlA5VyNQsJJ.htm](pathfinder-bestiary-3-items/mnF1BUlA5VyNQsJJ.htm)|At-Will Spells|Sorts à volonté|libre|
|[Mnk4wTPNqIwrijFP.htm](pathfinder-bestiary-3-items/Mnk4wTPNqIwrijFP.htm)|Curse of Darkness|Malédiction de ténèbres|libre|
|[mnmEm4rMTXMCnOlp.htm](pathfinder-bestiary-3-items/mnmEm4rMTXMCnOlp.htm)|Shuriken|Shuriken|libre|
|[mNMGvHwMrDdtnVoO.htm](pathfinder-bestiary-3-items/mNMGvHwMrDdtnVoO.htm)|Fast Swallow|Gober rapidement|libre|
|[mo4QicMXhbgaEdej.htm](pathfinder-bestiary-3-items/mo4QicMXhbgaEdej.htm)|Stealth|Discrétion|libre|
|[mODjcKhuMNY9CZD6.htm](pathfinder-bestiary-3-items/mODjcKhuMNY9CZD6.htm)|Trample|Piétinement|libre|
|[MODzmDCzn3MDbT9X.htm](pathfinder-bestiary-3-items/MODzmDCzn3MDbT9X.htm)|Negative Healing|Guérison négative|libre|
|[moI2lATWRRsnrF0Z.htm](pathfinder-bestiary-3-items/moI2lATWRRsnrF0Z.htm)|Raise Shields|Lever un bouclier|libre|
|[MoM2zK3F63zlLyW3.htm](pathfinder-bestiary-3-items/MoM2zK3F63zlLyW3.htm)|Venom Spritz|Giclée de venin|libre|
|[MOMA8yyykZNLQhay.htm](pathfinder-bestiary-3-items/MOMA8yyykZNLQhay.htm)|+2 Status to All Saves vs. Magic|bonus de statut de +2 aux JdS contre la magie|libre|
|[mOQO1nFke9ZyiXTD.htm](pathfinder-bestiary-3-items/mOQO1nFke9ZyiXTD.htm)|Mending (At Will)|Réparation (À volonté)|libre|
|[MoqxDoRDqZhJhZwN.htm](pathfinder-bestiary-3-items/MoqxDoRDqZhJhZwN.htm)|Troop Movement|Mouvement de troupe|libre|
|[MOxpvxjnhMCu9QIM.htm](pathfinder-bestiary-3-items/MOxpvxjnhMCu9QIM.htm)|Darkvision|Vision dans le noir|libre|
|[mpDohzU1L0FfV4Tr.htm](pathfinder-bestiary-3-items/mpDohzU1L0FfV4Tr.htm)|Toxic Blood|Sang toxique|libre|
|[MPHAiW7bYkK3AAtF.htm](pathfinder-bestiary-3-items/MPHAiW7bYkK3AAtF.htm)|Millinery Lore|Connaissance de la chapellerie|libre|
|[MpmCmnbV6vrc9yPG.htm](pathfinder-bestiary-3-items/MpmCmnbV6vrc9yPG.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[MPnS8zod0eSdIWLP.htm](pathfinder-bestiary-3-items/MPnS8zod0eSdIWLP.htm)|Mist Vision|Vision malgré la brume|libre|
|[Mq1buiTxEF2rhIx8.htm](pathfinder-bestiary-3-items/Mq1buiTxEF2rhIx8.htm)|Weep|Pleurs|libre|
|[MqBEcHEyj2aKh7uO.htm](pathfinder-bestiary-3-items/MqBEcHEyj2aKh7uO.htm)|Darkvision|Vision dans le noir|libre|
|[mqJmVUehvZOB9E80.htm](pathfinder-bestiary-3-items/mqJmVUehvZOB9E80.htm)|Null Spirit|Insensible aux esprits|libre|
|[mqzpWjry4YKks5Hz.htm](pathfinder-bestiary-3-items/mqzpWjry4YKks5Hz.htm)|Darkvision|Vision dans le noir|libre|
|[mR1hECOdG4ihzUpu.htm](pathfinder-bestiary-3-items/mR1hECOdG4ihzUpu.htm)|Drink Blood|Boire le sang|libre|
|[mr3JOEATvs2okn6i.htm](pathfinder-bestiary-3-items/mr3JOEATvs2okn6i.htm)|Slippery Grease|Graisse glissante|libre|
|[MRHUn0OVB4n1HA2L.htm](pathfinder-bestiary-3-items/MRHUn0OVB4n1HA2L.htm)|Sudden Charge|Charge soudaine|libre|
|[MS8VRrrPU2Us5eB9.htm](pathfinder-bestiary-3-items/MS8VRrrPU2Us5eB9.htm)|Shield Block|Blocage au bouclier|libre|
|[MtmIGyh1mEwfrLk8.htm](pathfinder-bestiary-3-items/MtmIGyh1mEwfrLk8.htm)|Ventriloquism (At Will)|Ventriloquie (À volonté)|officielle|
|[MtxKrGdtCxm0wczN.htm](pathfinder-bestiary-3-items/MtxKrGdtCxm0wczN.htm)|Phase Lurch|Marche déphasée|libre|
|[MUIAw06V4IdebBMA.htm](pathfinder-bestiary-3-items/MUIAw06V4IdebBMA.htm)|Construct Armor (Hardness 8)|Armure de créature artificielle|libre|
|[MUxEI1u92ZQGjdB8.htm](pathfinder-bestiary-3-items/MUxEI1u92ZQGjdB8.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[MvqE0WoDiotm6xOj.htm](pathfinder-bestiary-3-items/MvqE0WoDiotm6xOj.htm)|Gust of Wind (At Will)|Bourrasque de vent (À volonté)|officielle|
|[MvzDVPMtmss6IZMj.htm](pathfinder-bestiary-3-items/MvzDVPMtmss6IZMj.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[mw396KKLKWJKNSqQ.htm](pathfinder-bestiary-3-items/mw396KKLKWJKNSqQ.htm)|Jaws|Mâchoires|libre|
|[MWJz2HDAK5IGRX8W.htm](pathfinder-bestiary-3-items/MWJz2HDAK5IGRX8W.htm)|Darkvision|Vision dans le noir|libre|
|[MWlueD3tLQnzSbjz.htm](pathfinder-bestiary-3-items/MWlueD3tLQnzSbjz.htm)|Sneak Attack|Attaque d'opportunité|libre|
|[mx7M2JUGpqZUL4LJ.htm](pathfinder-bestiary-3-items/mx7M2JUGpqZUL4LJ.htm)|Projectile Vomit|Projectile de vomi|libre|
|[mX7yAH39yBUykxli.htm](pathfinder-bestiary-3-items/mX7yAH39yBUykxli.htm)|Darkvision|Vision dans le noir|libre|
|[MYdtzKzZtoPS3j70.htm](pathfinder-bestiary-3-items/MYdtzKzZtoPS3j70.htm)|Tail|Queue|libre|
|[mYKiwGxlqb75Vo6G.htm](pathfinder-bestiary-3-items/mYKiwGxlqb75Vo6G.htm)|Corrupting Touch|Toucher corrupteur|libre|
|[Mynf5Z4xbH7kVwpi.htm](pathfinder-bestiary-3-items/Mynf5Z4xbH7kVwpi.htm)|Spit|Crachat|libre|
|[MYOFzFbpGCWFCD85.htm](pathfinder-bestiary-3-items/MYOFzFbpGCWFCD85.htm)|Constrict|Constriction|libre|
|[MzHIjOVqhOXnluws.htm](pathfinder-bestiary-3-items/MzHIjOVqhOXnluws.htm)|Extinguishing Aversion|Aversion à l'extinction|libre|
|[MzIjuOCLnKBWgj5w.htm](pathfinder-bestiary-3-items/MzIjuOCLnKBWgj5w.htm)|Claw|Griffe|libre|
|[MZrswDwhii9LR4mL.htm](pathfinder-bestiary-3-items/MZrswDwhii9LR4mL.htm)|-2 to All Saves vs. Emotion Effects|Malus de -2 à tous les jets de sauvegarde contre les effets d'émotion|libre|
|[n0IdU8aHDRaDIoOA.htm](pathfinder-bestiary-3-items/n0IdU8aHDRaDIoOA.htm)|At-Will Spells|Sorts à volonté|libre|
|[N0j4xFqLJ9FtXRAP.htm](pathfinder-bestiary-3-items/N0j4xFqLJ9FtXRAP.htm)|Tremorsense (Imprecise) 60 feet|Perception des vibrations 18 m (imprécis)|libre|
|[N0L60ChJeeVdQogn.htm](pathfinder-bestiary-3-items/N0L60ChJeeVdQogn.htm)|All-Around Vision|Vision panoramique|libre|
|[N0n5SsuqfJHWLi1E.htm](pathfinder-bestiary-3-items/N0n5SsuqfJHWLi1E.htm)|Claw|Griffe|libre|
|[N0pERoSKEmaLOJSB.htm](pathfinder-bestiary-3-items/N0pERoSKEmaLOJSB.htm)|Leech Essence|Absorption d'essence|libre|
|[n249UcrNORZw28Hg.htm](pathfinder-bestiary-3-items/n249UcrNORZw28Hg.htm)|Swarm Mind|Esprit de la nuée|libre|
|[N2oBeSLsFrTuwOTn.htm](pathfinder-bestiary-3-items/N2oBeSLsFrTuwOTn.htm)|Frightening Flurry|Déluge de terreur|libre|
|[N2Qsn1VaKlDp82Ga.htm](pathfinder-bestiary-3-items/N2Qsn1VaKlDp82Ga.htm)|Scalding Oil|Huile brûlante|libre|
|[N4NhBjgFg8WdpS84.htm](pathfinder-bestiary-3-items/N4NhBjgFg8WdpS84.htm)|Negative Healing|Guérison négative|libre|
|[N6eFdrrSCUkH7776.htm](pathfinder-bestiary-3-items/N6eFdrrSCUkH7776.htm)|Misdirection (At Will) (Self Only)|Détection faussée (À volonté) (soi uniquement)|libre|
|[N6z6E7Ly3t7eFBoU.htm](pathfinder-bestiary-3-items/N6z6E7Ly3t7eFBoU.htm)|Moon Frenzy|Frénésie lunaire|libre|
|[N7bXlw2VkUQPBHUy.htm](pathfinder-bestiary-3-items/N7bXlw2VkUQPBHUy.htm)|Scorch|Calciner|libre|
|[n7LgURaW8hihxzYn.htm](pathfinder-bestiary-3-items/n7LgURaW8hihxzYn.htm)|Phalanx Charge|Charge en phalange|libre|
|[n7ZxA7v0OJE04AGq.htm](pathfinder-bestiary-3-items/n7ZxA7v0OJE04AGq.htm)|Frightful Presence|Présence terrifiante|libre|
|[n8MOp28YVOpx7Eoq.htm](pathfinder-bestiary-3-items/n8MOp28YVOpx7Eoq.htm)|Flit Back|Voltige arrière|libre|
|[N8Z7KBCRIUBXL2Gu.htm](pathfinder-bestiary-3-items/N8Z7KBCRIUBXL2Gu.htm)|Darkvision|Vision dans le noir|libre|
|[n96wAvXJdhT8a2uD.htm](pathfinder-bestiary-3-items/n96wAvXJdhT8a2uD.htm)|Claw|Griffe|libre|
|[N9fBVLGGH7iHDjIs.htm](pathfinder-bestiary-3-items/N9fBVLGGH7iHDjIs.htm)|Darkvision|Vision dans le noir|libre|
|[N9uynDS2jAyjfbwR.htm](pathfinder-bestiary-3-items/N9uynDS2jAyjfbwR.htm)|Breath Weapon|Arme de souffle|libre|
|[Na4ZinpZPOodP1WY.htm](pathfinder-bestiary-3-items/Na4ZinpZPOodP1WY.htm)|Blighted Footfalls|Marche de destruction|libre|
|[NAngdrs8j255ZSod.htm](pathfinder-bestiary-3-items/NAngdrs8j255ZSod.htm)|Steal Memories|Vol de souvenirs|libre|
|[NaTbdbqoxFlKljoH.htm](pathfinder-bestiary-3-items/NaTbdbqoxFlKljoH.htm)|Tremorsense (Precise) 40 feet, (Imprecise) 80 feet|Perception 12 m (précis), 24 m (imprécis)|libre|
|[NbGq64tR4svPBmpM.htm](pathfinder-bestiary-3-items/NbGq64tR4svPBmpM.htm)|Darkvision|Vision dans le noir|libre|
|[Nc31MydJOBHcqpyb.htm](pathfinder-bestiary-3-items/Nc31MydJOBHcqpyb.htm)|Divine Dispelling|Dissipation du divin|libre|
|[Nc80ofsnO8nL7SBk.htm](pathfinder-bestiary-3-items/Nc80ofsnO8nL7SBk.htm)|Nondetection (Constant) (Self Only)|Antidétection (constant)(soi uniquement)|libre|
|[NCFt2ltSfGTesan4.htm](pathfinder-bestiary-3-items/NCFt2ltSfGTesan4.htm)|-2 to All Saves vs. Emotion Effects|Malus de -2 à tous les jets de sauvegarde contre les effets d'émotion|libre|
|[NCGFbhlnJHZ1rJp9.htm](pathfinder-bestiary-3-items/NCGFbhlnJHZ1rJp9.htm)|Clairvoyance (At Will)|Clairvoyance (À volonté)|libre|
|[ncklibzh6Dfp2LTc.htm](pathfinder-bestiary-3-items/ncklibzh6Dfp2LTc.htm)|Shield Block|Blocage au bouclier|libre|
|[nCkzzJiycKKevhSG.htm](pathfinder-bestiary-3-items/nCkzzJiycKKevhSG.htm)|Countered by Water|Contré par l'eau|libre|
|[ncL6q9VyNef5Kfov.htm](pathfinder-bestiary-3-items/ncL6q9VyNef5Kfov.htm)|Interpose|Interposition|libre|
|[NCoWkgKyY8nUdbrO.htm](pathfinder-bestiary-3-items/NCoWkgKyY8nUdbrO.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[NcpXTQwFwbyulXQM.htm](pathfinder-bestiary-3-items/NcpXTQwFwbyulXQM.htm)|Claw|Griffe|libre|
|[NDkFem8B3XVJXQkP.htm](pathfinder-bestiary-3-items/NDkFem8B3XVJXQkP.htm)|Cecaelia Jet|Propulsion du cécaëlia|libre|
|[ndQeCjr03ZZsI5o1.htm](pathfinder-bestiary-3-items/ndQeCjr03ZZsI5o1.htm)|Memory Maelstrom|Vortex mémoriel|libre|
|[nEE49kEL2IsSaPcv.htm](pathfinder-bestiary-3-items/nEE49kEL2IsSaPcv.htm)|Green Grab|Agrippement végétal|libre|
|[nEHCDw3LRtrCnVMz.htm](pathfinder-bestiary-3-items/nEHCDw3LRtrCnVMz.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[ner0C5vBZGTckOHF.htm](pathfinder-bestiary-3-items/ner0C5vBZGTckOHF.htm)|Obscuring Mist (At Will)|Brume de dissimulation (À volonté)|officielle|
|[neSR5XCiVlUBQSvT.htm](pathfinder-bestiary-3-items/neSR5XCiVlUBQSvT.htm)|At-Will Spells|Sorts à volonté|libre|
|[NewHXtJk8yORbKND.htm](pathfinder-bestiary-3-items/NewHXtJk8yORbKND.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[NFIsLlfq5D02gNol.htm](pathfinder-bestiary-3-items/NFIsLlfq5D02gNol.htm)|Grab|Empoignade/Agrippement|libre|
|[Ng4z9p0PvzMzN8Di.htm](pathfinder-bestiary-3-items/Ng4z9p0PvzMzN8Di.htm)|Kinsense|Perception des proches|libre|
|[NgeIHogL6m58BKDp.htm](pathfinder-bestiary-3-items/NgeIHogL6m58BKDp.htm)|Verdant Burst|Explosion verdoyante|libre|
|[ngkPL0GL8aCtZQ2t.htm](pathfinder-bestiary-3-items/ngkPL0GL8aCtZQ2t.htm)|Flaming Sword|Épée enflammée|libre|
|[nH4VBesV9iFOfCnq.htm](pathfinder-bestiary-3-items/nH4VBesV9iFOfCnq.htm)|Ink Cloud|Nuage d'encre|libre|
|[nHC6WGYqix3arWBf.htm](pathfinder-bestiary-3-items/nHC6WGYqix3arWBf.htm)|Feed on Sorrow|Renforcé par la tristesse|libre|
|[nhlXnY0WEq9xwOO7.htm](pathfinder-bestiary-3-items/nhlXnY0WEq9xwOO7.htm)|Darkvision|Vision dans le noir|libre|
|[NHqBLwHkFRahYskc.htm](pathfinder-bestiary-3-items/NHqBLwHkFRahYskc.htm)|Telepathy 30 feet|Télépathie 9 m|libre|
|[NHrIQILaWSM6emU0.htm](pathfinder-bestiary-3-items/NHrIQILaWSM6emU0.htm)|Low-Light Vision|Vision nocturne|libre|
|[nIiLVQf3htOx2fUg.htm](pathfinder-bestiary-3-items/nIiLVQf3htOx2fUg.htm)|Grab|Empoignade/Agrippement|libre|
|[nIqiNS5etJdv3J1R.htm](pathfinder-bestiary-3-items/nIqiNS5etJdv3J1R.htm)|Amulet|Amulette|libre|
|[nivZKXrJ6jbmI61w.htm](pathfinder-bestiary-3-items/nivZKXrJ6jbmI61w.htm)|Plane Shift (Self Only) (To Or From The Shadow Plane Only)|Changement de plan (soi uniquement, de ou vers le plan des Ombres uniquement)|libre|
|[niyS4ePBUKlm1nbJ.htm](pathfinder-bestiary-3-items/niyS4ePBUKlm1nbJ.htm)|Frightful Presence|Présence terrifiante|libre|
|[NizhFg0yQ725ETUd.htm](pathfinder-bestiary-3-items/NizhFg0yQ725ETUd.htm)|Titanic Grasp|Étreinte titanesque|libre|
|[njHt74BnuaBXWlru.htm](pathfinder-bestiary-3-items/njHt74BnuaBXWlru.htm)|Claw|Griffe|libre|
|[nJj3OATPJmrNMM3y.htm](pathfinder-bestiary-3-items/nJj3OATPJmrNMM3y.htm)|Inflating Rush|Poussée enflée|libre|
|[NJm2VdVQrwDn112D.htm](pathfinder-bestiary-3-items/NJm2VdVQrwDn112D.htm)|Shadow Shift|Manipulation des ombres|libre|
|[NKqgBeuuqEtFMoGN.htm](pathfinder-bestiary-3-items/NKqgBeuuqEtFMoGN.htm)|Dual Mind|Double esprit|libre|
|[nkZ7Ek2VIoobJ59S.htm](pathfinder-bestiary-3-items/nkZ7Ek2VIoobJ59S.htm)|Cooking Lore|Connaissance culinaire|officielle|
|[NlBTftnU3oQdbjtw.htm](pathfinder-bestiary-3-items/NlBTftnU3oQdbjtw.htm)|Low-Light Vision|Vision nocturne|libre|
|[nllsy0skrldw3leB.htm](pathfinder-bestiary-3-items/nllsy0skrldw3leB.htm)|Low-Light Vision|Vision nocturne|libre|
|[nLrqfymDMpj0e5mW.htm](pathfinder-bestiary-3-items/nLrqfymDMpj0e5mW.htm)|Stealth|Discrétion|libre|
|[NLwKm20SkugAa88n.htm](pathfinder-bestiary-3-items/NLwKm20SkugAa88n.htm)|Feign Death|Feindre la mort|libre|
|[NmGeqt2BxwfGod5Z.htm](pathfinder-bestiary-3-items/NmGeqt2BxwfGod5Z.htm)|Form Up|Se reformer|libre|
|[nmzcowdCvUQCrcqI.htm](pathfinder-bestiary-3-items/nmzcowdCvUQCrcqI.htm)|Scent (Imprecise) 60 feet|Odorat 18 m (imprécis)|libre|
|[Nn5E53ud3JgH2R3T.htm](pathfinder-bestiary-3-items/Nn5E53ud3JgH2R3T.htm)|Champion Focus Spells|Sorts focalisés de champion|libre|
|[nNhyC2WwCF4L4Lhd.htm](pathfinder-bestiary-3-items/nNhyC2WwCF4L4Lhd.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[nNJH2B7s8hfn3Tci.htm](pathfinder-bestiary-3-items/nNJH2B7s8hfn3Tci.htm)|Negative Healing|Guérison négative|libre|
|[NO4hZxcJ6X16f4Og.htm](pathfinder-bestiary-3-items/NO4hZxcJ6X16f4Og.htm)|Change Shape|Changement de forme|libre|
|[NO59VkQbOQIqZLrI.htm](pathfinder-bestiary-3-items/NO59VkQbOQIqZLrI.htm)|Saturated|Saturation|libre|
|[nOiQxEVLn3tVCv9I.htm](pathfinder-bestiary-3-items/nOiQxEVLn3tVCv9I.htm)|Monastic Lore|Connaissance monastique|libre|
|[np15xTTViDna2Ngj.htm](pathfinder-bestiary-3-items/np15xTTViDna2Ngj.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[Np1YXLEjDe9xa1Qh.htm](pathfinder-bestiary-3-items/Np1YXLEjDe9xa1Qh.htm)|Darkvision|Vision dans le noir|libre|
|[NqfD7fDgNcg0SGu9.htm](pathfinder-bestiary-3-items/NqfD7fDgNcg0SGu9.htm)|True Seeing (Constant)|Vision lucide (constant)|officielle|
|[NqfZq31gduTX8PMN.htm](pathfinder-bestiary-3-items/NqfZq31gduTX8PMN.htm)|Thorn|Épine|libre|
|[nr0PMLn9MREOWXYD.htm](pathfinder-bestiary-3-items/nr0PMLn9MREOWXYD.htm)|Tongues (Constant)|Don des langues (constant)|officielle|
|[nr5RYRA0lNXTCCfR.htm](pathfinder-bestiary-3-items/nr5RYRA0lNXTCCfR.htm)|Ram Charge|Charge du bélier|libre|
|[nRcZT1xJaPAlURaU.htm](pathfinder-bestiary-3-items/nRcZT1xJaPAlURaU.htm)|Even Now?|Et maintenant ?|libre|
|[NrIJRYd3U38RpP6w.htm](pathfinder-bestiary-3-items/NrIJRYd3U38RpP6w.htm)|Fool's Gold|Or des fous|libre|
|[nROFFDEy5y0nKYS0.htm](pathfinder-bestiary-3-items/nROFFDEy5y0nKYS0.htm)|Suggestion (At Will)|Suggestion (À volonté)|officielle|
|[nrXR81qrvYlS1etB.htm](pathfinder-bestiary-3-items/nrXR81qrvYlS1etB.htm)|Chill Touch (Undead Only)|Contact glacial (morts-vivants uniquement)|libre|
|[Nsjcg03W4x2ElnCc.htm](pathfinder-bestiary-3-items/Nsjcg03W4x2ElnCc.htm)|Jaws|Mâchoires|libre|
|[Nskz1Z4716xdGyZy.htm](pathfinder-bestiary-3-items/Nskz1Z4716xdGyZy.htm)|Rock|Rocher|libre|
|[NsmFyn1MUaCrkX52.htm](pathfinder-bestiary-3-items/NsmFyn1MUaCrkX52.htm)|Frozen Weapons|Armes gelées|libre|
|[NsMzxbtDP9JAooC6.htm](pathfinder-bestiary-3-items/NsMzxbtDP9JAooC6.htm)|Bond in Light|Lien de lumière|libre|
|[NsrDJM8cD8NU8iJt.htm](pathfinder-bestiary-3-items/NsrDJM8cD8NU8iJt.htm)|Grioth Venom|Venin grioth|libre|
|[nTIiE8RjvvPcUYEK.htm](pathfinder-bestiary-3-items/nTIiE8RjvvPcUYEK.htm)|Rend|Éventration|libre|
|[NtoApZINNb0XOSCK.htm](pathfinder-bestiary-3-items/NtoApZINNb0XOSCK.htm)|Positive Energy Transfer|Transfert d'énergie positive|libre|
|[nul98z8Tuin8RNul.htm](pathfinder-bestiary-3-items/nul98z8Tuin8RNul.htm)|Low-Light Vision|Vision nocturne|libre|
|[NunhObJDovYnRhq5.htm](pathfinder-bestiary-3-items/NunhObJDovYnRhq5.htm)|Claw|Griffe|libre|
|[nVF38zJQgYheOlaQ.htm](pathfinder-bestiary-3-items/nVF38zJQgYheOlaQ.htm)|Snatch Skull|Attrape-crâne|libre|
|[Nvg6G4rwFK7be0Td.htm](pathfinder-bestiary-3-items/Nvg6G4rwFK7be0Td.htm)|Claw|Griffe|libre|
|[nVOb5B6Sn8JM7NDn.htm](pathfinder-bestiary-3-items/nVOb5B6Sn8JM7NDn.htm)|Constant Spells|Sorts constants|libre|
|[NvRpRxRlhF2Xz2bB.htm](pathfinder-bestiary-3-items/NvRpRxRlhF2Xz2bB.htm)|Touch|Contact|libre|
|[NweZyZVjfaAj87xq.htm](pathfinder-bestiary-3-items/NweZyZVjfaAj87xq.htm)|Nirvana Lore|Connaissance du Nirvana|libre|
|[Nwuz4zQLB3u2ujwO.htm](pathfinder-bestiary-3-items/Nwuz4zQLB3u2ujwO.htm)|Tail|Queue|libre|
|[NWXRpTU3jhjVFz93.htm](pathfinder-bestiary-3-items/NWXRpTU3jhjVFz93.htm)|Death Gasp|Soupir de mort|libre|
|[NX0pJgKM21qtQACy.htm](pathfinder-bestiary-3-items/NX0pJgKM21qtQACy.htm)|Stench|Puanteur|libre|
|[Nxg7iUxwWhxMhySE.htm](pathfinder-bestiary-3-items/Nxg7iUxwWhxMhySE.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[nXjrQb5q4kAXIS61.htm](pathfinder-bestiary-3-items/nXjrQb5q4kAXIS61.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[nXUfkwitBNWhIe8d.htm](pathfinder-bestiary-3-items/nXUfkwitBNWhIe8d.htm)|Fossilization|Fossilisation|libre|
|[NxYSa8z9vgTVhuCT.htm](pathfinder-bestiary-3-items/NxYSa8z9vgTVhuCT.htm)|Scent (Imprecise) 60 feet|Odorat 18 m (imprécis)|libre|
|[NY9oPlAKUHSpnJ52.htm](pathfinder-bestiary-3-items/NY9oPlAKUHSpnJ52.htm)|Darkvision|Vision dans le noir|libre|
|[NYACujLILqXuexgZ.htm](pathfinder-bestiary-3-items/NYACujLILqXuexgZ.htm)|Light Blindness|Aveuglé par la lumière|officielle|
|[NZUOo1WmtomF3Pyf.htm](pathfinder-bestiary-3-items/NZUOo1WmtomF3Pyf.htm)|Swarming Gnaw|Rongement de nuée|libre|
|[o11nVdIh4C9R8NHS.htm](pathfinder-bestiary-3-items/o11nVdIh4C9R8NHS.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[O1nBRrEFZ3T6AQYn.htm](pathfinder-bestiary-3-items/O1nBRrEFZ3T6AQYn.htm)|Attack of Opportunity (Tail Only)|Attaque d'opportunité (Queue uniqment)|libre|
|[o1xm0AkoMLLKUiqH.htm](pathfinder-bestiary-3-items/o1xm0AkoMLLKUiqH.htm)|Claw|Griffe|libre|
|[O36hrH8CJRLhvwrI.htm](pathfinder-bestiary-3-items/O36hrH8CJRLhvwrI.htm)|Temporal Sense|Perception temporelle|libre|
|[o3fl8ed8HrHnV97F.htm](pathfinder-bestiary-3-items/o3fl8ed8HrHnV97F.htm)|Longbow|Arc long|libre|
|[O4OD37pDdJ7DDfXC.htm](pathfinder-bestiary-3-items/O4OD37pDdJ7DDfXC.htm)|Staff|+2,striking|Bâton de frappe +2|libre|
|[O4uO0WyOyJlypiSX.htm](pathfinder-bestiary-3-items/O4uO0WyOyJlypiSX.htm)|Charm (Undead Only)|Charme (mort-vivant uniquement)|libre|
|[O5TqNDUtiJmWEbpZ.htm](pathfinder-bestiary-3-items/O5TqNDUtiJmWEbpZ.htm)|Desert Stride|Déplacement facilité dans le désert|libre|
|[O5Ub4Pr4nhdrG6Xy.htm](pathfinder-bestiary-3-items/O5Ub4Pr4nhdrG6Xy.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[o5UbC8XEchUb9DrO.htm](pathfinder-bestiary-3-items/o5UbC8XEchUb9DrO.htm)|Darkvision|Vision dans le noir|libre|
|[O5YGoCAO4nuXA6Pb.htm](pathfinder-bestiary-3-items/O5YGoCAO4nuXA6Pb.htm)|Ganzi Resistance|Résistance ganzi|libre|
|[o681r0ZsSIWthmiQ.htm](pathfinder-bestiary-3-items/o681r0ZsSIWthmiQ.htm)|Nimble Stride|Marche agile|libre|
|[o6IceHXhizmbsddI.htm](pathfinder-bestiary-3-items/o6IceHXhizmbsddI.htm)|Invisibility (Self Only)|Invisibilité (soi uniquement)|officielle|
|[o6pEQo5KbjxyW7Bj.htm](pathfinder-bestiary-3-items/o6pEQo5KbjxyW7Bj.htm)|Trample|Piétinement|libre|
|[o754wI61VUPIBAZy.htm](pathfinder-bestiary-3-items/o754wI61VUPIBAZy.htm)|Grab|Empoignade/Agrippement|libre|
|[O7GCOmC1ewn8Hglr.htm](pathfinder-bestiary-3-items/O7GCOmC1ewn8Hglr.htm)|Claw|Griffe|libre|
|[O7lGGxeUoi47EMD0.htm](pathfinder-bestiary-3-items/O7lGGxeUoi47EMD0.htm)|Fear (At Will)|Terreur (À volonté)|officielle|
|[o7YwycWP8YFceUCM.htm](pathfinder-bestiary-3-items/o7YwycWP8YFceUCM.htm)|Darkvision|Vision dans le noir|libre|
|[o9bdzGDtUTzmMtAv.htm](pathfinder-bestiary-3-items/o9bdzGDtUTzmMtAv.htm)|Tendril|Vrille|libre|
|[o9gWtjSqFLRnOVk2.htm](pathfinder-bestiary-3-items/o9gWtjSqFLRnOVk2.htm)|Claw|Griffe|libre|
|[O9KVtNjS6f57XAHS.htm](pathfinder-bestiary-3-items/O9KVtNjS6f57XAHS.htm)|Construct Armor (Hardness 14)|Armure de créature artificielle (solidité 14)|officielle|
|[o9sNXlscXUyhKlOK.htm](pathfinder-bestiary-3-items/o9sNXlscXUyhKlOK.htm)|Blatant Liar|Menteur invétéré|libre|
|[oaeEm5dS1vLarcaG.htm](pathfinder-bestiary-3-items/oaeEm5dS1vLarcaG.htm)|Whip Drain|Fouet draineur|libre|
|[oAwLdSvl1lwzYmYb.htm](pathfinder-bestiary-3-items/oAwLdSvl1lwzYmYb.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[oB4SNmUD0LdtVMO3.htm](pathfinder-bestiary-3-items/oB4SNmUD0LdtVMO3.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[oB6zbIt1d6nbCIXg.htm](pathfinder-bestiary-3-items/oB6zbIt1d6nbCIXg.htm)|Breath Weapon|Arme de souffle|libre|
|[obpFkh9f0LWUmFG2.htm](pathfinder-bestiary-3-items/obpFkh9f0LWUmFG2.htm)|Frightful Presence|Présence terrifiante|libre|
|[ObwEq85BpNcwAeik.htm](pathfinder-bestiary-3-items/ObwEq85BpNcwAeik.htm)|Tongues (Constant)|Don des langues (constant)|officielle|
|[oBzXzmE3ZsGSx8r0.htm](pathfinder-bestiary-3-items/oBzXzmE3ZsGSx8r0.htm)|Darkvision|Vision dans le noir|libre|
|[OC5iW5RWtphm9Mby.htm](pathfinder-bestiary-3-items/OC5iW5RWtphm9Mby.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|libre|
|[ocvnX6iZ7cCsqHIf.htm](pathfinder-bestiary-3-items/ocvnX6iZ7cCsqHIf.htm)|Low-Light Vision|Vision nocturne|libre|
|[OcXCtZyWYIL0Reqo.htm](pathfinder-bestiary-3-items/OcXCtZyWYIL0Reqo.htm)|True Seeing (Constant)|Vision lucide (constant)|officielle|
|[OCyAo1qV9dwYv7Ul.htm](pathfinder-bestiary-3-items/OCyAo1qV9dwYv7Ul.htm)|No Breath|Ne respire pas|libre|
|[oD3DnNMV4dQa3Gss.htm](pathfinder-bestiary-3-items/oD3DnNMV4dQa3Gss.htm)|Constant Spells|Sorts constants|libre|
|[ODB8H9ordvDj9MK8.htm](pathfinder-bestiary-3-items/ODB8H9ordvDj9MK8.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[oeWCnT69F8n5CEeg.htm](pathfinder-bestiary-3-items/oeWCnT69F8n5CEeg.htm)|Plane Shift (Self Only)|Changement de plan (soi uniquement)|libre|
|[oeXGUL3qzPHQgzrW.htm](pathfinder-bestiary-3-items/oeXGUL3qzPHQgzrW.htm)|Slime Trap|Piège visqueux|libre|
|[OeXYKvZIjZ3ny4IS.htm](pathfinder-bestiary-3-items/OeXYKvZIjZ3ny4IS.htm)|Glyph of Warding (At Will)|Glyphe de garde (À volonté)|libre|
|[OEzKykNunY5znzf7.htm](pathfinder-bestiary-3-items/OEzKykNunY5znzf7.htm)|Claw|Griffe|libre|
|[OflSSMFtOIlEpn58.htm](pathfinder-bestiary-3-items/OflSSMFtOIlEpn58.htm)|Elegant Cane (As Mace)|Canne élégante (comme une masse)|libre|
|[ofXdILv2twbnZo5Z.htm](pathfinder-bestiary-3-items/ofXdILv2twbnZo5Z.htm)|Claw|Griffe|libre|
|[Og5zTgT13s8QIGgb.htm](pathfinder-bestiary-3-items/Og5zTgT13s8QIGgb.htm)|Ram Charge|Charge du bélier|libre|
|[OgFZ82Cgk6qKkD9l.htm](pathfinder-bestiary-3-items/OgFZ82Cgk6qKkD9l.htm)|Aim as One|Viser comme un seul homme|libre|
|[oGImpwgKAU80AN98.htm](pathfinder-bestiary-3-items/oGImpwgKAU80AN98.htm)|Faceless|Sans visage|libre|
|[OH7aLLnIFKP4SaLG.htm](pathfinder-bestiary-3-items/OH7aLLnIFKP4SaLG.htm)|Sneak Attack|Attaque sournoise|libre|
|[OHawqyo3r0zsutMI.htm](pathfinder-bestiary-3-items/OHawqyo3r0zsutMI.htm)|Shortsword|+1,striking|Épée courte de frappe +1|officielle|
|[ohHcoAMXMCHN6Scv.htm](pathfinder-bestiary-3-items/ohHcoAMXMCHN6Scv.htm)|Beak|Bec|libre|
|[OhLCOozHQSHlWn0N.htm](pathfinder-bestiary-3-items/OhLCOozHQSHlWn0N.htm)|True Seeing (Constant)|Vision lucide (constant)|officielle|
|[ohqrq2d7d3If0s76.htm](pathfinder-bestiary-3-items/ohqrq2d7d3If0s76.htm)|Club|Gourdin|libre|
|[oHw8udhGCS0SJgQT.htm](pathfinder-bestiary-3-items/oHw8udhGCS0SJgQT.htm)|Hold Still|Rester immobile|libre|
|[OhwcTonDsZ7QOZTR.htm](pathfinder-bestiary-3-items/OhwcTonDsZ7QOZTR.htm)|Smoke Vision|Vision malgré la fumée|officielle|
|[OIExxzLzZHeopXKg.htm](pathfinder-bestiary-3-items/OIExxzLzZHeopXKg.htm)|Darkvision|Vision dans le noir|libre|
|[oil97jTz67aGZk18.htm](pathfinder-bestiary-3-items/oil97jTz67aGZk18.htm)|Tongues (Constant)|Don des langues (constant)|officielle|
|[OiznKRNhVSCejM5r.htm](pathfinder-bestiary-3-items/OiznKRNhVSCejM5r.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[Oj448zcLgfmVIFy1.htm](pathfinder-bestiary-3-items/Oj448zcLgfmVIFy1.htm)|Scent (Imprecise) 60 feet|Odorat 18 m (imprécis)|libre|
|[oj5hseYYTFuq4OYh.htm](pathfinder-bestiary-3-items/oj5hseYYTFuq4OYh.htm)|Darkvision|Vision dans le noir|libre|
|[ojGq55t3EY5AHaLi.htm](pathfinder-bestiary-3-items/ojGq55t3EY5AHaLi.htm)|Scent (Imprecise) 60 feet|Odorat 18 m (imprécis)|libre|
|[OjOzwh1AVQ8baTZt.htm](pathfinder-bestiary-3-items/OjOzwh1AVQ8baTZt.htm)|Gleaming Armor|Armure brillante|libre|
|[ojRftryhvZxryakY.htm](pathfinder-bestiary-3-items/ojRftryhvZxryakY.htm)|Godslayer|Assassin du divin|libre|
|[oJXCaHxdH67r6d1I.htm](pathfinder-bestiary-3-items/oJXCaHxdH67r6d1I.htm)|Inhabit Vessel|Possession du récipient|libre|
|[OK5mcs3GjpZRifx9.htm](pathfinder-bestiary-3-items/OK5mcs3GjpZRifx9.htm)|Jaws|Mâchoires|libre|
|[okbEN7zWQ3xJAzkg.htm](pathfinder-bestiary-3-items/okbEN7zWQ3xJAzkg.htm)|Jaws|Mâchoires|libre|
|[Ol5nO6bgZS5cJIkA.htm](pathfinder-bestiary-3-items/Ol5nO6bgZS5cJIkA.htm)|Constant Spells|Sorts constants|libre|
|[oLyWvtqHGoL3rQb1.htm](pathfinder-bestiary-3-items/oLyWvtqHGoL3rQb1.htm)|Constant Spells|Sorts constants|libre|
|[oM4VdHuJNfDqEoHR.htm](pathfinder-bestiary-3-items/oM4VdHuJNfDqEoHR.htm)|Anchor|Ancrage|libre|
|[oMZGYxkqoirlCaQm.htm](pathfinder-bestiary-3-items/oMZGYxkqoirlCaQm.htm)|Shortsword|Épée courte|libre|
|[ON9shjsp6vVBXsO0.htm](pathfinder-bestiary-3-items/ON9shjsp6vVBXsO0.htm)|Darkvision|Vision dans le noir|libre|
|[OnmLcXNYU1Jxp5ji.htm](pathfinder-bestiary-3-items/OnmLcXNYU1Jxp5ji.htm)|Bubonic Plague|Peste bubonique|libre|
|[oNP92kbO07u3ivGK.htm](pathfinder-bestiary-3-items/oNP92kbO07u3ivGK.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[ooPhbtvdtMFxysJe.htm](pathfinder-bestiary-3-items/ooPhbtvdtMFxysJe.htm)|Evasion|Évasion|libre|
|[OoyUDyw74uN0JNYI.htm](pathfinder-bestiary-3-items/OoyUDyw74uN0JNYI.htm)|Regeneration 40 (Deactivated by Positive, Mental, or Orichalcum)|Régénération 40 (désactivée par Positif, Mental ou Orichalque)|libre|
|[oPdEp3kAGptn9Bkh.htm](pathfinder-bestiary-3-items/oPdEp3kAGptn9Bkh.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[oPF7KkI27o6FCE0l.htm](pathfinder-bestiary-3-items/oPF7KkI27o6FCE0l.htm)|Improved Grab|Empoignade/Agrippement améliorés|libre|
|[Ophbfa4MbDPldjGG.htm](pathfinder-bestiary-3-items/Ophbfa4MbDPldjGG.htm)|Darkvision|Vision dans le noir|libre|
|[OPww9QAdYNjhEqoV.htm](pathfinder-bestiary-3-items/OPww9QAdYNjhEqoV.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[oQ1wQPUo6g2iysB1.htm](pathfinder-bestiary-3-items/oQ1wQPUo6g2iysB1.htm)|Jaws|Mâchoires|libre|
|[oqveVOVwv267LEYp.htm](pathfinder-bestiary-3-items/oqveVOVwv267LEYp.htm)|Speak with Animals (Constant)|Communication avec les animaux (constant)|officielle|
|[Or20Bb0UzTLxRqoj.htm](pathfinder-bestiary-3-items/Or20Bb0UzTLxRqoj.htm)|Shortbow|Arc court|libre|
|[OrjyA3TToykWjeyF.htm](pathfinder-bestiary-3-items/OrjyA3TToykWjeyF.htm)|Boneyard Lore|Connaissance du Cimetière|officielle|
|[ORnkr2b5BKCvtKXe.htm](pathfinder-bestiary-3-items/ORnkr2b5BKCvtKXe.htm)|Grab|Empoignade/Agrippement|libre|
|[oRzzljhsCixNqf5j.htm](pathfinder-bestiary-3-items/oRzzljhsCixNqf5j.htm)|Elegant Cane|Canne élégante|libre|
|[osh2zvGEnGifdNKA.htm](pathfinder-bestiary-3-items/osh2zvGEnGifdNKA.htm)|Darkvision|Vision dans le noir|libre|
|[OSib6ckkzfe3tvbE.htm](pathfinder-bestiary-3-items/OSib6ckkzfe3tvbE.htm)|Enormous|Énorme|libre|
|[OsJRsnBwCYSFdA4G.htm](pathfinder-bestiary-3-items/OsJRsnBwCYSFdA4G.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[OsLma7Z7aL2Uy9WP.htm](pathfinder-bestiary-3-items/OsLma7Z7aL2Uy9WP.htm)|Pass Without Trace (Constant) (Forest Terrain Only)|Passage sans trace (constant) (en forêt seulement)|libre|
|[OTCTmOT1dbEFaHMD.htm](pathfinder-bestiary-3-items/OTCTmOT1dbEFaHMD.htm)|Detect Alignment (At Will)|Détection de l'alignement (À volonté)|libre|
|[OTkxsBzxKFuPWRCX.htm](pathfinder-bestiary-3-items/OTkxsBzxKFuPWRCX.htm)|Composite Longbow|Arc long composite|libre|
|[oTrOmw914xPkqVd5.htm](pathfinder-bestiary-3-items/oTrOmw914xPkqVd5.htm)|Fist|Poing|libre|
|[OU34B5fiyPMmjnXO.htm](pathfinder-bestiary-3-items/OU34B5fiyPMmjnXO.htm)|Easy to Call|Facile à invoquer|libre|
|[ou4TzsHhJ2YJ9Ub3.htm](pathfinder-bestiary-3-items/ou4TzsHhJ2YJ9Ub3.htm)|Fireball (At Will) (See Unstable Magic)|Boule de feu(à volonté, voir Magie instable)|libre|
|[oU5HOhztZ5crg339.htm](pathfinder-bestiary-3-items/oU5HOhztZ5crg339.htm)|Kukri|Kukri|libre|
|[ou7GQNYw2dnoJPZW.htm](pathfinder-bestiary-3-items/ou7GQNYw2dnoJPZW.htm)|Greatsword|Épée à deux mains|libre|
|[oUqggV8Z8uzpYdNA.htm](pathfinder-bestiary-3-items/oUqggV8Z8uzpYdNA.htm)|Instrument of Retribution|Instrument du châtiment|libre|
|[oveFRPbhF3sXi9RJ.htm](pathfinder-bestiary-3-items/oveFRPbhF3sXi9RJ.htm)|Ephemeral Claw|Griffe éphémère|libre|
|[ovf1EYDzh0K1JNl2.htm](pathfinder-bestiary-3-items/ovf1EYDzh0K1JNl2.htm)|Ward|Pupille|libre|
|[oVjrC7X48FBIjCug.htm](pathfinder-bestiary-3-items/oVjrC7X48FBIjCug.htm)|Breath Weapon|Arme de souffle|libre|
|[oVMhwSHesHUR8PLW.htm](pathfinder-bestiary-3-items/oVMhwSHesHUR8PLW.htm)|Unluck Aura|Aura de malchance|libre|
|[OvoJR0fdN07ZLFYR.htm](pathfinder-bestiary-3-items/OvoJR0fdN07ZLFYR.htm)|Attack of Opportunity (Stinger Only)|Attaque d'opportunité (Dard uniquement)|libre|
|[Ovtg8L0Jkr5t6b4n.htm](pathfinder-bestiary-3-items/Ovtg8L0Jkr5t6b4n.htm)|Blazing Admonition|Réprimande ardente|libre|
|[OVXjqkURq28dcMFO.htm](pathfinder-bestiary-3-items/OVXjqkURq28dcMFO.htm)|Negative Healing|Guérison négative|libre|
|[OW53VBMI2Iw52vm3.htm](pathfinder-bestiary-3-items/OW53VBMI2Iw52vm3.htm)|Nature|Nature|libre|
|[OW9oEttpg9WFvgOt.htm](pathfinder-bestiary-3-items/OW9oEttpg9WFvgOt.htm)|Constant Spells|Sorts constants|libre|
|[OWa41XZ0GByISijn.htm](pathfinder-bestiary-3-items/OWa41XZ0GByISijn.htm)|Spear|Lance|libre|
|[oWdMjEIfQss7DdS1.htm](pathfinder-bestiary-3-items/oWdMjEIfQss7DdS1.htm)|Jaws|Mâchoires|libre|
|[oWFVODhnr16hmGjN.htm](pathfinder-bestiary-3-items/oWFVODhnr16hmGjN.htm)|Constant Spells|Sorts constants|libre|
|[OwP8GUdLEkFwmgjL.htm](pathfinder-bestiary-3-items/OwP8GUdLEkFwmgjL.htm)|Champion Focus Spells|Sorts focalisés de champion|libre|
|[OwpDVJif5SRqFrSS.htm](pathfinder-bestiary-3-items/OwpDVJif5SRqFrSS.htm)|Tail|Queue|libre|
|[OwZj80arbVgN8YC9.htm](pathfinder-bestiary-3-items/OwZj80arbVgN8YC9.htm)|-1 to All Saves vs. Emotion Effects|Malus de -1 à tous les jets de sauvegarde contre les effets d'émotion|libre|
|[oX5mS0nonoT9j23k.htm](pathfinder-bestiary-3-items/oX5mS0nonoT9j23k.htm)|Paint|Peindre|libre|
|[oX8m7VQu9DO6VFnp.htm](pathfinder-bestiary-3-items/oX8m7VQu9DO6VFnp.htm)|Boneshard Burst|Éclatement d'os|libre|
|[oxJLEKIfZkDETCv8.htm](pathfinder-bestiary-3-items/oxJLEKIfZkDETCv8.htm)|Darkvision|Vision dans le noir|libre|
|[oXM5aJ0mHfq2fGWh.htm](pathfinder-bestiary-3-items/oXM5aJ0mHfq2fGWh.htm)|Javelin|Javelot|libre|
|[oyLpBeZ7RvXbKxJA.htm](pathfinder-bestiary-3-items/oyLpBeZ7RvXbKxJA.htm)|Outcast's Curse (At Will)|Malédiction du paria (À volonté)|libre|
|[OYZDy9EFgKjBC4ZW.htm](pathfinder-bestiary-3-items/OYZDy9EFgKjBC4ZW.htm)|Coiling Frenzy|Frénésie torsadée|libre|
|[ozof8R0i6Gx0GTeA.htm](pathfinder-bestiary-3-items/ozof8R0i6Gx0GTeA.htm)|Fist|Poing|libre|
|[oZq1LEjJ1S0dXNc2.htm](pathfinder-bestiary-3-items/oZq1LEjJ1S0dXNc2.htm)|Change Shape|Changement de forme|libre|
|[ozywltVfeeRvEgor.htm](pathfinder-bestiary-3-items/ozywltVfeeRvEgor.htm)|Eel Jaws|Mâchoires d'anguille|libre|
|[p0P37OZT7ovYmUkB.htm](pathfinder-bestiary-3-items/p0P37OZT7ovYmUkB.htm)|Canine Vulnerability|Vulnérabilité aux canidés|libre|
|[p2TaK9MrvykMKB0o.htm](pathfinder-bestiary-3-items/p2TaK9MrvykMKB0o.htm)|Dimension Door (At Will)|Porte dimensionnelle (À volonté)|officielle|
|[p3DOduEPOUUeLaxK.htm](pathfinder-bestiary-3-items/p3DOduEPOUUeLaxK.htm)|Grab|Empoignade/Agrippement|libre|
|[P3e58ycpovGthuDe.htm](pathfinder-bestiary-3-items/P3e58ycpovGthuDe.htm)|Scent (Imprecise) 40 feet|Odorat 12 m (imprécis)|libre|
|[p3v8D49u0adS76qw.htm](pathfinder-bestiary-3-items/p3v8D49u0adS76qw.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[p4F4jzZzQp6LGCex.htm](pathfinder-bestiary-3-items/p4F4jzZzQp6LGCex.htm)|Smother|Étouffement|libre|
|[p4RYgQW9q1rXPgz5.htm](pathfinder-bestiary-3-items/p4RYgQW9q1rXPgz5.htm)|Darkvision|Vision dans le noir|libre|
|[p58AYJyyu8cSum0T.htm](pathfinder-bestiary-3-items/p58AYJyyu8cSum0T.htm)|Grab|Empoignade/Agrippement|libre|
|[p5i4xSC4BGMzlqUF.htm](pathfinder-bestiary-3-items/p5i4xSC4BGMzlqUF.htm)|Telepathy 60 feet|Télépathie 18 m|libre|
|[p5iUKuZYPVDb0i3z.htm](pathfinder-bestiary-3-items/p5iUKuZYPVDb0i3z.htm)|Sumbreiva Huntblade|Couteau de chasse sumbreiva|libre|
|[p5VLQl4f56EqDMBY.htm](pathfinder-bestiary-3-items/p5VLQl4f56EqDMBY.htm)|Magic Aura (Constant) (Self Only)|Aura magique (constant)(soi uniquement)|libre|
|[P6lMDdeaBGEoO8lA.htm](pathfinder-bestiary-3-items/P6lMDdeaBGEoO8lA.htm)|Tremorsense (Imprecise) 60 feet|Perception des vibrations 18 m (imprécis)|libre|
|[p6oPHL2T4fZfMivY.htm](pathfinder-bestiary-3-items/p6oPHL2T4fZfMivY.htm)|Jaws|Mâchoires|libre|
|[p6uToNU7wgFDgDDH.htm](pathfinder-bestiary-3-items/p6uToNU7wgFDgDDH.htm)|Sloth|Paresse|libre|
|[P6yyZcwSDswPX7xS.htm](pathfinder-bestiary-3-items/P6yyZcwSDswPX7xS.htm)|Immortal|Éternel|libre|
|[P7PEpIp5LPhybC2C.htm](pathfinder-bestiary-3-items/P7PEpIp5LPhybC2C.htm)|Engulf|Engloutir|libre|
|[P7rE9vi1P70NJ5iD.htm](pathfinder-bestiary-3-items/P7rE9vi1P70NJ5iD.htm)|Plague of Ancients|Peste des anciens|libre|
|[p7SnyvGd17jjb0aF.htm](pathfinder-bestiary-3-items/p7SnyvGd17jjb0aF.htm)|Tidal Wave|Vague de marée|libre|
|[p8hPnF6s5wU3KJk3.htm](pathfinder-bestiary-3-items/p8hPnF6s5wU3KJk3.htm)|Claw|Griffe|libre|
|[P8K5a9sUjb1J99U0.htm](pathfinder-bestiary-3-items/P8K5a9sUjb1J99U0.htm)|Talon|Serre|libre|
|[p931SlqMJE64SPG4.htm](pathfinder-bestiary-3-items/p931SlqMJE64SPG4.htm)|Spirit Tendril|Vrille d'esprits liés|libre|
|[P9EmNj8ZddzoKHMT.htm](pathfinder-bestiary-3-items/P9EmNj8ZddzoKHMT.htm)|Possession (See Mind Swap)|Possession (voir Échange d'esprit)|libre|
|[PA233M8LHOomjH0v.htm](pathfinder-bestiary-3-items/PA233M8LHOomjH0v.htm)|At-Will Spells|Sorts constants|libre|
|[pA64qfMCuUfGqBOF.htm](pathfinder-bestiary-3-items/pA64qfMCuUfGqBOF.htm)|Amphisbaena Venom|Venin d'amphisbène|libre|
|[paacuYr4AiURwRWW.htm](pathfinder-bestiary-3-items/paacuYr4AiURwRWW.htm)|Tail|Queue|libre|
|[PB6GFQeZfrHdNRmj.htm](pathfinder-bestiary-3-items/PB6GFQeZfrHdNRmj.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[pbe04bBz3BMbUs0Q.htm](pathfinder-bestiary-3-items/pbe04bBz3BMbUs0Q.htm)|Suggestion (At Will)|Suggestion (À volonté)|officielle|
|[pBgScXHzKgG3o8iv.htm](pathfinder-bestiary-3-items/pBgScXHzKgG3o8iv.htm)|Oceanic Armor|Armure océanique|libre|
|[PbrxNFl95AGC5nKu.htm](pathfinder-bestiary-3-items/PbrxNFl95AGC5nKu.htm)|Diplomacy|Diplomatie|libre|
|[pbSfPZuBX9faeDui.htm](pathfinder-bestiary-3-items/pbSfPZuBX9faeDui.htm)|Horn|Corne|libre|
|[pbwIXTUIrcHbVCd3.htm](pathfinder-bestiary-3-items/pbwIXTUIrcHbVCd3.htm)|Darkvision|Vision dans le noir|libre|
|[pCERhTThClxu07sY.htm](pathfinder-bestiary-3-items/pCERhTThClxu07sY.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[pcMLR5ngGmapeJYS.htm](pathfinder-bestiary-3-items/pcMLR5ngGmapeJYS.htm)|Tail|Queue|libre|
|[PCyOFsdSGo2P3emN.htm](pathfinder-bestiary-3-items/PCyOFsdSGo2P3emN.htm)|Glyph of Warding (At Will)|Glyphe de garde (À volonté)|libre|
|[pCYui6n9e9HBtE9b.htm](pathfinder-bestiary-3-items/pCYui6n9e9HBtE9b.htm)|Tree Stride (At Will)|Voyage par les arbres (À volonté)|libre|
|[PDk5irePkj7swYlp.htm](pathfinder-bestiary-3-items/PDk5irePkj7swYlp.htm)|Hatchet|+1|Hachette +1|libre|
|[pDKSuwaiL7A6j7D7.htm](pathfinder-bestiary-3-items/pDKSuwaiL7A6j7D7.htm)|Jaws|Mâchoires|libre|
|[pDpwUF5lNCdUWFQT.htm](pathfinder-bestiary-3-items/pDpwUF5lNCdUWFQT.htm)|Claw|Griffe|libre|
|[PDYDZKTsK9cr0dq7.htm](pathfinder-bestiary-3-items/PDYDZKTsK9cr0dq7.htm)|Deep Breath|Inspiration profonde|libre|
|[pedL6wHN0TYSu6Fd.htm](pathfinder-bestiary-3-items/pedL6wHN0TYSu6Fd.htm)|Spear|Lance|libre|
|[pePRN91wJ0Dpe7pM.htm](pathfinder-bestiary-3-items/pePRN91wJ0Dpe7pM.htm)|Urban Chasers|Traqueurs urbain|libre|
|[pEUpV6dWX1Vbavcs.htm](pathfinder-bestiary-3-items/pEUpV6dWX1Vbavcs.htm)|Glyph of Warding (At Will)|Glyphe de garde (À volonté)|libre|
|[pFDqDZzKZDMzZZk1.htm](pathfinder-bestiary-3-items/pFDqDZzKZDMzZZk1.htm)|Claw|Griffe|libre|
|[pFF77x8PA51YY8TF.htm](pathfinder-bestiary-3-items/pFF77x8PA51YY8TF.htm)|Household Lore|Connaissance domestique|officielle|
|[PfPbwkrM06fK0tGR.htm](pathfinder-bestiary-3-items/PfPbwkrM06fK0tGR.htm)|Hand|Main|libre|
|[pfRf6x3WGakhgEUH.htm](pathfinder-bestiary-3-items/pfRf6x3WGakhgEUH.htm)|Tongues (Constant)|Don des langues (constant)|officielle|
|[PFvXq6fIzpNGeRPv.htm](pathfinder-bestiary-3-items/PFvXq6fIzpNGeRPv.htm)|Darkvision|Vision dans le noir|libre|
|[PG2qs05bMZmS8OIt.htm](pathfinder-bestiary-3-items/PG2qs05bMZmS8OIt.htm)|Composite Shortbow|Arc court composite|libre|
|[pgjyNm2rRbnBKZmb.htm](pathfinder-bestiary-3-items/pgjyNm2rRbnBKZmb.htm)|Sudden Manifestation|Manifestation soudaine|libre|
|[pGmtgv4CyBqJ9cJH.htm](pathfinder-bestiary-3-items/pGmtgv4CyBqJ9cJH.htm)|Stunning Screech|Cri étourdissant|libre|
|[pgNaQm9L35oslwOp.htm](pathfinder-bestiary-3-items/pgNaQm9L35oslwOp.htm)|All-Around Vision|Vision panoramique|libre|
|[PgnWSQrfu9kmcYy1.htm](pathfinder-bestiary-3-items/PgnWSQrfu9kmcYy1.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[pGQDgZ3Sme2JsoNm.htm](pathfinder-bestiary-3-items/pGQDgZ3Sme2JsoNm.htm)|Clutches|Étreinte de serres|libre|
|[pgVVTGf6DwqRarDp.htm](pathfinder-bestiary-3-items/pgVVTGf6DwqRarDp.htm)|Wolfrime|Givre du loup|libre|
|[phH8RreUZhjjwiuo.htm](pathfinder-bestiary-3-items/phH8RreUZhjjwiuo.htm)|Body|Corps|libre|
|[Phi18mvpdKJpQmql.htm](pathfinder-bestiary-3-items/Phi18mvpdKJpQmql.htm)|Phantom Horn|Cor fantôme|libre|
|[pHk7V2hLZrgzY4OE.htm](pathfinder-bestiary-3-items/pHk7V2hLZrgzY4OE.htm)|Charm (At Will)|Charme (À volonté)|officielle|
|[phln4muQIICQhLF5.htm](pathfinder-bestiary-3-items/phln4muQIICQhLF5.htm)|Illusory Disguise (At Will)|Déguisement illusoire (À volonté)|officielle|
|[pHOf2i3tNwpIeAHs.htm](pathfinder-bestiary-3-items/pHOf2i3tNwpIeAHs.htm)|Frightful Presence|Présence terrifiante|libre|
|[phtWMfKuIsMCi5cE.htm](pathfinder-bestiary-3-items/phtWMfKuIsMCi5cE.htm)|Tongue Reposition|Repositionnement de la langue|libre|
|[PhxCt9NDQTWqfCU5.htm](pathfinder-bestiary-3-items/PhxCt9NDQTWqfCU5.htm)|Eviscerate|Éviscération|libre|
|[PHZ0zixmQt2ljkJm.htm](pathfinder-bestiary-3-items/PHZ0zixmQt2ljkJm.htm)|Entrails|Entrailles|libre|
|[pIoRWbV6ahXe5lvR.htm](pathfinder-bestiary-3-items/pIoRWbV6ahXe5lvR.htm)|Change Shape|Changement de forme|libre|
|[pj0FI4XEhBPiuOmy.htm](pathfinder-bestiary-3-items/pj0FI4XEhBPiuOmy.htm)|Change Shape|Changement de forme|libre|
|[pJKtjGeoTW0LzTPG.htm](pathfinder-bestiary-3-items/pJKtjGeoTW0LzTPG.htm)|Draconic Momentum|Impulsion draconique|libre|
|[pjtcZhkm20HjFEmn.htm](pathfinder-bestiary-3-items/pjtcZhkm20HjFEmn.htm)|Darkvision|Vision dans le noir|libre|
|[pJUrlkv2i5xEz2WO.htm](pathfinder-bestiary-3-items/pJUrlkv2i5xEz2WO.htm)|Star Child|Enfant des étoiles|libre|
|[pKaeBB4bJYXTzs69.htm](pathfinder-bestiary-3-items/pKaeBB4bJYXTzs69.htm)|Coiling Frenzy|Frénésie torsadée|libre|
|[PKh7s5uQyxm0yPQg.htm](pathfinder-bestiary-3-items/PKh7s5uQyxm0yPQg.htm)|Pliers|Pinces|libre|
|[pKpXpiLPYdr4VX46.htm](pathfinder-bestiary-3-items/pKpXpiLPYdr4VX46.htm)|Skip Between|Alterner|libre|
|[PLTi8diRg1rhDOR9.htm](pathfinder-bestiary-3-items/PLTi8diRg1rhDOR9.htm)|Trackless Step|Absence de traces|officielle|
|[PlTKFXVlBOQ1pLtX.htm](pathfinder-bestiary-3-items/PlTKFXVlBOQ1pLtX.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[pLvuVluNzMrdi4RY.htm](pathfinder-bestiary-3-items/pLvuVluNzMrdi4RY.htm)|Illusory Scene (At Will)|Scène illusoire (À volonté)|officielle|
|[pmhHWb3PBr7rQpE5.htm](pathfinder-bestiary-3-items/pmhHWb3PBr7rQpE5.htm)|Spitfire|Crachat de feu|libre|
|[PmLhjqOQdj9Bkm2j.htm](pathfinder-bestiary-3-items/PmLhjqOQdj9Bkm2j.htm)|Drink Oil|Boire de l'huile|libre|
|[pnaFRppT92BaCHjj.htm](pathfinder-bestiary-3-items/pnaFRppT92BaCHjj.htm)|Claw|Griffe|libre|
|[pnC70Lg68qGCzyl2.htm](pathfinder-bestiary-3-items/pnC70Lg68qGCzyl2.htm)|Darkvision|Vision dans le noir|libre|
|[pnip6OCCdZn9i0pD.htm](pathfinder-bestiary-3-items/pnip6OCCdZn9i0pD.htm)|Countered by Fire|Contré par le feu|libre|
|[pOCKvTWHNSvPbzYY.htm](pathfinder-bestiary-3-items/pOCKvTWHNSvPbzYY.htm)|See Invisibility (Constant)|Détection de l'invisibilité (constant)|officielle|
|[poFV0K8IzaMCquPb.htm](pathfinder-bestiary-3-items/poFV0K8IzaMCquPb.htm)|Nirvana Lore|Connaissance du Nirvana|libre|
|[POpEyXnpPzh143yS.htm](pathfinder-bestiary-3-items/POpEyXnpPzh143yS.htm)|Improved Grab|Empoignade/Agrippement améliorés|libre|
|[POZqpY2nqdOUZBf7.htm](pathfinder-bestiary-3-items/POZqpY2nqdOUZBf7.htm)|At-Will Spells|Sorts à volonté|libre|
|[pp693fsJOt23L3X9.htm](pathfinder-bestiary-3-items/pp693fsJOt23L3X9.htm)|Shock Mind|Choc mentaliste|libre|
|[pPPA3LPdsuQ55Gp0.htm](pathfinder-bestiary-3-items/pPPA3LPdsuQ55Gp0.htm)|Counterattack|Contre-attaque|libre|
|[PqKDvCgTiNBUGAhF.htm](pathfinder-bestiary-3-items/PqKDvCgTiNBUGAhF.htm)|Coven Spells|Sorts de cercle|libre|
|[pQzYMrj90yKkCKzz.htm](pathfinder-bestiary-3-items/pQzYMrj90yKkCKzz.htm)|Mind Blank (Constant)|Esprit impénétrable (constant)|officielle|
|[PrETYXvcaucelz4m.htm](pathfinder-bestiary-3-items/PrETYXvcaucelz4m.htm)|Powerful Leaper|Sauteur puissant|libre|
|[pRfIkGoEX8AyxeSw.htm](pathfinder-bestiary-3-items/pRfIkGoEX8AyxeSw.htm)|Darkvision|Vision dans le noir|libre|
|[prTBGYHOcT9eQskj.htm](pathfinder-bestiary-3-items/prTBGYHOcT9eQskj.htm)|Eye Probe|Sonde occulaire|libre|
|[pRvVMMMJPM8IsEkp.htm](pathfinder-bestiary-3-items/pRvVMMMJPM8IsEkp.htm)|Darkvision|Vision dans le noir|libre|
|[PrWWui76kCsgmSBk.htm](pathfinder-bestiary-3-items/PrWWui76kCsgmSBk.htm)|Flying Wheel|Roue volante|libre|
|[pRXjlEM0YDK7HrCS.htm](pathfinder-bestiary-3-items/pRXjlEM0YDK7HrCS.htm)|Squirming Embrace|Étreinte grouillante|libre|
|[PS82ZXV5QXRxiBXf.htm](pathfinder-bestiary-3-items/PS82ZXV5QXRxiBXf.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[PSRTnwfMOQYpIg4X.htm](pathfinder-bestiary-3-items/PSRTnwfMOQYpIg4X.htm)|Scribing Lore|Connaissance du métier de scribe|officielle|
|[ptpsl6OafcvwkgOx.htm](pathfinder-bestiary-3-items/ptpsl6OafcvwkgOx.htm)|Accord Essence|Essence de résonance|libre|
|[PtvDEVABQH9iJs6d.htm](pathfinder-bestiary-3-items/PtvDEVABQH9iJs6d.htm)|Siphon Magic|Siphon de magie|libre|
|[Pv0epZ0IMHJFIRG2.htm](pathfinder-bestiary-3-items/Pv0epZ0IMHJFIRG2.htm)|Darkvision|Vision dans le noir|libre|
|[pV1OZsdl0kyBME6Q.htm](pathfinder-bestiary-3-items/pV1OZsdl0kyBME6Q.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[PVVUt6V4luw3UnL9.htm](pathfinder-bestiary-3-items/PVVUt6V4luw3UnL9.htm)|Dimension Door (At Will)|Porte dimensionnelle (À volonté)|officielle|
|[pVxNWlSICm5ij03c.htm](pathfinder-bestiary-3-items/pVxNWlSICm5ij03c.htm)|Grab|Empoignade/Agrippement|libre|
|[pW9sevYZuAwpWAr3.htm](pathfinder-bestiary-3-items/pW9sevYZuAwpWAr3.htm)|Darkvision|Vision dans le noir|libre|
|[PWE9sinIQ5L6H4v2.htm](pathfinder-bestiary-3-items/PWE9sinIQ5L6H4v2.htm)|Consecration Vulnerability|Vulnérable à la consécration|libre|
|[pwmnQMrCnI7oQIoE.htm](pathfinder-bestiary-3-items/pwmnQMrCnI7oQIoE.htm)|Jaws|Mâchoires|libre|
|[pWSuV2gScsPeanBP.htm](pathfinder-bestiary-3-items/pWSuV2gScsPeanBP.htm)|Divine Lance (Chaos or Evil)|Lance divine (chaotique ou Mauvais)|libre|
|[pWvysguPWMKlVIzr.htm](pathfinder-bestiary-3-items/pWvysguPWMKlVIzr.htm)|Raptor Dive|Piqué du rapace|libre|
|[pwWN3Mp741VnQ6Lx.htm](pathfinder-bestiary-3-items/pwWN3Mp741VnQ6Lx.htm)|Command (Animals Only)|Injonction (Animaux uniquement)|libre|
|[Px9k1rac0p4O9b4J.htm](pathfinder-bestiary-3-items/Px9k1rac0p4O9b4J.htm)|Fist|Poing|libre|
|[PxEpWsl4UxCVOJxy.htm](pathfinder-bestiary-3-items/PxEpWsl4UxCVOJxy.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[PxHTXBiqvpnvByT4.htm](pathfinder-bestiary-3-items/PxHTXBiqvpnvByT4.htm)|Calculated Blow|Coup calculé|libre|
|[pXlz78EGb6lw6Aij.htm](pathfinder-bestiary-3-items/pXlz78EGb6lw6Aij.htm)|Fist|Poing|libre|
|[pxvM1CdSpuWlWrQ3.htm](pathfinder-bestiary-3-items/pxvM1CdSpuWlWrQ3.htm)|-1 Status to All Saves vs. Death Effects|Malus de statut -1 à tous les jets de sauvegarde contre les effets de mort|libre|
|[pYnHQN69rHC0bPm0.htm](pathfinder-bestiary-3-items/pYnHQN69rHC0bPm0.htm)|Longspear|Pique|libre|
|[PyxiGz5dlEpJ0SE7.htm](pathfinder-bestiary-3-items/PyxiGz5dlEpJ0SE7.htm)|Deluge|Inonder|libre|
|[pZYPf41244FTorQE.htm](pathfinder-bestiary-3-items/pZYPf41244FTorQE.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[Q0IYQhi4qttZkUKn.htm](pathfinder-bestiary-3-items/Q0IYQhi4qttZkUKn.htm)|Scent (Imprecise) 60 feet|Odorat 18 m (imprécis)|libre|
|[Q2J6B9C1q7HPAVHY.htm](pathfinder-bestiary-3-items/Q2J6B9C1q7HPAVHY.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[Q345ui1zxYcyOoLX.htm](pathfinder-bestiary-3-items/Q345ui1zxYcyOoLX.htm)|Constant Spells|Sorts constants|libre|
|[q3uWzNeCqQuLwiIA.htm](pathfinder-bestiary-3-items/q3uWzNeCqQuLwiIA.htm)|Aura of Courage|Aura de courage|libre|
|[q44VpDPavuRcix13.htm](pathfinder-bestiary-3-items/q44VpDPavuRcix13.htm)|Composite Shortbow|+1,striking|Arc court composite de Frappe +1|libre|
|[Q49aLd8cKOkjpk9J.htm](pathfinder-bestiary-3-items/Q49aLd8cKOkjpk9J.htm)|Darkvision|Vision dans le noir|libre|
|[q5JXKKVYw0NywPhT.htm](pathfinder-bestiary-3-items/q5JXKKVYw0NywPhT.htm)|Pummeling Charge|Charge tabassante|libre|
|[Q6AA6pHAaFSGJA1w.htm](pathfinder-bestiary-3-items/Q6AA6pHAaFSGJA1w.htm)|Little Oasis|Petit oasis|libre|
|[q6bluZPiKPFzD79n.htm](pathfinder-bestiary-3-items/q6bluZPiKPFzD79n.htm)|Frightful Presence|Présence terrifiante|libre|
|[Q6ieY38ZPpXhQz2w.htm](pathfinder-bestiary-3-items/Q6ieY38ZPpXhQz2w.htm)|Telepathy 500 feet|Télépathie 150 m|libre|
|[q72VNUpEzEqiaYgo.htm](pathfinder-bestiary-3-items/q72VNUpEzEqiaYgo.htm)|Tongues (Constant)|Don des langues (constant)|officielle|
|[Q77O3dFWbNc8OyJh.htm](pathfinder-bestiary-3-items/Q77O3dFWbNc8OyJh.htm)|Grab|Empoignade/Agrippement|libre|
|[Q7Qfg4mcXnQcUmCd.htm](pathfinder-bestiary-3-items/Q7Qfg4mcXnQcUmCd.htm)|Fist|Poing|libre|
|[Q8UNdxr6PscskBIx.htm](pathfinder-bestiary-3-items/Q8UNdxr6PscskBIx.htm)|Flaming Sphere (At Will) (See Unstable Magic)|Sphère de feu (À volonté) (voir Magie instable)|libre|
|[Q9b50evzwuGvfkAB.htm](pathfinder-bestiary-3-items/Q9b50evzwuGvfkAB.htm)|Self-Destruct|Autodestruction|libre|
|[q9cxVnhnmsCNsS4G.htm](pathfinder-bestiary-3-items/q9cxVnhnmsCNsS4G.htm)|Earthen Fist|Poing de terre|libre|
|[Q9YkvCkVgM00oIy8.htm](pathfinder-bestiary-3-items/Q9YkvCkVgM00oIy8.htm)|Thorn|Épine|libre|
|[qaQZJI6uMjww56l2.htm](pathfinder-bestiary-3-items/qaQZJI6uMjww56l2.htm)|Tongues (Constant)|Don des langues (constant)|officielle|
|[qAXVCcqDcG1w6bek.htm](pathfinder-bestiary-3-items/qAXVCcqDcG1w6bek.htm)|Claw|Griffe|libre|
|[QB0FNEPET64VPGnO.htm](pathfinder-bestiary-3-items/QB0FNEPET64VPGnO.htm)|Greatclub|Massue|libre|
|[QBCimVCmKxfPzDh2.htm](pathfinder-bestiary-3-items/QBCimVCmKxfPzDh2.htm)|+1 Breastplate|Cuirasse +1|libre|
|[QBHI3WhGKrjBNZMq.htm](pathfinder-bestiary-3-items/QBHI3WhGKrjBNZMq.htm)|Hydration|Hydratation|libre|
|[qbjGaacQh4FUm7lS.htm](pathfinder-bestiary-3-items/qbjGaacQh4FUm7lS.htm)|Athletics|Athlétisme|libre|
|[QbUWrst99xyuP7X9.htm](pathfinder-bestiary-3-items/QbUWrst99xyuP7X9.htm)|Scent (Imprecise) 60 feet|Odorat 18 m (imprécis)|libre|
|[QC1IrArhxNW6ef3t.htm](pathfinder-bestiary-3-items/QC1IrArhxNW6ef3t.htm)|Longbow|+1|Arc long +1|libre|
|[qcMqwJBE8PblmJXd.htm](pathfinder-bestiary-3-items/qcMqwJBE8PblmJXd.htm)|Tongues (Constant)|Don des langues (constant)|officielle|
|[QCpgvjWcAi1GGE3l.htm](pathfinder-bestiary-3-items/QCpgvjWcAi1GGE3l.htm)|Foot|Pied|libre|
|[QCRkv3Pld8YuyUlE.htm](pathfinder-bestiary-3-items/QCRkv3Pld8YuyUlE.htm)|Light Blindness|Aveuglé par la lumière|officielle|
|[QE9gzkXJFMTGYg19.htm](pathfinder-bestiary-3-items/QE9gzkXJFMTGYg19.htm)|Out You Go|Sors de là|libre|
|[QeDdL4PRsKD6EnrC.htm](pathfinder-bestiary-3-items/QeDdL4PRsKD6EnrC.htm)|Rock|Rocher|libre|
|[qedIdKEDmmqyp8F5.htm](pathfinder-bestiary-3-items/qedIdKEDmmqyp8F5.htm)|Low-Light Vision|Vision nocturne|libre|
|[QeGks9jOjqzdzWci.htm](pathfinder-bestiary-3-items/QeGks9jOjqzdzWci.htm)|Bloodsense (Imprecise) 60 feet|Perception du sang 18 m (imprécis)|libre|
|[qEjTHqZLDRx2o0dJ.htm](pathfinder-bestiary-3-items/qEjTHqZLDRx2o0dJ.htm)|Claw|Griffe|libre|
|[qen9rYoBFDQD84n7.htm](pathfinder-bestiary-3-items/qen9rYoBFDQD84n7.htm)|Cannonade|Cannonade|libre|
|[QEObXX17C19vzj8L.htm](pathfinder-bestiary-3-items/QEObXX17C19vzj8L.htm)|Tail|Queue|libre|
|[QfgdLYrvrXT358Kh.htm](pathfinder-bestiary-3-items/QfgdLYrvrXT358Kh.htm)|Body|Corps|libre|
|[QfsNS0isn7JsRZIX.htm](pathfinder-bestiary-3-items/QfsNS0isn7JsRZIX.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[qgbvTpc5c7E9GDGd.htm](pathfinder-bestiary-3-items/qgbvTpc5c7E9GDGd.htm)|Jaws|Mâchoires|libre|
|[Qgr1ThnIaqkHRArD.htm](pathfinder-bestiary-3-items/Qgr1ThnIaqkHRArD.htm)|At-Will Spells|Sorts à volonté|libre|
|[qH5DHTYxShVT5a5y.htm](pathfinder-bestiary-3-items/qH5DHTYxShVT5a5y.htm)|Curse of the Werecrocodile|Malédiction du crocodile-garou|libre|
|[qhMGpfia6l2CmKf0.htm](pathfinder-bestiary-3-items/qhMGpfia6l2CmKf0.htm)|Leech Thought|Parasite de souvenirs|libre|
|[qhoxtRXXWKvmotj5.htm](pathfinder-bestiary-3-items/qhoxtRXXWKvmotj5.htm)|Darkvision|Vision dans le noir|libre|
|[qHZIM3SkMqqfQWWG.htm](pathfinder-bestiary-3-items/qHZIM3SkMqqfQWWG.htm)|Beak|Bec|libre|
|[qi2lvWIXN1GWDfm1.htm](pathfinder-bestiary-3-items/qi2lvWIXN1GWDfm1.htm)|Entangling Train|Traîne enchevêtrante|libre|
|[qI7na98jgBlQpNui.htm](pathfinder-bestiary-3-items/qI7na98jgBlQpNui.htm)|Blade Ally|Lame alliée|libre|
|[qiEzVS822d89t62v.htm](pathfinder-bestiary-3-items/qiEzVS822d89t62v.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[QIy5U5WATHHnpiex.htm](pathfinder-bestiary-3-items/QIy5U5WATHHnpiex.htm)|Touch of Idiocy (At Will)|Idiotie (À volonté)|libre|
|[qJi07N1qiZ8im9Ed.htm](pathfinder-bestiary-3-items/qJi07N1qiZ8im9Ed.htm)|Healing Arrow|Flèche de guérison|libre|
|[qjiQvNcg8Xp6U5g6.htm](pathfinder-bestiary-3-items/qjiQvNcg8Xp6U5g6.htm)|Jaws|Mâchoires|libre|
|[QJZgoLZp0BfuvxvJ.htm](pathfinder-bestiary-3-items/QJZgoLZp0BfuvxvJ.htm)|Echolocation (Precise) 120 feet|Écholocalisation 36 m (précis)|libre|
|[qkbQpoQc1isiqlSL.htm](pathfinder-bestiary-3-items/qkbQpoQc1isiqlSL.htm)|Spiritual Rope|Corde spirituelle|libre|
|[qki0wO5XwHHhxpMl.htm](pathfinder-bestiary-3-items/qki0wO5XwHHhxpMl.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[qKR5KyDCt5y2Z7A4.htm](pathfinder-bestiary-3-items/qKR5KyDCt5y2Z7A4.htm)|Liquefy|Liquéfier|libre|
|[qkU2EydXIrYI5Hxx.htm](pathfinder-bestiary-3-items/qkU2EydXIrYI5Hxx.htm)|Tusk|Défense|libre|
|[qLKTHssJqXd4ynva.htm](pathfinder-bestiary-3-items/qLKTHssJqXd4ynva.htm)|Darkvision|Vision dans le noir|libre|
|[qm3E8aZeyjHSz3i8.htm](pathfinder-bestiary-3-items/qm3E8aZeyjHSz3i8.htm)|Rend|Éventration|libre|
|[qmwnlcLuNKPhcNEX.htm](pathfinder-bestiary-3-items/qmwnlcLuNKPhcNEX.htm)|Undead Lore|Connaissance des morts-vivants|libre|
|[QNcsHGzGsxzU10k0.htm](pathfinder-bestiary-3-items/QNcsHGzGsxzU10k0.htm)|Consume Spell|Consummer le sort|libre|
|[qNUdm1wGl9coLrPP.htm](pathfinder-bestiary-3-items/qNUdm1wGl9coLrPP.htm)|Dagger|Dague|libre|
|[QOpnCjoiZN5QnY8X.htm](pathfinder-bestiary-3-items/QOpnCjoiZN5QnY8X.htm)|Jaws|Mâchoires|libre|
|[qP4bmFVnqKAeVCn1.htm](pathfinder-bestiary-3-items/qP4bmFVnqKAeVCn1.htm)|At-Will Spells|Sorts à volonté|libre|
|[QP9beVsoQsi5P8A5.htm](pathfinder-bestiary-3-items/QP9beVsoQsi5P8A5.htm)|Improved Grab|Empoignade/Agrippement améliorés|libre|
|[qpCzKmCc7RVqEcVK.htm](pathfinder-bestiary-3-items/qpCzKmCc7RVqEcVK.htm)|At-Will Spells|Sorts à volonté|libre|
|[qppBAh5FNr8f4f2X.htm](pathfinder-bestiary-3-items/qppBAh5FNr8f4f2X.htm)|Divine Destruction|Destruction divine|libre|
|[Qq0M2j9jLs4ePYWY.htm](pathfinder-bestiary-3-items/Qq0M2j9jLs4ePYWY.htm)|Athletics|Athlétisme|libre|
|[qqLORgTIh6GxitQS.htm](pathfinder-bestiary-3-items/qqLORgTIh6GxitQS.htm)|Lore (Any One)|Connaissance (une au choix)|libre|
|[qQZbVChzEGjI5sIk.htm](pathfinder-bestiary-3-items/qQZbVChzEGjI5sIk.htm)|Claw|Griffe|libre|
|[qRW2k1VmeIkTIOB9.htm](pathfinder-bestiary-3-items/qRW2k1VmeIkTIOB9.htm)|Throw Rock|Projection de rocher|libre|
|[qRZz1ras2ekZ3Dnn.htm](pathfinder-bestiary-3-items/qRZz1ras2ekZ3Dnn.htm)|Crushing Despair (At Will)|Désespoir foudroyant (À volonté)|libre|
|[qsBmWCL2GK9QfAiP.htm](pathfinder-bestiary-3-items/qsBmWCL2GK9QfAiP.htm)|Crafting|Artisanat|libre|
|[qSCF9pmEsCQejxxw.htm](pathfinder-bestiary-3-items/qSCF9pmEsCQejxxw.htm)|Lifesense 30 feet|Perception de la vie (9 m)|libre|
|[qSEQHwgppWcWoGDK.htm](pathfinder-bestiary-3-items/qSEQHwgppWcWoGDK.htm)|Tail|Queue|libre|
|[QSp7Nee2yP67M9zp.htm](pathfinder-bestiary-3-items/QSp7Nee2yP67M9zp.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[qT7gzi3MA0fVfY8a.htm](pathfinder-bestiary-3-items/qT7gzi3MA0fVfY8a.htm)|Grab|Empoignade/Agrippement|libre|
|[QTLnr9MYtcbjoq6C.htm](pathfinder-bestiary-3-items/QTLnr9MYtcbjoq6C.htm)|Negative Healing|Guérison négative|libre|
|[QtPVup2bHHIHGyCe.htm](pathfinder-bestiary-3-items/QtPVup2bHHIHGyCe.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[qTVf39ECIPyC5jMa.htm](pathfinder-bestiary-3-items/qTVf39ECIPyC5jMa.htm)|Negative Healing|Guérison négative|libre|
|[Qu61Gf9rt5ANopIq.htm](pathfinder-bestiary-3-items/Qu61Gf9rt5ANopIq.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[quetxJ36kYJJorBD.htm](pathfinder-bestiary-3-items/quetxJ36kYJJorBD.htm)|Feathered Charge|Charge emplummée|libre|
|[qUO24kmJdTM3pw3H.htm](pathfinder-bestiary-3-items/qUO24kmJdTM3pw3H.htm)|Jaws|Mâchoires|libre|
|[QUp7RYwQLvBoTGp7.htm](pathfinder-bestiary-3-items/QUp7RYwQLvBoTGp7.htm)|Grab|Empoignade/Agrippement|libre|
|[QuSzkFT66YGAGqLE.htm](pathfinder-bestiary-3-items/QuSzkFT66YGAGqLE.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[Qv1U20ksT1elULUW.htm](pathfinder-bestiary-3-items/Qv1U20ksT1elULUW.htm)|Rock|Pierre|libre|
|[qVbkGmZA4J4JEc5c.htm](pathfinder-bestiary-3-items/qVbkGmZA4J4JEc5c.htm)|Temporal Sense|Perception temporelle|libre|
|[qvJ5SX7zo7TKIP2h.htm](pathfinder-bestiary-3-items/qvJ5SX7zo7TKIP2h.htm)|Toenail Cutter|Coupe-ongles|libre|
|[qVqG3qPrNw7cF8D4.htm](pathfinder-bestiary-3-items/qVqG3qPrNw7cF8D4.htm)|Breath Weapon|Arme de souffle|libre|
|[QvzzSlemey10PLLP.htm](pathfinder-bestiary-3-items/QvzzSlemey10PLLP.htm)|Claw|Griffe|libre|
|[QwKsTc2ps0cjzmnW.htm](pathfinder-bestiary-3-items/QwKsTc2ps0cjzmnW.htm)|Jaws|Mâchoires|libre|
|[QxQT0BLNv0boSPAf.htm](pathfinder-bestiary-3-items/QxQT0BLNv0boSPAf.htm)|Abandon Puppet|Abandon de marionnette|libre|
|[QXV1JtzUVWMyWGFW.htm](pathfinder-bestiary-3-items/QXV1JtzUVWMyWGFW.htm)|Thoughtsense (Imprecise) 120 feet|Perception des pensées 36 m (imprécis)|libre|
|[QyBhco1VMbstqt4y.htm](pathfinder-bestiary-3-items/QyBhco1VMbstqt4y.htm)|Branch|Branche|libre|
|[qYJ6jUOSJMEwOOuC.htm](pathfinder-bestiary-3-items/qYJ6jUOSJMEwOOuC.htm)|Nibble|Grignotage|libre|
|[Qz5ELLWHhm9W0RD3.htm](pathfinder-bestiary-3-items/Qz5ELLWHhm9W0RD3.htm)|Gnathobase|Gnathobase|libre|
|[qzIjJHwF4eyLDmOh.htm](pathfinder-bestiary-3-items/qzIjJHwF4eyLDmOh.htm)|Fountain Pen|Plume d'encre|libre|
|[QZPUv0k24Dx8d0JZ.htm](pathfinder-bestiary-3-items/QZPUv0k24Dx8d0JZ.htm)|At-Will Spells|Sorts à volonté|libre|
|[QZx7vPULEK8wzVbx.htm](pathfinder-bestiary-3-items/QZx7vPULEK8wzVbx.htm)|Void Weapon|Arme du néant|libre|
|[R0bN0QbVsJDFnHId.htm](pathfinder-bestiary-3-items/R0bN0QbVsJDFnHId.htm)|Dagger|Dague|libre|
|[r0CnvIkvKsI5FMgI.htm](pathfinder-bestiary-3-items/r0CnvIkvKsI5FMgI.htm)|Thoughtsense (Precise) 120 feet|Perception des pensées 36 m (précis)|libre|
|[R0HLiBnfzTgXGH4w.htm](pathfinder-bestiary-3-items/R0HLiBnfzTgXGH4w.htm)|Staff|Bâton|libre|
|[R0vde3bCSBW2a8br.htm](pathfinder-bestiary-3-items/R0vde3bCSBW2a8br.htm)|Clinging Bites|Morsures acharnées|libre|
|[r1Ei390nYbTkJ2Ac.htm](pathfinder-bestiary-3-items/r1Ei390nYbTkJ2Ac.htm)|Air Walk (Constant)|Marche dans les airs (constant)|officielle|
|[R1JY8Uw7yAyXK7JQ.htm](pathfinder-bestiary-3-items/R1JY8Uw7yAyXK7JQ.htm)|Sunset Ribbon|Ruban du crépuscule|libre|
|[R2Ck3XzpQHa6qLKl.htm](pathfinder-bestiary-3-items/R2Ck3XzpQHa6qLKl.htm)|Darkvision|Vision dans le noir|libre|
|[r3ldx2ZybEAqaKTp.htm](pathfinder-bestiary-3-items/r3ldx2ZybEAqaKTp.htm)|+2 Perception to Sense Motive|bonus de +2 à la Perception pour Deviner les intentions|libre|
|[r3zwcLhwIxuPvofI.htm](pathfinder-bestiary-3-items/r3zwcLhwIxuPvofI.htm)|Fist|Poing|libre|
|[r4jLytkNtr1uMewT.htm](pathfinder-bestiary-3-items/r4jLytkNtr1uMewT.htm)|Gleaming Armor|Armure brillante|libre|
|[R4srCUwoLVvI4h6T.htm](pathfinder-bestiary-3-items/R4srCUwoLVvI4h6T.htm)|Negative Healing|Guérison négative|libre|
|[R4UxTvMjd8XGo0H1.htm](pathfinder-bestiary-3-items/R4UxTvMjd8XGo0H1.htm)|Darkvision|Vision dans le noir|libre|
|[R4Vzee8KxsqTF0Wf.htm](pathfinder-bestiary-3-items/R4Vzee8KxsqTF0Wf.htm)|Darkvision|Vision dans le noir|libre|
|[r5va25MjWQsL5Nut.htm](pathfinder-bestiary-3-items/r5va25MjWQsL5Nut.htm)|At-Will Spells|Sorts à volonté|libre|
|[R6LPamKnpozwfOnm.htm](pathfinder-bestiary-3-items/R6LPamKnpozwfOnm.htm)|Telepathic Ballad|Ballade télépathique|libre|
|[R6XHv4H8ZshCgHND.htm](pathfinder-bestiary-3-items/R6XHv4H8ZshCgHND.htm)|Grab|Empoignade/Agrippement|libre|
|[R8ZpJ5Qjyevn9M2I.htm](pathfinder-bestiary-3-items/R8ZpJ5Qjyevn9M2I.htm)|Control Weather (Doesn't Require Secondary Casters)|Contrôle du climat (ne nécessite pas d'incantateurs secondaires)|libre|
|[r9r9sl3qGETAhlH9.htm](pathfinder-bestiary-3-items/r9r9sl3qGETAhlH9.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[r9s9fT1PAiICOzs0.htm](pathfinder-bestiary-3-items/r9s9fT1PAiICOzs0.htm)|Telepathy 60 feet|Télépathie 18 m|libre|
|[raEWo6pY0qJvkkyn.htm](pathfinder-bestiary-3-items/raEWo6pY0qJvkkyn.htm)|+2 Status to All Saves vs. Poison|bonus de statut de +2 aux JdS contre le poison|libre|
|[Ran96W84fw5Bh3Y3.htm](pathfinder-bestiary-3-items/Ran96W84fw5Bh3Y3.htm)|Manticore Lore|Connaissance des manticores|libre|
|[rb3UkMRburjTZJpj.htm](pathfinder-bestiary-3-items/rb3UkMRburjTZJpj.htm)|At-Will Spells|Sorts à volonté|libre|
|[RbIcnu5Fx9eAX5Uq.htm](pathfinder-bestiary-3-items/RbIcnu5Fx9eAX5Uq.htm)|At-Will Spells|Sorts à volonté|libre|
|[RbKBM2RsozHFLA9U.htm](pathfinder-bestiary-3-items/RbKBM2RsozHFLA9U.htm)|Bond with Mortal|Lié à un Mortel|libre|
|[rbM0VDuWa5kOfsbQ.htm](pathfinder-bestiary-3-items/rbM0VDuWa5kOfsbQ.htm)|Tentacle|Tentacule|libre|
|[RBrP1ThmfTW2EUCG.htm](pathfinder-bestiary-3-items/RBrP1ThmfTW2EUCG.htm)|Whistling Bones|Sifflement osseux|libre|
|[RcFq3iNSRnikQSug.htm](pathfinder-bestiary-3-items/RcFq3iNSRnikQSug.htm)|Tusk|Défense|libre|
|[RcHSgAM2xaq0Lve5.htm](pathfinder-bestiary-3-items/RcHSgAM2xaq0Lve5.htm)|Easily Fascinated|Fascination facile|libre|
|[RcQhtjlnYqR8A8J9.htm](pathfinder-bestiary-3-items/RcQhtjlnYqR8A8J9.htm)|Spray Perfume|Vaporisation odorante|libre|
|[RdbeYdVwFLgopJjk.htm](pathfinder-bestiary-3-items/RdbeYdVwFLgopJjk.htm)|Talon|Serre|libre|
|[RDF08DgCVW9LDvLd.htm](pathfinder-bestiary-3-items/RDF08DgCVW9LDvLd.htm)|Change Shape|Changement de forme|libre|
|[RdGjfkq57iIV43T3.htm](pathfinder-bestiary-3-items/RdGjfkq57iIV43T3.htm)|Telepathy 30 feet|Télépathie 9 mètres|libre|
|[Rdy2GbIHuRoKy7bU.htm](pathfinder-bestiary-3-items/Rdy2GbIHuRoKy7bU.htm)|Fed by Wood|Renforcé par le bois|libre|
|[Re0uYYA9c4nnaRne.htm](pathfinder-bestiary-3-items/Re0uYYA9c4nnaRne.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[Re3laGuS2iEiMxt0.htm](pathfinder-bestiary-3-items/Re3laGuS2iEiMxt0.htm)|Burning Touch|Contact brûlant|libre|
|[Re8xzF2naY7y8ZBf.htm](pathfinder-bestiary-3-items/Re8xzF2naY7y8ZBf.htm)|Mauling Rush|Ruée massacrante|libre|
|[RE9BxxRzi4ENZrXg.htm](pathfinder-bestiary-3-items/RE9BxxRzi4ENZrXg.htm)|Proboscis Tongue|Langue proboscopique|libre|
|[REbcJzGp6sT7jOuZ.htm](pathfinder-bestiary-3-items/REbcJzGp6sT7jOuZ.htm)|Limb|Membre|libre|
|[rFakD0KrlUr9DJiQ.htm](pathfinder-bestiary-3-items/rFakD0KrlUr9DJiQ.htm)|Claw|Griffe|libre|
|[RFaRYNVP3GPdcMkY.htm](pathfinder-bestiary-3-items/RFaRYNVP3GPdcMkY.htm)|Darkvision|Vision dans le noir|libre|
|[RfjRZPiLUrrypXhA.htm](pathfinder-bestiary-3-items/RfjRZPiLUrrypXhA.htm)|At-Will Spells|Sorts à volonté|libre|
|[RfXLcZpuY88IxZTD.htm](pathfinder-bestiary-3-items/RfXLcZpuY88IxZTD.htm)|Catch Rock|Interception de rochers|libre|
|[RGJbS9RTIb1LL5uF.htm](pathfinder-bestiary-3-items/RGJbS9RTIb1LL5uF.htm)|Swarming Snips|Nuée de pincements|libre|
|[rgq47BGgm0jbIXb6.htm](pathfinder-bestiary-3-items/rgq47BGgm0jbIXb6.htm)|Darkvision|Vision dans le noir|libre|
|[rGx3BT8mbRKRUpA2.htm](pathfinder-bestiary-3-items/rGx3BT8mbRKRUpA2.htm)|Destabilizing Field|Champ déstabilisant|libre|
|[RgzVnccNPsj40XQj.htm](pathfinder-bestiary-3-items/RgzVnccNPsj40XQj.htm)|Speak with Animals (Constant)|Communication avec les animaux (constant)|officielle|
|[RIF4K2wDuZCHWToa.htm](pathfinder-bestiary-3-items/RIF4K2wDuZCHWToa.htm)|At-Will Spells|Sorts à volonté|libre|
|[RIfmoCoHRDlhRlU1.htm](pathfinder-bestiary-3-items/RIfmoCoHRDlhRlU1.htm)|Claw|Griffe|libre|
|[riFooTptrWPMV3tk.htm](pathfinder-bestiary-3-items/riFooTptrWPMV3tk.htm)|+1 Status to All Saves vs. Evil|bonus de statut de +1 aux JdS contre le mal|libre|
|[riqBNngN6YkDWYqv.htm](pathfinder-bestiary-3-items/riqBNngN6YkDWYqv.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[rIrMS5z48NzYuh9b.htm](pathfinder-bestiary-3-items/rIrMS5z48NzYuh9b.htm)|Impending Dread|Effroi tenace|libre|
|[rjh44a42EWaBBBP1.htm](pathfinder-bestiary-3-items/rjh44a42EWaBBBP1.htm)|Fed by Earth|Renforcé par la terre|libre|
|[rjVHVaa9BN3ZAiKL.htm](pathfinder-bestiary-3-items/rjVHVaa9BN3ZAiKL.htm)|Spike Storm|Tempête de piques|libre|
|[RKz5iqiz10TiodA6.htm](pathfinder-bestiary-3-items/RKz5iqiz10TiodA6.htm)|Breath Weapon|Arme de souffle|libre|
|[RKZuKmTaOGPFwnzJ.htm](pathfinder-bestiary-3-items/RKZuKmTaOGPFwnzJ.htm)|Impossible Stature|Stature imposante|libre|
|[rL2cjpuNRY9BBahF.htm](pathfinder-bestiary-3-items/rL2cjpuNRY9BBahF.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[RLIvbPC5j2g106DY.htm](pathfinder-bestiary-3-items/RLIvbPC5j2g106DY.htm)|Katana|+1|Katana +1|libre|
|[RLNzraMr15AQohaZ.htm](pathfinder-bestiary-3-items/RLNzraMr15AQohaZ.htm)|Endure Elements (Constant)|Endurance aux éléments (constant)|libre|
|[rlXD928xXNnyihPK.htm](pathfinder-bestiary-3-items/rlXD928xXNnyihPK.htm)|At-Will Spells|Sorts à volonté|libre|
|[RM8HImoxZfJtj8F9.htm](pathfinder-bestiary-3-items/RM8HImoxZfJtj8F9.htm)|Tremorsense (Imprecise) 60 feet|Perception des vibrations 18 m (imprécis)|libre|
|[rmezKNjwT2IakFHV.htm](pathfinder-bestiary-3-items/rmezKNjwT2IakFHV.htm)|Spine|Épines|libre|
|[rmrUx8mVzDx7W96E.htm](pathfinder-bestiary-3-items/rmrUx8mVzDx7W96E.htm)|Flames of Fury|Flammes de fureur|libre|
|[Rmtq9X0tzbZPgELk.htm](pathfinder-bestiary-3-items/Rmtq9X0tzbZPgELk.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[Rmxe7dutW3AoDW4m.htm](pathfinder-bestiary-3-items/Rmxe7dutW3AoDW4m.htm)|Echolocation (Precise) 40 feet|Écholocalisation 12 m (précis)|libre|
|[rNBF8QplzOETa39b.htm](pathfinder-bestiary-3-items/rNBF8QplzOETa39b.htm)|Branch|Branche|libre|
|[RP2SeHrbTJkiKCCL.htm](pathfinder-bestiary-3-items/RP2SeHrbTJkiKCCL.htm)|Constrict|Constriction|libre|
|[rpBeKBUmHNZG8uQA.htm](pathfinder-bestiary-3-items/rpBeKBUmHNZG8uQA.htm)|Hoof|Sabot|libre|
|[rPhxztMzLxuNALLo.htm](pathfinder-bestiary-3-items/rPhxztMzLxuNALLo.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[RpO4Le4eD3npO4n3.htm](pathfinder-bestiary-3-items/RpO4Le4eD3npO4n3.htm)|Low-Light Vision|Vision nocturne|libre|
|[rPTqCxoOB0B5ON1m.htm](pathfinder-bestiary-3-items/rPTqCxoOB0B5ON1m.htm)|Weapon Arm|Bras armé|libre|
|[RqeQ9keiv0CzpNUe.htm](pathfinder-bestiary-3-items/RqeQ9keiv0CzpNUe.htm)|Pass Without Trace (Constant) (Forest Terrain Only)|Passage sans trace (constant) (en forêt seulement)|libre|
|[rQhc6SkjKUBdbb2C.htm](pathfinder-bestiary-3-items/rQhc6SkjKUBdbb2C.htm)|Darkvision|Vision dans le noir|libre|
|[rREO3eXjpKJUOkJt.htm](pathfinder-bestiary-3-items/rREO3eXjpKJUOkJt.htm)|Dispel Magic (At Will)|Dissipation de la magie (À volonté)|officielle|
|[rRrYw3Yi8aOQcMIK.htm](pathfinder-bestiary-3-items/rRrYw3Yi8aOQcMIK.htm)|Detect Alignment (At Will)|Détection de l'alignement (À volonté)|libre|
|[RscN8Wq0CouXZD3j.htm](pathfinder-bestiary-3-items/RscN8Wq0CouXZD3j.htm)|Plane of Water Lore|Connaissance du Plan de l'eau|libre|
|[rtGARtAYnK1Pseab.htm](pathfinder-bestiary-3-items/rtGARtAYnK1Pseab.htm)|Darkvision|Vision dans le noir|libre|
|[RTMzhRViOSVhuoXI.htm](pathfinder-bestiary-3-items/RTMzhRViOSVhuoXI.htm)|Electric Projectiles|Projectile électrique|libre|
|[rU17t3NM7TJ3u03o.htm](pathfinder-bestiary-3-items/rU17t3NM7TJ3u03o.htm)|Regeneration 10 (Deactivated by Piercing)|Régénération 10 (désactivée par Perforant)|libre|
|[rUKEfvwyoUMhaFzX.htm](pathfinder-bestiary-3-items/rUKEfvwyoUMhaFzX.htm)|Vicelike Jaws|Mâchoires-étau|libre|
|[RUqAGMQwHrGpfJPs.htm](pathfinder-bestiary-3-items/RUqAGMQwHrGpfJPs.htm)|Arcane Tendril|Vrille arcanique|libre|
|[rVdhm2xzR2AUjY4g.htm](pathfinder-bestiary-3-items/rVdhm2xzR2AUjY4g.htm)|Beak|Bec|libre|
|[RvhHvUE7nZmwkyMD.htm](pathfinder-bestiary-3-items/RvhHvUE7nZmwkyMD.htm)|Accord Essence|Essence de résonance|libre|
|[RVk9mcBqt1YRswGY.htm](pathfinder-bestiary-3-items/RVk9mcBqt1YRswGY.htm)|Desert-Adapted|Adapté au désert|libre|
|[RwrYgxqil6oSI14J.htm](pathfinder-bestiary-3-items/RwrYgxqil6oSI14J.htm)|Hurl Net|Lancé de filet|libre|
|[rXKR8tn1tmh9DdJ3.htm](pathfinder-bestiary-3-items/rXKR8tn1tmh9DdJ3.htm)|Jaws|Mâchoires|libre|
|[rXrJgVzgOwMP7Cbr.htm](pathfinder-bestiary-3-items/rXrJgVzgOwMP7Cbr.htm)|Stable Stance|Posture stable|libre|
|[rxsLa6trqZBzZIOL.htm](pathfinder-bestiary-3-items/rxsLa6trqZBzZIOL.htm)|Impossible Stature|Stature imposante|libre|
|[rxu5E3okZFA5hwkO.htm](pathfinder-bestiary-3-items/rxu5E3okZFA5hwkO.htm)|Plaque Burst|Éclat de plaque dentaire|libre|
|[RxVHmIYJh9eOX9c3.htm](pathfinder-bestiary-3-items/RxVHmIYJh9eOX9c3.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[rYBfhejHUk9mGJyA.htm](pathfinder-bestiary-3-items/rYBfhejHUk9mGJyA.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|libre|
|[RyfXnY4RnP8Gsh8n.htm](pathfinder-bestiary-3-items/RyfXnY4RnP8Gsh8n.htm)|Vulnerable to Calm Emotions|Vulnérable à Apaisement des émotions|libre|
|[rYN4yg46PUJy1sS9.htm](pathfinder-bestiary-3-items/rYN4yg46PUJy1sS9.htm)|Tail|Queue|libre|
|[rZeB3sV7jSAvG69a.htm](pathfinder-bestiary-3-items/rZeB3sV7jSAvG69a.htm)|Darkvision|Vision dans le noir|libre|
|[RzulMB9xO8oiQIcf.htm](pathfinder-bestiary-3-items/RzulMB9xO8oiQIcf.htm)|Forest Lore|Connaissance des forêts|officielle|
|[S0fNGUpgdHgR1UVg.htm](pathfinder-bestiary-3-items/S0fNGUpgdHgR1UVg.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[S1xpDPa446jLmH3J.htm](pathfinder-bestiary-3-items/S1xpDPa446jLmH3J.htm)|Mask of Terror (Self Only)|Masque terrifiant (soi uniquement)|officielle|
|[s2oghprwMzFfjM11.htm](pathfinder-bestiary-3-items/s2oghprwMzFfjM11.htm)|Shy|Timide|libre|
|[S2oLvvECz7beXP1O.htm](pathfinder-bestiary-3-items/S2oLvvECz7beXP1O.htm)|Jaws|Mâchoires|libre|
|[S3r2AvwbHmmhBwB3.htm](pathfinder-bestiary-3-items/S3r2AvwbHmmhBwB3.htm)|Regression|Régression|libre|
|[S3S752aZGm6sxlqG.htm](pathfinder-bestiary-3-items/S3S752aZGm6sxlqG.htm)|Knockdown|Renversement|libre|
|[s49RXHWrH4qqNcEe.htm](pathfinder-bestiary-3-items/s49RXHWrH4qqNcEe.htm)|Tree Stride (At Will) (Cherry Trees Only)|Voyage par les arbres (À volonté) (cerisiers uniquement)|libre|
|[s5b07GowVsMRP7ro.htm](pathfinder-bestiary-3-items/s5b07GowVsMRP7ro.htm)|Mask of Terror (At Will)|Masque terrifiant (À volonté)|libre|
|[S5fuvAOuYoMnXjOB.htm](pathfinder-bestiary-3-items/S5fuvAOuYoMnXjOB.htm)|Darkvision|Vision dans le noir|libre|
|[s65OrlZwX6JQgM40.htm](pathfinder-bestiary-3-items/s65OrlZwX6JQgM40.htm)|Radiant Mantle|Aura rayonnante|libre|
|[s6BLghmvkOpXc6TW.htm](pathfinder-bestiary-3-items/s6BLghmvkOpXc6TW.htm)|Darkvision|Vision dans le noir|libre|
|[s6k7oyMyiZKIqAar.htm](pathfinder-bestiary-3-items/s6k7oyMyiZKIqAar.htm)|Claw|Griffe|libre|
|[s6UD93sTVBhNgF8P.htm](pathfinder-bestiary-3-items/s6UD93sTVBhNgF8P.htm)|Assume Fiery Form|Prendre la forme ardente|libre|
|[S7iB7CDLRoVYT6Pp.htm](pathfinder-bestiary-3-items/S7iB7CDLRoVYT6Pp.htm)|Mind Reading (At Will)|Lecture des pensées (À volonté)|officielle|
|[s7ODA9MMbyTJSlyF.htm](pathfinder-bestiary-3-items/s7ODA9MMbyTJSlyF.htm)|Lifesense 30 feet|Perception de la vie (9 m)|libre|
|[S7uNAygkE7sTVfcL.htm](pathfinder-bestiary-3-items/S7uNAygkE7sTVfcL.htm)|Vindictive Crush|Écrasement vindicatif|libre|
|[s87z24wrwStP1Ouk.htm](pathfinder-bestiary-3-items/s87z24wrwStP1Ouk.htm)|Positive Energy Transfer|Transfert d'énergie positive|libre|
|[S8A8KdLa7fpa3GXF.htm](pathfinder-bestiary-3-items/S8A8KdLa7fpa3GXF.htm)|Ectoplasmic Form|Forme ectoplasmique|libre|
|[S8UsqHDRsdN6XtV5.htm](pathfinder-bestiary-3-items/S8UsqHDRsdN6XtV5.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[s9EYmFKDJ3ygchjQ.htm](pathfinder-bestiary-3-items/s9EYmFKDJ3ygchjQ.htm)|Darkvision|Vision dans le noir|libre|
|[S9RBBqUiQCE9JKB1.htm](pathfinder-bestiary-3-items/S9RBBqUiQCE9JKB1.htm)|Emotionally Unaware|Incompréhension émotionnelle|libre|
|[s9Y9sorFMjlmE7OS.htm](pathfinder-bestiary-3-items/s9Y9sorFMjlmE7OS.htm)|Anathematic Aversion|Aversion anathématique|libre|
|[SA7P07cUNUdSC4Sp.htm](pathfinder-bestiary-3-items/SA7P07cUNUdSC4Sp.htm)|Betrayal Toxin|Toxine de trahison|libre|
|[sAINNUJA1VNOBd31.htm](pathfinder-bestiary-3-items/sAINNUJA1VNOBd31.htm)|Lifesense 60 feet|Perception de la vie 18 m|officielle|
|[SamwGEbNZfw3hWWZ.htm](pathfinder-bestiary-3-items/SamwGEbNZfw3hWWZ.htm)|Rapier|Rapière|libre|
|[SaZttWSFJOlcV8gF.htm](pathfinder-bestiary-3-items/SaZttWSFJOlcV8gF.htm)|Violent Retort|Riposte violente|libre|
|[SBSCHoUK3Je0TZWJ.htm](pathfinder-bestiary-3-items/SBSCHoUK3Je0TZWJ.htm)|Axe Vulnerability|Vulnérabilité à la hache|libre|
|[sBY73yijWI9qogpX.htm](pathfinder-bestiary-3-items/sBY73yijWI9qogpX.htm)|All-Around Vision|Vision panoramique|libre|
|[SbZX27efEktYhABv.htm](pathfinder-bestiary-3-items/SbZX27efEktYhABv.htm)|Tremorsense (Imprecise) 60 feet|Perception des vibrations 18 m (imprécis)|libre|
|[Scblqwycs4aZk1u1.htm](pathfinder-bestiary-3-items/Scblqwycs4aZk1u1.htm)|Scent (Imprecise) 60 feet|Odorat 18 m (imprécis)|libre|
|[sCnH4uFy8fBv6bnP.htm](pathfinder-bestiary-3-items/sCnH4uFy8fBv6bnP.htm)|No Breath|Ne respire pas|libre|
|[SCRF3318aESeiNRz.htm](pathfinder-bestiary-3-items/SCRF3318aESeiNRz.htm)|Idols of Stone|Idoles de pierre|libre|
|[ScsosKeNG0BlbPN5.htm](pathfinder-bestiary-3-items/ScsosKeNG0BlbPN5.htm)|Hallucinatory Haunting|Tourments hallucinatoires|libre|
|[sCv6nwuflODCdMaO.htm](pathfinder-bestiary-3-items/sCv6nwuflODCdMaO.htm)|Portentous Gaze|Regard de mauvaise augure|libre|
|[SCWOEv6X8RtegOjU.htm](pathfinder-bestiary-3-items/SCWOEv6X8RtegOjU.htm)|Scythe|Faux|libre|
|[SE2Oc5VGiAxYtDb3.htm](pathfinder-bestiary-3-items/SE2Oc5VGiAxYtDb3.htm)|Crossbow|Arbalète|libre|
|[se2uQ9cHOTmmlxON.htm](pathfinder-bestiary-3-items/se2uQ9cHOTmmlxON.htm)|Spectral Claw|Griffe spectrale|libre|
|[sEbF3y3ttLqHmwtS.htm](pathfinder-bestiary-3-items/sEbF3y3ttLqHmwtS.htm)|Flame Ray|Rayon de flammes|libre|
|[SEw7wjeoKGHYcloR.htm](pathfinder-bestiary-3-items/SEw7wjeoKGHYcloR.htm)|Accord Essence|Essence de résonance|libre|
|[SF8eX3XCiOBA6wh4.htm](pathfinder-bestiary-3-items/SF8eX3XCiOBA6wh4.htm)|Intuit Object|Connaissance matérielle|libre|
|[SfFcwGTaNbZ6zFNW.htm](pathfinder-bestiary-3-items/SfFcwGTaNbZ6zFNW.htm)|Spear|Lance|libre|
|[sfp7Tgb12PISMHBT.htm](pathfinder-bestiary-3-items/sfp7Tgb12PISMHBT.htm)|Fist|Poing|libre|
|[SfQfbmrgZ4vr3nFt.htm](pathfinder-bestiary-3-items/SfQfbmrgZ4vr3nFt.htm)|Spear|Lance|libre|
|[sfynZ9FF1MvgyIsO.htm](pathfinder-bestiary-3-items/sfynZ9FF1MvgyIsO.htm)|At-Will Spells|Sorts à volonté|libre|
|[sGASuiT2DzsMvSIa.htm](pathfinder-bestiary-3-items/sGASuiT2DzsMvSIa.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[sGEoB9eohFcV4gEe.htm](pathfinder-bestiary-3-items/sGEoB9eohFcV4gEe.htm)|Regeneration 50 (Deactivated by Fire)|Régénération 50 (désactivée par Feu)|libre|
|[SGw3tiHnJxU3HeUz.htm](pathfinder-bestiary-3-items/SGw3tiHnJxU3HeUz.htm)|Darkvision|Vision dans le noir|libre|
|[SgXWjRi0fOvCndJM.htm](pathfinder-bestiary-3-items/SgXWjRi0fOvCndJM.htm)|Tongue|Langue|libre|
|[Sh368mCxkzNFInV2.htm](pathfinder-bestiary-3-items/Sh368mCxkzNFInV2.htm)|Greataxe|Grande hache|libre|
|[shT4gXjrfuduYtZr.htm](pathfinder-bestiary-3-items/shT4gXjrfuduYtZr.htm)|Hellwasp Stings|Piqûres de guêpes infernales|libre|
|[sIbPX7HRtLZsZC7R.htm](pathfinder-bestiary-3-items/sIbPX7HRtLZsZC7R.htm)|Sneak Attack|Attaque sournoise|libre|
|[sieqVjLe7KzZuR5l.htm](pathfinder-bestiary-3-items/sieqVjLe7KzZuR5l.htm)|Clairvoyance (At Will)|Clairvoyance (À volonté)|libre|
|[SIJ9zzLLshuohFwx.htm](pathfinder-bestiary-3-items/SIJ9zzLLshuohFwx.htm)|Darkvision|Vision dans le noir|libre|
|[sIJOxufLnRAxLd8H.htm](pathfinder-bestiary-3-items/sIJOxufLnRAxLd8H.htm)|Darkvision|Vision dans le noir|libre|
|[SIJXP2XGtiBASmHE.htm](pathfinder-bestiary-3-items/SIJXP2XGtiBASmHE.htm)|Constant Spells|Sorts constants|libre|
|[sIlgLiMP9ruto6w9.htm](pathfinder-bestiary-3-items/sIlgLiMP9ruto6w9.htm)|Scent (Imprecise) 40 feet|Odorat 12 m (imprécis)|libre|
|[sIWC4aZPyNtODaFY.htm](pathfinder-bestiary-3-items/sIWC4aZPyNtODaFY.htm)|Negative Healing|Guérison négative|libre|
|[sIZrd0MxYGEnR6YP.htm](pathfinder-bestiary-3-items/sIZrd0MxYGEnR6YP.htm)|Dual Opportunity|Double opportunité|libre|
|[sj9VbYqqHsDlLaRb.htm](pathfinder-bestiary-3-items/sj9VbYqqHsDlLaRb.htm)|Fire Jackal Saliva|Salive de chacal|libre|
|[sjeNzkkJM6xfdPo8.htm](pathfinder-bestiary-3-items/sjeNzkkJM6xfdPo8.htm)|Eye Beam|Rayon occulaire|libre|
|[SJtGnpq0cBxkmxWc.htm](pathfinder-bestiary-3-items/SJtGnpq0cBxkmxWc.htm)|Punish Flight|Vol punitif|libre|
|[sK09hTiJx42HpDxj.htm](pathfinder-bestiary-3-items/sK09hTiJx42HpDxj.htm)|Nature's Rebirth|Réincarnation naturelle|libre|
|[sk5aFaaf3tRIgb0p.htm](pathfinder-bestiary-3-items/sk5aFaaf3tRIgb0p.htm)|Spear|Lance|libre|
|[SKfXJh58tHRMQxNa.htm](pathfinder-bestiary-3-items/SKfXJh58tHRMQxNa.htm)|Low-Light Vision|Vision nocturne|libre|
|[SKICe54HSoZHNbYB.htm](pathfinder-bestiary-3-items/SKICe54HSoZHNbYB.htm)|Darkvision|Vision dans le noir|libre|
|[sKizGnItFf8dn6kf.htm](pathfinder-bestiary-3-items/sKizGnItFf8dn6kf.htm)|Dooming Touch|Contact maudit|libre|
|[sl07Not81gCCYcg7.htm](pathfinder-bestiary-3-items/sl07Not81gCCYcg7.htm)|Cavern Empathy|Empathie dans les cavernes|libre|
|[sLdsVr8WCWegLUjP.htm](pathfinder-bestiary-3-items/sLdsVr8WCWegLUjP.htm)|Darkvision|Vision dans le noir|libre|
|[SlKd6WmjF0C4lXGp.htm](pathfinder-bestiary-3-items/SlKd6WmjF0C4lXGp.htm)|Form Up|Se reformer|libre|
|[sLLDvETLI8wX6s36.htm](pathfinder-bestiary-3-items/sLLDvETLI8wX6s36.htm)|Heal (At Will)|Guérison (À volonté)|libre|
|[sLqam8gjwb8YnuXS.htm](pathfinder-bestiary-3-items/sLqam8gjwb8YnuXS.htm)|Voidglass Kukri|Kukri en verre du vide|libre|
|[sM1MA3hKVxt7ZvXp.htm](pathfinder-bestiary-3-items/sM1MA3hKVxt7ZvXp.htm)|+2 Circumstance to All Saves vs. Disease|bonus de circonstances de +2 aux JdS contre la maladie|libre|
|[sMOW7L2I7HwP3CtJ.htm](pathfinder-bestiary-3-items/sMOW7L2I7HwP3CtJ.htm)|Reassemble|Reconfiguration|libre|
|[Sn9QsKQ5fSuHrD9F.htm](pathfinder-bestiary-3-items/Sn9QsKQ5fSuHrD9F.htm)|At-Will Spells|Sorts à volonté|libre|
|[sNchZQFp9J0mXyVA.htm](pathfinder-bestiary-3-items/sNchZQFp9J0mXyVA.htm)|Throw Rock|Projection de rocher|libre|
|[SnFwuUdykz9voojZ.htm](pathfinder-bestiary-3-items/SnFwuUdykz9voojZ.htm)|Darkvision|Vision dans le noir|libre|
|[sNGs8FpXUh0x309i.htm](pathfinder-bestiary-3-items/sNGs8FpXUh0x309i.htm)|Telepathy 30 feet|Télépathie 9 m|libre|
|[snPpaYVXqbrRM8Ne.htm](pathfinder-bestiary-3-items/snPpaYVXqbrRM8Ne.htm)|Breath Weapon|Arme de souffle|libre|
|[Snsjj1UYj5mIU9UL.htm](pathfinder-bestiary-3-items/Snsjj1UYj5mIU9UL.htm)|Falchion|+2,greaterStriking|Cimeterre à deux mains de frappe supérieure +2|libre|
|[sNth9LOE6nRfhE3n.htm](pathfinder-bestiary-3-items/sNth9LOE6nRfhE3n.htm)|Telepathy (Touch)|Télépathie (Contact)|libre|
|[SoMnIapfSLR7sCTB.htm](pathfinder-bestiary-3-items/SoMnIapfSLR7sCTB.htm)|Control Weather (No Secondary Caster Required)|Contrôle du climat (ne nécessite pas d'incantateurs secondaires)|libre|
|[sOt6KDS7Y34upG7G.htm](pathfinder-bestiary-3-items/sOt6KDS7Y34upG7G.htm)|Breath Weapon|Arme de souffle|libre|
|[SP4I3NKUoM1EaTaK.htm](pathfinder-bestiary-3-items/SP4I3NKUoM1EaTaK.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[SPB7FRg5vvaLT9yY.htm](pathfinder-bestiary-3-items/SPB7FRg5vvaLT9yY.htm)|Pummeling Assault|Rouée de coups|libre|
|[sq2f8Tcx4gzFa7t5.htm](pathfinder-bestiary-3-items/sq2f8Tcx4gzFa7t5.htm)|Tusk|Défense|libre|
|[SQ3H5jQFY4vWcYDC.htm](pathfinder-bestiary-3-items/SQ3H5jQFY4vWcYDC.htm)|Arcane Prepared Spells|Sorts arcaniques préparés|libre|
|[SQvpxbj1u4t8orbJ.htm](pathfinder-bestiary-3-items/SQvpxbj1u4t8orbJ.htm)|Ferocious Roar|Rugissement féroce|libre|
|[sqYKSxNkOZ37Fmz6.htm](pathfinder-bestiary-3-items/sqYKSxNkOZ37Fmz6.htm)|Warfare Lore|Connaissance de la guerre|officielle|
|[SrTNivRRl7pbrWZN.htm](pathfinder-bestiary-3-items/SrTNivRRl7pbrWZN.htm)|Ghostly Longbow|Arc long spectral|libre|
|[sRU8pYLkFEMCRECp.htm](pathfinder-bestiary-3-items/sRU8pYLkFEMCRECp.htm)|Death Blaze|Brasier fatal|libre|
|[SRv1ATMYjrhtxE5O.htm](pathfinder-bestiary-3-items/SRv1ATMYjrhtxE5O.htm)|Gloom Aura|Aura de morosité|libre|
|[Sryn4awFAiEFPWJi.htm](pathfinder-bestiary-3-items/Sryn4awFAiEFPWJi.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[sT02s8CpPBlLaICN.htm](pathfinder-bestiary-3-items/sT02s8CpPBlLaICN.htm)|Scent (Imprecise) 60 feet|Odorat 18 m (imprécis)|libre|
|[st3MDTSpYydB4U32.htm](pathfinder-bestiary-3-items/st3MDTSpYydB4U32.htm)|Material Leap|Bond vers le plan Matériel|libre|
|[sT9rrvUVQPaZDRMI.htm](pathfinder-bestiary-3-items/sT9rrvUVQPaZDRMI.htm)|Drain Life|Drain de vie|libre|
|[stALstSmHYXTOJRU.htm](pathfinder-bestiary-3-items/stALstSmHYXTOJRU.htm)|Low-Light Vision|Vision nocturne|libre|
|[sTFtTWlMXrbtb4sx.htm](pathfinder-bestiary-3-items/sTFtTWlMXrbtb4sx.htm)|Detect Alignment (At Will) (Evil Only)|Détection de l'alignement (à volonté, mauvais uniquement)|officielle|
|[STN5ZaMm14HJIg38.htm](pathfinder-bestiary-3-items/STN5ZaMm14HJIg38.htm)|Jaws|Mâchoires|libre|
|[STWxL9WtBfQg569j.htm](pathfinder-bestiary-3-items/STWxL9WtBfQg569j.htm)|Dimension Door (At Will)|Porte dimensionnelle (À volonté)|officielle|
|[stXqE9CezJeM4Whd.htm](pathfinder-bestiary-3-items/stXqE9CezJeM4Whd.htm)|Greatclub|Massue|libre|
|[SU3ySHGYIBWN3dgx.htm](pathfinder-bestiary-3-items/SU3ySHGYIBWN3dgx.htm)|Adamantine Claw|Griffe en adamantium|libre|
|[SUBRW3AOWawVs2rU.htm](pathfinder-bestiary-3-items/SUBRW3AOWawVs2rU.htm)|Trample|Piétinement|libre|
|[SuGCwf8YVqJ0KZNG.htm](pathfinder-bestiary-3-items/SuGCwf8YVqJ0KZNG.htm)|Energy Ray|Rayon d'énergie|libre|
|[SUKtpi6VGVyh7Jip.htm](pathfinder-bestiary-3-items/SUKtpi6VGVyh7Jip.htm)|Retaliatory Scratch|Riposte de griffe|libre|
|[SUQKMz3YCGazvNeH.htm](pathfinder-bestiary-3-items/SUQKMz3YCGazvNeH.htm)|At-Will Spells|Sorts à volonté|libre|
|[Svc6cpZQ69ghEA6T.htm](pathfinder-bestiary-3-items/Svc6cpZQ69ghEA6T.htm)|At-Will Spells|Sorts à volonté|libre|
|[SVF9VI2ywkitioon.htm](pathfinder-bestiary-3-items/SVF9VI2ywkitioon.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[svqKqAYsrMhgU25p.htm](pathfinder-bestiary-3-items/svqKqAYsrMhgU25p.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[sVS5x7UzSVU5jjX2.htm](pathfinder-bestiary-3-items/sVS5x7UzSVU5jjX2.htm)|Darkvision|Vision dans le noir|libre|
|[SVucgTqH9TEpdPX4.htm](pathfinder-bestiary-3-items/SVucgTqH9TEpdPX4.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[sW0kQIjx6HrXLDiV.htm](pathfinder-bestiary-3-items/sW0kQIjx6HrXLDiV.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[sX0MwC23Mvp1YeD2.htm](pathfinder-bestiary-3-items/sX0MwC23Mvp1YeD2.htm)|Merciless Thrust|Botte impitoyable|libre|
|[SYiq4FiF50sGlZtS.htm](pathfinder-bestiary-3-items/SYiq4FiF50sGlZtS.htm)|In Concert|En harmonie|libre|
|[SZ0KpuFOSI1WY5Fo.htm](pathfinder-bestiary-3-items/SZ0KpuFOSI1WY5Fo.htm)|Low-Light Vision|Vision nocturne|libre|
|[sZ10byXqZzWm4yp5.htm](pathfinder-bestiary-3-items/sZ10byXqZzWm4yp5.htm)|Throw Rock|Projection de rocher|libre|
|[sZ2qJ9Ced7EAt6pU.htm](pathfinder-bestiary-3-items/sZ2qJ9Ced7EAt6pU.htm)|Fist|Poing|libre|
|[SZBQldnYP5zx1PyP.htm](pathfinder-bestiary-3-items/SZBQldnYP5zx1PyP.htm)|Coiling Frenzy|Frénésie torsadée|libre|
|[SzbZwTdWXUHNi17Y.htm](pathfinder-bestiary-3-items/SzbZwTdWXUHNi17Y.htm)|Claw|Griffe|libre|
|[SZjcOjfoi55nSlVu.htm](pathfinder-bestiary-3-items/SZjcOjfoi55nSlVu.htm)|Breath Weapon|Arme de souffle|libre|
|[szK7m2cRz0pWuEUb.htm](pathfinder-bestiary-3-items/szK7m2cRz0pWuEUb.htm)|Mind Reading (At Will)|Lecture des pensées (À volonté)|officielle|
|[SzriRFiHHVYO1GYI.htm](pathfinder-bestiary-3-items/SzriRFiHHVYO1GYI.htm)|Gremlin Lice|Poux de gremlin|libre|
|[t0I4vgS1D9by3wX6.htm](pathfinder-bestiary-3-items/t0I4vgS1D9by3wX6.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|libre|
|[t0jxVnsS6HbKkoTf.htm](pathfinder-bestiary-3-items/t0jxVnsS6HbKkoTf.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[T1AhLjoX6p5mr4gQ.htm](pathfinder-bestiary-3-items/T1AhLjoX6p5mr4gQ.htm)|Grab|Empoignade/Agrippement|libre|
|[T27ladpNIH9pULrU.htm](pathfinder-bestiary-3-items/T27ladpNIH9pULrU.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[t2HIuBRLJiGYJZXE.htm](pathfinder-bestiary-3-items/t2HIuBRLJiGYJZXE.htm)|At-Will Spells|Sorts à volonté|libre|
|[t4NelvHMno5EsQBb.htm](pathfinder-bestiary-3-items/t4NelvHMno5EsQBb.htm)|Penanggalan Bile|Bile de penanggalan|libre|
|[T4S3miQ63r06a6Bu.htm](pathfinder-bestiary-3-items/T4S3miQ63r06a6Bu.htm)|Low-Light Vision|Vision nocturne|libre|
|[T52SfDBkv0NA6Q8o.htm](pathfinder-bestiary-3-items/T52SfDBkv0NA6Q8o.htm)|Pincer|Pince|libre|
|[t6e0LBuCBj0cSlzG.htm](pathfinder-bestiary-3-items/t6e0LBuCBj0cSlzG.htm)|Ward|Pupille|libre|
|[T6ZAqc1mMzmpUCtm.htm](pathfinder-bestiary-3-items/T6ZAqc1mMzmpUCtm.htm)|Barbed Net|Filet barbelé|libre|
|[t73fUopYHfJiDPvy.htm](pathfinder-bestiary-3-items/t73fUopYHfJiDPvy.htm)|Grab|Empoignade/Agrippement|libre|
|[T7cl6r9Q9cdH7q2Q.htm](pathfinder-bestiary-3-items/T7cl6r9Q9cdH7q2Q.htm)|Anchored Soul|Âme ancrée|libre|
|[t9cow1t47Ns4t0H0.htm](pathfinder-bestiary-3-items/t9cow1t47Ns4t0H0.htm)|Ravenspeaker|langage des corbeaux|libre|
|[TAMn8gMkkXRH5ctl.htm](pathfinder-bestiary-3-items/TAMn8gMkkXRH5ctl.htm)|Darkvision|Vision dans le noir|libre|
|[tAmUn1fmHBzAZrMX.htm](pathfinder-bestiary-3-items/tAmUn1fmHBzAZrMX.htm)|Arrow Volley|Volée de flèches|libre|
|[tasMLbX195fSmOL7.htm](pathfinder-bestiary-3-items/tasMLbX195fSmOL7.htm)|Engineering Lore|Connaissance de l'ingénierie|officielle|
|[tAtaZY5U93gi2tUw.htm](pathfinder-bestiary-3-items/tAtaZY5U93gi2tUw.htm)|Leech Moisture|Absorber l'humidité|libre|
|[tauTat1vId2N5H4u.htm](pathfinder-bestiary-3-items/tauTat1vId2N5H4u.htm)|Bite|Morsure|officielle|
|[Taw4K2MqiOVhIddu.htm](pathfinder-bestiary-3-items/Taw4K2MqiOVhIddu.htm)|Pain Touch|Contact douloureux|libre|
|[Tb6i1oczFczW6Kyf.htm](pathfinder-bestiary-3-items/Tb6i1oczFczW6Kyf.htm)|Jaws|Mâchoires|libre|
|[TBMl5Ts5TOwuhfd4.htm](pathfinder-bestiary-3-items/TBMl5Ts5TOwuhfd4.htm)|+2 Circumstance to All Saves vs. Disease|bonus de circonstances de +2 aux JdS contre la maladie|libre|
|[tBtFLroAVGw32fIG.htm](pathfinder-bestiary-3-items/tBtFLroAVGw32fIG.htm)|Negative Healing|Guérison négative|libre|
|[tC5SlnihlaxwiNPE.htm](pathfinder-bestiary-3-items/tC5SlnihlaxwiNPE.htm)|Horn|Corne|libre|
|[TcyFcfEZxlVqc3lO.htm](pathfinder-bestiary-3-items/TcyFcfEZxlVqc3lO.htm)|At-Will Spells|Sorts à volonté|libre|
|[td3gay0LzawYp5Ff.htm](pathfinder-bestiary-3-items/td3gay0LzawYp5Ff.htm)|Death Throes|Agonie fatale|libre|
|[td7f48qGt23BZNFA.htm](pathfinder-bestiary-3-items/td7f48qGt23BZNFA.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[Tdbu943P6LKUO8bz.htm](pathfinder-bestiary-3-items/Tdbu943P6LKUO8bz.htm)|Arcane Prepared Spells|Sorts arcaniques préparés|libre|
|[Te1JpsOS99z9IDyD.htm](pathfinder-bestiary-3-items/Te1JpsOS99z9IDyD.htm)|Cone of Chalk|Cône de craie|libre|
|[tetpWKxbpwLYXdRP.htm](pathfinder-bestiary-3-items/tetpWKxbpwLYXdRP.htm)|Improved Grab|Empoignade/Agrippement améliorés|libre|
|[TF0po3aZY65Jw1V4.htm](pathfinder-bestiary-3-items/TF0po3aZY65Jw1V4.htm)|Suggestion (At Will)|Suggestion (À volonté)|officielle|
|[tF1tkro4vKNfKEDE.htm](pathfinder-bestiary-3-items/tF1tkro4vKNfKEDE.htm)|Spiritual Rope|Corde spirituelle|libre|
|[TFCVPbBolzO2jAdu.htm](pathfinder-bestiary-3-items/TFCVPbBolzO2jAdu.htm)|Knockdown|Renversement|libre|
|[TFFEZk6wOV2NQ6BA.htm](pathfinder-bestiary-3-items/TFFEZk6wOV2NQ6BA.htm)|Hoof|Sabot|libre|
|[TFIM3DwopeaNPL4N.htm](pathfinder-bestiary-3-items/TFIM3DwopeaNPL4N.htm)|Darkvision|Vision dans le noir|libre|
|[TgCafFeowTgKBPzr.htm](pathfinder-bestiary-3-items/TgCafFeowTgKBPzr.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|libre|
|[TGEeTa0OpK8ohmMq.htm](pathfinder-bestiary-3-items/TGEeTa0OpK8ohmMq.htm)|Athletics|Athlétisme|libre|
|[tgfeTgJO1gvajbtU.htm](pathfinder-bestiary-3-items/tgfeTgJO1gvajbtU.htm)|Dimension of Time Lore|Connaissance de la Dimension du temps|libre|
|[TgpF2GlBi4HH9xoC.htm](pathfinder-bestiary-3-items/TgpF2GlBi4HH9xoC.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|libre|
|[tHyoisfllt6q3L0n.htm](pathfinder-bestiary-3-items/tHyoisfllt6q3L0n.htm)|Fed by Water|Renforcé par l'eau|libre|
|[Ti3e0CkEDJSOuOJy.htm](pathfinder-bestiary-3-items/Ti3e0CkEDJSOuOJy.htm)|Ferocity|Férocité|libre|
|[ti9wuWwyYalyQsgW.htm](pathfinder-bestiary-3-items/ti9wuWwyYalyQsgW.htm)|Tongue|Langue|libre|
|[Tiw7WqKClIOO7liK.htm](pathfinder-bestiary-3-items/Tiw7WqKClIOO7liK.htm)|Rearward Rush|Marche arrière|libre|
|[TjBokB0HyB1Xj2dM.htm](pathfinder-bestiary-3-items/TjBokB0HyB1Xj2dM.htm)|Puppetmaster|Maître marionnettiste|libre|
|[tJCrBl4wNM7ey8KT.htm](pathfinder-bestiary-3-items/tJCrBl4wNM7ey8KT.htm)|Paralysis|Paralysie|libre|
|[TjfSgZMdZR3uPTrF.htm](pathfinder-bestiary-3-items/TjfSgZMdZR3uPTrF.htm)|Dimension Door (At Will)|Porte dimensionnelle (À volonté)|officielle|
|[TjHnriTSE9y88Oaj.htm](pathfinder-bestiary-3-items/TjHnriTSE9y88Oaj.htm)|Purple Pox|Vérole pourpre|libre|
|[TJq1OPO5CFe7Ww6A.htm](pathfinder-bestiary-3-items/TJq1OPO5CFe7Ww6A.htm)|Fist|Poing|libre|
|[tkG1nJtu7gnW9eby.htm](pathfinder-bestiary-3-items/tkG1nJtu7gnW9eby.htm)|Outrageous Hat|Chapeau extravagant|libre|
|[tLkLt02YQsb0Ahfo.htm](pathfinder-bestiary-3-items/tLkLt02YQsb0Ahfo.htm)|Low-Light Vision|Vision nocturne|libre|
|[TlwNO1rmOqaNw0W7.htm](pathfinder-bestiary-3-items/TlwNO1rmOqaNw0W7.htm)|Bond with Mortal|Lié à un Mortel|libre|
|[TM9l7vqFbUf1UCLT.htm](pathfinder-bestiary-3-items/TM9l7vqFbUf1UCLT.htm)|Negative Healing|Guérison négative|libre|
|[TmaCh6JDsKX35pc2.htm](pathfinder-bestiary-3-items/TmaCh6JDsKX35pc2.htm)|Upper Jaw|Mâchoires supérieure|libre|
|[TnfM5nNHrq8AMX9L.htm](pathfinder-bestiary-3-items/TnfM5nNHrq8AMX9L.htm)|Tremorsense (Imprecise) within their entire bound granary or storeroom|Perceptions des vibrations (imprecise) au sein de son grenier ou de sa réserve lié|libre|
|[tNH3T8aLwLgqsb4C.htm](pathfinder-bestiary-3-items/tNH3T8aLwLgqsb4C.htm)|Scintillating Claw|Griffe scintillante|libre|
|[TnmeD4Xchfk7n9oy.htm](pathfinder-bestiary-3-items/TnmeD4Xchfk7n9oy.htm)|Veil of Lies|Voile de mensonges|libre|
|[tnQ4cIcOMWuGbaPY.htm](pathfinder-bestiary-3-items/tnQ4cIcOMWuGbaPY.htm)|Swift Staff Strike|Frappes de bâton rapide|libre|
|[TnYQrIo30Kk8l4BV.htm](pathfinder-bestiary-3-items/TnYQrIo30Kk8l4BV.htm)|Temporal Sense|Perception temporelle|libre|
|[To0rnGkFKHIi4i3T.htm](pathfinder-bestiary-3-items/To0rnGkFKHIi4i3T.htm)|Echolocation (Precise) 20 feet|Écholocalisation 6 m (précis)|libre|
|[tODukcahVjECiTcK.htm](pathfinder-bestiary-3-items/tODukcahVjECiTcK.htm)|Ghostly Light Mace|Masse d'armes légères spectral|libre|
|[Tp53J4QE9RBvdlCd.htm](pathfinder-bestiary-3-items/Tp53J4QE9RBvdlCd.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[tpcdOVPJoLXH4D7L.htm](pathfinder-bestiary-3-items/tpcdOVPJoLXH4D7L.htm)|Planar Binding (Doesn't Require Secondary Casters)|Contrat planaire (Ne nécessite pas d'incantateur secondaire)|libre|
|[tpcpAmqxc53jhCQ7.htm](pathfinder-bestiary-3-items/tpcpAmqxc53jhCQ7.htm)|Talon|Serre|libre|
|[tqdfXRytHABtgkJ4.htm](pathfinder-bestiary-3-items/tqdfXRytHABtgkJ4.htm)|Devour Tail|Dévoration de la queue|libre|
|[TQhgfFteEWb6zzlT.htm](pathfinder-bestiary-3-items/TQhgfFteEWb6zzlT.htm)|Curl Up|S'enrouler|libre|
|[tqRZXY6ERk4h1lPa.htm](pathfinder-bestiary-3-items/tqRZXY6ERk4h1lPa.htm)|Nondetection (At Will) (Self Only)|Antidétection (À volonté) (soi uniquement)|libre|
|[tqxAfXEcZbDZLdxn.htm](pathfinder-bestiary-3-items/tqxAfXEcZbDZLdxn.htm)|Darkvision|Vision dans le noir|libre|
|[tR481pq7xrt35Xrw.htm](pathfinder-bestiary-3-items/tR481pq7xrt35Xrw.htm)|Frightful Presence|Présence terrifiante|libre|
|[TRofD56vepZwhI9C.htm](pathfinder-bestiary-3-items/TRofD56vepZwhI9C.htm)|Draconic Momentum|Impulsion draconique|libre|
|[TROzn3iLJ2A6fcpj.htm](pathfinder-bestiary-3-items/TROzn3iLJ2A6fcpj.htm)|At-Will Spells|Sorts à volonté|libre|
|[TRPmSholsdyNcMiC.htm](pathfinder-bestiary-3-items/TRPmSholsdyNcMiC.htm)|Guardian Monolith|Gardien monolithique|libre|
|[Ts3F6FIbPPOUlZZJ.htm](pathfinder-bestiary-3-items/Ts3F6FIbPPOUlZZJ.htm)|Pass Without Trace (Constant)|Passage sans trace (constant)|officielle|
|[TS6AnRbKZab05oJP.htm](pathfinder-bestiary-3-items/TS6AnRbKZab05oJP.htm)|Fly (At Will)|Vol (À volonté)|libre|
|[tsZ8GnCQamaRzMs3.htm](pathfinder-bestiary-3-items/tsZ8GnCQamaRzMs3.htm)|Darkvision|Vision dans le noir|libre|
|[tT4vLmSN3IsZ2yur.htm](pathfinder-bestiary-3-items/tT4vLmSN3IsZ2yur.htm)|Four-Fanged Assault|Assaut à quatre têtes|libre|
|[ttBTEeYxisLmK0oL.htm](pathfinder-bestiary-3-items/ttBTEeYxisLmK0oL.htm)|Claw|Griffe|libre|
|[TTmqj0RzBKjYpDRC.htm](pathfinder-bestiary-3-items/TTmqj0RzBKjYpDRC.htm)|Telepathy 300 feet|Télépathie 90 mètres|libre|
|[TTotdT50Y6eFIsHY.htm](pathfinder-bestiary-3-items/TTotdT50Y6eFIsHY.htm)|Jaws|Mâchoires|libre|
|[tTRbrpeMS0tQXfb1.htm](pathfinder-bestiary-3-items/tTRbrpeMS0tQXfb1.htm)|Tremorsense (Imprecise) 60 feet|Perception des vibrations 18 m (imprécis)|libre|
|[tuqKOrUDPUmRlE6Q.htm](pathfinder-bestiary-3-items/tuqKOrUDPUmRlE6Q.htm)|Primal Purpose|But primordial|libre|
|[TUW5DOM2gHCfHxkX.htm](pathfinder-bestiary-3-items/TUW5DOM2gHCfHxkX.htm)|Air Walk (Constant)|Marche dans les airs (constant)|officielle|
|[tvG7g3H6RsfVKvsw.htm](pathfinder-bestiary-3-items/tvG7g3H6RsfVKvsw.htm)|Draconic Momentum|Impulsion draconique|libre|
|[TwivoTQWKF6KVynb.htm](pathfinder-bestiary-3-items/TwivoTQWKF6KVynb.htm)|Ransack|Pillage|libre|
|[tWOZ4o9V5cviyUJD.htm](pathfinder-bestiary-3-items/tWOZ4o9V5cviyUJD.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[Twp10eJAJOK8lgbO.htm](pathfinder-bestiary-3-items/Twp10eJAJOK8lgbO.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|libre|
|[tx21vhxchyBGHxOD.htm](pathfinder-bestiary-3-items/tx21vhxchyBGHxOD.htm)|Touch of Idiocy (At Will)|Idiotie (À volonté)|libre|
|[tX7g78Vle2NcjulW.htm](pathfinder-bestiary-3-items/tX7g78Vle2NcjulW.htm)|Flying Strafe|Griffes volantes|libre|
|[TxmC3aaWzECZldPm.htm](pathfinder-bestiary-3-items/TxmC3aaWzECZldPm.htm)|Bone Spike|Pique d'os|libre|
|[tXq7aF0UVeeMJx53.htm](pathfinder-bestiary-3-items/tXq7aF0UVeeMJx53.htm)|Grab|Empoignade/Agrippement|libre|
|[txykeoHQFVm7Kix0.htm](pathfinder-bestiary-3-items/txykeoHQFVm7Kix0.htm)|At-Will Spells|Sorts à volonté|libre|
|[ty02a1CedQJfgYkL.htm](pathfinder-bestiary-3-items/ty02a1CedQJfgYkL.htm)|Wild Swing|Attaque sauvage|libre|
|[tYt3A9hKDpzLb05E.htm](pathfinder-bestiary-3-items/tYt3A9hKDpzLb05E.htm)|Deception|Duperie|libre|
|[tZtHn0wl4msKd5S2.htm](pathfinder-bestiary-3-items/tZtHn0wl4msKd5S2.htm)|Telepathy 60 feet|Télépathie 18 m|libre|
|[u0EOvM37SL9GUjJu.htm](pathfinder-bestiary-3-items/u0EOvM37SL9GUjJu.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[u191bxAWSFhNXMic.htm](pathfinder-bestiary-3-items/u191bxAWSFhNXMic.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[u1oQzKxAI1vhe2bM.htm](pathfinder-bestiary-3-items/u1oQzKxAI1vhe2bM.htm)|Confectionery Lore|Connaissance de la confiserie|libre|
|[U1W2qEPWeEon0DtN.htm](pathfinder-bestiary-3-items/U1W2qEPWeEon0DtN.htm)|Recall Reflection|Se souvenir du reflet|libre|
|[U2e7s0f0SwOH3IDu.htm](pathfinder-bestiary-3-items/U2e7s0f0SwOH3IDu.htm)|Air Walk (Constant)|Marche dans les airs (constant)|officielle|
|[u3ndMdQRfiAH7Z7J.htm](pathfinder-bestiary-3-items/u3ndMdQRfiAH7Z7J.htm)|Phalanx Fighter|Guerrier de phalange|libre|
|[u4b35ICyQPSv3OKF.htm](pathfinder-bestiary-3-items/u4b35ICyQPSv3OKF.htm)|Scimitar|Cimeterre|libre|
|[u4eux5FgWu5TDOdj.htm](pathfinder-bestiary-3-items/u4eux5FgWu5TDOdj.htm)|Positive Nature|Nature positive|libre|
|[U4IAJfb86qQA800m.htm](pathfinder-bestiary-3-items/U4IAJfb86qQA800m.htm)|Pincer|Pince|libre|
|[u4uyw8rTejQnjDhT.htm](pathfinder-bestiary-3-items/u4uyw8rTejQnjDhT.htm)|Tail|Queue|libre|
|[u5cLd0qEPkNofLzr.htm](pathfinder-bestiary-3-items/u5cLd0qEPkNofLzr.htm)|Clutching Cobbles|Pavés agrippants|libre|
|[u5VmraPdTduQVeUa.htm](pathfinder-bestiary-3-items/u5VmraPdTduQVeUa.htm)|Breach Vulnerability|Vulnérabilité de brèche|libre|
|[U615W2wUWCOrTcC6.htm](pathfinder-bestiary-3-items/U615W2wUWCOrTcC6.htm)|Splinter Sycophant|Éclat du flagorneur|libre|
|[U74B3Cx6dSGCsSHP.htm](pathfinder-bestiary-3-items/U74B3Cx6dSGCsSHP.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|libre|
|[u8fKUkjNFuyL7a5x.htm](pathfinder-bestiary-3-items/u8fKUkjNFuyL7a5x.htm)|Darkvision|Vision dans le noir|libre|
|[U8FXRYDBOA6S2wc1.htm](pathfinder-bestiary-3-items/U8FXRYDBOA6S2wc1.htm)|Darkness (At Will)|Ténèbres (À volonté)|officielle|
|[U9ViaHzmhkqzKJ3Q.htm](pathfinder-bestiary-3-items/U9ViaHzmhkqzKJ3Q.htm)|See Invisibility (Constant)|Détection de l'invisibilité (constant)|officielle|
|[uaFUxzIXa5N9QFrp.htm](pathfinder-bestiary-3-items/uaFUxzIXa5N9QFrp.htm)|Fly (Constant)|Vol (constant)|officielle|
|[uara7mD6sRfBQr3k.htm](pathfinder-bestiary-3-items/uara7mD6sRfBQr3k.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|libre|
|[UBAt1dB8VrLmKvtW.htm](pathfinder-bestiary-3-items/UBAt1dB8VrLmKvtW.htm)|Holy Weaponry|Armement saint|libre|
|[uBnRvlWD0zCwJWoX.htm](pathfinder-bestiary-3-items/uBnRvlWD0zCwJWoX.htm)|Grab|Empoignade/Agrippement|libre|
|[uC8vf4vEv2zFOWgo.htm](pathfinder-bestiary-3-items/uC8vf4vEv2zFOWgo.htm)|Jaws|Mâchoires|libre|
|[uCO7vGNGI7hr4h24.htm](pathfinder-bestiary-3-items/uCO7vGNGI7hr4h24.htm)|Stealth|Discrétion|libre|
|[uCxQUNy75OANDWsm.htm](pathfinder-bestiary-3-items/uCxQUNy75OANDWsm.htm)|Adamantine Jaws|Mâchoires en adamantium|libre|
|[uD92m7U1xCZSE1U8.htm](pathfinder-bestiary-3-items/uD92m7U1xCZSE1U8.htm)|At-Will Spells|Sorts à volonté|libre|
|[udfFYOsTo90aVSzS.htm](pathfinder-bestiary-3-items/udfFYOsTo90aVSzS.htm)|Throw Rock|Projection de rocher|libre|
|[UDgM4FtV95bM8Jo2.htm](pathfinder-bestiary-3-items/UDgM4FtV95bM8Jo2.htm)|Sprint|Sprint|libre|
|[uDHQXwHJ5JXGBLA0.htm](pathfinder-bestiary-3-items/uDHQXwHJ5JXGBLA0.htm)|Fire Shield (Constant)|Bouclier de feu (constant)|officielle|
|[uDO2KFHi9xxyZa8l.htm](pathfinder-bestiary-3-items/uDO2KFHi9xxyZa8l.htm)|Trample|Piétinement|libre|
|[uDYAOTcNICSCyK3S.htm](pathfinder-bestiary-3-items/uDYAOTcNICSCyK3S.htm)|Shadow Invisibility|Invisibilité des ombres|libre|
|[uEckCwzDaV3V8orK.htm](pathfinder-bestiary-3-items/uEckCwzDaV3V8orK.htm)|Light Blindness|Aveuglé par la lumière|libre|
|[UeMKUFMnWNbhRbEj.htm](pathfinder-bestiary-3-items/UeMKUFMnWNbhRbEj.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[UEtsgHyvVOtngUcs.htm](pathfinder-bestiary-3-items/UEtsgHyvVOtngUcs.htm)|Low-Light Vision|Vision nocturne|libre|
|[uf75VmCg2lXJXIS6.htm](pathfinder-bestiary-3-items/uf75VmCg2lXJXIS6.htm)|Spore Pop|Éclatement de spores|libre|
|[UFxr3UoWoyUD8Oh4.htm](pathfinder-bestiary-3-items/UFxr3UoWoyUD8Oh4.htm)|Feral Gnaw|Rongement féroce|libre|
|[UGDbrE4flLByNgPe.htm](pathfinder-bestiary-3-items/UGDbrE4flLByNgPe.htm)|Tusk|Défense|libre|
|[UGRYo2y98N0QxtVh.htm](pathfinder-bestiary-3-items/UGRYo2y98N0QxtVh.htm)|Shadowbind|Lien d'ombre|libre|
|[UH1P5Ajkj0bfwuUa.htm](pathfinder-bestiary-3-items/UH1P5Ajkj0bfwuUa.htm)|+2 Status to All Saves vs. Curses|bonus de statut de +2 aux JdS contre les malédictions|libre|
|[UHVntHDP7laXevSA.htm](pathfinder-bestiary-3-items/UHVntHDP7laXevSA.htm)|+2 Status to All Saves vs. Magic|bonus de statut de +2 aux JdS contre la magie|libre|
|[UjrUewvwpknHeqGz.htm](pathfinder-bestiary-3-items/UjrUewvwpknHeqGz.htm)|Flaming Composite Longbow|Arc long composite enflammé|libre|
|[UJX7CIiJGskPMXPv.htm](pathfinder-bestiary-3-items/UJX7CIiJGskPMXPv.htm)|Consecration Vulnerability|Vulnérable à la consécration|libre|
|[UK9gsjbHYgmKZze9.htm](pathfinder-bestiary-3-items/UK9gsjbHYgmKZze9.htm)|Self-Destruct|Autodestruction|libre|
|[UKAw8CcCGNk7VFkB.htm](pathfinder-bestiary-3-items/UKAw8CcCGNk7VFkB.htm)|At-Will Spells|Sorts à volonté|libre|
|[UkgUfv2gU8Cnz7Dk.htm](pathfinder-bestiary-3-items/UkgUfv2gU8Cnz7Dk.htm)|Central Weapon|Arme centrale|libre|
|[ulnI58GokIE09FJW.htm](pathfinder-bestiary-3-items/ulnI58GokIE09FJW.htm)|Fist|Poing|libre|
|[ULp1f7aziCabSbdr.htm](pathfinder-bestiary-3-items/ULp1f7aziCabSbdr.htm)|Claw|Griffe|libre|
|[uLwnIHpENAqfjCwz.htm](pathfinder-bestiary-3-items/uLwnIHpENAqfjCwz.htm)|Jaws|Mâchoires|libre|
|[uLZvd57CejMavFox.htm](pathfinder-bestiary-3-items/uLZvd57CejMavFox.htm)|Eurypterid Venom|Venin d'euryptéride|libre|
|[uM4bCzmiegIXPcJf.htm](pathfinder-bestiary-3-items/uM4bCzmiegIXPcJf.htm)|Meld into Stone (At Will)|Fusion dans la pierre (À volonté)|officielle|
|[uM7SvgL7DwtCruSv.htm](pathfinder-bestiary-3-items/uM7SvgL7DwtCruSv.htm)|Leg|Jambe|libre|
|[Umi4PGsADEgpFaTb.htm](pathfinder-bestiary-3-items/Umi4PGsADEgpFaTb.htm)|Skip Between|Alterner|libre|
|[UmmKeGJIw44WAK7i.htm](pathfinder-bestiary-3-items/UmmKeGJIw44WAK7i.htm)|Fell Shriek|Cri déchirant|libre|
|[uMNXrK8Is6PAzNEB.htm](pathfinder-bestiary-3-items/uMNXrK8Is6PAzNEB.htm)|Scuttle Away|Déguerpir|libre|
|[UmrTl2VeCsF3GsBm.htm](pathfinder-bestiary-3-items/UmrTl2VeCsF3GsBm.htm)|Plantsense 60 feet|Perception des plante 18 m|libre|
|[uN385UW65mQl9tQb.htm](pathfinder-bestiary-3-items/uN385UW65mQl9tQb.htm)|Occult Prepared Spells|Sorts occultes préparés|libre|
|[unEjKFEEILdk8x7m.htm](pathfinder-bestiary-3-items/unEjKFEEILdk8x7m.htm)|Grab|Empoignade/Agrippement|libre|
|[UNgk0PBDduIdcbGj.htm](pathfinder-bestiary-3-items/UNgk0PBDduIdcbGj.htm)|Ocean Lore|Connaissance des océans|libre|
|[unMEeRrwlVMzjW7n.htm](pathfinder-bestiary-3-items/unMEeRrwlVMzjW7n.htm)|Mind Reading (At Will)|Lecture des pensées (À volonté)|officielle|
|[Unq1mYTZ7Jk2AHCF.htm](pathfinder-bestiary-3-items/Unq1mYTZ7Jk2AHCF.htm)|Primal Prepared Spells|Sorts primordiaux préparés|libre|
|[UnTBAVC6J8CCjIdl.htm](pathfinder-bestiary-3-items/UnTBAVC6J8CCjIdl.htm)|Rotting Flesh|Chair en décomposition|libre|
|[UOOf6xLzb7FqQtsU.htm](pathfinder-bestiary-3-items/UOOf6xLzb7FqQtsU.htm)|Offal|Abats|libre|
|[upknbVcHKdFGHUwR.htm](pathfinder-bestiary-3-items/upknbVcHKdFGHUwR.htm)|Glorious Fist|Poing glorieux|libre|
|[UpN12Ls7Ii9P965x.htm](pathfinder-bestiary-3-items/UpN12Ls7Ii9P965x.htm)|Troop Defenses|Défenses des troupes|libre|
|[UPSTethShZCl8vHY.htm](pathfinder-bestiary-3-items/UPSTethShZCl8vHY.htm)|Fist|Poing|libre|
|[Upzw2dn4HSqTYIma.htm](pathfinder-bestiary-3-items/Upzw2dn4HSqTYIma.htm)|Home Guardian|Gardien de maison|libre|
|[UQ5bqrNhcoxqd2TA.htm](pathfinder-bestiary-3-items/UQ5bqrNhcoxqd2TA.htm)|Change Shape|Changement de forme|libre|
|[uQaUx0jJSihzX8ob.htm](pathfinder-bestiary-3-items/uQaUx0jJSihzX8ob.htm)|Scent (Imprecise) 60 feet|Odorat 18 m (imprécis)|libre|
|[UQgqIDpkTWprpPzL.htm](pathfinder-bestiary-3-items/UQgqIDpkTWprpPzL.htm)|Low-Light Vision|Vision nocturne|libre|
|[UQmOfNcYdQJoHq8o.htm](pathfinder-bestiary-3-items/UQmOfNcYdQJoHq8o.htm)|Avenging Bite|Morsure vengeresse|libre|
|[uQmt7JbKVVMAsd5R.htm](pathfinder-bestiary-3-items/uQmt7JbKVVMAsd5R.htm)|Grab|Empoignade/Agrippement|libre|
|[uQOW7jTQlM2mFtVU.htm](pathfinder-bestiary-3-items/uQOW7jTQlM2mFtVU.htm)|Endure Elements (Self Only)|Endurance aux éléments (Soi uniquement)|libre|
|[UR0KFHIUkZ47JTL0.htm](pathfinder-bestiary-3-items/UR0KFHIUkZ47JTL0.htm)|Athletics|Athlétisme|libre|
|[URIeoHiQX0CLnpUA.htm](pathfinder-bestiary-3-items/URIeoHiQX0CLnpUA.htm)|Jaws|Mâchoires|libre|
|[UrsYHXoFzbr0aW0A.htm](pathfinder-bestiary-3-items/UrsYHXoFzbr0aW0A.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[Urwp7vLcfy6avc98.htm](pathfinder-bestiary-3-items/Urwp7vLcfy6avc98.htm)|Grab|Empoignade/Agrippement|libre|
|[uS98HfKqTgAefSsN.htm](pathfinder-bestiary-3-items/uS98HfKqTgAefSsN.htm)|Devour Life|Engloutissement de la vie|libre|
|[usTqKqK0mzh2dkzZ.htm](pathfinder-bestiary-3-items/usTqKqK0mzh2dkzZ.htm)|Scroll of Dimensional Anchor (Level 4)|Parchemin d'Ancre dimensionnelle (Niveau 4)|libre|
|[USVxY3qpSEozrQU8.htm](pathfinder-bestiary-3-items/USVxY3qpSEozrQU8.htm)|Fist|Poing|libre|
|[uubvbSioie58su3V.htm](pathfinder-bestiary-3-items/uubvbSioie58su3V.htm)|Feeding Frenzy|Repas frénétique|libre|
|[uUe3OiXyQgpCeszE.htm](pathfinder-bestiary-3-items/uUe3OiXyQgpCeszE.htm)|Lore (Its Home Settlement or Country)|Connaissance (son pays ou sa communauté natale)|libre|
|[UuZpaLnXCCuS5ijA.htm](pathfinder-bestiary-3-items/UuZpaLnXCCuS5ijA.htm)|Concussive Blow|Coup de bec assommant|libre|
|[uVHi6RIyqbXq9V9N.htm](pathfinder-bestiary-3-items/uVHi6RIyqbXq9V9N.htm)|Regurgitated Wrath|Colère régurgitée|libre|
|[Uw2yhPghf93MQk8d.htm](pathfinder-bestiary-3-items/Uw2yhPghf93MQk8d.htm)|Constrict|Constriction|libre|
|[UwDGN0t984W2aQuV.htm](pathfinder-bestiary-3-items/UwDGN0t984W2aQuV.htm)|Paired Strike|Frappes jumelées|libre|
|[uWPyQC3rQMHc7uh1.htm](pathfinder-bestiary-3-items/uWPyQC3rQMHc7uh1.htm)|Hypercognition (At Will)|Hypercognition (À volonté)|officielle|
|[UWTdQ7IdEAOO2D4Q.htm](pathfinder-bestiary-3-items/UWTdQ7IdEAOO2D4Q.htm)|Champion Focus Spells|Sorts focalisés de champion|libre|
|[UXcwYHarnmB2msA1.htm](pathfinder-bestiary-3-items/UXcwYHarnmB2msA1.htm)|Lifesense 60 feet|Perception de la vie 18 m|libre|
|[UXEJVTlMCdSK3nis.htm](pathfinder-bestiary-3-items/UXEJVTlMCdSK3nis.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[UxfNIf8feaMNNjuy.htm](pathfinder-bestiary-3-items/UxfNIf8feaMNNjuy.htm)|Jaws|Mâchoires|libre|
|[uxgEQgxtbzCI4zVo.htm](pathfinder-bestiary-3-items/uxgEQgxtbzCI4zVo.htm)|Athletics|Athlétisme|libre|
|[uxGxWm5nbZ7Oxq7U.htm](pathfinder-bestiary-3-items/uxGxWm5nbZ7Oxq7U.htm)|Telepathy 200 feet|Télépathie 60 mètres|libre|
|[UxlcIKPaetxhxrB9.htm](pathfinder-bestiary-3-items/UxlcIKPaetxhxrB9.htm)|Stench|Puanteur|libre|
|[Uy3YAPSoQX8UsHK3.htm](pathfinder-bestiary-3-items/Uy3YAPSoQX8UsHK3.htm)|Athletics|Athlétisme|libre|
|[uybrNwUFDqMCKZ3R.htm](pathfinder-bestiary-3-items/uybrNwUFDqMCKZ3R.htm)|Tail|Queue|libre|
|[UYKJzdzZCg9KRzcD.htm](pathfinder-bestiary-3-items/UYKJzdzZCg9KRzcD.htm)|Easy to Call|Facile à invoquer|libre|
|[uyLFQBi1NYtMlqCF.htm](pathfinder-bestiary-3-items/uyLFQBi1NYtMlqCF.htm)|Tremorsense 60 feet|Perception des vibrations 18 m|libre|
|[uYmFPiCSJytUnLkG.htm](pathfinder-bestiary-3-items/uYmFPiCSJytUnLkG.htm)|Fatal Fantasia|Fantasie fatale|libre|
|[Uytd5UdwOMgJqTS5.htm](pathfinder-bestiary-3-items/Uytd5UdwOMgJqTS5.htm)|Tongues (Constant)|Don des langues (constant)|officielle|
|[UYwd2bv5bCYGaPAI.htm](pathfinder-bestiary-3-items/UYwd2bv5bCYGaPAI.htm)|Silver Strike|Frappe d'argent|libre|
|[uZG6teo4GWKlxaiD.htm](pathfinder-bestiary-3-items/uZG6teo4GWKlxaiD.htm)|Punishing Strike|Frappe punitive|libre|
|[UZivbag22tDFAKdd.htm](pathfinder-bestiary-3-items/UZivbag22tDFAKdd.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[UzKSlqvopdc1rGlh.htm](pathfinder-bestiary-3-items/UzKSlqvopdc1rGlh.htm)|Spit|Crachat|libre|
|[uzLf2ezYP1JPXfrQ.htm](pathfinder-bestiary-3-items/uzLf2ezYP1JPXfrQ.htm)|Attack of Opportunity (Jaws Only)|Attaque d'opportunité (Mâchoires uniquement)|libre|
|[v0UApfvf8TYOWcOQ.htm](pathfinder-bestiary-3-items/v0UApfvf8TYOWcOQ.htm)|Constant Spells|Sorts constants|libre|
|[V1DW1BLRqkZoZJFG.htm](pathfinder-bestiary-3-items/V1DW1BLRqkZoZJFG.htm)|Shape Stone (At Will)|Façonnage de la pierre (À volonté)|officielle|
|[V1IL4PehVXrysvbs.htm](pathfinder-bestiary-3-items/V1IL4PehVXrysvbs.htm)|Darkvision|Vision dans le noir|libre|
|[V2ghyimSN4jxOywM.htm](pathfinder-bestiary-3-items/V2ghyimSN4jxOywM.htm)|Talon|Serre|libre|
|[V2NsuzlWxUrYA0mE.htm](pathfinder-bestiary-3-items/V2NsuzlWxUrYA0mE.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[v2RqXgBeNgkZ0DtA.htm](pathfinder-bestiary-3-items/v2RqXgBeNgkZ0DtA.htm)|Wind-Up|Remonter un dispositif|libre|
|[V3rzyrOOf8B6y3I2.htm](pathfinder-bestiary-3-items/V3rzyrOOf8B6y3I2.htm)|Silver Scissors|Ciseaux en argent|libre|
|[v43kRV9YPyaSeo23.htm](pathfinder-bestiary-3-items/v43kRV9YPyaSeo23.htm)|Air Walk (Constant)|Marche dans les airs (constant)|officielle|
|[V4TijnwUl9hbxsnI.htm](pathfinder-bestiary-3-items/V4TijnwUl9hbxsnI.htm)|Greatpick|+3,greaterStriking|Grand pic de guerre de frappe supérieure +3|libre|
|[V5i5HJfqqedgNK8E.htm](pathfinder-bestiary-3-items/V5i5HJfqqedgNK8E.htm)|Bind Undead (At Will)|Lier un mort-vivant (À volonté)|libre|
|[V6QvQyNmHdNppYYJ.htm](pathfinder-bestiary-3-items/V6QvQyNmHdNppYYJ.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[v79Z6h5Gc46hkIBk.htm](pathfinder-bestiary-3-items/v79Z6h5Gc46hkIBk.htm)|Form Up|Se reformer|libre|
|[v88O0NAy45VWmtTO.htm](pathfinder-bestiary-3-items/v88O0NAy45VWmtTO.htm)|Curse of the Werebat|Malédiction de la chauve-souris garou|libre|
|[v9Q0Mz13HzloiDTp.htm](pathfinder-bestiary-3-items/v9Q0Mz13HzloiDTp.htm)|Dagger|Dague|libre|
|[VAggE1QOFwbQrV0m.htm](pathfinder-bestiary-3-items/VAggE1QOFwbQrV0m.htm)|Grab|Empoignade/Agrippement|libre|
|[vagKbfsnprcn97gn.htm](pathfinder-bestiary-3-items/vagKbfsnprcn97gn.htm)|Flurry of Kicks|Déluge de coups de pied|libre|
|[VAj42GSNyP2at5VR.htm](pathfinder-bestiary-3-items/VAj42GSNyP2at5VR.htm)|Tremorsense (Imprecise) 30 feet|Perception des vibrations 9 m (imprécis)|libre|
|[vAYa9X9DeqTHfehJ.htm](pathfinder-bestiary-3-items/vAYa9X9DeqTHfehJ.htm)|Shrieking Scream|Cris perçants|libre|
|[VB7Ak1txQ1RPwHwJ.htm](pathfinder-bestiary-3-items/VB7Ak1txQ1RPwHwJ.htm)|Construct Armor (Hardness 3)|Armure de créature artificielle (Solidité 3)|libre|
|[VBA2WIH3wy9R2XsO.htm](pathfinder-bestiary-3-items/VBA2WIH3wy9R2XsO.htm)|Kukri|Kukri|libre|
|[vBiVfLo0mr2jIdhC.htm](pathfinder-bestiary-3-items/vBiVfLo0mr2jIdhC.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[vBOtCKqI5yNghuqQ.htm](pathfinder-bestiary-3-items/vBOtCKqI5yNghuqQ.htm)|Low-Light Vision|Vision nocturne|libre|
|[vbSi698j0JxF2fvv.htm](pathfinder-bestiary-3-items/vbSi698j0JxF2fvv.htm)|Negative Healing|Guérison négative|libre|
|[VBxySuyNQGXdtmjL.htm](pathfinder-bestiary-3-items/VBxySuyNQGXdtmjL.htm)|Claw|Griffe|libre|
|[VbzlMz5d1BV2siTI.htm](pathfinder-bestiary-3-items/VbzlMz5d1BV2siTI.htm)|Knockdown|Renversement|libre|
|[vcbazPs6B9bMeBpm.htm](pathfinder-bestiary-3-items/vcbazPs6B9bMeBpm.htm)|Flurry of Blows|Déluges de coups|libre|
|[vct7Th4oUyktL8WO.htm](pathfinder-bestiary-3-items/vct7Th4oUyktL8WO.htm)|Constant Spells|Sorts constants|libre|
|[vdcv4s3CzGpAj5KB.htm](pathfinder-bestiary-3-items/vdcv4s3CzGpAj5KB.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[VdgqOmcdTkgggnVY.htm](pathfinder-bestiary-3-items/VdgqOmcdTkgggnVY.htm)|Dagger|Dague|libre|
|[VdKAGQI4ZZFDhZeL.htm](pathfinder-bestiary-3-items/VdKAGQI4ZZFDhZeL.htm)|Claw|Griffe|libre|
|[VdozlfKy0DHfmOKJ.htm](pathfinder-bestiary-3-items/VdozlfKy0DHfmOKJ.htm)|Flail|Fléau d'armes|libre|
|[vdpzi8I0mzpB3lHw.htm](pathfinder-bestiary-3-items/vdpzi8I0mzpB3lHw.htm)|Constant Spells|Sort constants|libre|
|[vDs8pDlAo0An3Z5L.htm](pathfinder-bestiary-3-items/vDs8pDlAo0An3Z5L.htm)|Negative Healing|Guérison négative|libre|
|[VE0UqyozDof75tsl.htm](pathfinder-bestiary-3-items/VE0UqyozDof75tsl.htm)|Form Up|Se reformer|libre|
|[Ve2k20bE8qSEdyvY.htm](pathfinder-bestiary-3-items/Ve2k20bE8qSEdyvY.htm)|Drain Life|Drain de vie|libre|
|[VE76RnTQLL1Y1ttf.htm](pathfinder-bestiary-3-items/VE76RnTQLL1Y1ttf.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|libre|
|[VECsjpvEAaMDK0IJ.htm](pathfinder-bestiary-3-items/VECsjpvEAaMDK0IJ.htm)|Flying Strafe|Mitraille volante|libre|
|[vewX6T3r92AKkxif.htm](pathfinder-bestiary-3-items/vewX6T3r92AKkxif.htm)|Dark Tapestry Lore|Connaissance de la Sombre Tapisserie|officielle|
|[veXylj85GdkPPrim.htm](pathfinder-bestiary-3-items/veXylj85GdkPPrim.htm)|Swallow Whole|Gober|libre|
|[veYnaiiLoAVleTjO.htm](pathfinder-bestiary-3-items/veYnaiiLoAVleTjO.htm)|Darkvision|Vision dans le noir|libre|
|[vFdekjhomHHOwhBO.htm](pathfinder-bestiary-3-items/vFdekjhomHHOwhBO.htm)|Darkvision|Vision dans le noir|libre|
|[VFkAq790PgfMtp1j.htm](pathfinder-bestiary-3-items/VFkAq790PgfMtp1j.htm)|Low-Light Vision|Vision nocturne|libre|
|[vFTbscR1hFTWc1f3.htm](pathfinder-bestiary-3-items/vFTbscR1hFTWc1f3.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[vFvsiqwCZ9mkVwHL.htm](pathfinder-bestiary-3-items/vFvsiqwCZ9mkVwHL.htm)|True Seeing (Constant)|Vision lucide (constant)|officielle|
|[vg0HZLmdVPcQrpjQ.htm](pathfinder-bestiary-3-items/vg0HZLmdVPcQrpjQ.htm)|Speak with Animals (Constant)|Communication avec les animaux (constant)|officielle|
|[VgbKhZurZd89uLHJ.htm](pathfinder-bestiary-3-items/VgbKhZurZd89uLHJ.htm)|Frightening Rant|Diatribe effrayante|libre|
|[Vge4HZcUTyLH8jKo.htm](pathfinder-bestiary-3-items/Vge4HZcUTyLH8jKo.htm)|Darkvision|Vision dans le noir|libre|
|[vgGl8uaNNQbavIkC.htm](pathfinder-bestiary-3-items/vgGl8uaNNQbavIkC.htm)|Stealth|Discrétion|libre|
|[vGPorxL6swyKt73y.htm](pathfinder-bestiary-3-items/vGPorxL6swyKt73y.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|libre|
|[VgsFe1dtKkzBHRVz.htm](pathfinder-bestiary-3-items/VgsFe1dtKkzBHRVz.htm)|Darkvision|Vision dans le noir|libre|
|[vGTPPE8IbwvNQkbd.htm](pathfinder-bestiary-3-items/vGTPPE8IbwvNQkbd.htm)|Shock Mind|Choc mentaliste|libre|
|[VgzHbzcl0K2wE4qk.htm](pathfinder-bestiary-3-items/VgzHbzcl0K2wE4qk.htm)|Telepathy 60 feet|Télépathie 18 m|libre|
|[Vh1hkSBb3FKa0QjG.htm](pathfinder-bestiary-3-items/Vh1hkSBb3FKa0QjG.htm)|Sumbreiva Huntblade|Couteau de chasse sumbreiva|libre|
|[VHBHFrYc6mpE6sPl.htm](pathfinder-bestiary-3-items/VHBHFrYc6mpE6sPl.htm)|Trawl for Bones|Treuiller des os|libre|
|[vhL7f1KV4YaXn5FV.htm](pathfinder-bestiary-3-items/vhL7f1KV4YaXn5FV.htm)|Darkvision|Vision dans le noir|libre|
|[vhPEduQMUJTBlxzC.htm](pathfinder-bestiary-3-items/vhPEduQMUJTBlxzC.htm)|Shape Void|Façonnage du néant|libre|
|[VHt437pso612ShZb.htm](pathfinder-bestiary-3-items/VHt437pso612ShZb.htm)|Darkvision|Vision dans le noir|libre|
|[VHwtj3724SEQAAlz.htm](pathfinder-bestiary-3-items/VHwtj3724SEQAAlz.htm)|Constant Spells|Sorts constants|libre|
|[VHx2gNusdgS21WWh.htm](pathfinder-bestiary-3-items/VHx2gNusdgS21WWh.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[VhXGyXRgvJQTjUxr.htm](pathfinder-bestiary-3-items/VhXGyXRgvJQTjUxr.htm)|Claw|Griffe|libre|
|[vHyMiWUvIDKSzTO4.htm](pathfinder-bestiary-3-items/vHyMiWUvIDKSzTO4.htm)|Darkvision|Vision dans le noir|libre|
|[vHZ8RpuMQkQsDTHW.htm](pathfinder-bestiary-3-items/vHZ8RpuMQkQsDTHW.htm)|Light Blindness|Aveuglé par la lumière|libre|
|[VI8C7X1hf2SOT0jS.htm](pathfinder-bestiary-3-items/VI8C7X1hf2SOT0jS.htm)|Violent Retort|Riposte violente|libre|
|[vICaR1ONcB3ugGfH.htm](pathfinder-bestiary-3-items/vICaR1ONcB3ugGfH.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|libre|
|[VIDJVH2nBIkzXg2L.htm](pathfinder-bestiary-3-items/VIDJVH2nBIkzXg2L.htm)|Innate Divine Spells|Sorts divins innés|libre|
|[vihivNwS1MIyMHBB.htm](pathfinder-bestiary-3-items/vihivNwS1MIyMHBB.htm)|Rootbound|Enraciné|libre|
|[vIMdth3wX5RnQu2i.htm](pathfinder-bestiary-3-items/vIMdth3wX5RnQu2i.htm)|Sneak Attack|Attaque sournoise|libre|
|[viOxzbPu5sPdBqSc.htm](pathfinder-bestiary-3-items/viOxzbPu5sPdBqSc.htm)|Constrict|Constriction|libre|
|[vJ4WPwSeQegH8KMs.htm](pathfinder-bestiary-3-items/vJ4WPwSeQegH8KMs.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[VjdE8difbmDAhk1V.htm](pathfinder-bestiary-3-items/VjdE8difbmDAhk1V.htm)|Tail|Queue|libre|
|[VjJS4dQI0QyHntez.htm](pathfinder-bestiary-3-items/VjJS4dQI0QyHntez.htm)|Soul Feast|Festin d'âmes|libre|
|[vjjTR5hrPf5pDmGk.htm](pathfinder-bestiary-3-items/vjjTR5hrPf5pDmGk.htm)|Change Shape|Changement de forme|libre|
|[vjPEjP5swzJDQYdF.htm](pathfinder-bestiary-3-items/vjPEjP5swzJDQYdF.htm)|Plane Shift (Self and Mount Only)|Changement de plant (soi et monture uniquement)|libre|
|[VjpfyKmnhYR8Bus6.htm](pathfinder-bestiary-3-items/VjpfyKmnhYR8Bus6.htm)|Regeneration 5 (Deactivated by Acid or Fire)|Régénération 5 (désactivée par Acide et Feu)|libre|
|[Vk1vUceA5furX6Ez.htm](pathfinder-bestiary-3-items/Vk1vUceA5furX6Ez.htm)|Wide Cleave|Affliction élargie|libre|
|[vkAopqi1PCG6CvnD.htm](pathfinder-bestiary-3-items/vkAopqi1PCG6CvnD.htm)|Flame Jump|Porte de flammes|libre|
|[vkQCMGp1H43fc8Jo.htm](pathfinder-bestiary-3-items/vkQCMGp1H43fc8Jo.htm)|Manipulate Luck|Manipulation de la chance|libre|
|[vKSl7FDTI6tQZCio.htm](pathfinder-bestiary-3-items/vKSl7FDTI6tQZCio.htm)|Negative Healing|Guérison négative|libre|
|[vlGXeDzuvihHbasj.htm](pathfinder-bestiary-3-items/vlGXeDzuvihHbasj.htm)|Claw|Griffe|libre|
|[vlieQSRegKDFCdLu.htm](pathfinder-bestiary-3-items/vlieQSRegKDFCdLu.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|libre|
|[vlJE8WHtB9SH2Ca0.htm](pathfinder-bestiary-3-items/vlJE8WHtB9SH2Ca0.htm)|Telepathy 60 feet|Télépathie 18 m|libre|
|[vLM5mXAWGq7oMjMv.htm](pathfinder-bestiary-3-items/vLM5mXAWGq7oMjMv.htm)|Grab|Empoignade/Agrippement|libre|
|[VMapQLoJPfFmzdaS.htm](pathfinder-bestiary-3-items/VMapQLoJPfFmzdaS.htm)|Susceptible to Death|Exposé à la mort|libre|
|[VMiBWybV4muz0sCm.htm](pathfinder-bestiary-3-items/VMiBWybV4muz0sCm.htm)|Contingent Glyph|Glyphe contingence|libre|
|[VmpcHMhX036wRdpf.htm](pathfinder-bestiary-3-items/VmpcHMhX036wRdpf.htm)|Thorns|Épines|libre|
|[vMRN1ON8B5Jk9K9h.htm](pathfinder-bestiary-3-items/vMRN1ON8B5Jk9K9h.htm)|Easy to Call|Facile à invoquer|libre|
|[vMTJr0Wv9CyKUmqi.htm](pathfinder-bestiary-3-items/vMTJr0Wv9CyKUmqi.htm)|Composite Shortbow|Arc court composite|libre|
|[vNKRTp7YasyHRjqH.htm](pathfinder-bestiary-3-items/vNKRTp7YasyHRjqH.htm)|Claw|Griffe|libre|
|[VnUdpwyEF14owCWc.htm](pathfinder-bestiary-3-items/VnUdpwyEF14owCWc.htm)|Grab|Empoignade/Agrippement|libre|
|[vOKfAcHQJQuAlDmX.htm](pathfinder-bestiary-3-items/vOKfAcHQJQuAlDmX.htm)|Low-Light Vision|Vision nocturne|libre|
|[vOU0EcH2WqzHGtgl.htm](pathfinder-bestiary-3-items/vOU0EcH2WqzHGtgl.htm)|Air Walk (Constant)|Marche dans les airs (constant)|officielle|
|[Vp57wsIQVmtG1jPa.htm](pathfinder-bestiary-3-items/Vp57wsIQVmtG1jPa.htm)|Suggestion (At Will)|Suggestion (À volonté)|officielle|
|[vPFPkp810JifvYk3.htm](pathfinder-bestiary-3-items/vPFPkp810JifvYk3.htm)|Pack Attack|Attaque en meute|libre|
|[vPiD83jHe3pfnCGb.htm](pathfinder-bestiary-3-items/vPiD83jHe3pfnCGb.htm)|Vine|Liane|libre|
|[vpKKf7MXtmLs5zCF.htm](pathfinder-bestiary-3-items/vpKKf7MXtmLs5zCF.htm)|Coven|Cercle|libre|
|[vpPweRPkFLYzku6Q.htm](pathfinder-bestiary-3-items/vpPweRPkFLYzku6Q.htm)|Resonance|Résonance|libre|
|[VPTJLGDVtuMcWdkQ.htm](pathfinder-bestiary-3-items/VPTJLGDVtuMcWdkQ.htm)|Constrict|Constriction|libre|
|[VPwJArBSwEFYQLmI.htm](pathfinder-bestiary-3-items/VPwJArBSwEFYQLmI.htm)|Viscous Trap|Piège visqueux|libre|
|[vQvd4d7TqB4xYYzG.htm](pathfinder-bestiary-3-items/vQvd4d7TqB4xYYzG.htm)|Silence (At Will)|Silence (À volonté)|libre|
|[vQY8xEtGMI71ZFRC.htm](pathfinder-bestiary-3-items/vQY8xEtGMI71ZFRC.htm)|Outcast's Curse (At Will)|Malédiction du paria (À volonté)|libre|
|[VR2zhinm3bRfVONP.htm](pathfinder-bestiary-3-items/VR2zhinm3bRfVONP.htm)|Breath Weapon|Arme de souffle|libre|
|[vRBgSJWn6sIDkLoJ.htm](pathfinder-bestiary-3-items/vRBgSJWn6sIDkLoJ.htm)|Boneshard Burst|Éclatement d'os|libre|
|[VRhuQIGcrHKCOyL4.htm](pathfinder-bestiary-3-items/VRhuQIGcrHKCOyL4.htm)|Flaming Weapon|Arme enflammée|libre|
|[vRX7fHLnwfVnMQMN.htm](pathfinder-bestiary-3-items/vRX7fHLnwfVnMQMN.htm)|Trackless Step|Absence de traces|officielle|
|[vrYCafAUKL8c1AqG.htm](pathfinder-bestiary-3-items/vrYCafAUKL8c1AqG.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[VS3BUS7yb5FWxaZH.htm](pathfinder-bestiary-3-items/VS3BUS7yb5FWxaZH.htm)|Appetizing Aroma|Arôme appétissant|libre|
|[VSblAk10Who8I3wv.htm](pathfinder-bestiary-3-items/VSblAk10Who8I3wv.htm)|Darkvision|Vision dans le noir|libre|
|[VSLTIQMCHCfLxq1H.htm](pathfinder-bestiary-3-items/VSLTIQMCHCfLxq1H.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[Vsv36iP3BjY1rHBr.htm](pathfinder-bestiary-3-items/Vsv36iP3BjY1rHBr.htm)|Claw|Griffe|libre|
|[VT3MCbqQoHOeEO6f.htm](pathfinder-bestiary-3-items/VT3MCbqQoHOeEO6f.htm)|Shadowed Blade|Lame d'ombre|libre|
|[VtDOTpa01IbIaeYP.htm](pathfinder-bestiary-3-items/VtDOTpa01IbIaeYP.htm)|Pack Attack|Attaque en meute|officielle|
|[VTqkprtwM4tCpoqy.htm](pathfinder-bestiary-3-items/VTqkprtwM4tCpoqy.htm)|True Seeing (Constant)|Vision lucide (constant)|officielle|
|[vTV8lwwNuVs4eL7F.htm](pathfinder-bestiary-3-items/vTV8lwwNuVs4eL7F.htm)|Focus Vines|Focaliser les vrilles|libre|
|[Vu7yHDGMbHRCK2KR.htm](pathfinder-bestiary-3-items/Vu7yHDGMbHRCK2KR.htm)|All-Around Vision|Vision panoramique|libre|
|[vumGr1e1Y2sLOMlM.htm](pathfinder-bestiary-3-items/vumGr1e1Y2sLOMlM.htm)|Negative Healing|Guérison négative|libre|
|[VvNRwoYk3JBUgyor.htm](pathfinder-bestiary-3-items/VvNRwoYk3JBUgyor.htm)|Infest Corpse|Infestation de cadavre|libre|
|[VvTUZ7JyzVpWPNRj.htm](pathfinder-bestiary-3-items/VvTUZ7JyzVpWPNRj.htm)|Sunset Ray|Rayon du crépuscule|libre|
|[Vw9udtwUZC4fiY9x.htm](pathfinder-bestiary-3-items/Vw9udtwUZC4fiY9x.htm)|Trample|Piétinement|libre|
|[VWEAgF7iG7hLA3uD.htm](pathfinder-bestiary-3-items/VWEAgF7iG7hLA3uD.htm)|Gaseous Form (At Will)|Forme gazeuse (À volonté)|libre|
|[vx5wkGlnDaE4xpPz.htm](pathfinder-bestiary-3-items/vx5wkGlnDaE4xpPz.htm)|Tail|Queue|libre|
|[Vx9iqVf4ismUN743.htm](pathfinder-bestiary-3-items/Vx9iqVf4ismUN743.htm)|Strike as One|Frapper comme un seul homme|libre|
|[vxPqbqu0444ekUR7.htm](pathfinder-bestiary-3-items/vxPqbqu0444ekUR7.htm)|Ghostly Longsword|Épée longue spectrale|libre|
|[VXQuJ8bq1mVA48Xh.htm](pathfinder-bestiary-3-items/VXQuJ8bq1mVA48Xh.htm)|Heretic's Smite|Châtiment de l'hérétique|libre|
|[vxu3Y6OKaA1HJSb6.htm](pathfinder-bestiary-3-items/vxu3Y6OKaA1HJSb6.htm)|At-Will Spells|Sorts à volonté|libre|
|[VY2kHKsJk6ZDMTkn.htm](pathfinder-bestiary-3-items/VY2kHKsJk6ZDMTkn.htm)|Demolish Veil|Démolition du voile|libre|
|[vYEI2Ae6r4vFEWMb.htm](pathfinder-bestiary-3-items/vYEI2Ae6r4vFEWMb.htm)|Negative Healing|Guérison négative|libre|
|[vyrmCD1p0HDQDVzt.htm](pathfinder-bestiary-3-items/vyrmCD1p0HDQDVzt.htm)|Wailing Dive|Piqué hurlant|libre|
|[vYyLO2LUYvXaIzFj.htm](pathfinder-bestiary-3-items/vYyLO2LUYvXaIzFj.htm)|Warding Glyph|Glyphe de protection|libre|
|[VZit0bc3TQKyAAbe.htm](pathfinder-bestiary-3-items/VZit0bc3TQKyAAbe.htm)|Shadow Plane Lore|Connaissance du plan de l'ombre|officielle|
|[VzlO3rF9ml7NQJRF.htm](pathfinder-bestiary-3-items/VzlO3rF9ml7NQJRF.htm)|Fleeting Blossoms|Floraison éphémères|libre|
|[VZlTraWqYIrTlPGW.htm](pathfinder-bestiary-3-items/VZlTraWqYIrTlPGW.htm)|Tormenting Dreams|Tourments cauchemardesques|libre|
|[VZVdci43sd9eSYaO.htm](pathfinder-bestiary-3-items/VZVdci43sd9eSYaO.htm)|Absorb Spell|Absorption de sort|libre|
|[VzzWLmieSojZVoLh.htm](pathfinder-bestiary-3-items/VzzWLmieSojZVoLh.htm)|Wrap in Coils|Enrouler les anneaux|libre|
|[W3i2bV0xohyukCqg.htm](pathfinder-bestiary-3-items/W3i2bV0xohyukCqg.htm)|Foxfire|Feu du renard|libre|
|[w43ksSNtCpLwtJqW.htm](pathfinder-bestiary-3-items/w43ksSNtCpLwtJqW.htm)|Ice Shard|Éclat de glace|libre|
|[w4lnLML460ytZr1D.htm](pathfinder-bestiary-3-items/w4lnLML460ytZr1D.htm)|Form Up|Se reformer|libre|
|[w5870YWidWVrdHst.htm](pathfinder-bestiary-3-items/w5870YWidWVrdHst.htm)|Nondetection (Constant) (Self Only)|Antidétection (constant) (soi uniquement)|libre|
|[w6hxoqW1tHgrDy7z.htm](pathfinder-bestiary-3-items/w6hxoqW1tHgrDy7z.htm)|Constant Spells|Sorts constants|libre|
|[W7hhuWr7UL3cytW8.htm](pathfinder-bestiary-3-items/W7hhuWr7UL3cytW8.htm)|Defiled Religious Symbol of Pharasma (Wooden)|Symbole religieux de Pharasma profané (bois)|libre|
|[w7zrM0Z9BtHG4iCl.htm](pathfinder-bestiary-3-items/w7zrM0Z9BtHG4iCl.htm)|Darkvision|Vision dans le noir|libre|
|[W87SHAgheuR4yFTV.htm](pathfinder-bestiary-3-items/W87SHAgheuR4yFTV.htm)|Darkvision|Vision dans le noir|libre|
|[wA8VVomLPk1zjrds.htm](pathfinder-bestiary-3-items/wA8VVomLPk1zjrds.htm)|Handwraps of Mighty Blows|+1|Bandelettes de coups puissants +1|libre|
|[WAFwt6ZXf9QztSeJ.htm](pathfinder-bestiary-3-items/WAFwt6ZXf9QztSeJ.htm)|Create Water (At Will)|Création d'eau (À volonté)|libre|
|[WavB32uYsyFzI2uA.htm](pathfinder-bestiary-3-items/WavB32uYsyFzI2uA.htm)|Performance|Représentation|libre|
|[wBBkm38TLkhTV2GZ.htm](pathfinder-bestiary-3-items/wBBkm38TLkhTV2GZ.htm)|Control Weather (No Secondary Caster Required)|Contrôle du climat (ne nécessite pas d'incantateurs secondaires)|libre|
|[WBhfbegIvXs0rmDc.htm](pathfinder-bestiary-3-items/WBhfbegIvXs0rmDc.htm)|Air Walk (Constant)|Marche dans les airs (constant)|officielle|
|[WC0xg40Qwic6NwdR.htm](pathfinder-bestiary-3-items/WC0xg40Qwic6NwdR.htm)|Radiant Ray|Rayon radiant|libre|
|[Wc4yL4hHzoo1254s.htm](pathfinder-bestiary-3-items/Wc4yL4hHzoo1254s.htm)|Pained Muttering|Marmonnements de douleur|libre|
|[wCK0LYZXOOURpX1D.htm](pathfinder-bestiary-3-items/wCK0LYZXOOURpX1D.htm)|Constant Spells|Sorts constants|libre|
|[WCzpBUZV9MrBfZQq.htm](pathfinder-bestiary-3-items/WCzpBUZV9MrBfZQq.htm)|+4 Status to All Saves vs. Mental or Divine|bonus de statut de +4 aux JdS contre les effets mentaux ou divins|libre|
|[WD4ykPiVOX7xRtgI.htm](pathfinder-bestiary-3-items/WD4ykPiVOX7xRtgI.htm)|Perfected Flight|Vol parfait|libre|
|[wDaKwffZUuh51WSU.htm](pathfinder-bestiary-3-items/wDaKwffZUuh51WSU.htm)|Emit Musk|Décharge de musc|libre|
|[WDDv1gdGqwkBHqVr.htm](pathfinder-bestiary-3-items/WDDv1gdGqwkBHqVr.htm)|Wavesense (Imprecise) 60 feet|Perception des ondes 18 m (imprécis)|officielle|
|[WDHTK76R7cwm5ZSc.htm](pathfinder-bestiary-3-items/WDHTK76R7cwm5ZSc.htm)|Intimidation|Intimidation|libre|
|[wdoiMwQp71XmTDBR.htm](pathfinder-bestiary-3-items/wdoiMwQp71XmTDBR.htm)|Spikes|Piques|libre|
|[WdP7r88rTRk6Y9Lu.htm](pathfinder-bestiary-3-items/WdP7r88rTRk6Y9Lu.htm)|Positive Energy Plane Lore|Connaissance du Plan de l'énergie positive|libre|
|[wdp8tW3xpAHzFNVP.htm](pathfinder-bestiary-3-items/wdp8tW3xpAHzFNVP.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[wDXudHrGRboTsL0B.htm](pathfinder-bestiary-3-items/wDXudHrGRboTsL0B.htm)|Grab and Go|Prendre et Filer|libre|
|[WdzKzT9cZ51TPqV6.htm](pathfinder-bestiary-3-items/WdzKzT9cZ51TPqV6.htm)|Jotun Slayer|Tueur de Jotun|libre|
|[WE0EIWm5dsgBMGU3.htm](pathfinder-bestiary-3-items/WE0EIWm5dsgBMGU3.htm)|Throw Rock|Projection de rocher|libre|
|[wEJxEzOsrNPsy1G2.htm](pathfinder-bestiary-3-items/wEJxEzOsrNPsy1G2.htm)|Absorb Memories|Absorber des souvenirs|libre|
|[wETifc6MCAQ7g53r.htm](pathfinder-bestiary-3-items/wETifc6MCAQ7g53r.htm)|Low-Light Vision|Vision nocturne|libre|
|[WEZI1PxyY7ic04Cy.htm](pathfinder-bestiary-3-items/WEZI1PxyY7ic04Cy.htm)|Grab|Empoignade/Agrippement|libre|
|[Wf76V3o8gstoUE6k.htm](pathfinder-bestiary-3-items/Wf76V3o8gstoUE6k.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[wFnxNcpjZQq97vyn.htm](pathfinder-bestiary-3-items/wFnxNcpjZQq97vyn.htm)|Mountain Stride|Déplacement facilité en montagne|libre|
|[WFRxpdnf4FFb3z7i.htm](pathfinder-bestiary-3-items/WFRxpdnf4FFb3z7i.htm)|Telepathy 30 feet|Télépathie 9 m|libre|
|[WFS8UUOKxm78jwiH.htm](pathfinder-bestiary-3-items/WFS8UUOKxm78jwiH.htm)|Grab|Empoignade/Agrippement|libre|
|[Wg3QK5gzo1szg9zD.htm](pathfinder-bestiary-3-items/Wg3QK5gzo1szg9zD.htm)|Mindlink (At Will)|Lien mental (À volonté)|officielle|
|[WgefMvFrdC4a4wjm.htm](pathfinder-bestiary-3-items/WgefMvFrdC4a4wjm.htm)|At-Will Spells|Sorts à volonté|libre|
|[WgimcieBkqdGPzKv.htm](pathfinder-bestiary-3-items/WgimcieBkqdGPzKv.htm)|Change Shape|Changement de forme|libre|
|[wGOw52Kk1d6zz1Wr.htm](pathfinder-bestiary-3-items/wGOw52Kk1d6zz1Wr.htm)|Avenging Claws|Griffes vengeresses|libre|
|[whcEPoZw0dVdy1U1.htm](pathfinder-bestiary-3-items/whcEPoZw0dVdy1U1.htm)|Telepathy 100 feet (Myceloids and Those Afflicted by Purple Pox Only)|Télépathie 30 m (Mycéloïdes et les créatures affectée par la Vérole pourpre uniquement)|libre|
|[wHFnTcSpBzMvrkSD.htm](pathfinder-bestiary-3-items/wHFnTcSpBzMvrkSD.htm)|Darkvision|Vision dans le noir|libre|
|[WIkVLqYDYSPO2pUz.htm](pathfinder-bestiary-3-items/WIkVLqYDYSPO2pUz.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[WIxfvddFY1utvHlp.htm](pathfinder-bestiary-3-items/WIxfvddFY1utvHlp.htm)|Air of Sickness|Atmosphère maladive|libre|
|[WIXzhLmn2W2ARVKv.htm](pathfinder-bestiary-3-items/WIXzhLmn2W2ARVKv.htm)|Divine Spontaneous Spells|Sorts divins spontanés|libre|
|[wJ1vHRd41BTDplWu.htm](pathfinder-bestiary-3-items/wJ1vHRd41BTDplWu.htm)|Longspear|Pique|libre|
|[wjAhC8gFPZh72a5L.htm](pathfinder-bestiary-3-items/wjAhC8gFPZh72a5L.htm)|Darkvision|Vision dans le noir|libre|
|[wJFd0snSm32sHDWx.htm](pathfinder-bestiary-3-items/wJFd0snSm32sHDWx.htm)|Punish the Naughty|Punition des vilains|libre|
|[WjhRi0h58DIX9ATj.htm](pathfinder-bestiary-3-items/WjhRi0h58DIX9ATj.htm)|Suggestion (At Will)|Suggestion (À volonté)|officielle|
|[WJSuFXX25ngL6SXe.htm](pathfinder-bestiary-3-items/WJSuFXX25ngL6SXe.htm)|Speak With Snakes|Communication avec les serpents|libre|
|[WjVRDLqgP2pwz4W9.htm](pathfinder-bestiary-3-items/WjVRDLqgP2pwz4W9.htm)|Wavesense (Imprecise) 30 feet|Perception des ondes 9 m (imprécis)|libre|
|[wjyglR2qfZUfzQhE.htm](pathfinder-bestiary-3-items/wjyglR2qfZUfzQhE.htm)|Glide|Planer|libre|
|[wK3dnqQbpz2HUZw5.htm](pathfinder-bestiary-3-items/wK3dnqQbpz2HUZw5.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[wKM9T9u0owr9Pynk.htm](pathfinder-bestiary-3-items/wKM9T9u0owr9Pynk.htm)|Divine Spontaneous Spells|Sorts divins spontanés|libre|
|[WKOA5jV7935qIaIo.htm](pathfinder-bestiary-3-items/WKOA5jV7935qIaIo.htm)|Wavesense (Imprecise) 30 feet|Perception des ondes 9 m (imprécis)|libre|
|[wkqPL5ekfW5yOlp9.htm](pathfinder-bestiary-3-items/wkqPL5ekfW5yOlp9.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[WKzCKcnmUlEawWY8.htm](pathfinder-bestiary-3-items/WKzCKcnmUlEawWY8.htm)|Fangs|Crocs|libre|
|[WLELPQnngNQdOSNv.htm](pathfinder-bestiary-3-items/WLELPQnngNQdOSNv.htm)|Contorted Clutch|Étreinte de haine|libre|
|[WLzqAFoShoUcDjON.htm](pathfinder-bestiary-3-items/WLzqAFoShoUcDjON.htm)|Death Gasp|Soupir de mort|libre|
|[wMD2GLX6aqo2KE1s.htm](pathfinder-bestiary-3-items/wMD2GLX6aqo2KE1s.htm)|+1 Circumstance to All Saves vs. Disease, Poison, and Radiation|bonus de circonstances de +1 aux JdS contre la maladie, le poison et les radiations|libre|
|[WMgMO9G3wUtuSBs8.htm](pathfinder-bestiary-3-items/WMgMO9G3wUtuSBs8.htm)|Fangs|Crocs|libre|
|[wncZzBRw5mBr1I3b.htm](pathfinder-bestiary-3-items/wncZzBRw5mBr1I3b.htm)|Claw|Griffe|libre|
|[wndNBL1gS8EX4LkQ.htm](pathfinder-bestiary-3-items/wndNBL1gS8EX4LkQ.htm)|Coiling Frenzy|Frénésie torsadée|libre|
|[WnLAb54M532yXeAK.htm](pathfinder-bestiary-3-items/WnLAb54M532yXeAK.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|libre|
|[Wo73VHAjF7M37ttK.htm](pathfinder-bestiary-3-items/Wo73VHAjF7M37ttK.htm)|Dagger|Dague|libre|
|[WOLOhrv8TOFesFur.htm](pathfinder-bestiary-3-items/WOLOhrv8TOFesFur.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[WOmi5gJbb9PPIAM8.htm](pathfinder-bestiary-3-items/WOmi5gJbb9PPIAM8.htm)|Ventriloquism (At Will)|Ventriloquie (À volonté)|officielle|
|[wP25CSpHH1XfhQZV.htm](pathfinder-bestiary-3-items/wP25CSpHH1XfhQZV.htm)|Wing|Aile|libre|
|[wPEh1COqJAlq37ce.htm](pathfinder-bestiary-3-items/wPEh1COqJAlq37ce.htm)|Shock Composite Longbow|Arc long composite de Foudre|libre|
|[WpHT325mhd62bmHx.htm](pathfinder-bestiary-3-items/WpHT325mhd62bmHx.htm)|Low-Light Vision|Vision nocturne|libre|
|[wPsE3OnHZcFptWrh.htm](pathfinder-bestiary-3-items/wPsE3OnHZcFptWrh.htm)|Claimer of the Slain|Renvendication des défunts|libre|
|[wq97ytCKPYfmWhuM.htm](pathfinder-bestiary-3-items/wq97ytCKPYfmWhuM.htm)|Hand Crossbow|Arbalète de poing|libre|
|[wqfNw7k5yrmfOl9I.htm](pathfinder-bestiary-3-items/wqfNw7k5yrmfOl9I.htm)|Cat's Curiosity|Curiosité du chat|libre|
|[wQOPxpduTlBDFOg3.htm](pathfinder-bestiary-3-items/wQOPxpduTlBDFOg3.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[WrlWLg3CTbIvRroV.htm](pathfinder-bestiary-3-items/WrlWLg3CTbIvRroV.htm)|Mace|Masse d'armes|libre|
|[wRO8DImltQqpTp5v.htm](pathfinder-bestiary-3-items/wRO8DImltQqpTp5v.htm)|Felt Shears|Ciseaux de couture|libre|
|[wRvyhBs5zmAJtpe8.htm](pathfinder-bestiary-3-items/wRvyhBs5zmAJtpe8.htm)|Temporal Sense|Perception temporelle|libre|
|[WsuMP3feuXNP1eeE.htm](pathfinder-bestiary-3-items/WsuMP3feuXNP1eeE.htm)|Sling|Fronde|libre|
|[WtYldqbo5WHxuBHU.htm](pathfinder-bestiary-3-items/WtYldqbo5WHxuBHU.htm)|Swarm Mind|Esprit de la nuée|libre|
|[wuJYkji7dBLYrLAK.htm](pathfinder-bestiary-3-items/wuJYkji7dBLYrLAK.htm)|Constant Spells|Sorts constants|libre|
|[WUkTk0F7LErjOXbO.htm](pathfinder-bestiary-3-items/WUkTk0F7LErjOXbO.htm)|Site Bound|Lié à un site|libre|
|[WUlRpT25Akwrbx9n.htm](pathfinder-bestiary-3-items/WUlRpT25Akwrbx9n.htm)|Hatchet|Hachette|libre|
|[WUrrk4eny3GFngud.htm](pathfinder-bestiary-3-items/WUrrk4eny3GFngud.htm)|Burning Cold|Froid brûlant|libre|
|[wuWal4gyvorVZotg.htm](pathfinder-bestiary-3-items/wuWal4gyvorVZotg.htm)|Deflecting Lie|Mensonge de diversion|libre|
|[Wv5iLNU2m5Tyg4mv.htm](pathfinder-bestiary-3-items/Wv5iLNU2m5Tyg4mv.htm)|Dimension Door (At Will)|Porte dimensionnelle (À volonté)|officielle|
|[WvLlxDJajjcl40yg.htm](pathfinder-bestiary-3-items/WvLlxDJajjcl40yg.htm)|Pull Arm|Étirer le bras|libre|
|[wVvVI3Jfnk8L5F3L.htm](pathfinder-bestiary-3-items/wVvVI3Jfnk8L5F3L.htm)|Pustulant Flail|Fléau d'armes purulente|libre|
|[wVY7ijG6fY0kdgBv.htm](pathfinder-bestiary-3-items/wVY7ijG6fY0kdgBv.htm)|Spear|Lance|libre|
|[wwKhJGtC2Ah8Opom.htm](pathfinder-bestiary-3-items/wwKhJGtC2Ah8Opom.htm)|Satchel for Holding Rocks|Sac pour contenir des pierres|libre|
|[wWvqRgRb0u1JwCr6.htm](pathfinder-bestiary-3-items/wWvqRgRb0u1JwCr6.htm)|Troop Movement|Mouvement de troupe|libre|
|[wX6Fc9wZK3CK5WNe.htm](pathfinder-bestiary-3-items/wX6Fc9wZK3CK5WNe.htm)|Shadow Walk (See Shadow's Swiftness)|Traversée des ombres (voir Célérité des ombres)|libre|
|[wY1dx0xYE5qgM9aA.htm](pathfinder-bestiary-3-items/wY1dx0xYE5qgM9aA.htm)|Sonic Pulse|Pulsion sonique|libre|
|[WY6HZHUNFdCpdD8m.htm](pathfinder-bestiary-3-items/WY6HZHUNFdCpdD8m.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[wy6isKCHF1syJmZf.htm](pathfinder-bestiary-3-items/wy6isKCHF1syJmZf.htm)|Fist|Poing|libre|
|[wycfQOBOlmzkFJ7X.htm](pathfinder-bestiary-3-items/wycfQOBOlmzkFJ7X.htm)|Slough Toxins|Rejet des toxines|libre|
|[WypFVNf6qa1B0aVW.htm](pathfinder-bestiary-3-items/WypFVNf6qa1B0aVW.htm)|Voice of the Storm|Voix dans la tempête|libre|
|[WYWDDEYazjVp60qA.htm](pathfinder-bestiary-3-items/WYWDDEYazjVp60qA.htm)|Rain of Debris|Déluge de débris|libre|
|[WZ8Y5R6xt0YzzTUf.htm](pathfinder-bestiary-3-items/WZ8Y5R6xt0YzzTUf.htm)|Darkvision|Vision dans le noir|libre|
|[wzhDzF6I1V67YTEq.htm](pathfinder-bestiary-3-items/wzhDzF6I1V67YTEq.htm)|Low-Light Vision|Vision nocturne|libre|
|[Wzi3fuQeULw68X2T.htm](pathfinder-bestiary-3-items/Wzi3fuQeULw68X2T.htm)|Purify Food and Drink (At Will)|Purifier la nourriture et la boisson (À volonté)|libre|
|[X0s0MfJlPDdh23yP.htm](pathfinder-bestiary-3-items/X0s0MfJlPDdh23yP.htm)|Peaceful Aura|Aura de paix|libre|
|[X0u8yRaoumH1Qm27.htm](pathfinder-bestiary-3-items/X0u8yRaoumH1Qm27.htm)|Lithe|Souplesse|libre|
|[x29TRtgx6HdtboNu.htm](pathfinder-bestiary-3-items/x29TRtgx6HdtboNu.htm)|Dimension Door (At Will)|Porte dimensionnelle (À volonté)|officielle|
|[X2Wh8jC2XB0vLQWK.htm](pathfinder-bestiary-3-items/X2Wh8jC2XB0vLQWK.htm)|At-Will Spells|Sorts à volonté|libre|
|[x3F1PlXQKZuCBCGg.htm](pathfinder-bestiary-3-items/x3F1PlXQKZuCBCGg.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[x40IMqy9XVt1sVgV.htm](pathfinder-bestiary-3-items/x40IMqy9XVt1sVgV.htm)|Darkvision|Vision dans le noir|libre|
|[X47wX3wcEYVktTy9.htm](pathfinder-bestiary-3-items/X47wX3wcEYVktTy9.htm)|Bound|Lié|libre|
|[X4dbGwvffyOz8AAl.htm](pathfinder-bestiary-3-items/X4dbGwvffyOz8AAl.htm)|Fist|Poing|libre|
|[X4oCGHtZLhDPu84J.htm](pathfinder-bestiary-3-items/X4oCGHtZLhDPu84J.htm)|Shortbow|Arc court|libre|
|[X4QNdoSZgj2ri5we.htm](pathfinder-bestiary-3-items/X4QNdoSZgj2ri5we.htm)|Wavesense (Imprecise) 30 feet|Perception des ondes 9 m (imprécis)|libre|
|[X5bjxwBhWpEDkFUA.htm](pathfinder-bestiary-3-items/X5bjxwBhWpEDkFUA.htm)|Forge Weapon|Arme forgée|libre|
|[x5dNPm9EV0pS1WDJ.htm](pathfinder-bestiary-3-items/x5dNPm9EV0pS1WDJ.htm)|Spiked Chain|Chaîne cloutée|libre|
|[X5iRzpgvcVsGqbsr.htm](pathfinder-bestiary-3-items/X5iRzpgvcVsGqbsr.htm)|Thorn|Épines|libre|
|[x5ke7WXv7p23V2Mc.htm](pathfinder-bestiary-3-items/x5ke7WXv7p23V2Mc.htm)|Low-Light Vision|Vision nocturne|libre|
|[x68dPIsgXM3s4jCZ.htm](pathfinder-bestiary-3-items/x68dPIsgXM3s4jCZ.htm)|Claw|Griffe|libre|
|[X6SY2vedf1w4NFyM.htm](pathfinder-bestiary-3-items/X6SY2vedf1w4NFyM.htm)|Fangs|Crocs|libre|
|[x7CJTFxW4u5WZvs5.htm](pathfinder-bestiary-3-items/x7CJTFxW4u5WZvs5.htm)|Fade Away|S'estomper|libre|
|[X7keRhNv8rJSPJUM.htm](pathfinder-bestiary-3-items/X7keRhNv8rJSPJUM.htm)|Composite Longbow|Arc long composite|libre|
|[X7My4JpekzJFoAlA.htm](pathfinder-bestiary-3-items/X7My4JpekzJFoAlA.htm)|Jaws|Mâchoires|libre|
|[X7tUpkvJ2IFENOEb.htm](pathfinder-bestiary-3-items/X7tUpkvJ2IFENOEb.htm)|Low-Light Vision|Vision nocturne|libre|
|[X8q0VOcCAdQbazmw.htm](pathfinder-bestiary-3-items/X8q0VOcCAdQbazmw.htm)|Urban Legend|Légende urbaine|libre|
|[x8x4bMdmpqcgY6vi.htm](pathfinder-bestiary-3-items/x8x4bMdmpqcgY6vi.htm)|Darkvision|Vision dans le noir|libre|
|[xA0xMv9xbqMW0QIQ.htm](pathfinder-bestiary-3-items/xA0xMv9xbqMW0QIQ.htm)|Tearing Clutch|Attaque déchirante|libre|
|[XAD9XYroOQ2xoyQF.htm](pathfinder-bestiary-3-items/XAD9XYroOQ2xoyQF.htm)|+4 Status to All Saves vs. Mental|bonus de statut de +4 aux JdS contre les effets mentaux|libre|
|[XAg8ikRjbnklrzff.htm](pathfinder-bestiary-3-items/XAg8ikRjbnklrzff.htm)|True Seeing (Constant)|Vision lucide (constant)|officielle|
|[XAyHusrI0x96dmIN.htm](pathfinder-bestiary-3-items/XAyHusrI0x96dmIN.htm)|True Seeing (Constant)|Vision lucide (constant)|officielle|
|[xb36ZcB5nu2McDrU.htm](pathfinder-bestiary-3-items/xb36ZcB5nu2McDrU.htm)|Grave Tide|Vague sépulcrale|libre|
|[XbI8HI1jqqylY3ZB.htm](pathfinder-bestiary-3-items/XbI8HI1jqqylY3ZB.htm)|Fly Free|Se détacher|libre|
|[xBKXa275uqX5FmD9.htm](pathfinder-bestiary-3-items/xBKXa275uqX5FmD9.htm)|Beak|Bec|libre|
|[XBz2toYQ5LlpBAkp.htm](pathfinder-bestiary-3-items/XBz2toYQ5LlpBAkp.htm)|Transpose|Transposition|libre|
|[xc23c2YKrqqX2Dqy.htm](pathfinder-bestiary-3-items/xc23c2YKrqqX2Dqy.htm)|At-Will Spells|Sorts à volonté|libre|
|[Xco2zlLptJvbWtBI.htm](pathfinder-bestiary-3-items/Xco2zlLptJvbWtBI.htm)|Dimension Door (At Will)|Porte dimensionnelle (À volonté)|officielle|
|[XCtNEYFEjjOTsfCx.htm](pathfinder-bestiary-3-items/XCtNEYFEjjOTsfCx.htm)|Divine Prepared Spells|Sorts divins préparés|libre|
|[xcujrw8UlSaahhu6.htm](pathfinder-bestiary-3-items/xcujrw8UlSaahhu6.htm)|Earthbind (At Will)|Cloué à terre (À volonté)|libre|
|[xdDVdaosFR2v4kpi.htm](pathfinder-bestiary-3-items/xdDVdaosFR2v4kpi.htm)|Black Onyx Gems|Gemmes d'onyx noires|libre|
|[XdiuvRsoT9Lh40Vm.htm](pathfinder-bestiary-3-items/XdiuvRsoT9Lh40Vm.htm)|Spike|Pointe|libre|
|[xDnUeGZfUafSNkYD.htm](pathfinder-bestiary-3-items/xDnUeGZfUafSNkYD.htm)|Darkvision|Vision dans le noir|libre|
|[XdPq6vZ9PifML9Fy.htm](pathfinder-bestiary-3-items/XdPq6vZ9PifML9Fy.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[xdQJFj57QglkQXeG.htm](pathfinder-bestiary-3-items/xdQJFj57QglkQXeG.htm)|Vicious Criticals|Critique vicieux|libre|
|[xDxTLZhpgmfspHHB.htm](pathfinder-bestiary-3-items/xDxTLZhpgmfspHHB.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[xEaJ3PQ5662GbdCV.htm](pathfinder-bestiary-3-items/xEaJ3PQ5662GbdCV.htm)|Jaws|Mâchoires|libre|
|[xeAn1C6HdB8ksSEc.htm](pathfinder-bestiary-3-items/xeAn1C6HdB8ksSEc.htm)|Instant Repair|Réparation instantanée|libre|
|[xEEbQADYYULp07pu.htm](pathfinder-bestiary-3-items/xEEbQADYYULp07pu.htm)|Stinger|Dard|libre|
|[XFaAhmVWAH18gZJm.htm](pathfinder-bestiary-3-items/XFaAhmVWAH18gZJm.htm)|Low-Light Vision|Vision nocturne|libre|
|[XFheFXfH7AWzxt0S.htm](pathfinder-bestiary-3-items/XFheFXfH7AWzxt0S.htm)|Bone Cannon|Canon d'Os|libre|
|[xFhOtowzFmFYfYz2.htm](pathfinder-bestiary-3-items/xFhOtowzFmFYfYz2.htm)|Constant Spells|Sorts constants|libre|
|[xFikHberuOZ3zfc4.htm](pathfinder-bestiary-3-items/xFikHberuOZ3zfc4.htm)|Speak with Plants (Constant)|Communication avec les plantes (constant)|libre|
|[xfMHekuTgDpCE7UY.htm](pathfinder-bestiary-3-items/xfMHekuTgDpCE7UY.htm)|See Invisibility (Constant)|Détection de l'invisibilité (constant)|officielle|
|[XfqrwEKpnBBJXrSD.htm](pathfinder-bestiary-3-items/XfqrwEKpnBBJXrSD.htm)|Attack of Opportunity (Special)|Attaque d'opportunité (Spéciale)|libre|
|[xFzHJGL72aPpRLgB.htm](pathfinder-bestiary-3-items/xFzHJGL72aPpRLgB.htm)|Lore (Any One Subcategory)|Connaissance (n'importe quelle sous-catégorie)|libre|
|[xGCY0XLJysaFyIyL.htm](pathfinder-bestiary-3-items/xGCY0XLJysaFyIyL.htm)|Labor Lore|Connaissance du travail|officielle|
|[XGigEFgiBGNmtOkF.htm](pathfinder-bestiary-3-items/XGigEFgiBGNmtOkF.htm)|Change Shape|Changement de forme|libre|
|[xglgBFL5H1uvbArD.htm](pathfinder-bestiary-3-items/xglgBFL5H1uvbArD.htm)|Adamantine Claws|Griffes en adamantium|libre|
|[xglqaHZLA1NfPGQN.htm](pathfinder-bestiary-3-items/xglqaHZLA1NfPGQN.htm)|Divine Prepared Spells|Sorts divins préparés|libre|
|[XGpAwWvwSpDI36HG.htm](pathfinder-bestiary-3-items/XGpAwWvwSpDI36HG.htm)|Skeleton Crew|Équipage de squelettes|libre|
|[xGpHtG4FKoXUAvFz.htm](pathfinder-bestiary-3-items/xGpHtG4FKoXUAvFz.htm)|Trident|Trident|libre|
|[XH6YaBHVaUMGRWkN.htm](pathfinder-bestiary-3-items/XH6YaBHVaUMGRWkN.htm)|Darkvision|Vision dans le noir|libre|
|[xhfzAaoPZ4coGOku.htm](pathfinder-bestiary-3-items/xhfzAaoPZ4coGOku.htm)|Detect Alignment (At Will) (Good or Evil Only)|Détection de l'alignement (à volonté, bon ou mauvais uniquement)|libre|
|[XhqGsa6pKMextMJ1.htm](pathfinder-bestiary-3-items/XhqGsa6pKMextMJ1.htm)|Scent (Imprecise) 60 feet|Odorat 18 m (imprécis)|libre|
|[xIt4uW4tEMRY5Y7p.htm](pathfinder-bestiary-3-items/xIt4uW4tEMRY5Y7p.htm)|Fist|Poing|libre|
|[Xiw2DZbMz5qQ6sUm.htm](pathfinder-bestiary-3-items/Xiw2DZbMz5qQ6sUm.htm)|Tongues (Constant)|Don des langues (constant)|officielle|
|[XjarabsV3GAFA01K.htm](pathfinder-bestiary-3-items/XjarabsV3GAFA01K.htm)|Claw|Griffe|libre|
|[xJGiCaAnnERzWXes.htm](pathfinder-bestiary-3-items/xJGiCaAnnERzWXes.htm)|Change Shape|Changement de forme|libre|
|[xji68Q4mnqfdWVnq.htm](pathfinder-bestiary-3-items/xji68Q4mnqfdWVnq.htm)|Shield Block|Blocage au bouclier|libre|
|[xKAa1bclabyOkPCJ.htm](pathfinder-bestiary-3-items/xKAa1bclabyOkPCJ.htm)|Pliers|Pinces|libre|
|[XkAvPPqqBigAbyb2.htm](pathfinder-bestiary-3-items/XkAvPPqqBigAbyb2.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[xkZQodLXco1BCblV.htm](pathfinder-bestiary-3-items/xkZQodLXco1BCblV.htm)|Suggestion (At Will)|Suggestion (À volonté)|officielle|
|[XlCJ1ZHQ8EGtKrFn.htm](pathfinder-bestiary-3-items/XlCJ1ZHQ8EGtKrFn.htm)|Bite|Morsure|officielle|
|[Xlnd5wqyTUYU18hD.htm](pathfinder-bestiary-3-items/Xlnd5wqyTUYU18hD.htm)|Fangs|Crocs|libre|
|[xLpHFUCeWtRMaSOY.htm](pathfinder-bestiary-3-items/xLpHFUCeWtRMaSOY.htm)|Crafting|Artisanat|libre|
|[xly8aKaWKIrEtqCD.htm](pathfinder-bestiary-3-items/xly8aKaWKIrEtqCD.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[xMaFAzpC6rkfoyYs.htm](pathfinder-bestiary-3-items/xMaFAzpC6rkfoyYs.htm)|Draconic Momentum|Impulsion draconique|libre|
|[xMNtSzQGqtHwsi9I.htm](pathfinder-bestiary-3-items/xMNtSzQGqtHwsi9I.htm)|Rock|Rocher|libre|
|[xmvNKENAwK9gdUuB.htm](pathfinder-bestiary-3-items/xmvNKENAwK9gdUuB.htm)|Grab|Empoignade/Agrippement|libre|
|[Xn5fxlgPbRTrvMi3.htm](pathfinder-bestiary-3-items/Xn5fxlgPbRTrvMi3.htm)|Crew's Call|Appel de l'équipage|libre|
|[Xn6fpT76Eq3gNDgg.htm](pathfinder-bestiary-3-items/Xn6fpT76Eq3gNDgg.htm)|+4 to Will Saves vs. Fear|bonus de +4 aux jets de sauvegarde contre la peur|libre|
|[XN9PqGr1Gft89U0v.htm](pathfinder-bestiary-3-items/XN9PqGr1Gft89U0v.htm)|Pervert Miracle|Miracle perverti|libre|
|[xNumFo6zFMddKGAf.htm](pathfinder-bestiary-3-items/xNumFo6zFMddKGAf.htm)|Master of the Yard|Maître de la cour|libre|
|[xOPKChTgrsXHKQAv.htm](pathfinder-bestiary-3-items/xOPKChTgrsXHKQAv.htm)|Light Blindness|Aveuglé par la lumière|officielle|
|[XPaMcuntcVkLXwLT.htm](pathfinder-bestiary-3-items/XPaMcuntcVkLXwLT.htm)|Jaws|Mâchoires|libre|
|[xPc5xxHX62bKWgkk.htm](pathfinder-bestiary-3-items/xPc5xxHX62bKWgkk.htm)|Tongue Grab|Agrippement avec la langue|libre|
|[xpiNy47A0TciwBNQ.htm](pathfinder-bestiary-3-items/xpiNy47A0TciwBNQ.htm)|Jaws|Mâchoires|libre|
|[XpkRG2gKRWLJkJEI.htm](pathfinder-bestiary-3-items/XpkRG2gKRWLJkJEI.htm)|Constant Spells|Sorts constants|libre|
|[XPllxNneASW7mX3o.htm](pathfinder-bestiary-3-items/XPllxNneASW7mX3o.htm)|Blizzard Sight|Vision malgré le blizzard|libre|
|[XpzY4UQrwOlgO4km.htm](pathfinder-bestiary-3-items/XpzY4UQrwOlgO4km.htm)|Jaws|Mâchoires|libre|
|[Xq6IkurlmqAmuchc.htm](pathfinder-bestiary-3-items/Xq6IkurlmqAmuchc.htm)|Draconic Momentum|Impulsion draconique|libre|
|[XqjbxnxA89sOEKAK.htm](pathfinder-bestiary-3-items/XqjbxnxA89sOEKAK.htm)|Claw|Griffe|libre|
|[xQqEt4PLNOCTutWG.htm](pathfinder-bestiary-3-items/xQqEt4PLNOCTutWG.htm)|Draconic Momentum|Impulsion draconique|libre|
|[XRASk2fmJyT1LAqe.htm](pathfinder-bestiary-3-items/XRASk2fmJyT1LAqe.htm)|Mind Reading (At Will)|Lecture des pensées (À volonté)|officielle|
|[XrFiVMNybl9KRDD9.htm](pathfinder-bestiary-3-items/XrFiVMNybl9KRDD9.htm)|Speak with Animals (Constant)|Communication avec les animaux (constant)|officielle|
|[xRGfPORY2OUX19Yt.htm](pathfinder-bestiary-3-items/xRGfPORY2OUX19Yt.htm)|Spirit Dart|Fléchette d'esprits liés|libre|
|[XrGSSYX0IvMaaBC0.htm](pathfinder-bestiary-3-items/XrGSSYX0IvMaaBC0.htm)|Stone Tell (Constant)|Pierre commère (constant)|libre|
|[xsnc3cQ3J9m1QSHk.htm](pathfinder-bestiary-3-items/xsnc3cQ3J9m1QSHk.htm)|Vie for Victory|V pour victoire|libre|
|[xSSsTbYujfY4mN5H.htm](pathfinder-bestiary-3-items/xSSsTbYujfY4mN5H.htm)|Spade|Pelle|libre|
|[xt4bQy0cnsmUHdm8.htm](pathfinder-bestiary-3-items/xt4bQy0cnsmUHdm8.htm)|Spherical Body|Corps sphérique|libre|
|[XTe4xCgJVw7ZYYiN.htm](pathfinder-bestiary-3-items/XTe4xCgJVw7ZYYiN.htm)|Upside Down|Sens dessus dessous|libre|
|[XTgwZSQK0BbVTSBC.htm](pathfinder-bestiary-3-items/XTgwZSQK0BbVTSBC.htm)|+1 Status to All Saves vs. Evil|Bonus de statut de +1 aux JdS contre le mal|libre|
|[XTh9EmC4LPMteKEg.htm](pathfinder-bestiary-3-items/XTh9EmC4LPMteKEg.htm)|Darkvision|Vision dans le noir|libre|
|[xTlKFL3sVgzIZhmL.htm](pathfinder-bestiary-3-items/xTlKFL3sVgzIZhmL.htm)|Pass Without Trace (Constant)|Passage sans trace (constant)|officielle|
|[XuhLACe64duPSQuG.htm](pathfinder-bestiary-3-items/XuhLACe64duPSQuG.htm)|Phantom Bow|Arc fantôme|libre|
|[xuNy9YUXbjhDbBPJ.htm](pathfinder-bestiary-3-items/xuNy9YUXbjhDbBPJ.htm)|Leaping Pounce|Bond agressif|libre|
|[xUovRSHL0ImgkXwW.htm](pathfinder-bestiary-3-items/xUovRSHL0ImgkXwW.htm)|Claw|Griffe|libre|
|[xvKXt3hrhkReIqhL.htm](pathfinder-bestiary-3-items/xvKXt3hrhkReIqhL.htm)|Corrupt Speech|Discours corrompu|libre|
|[xVneolKzqFvV56qV.htm](pathfinder-bestiary-3-items/xVneolKzqFvV56qV.htm)|Jaws|Mâchoires|libre|
|[xWKUQcMCVRZCnWMH.htm](pathfinder-bestiary-3-items/xWKUQcMCVRZCnWMH.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[xWTrZfFzg3jvqkuu.htm](pathfinder-bestiary-3-items/xWTrZfFzg3jvqkuu.htm)|Greater Constrict|Constriction supérieure|libre|
|[xWxtACkZ1XENgLax.htm](pathfinder-bestiary-3-items/xWxtACkZ1XENgLax.htm)|Greatclub|+1,striking|Massue de frappe +1|libre|
|[XX6N64aoSGL9jWS6.htm](pathfinder-bestiary-3-items/XX6N64aoSGL9jWS6.htm)|Magic Weapon (At Will)|Arme magique (À volonté)|libre|
|[xXgKRQDkgjLOs0Pw.htm](pathfinder-bestiary-3-items/xXgKRQDkgjLOs0Pw.htm)|At-Will Spells|Sorts à volonté|libre|
|[xxSdDW74Jjn0m4TC.htm](pathfinder-bestiary-3-items/xxSdDW74Jjn0m4TC.htm)|At-Will Spells|Sorts à volonté|libre|
|[xyVLktOlqlJ6Ce4h.htm](pathfinder-bestiary-3-items/xyVLktOlqlJ6Ce4h.htm)|Resonance|Résonance|libre|
|[Xyy6EkOtHfPObm42.htm](pathfinder-bestiary-3-items/Xyy6EkOtHfPObm42.htm)|Constant Spells|Sorts constants|libre|
|[xz2zppQgQtgwnNtp.htm](pathfinder-bestiary-3-items/xz2zppQgQtgwnNtp.htm)|Shriek|Cri strident|libre|
|[XzaWmxiliLGXDQy7.htm](pathfinder-bestiary-3-items/XzaWmxiliLGXDQy7.htm)|Lore (Associated with the Guardian's Need)|Connaissance (associée avec les besoins du gardien)|libre|
|[xzjn66Avz2JyCDZb.htm](pathfinder-bestiary-3-items/xzjn66Avz2JyCDZb.htm)|Chitinous Spines|Épines en chitine|libre|
|[Xzku75HXAlTNH0q3.htm](pathfinder-bestiary-3-items/Xzku75HXAlTNH0q3.htm)|Constrict|Constriction|libre|
|[xZlNbzJzZCNKvumU.htm](pathfinder-bestiary-3-items/xZlNbzJzZCNKvumU.htm)|Plane Shift (Self Only) (to the Material or Shadow Plane only)|Changement de plan (soi uniquement, vers le plan Matériel ou de l'Ombre uniquement)|libre|
|[y0GOJlbF7uqf9iXm.htm](pathfinder-bestiary-3-items/y0GOJlbF7uqf9iXm.htm)|Jaws|Mâchoires|libre|
|[Y1qTkVB6Ss3pauQB.htm](pathfinder-bestiary-3-items/Y1qTkVB6Ss3pauQB.htm)|Disorienting Faces|Visages déroutants|libre|
|[Y2DLiZGrL1Rc9uJZ.htm](pathfinder-bestiary-3-items/Y2DLiZGrL1Rc9uJZ.htm)|Vulnerable to Slow|Vulnérabilité à Lenteur|libre|
|[Y2rKb2rKiZlOJBow.htm](pathfinder-bestiary-3-items/Y2rKb2rKiZlOJBow.htm)|Darkvision|Vision dans le noir|libre|
|[Y2Zs23iG5XQJYKVX.htm](pathfinder-bestiary-3-items/Y2Zs23iG5XQJYKVX.htm)|Darkvision|Vision dans le noir|libre|
|[Y4jOGsg9U0NdL52x.htm](pathfinder-bestiary-3-items/Y4jOGsg9U0NdL52x.htm)|Darkvision|Vision dans le noir|libre|
|[y4KzwwQKc4qz6rj7.htm](pathfinder-bestiary-3-items/y4KzwwQKc4qz6rj7.htm)|Colossal Echo|Écho colossal|libre|
|[Y4pImWAwa2NK5CDI.htm](pathfinder-bestiary-3-items/Y4pImWAwa2NK5CDI.htm)|Darkvision|Vision dans le noir|libre|
|[y5xPl9xFyELDGekP.htm](pathfinder-bestiary-3-items/y5xPl9xFyELDGekP.htm)|Desert Lore|Connaissance du désert|officielle|
|[Y7BT5iq85aFgz9ND.htm](pathfinder-bestiary-3-items/Y7BT5iq85aFgz9ND.htm)|Rearing Thrust|Cabrer et frapper|libre|
|[y7oztbBrqRdXk2kF.htm](pathfinder-bestiary-3-items/y7oztbBrqRdXk2kF.htm)|Knockdown|Renversement|libre|
|[y7quO1sHtNujvw9P.htm](pathfinder-bestiary-3-items/y7quO1sHtNujvw9P.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[y8arNw5bZrQloRjm.htm](pathfinder-bestiary-3-items/y8arNw5bZrQloRjm.htm)|Wavesense (Imprecise) 60 feet|Perception des ondes 18 m (imprécis)|officielle|
|[Y8b1W9ZdIMaUhI2I.htm](pathfinder-bestiary-3-items/Y8b1W9ZdIMaUhI2I.htm)|Magic Aura (Constant) (Self Only)|Aura magique (constant) (soi uniquement)|libre|
|[Y9n8hgpI4feL9erM.htm](pathfinder-bestiary-3-items/Y9n8hgpI4feL9erM.htm)|Draconic Momentum|Impulsion draconique|libre|
|[y9v6b1HnMTeyR6Aa.htm](pathfinder-bestiary-3-items/y9v6b1HnMTeyR6Aa.htm)|Jaws|Mâchoires|libre|
|[YAt3ohXujxgfi1dY.htm](pathfinder-bestiary-3-items/YAt3ohXujxgfi1dY.htm)|Darkvision|Vision dans le noir|libre|
|[YAtOaRD1Fb2RuzBs.htm](pathfinder-bestiary-3-items/YAtOaRD1Fb2RuzBs.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[YbkGuRsw8NVVsIfB.htm](pathfinder-bestiary-3-items/YbkGuRsw8NVVsIfB.htm)|Spectral Jaws|Mâchoires spectrale|libre|
|[yBpXyahw41IRnTwr.htm](pathfinder-bestiary-3-items/yBpXyahw41IRnTwr.htm)|Dreadful Prediction|Effroyable prédiction|libre|
|[ybXtrOFeg06hi75j.htm](pathfinder-bestiary-3-items/ybXtrOFeg06hi75j.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[yC55qBRcZsAS9EaR.htm](pathfinder-bestiary-3-items/yC55qBRcZsAS9EaR.htm)|Low-Light Vision|Vision nocturne|libre|
|[YC9q2aIlbYj8sEUp.htm](pathfinder-bestiary-3-items/YC9q2aIlbYj8sEUp.htm)|Greatclub|+1,striking|Massue de frappe +1|libre|
|[ycIkiccSXhjaJ6am.htm](pathfinder-bestiary-3-items/ycIkiccSXhjaJ6am.htm)|Deflecting Gale|Bourrasque déviatrice|libre|
|[yCLa7vNTrFdsMYUz.htm](pathfinder-bestiary-3-items/yCLa7vNTrFdsMYUz.htm)|Low-Light Vision|Vision nocturne|libre|
|[Ycopi6QWkxh3DeYu.htm](pathfinder-bestiary-3-items/Ycopi6QWkxh3DeYu.htm)|Easy to Call|Facile à invoquer|libre|
|[ycvxnNDxb7l6WNHB.htm](pathfinder-bestiary-3-items/ycvxnNDxb7l6WNHB.htm)|Athletics|Athlétisme|libre|
|[yd3ZruXaEyefPpGs.htm](pathfinder-bestiary-3-items/yd3ZruXaEyefPpGs.htm)|Talon|Serre|libre|
|[yDBgrswetRTvDh9j.htm](pathfinder-bestiary-3-items/yDBgrswetRTvDh9j.htm)|Revived Retaliation|Riposte ravivée|libre|
|[YDmIyZCs3c67oBWR.htm](pathfinder-bestiary-3-items/YDmIyZCs3c67oBWR.htm)|Jaws|Mâchoires|libre|
|[yds3BWASeIlBPuQ3.htm](pathfinder-bestiary-3-items/yds3BWASeIlBPuQ3.htm)|Telepathy 500 feet|Télépathie 150 m|libre|
|[YE7BBnb0szfbgyZp.htm](pathfinder-bestiary-3-items/YE7BBnb0szfbgyZp.htm)|Change Shape|Changement de forme|libre|
|[YEEOqFUadOvlmJnt.htm](pathfinder-bestiary-3-items/YEEOqFUadOvlmJnt.htm)|Bounding Sprint|Bond avec élan|libre|
|[YF0cM4fWT0fvGHqC.htm](pathfinder-bestiary-3-items/YF0cM4fWT0fvGHqC.htm)|Revived Retaliation|Riposte ravivée|libre|
|[yF5x6s4ZYEZtto6J.htm](pathfinder-bestiary-3-items/yF5x6s4ZYEZtto6J.htm)|Claw|Griffe|libre|
|[YFR9EAebk1pSnIl3.htm](pathfinder-bestiary-3-items/YFR9EAebk1pSnIl3.htm)|Tremorsense (Precise) 30 feet|Perception des vibrations 9 m (précis)|libre|
|[Yft4ncVPX04wtjTC.htm](pathfinder-bestiary-3-items/Yft4ncVPX04wtjTC.htm)|Echolocation (Precise) 20 feet|Écholocalisation 6 m (précis)|libre|
|[YfW3hmqsouC0M5an.htm](pathfinder-bestiary-3-items/YfW3hmqsouC0M5an.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[yfzdBwOPqpyuRl0x.htm](pathfinder-bestiary-3-items/yfzdBwOPqpyuRl0x.htm)|Flute|Flûte|libre|
|[YGMkhOIOrpQ8AZbt.htm](pathfinder-bestiary-3-items/YGMkhOIOrpQ8AZbt.htm)|Erudite|Érudition|libre|
|[YHH8WnAJViHJAQnW.htm](pathfinder-bestiary-3-items/YHH8WnAJViHJAQnW.htm)|Household Lore|Connaissance domestique|officielle|
|[yHPiBI51EEqH1jct.htm](pathfinder-bestiary-3-items/yHPiBI51EEqH1jct.htm)|Rituals|Rituel|libre|
|[yipkwbXsFDd1t15w.htm](pathfinder-bestiary-3-items/yipkwbXsFDd1t15w.htm)|Consume Thoughts|Absorption des pensées|libre|
|[YJ4VNOPXDLADhc0v.htm](pathfinder-bestiary-3-items/YJ4VNOPXDLADhc0v.htm)|Jaws|Mâchoires|libre|
|[yJ8dDwM4sX2nS4UR.htm](pathfinder-bestiary-3-items/yJ8dDwM4sX2nS4UR.htm)|Aquatic Drag|Emporter sous l'eau|libre|
|[yJoFSnPHI2YuGrUT.htm](pathfinder-bestiary-3-items/yJoFSnPHI2YuGrUT.htm)|Darkvision|Vision dans le noir|libre|
|[ykB9VqfwXdIegLS2.htm](pathfinder-bestiary-3-items/ykB9VqfwXdIegLS2.htm)|Sneak Attack|Attaque sournoise|officielle|
|[YKcp0LZjzLqu4KfR.htm](pathfinder-bestiary-3-items/YKcp0LZjzLqu4KfR.htm)|Attack of Opportunity (Special)|Attaque d'opportunité (Spéciale)|libre|
|[YKo3Am15Wz2kJyKI.htm](pathfinder-bestiary-3-items/YKo3Am15Wz2kJyKI.htm)|Illusory Weapon|Arme illusoire|libre|
|[Ykw6ewrTUkGjjiKl.htm](pathfinder-bestiary-3-items/Ykw6ewrTUkGjjiKl.htm)|Defensive Shove|Bousculade défensive|libre|
|[ylEVi6mr1P1f1Nwz.htm](pathfinder-bestiary-3-items/ylEVi6mr1P1f1Nwz.htm)|Darkvision|Vision dans le noir|libre|
|[YLG2dq3uetOa36Vb.htm](pathfinder-bestiary-3-items/YLG2dq3uetOa36Vb.htm)|Bind Undead (At Will)|Lier un mort-vivant (À volonté)|libre|
|[YLigc5sHGyr9ZE6A.htm](pathfinder-bestiary-3-items/YLigc5sHGyr9ZE6A.htm)|Detect Alignment (At Will) (Evil Only)|Détection de l'alignement (à volonté, mauvais uniquement)|officielle|
|[YlQhHUez8TDobfxk.htm](pathfinder-bestiary-3-items/YlQhHUez8TDobfxk.htm)|Illusory Object (At Will)|Objet illusoire (À volonté)|officielle|
|[YLX02foABJxLh21p.htm](pathfinder-bestiary-3-items/YLX02foABJxLh21p.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[Ym03EsvdVPymdBSB.htm](pathfinder-bestiary-3-items/Ym03EsvdVPymdBSB.htm)|Deep Breath|Inspiration profonde|libre|
|[ymApeJr8ZiUS6OhX.htm](pathfinder-bestiary-3-items/ymApeJr8ZiUS6OhX.htm)|Spewing Bile|Projection de bile|libre|
|[YmLWpVwgT7G41DLf.htm](pathfinder-bestiary-3-items/YmLWpVwgT7G41DLf.htm)|Tattered Soul|Âme en lambeaux|libre|
|[yMNZmEZoX7NmpxxP.htm](pathfinder-bestiary-3-items/yMNZmEZoX7NmpxxP.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[yMZjDluDwdyDmxeR.htm](pathfinder-bestiary-3-items/yMZjDluDwdyDmxeR.htm)|Dominate (At Will) (See Dominate)|Domination (À volonté) (Voir Domination)|libre|
|[yn9p5OZFEVAJgqpJ.htm](pathfinder-bestiary-3-items/yn9p5OZFEVAJgqpJ.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[YNbqq2itxEP5DMSE.htm](pathfinder-bestiary-3-items/YNbqq2itxEP5DMSE.htm)|Wall of Fire (At Will) (See Unstable Magic)|Mur de feu (À volonté) (voir Magie instable)|libre|
|[yNJghW8czppyVYi1.htm](pathfinder-bestiary-3-items/yNJghW8czppyVYi1.htm)|Draconic Momentum|Impulsion draconique|libre|
|[YNpN5FwvZ27Zfviz.htm](pathfinder-bestiary-3-items/YNpN5FwvZ27Zfviz.htm)|Shambling Onslaught|Tuerie titubante|libre|
|[YnX58rhrT4hsl8zl.htm](pathfinder-bestiary-3-items/YnX58rhrT4hsl8zl.htm)|Arcane Bolt|Éclair arcanique|libre|
|[YnXG494wllYVpiYZ.htm](pathfinder-bestiary-3-items/YnXG494wllYVpiYZ.htm)|Negative Healing|Guérison négative|libre|
|[yo565WZx3G5mcub3.htm](pathfinder-bestiary-3-items/yo565WZx3G5mcub3.htm)|Coiling Frenzy|Frénésie torsadée|libre|
|[yO6YNvoUDLZPlNwE.htm](pathfinder-bestiary-3-items/yO6YNvoUDLZPlNwE.htm)|Focus Beauty|Focaliser la beauté|libre|
|[yodQpRTpnwuSLWgE.htm](pathfinder-bestiary-3-items/yodQpRTpnwuSLWgE.htm)|At-Will Spells|Sorts à volonté|libre|
|[yoFbIDWoysXLt9p4.htm](pathfinder-bestiary-3-items/yoFbIDWoysXLt9p4.htm)|Foot|Pied|libre|
|[yox41dJM9YFdHSRU.htm](pathfinder-bestiary-3-items/yox41dJM9YFdHSRU.htm)|Constant Spells|Sorts constants|libre|
|[YPcOC7ZuzY8aASqU.htm](pathfinder-bestiary-3-items/YPcOC7ZuzY8aASqU.htm)|Ghost Hunter|Chasseur de fantômes|libre|
|[YPqH3eXv1Ux2rBDb.htm](pathfinder-bestiary-3-items/YPqH3eXv1Ux2rBDb.htm)|Lifesense (Imprecise) 60 feet|Perception de la vie 18 m (imprécis)|libre|
|[ypyEw36BPA1kfq0T.htm](pathfinder-bestiary-3-items/ypyEw36BPA1kfq0T.htm)|Claw|Griffe|libre|
|[yQ6h3MLUgYAJcomj.htm](pathfinder-bestiary-3-items/yQ6h3MLUgYAJcomj.htm)|Furious Possession|Possession de fureur|libre|
|[YQs3tTvZeC9rNSZB.htm](pathfinder-bestiary-3-items/YQs3tTvZeC9rNSZB.htm)|Song of the Swamp|Chant des marais|libre|
|[yQzN4wExqO58Ssdr.htm](pathfinder-bestiary-3-items/yQzN4wExqO58Ssdr.htm)|Slow|Lent|libre|
|[YrKVoYsRIkzzPSEH.htm](pathfinder-bestiary-3-items/YrKVoYsRIkzzPSEH.htm)|Darkvision|Vision dans le noir|libre|
|[yRRl0UTbRwQx83WC.htm](pathfinder-bestiary-3-items/yRRl0UTbRwQx83WC.htm)|Smear|Fracassement brutal|libre|
|[yrUTciOjsrqkiFEh.htm](pathfinder-bestiary-3-items/yrUTciOjsrqkiFEh.htm)|Jaws|Mâchoires|libre|
|[YS29qvFcBB3p68fh.htm](pathfinder-bestiary-3-items/YS29qvFcBB3p68fh.htm)|Darkvision|Vision dans le noir|libre|
|[YsbObHTSL33vRGBA.htm](pathfinder-bestiary-3-items/YsbObHTSL33vRGBA.htm)|Darkvision|Vision dans le noir|libre|
|[ySPa5Sa7wY7Oyugj.htm](pathfinder-bestiary-3-items/ySPa5Sa7wY7Oyugj.htm)|Claw|Griffe|libre|
|[ySTvjgpwqUPCVoUE.htm](pathfinder-bestiary-3-items/ySTvjgpwqUPCVoUE.htm)|Darkvision|Vision dans le noir|libre|
|[YSVbN2hohx9XI8OH.htm](pathfinder-bestiary-3-items/YSVbN2hohx9XI8OH.htm)|Tail|Queue|libre|
|[ytbxTl9miHNeB5iO.htm](pathfinder-bestiary-3-items/ytbxTl9miHNeB5iO.htm)|Spring Up|Rebond|libre|
|[yTqrSB5RzCRPTbYd.htm](pathfinder-bestiary-3-items/yTqrSB5RzCRPTbYd.htm)|Countered by Earth|Contré par la terre|libre|
|[yUtAKhKRZtc1R8LE.htm](pathfinder-bestiary-3-items/yUtAKhKRZtc1R8LE.htm)|Athletics|Athlétisme|libre|
|[YvbJG9mY33OhkVRp.htm](pathfinder-bestiary-3-items/YvbJG9mY33OhkVRp.htm)|Negative Healing|Guérison négative|libre|
|[yVpUeHZRYHGkXayx.htm](pathfinder-bestiary-3-items/yVpUeHZRYHGkXayx.htm)|Suspended Animation|Animation suspendue|libre|
|[YwaUnI2GQySpg6fO.htm](pathfinder-bestiary-3-items/YwaUnI2GQySpg6fO.htm)|Stealth|Discrétion|libre|
|[YWWsxw1woFyoUa9a.htm](pathfinder-bestiary-3-items/YWWsxw1woFyoUa9a.htm)|Negative Healing|Guérison négative|libre|
|[Yx7C6ebcU2WQckqn.htm](pathfinder-bestiary-3-items/Yx7C6ebcU2WQckqn.htm)|Rift Sense|Perception des failles|libre|
|[YXFicpAJz9WZdrnS.htm](pathfinder-bestiary-3-items/YXFicpAJz9WZdrnS.htm)|Boneshard Burst|Éclatement d'os|libre|
|[yxOzb3edpHIxCIh9.htm](pathfinder-bestiary-3-items/yxOzb3edpHIxCIh9.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[YxUwb1otEos5UMSS.htm](pathfinder-bestiary-3-items/YxUwb1otEos5UMSS.htm)|All This Has Happened Before|Cela s'est déjà produit|libre|
|[YyJ8ioOaohHGksR2.htm](pathfinder-bestiary-3-items/YyJ8ioOaohHGksR2.htm)|Negative Healing|Guérison négative|libre|
|[yySGyBmXQKMzLIZ0.htm](pathfinder-bestiary-3-items/yySGyBmXQKMzLIZ0.htm)|+1 Status to all Saves vs. Divine and Positive|bonus de statut de +1 aux JdS contre les effets divins et positifs|libre|
|[yyvo4Lyc2M4RQhHy.htm](pathfinder-bestiary-3-items/yyvo4Lyc2M4RQhHy.htm)|Grab|Empoignade/Agrippement|libre|
|[YZALPaj0pw2QQhWF.htm](pathfinder-bestiary-3-items/YZALPaj0pw2QQhWF.htm)|Grab|Empoignade/Agrippement|libre|
|[YZgastPDC3pUUwDr.htm](pathfinder-bestiary-3-items/YZgastPDC3pUUwDr.htm)|Ink Blade|Lame d'encre|libre|
|[yzSpI7KmbcXVQQEF.htm](pathfinder-bestiary-3-items/yzSpI7KmbcXVQQEF.htm)|Mentalist Counterspell|Contresort mental|libre|
|[YzVNI5zauuNyLldf.htm](pathfinder-bestiary-3-items/YzVNI5zauuNyLldf.htm)|Sunset Dependent|Tributaire du crépuscule|libre|
|[z23Nlek8h4x4L0lq.htm](pathfinder-bestiary-3-items/z23Nlek8h4x4L0lq.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[Z2AdAroZg5MIB8bp.htm](pathfinder-bestiary-3-items/Z2AdAroZg5MIB8bp.htm)|Wide Cleave|Affliction élargie|libre|
|[z2iLBR1l32mSdxZg.htm](pathfinder-bestiary-3-items/z2iLBR1l32mSdxZg.htm)|Cynic's Curse|Malédiction d'apathie|libre|
|[Z2P5qc4Zo39S1t6w.htm](pathfinder-bestiary-3-items/Z2P5qc4Zo39S1t6w.htm)|Fist|Poing|libre|
|[Z3lZR1EPDU26XJlG.htm](pathfinder-bestiary-3-items/Z3lZR1EPDU26XJlG.htm)|Jaws|Mâchoires|libre|
|[Z4KMWDa1g8Ai7MCE.htm](pathfinder-bestiary-3-items/Z4KMWDa1g8Ai7MCE.htm)|At-Will Spells|Sorts à volonté|libre|
|[Z4wolFb7CMno5VO4.htm](pathfinder-bestiary-3-items/Z4wolFb7CMno5VO4.htm)|Forest Stride|Marche facilité en forêt|libre|
|[Z67Fh0402PQRc5fp.htm](pathfinder-bestiary-3-items/Z67Fh0402PQRc5fp.htm)|Weather Lore|Connaissance du climat|libre|
|[Z6FPbe7j4A4d8s6s.htm](pathfinder-bestiary-3-items/Z6FPbe7j4A4d8s6s.htm)|Swarming Bites|Morsures de nuée|libre|
|[Z79hB9KYIozan9AY.htm](pathfinder-bestiary-3-items/Z79hB9KYIozan9AY.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[z8GylSGuIeDFrE0Z.htm](pathfinder-bestiary-3-items/z8GylSGuIeDFrE0Z.htm)|Shy|Timide|libre|
|[z8QOqIIzB4F2TaBY.htm](pathfinder-bestiary-3-items/z8QOqIIzB4F2TaBY.htm)|Claw|Griffe|libre|
|[zaD6IUn59EF4mqzi.htm](pathfinder-bestiary-3-items/zaD6IUn59EF4mqzi.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie.|libre|
|[zAfIfdV5UBJtB564.htm](pathfinder-bestiary-3-items/zAfIfdV5UBJtB564.htm)|Smooth Swimmer|Nageur chevronné|libre|
|[zANtgVUDX9St398r.htm](pathfinder-bestiary-3-items/zANtgVUDX9St398r.htm)|Tree Shape (Cherry Tree Only)|Morphologie d'arbres (cerisier uniquement)|libre|
|[ZaqhlgxYla5yl1jh.htm](pathfinder-bestiary-3-items/ZaqhlgxYla5yl1jh.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|libre|
|[ZarnbOm1D79SECpY.htm](pathfinder-bestiary-3-items/ZarnbOm1D79SECpY.htm)|Summon Animal (Spiders Only)|Convocation d'animal (araignée uniquement)|libre|
|[zBdDYujCaw7HK02d.htm](pathfinder-bestiary-3-items/zBdDYujCaw7HK02d.htm)|Anticoagulant|Anticoagulant|libre|
|[zBG0dgN8cMdRn2tF.htm](pathfinder-bestiary-3-items/zBG0dgN8cMdRn2tF.htm)|Phantasmal Killer (Image Resembles the Brainchild)|Assassin imaginaire (Image ressemble à un monstre incarné)|libre|
|[zbmQRPZHDEeRga68.htm](pathfinder-bestiary-3-items/zbmQRPZHDEeRga68.htm)|At-Will Spells|Sorts à volonté|libre|
|[zbt0txkLR1uhzPjL.htm](pathfinder-bestiary-3-items/zbt0txkLR1uhzPjL.htm)|+1 Status to All Saves vs. Controlled Condition|bonus de statut de +1 aux JdS contre l'état Contrôlé|libre|
|[zc0tBM6QCWY3pRQR.htm](pathfinder-bestiary-3-items/zc0tBM6QCWY3pRQR.htm)|Arm|Bras|libre|
|[zChDwHyu2FWl3wIc.htm](pathfinder-bestiary-3-items/zChDwHyu2FWl3wIc.htm)|+1 Breastplate|Cuirasse +1|libre|
|[zCqM5htGK65bK9Uh.htm](pathfinder-bestiary-3-items/zCqM5htGK65bK9Uh.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[zdb8RR0jcIIol6on.htm](pathfinder-bestiary-3-items/zdb8RR0jcIIol6on.htm)|Wind-Up|Remonter un dispositif|libre|
|[zDFtneAmSUjx6qnK.htm](pathfinder-bestiary-3-items/zDFtneAmSUjx6qnK.htm)|Pointed Charge|Charge de cornes|libre|
|[ZEFMlTiTYApQQKuS.htm](pathfinder-bestiary-3-items/ZEFMlTiTYApQQKuS.htm)|Grab|Empoignade/Agrippement|libre|
|[ZEGxGxVAaZSmhNp7.htm](pathfinder-bestiary-3-items/ZEGxGxVAaZSmhNp7.htm)|Foot|Pied|libre|
|[zEouFbaKNoTFZeHy.htm](pathfinder-bestiary-3-items/zEouFbaKNoTFZeHy.htm)|Mix Couatl Venom|Venin de mix couatl|libre|
|[zeWAnIFSjhteCiiY.htm](pathfinder-bestiary-3-items/zeWAnIFSjhteCiiY.htm)|At-Will Spells|Sorts à volonté|libre|
|[zfcj9MZNHpEvkerg.htm](pathfinder-bestiary-3-items/zfcj9MZNHpEvkerg.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[ZffdskKO51EjTFvx.htm](pathfinder-bestiary-3-items/ZffdskKO51EjTFvx.htm)|Constrict|Constriction|libre|
|[ZFMGP6RS8c651Pki.htm](pathfinder-bestiary-3-items/ZFMGP6RS8c651Pki.htm)|Claw|Griffe|libre|
|[zFOxLexJnjqhpWy0.htm](pathfinder-bestiary-3-items/zFOxLexJnjqhpWy0.htm)|Needle|Aiguille|libre|
|[zFuVmNNi7HexpunZ.htm](pathfinder-bestiary-3-items/zFuVmNNi7HexpunZ.htm)|Sixfold Flurry|Frappe sextuple|libre|
|[zg034Oefe1w3sHwh.htm](pathfinder-bestiary-3-items/zg034Oefe1w3sHwh.htm)|+2 Status to All Saves vs. Primal Magic|bonus de statut de +2 aux JdS contre la magie primordiale|libre|
|[zGreN5MDFtAqJh8U.htm](pathfinder-bestiary-3-items/zGreN5MDFtAqJh8U.htm)|Darkvision|Vision dans le noir|libre|
|[zGXRWPh3UMA5q46D.htm](pathfinder-bestiary-3-items/zGXRWPh3UMA5q46D.htm)|Darkvision|Vision dans le noir|libre|
|[ZgZ9FcS7hIAO9FxC.htm](pathfinder-bestiary-3-items/ZgZ9FcS7hIAO9FxC.htm)|Girtablilu Venom|Venin girtablilu|libre|
|[Zh5jidqsamP7xlFa.htm](pathfinder-bestiary-3-items/Zh5jidqsamP7xlFa.htm)|Detect Alignment (At Will) (Good or Evil Only)|Détection de l'alignement (à volonté, bon ou mauvais uniquement)|libre|
|[Zh8VyOJVStjNfjec.htm](pathfinder-bestiary-3-items/Zh8VyOJVStjNfjec.htm)|Acrobatics|Acrobaties|libre|
|[zHIoJ0oELrIHabJu.htm](pathfinder-bestiary-3-items/zHIoJ0oELrIHabJu.htm)|Slippery|Glissant|libre|
|[ZHJXY7uHZTuD4bL6.htm](pathfinder-bestiary-3-items/ZHJXY7uHZTuD4bL6.htm)|Scatterbrain Palm|Claque brise-cervelle|libre|
|[zhUapm5Z8hOglOwU.htm](pathfinder-bestiary-3-items/zhUapm5Z8hOglOwU.htm)|Low-Light Vision|Vision nocturne|libre|
|[zILlYq3L0c0BJfWG.htm](pathfinder-bestiary-3-items/zILlYq3L0c0BJfWG.htm)|Runic Resistance|Résistance runique|libre|
|[ZiNy2010T2g4saPw.htm](pathfinder-bestiary-3-items/ZiNy2010T2g4saPw.htm)|Spiny Body|Corps épineux|libre|
|[zj5lykgkozQXgCYv.htm](pathfinder-bestiary-3-items/zj5lykgkozQXgCYv.htm)|Skip Between|Alterner|libre|
|[zJKO70Z0e4PdkPUS.htm](pathfinder-bestiary-3-items/zJKO70Z0e4PdkPUS.htm)|Quills|Piquants|libre|
|[zJprqN3Lkl03vYP7.htm](pathfinder-bestiary-3-items/zJprqN3Lkl03vYP7.htm)|Arcane Prepared Spells|Sorts arcaniques préparés|libre|
|[zjRf8vLHR2qRhuCx.htm](pathfinder-bestiary-3-items/zjRf8vLHR2qRhuCx.htm)|Kukri|Kukri|libre|
|[zJSR7LD72PzGqnDl.htm](pathfinder-bestiary-3-items/zJSR7LD72PzGqnDl.htm)|Low-Light Vision|Vision nocturne|libre|
|[zJVmAmV1kg1wV6T3.htm](pathfinder-bestiary-3-items/zJVmAmV1kg1wV6T3.htm)|Scent (Imprecise) 60 feet|Odorat 18 m (imprécis)|libre|
|[ZkPq0d1hLdzoA3Mf.htm](pathfinder-bestiary-3-items/ZkPq0d1hLdzoA3Mf.htm)|Low-Light Vision|Vision nocturne|libre|
|[zLh5jRcxUaafQvod.htm](pathfinder-bestiary-3-items/zLh5jRcxUaafQvod.htm)|Greater Constrict|Constriction supérieure|libre|
|[ZlMkrt960OMod6zh.htm](pathfinder-bestiary-3-items/ZlMkrt960OMod6zh.htm)|Jaws|Mâchoires|libre|
|[ZLvVuoT7wsCVMtYC.htm](pathfinder-bestiary-3-items/ZLvVuoT7wsCVMtYC.htm)|Frightful Presence|Présence terrifiante|libre|
|[ZmPA9nWFtMXPCArA.htm](pathfinder-bestiary-3-items/ZmPA9nWFtMXPCArA.htm)|Ganzi Spells|Sorts Ganzi|libre|
|[zMQJh0NjokM3IneE.htm](pathfinder-bestiary-3-items/zMQJh0NjokM3IneE.htm)|Draconic Momentum|Impulsion draconique|libre|
|[ZmVqNTMjqte6Xf1k.htm](pathfinder-bestiary-3-items/ZmVqNTMjqte6Xf1k.htm)|Fist|Poing|libre|
|[zMy5ikhAFWW94nIF.htm](pathfinder-bestiary-3-items/zMy5ikhAFWW94nIF.htm)|Darkvision|Vision dans le noir|libre|
|[zmZ1L8pspuQQGh1m.htm](pathfinder-bestiary-3-items/zmZ1L8pspuQQGh1m.htm)|Drain Blood|Boire le sang|libre|
|[znaX5hWmLXH23ywJ.htm](pathfinder-bestiary-3-items/znaX5hWmLXH23ywJ.htm)|Subsonic Pulse|Pulsion subsonique|libre|
|[ZneHwzrBFdJkdzsG.htm](pathfinder-bestiary-3-items/ZneHwzrBFdJkdzsG.htm)|Change Shape|Changement de forme|libre|
|[znuFqFCIX1YQTcWx.htm](pathfinder-bestiary-3-items/znuFqFCIX1YQTcWx.htm)|Resonant Chimes|Carillons résonnants|libre|
|[znWQpCRgJXaYEk5Q.htm](pathfinder-bestiary-3-items/znWQpCRgJXaYEk5Q.htm)|Grab|Empoignade/Agrippement|libre|
|[zNYXcSmgZ8w8pTTE.htm](pathfinder-bestiary-3-items/zNYXcSmgZ8w8pTTE.htm)|Tremorsense (Imprecise) 15 feet|Perception des vibrations 4,5 m (imprécis)|libre|
|[Zo7w17AAhYEJgyRJ.htm](pathfinder-bestiary-3-items/Zo7w17AAhYEJgyRJ.htm)|Catch Rock|Interception de rochers|libre|
|[zobdYiwWZOnM8PAI.htm](pathfinder-bestiary-3-items/zobdYiwWZOnM8PAI.htm)|Lifewick Candle|Bougie absorbe-vie|libre|
|[zoT7uILQYCXy5Dtz.htm](pathfinder-bestiary-3-items/zoT7uILQYCXy5Dtz.htm)|Grab|Empoignade/Agrippement|libre|
|[zOtMfpFqx7YaAJxc.htm](pathfinder-bestiary-3-items/zOtMfpFqx7YaAJxc.htm)|Crystalline Dust|Poussière cristalline|libre|
|[Zp3zBQ50NNxsfjVx.htm](pathfinder-bestiary-3-items/Zp3zBQ50NNxsfjVx.htm)|Low-Light Vision|Vision nocturne|libre|
|[ZPn8LuEY8HTRPp38.htm](pathfinder-bestiary-3-items/ZPn8LuEY8HTRPp38.htm)|Withering Aura|Aura de flétrissure|libre|
|[zPSlJozXBmZIdCsT.htm](pathfinder-bestiary-3-items/zPSlJozXBmZIdCsT.htm)|Nervous Consumption|Épuisement nerveux|libre|
|[zPU1mG2FJpTt19Z0.htm](pathfinder-bestiary-3-items/zPU1mG2FJpTt19Z0.htm)|Change Shape|Changement de forme|libre|
|[zpYoiV9dZOr8GH5F.htm](pathfinder-bestiary-3-items/zpYoiV9dZOr8GH5F.htm)|Motion Sense 60 feet|Perception du mouvement 18 m|libre|
|[zQ1jyG0TlCxqXyuU.htm](pathfinder-bestiary-3-items/zQ1jyG0TlCxqXyuU.htm)|Jaws|Mâchoires|libre|
|[zqnwkdM7RpErrayk.htm](pathfinder-bestiary-3-items/zqnwkdM7RpErrayk.htm)|Death Gasp|Soupir du mort|libre|
|[zR7WoyREJtjNrtuv.htm](pathfinder-bestiary-3-items/zR7WoyREJtjNrtuv.htm)|Rock|Pierre|libre|
|[zr91h9LPlTzhqjrE.htm](pathfinder-bestiary-3-items/zr91h9LPlTzhqjrE.htm)|True Seeing (Constant)|Vision lucide (constant)|officielle|
|[zsHciQgmfpHT7Rdu.htm](pathfinder-bestiary-3-items/zsHciQgmfpHT7Rdu.htm)|Jaws|Mâchoires|libre|
|[ZSiz1ntZMGGH8ef9.htm](pathfinder-bestiary-3-items/ZSiz1ntZMGGH8ef9.htm)|Low-Light Vision|Vision nocturne|libre|
|[zSnETnJBgsrn1A04.htm](pathfinder-bestiary-3-items/zSnETnJBgsrn1A04.htm)|Earthmound Dweller|Habitant de tunnel souterrain|libre|
|[ZsO3Se3p63hjEFm6.htm](pathfinder-bestiary-3-items/ZsO3Se3p63hjEFm6.htm)|Darkvision|Vision dans le noir|libre|
|[ZSyd5tDHWnYAj8zD.htm](pathfinder-bestiary-3-items/ZSyd5tDHWnYAj8zD.htm)|Spectral Jaws|Mâchoires spectrale|libre|
|[Zta2Kf1JwyI8OprE.htm](pathfinder-bestiary-3-items/Zta2Kf1JwyI8OprE.htm)|Burning Cold|Froid brûlant|libre|
|[ztbAEwewoiM3nyRj.htm](pathfinder-bestiary-3-items/ztbAEwewoiM3nyRj.htm)|Dagger|Dague|libre|
|[zTdgUNL0sTJoxpwC.htm](pathfinder-bestiary-3-items/zTdgUNL0sTJoxpwC.htm)|Clutching Stones|Pierres agrippantes|libre|
|[zTFuXpu6BuPdbg6N.htm](pathfinder-bestiary-3-items/zTFuXpu6BuPdbg6N.htm)|Brawling Critical|Pugilat critique|libre|
|[zTJl5YEwlSQQwXu1.htm](pathfinder-bestiary-3-items/zTJl5YEwlSQQwXu1.htm)|Stealth|Discrétion|libre|
|[Zu7OCWAY7oZL41Am.htm](pathfinder-bestiary-3-items/Zu7OCWAY7oZL41Am.htm)|Walk the Ethereal Line|En équilibre sur la frontière de l'Éthéré|libre|
|[ZuL0FdvovBnvOlaq.htm](pathfinder-bestiary-3-items/ZuL0FdvovBnvOlaq.htm)|Negative Healing|Guérison négative|libre|
|[zuM9eNga9GMtun7x.htm](pathfinder-bestiary-3-items/zuM9eNga9GMtun7x.htm)|Low-Light Vision|Vision nocturne|libre|
|[ZUqkHDdNfKyhgkOs.htm](pathfinder-bestiary-3-items/ZUqkHDdNfKyhgkOs.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|libre|
|[ZVB4BE6xf6rF4Gl7.htm](pathfinder-bestiary-3-items/ZVB4BE6xf6rF4Gl7.htm)|Tongues (Constant)|Don des langues (constant)|officielle|
|[zvCWR1v2of2jkxSl.htm](pathfinder-bestiary-3-items/zvCWR1v2of2jkxSl.htm)|Plague of Ancients|Peste des anciens|libre|
|[zVDg72KpqmTWTtgS.htm](pathfinder-bestiary-3-items/zVDg72KpqmTWTtgS.htm)|Doru Venom|Venin de doru|libre|
|[zVHM5ms0PMEfCJcT.htm](pathfinder-bestiary-3-items/zVHM5ms0PMEfCJcT.htm)|Darkness (At Will)|Ténèbres (À volonté)|officielle|
|[ZVncjXiJwtqSbdbS.htm](pathfinder-bestiary-3-items/ZVncjXiJwtqSbdbS.htm)|+4 Status to All Saves vs. Mental or Divine|bonus de statut de +4 aux JdS contre les effets mentaux ou divins|libre|
|[ZVnCqIGwZwPukDhh.htm](pathfinder-bestiary-3-items/ZVnCqIGwZwPukDhh.htm)|Spear|Lance|libre|
|[zvQC0uB9gzHndGBg.htm](pathfinder-bestiary-3-items/zvQC0uB9gzHndGBg.htm)|Air Walk (Constant)|Marche dans les airs (constant)|officielle|
|[zwfNSb5Ft4v1QTW3.htm](pathfinder-bestiary-3-items/zwfNSb5Ft4v1QTW3.htm)|Rock|Rocher|libre|
|[zwNUc8bsuHFTyEqi.htm](pathfinder-bestiary-3-items/zwNUc8bsuHFTyEqi.htm)|Coiling Frenzy|Frénésie torsadée|libre|
|[zwPydFnSps4ZXlka.htm](pathfinder-bestiary-3-items/zwPydFnSps4ZXlka.htm)|Swarmwalker|Passe-nuées|libre|
|[zWsIEzGEL4qx0Z0P.htm](pathfinder-bestiary-3-items/zWsIEzGEL4qx0Z0P.htm)|Scimitar|+1,striking|Cimeterre de frappe +1|libre|
|[zXCRAQ4vboei7ilQ.htm](pathfinder-bestiary-3-items/zXCRAQ4vboei7ilQ.htm)|Carrion Scent (Imprecise) 30 feet|Odorat du charognard 9 m (imprécis)|libre|
|[ZXDEnUnH0Uz4H2J7.htm](pathfinder-bestiary-3-items/ZXDEnUnH0Uz4H2J7.htm)|Tremorsense (Precise) 120 feet, (Imprecise) 240 feet|Perception des vibrations 36 m (précis), (imprécis) 72 m|libre|
|[ZxPGNj1JDdTfH2vH.htm](pathfinder-bestiary-3-items/ZxPGNj1JDdTfH2vH.htm)|Sunlight Powerlessness|Impuissance solaire|libre|
|[zxRmkFpH5qKW2WJe.htm](pathfinder-bestiary-3-items/zxRmkFpH5qKW2WJe.htm)|Darkvision|Vision dans le noir|libre|
|[ZY0LmvdvxDzWYzTp.htm](pathfinder-bestiary-3-items/ZY0LmvdvxDzWYzTp.htm)|At-Will Spells|Sorts à volonté|libre|
|[zY3L0jOu3AcqWwDa.htm](pathfinder-bestiary-3-items/zY3L0jOu3AcqWwDa.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[zY6HRNQ28oqHNnmV.htm](pathfinder-bestiary-3-items/zY6HRNQ28oqHNnmV.htm)|Unsettling Mind|Esprit déconcertant|libre|
|[ZYC0qWDiNlFEQBZ3.htm](pathfinder-bestiary-3-items/ZYC0qWDiNlFEQBZ3.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[Zylzzu3xfzA6axxc.htm](pathfinder-bestiary-3-items/Zylzzu3xfzA6axxc.htm)|Craft Ice Staff|Fabriquer un bâton de glace|libre|
|[zZADJ8AZtILSidCx.htm](pathfinder-bestiary-3-items/zZADJ8AZtILSidCx.htm)|Horn|Corne|libre|
|[zZdlKoZukVTRu2lC.htm](pathfinder-bestiary-3-items/zZdlKoZukVTRu2lC.htm)|Cavern Dependent|Dépendance à une caverne|libre|
|[zZdUA3JVNbgUwFrw.htm](pathfinder-bestiary-3-items/zZdUA3JVNbgUwFrw.htm)|Jump Scare|Bond d'effroi|libre|
|[ZZpXzxmtbX8o1MxN.htm](pathfinder-bestiary-3-items/ZZpXzxmtbX8o1MxN.htm)|Vulnerable to Stone to Flesh|Vulnérable à Pierre en Chair|libre|
|[zzRvWHp5hTbs93tu.htm](pathfinder-bestiary-3-items/zzRvWHp5hTbs93tu.htm)|Breath Weapon|Arme de souffle|libre|
|[ZZwg2ODhydDeHkEy.htm](pathfinder-bestiary-3-items/ZZwg2ODhydDeHkEy.htm)|Scimitar Blitz|Raid de cimeterre|libre|
|[zZzMJYEYbjykXd3S.htm](pathfinder-bestiary-3-items/zZzMJYEYbjykXd3S.htm)|Flamewing Buffet|Valse d'ailes enflammées|libre|
