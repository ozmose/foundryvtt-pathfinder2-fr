# État de la traduction (adventure-specific-actions)

 * **libre**: 88
 * **aucune**: 5
 * **officielle**: 5


Dernière mise à jour: 2023-07-12 20:28 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[46pkhUrd57gTd4th.htm](adventure-specific-actions/46pkhUrd57gTd4th.htm)|(Affinity Ablaze) Speakers to the Winds: All is One, One is All|
|[8CZwqRQ1w8OaAtO7.htm](adventure-specific-actions/8CZwqRQ1w8OaAtO7.htm)|(Affinity Ablaze) Biting Roses: Glimpses to Beyond|
|[MFyKaQVMMphwpHut.htm](adventure-specific-actions/MFyKaQVMMphwpHut.htm)|Explore the Electric Castle|
|[Pw8VOyhZP1RW7LNM.htm](adventure-specific-actions/Pw8VOyhZP1RW7LNM.htm)|(Affinity Ablaze) Winter’s Roar: Rampaging Glacier Charge|
|[PZGE4lLJ8DHbGIUI.htm](adventure-specific-actions/PZGE4lLJ8DHbGIUI.htm)|(Affinity Ablaze) Steps of the Sun: Grand Harmony|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0nyMrziUqqRcSxXD.htm](adventure-specific-actions/0nyMrziUqqRcSxXD.htm)|Fight Fires|Combattre les feux|libre|
|[0z53rJzCIGysXGTy.htm](adventure-specific-actions/0z53rJzCIGysXGTy.htm)|Fire the Cannons! (5-6)|Tirez les cannons ! (5-6)|libre|
|[236E6kEEayy8h2CF.htm](adventure-specific-actions/236E6kEEayy8h2CF.htm)|Position the Hunters|Positionner les chasseurs|libre|
|[2Kz5whXqnRsL4fEl.htm](adventure-specific-actions/2Kz5whXqnRsL4fEl.htm)|Practical Research|Recherche sur le terrain|libre|
|[2Vrg2RkEdOteyp5O.htm](adventure-specific-actions/2Vrg2RkEdOteyp5O.htm)|Perform a Trick|Présenter un tour d'adresse|libre|
|[3E5TMUDCSEI5x2bv.htm](adventure-specific-actions/3E5TMUDCSEI5x2bv.htm)|Smooth the Path|Faciliter l'infiltration|libre|
|[3P0rSASN69wXV6Fw.htm](adventure-specific-actions/3P0rSASN69wXV6Fw.htm)|Approach Duneshadow|Approcher Ombre des dunes|libre|
|[3Pr6Jk6AobttQRqN.htm](adventure-specific-actions/3Pr6Jk6AobttQRqN.htm)|Forge Documents|Contrefaire des documents|libre|
|[43vVOSRi8Lnk1Ril.htm](adventure-specific-actions/43vVOSRi8Lnk1Ril.htm)|Investigate Chamber|Enquêter dans une salle|libre|
|[52zcawpATnCLh4J9.htm](adventure-specific-actions/52zcawpATnCLh4J9.htm)|Send in the Clowns|Envoyez les clowns|libre|
|[56ajIlNy3SEvP9Ud.htm](adventure-specific-actions/56ajIlNy3SEvP9Ud.htm)|Costar|Star partenaire|libre|
|[5DVnTrN2xt3NINGZ.htm](adventure-specific-actions/5DVnTrN2xt3NINGZ.htm)|Make General Repairs|Effectuer des réparations générales|libre|
|[5QQqv1A3aG6G2Sui.htm](adventure-specific-actions/5QQqv1A3aG6G2Sui.htm)|Rebuild Collapsed Stairs|Reconstruire les escaliers effondrés|libre|
|[6hrDW8tF6CDIaZl7.htm](adventure-specific-actions/6hrDW8tF6CDIaZl7.htm)|Dream Research|Recherches dans les rêves|libre|
|[764EVGVADrDSbdqu.htm](adventure-specific-actions/764EVGVADrDSbdqu.htm)|Forgive Foe|Pardonner l'ennemi|libre|
|[799p70dXF3UYkTih.htm](adventure-specific-actions/799p70dXF3UYkTih.htm)|Communicate From Beyond|Transmettre de l'au-delà|libre|
|[7WczIXwjOs9raMja.htm](adventure-specific-actions/7WczIXwjOs9raMja.htm)|Fey Luck|Chance de la fée|libre|
|[9NWMchSkF940L0SW.htm](adventure-specific-actions/9NWMchSkF940L0SW.htm)|Invoke Eiseth|Convocation d'Eiseth|libre|
|[aCFKA59YgGDNnSJ8.htm](adventure-specific-actions/aCFKA59YgGDNnSJ8.htm)|Exhale Poison|Exhaler du poison|libre|
|[aUgAKW3SWhU9ATl8.htm](adventure-specific-actions/aUgAKW3SWhU9ATl8.htm)|Repair Crumbled Walls|Réparer les murs effondrés|libre|
|[AvpKHblov4xjANaH.htm](adventure-specific-actions/AvpKHblov4xjANaH.htm)|Build Training Facility|Construire une salle d'entraînement|libre|
|[aw2JzLBAsmO8dD3r.htm](adventure-specific-actions/aw2JzLBAsmO8dD3r.htm)|Fire the Cannons! (3-4)|Tirez avec les canons ! (3-4)|libre|
|[awJY2ylrsVEJNTFi.htm](adventure-specific-actions/awJY2ylrsVEJNTFi.htm)|Pander to the Crowd|Rallier la foule|libre|
|[Bh1Qnh8JP4CMysRK.htm](adventure-specific-actions/Bh1Qnh8JP4CMysRK.htm)|Shootist's Draw|Interaction du tireur|libre|
|[BnKNrkcM6F3v0p7s.htm](adventure-specific-actions/BnKNrkcM6F3v0p7s.htm)|Haul Supplies|Transporter des provisions|libre|
|[Bw2L4guiRFphTpF1.htm](adventure-specific-actions/Bw2L4guiRFphTpF1.htm)|Appeal to Shadowy Intruders|Appel aux intrus ombreux|libre|
|[cGgeAlsnsSQPSPDQ.htm](adventure-specific-actions/cGgeAlsnsSQPSPDQ.htm)|Upgrade Defenses|Améliorer les défenses|libre|
|[cp8eFBCr1n7xIaxq.htm](adventure-specific-actions/cp8eFBCr1n7xIaxq.htm)|Rebuild Battlements|Reconstruire les remparts|libre|
|[CRiItJtc8N9Hc0X0.htm](adventure-specific-actions/CRiItJtc8N9Hc0X0.htm)|Explore the Vault of Boundless Wonder|Explorer le coffre-fort des Merveilles sans limites|libre|
|[Dir7OyCss7H1XQGX.htm](adventure-specific-actions/Dir7OyCss7H1XQGX.htm)|Organize Labor|Organiser le travail|libre|
|[dqE9pP9Pv0JG9d8X.htm](adventure-specific-actions/dqE9pP9Pv0JG9d8X.htm)|Smuggled|Passer en contrebande|libre|
|[EUU6zUeCzYm9jIhT.htm](adventure-specific-actions/EUU6zUeCzYm9jIhT.htm)|Find the Cells|Trouver les cellules|libre|
|[FLz8SEF0Y4UEavvD.htm](adventure-specific-actions/FLz8SEF0Y4UEavvD.htm)|Hunt the Animals|Chasser les animaux|libre|
|[fUNCyoyLgpIYFLe1.htm](adventure-specific-actions/fUNCyoyLgpIYFLe1.htm)|Influence Guild|Influencer une guilde|libre|
|[FyT7VwMCJjjHDSgO.htm](adventure-specific-actions/FyT7VwMCJjjHDSgO.htm)|Contact Steel Falcons|Contacter les Faucons d'Acier|libre|
|[G8xZPhzoLF1SGyV9.htm](adventure-specific-actions/G8xZPhzoLF1SGyV9.htm)|Diviner on Duty|Vigilance du Devin|libre|
|[GhahZSxNPSrabaA6.htm](adventure-specific-actions/GhahZSxNPSrabaA6.htm)|Scout the Facility|Reconnaître les lieux|libre|
|[GpT5RXUwhINODqkJ.htm](adventure-specific-actions/GpT5RXUwhINODqkJ.htm)|(Affinity Ablaze) Arms of Balance: Walking the Cardinal Paths|Bras de l'équilibre (Affinité embrasée) : Arpenter les chemins cardinaux|libre|
|[HcASfeYcE8QXayfk.htm](adventure-specific-actions/HcASfeYcE8QXayfk.htm)|Check the Walls|Étudier les murs|libre|
|[hQ5BAIjAKpp2dYhR.htm](adventure-specific-actions/hQ5BAIjAKpp2dYhR.htm)|Build Connections|Construire des relations|libre|
|[hq5KwJRPbTVcoD3k.htm](adventure-specific-actions/hq5KwJRPbTVcoD3k.htm)|Dispel a Disguise|Dissiper un déguisement|libre|
|[hvvzc86tW5MgElMB.htm](adventure-specific-actions/hvvzc86tW5MgElMB.htm)|Assessing Tatzlford's Defenses|Estimer les défenses de Tatzlford|libre|
|[I19VNyhXYaFCpsxl.htm](adventure-specific-actions/I19VNyhXYaFCpsxl.htm)|Scout Duneshadow|Pister Ombre des dunes|libre|
|[IQewgylxmkmBYcoY.htm](adventure-specific-actions/IQewgylxmkmBYcoY.htm)|Secure Invitation|Obtenir des invitations|libre|
|[iQfHTjg9dNLbuzr8.htm](adventure-specific-actions/iQfHTjg9dNLbuzr8.htm)|Clear Courtyard|Déblayer la cour intérieure|libre|
|[IuD5u9tSTabZ0KD1.htm](adventure-specific-actions/IuD5u9tSTabZ0KD1.htm)|Build Library|Construire une bibliothèque|libre|
|[jwo5CvftA5puYp7i.htm](adventure-specific-actions/jwo5CvftA5puYp7i.htm)|Mesmerizing Performance|Représentation captivante|libre|
|[KIU5eDZP9VyQIfas.htm](adventure-specific-actions/KIU5eDZP9VyQIfas.htm)|Protector's Interdiction|Interdiction du protecteur|libre|
|[KmpPAOjNP980NuCY.htm](adventure-specific-actions/KmpPAOjNP980NuCY.htm)|Repair Huntergate|Réparer le portail des Chasseurs|libre|
|[KsYvgnBNvdC23gnC.htm](adventure-specific-actions/KsYvgnBNvdC23gnC.htm)|Erect Barricades|Ériger des barricades|libre|
|[L8UbCtOYzgEutput.htm](adventure-specific-actions/L8UbCtOYzgEutput.htm)|Fight the Fire|Combattre le feu|libre|
|[lD2RA75awEu4cG7e.htm](adventure-specific-actions/lD2RA75awEu4cG7e.htm)|Promote the Circus|Promouvoir le cirque|libre|
|[LRuwz61jNmIfQYby.htm](adventure-specific-actions/LRuwz61jNmIfQYby.htm)|Shed Time|Verser le Temps|libre|
|[lySoX0VbIaEEEnDZ.htm](adventure-specific-actions/lySoX0VbIaEEEnDZ.htm)|Clean|Nettoyage|libre|
|[m9Si1ygkv9ISjKVN.htm](adventure-specific-actions/m9Si1ygkv9ISjKVN.htm)|Navigate Steamgrotto|Navigation dans la Grotte de vapeur|libre|
|[MgSwLes5lp3TE1ZV.htm](adventure-specific-actions/MgSwLes5lp3TE1ZV.htm)|Mental Ward|Barrière mentale|libre|
|[mluc8JLd20HjGrqu.htm](adventure-specific-actions/mluc8JLd20HjGrqu.htm)|Convince Mengkare|Convaincre Mengkare|libre|
|[nLLgAxo4IHebsyg1.htm](adventure-specific-actions/nLLgAxo4IHebsyg1.htm)|Deadly Traps|Pièges mortels|libre|
|[nmgmPqExUZt5u5Wr.htm](adventure-specific-actions/nmgmPqExUZt5u5Wr.htm)|Rotate the Wheel|Tourner la roue|libre|
|[nVdoKUIWaJ47xMuB.htm](adventure-specific-actions/nVdoKUIWaJ47xMuB.htm)|Distract Guards|Distraire les gardes|libre|
|[o3u4snDwjBDcNlG3.htm](adventure-specific-actions/o3u4snDwjBDcNlG3.htm)|Topple Crates|Renverser des caisses|libre|
|[O4MWAxAYxNkndVnt.htm](adventure-specific-actions/O4MWAxAYxNkndVnt.htm)|Shortcut Through the Wastes|Raccourci à travers la Désolation|libre|
|[o6hu1my40jfcLHqD.htm](adventure-specific-actions/o6hu1my40jfcLHqD.htm)|Host Event|Organisation d'un événement|libre|
|[OSFi8oH5ndLgnksD.htm](adventure-specific-actions/OSFi8oH5ndLgnksD.htm)|Search the Laughing Jungle|Recherches dans la Jungle Riante|libre|
|[OV77buFW6zHl4Smo.htm](adventure-specific-actions/OV77buFW6zHl4Smo.htm)|Breaking and Entering|Entrer par effraction|libre|
|[OxRT8qhujKG9Rhb2.htm](adventure-specific-actions/OxRT8qhujKG9Rhb2.htm)|Build Infirmary|Construire un dispensaire|libre|
|[pQIgAdZucEEWCMfL.htm](adventure-specific-actions/pQIgAdZucEEWCMfL.htm)|Gather Information|Recueillir des informations|libre|
|[pWxRtCeAw4XnyzoM.htm](adventure-specific-actions/pWxRtCeAw4XnyzoM.htm)|Post Snipers|Placer des tireurs embusqués|libre|
|[Qkm7jcKPA3elk9Nx.htm](adventure-specific-actions/Qkm7jcKPA3elk9Nx.htm)|Administer|Gérer|libre|
|[QW2gYIKce3W31xXf.htm](adventure-specific-actions/QW2gYIKce3W31xXf.htm)|Prove Peace|Prouver la paix|officielle|
|[R90HXdiRwl0Fa4wb.htm](adventure-specific-actions/R90HXdiRwl0Fa4wb.htm)|Loot the Vaults|Piller les Coffres|libre|
|[rI9qKyONeMPtajZ8.htm](adventure-specific-actions/rI9qKyONeMPtajZ8.htm)|Build Workshop (Crafting)|Construire un atelier (Artisanat)|libre|
|[rKWfjflS15KEB3Yt.htm](adventure-specific-actions/rKWfjflS15KEB3Yt.htm)|De-Animating Gestures (False)|Geste de désanimation (Faux)|libre|
|[rR8UYHHpo2Rffi1p.htm](adventure-specific-actions/rR8UYHHpo2Rffi1p.htm)|De-Animating Gestures (True)|Geste de désanimation (Vrai)|libre|
|[RX62MAyEUtuHMNBm.htm](adventure-specific-actions/RX62MAyEUtuHMNBm.htm)|Guild Investigation|Enquêter à la guilde|officielle|
|[sT8EpCnySUSiBqBp.htm](adventure-specific-actions/sT8EpCnySUSiBqBp.htm)|Locked Doors|Portes fermées à clé|libre|
|[sWTvJahHpdz4CC6E.htm](adventure-specific-actions/sWTvJahHpdz4CC6E.htm)|Recruit Wildlife|Recruter des animaux sauvages|libre|
|[tay92zbn04IR40qv.htm](adventure-specific-actions/tay92zbn04IR40qv.htm)|Prepare Firepots|Préparer des chaudrons de feu|libre|
|[TM4pOPSM9r7XEM64.htm](adventure-specific-actions/TM4pOPSM9r7XEM64.htm)|Soul Ward|Gardien de l'âme|libre|
|[Vt6CuD83hPiyoiOZ.htm](adventure-specific-actions/Vt6CuD83hPiyoiOZ.htm)|Study|Étudier|libre|
|[WKFZlmmuZGnucRen.htm](adventure-specific-actions/WKFZlmmuZGnucRen.htm)|Breaking the Chains|Briser les chaînes|libre|
|[WlXmO2kwyWGgAuOv.htm](adventure-specific-actions/WlXmO2kwyWGgAuOv.htm)|Deduce Traditions|Déduire les traditions|officielle|
|[x5hIMfjmsDlpQWyt.htm](adventure-specific-actions/x5hIMfjmsDlpQWyt.htm)|Blend In|Se mêler à la foule|libre|
|[xklnt1hmdFnix64F.htm](adventure-specific-actions/xklnt1hmdFnix64F.htm)|Rescue Citizens|Sauver les résidents|libre|
|[XTDd73QzhETeXY3g.htm](adventure-specific-actions/XTDd73QzhETeXY3g.htm)|Secure Disguises|Déguisement précis|libre|
|[XUNM9eqfhnSaPVov.htm](adventure-specific-actions/XUNM9eqfhnSaPVov.htm)|Steal Keys|Voler les clés|libre|
|[XyfC1zPgSTP01ZUr.htm](adventure-specific-actions/XyfC1zPgSTP01ZUr.htm)|Cram|Bachoter|libre|
|[Ys6rFLuyxocQy2hA.htm](adventure-specific-actions/Ys6rFLuyxocQy2hA.htm)|Seek the Hidden Forge|Localiser la forge cachée|officielle|
|[YvSjpOAI9bCDQU5h.htm](adventure-specific-actions/YvSjpOAI9bCDQU5h.htm)|Seek the Animals|Chercher des animaux|libre|
|[yy5PeDyV7kIWlPOU.htm](adventure-specific-actions/yy5PeDyV7kIWlPOU.htm)|Influence Regent|Influencer un régent|officielle|
|[z1noqZiavhnqQL50.htm](adventure-specific-actions/z1noqZiavhnqQL50.htm)|Set Traps|Poser des pièges|libre|
|[zF3q1nTANa5NYYJu.htm](adventure-specific-actions/zF3q1nTANa5NYYJu.htm)|Cunning Disguise|Déguisements astucieux|libre|
|[zIwbbth7qyKraiWV.htm](adventure-specific-actions/zIwbbth7qyKraiWV.htm)|Issue Challenge|Lancer un défi|libre|
