# État de la traduction (abomination-vaults-bestiary-items)

 * **officielle**: 826
 * **libre**: 9


Dernière mise à jour: 2023-07-12 20:28 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[01CKBc5uwpZchw7t.htm](abomination-vaults-bestiary-items/01CKBc5uwpZchw7t.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|officielle|
|[01oCBNgr4LTyuncP.htm](abomination-vaults-bestiary-items/01oCBNgr4LTyuncP.htm)|Infused Acid Flask (Moderate)|Fiole d'acide modérées imprégnées|officielle|
|[06lGWrOe3DOYvTgV.htm](abomination-vaults-bestiary-items/06lGWrOe3DOYvTgV.htm)|Bright Release|Délivrance lumineuse|officielle|
|[09R0B0sIkwFfNoZ5.htm](abomination-vaults-bestiary-items/09R0B0sIkwFfNoZ5.htm)|Shuriken|Shuriken|officielle|
|[0agBOEbezDUSkYUQ.htm](abomination-vaults-bestiary-items/0agBOEbezDUSkYUQ.htm)|Greater Darkvision|Vision dans le noir supérieure|officielle|
|[0dO1DU5gt5v8bFwc.htm](abomination-vaults-bestiary-items/0dO1DU5gt5v8bFwc.htm)|Swallow Whole|Gober|officielle|
|[0gD4v7579cLZZRwx.htm](abomination-vaults-bestiary-items/0gD4v7579cLZZRwx.htm)|+1 Striking Handwraps of Mighty Blows|+1,striking|Bandelettes de coups puissants de frappe +1|officielle|
|[0mexpNYfz6rpTLXa.htm](abomination-vaults-bestiary-items/0mexpNYfz6rpTLXa.htm)|Kukri|+1,striking|Kukri de frappe +1|officielle|
|[0PqUwOqIbxXioOPh.htm](abomination-vaults-bestiary-items/0PqUwOqIbxXioOPh.htm)|Negative Healing|Guérison négative|officielle|
|[0QhLcUbXJtWWVCmp.htm](abomination-vaults-bestiary-items/0QhLcUbXJtWWVCmp.htm)|Paralysis|Paralysie|officielle|
|[0siiEWVFxVLV5eB0.htm](abomination-vaults-bestiary-items/0siiEWVFxVLV5eB0.htm)|Obscuring Mist (At Will)|Brume de dissimulation (À volonté)|officielle|
|[11ioR4E9tTmB1Wak.htm](abomination-vaults-bestiary-items/11ioR4E9tTmB1Wak.htm)|Filth Fever|Fièvre de la fange|officielle|
|[122ICOgqYvLmpu9U.htm](abomination-vaults-bestiary-items/122ICOgqYvLmpu9U.htm)|Sneak Attack|Attaque sournoise|officielle|
|[18GcGCMe0ekCnLyC.htm](abomination-vaults-bestiary-items/18GcGCMe0ekCnLyC.htm)|Dagger|Dague|officielle|
|[19toTT0P9kxuQAmz.htm](abomination-vaults-bestiary-items/19toTT0P9kxuQAmz.htm)|At-Will Spells|Sorts à volonté|officielle|
|[1B109bqthYI0DGyC.htm](abomination-vaults-bestiary-items/1B109bqthYI0DGyC.htm)|Innocuous|Inoffensif|officielle|
|[1CumgFDHAf5o5Yfs.htm](abomination-vaults-bestiary-items/1CumgFDHAf5o5Yfs.htm)|Breath Weapon|Souffle|officielle|
|[1FEKSwqCrM8p8ho7.htm](abomination-vaults-bestiary-items/1FEKSwqCrM8p8ho7.htm)|Negative Healing|Guérison négative|officielle|
|[1G0kEOTWyivIisjY.htm](abomination-vaults-bestiary-items/1G0kEOTWyivIisjY.htm)|Psychokinetic Honing|Affûtage psychokinétique|officielle|
|[1gu2mF6EgXl6v3eM.htm](abomination-vaults-bestiary-items/1gu2mF6EgXl6v3eM.htm)|Crafting|Artisanat|officielle|
|[1IcPKJ8QCEzRvPPY.htm](abomination-vaults-bestiary-items/1IcPKJ8QCEzRvPPY.htm)|Light Hammer|Marteau de guerre léger|officielle|
|[1lJU8hzSml2TMM3M.htm](abomination-vaults-bestiary-items/1lJU8hzSml2TMM3M.htm)|Telepathy 100 feet|Télépathie 30 m|officielle|
|[1M3Chy2EO0WvLGrF.htm](abomination-vaults-bestiary-items/1M3Chy2EO0WvLGrF.htm)|Darkness (At Will)|Ténèbres (À volonté)|officielle|
|[1SovHiGHboDM9yGe.htm](abomination-vaults-bestiary-items/1SovHiGHboDM9yGe.htm)|Light Aura|Aura de lumière|officielle|
|[1tb7DMhGgvu0yKi2.htm](abomination-vaults-bestiary-items/1tb7DMhGgvu0yKi2.htm)|Seugathi Venom|Venin seugathi|officielle|
|[1ubYo2q1E42ysH5K.htm](abomination-vaults-bestiary-items/1ubYo2q1E42ysH5K.htm)|Darkvision|Vision dans le noir|officielle|
|[1zCZasUbrbz7SMAh.htm](abomination-vaults-bestiary-items/1zCZasUbrbz7SMAh.htm)|Flicker|Clignoter|officielle|
|[2CDZBmJ8jMDA4sZk.htm](abomination-vaults-bestiary-items/2CDZBmJ8jMDA4sZk.htm)|Feather Fall (At Will) (Self Only)|Feuille morte (À volonté) (soi uniquement)|officielle|
|[2l5Df8352WwEuAXJ.htm](abomination-vaults-bestiary-items/2l5Df8352WwEuAXJ.htm)|Swarming Bites|Nuée de morsures|officielle|
|[2lRJXkUSUIu9w7Vg.htm](abomination-vaults-bestiary-items/2lRJXkUSUIu9w7Vg.htm)|Elven Curve Blade|Lame courbe elfique|officielle|
|[2NEvfMuBqdlReHe7.htm](abomination-vaults-bestiary-items/2NEvfMuBqdlReHe7.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|officielle|
|[2pz6VpKOe8cRxqUY.htm](abomination-vaults-bestiary-items/2pz6VpKOe8cRxqUY.htm)|Guiding Rhythm|Rythme entraînant|officielle|
|[2rGZ3A8JPafZZZur.htm](abomination-vaults-bestiary-items/2rGZ3A8JPafZZZur.htm)|Terrifying Charge|Ruée terrifiante|officielle|
|[2Ufw7cZauI4kkDrY.htm](abomination-vaults-bestiary-items/2Ufw7cZauI4kkDrY.htm)|Starknife|Lamétoile|officielle|
|[2wByf3NmPbP0YRzy.htm](abomination-vaults-bestiary-items/2wByf3NmPbP0YRzy.htm)|Leng Ruby|Rubis de Leng|officielle|
|[37DJ9kew2cByiJDV.htm](abomination-vaults-bestiary-items/37DJ9kew2cByiJDV.htm)|Self-Loathing|Auto-dénigrement|officielle|
|[3b3sEGxcz23toRTi.htm](abomination-vaults-bestiary-items/3b3sEGxcz23toRTi.htm)|Wavesense (Imprecise) 60 feet|Perception des ondes 18 m (imprécis)|officielle|
|[3gFefPT1vPEUJqTj.htm](abomination-vaults-bestiary-items/3gFefPT1vPEUJqTj.htm)|Sneak Attack|Attaque sournoise|libre|
|[3mqnU8Lr3Tl3zgd2.htm](abomination-vaults-bestiary-items/3mqnU8Lr3Tl3zgd2.htm)|Claw|Griffes|officielle|
|[3RI2spwyiEWuXdgF.htm](abomination-vaults-bestiary-items/3RI2spwyiEWuXdgF.htm)|Negative Healing|Guérison négative|officielle|
|[3sSrZrwFGWNyzaIj.htm](abomination-vaults-bestiary-items/3sSrZrwFGWNyzaIj.htm)|Light Blindness|Aveuglé par la lumière|officielle|
|[3Y7de8nXARx61nEk.htm](abomination-vaults-bestiary-items/3Y7de8nXARx61nEk.htm)|Spray Toxic Oil|Vomir de l’huile toxique|officielle|
|[41bzyFqCaaL6LCZF.htm](abomination-vaults-bestiary-items/41bzyFqCaaL6LCZF.htm)|Scriptorium Lore|Connaissance des scriptoriums|officielle|
|[41jyyqtM7TidzhJx.htm](abomination-vaults-bestiary-items/41jyyqtM7TidzhJx.htm)|Greater Darkvision|Vision dans le noir supérieure|officielle|
|[43FJrgdgr667MXKo.htm](abomination-vaults-bestiary-items/43FJrgdgr667MXKo.htm)|Darkvision|Vision dans le noir|officielle|
|[45yP2nFzQUcp3Zvm.htm](abomination-vaults-bestiary-items/45yP2nFzQUcp3Zvm.htm)|Feed on Fear|Se nourrir de la peur|officielle|
|[4GEUTovCKhSoT0PD.htm](abomination-vaults-bestiary-items/4GEUTovCKhSoT0PD.htm)|Stupor Poison|Poison de stupeur|officielle|
|[4k6yZiC3ggdlq57F.htm](abomination-vaults-bestiary-items/4k6yZiC3ggdlq57F.htm)|Ghostly Hand|Main spectrale|officielle|
|[4mS9YqseWjLaRiQq.htm](abomination-vaults-bestiary-items/4mS9YqseWjLaRiQq.htm)|Skirmishing Dash|Avancée du tirailleur|officielle|
|[4pjPyWF2dnob8Tlm.htm](abomination-vaults-bestiary-items/4pjPyWF2dnob8Tlm.htm)|Necrotic Decay|Décomposition nécrotique|officielle|
|[4UocioViS4VkF9mM.htm](abomination-vaults-bestiary-items/4UocioViS4VkF9mM.htm)|Scythe Shuffle|Faux mobiles|officielle|
|[51r9xhJpaBdZxkK3.htm](abomination-vaults-bestiary-items/51r9xhJpaBdZxkK3.htm)|Darkvision|Vision dans le noir|officielle|
|[5CnYknDSEBbdP7iX.htm](abomination-vaults-bestiary-items/5CnYknDSEBbdP7iX.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|officielle|
|[5dcaJKOKohQRyeQx.htm](abomination-vaults-bestiary-items/5dcaJKOKohQRyeQx.htm)|Glow|Lueur|officielle|
|[5dn4uxBWvzHmjJF9.htm](abomination-vaults-bestiary-items/5dn4uxBWvzHmjJF9.htm)|Mummy Rot|Putréfaction de la momie|officielle|
|[5FuqfP9EivTFAxuL.htm](abomination-vaults-bestiary-items/5FuqfP9EivTFAxuL.htm)|Soul Gem|Gemme d'âme|officielle|
|[5ghMpQ8FJkhKBa0c.htm](abomination-vaults-bestiary-items/5ghMpQ8FJkhKBa0c.htm)|Darkness (At Will)|Ténèbres (À volonté)|officielle|
|[5htUbZNH2cz3d5sv.htm](abomination-vaults-bestiary-items/5htUbZNH2cz3d5sv.htm)|Club|Gourdin|officielle|
|[5jRdb5HrdzxGCFRq.htm](abomination-vaults-bestiary-items/5jRdb5HrdzxGCFRq.htm)|Faerie Fire (At Will)|Lueurs féeriques (À volonté)|officielle|
|[5NK2vxZn4QPORBlo.htm](abomination-vaults-bestiary-items/5NK2vxZn4QPORBlo.htm)|Scalathrax Venom|Venin de scalathraxe|officielle|
|[5OrtHpltPsigs3A8.htm](abomination-vaults-bestiary-items/5OrtHpltPsigs3A8.htm)|Jaws|Mâchoires|officielle|
|[5pnWBWoyvDchoWWQ.htm](abomination-vaults-bestiary-items/5pnWBWoyvDchoWWQ.htm)|Divine Innate Spells|Sorts divins innés|officielle|
|[5rrKtIIzYEri3htn.htm](abomination-vaults-bestiary-items/5rrKtIIzYEri3htn.htm)|Wolf Coordination|Coordination avec le loup|officielle|
|[5s3WbfpPg11BNAkO.htm](abomination-vaults-bestiary-items/5s3WbfpPg11BNAkO.htm)|Apocalypse Beam (From Tsunami)|Faisceau apocalypste (du tsunami)|officielle|
|[5Teyg9iopHoZ6xTN.htm](abomination-vaults-bestiary-items/5Teyg9iopHoZ6xTN.htm)|Death's Grace|Grâce de la mort|officielle|
|[5TV0w8GnwrjMX6pX.htm](abomination-vaults-bestiary-items/5TV0w8GnwrjMX6pX.htm)|Apocalypse Beam (from Earthquake)|Faisceau apocalyptique (du tremblement de terre)|officielle|
|[5UlLFoV3FkK9X6Xn.htm](abomination-vaults-bestiary-items/5UlLFoV3FkK9X6Xn.htm)|True Seeing (Constant)|Vision lucide (constant)|officielle|
|[5yALvKiWzaN3usex.htm](abomination-vaults-bestiary-items/5yALvKiWzaN3usex.htm)|Darkness (At Will)|Ténèbres (À volonté)|officielle|
|[5YFP1XKwVqEkmjZV.htm](abomination-vaults-bestiary-items/5YFP1XKwVqEkmjZV.htm)|Swift Leap|Bond rapide|officielle|
|[5YNto5CTjVF4xtDp.htm](abomination-vaults-bestiary-items/5YNto5CTjVF4xtDp.htm)|At-Will Spells|Sorts à volonté|officielle|
|[5ZtyVQVWawYV83pF.htm](abomination-vaults-bestiary-items/5ZtyVQVWawYV83pF.htm)|Feather Fall (At Will) (Self Only)|Feuille morte (À volonté) (soi uniquement)|officielle|
|[5ZWQSqoOaC6KzTtA.htm](abomination-vaults-bestiary-items/5ZWQSqoOaC6KzTtA.htm)|Darkness (At Will)|Ténèbres (À volonté)|officielle|
|[60GCM4eYgqqlPJau.htm](abomination-vaults-bestiary-items/60GCM4eYgqqlPJau.htm)|Light Blindness|Aveuglé par la lumière|officielle|
|[65o1CNgkOxBuaZU6.htm](abomination-vaults-bestiary-items/65o1CNgkOxBuaZU6.htm)|At-Will Spells|Sorts à volonté|officielle|
|[6BBZEA8kkI2GaLNH.htm](abomination-vaults-bestiary-items/6BBZEA8kkI2GaLNH.htm)|Shauth Lash|Fouet shauth|officielle|
|[6EdSmIm5HogvdHr3.htm](abomination-vaults-bestiary-items/6EdSmIm5HogvdHr3.htm)|Jaws|Mâchoires|officielle|
|[6FmfnLeOcKMh8v5n.htm](abomination-vaults-bestiary-items/6FmfnLeOcKMh8v5n.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|officielle|
|[6glfioS5uXVucJjF.htm](abomination-vaults-bestiary-items/6glfioS5uXVucJjF.htm)|Greater Darkvision|Vision dans le noir supérieure|officielle|
|[6j3vC2OBRq8nReei.htm](abomination-vaults-bestiary-items/6j3vC2OBRq8nReei.htm)|Divine Innate Spells|Sorts divins innés|officielle|
|[6P6uGjuZLcSJBoME.htm](abomination-vaults-bestiary-items/6P6uGjuZLcSJBoME.htm)|Light Blindness|Aveuglé par la lumière|officielle|
|[6pIoss4a281w0Fed.htm](abomination-vaults-bestiary-items/6pIoss4a281w0Fed.htm)|Light Flare|Éclat de lumière|officielle|
|[6sW128BEd2N5fP96.htm](abomination-vaults-bestiary-items/6sW128BEd2N5fP96.htm)|At-Will Spells|Sorts à volonté|officielle|
|[6sz5Y1Ptd0vhrLiM.htm](abomination-vaults-bestiary-items/6sz5Y1Ptd0vhrLiM.htm)|+1 Striking Corrosive Longsword|+1,striking,corrosive|Épée longue corrosive de frappe +1|officielle|
|[6TMEJEZsiK6x4E68.htm](abomination-vaults-bestiary-items/6TMEJEZsiK6x4E68.htm)|Necrotic Decay|Décomposition nécrotique|officielle|
|[6uvSpdy8ezlFUIIo.htm](abomination-vaults-bestiary-items/6uvSpdy8ezlFUIIo.htm)|Shred Flesh|Déchiqueter les chairs|officielle|
|[6XRN01EUnaz1YCgJ.htm](abomination-vaults-bestiary-items/6XRN01EUnaz1YCgJ.htm)|Hand Crossbow|Arbalète de poing|officielle|
|[72l4ViSD8vfFJXpK.htm](abomination-vaults-bestiary-items/72l4ViSD8vfFJXpK.htm)|Battle Lute|+1,striking|Luth de combat de frappe +1|officielle|
|[72UcADd2WTnoNikm.htm](abomination-vaults-bestiary-items/72UcADd2WTnoNikm.htm)|Occult Prepared Spells|Sorts occultes préparés|officielle|
|[78oc41E029XUkMoS.htm](abomination-vaults-bestiary-items/78oc41E029XUkMoS.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[7hyO9Fv7Tl6MS4L3.htm](abomination-vaults-bestiary-items/7hyO9Fv7Tl6MS4L3.htm)|Skull Rot|Céphalite déliquescente|officielle|
|[7JPnTGzj40aMM3Hk.htm](abomination-vaults-bestiary-items/7JPnTGzj40aMM3Hk.htm)|Wand of Status (Level 2)|Baguette de Rapport (niveau 2)|officielle|
|[7mOx4vdvxyfHNF71.htm](abomination-vaults-bestiary-items/7mOx4vdvxyfHNF71.htm)|Blood Magic|Magie du sang|officielle|
|[7QmN8VFF7J1MzXXO.htm](abomination-vaults-bestiary-items/7QmN8VFF7J1MzXXO.htm)|Darkvision|Vision dans le noir|officielle|
|[7QQL3bbmfxHIb7Tz.htm](abomination-vaults-bestiary-items/7QQL3bbmfxHIb7Tz.htm)|Greater Darkvision|Vision dans le noir supérieure|officielle|
|[7sBSRQrV9kFrhQxu.htm](abomination-vaults-bestiary-items/7sBSRQrV9kFrhQxu.htm)|Bite|Morsure|officielle|
|[7Scw9mC9n5d0Jon5.htm](abomination-vaults-bestiary-items/7Scw9mC9n5d0Jon5.htm)|Spear Frog Poison|Poison de grenouille-lance|officielle|
|[7tCu8SPq0TpF1lJX.htm](abomination-vaults-bestiary-items/7tCu8SPq0TpF1lJX.htm)|Mindfog Aura|Aura de brume mentale|officielle|
|[7VBvaubtWnbIVsZ9.htm](abomination-vaults-bestiary-items/7VBvaubtWnbIVsZ9.htm)|At-Will Spells|Sorts à volonté|officielle|
|[7wW5Mpc0dnnQnoko.htm](abomination-vaults-bestiary-items/7wW5Mpc0dnnQnoko.htm)|Wicked Bite|Méchante morsure|officielle|
|[7Y7vl4vBRBKsb6wB.htm](abomination-vaults-bestiary-items/7Y7vl4vBRBKsb6wB.htm)|Jaws|Mâchoires|officielle|
|[800HkICSLpenBkq9.htm](abomination-vaults-bestiary-items/800HkICSLpenBkq9.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|officielle|
|[87c42mEQ6XMyoNBu.htm](abomination-vaults-bestiary-items/87c42mEQ6XMyoNBu.htm)|Read the Stars|Lecture des étoiles|officielle|
|[8CznKBBokxBMcUVf.htm](abomination-vaults-bestiary-items/8CznKBBokxBMcUVf.htm)|Warfare Lore|Connaissance de la guerre|officielle|
|[8g0Gpku3xbzwpSTx.htm](abomination-vaults-bestiary-items/8g0Gpku3xbzwpSTx.htm)|Go Dark|S'éteindre|officielle|
|[8HBlAYPbdVvURrBC.htm](abomination-vaults-bestiary-items/8HBlAYPbdVvURrBC.htm)|Research Book|Livre de recherches|officielle|
|[8l05kDJb53W6NV0L.htm](abomination-vaults-bestiary-items/8l05kDJb53W6NV0L.htm)|Jaws|Mâchoires|officielle|
|[8nidqgL5pawfDVZa.htm](abomination-vaults-bestiary-items/8nidqgL5pawfDVZa.htm)|Darkvision|Vision dans le noir|officielle|
|[8QZHl09gya9bBM7r.htm](abomination-vaults-bestiary-items/8QZHl09gya9bBM7r.htm)|Darkvision|Vision dans le noir|officielle|
|[8rL4XxfF1Fb3WMBG.htm](abomination-vaults-bestiary-items/8rL4XxfF1Fb3WMBG.htm)|Occult Innate Spells|Sorts occultes innés|officielle|
|[8SE9FJ5v5zXgilOT.htm](abomination-vaults-bestiary-items/8SE9FJ5v5zXgilOT.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[8vdtmNasjIJH8Bi3.htm](abomination-vaults-bestiary-items/8vdtmNasjIJH8Bi3.htm)|Light Hammer|Marteau de guerre léger|officielle|
|[8xM2M4RGdKhM7ex3.htm](abomination-vaults-bestiary-items/8xM2M4RGdKhM7ex3.htm)|Divine Innate Spells|Sorts divins innés|officielle|
|[8z4D5pllZhfFA85D.htm](abomination-vaults-bestiary-items/8z4D5pllZhfFA85D.htm)|Stench Suppression|Suppression de puanteur|officielle|
|[8zJvcrLeqTjH3Eup.htm](abomination-vaults-bestiary-items/8zJvcrLeqTjH3Eup.htm)|Primal Spontaneous Spells|Sorts primordiaux spontanés|officielle|
|[90JhVU3ps4TG9jNw.htm](abomination-vaults-bestiary-items/90JhVU3ps4TG9jNw.htm)|Faerie Fire (At Will)|Lueurs féeriques (À volonté)|officielle|
|[94npIYAEHcak0YRt.htm](abomination-vaults-bestiary-items/94npIYAEHcak0YRt.htm)|+1 Striking Kukri|+1,striking|Kukri de frappe +1|officielle|
|[94yfc2eSiwRZfq49.htm](abomination-vaults-bestiary-items/94yfc2eSiwRZfq49.htm)|Needle|Aiguille|officielle|
|[9C4djpvjI0fNA6Fa.htm](abomination-vaults-bestiary-items/9C4djpvjI0fNA6Fa.htm)|Darkvision|Vision dans le noir|officielle|
|[9DkYcHRtL1rgB4Ob.htm](abomination-vaults-bestiary-items/9DkYcHRtL1rgB4Ob.htm)|Constrict|Constriction|officielle|
|[9Eh0XQSnJ9rPgQ3d.htm](abomination-vaults-bestiary-items/9Eh0XQSnJ9rPgQ3d.htm)|Push|Bousculade|officielle|
|[9i3UjVYq4lp3MqZJ.htm](abomination-vaults-bestiary-items/9i3UjVYq4lp3MqZJ.htm)|Burning Lash|Fouet brûlant|officielle|
|[9J2GBQp8uuYCXtLo.htm](abomination-vaults-bestiary-items/9J2GBQp8uuYCXtLo.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[9xQRUNNxUssnoy4j.htm](abomination-vaults-bestiary-items/9xQRUNNxUssnoy4j.htm)|Chameleon Skin|Peau de caméléon|officielle|
|[9yk5MBYML8Z0OmtD.htm](abomination-vaults-bestiary-items/9yk5MBYML8Z0OmtD.htm)|Telepathy (with spectral thralls only)|Télépathie (avec les esclaves spectraux uniquement)|officielle|
|[a08Gf29QPITHX7mo.htm](abomination-vaults-bestiary-items/a08Gf29QPITHX7mo.htm)|Scriptorium Lore|Connaissance des scriptoriums|officielle|
|[a4lUBuJPU86WP0Gq.htm](abomination-vaults-bestiary-items/a4lUBuJPU86WP0Gq.htm)|Mindfog Aura|Aura de brume mentale|officielle|
|[a4vxarsPYtIYy5Br.htm](abomination-vaults-bestiary-items/a4vxarsPYtIYy5Br.htm)|At-Will Spells|Sorts à volonté|officielle|
|[A5aJvo6ma9oQG3Hh.htm](abomination-vaults-bestiary-items/A5aJvo6ma9oQG3Hh.htm)|Dimension Door (At Will)|Porte dimensionnelle (À volonté)|officielle|
|[a6xLeWb5L726R1lN.htm](abomination-vaults-bestiary-items/a6xLeWb5L726R1lN.htm)|Push|Bousculade|officielle|
|[ACtqtCeexz93EgK9.htm](abomination-vaults-bestiary-items/ACtqtCeexz93EgK9.htm)|Light Blindness|Aveuglé par la lumière|officielle|
|[AGshXsiKjGrOkqWk.htm](abomination-vaults-bestiary-items/AGshXsiKjGrOkqWk.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|officielle|
|[AHdJDOIrRS3IZSrc.htm](abomination-vaults-bestiary-items/AHdJDOIrRS3IZSrc.htm)|Greater Darkvision|Vision dans le noir supérieure|officielle|
|[aK6ekR5eDVtsBkFZ.htm](abomination-vaults-bestiary-items/aK6ekR5eDVtsBkFZ.htm)|Occult Attack|Attaque occulte|officielle|
|[AkS8oNJyeHZyFiyP.htm](abomination-vaults-bestiary-items/AkS8oNJyeHZyFiyP.htm)|Pitfall|Chute dans la fosse|officielle|
|[alpHOBrHeF3AD5cW.htm](abomination-vaults-bestiary-items/alpHOBrHeF3AD5cW.htm)|At-Will Spells|Sorts à volonté|officielle|
|[AO9HakVAxS0tqRrU.htm](abomination-vaults-bestiary-items/AO9HakVAxS0tqRrU.htm)|Fragment of Urevian's Pendant|Fragment du pendentif d'Urévian|officielle|
|[aSbHK3DUVAsb8eW6.htm](abomination-vaults-bestiary-items/aSbHK3DUVAsb8eW6.htm)|Jaws|Mâchoires|officielle|
|[aUiw4yrSZnbmjXeR.htm](abomination-vaults-bestiary-items/aUiw4yrSZnbmjXeR.htm)|Sneak Attack|Attaque sournoise|officielle|
|[aV7tH63nZZOcXqzb.htm](abomination-vaults-bestiary-items/aV7tH63nZZOcXqzb.htm)|Tongues (Constant)|Don des langues (constant)|officielle|
|[AyfJyi6gCHBgaToD.htm](abomination-vaults-bestiary-items/AyfJyi6gCHBgaToD.htm)|Darkvision|Vision dans le noir|officielle|
|[ayqWkUKHp3vU6wiY.htm](abomination-vaults-bestiary-items/ayqWkUKHp3vU6wiY.htm)|Blood Offering|Offrande de sang|officielle|
|[azGXpMTZu0Ft4DaJ.htm](abomination-vaults-bestiary-items/azGXpMTZu0Ft4DaJ.htm)|Horn|Corne|officielle|
|[b6BZWa1ISKLflSTs.htm](abomination-vaults-bestiary-items/b6BZWa1ISKLflSTs.htm)|Light Blindness|Aveuglé par la lumière|officielle|
|[b6FIdRGaxea1KX6k.htm](abomination-vaults-bestiary-items/b6FIdRGaxea1KX6k.htm)|Claw|Griffes|officielle|
|[B8jNXhGz4H4CYK3k.htm](abomination-vaults-bestiary-items/B8jNXhGz4H4CYK3k.htm)|Negative Healing|Guérison négative|officielle|
|[BBdrCuAIiJoPRLOJ.htm](abomination-vaults-bestiary-items/BBdrCuAIiJoPRLOJ.htm)|Breath of the Bog|Souffle de la tourbière|officielle|
|[BCdQbDHjNRy8IfGp.htm](abomination-vaults-bestiary-items/BCdQbDHjNRy8IfGp.htm)|Unwilling Teleportation|Téléportation forcée|officielle|
|[BEAgqEuzuu4pSB1t.htm](abomination-vaults-bestiary-items/BEAgqEuzuu4pSB1t.htm)|Infused Mistform Elixir (Moderate)|Élixir de brumeforme modérée imprégnée|officielle|
|[BeNcnupTKc7WS9e5.htm](abomination-vaults-bestiary-items/BeNcnupTKc7WS9e5.htm)|Dance Moves|Pas de danse|officielle|
|[BeVACGeSdq4dZpvw.htm](abomination-vaults-bestiary-items/BeVACGeSdq4dZpvw.htm)|Darkness (At Will)|Ténèbres (À volonté)|officielle|
|[BFiMEADLB2Qw0lL1.htm](abomination-vaults-bestiary-items/BFiMEADLB2Qw0lL1.htm)|Spike|Pointe|officielle|
|[BhqjfA3rcAJRma6F.htm](abomination-vaults-bestiary-items/BhqjfA3rcAJRma6F.htm)|Claw|Griffes|officielle|
|[bIhvOKLmWJLAIfDK.htm](abomination-vaults-bestiary-items/bIhvOKLmWJLAIfDK.htm)|Immobilizing Blow|Coup immobilisant|officielle|
|[BkBEhqLM3mXNI13M.htm](abomination-vaults-bestiary-items/BkBEhqLM3mXNI13M.htm)|Dimension Door (At Will)|Porte dimensionnelle (À volonté)|officielle|
|[BkxqdQTCmF312Eby.htm](abomination-vaults-bestiary-items/BkxqdQTCmF312Eby.htm)|At-Will Spells|Sorts à volonté|officielle|
|[BldzzBvbyFPySPXc.htm](abomination-vaults-bestiary-items/BldzzBvbyFPySPXc.htm)|Occult Innate Spells|Sorts occultes innés|officielle|
|[bo6bsp0fKrJ9ddNK.htm](abomination-vaults-bestiary-items/bo6bsp0fKrJ9ddNK.htm)|Stealth|Discrétion|officielle|
|[boE5h9E3qlCaVd4L.htm](abomination-vaults-bestiary-items/boE5h9E3qlCaVd4L.htm)|Darkvision|Vision dans le noir|officielle|
|[BPJS4QzIXUC3Zmwf.htm](abomination-vaults-bestiary-items/BPJS4QzIXUC3Zmwf.htm)|Frightful Presence|Présence terrifiante|officielle|
|[BPvAPhpd3Nzs8pvv.htm](abomination-vaults-bestiary-items/BPvAPhpd3Nzs8pvv.htm)|Needle Spray|Nuage d'aiguilles|officielle|
|[buzf372gE5wTjjeH.htm](abomination-vaults-bestiary-items/buzf372gE5wTjjeH.htm)|Fist|Poing|officielle|
|[BWdMcnFnD4W0LZpu.htm](abomination-vaults-bestiary-items/BWdMcnFnD4W0LZpu.htm)|Athletics|Athlétisme|officielle|
|[BZL7n09A5aQfGHjz.htm](abomination-vaults-bestiary-items/BZL7n09A5aQfGHjz.htm)|Occult Innate Spells|Sorts occultes innés|officielle|
|[c0PlxHB3aGqwm9qD.htm](abomination-vaults-bestiary-items/c0PlxHB3aGqwm9qD.htm)|Faerie Fire (At Will)|Lueurs féeriques (À volonté)|officielle|
|[C1gCdCYUAoqjpkXr.htm](abomination-vaults-bestiary-items/C1gCdCYUAoqjpkXr.htm)|Stealth|Discrétion|officielle|
|[C2q1zvPuwqgQMvwX.htm](abomination-vaults-bestiary-items/C2q1zvPuwqgQMvwX.htm)|Change Shape|Changement de forme|officielle|
|[C2yMhpgqYU1HM1NR.htm](abomination-vaults-bestiary-items/C2yMhpgqYU1HM1NR.htm)|Claw|Griffes|officielle|
|[C46qe4d4dQ5DqKp4.htm](abomination-vaults-bestiary-items/C46qe4d4dQ5DqKp4.htm)|Vile Blowgun|Vile sarbacane|officielle|
|[c8DR2M4wTFOhiB7j.htm](abomination-vaults-bestiary-items/c8DR2M4wTFOhiB7j.htm)|Fast Healing 7|Guérison accélérée 7|officielle|
|[c9PJ35ELMYksO3fR.htm](abomination-vaults-bestiary-items/c9PJ35ELMYksO3fR.htm)|Faerie Fire (at will)|Lueurs féeriques (À volonté)|officielle|
|[CAWJlqwSDN3w5gWy.htm](abomination-vaults-bestiary-items/CAWJlqwSDN3w5gWy.htm)|Overpowering Jaws|Mâchoires surpuissantes|officielle|
|[cb5fpVrLbNcM4VUi.htm](abomination-vaults-bestiary-items/cb5fpVrLbNcM4VUi.htm)|Suppress Aura|Réprimer l’aura|officielle|
|[CcTlJ52vSeJqkx5b.htm](abomination-vaults-bestiary-items/CcTlJ52vSeJqkx5b.htm)|Light Blindness|Aveuglé par la lumière|officielle|
|[cdTaJad7jDM2QE73.htm](abomination-vaults-bestiary-items/cdTaJad7jDM2QE73.htm)|Wavesense (Imprecise) 60 feet|Perception des ondes 18 m (imprécis)|officielle|
|[cfM8s4VVlCf81rzl.htm](abomination-vaults-bestiary-items/cfM8s4VVlCf81rzl.htm)|Repeating Hand Crossbow|Arbalète de poing à répétition|officielle|
|[CH1wifIGckLBQDfe.htm](abomination-vaults-bestiary-items/CH1wifIGckLBQDfe.htm)|All-Around Vision|Vision panoramique|officielle|
|[ch9nYPl6I99TJLu9.htm](abomination-vaults-bestiary-items/ch9nYPl6I99TJLu9.htm)|Swarming Stance|Posture de nuée|officielle|
|[CH9sJBUN2HtEAgtt.htm](abomination-vaults-bestiary-items/CH9sJBUN2HtEAgtt.htm)|Vocal Warm-Up|Vocalises|officielle|
|[Ci03R6SeE2d256my.htm](abomination-vaults-bestiary-items/Ci03R6SeE2d256my.htm)|Aquatic Ambush|Embuscade aquatique|officielle|
|[cj3wll0YeOYjYWaL.htm](abomination-vaults-bestiary-items/cj3wll0YeOYjYWaL.htm)|Tremorsense (Imprecise) 30 feet|Perception des vibrations 9 m (imprécis)|officielle|
|[cKCHCzZNwEgF3rgQ.htm](abomination-vaults-bestiary-items/cKCHCzZNwEgF3rgQ.htm)|Negative Healing|Guérison négative|officielle|
|[ckEhVhjZnjMqhSjZ.htm](abomination-vaults-bestiary-items/ckEhVhjZnjMqhSjZ.htm)|Shootist's Draw|Dégainer comme un arbalétrier d’élite|officielle|
|[CL3iEdGmI7RDCvqu.htm](abomination-vaults-bestiary-items/CL3iEdGmI7RDCvqu.htm)|Divine Innate Spells|Sorts divins innés|officielle|
|[CncVGQus926xFSl2.htm](abomination-vaults-bestiary-items/CncVGQus926xFSl2.htm)|Shauth Bite|Morsure shauthe|officielle|
|[cNM2OqWLz3u5w7ai.htm](abomination-vaults-bestiary-items/cNM2OqWLz3u5w7ai.htm)|Wand of Gentle Repose (Level 2)|Baguette de préservation des morts (Niveau 2)|officielle|
|[coOSYslrfzzpX9Qf.htm](abomination-vaults-bestiary-items/coOSYslrfzzpX9Qf.htm)|Darkvision|Vision dans le noir|officielle|
|[COyeCB6tCf47WbAu.htm](abomination-vaults-bestiary-items/COyeCB6tCf47WbAu.htm)|Jaws|Mâchoires|officielle|
|[CpXxvpsrf7rcrJM4.htm](abomination-vaults-bestiary-items/CpXxvpsrf7rcrJM4.htm)|Darkvision|Vision dans le noir|officielle|
|[Cs1TEn4HJrImL292.htm](abomination-vaults-bestiary-items/Cs1TEn4HJrImL292.htm)|War Flail|+1,striking|Fléau de guerre de frappe +1|officielle|
|[Cth0VtGbtTcGpsAR.htm](abomination-vaults-bestiary-items/Cth0VtGbtTcGpsAR.htm)|Red Claw|Griffe rouge|officielle|
|[CtYG2Idf6Es4FDCt.htm](abomination-vaults-bestiary-items/CtYG2Idf6Es4FDCt.htm)|+2 Status to All Saves vs. Mental|+2 de statut aux JdS contre mental|officielle|
|[cU4WaqnerXGtIpEM.htm](abomination-vaults-bestiary-items/cU4WaqnerXGtIpEM.htm)|Jaws|Mâchoires|officielle|
|[cu7cx46lq0JVi00X.htm](abomination-vaults-bestiary-items/cu7cx46lq0JVi00X.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|officielle|
|[cxiSU1lrmfq49Cag.htm](abomination-vaults-bestiary-items/cxiSU1lrmfq49Cag.htm)|Grab|Empoignade|officielle|
|[cY5sdUKAQ65ORaUe.htm](abomination-vaults-bestiary-items/cY5sdUKAQ65ORaUe.htm)|Crafting|Artisanat|officielle|
|[cZSuVe7TToENdQod.htm](abomination-vaults-bestiary-items/cZSuVe7TToENdQod.htm)|Spore Jet|Jet de spores|officielle|
|[D0n53VOiJk5g1YnP.htm](abomination-vaults-bestiary-items/D0n53VOiJk5g1YnP.htm)|Necrotic Bomb|Bombe nécrotique|officielle|
|[d4M2oXHQpDvC4Crx.htm](abomination-vaults-bestiary-items/d4M2oXHQpDvC4Crx.htm)|Darkness (At Will)|Ténèbres (À volonté)|officielle|
|[D6oLgwNpejO3m2LR.htm](abomination-vaults-bestiary-items/D6oLgwNpejO3m2LR.htm)|Darkvision|Vision dans le noir|officielle|
|[D8o83tvKi46maS3U.htm](abomination-vaults-bestiary-items/D8o83tvKi46maS3U.htm)|Sneak Attack|Attaque sournoise|officielle|
|[d9oLx0w5SZt7KSEv.htm](abomination-vaults-bestiary-items/d9oLx0w5SZt7KSEv.htm)|Divine Innate Spells|Sorts divins innés|officielle|
|[dBpRjWleNemuJ0CH.htm](abomination-vaults-bestiary-items/dBpRjWleNemuJ0CH.htm)|Occult Innate Spells|Sorts occultes innés|officielle|
|[dD1t8c61JZZMf4Qo.htm](abomination-vaults-bestiary-items/dD1t8c61JZZMf4Qo.htm)|Scrying (At Will) (See Right of Inspection)|Scrutation (à volonté, voir Droit de regard)|officielle|
|[dd53Mcobz1fpxVhN.htm](abomination-vaults-bestiary-items/dd53Mcobz1fpxVhN.htm)|Claw|Griffes|officielle|
|[DejWulVQHxSP4SbB.htm](abomination-vaults-bestiary-items/DejWulVQHxSP4SbB.htm)|Astrology Lore|Connaissance en astrologie|officielle|
|[deTqYUQHSSEOChwZ.htm](abomination-vaults-bestiary-items/deTqYUQHSSEOChwZ.htm)|Tentacle|Tentacule|officielle|
|[DfDsOQ6I6MwVdnPd.htm](abomination-vaults-bestiary-items/DfDsOQ6I6MwVdnPd.htm)|Fleshy Slap|Claque charnue|officielle|
|[dFe2hAb3rrhlAVsx.htm](abomination-vaults-bestiary-items/dFe2hAb3rrhlAVsx.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|officielle|
|[dFO4VUU2D6u32L0j.htm](abomination-vaults-bestiary-items/dFO4VUU2D6u32L0j.htm)|Wicked Bite|Méchante morsure|officielle|
|[dFwCuZjzgm11pQbh.htm](abomination-vaults-bestiary-items/dFwCuZjzgm11pQbh.htm)|Nyzuros's Keys|Clés de Nyzuro|libre|
|[DGCZumwAduW04eMJ.htm](abomination-vaults-bestiary-items/DGCZumwAduW04eMJ.htm)|Darkvision|Vision dans le noir|officielle|
|[DHJskQgx4Ucg690b.htm](abomination-vaults-bestiary-items/DHJskQgx4Ucg690b.htm)|Darkvision|Vision dans le noir|officielle|
|[dHv4zBnoFd4KSWBz.htm](abomination-vaults-bestiary-items/dHv4zBnoFd4KSWBz.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|officielle|
|[DI73oBXVmI2VUsnm.htm](abomination-vaults-bestiary-items/DI73oBXVmI2VUsnm.htm)|Light Blindness|Aveuglé par la lumière|officielle|
|[DIqlApNIdzR6Eh6H.htm](abomination-vaults-bestiary-items/DIqlApNIdzR6Eh6H.htm)|Hunter's Wound|Blessure du chasseur|officielle|
|[dJQlNu9f34qL4mEx.htm](abomination-vaults-bestiary-items/dJQlNu9f34qL4mEx.htm)|Tremorsense (Imprecise) 30 feet|Perception des vibrations 9 m (imprécis)|officielle|
|[DKikA8jlUkgQxScq.htm](abomination-vaults-bestiary-items/DKikA8jlUkgQxScq.htm)|Constant Spells|Sorts constants|officielle|
|[dl0aE5MkOn0AmMJs.htm](abomination-vaults-bestiary-items/dl0aE5MkOn0AmMJs.htm)|Jaws|Mâchoires|officielle|
|[DLzdlzxfprHMmnS5.htm](abomination-vaults-bestiary-items/DLzdlzxfprHMmnS5.htm)|Greater Darkvision|Vision dans le noir supérieure|officielle|
|[dNigd85c2ZydQi1Z.htm](abomination-vaults-bestiary-items/dNigd85c2ZydQi1Z.htm)|Swarm Mind|Esprit de la nuée|officielle|
|[DriJGrzzm1Qq0NNX.htm](abomination-vaults-bestiary-items/DriJGrzzm1Qq0NNX.htm)|Light Blindness|Aveuglé par la lumière|officielle|
|[dsBZk8GZYws5VlE1.htm](abomination-vaults-bestiary-items/dsBZk8GZYws5VlE1.htm)|Claustrophobia|Claustrophobie|officielle|
|[DSrUNlWJjUN1fnWL.htm](abomination-vaults-bestiary-items/DSrUNlWJjUN1fnWL.htm)|Poisoned Breath|Souffle empoisonné|officielle|
|[DSW55AZz6cwRj1rc.htm](abomination-vaults-bestiary-items/DSW55AZz6cwRj1rc.htm)|Jaws|Mâchoires|officielle|
|[dTlqxVR7MGmNsHHm.htm](abomination-vaults-bestiary-items/dTlqxVR7MGmNsHHm.htm)|Kukri|Kukri|officielle|
|[dwzqPqd8eTnASsxZ.htm](abomination-vaults-bestiary-items/dwzqPqd8eTnASsxZ.htm)|Stone Defense|Défense de pierre|officielle|
|[DYoEwMCwfSHk2Og2.htm](abomination-vaults-bestiary-items/DYoEwMCwfSHk2Og2.htm)|Kukri|Kukri|officielle|
|[DZ1joQ2LpXlkz2Jm.htm](abomination-vaults-bestiary-items/DZ1joQ2LpXlkz2Jm.htm)|Dagger|Dague|officielle|
|[E1j9pEdjLX1NuzdV.htm](abomination-vaults-bestiary-items/E1j9pEdjLX1NuzdV.htm)|Staff|Bâton|officielle|
|[e2TItLJ7Ia5jTsir.htm](abomination-vaults-bestiary-items/e2TItLJ7Ia5jTsir.htm)|+2 Status To All Saves vs. Mental|+2 de statut aux JdS contre mental|officielle|
|[E952Mn29Vyncndfi.htm](abomination-vaults-bestiary-items/E952Mn29Vyncndfi.htm)|Katar|+1,striking|Katar de frappe +1|officielle|
|[eAK76xdqlZiTDlXb.htm](abomination-vaults-bestiary-items/eAK76xdqlZiTDlXb.htm)|Jafaki's Four-pronged Key|Clé de Jafaki à quatre volets|libre|
|[eAyilI89Ya4AzPjX.htm](abomination-vaults-bestiary-items/eAyilI89Ya4AzPjX.htm)|Bounce|Rebond|officielle|
|[Eb8EhrTUrXxsrLz3.htm](abomination-vaults-bestiary-items/Eb8EhrTUrXxsrLz3.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|officielle|
|[EBgiFSyYf0W5Fxcr.htm](abomination-vaults-bestiary-items/EBgiFSyYf0W5Fxcr.htm)|Jaws|Mâchoires|officielle|
|[ediMsqeJ8TKtfzcR.htm](abomination-vaults-bestiary-items/ediMsqeJ8TKtfzcR.htm)|Darkness (At Will)|Ténèbres (À volonté)|officielle|
|[EDPFYDhj0ZOTpRmX.htm](abomination-vaults-bestiary-items/EDPFYDhj0ZOTpRmX.htm)|Animal Order Spells|Sorts de l'ordre animal|officielle|
|[eDZZSOhzP0LwbDLp.htm](abomination-vaults-bestiary-items/eDZZSOhzP0LwbDLp.htm)|Fragment of Urevian's Pendant|Fragment du pendentif d'Urévian|officielle|
|[eebzEKEUQ1IX3JhM.htm](abomination-vaults-bestiary-items/eebzEKEUQ1IX3JhM.htm)|Bottled Lightning|Foudre en bouteille|officielle|
|[EEwIe3zMikRnRWg6.htm](abomination-vaults-bestiary-items/EEwIe3zMikRnRWg6.htm)|Warp Reality|Distorsion de la réalité|officielle|
|[eGvWOYTY1N6i6QpT.htm](abomination-vaults-bestiary-items/eGvWOYTY1N6i6QpT.htm)|Blood Drain|Absorption de sang|officielle|
|[eIBT42KNs5UfO41v.htm](abomination-vaults-bestiary-items/eIBT42KNs5UfO41v.htm)|Darkvision|Vision dans le noir|officielle|
|[EiWEO80FhEG270MQ.htm](abomination-vaults-bestiary-items/EiWEO80FhEG270MQ.htm)|Paralysis|Paralysie|officielle|
|[Elphpw5BdTHWoPps.htm](abomination-vaults-bestiary-items/Elphpw5BdTHWoPps.htm)|Occult Innate Spells|Sorts occultes innés|officielle|
|[eltHO3xRIKJCfFcF.htm](abomination-vaults-bestiary-items/eltHO3xRIKJCfFcF.htm)|Claw|Griffes|officielle|
|[eMT2uTNjSReRvfV8.htm](abomination-vaults-bestiary-items/eMT2uTNjSReRvfV8.htm)|Sneak Attack|Attaque sournoise|officielle|
|[eP5tBspqEqP18oTY.htm](abomination-vaults-bestiary-items/eP5tBspqEqP18oTY.htm)|Site Bound|Lié à un site|officielle|
|[eRq3BYWv8B8rLpXL.htm](abomination-vaults-bestiary-items/eRq3BYWv8B8rLpXL.htm)|Shadow Flitter|Vol de l’ombre|officielle|
|[ErwsWW6oip6zma6i.htm](abomination-vaults-bestiary-items/ErwsWW6oip6zma6i.htm)|Shauth Seize|Saisie shauthe|officielle|
|[ESemIP7QRiArXvT4.htm](abomination-vaults-bestiary-items/ESemIP7QRiArXvT4.htm)|Darkvision|Vision dans le noir|officielle|
|[EvzzltzygJO4ess9.htm](abomination-vaults-bestiary-items/EvzzltzygJO4ess9.htm)|Negative Healing|Guérison négative|officielle|
|[ExTn8RH5Xp1BVvJk.htm](abomination-vaults-bestiary-items/ExTn8RH5Xp1BVvJk.htm)|Illusory Disguise (At Will)|Déguisement illusoire (À volonté)|officielle|
|[EXyt7FC2RKPKjWCX.htm](abomination-vaults-bestiary-items/EXyt7FC2RKPKjWCX.htm)|Despair|Désespoir|officielle|
|[EyBIlDKJTvz0g8ka.htm](abomination-vaults-bestiary-items/EyBIlDKJTvz0g8ka.htm)|Echoes of Defeat|Échos de la défaite|officielle|
|[eZxgYfIRD41tf3F9.htm](abomination-vaults-bestiary-items/eZxgYfIRD41tf3F9.htm)|Radiant Ray|Rayon radieux|officielle|
|[F1MZjO36rRuFQXYR.htm](abomination-vaults-bestiary-items/F1MZjO36rRuFQXYR.htm)|Envenom Weapon|Envenimer une arme|officielle|
|[f2TrT4l8m1DRgTZ4.htm](abomination-vaults-bestiary-items/f2TrT4l8m1DRgTZ4.htm)|Darkvision|Vision dans le noir|officielle|
|[f50ST8T8gY3pQJfx.htm](abomination-vaults-bestiary-items/f50ST8T8gY3pQJfx.htm)|Evasion|Évasion|officielle|
|[f7rkC7qodqH9NbVI.htm](abomination-vaults-bestiary-items/f7rkC7qodqH9NbVI.htm)|Tremorsense (Imprecise) 30 feet|Perception des vibrations 9 m (imprécis)|officielle|
|[F7zqmFxLH2v3hOAA.htm](abomination-vaults-bestiary-items/F7zqmFxLH2v3hOAA.htm)|Telepathy 100 feet|Télépathie 30 m|officielle|
|[F8VzPOjdtS2Gmx87.htm](abomination-vaults-bestiary-items/F8VzPOjdtS2Gmx87.htm)|Mind Feeding|Manger les esprits|officielle|
|[Fap5Oo5cGmdLjzes.htm](abomination-vaults-bestiary-items/Fap5Oo5cGmdLjzes.htm)|Reloading Trick|Recharger en un tour de main|officielle|
|[faxsM9HLLEPrf5fu.htm](abomination-vaults-bestiary-items/faxsM9HLLEPrf5fu.htm)|Wand of Magic Missile (Level 3)|Baguette de Projectile magique (niveau 3)|officielle|
|[fBrZMabfv55LX5bY.htm](abomination-vaults-bestiary-items/fBrZMabfv55LX5bY.htm)|Show the Looming Moon|Éclat de la lune montante|officielle|
|[fdAMCESh7OW3fKWY.htm](abomination-vaults-bestiary-items/fdAMCESh7OW3fKWY.htm)|Consume Confusion|Engloutir la confu|officielle|
|[fDft2vRYcFM3JXBe.htm](abomination-vaults-bestiary-items/fDft2vRYcFM3JXBe.htm)|Intense Performer|Artiste intense|officielle|
|[FgeH6g4PelOU0Npx.htm](abomination-vaults-bestiary-items/FgeH6g4PelOU0Npx.htm)|Tamchal Chakram|+1|Chakram Tamchal +1|officielle|
|[fGmd67WxTzSxDIpe.htm](abomination-vaults-bestiary-items/fGmd67WxTzSxDIpe.htm)|Tendril|Vrille|officielle|
|[fJKkYX781x7u24J6.htm](abomination-vaults-bestiary-items/fJKkYX781x7u24J6.htm)|Mud and Bug Crown|Couronne de boue et d'insectes|officielle|
|[fL540WJoPfl2yEVm.htm](abomination-vaults-bestiary-items/fL540WJoPfl2yEVm.htm)|Darkness (at will)|Ténèbres (À volonté)|officielle|
|[FLe1chjATs0hNKPw.htm](abomination-vaults-bestiary-items/FLe1chjATs0hNKPw.htm)|Explosive Decay|Dégradation explosive|officielle|
|[fMZfXQMCUD23X5H9.htm](abomination-vaults-bestiary-items/fMZfXQMCUD23X5H9.htm)|Greater Darkvision|Vision dans le noir supérieure|officielle|
|[fNQ2aHpM6vTQ7iqr.htm](abomination-vaults-bestiary-items/fNQ2aHpM6vTQ7iqr.htm)|Command Confusion|Injonction aux confus|officielle|
|[fpyaGQ7nPlsCH1nc.htm](abomination-vaults-bestiary-items/fpyaGQ7nPlsCH1nc.htm)|True Seeing (Constant)|Vision lucide (constant)|officielle|
|[FsHTfpYkyQ2CCGzZ.htm](abomination-vaults-bestiary-items/FsHTfpYkyQ2CCGzZ.htm)|Light Blindness|Aveuglé par la lumière|officielle|
|[FteyjzEwACt18LfC.htm](abomination-vaults-bestiary-items/FteyjzEwACt18LfC.htm)|Gnawing Fog|Brouillard rongeant|officielle|
|[fueyaPehWakc93nh.htm](abomination-vaults-bestiary-items/fueyaPehWakc93nh.htm)|Swarming Stance|Posture de nuée|officielle|
|[FZlSN0F2oSrYwSf4.htm](abomination-vaults-bestiary-items/FZlSN0F2oSrYwSf4.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[G0WFBGJRBnL3fapV.htm](abomination-vaults-bestiary-items/G0WFBGJRBnL3fapV.htm)|Composite Shortbow|+1|Arc court composite +1|officielle|
|[g2QS3ryiZhgirGmj.htm](abomination-vaults-bestiary-items/g2QS3ryiZhgirGmj.htm)|Bog Rot|Lèpre des marais|officielle|
|[G3bgPlOpXpztjTdf.htm](abomination-vaults-bestiary-items/G3bgPlOpXpztjTdf.htm)|Katar|Katar|officielle|
|[G4UvThy3GwXSiOIo.htm](abomination-vaults-bestiary-items/G4UvThy3GwXSiOIo.htm)|Low-Light Vision|Vision nocturne|officielle|
|[GA3sOIQc3gtd1dyN.htm](abomination-vaults-bestiary-items/GA3sOIQc3gtd1dyN.htm)|Shadow Jump|Saut de l’ombre|officielle|
|[gAui54rDueAP4nNh.htm](abomination-vaults-bestiary-items/gAui54rDueAP4nNh.htm)|Sneak Attack|Attaque sournoise|officielle|
|[GDIw4d1nppVwAcDM.htm](abomination-vaults-bestiary-items/GDIw4d1nppVwAcDM.htm)|Blood Magic|Magie du sang|officielle|
|[gdWSFRbbf21JhOa9.htm](abomination-vaults-bestiary-items/gdWSFRbbf21JhOa9.htm)|Ectoplasmic Web Trap|Piège de toile ectoplasmique|officielle|
|[gHMs8jNdIsdBy2OZ.htm](abomination-vaults-bestiary-items/gHMs8jNdIsdBy2OZ.htm)|Shock|Foudre|officielle|
|[gkIxUkhYZg4oHj1t.htm](abomination-vaults-bestiary-items/gkIxUkhYZg4oHj1t.htm)|+2 Status to All Saves vs. Bleed, Death Effects, Disease, Doomed, Fatigued, Paralyzed, Poison, Sickened|+2 de statut aux JdS contre le saignement, effets de mort, maladie, condamné, fatigué, paralysé, poison et malade|officielle|
|[gl7lB276y1xS70s3.htm](abomination-vaults-bestiary-items/gl7lB276y1xS70s3.htm)|Jaul Coordination|Coordination avec Jaul|officielle|
|[GMWiXA8zpcvJD4Fp.htm](abomination-vaults-bestiary-items/GMWiXA8zpcvJD4Fp.htm)|Darkness (At Will)|Ténèbres (À volonté)|officielle|
|[GOUc826yPXfyrWNW.htm](abomination-vaults-bestiary-items/GOUc826yPXfyrWNW.htm)|Shootist's Luck|Chance de l’arbalétrier d’élite|officielle|
|[GpoVeh1bdg8JH2SG.htm](abomination-vaults-bestiary-items/GpoVeh1bdg8JH2SG.htm)|Deny Advantage|Refus d'avantage|officielle|
|[GqaPr3pgEY5WHWQc.htm](abomination-vaults-bestiary-items/GqaPr3pgEY5WHWQc.htm)|Athletics|Athlétisme|officielle|
|[GsXsiIe2IQXx1C8I.htm](abomination-vaults-bestiary-items/GsXsiIe2IQXx1C8I.htm)|At-Will Spells|Sorts à volonté|officielle|
|[gTviRIemgZXPfEW1.htm](abomination-vaults-bestiary-items/gTviRIemgZXPfEW1.htm)|Draining Venom|Venin drainant|officielle|
|[gyyRt5jyTNORzIpr.htm](abomination-vaults-bestiary-items/gyyRt5jyTNORzIpr.htm)|Defensive Needle|Aiguille défensive|officielle|
|[gZDMpjUjAgRTjrcL.htm](abomination-vaults-bestiary-items/gZDMpjUjAgRTjrcL.htm)|Spike|Pointe|officielle|
|[GzMhkd0CBpUeUX63.htm](abomination-vaults-bestiary-items/GzMhkd0CBpUeUX63.htm)|Projectile Launcher|Lance-projectile|officielle|
|[h0A9RTrbTZbrFUe8.htm](abomination-vaults-bestiary-items/h0A9RTrbTZbrFUe8.htm)|Spirit Lore|Connaissance des esprits|officielle|
|[H34w70sTO05v4xsB.htm](abomination-vaults-bestiary-items/H34w70sTO05v4xsB.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[H3dPRQwlEWXQFPUe.htm](abomination-vaults-bestiary-items/H3dPRQwlEWXQFPUe.htm)|At-Will Spells|Sorts à volonté|officielle|
|[H3obBC81SMnIiDVI.htm](abomination-vaults-bestiary-items/H3obBC81SMnIiDVI.htm)|Rapier|+1,striking|Rapière de frappe +1|officielle|
|[H44u0Lp3c8QPlUiB.htm](abomination-vaults-bestiary-items/H44u0Lp3c8QPlUiB.htm)|Obscuring Mist (At Will)|Brume de dissimulation (À volonté)|officielle|
|[H61mcURmEhYdmt9L.htm](abomination-vaults-bestiary-items/H61mcURmEhYdmt9L.htm)|Ghoul Fever|Fièvre des goules|officielle|
|[H6sBL4aimiKQcdtJ.htm](abomination-vaults-bestiary-items/H6sBL4aimiKQcdtJ.htm)|Snout|Museau|officielle|
|[HcrsT0ltPpWRSZG1.htm](abomination-vaults-bestiary-items/HcrsT0ltPpWRSZG1.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|officielle|
|[hfZmDX2fsCvlGR4v.htm](abomination-vaults-bestiary-items/hfZmDX2fsCvlGR4v.htm)|Occult Innate Spells|Sorts occultes innés|officielle|
|[HHbU5fdUwrxiWVG3.htm](abomination-vaults-bestiary-items/HHbU5fdUwrxiWVG3.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|officielle|
|[hIYUGmOfZ6ZD3cNt.htm](abomination-vaults-bestiary-items/hIYUGmOfZ6ZD3cNt.htm)|Darkvision|Vision dans le noir|officielle|
|[Hl7cINV4KNf7cW0s.htm](abomination-vaults-bestiary-items/Hl7cINV4KNf7cW0s.htm)|Darkvision|Vision dans le noir|officielle|
|[HLLk7YCS6Ad1ibE5.htm](abomination-vaults-bestiary-items/HLLk7YCS6Ad1ibE5.htm)|Staff|Bâton|officielle|
|[hNe75I8nMGqBfaaP.htm](abomination-vaults-bestiary-items/hNe75I8nMGqBfaaP.htm)|Rise Up|Soulèvement|officielle|
|[hpMBDF24fDXi90pI.htm](abomination-vaults-bestiary-items/hpMBDF24fDXi90pI.htm)|Divine Innate Spells|Sorts divins innés|officielle|
|[hrgnNQ80aeUqTSge.htm](abomination-vaults-bestiary-items/hrgnNQ80aeUqTSge.htm)|Athletics|Athlétisme|officielle|
|[hSFAedu25PZDak7p.htm](abomination-vaults-bestiary-items/hSFAedu25PZDak7p.htm)|Stench|Puanteur|officielle|
|[HSgw6P66g1tETSK5.htm](abomination-vaults-bestiary-items/HSgw6P66g1tETSK5.htm)|Improvised Projectile|Projectile improvisé|officielle|
|[HSib85eXEGK4xoXf.htm](abomination-vaults-bestiary-items/HSib85eXEGK4xoXf.htm)|Diabolic Quill|Plume diabolique|officielle|
|[HtINDVVIov9yMEhV.htm](abomination-vaults-bestiary-items/HtINDVVIov9yMEhV.htm)|Site Bound|Lié à un site|officielle|
|[htwDT7obXSlNmWpU.htm](abomination-vaults-bestiary-items/htwDT7obXSlNmWpU.htm)|Longsword|Épée longue|officielle|
|[huUPSBc3tD6r0B4N.htm](abomination-vaults-bestiary-items/huUPSBc3tD6r0B4N.htm)|Darkvision|Vision dans le noir|officielle|
|[hvENSxQhO8lwlD1Y.htm](abomination-vaults-bestiary-items/hvENSxQhO8lwlD1Y.htm)|Commanding Aura|Aura de commandement|officielle|
|[hw9SLn99zC7mqeTR.htm](abomination-vaults-bestiary-items/hw9SLn99zC7mqeTR.htm)|Stupor Poison|Poison de stupeur|officielle|
|[hwoQ4oeElvvImKgQ.htm](abomination-vaults-bestiary-items/hwoQ4oeElvvImKgQ.htm)|Jaws|Mâchoires|officielle|
|[hwqvhAAeZ5lZGags.htm](abomination-vaults-bestiary-items/hwqvhAAeZ5lZGags.htm)|Landbound|Lié au sol|officielle|
|[HytGc8m6N3nhaxiO.htm](abomination-vaults-bestiary-items/HytGc8m6N3nhaxiO.htm)|Divine Innate Spells|Sorts divins innés|officielle|
|[hZ7GydaiKm8Egcxd.htm](abomination-vaults-bestiary-items/hZ7GydaiKm8Egcxd.htm)|Quickened Casting|Incantation accélérée|officielle|
|[I41NEY2RdcZSR1ft.htm](abomination-vaults-bestiary-items/I41NEY2RdcZSR1ft.htm)|Apocalypse Beam (from Undead Uprising)|Faisceau apocalyptique (de l'assaut des morts-vivants)|officielle|
|[i4Z53PbHjjlFshli.htm](abomination-vaults-bestiary-items/i4Z53PbHjjlFshli.htm)|Primal Prepared Spells|Sorts primordiaux préparés|officielle|
|[I4ZxiQRjcJ5rCCvp.htm](abomination-vaults-bestiary-items/I4ZxiQRjcJ5rCCvp.htm)|Magic Immunity|Immunité à la magie|officielle|
|[i68AZkuUsYHLKLWd.htm](abomination-vaults-bestiary-items/i68AZkuUsYHLKLWd.htm)|Jaws|Mâchoires|officielle|
|[i7f5BgAL5FJKJd7b.htm](abomination-vaults-bestiary-items/i7f5BgAL5FJKJd7b.htm)|Platinum Medal|Médaille de platine|officielle|
|[i7kJARfn6bo7YXTy.htm](abomination-vaults-bestiary-items/i7kJARfn6bo7YXTy.htm)|Staff|Bâton|officielle|
|[I7Zg9nDgRBezMT5l.htm](abomination-vaults-bestiary-items/I7Zg9nDgRBezMT5l.htm)|Darkvision|Vision dans le noir|officielle|
|[IcalXla4INxKnCPk.htm](abomination-vaults-bestiary-items/IcalXla4INxKnCPk.htm)|Occult Innate Spells|Sorts occultes innés|officielle|
|[IDg4tkvpzcQVtLok.htm](abomination-vaults-bestiary-items/IDg4tkvpzcQVtLok.htm)|Bloodline Spells|Sorts de lignage|officielle|
|[igyVJ2r1r0X9HNP8.htm](abomination-vaults-bestiary-items/igyVJ2r1r0X9HNP8.htm)|Moon Frenzy|Frénésie lunaire|officielle|
|[IHcY0GaB5rFEpIJT.htm](abomination-vaults-bestiary-items/IHcY0GaB5rFEpIJT.htm)|Site Bound|Lié à un site|officielle|
|[Iif4AgPk3mZlhi6z.htm](abomination-vaults-bestiary-items/Iif4AgPk3mZlhi6z.htm)|Library Lore|Connaissance des bibliothèques|officielle|
|[IiFLZ6cZ0dZBAcee.htm](abomination-vaults-bestiary-items/IiFLZ6cZ0dZBAcee.htm)|Squirming Embrace|Étreinte grouillante|officielle|
|[ijACdC2w6CsLO9Ks.htm](abomination-vaults-bestiary-items/ijACdC2w6CsLO9Ks.htm)|Telepathy 100 feet|Télépathie 30 m|officielle|
|[iJcL8jitqKxQqnx8.htm](abomination-vaults-bestiary-items/iJcL8jitqKxQqnx8.htm)|Grab|Empoignade|officielle|
|[IJTHmP61FLx42vVO.htm](abomination-vaults-bestiary-items/IJTHmP61FLx42vVO.htm)|Sneak Attack|Attaque sournoise|officielle|
|[ikaHblTttb4ck8mr.htm](abomination-vaults-bestiary-items/ikaHblTttb4ck8mr.htm)|Negative Healing|Guérison négative|officielle|
|[ikOLQArHJ3ox5p0j.htm](abomination-vaults-bestiary-items/ikOLQArHJ3ox5p0j.htm)|Light Blindness|Aveuglé par la lumière|officielle|
|[ilL5pHw5IAPVwhHU.htm](abomination-vaults-bestiary-items/ilL5pHw5IAPVwhHU.htm)|Darkvision|Vision dans le noir|officielle|
|[IM2z6ZQvAhXIgpsu.htm](abomination-vaults-bestiary-items/IM2z6ZQvAhXIgpsu.htm)|Confusing Confrontation|Confrontation déroutante|officielle|
|[IopEu45YRMrLuNzn.htm](abomination-vaults-bestiary-items/IopEu45YRMrLuNzn.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[iOvO0dFhneixzLvw.htm](abomination-vaults-bestiary-items/iOvO0dFhneixzLvw.htm)|+2 Status to All Saves vs. Mental|+2 de statut aux JdS contre mental|officielle|
|[IPH7UJLRufEnfg32.htm](abomination-vaults-bestiary-items/IPH7UJLRufEnfg32.htm)|Necrotic Decay|Décomposition nécrotique|officielle|
|[IR6YUO9Gdy6kxYo5.htm](abomination-vaults-bestiary-items/IR6YUO9Gdy6kxYo5.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|officielle|
|[Ir7VUc4vTn1TT3EX.htm](abomination-vaults-bestiary-items/Ir7VUc4vTn1TT3EX.htm)|100 feet of Erinys-Hair Rope|Corde en cheveux d'érinye (30 m)|officielle|
|[Irss8fPxNC91YnDe.htm](abomination-vaults-bestiary-items/Irss8fPxNC91YnDe.htm)|Greater Darkvision|Vision dans le noir supérieure|officielle|
|[IS350crGK3U3r6SG.htm](abomination-vaults-bestiary-items/IS350crGK3U3r6SG.htm)|Darkvision|Vision dans le noir|officielle|
|[IuwOAyVqNq1LTtkA.htm](abomination-vaults-bestiary-items/IuwOAyVqNq1LTtkA.htm)|Arcane Prepared Spells|Sorts arcaniques préparés|officielle|
|[IvhwAKjsxJV7jjkI.htm](abomination-vaults-bestiary-items/IvhwAKjsxJV7jjkI.htm)|Battle Axe|+1|Hache d'armes +1|officielle|
|[iWHE4a4vmetksWd7.htm](abomination-vaults-bestiary-items/iWHE4a4vmetksWd7.htm)|Aquatic Ambush|Embuscade aquatique|officielle|
|[iY5D7YKAlye71zpy.htm](abomination-vaults-bestiary-items/iY5D7YKAlye71zpy.htm)|Scythe|Faux|officielle|
|[iZFB4xCQhaumJQTC.htm](abomination-vaults-bestiary-items/iZFB4xCQhaumJQTC.htm)|Death Motes|Particules mortelles|officielle|
|[J0lZcblrLKOLNSZZ.htm](abomination-vaults-bestiary-items/J0lZcblrLKOLNSZZ.htm)|Occultism|Occultisme|officielle|
|[j0uUpSfzz62wPB0o.htm](abomination-vaults-bestiary-items/j0uUpSfzz62wPB0o.htm)|Web Trap|Piège de toile|officielle|
|[j1CfaIqiOES2OC3Y.htm](abomination-vaults-bestiary-items/j1CfaIqiOES2OC3Y.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|officielle|
|[J1YjLfTIk9vqgvRy.htm](abomination-vaults-bestiary-items/J1YjLfTIk9vqgvRy.htm)|Malevolent Possession|Possession malveillante|officielle|
|[j2rbPzfS6n5EwSzO.htm](abomination-vaults-bestiary-items/j2rbPzfS6n5EwSzO.htm)|Cooperative Blade|+1,striking|Lame de coopération|officielle|
|[J3s8xl5VZ1K6APbE.htm](abomination-vaults-bestiary-items/J3s8xl5VZ1K6APbE.htm)|Envenom Weapon|Envenimer une arme|officielle|
|[j5nOoy2JdxGbg96d.htm](abomination-vaults-bestiary-items/j5nOoy2JdxGbg96d.htm)|Dimension Door (At Will)|Porte dimensionnelle (À volonté)|officielle|
|[J7wgD2uQamiPxQFV.htm](abomination-vaults-bestiary-items/J7wgD2uQamiPxQFV.htm)|Darkvision|Vision dans le noir|officielle|
|[J82EHqf8yKe7pMEq.htm](abomination-vaults-bestiary-items/J82EHqf8yKe7pMEq.htm)|Tremorsense 30 feet|Perception des vibrations 9 m|officielle|
|[J9CjxTTQRdZozTW4.htm](abomination-vaults-bestiary-items/J9CjxTTQRdZozTW4.htm)|Right of Inspection|Droit de regard|officielle|
|[j9vgTM71zw7kdn4T.htm](abomination-vaults-bestiary-items/j9vgTM71zw7kdn4T.htm)|+2 Status to All Saves vs. Disease and Poison|+2 de statut aux JdS contre la maladie et le poison|officielle|
|[ja1vr0cUJvPastvq.htm](abomination-vaults-bestiary-items/ja1vr0cUJvPastvq.htm)|Speak with Animals (At Will) (Arthropods Only)|Communication avec les animaux (à volonté, arthropodes uniquement)|officielle|
|[jbA5MFmKFD5CdN9h.htm](abomination-vaults-bestiary-items/jbA5MFmKFD5CdN9h.htm)|+1 Studded Leather Armor|Armure en cuir clouté +1|officielle|
|[JCCJdKRetmUpfNFn.htm](abomination-vaults-bestiary-items/JCCJdKRetmUpfNFn.htm)|Fragment of Urevian's Pendant|Fragment du pendentif d'Urévian|officielle|
|[JCTMNa7QfednvGsM.htm](abomination-vaults-bestiary-items/JCTMNa7QfednvGsM.htm)|Acrobatics|Acrobaties|officielle|
|[JEA54JYtf51IHnRH.htm](abomination-vaults-bestiary-items/JEA54JYtf51IHnRH.htm)|Kukri|Kukri|officielle|
|[jEONedyoxPV0Uymr.htm](abomination-vaults-bestiary-items/jEONedyoxPV0Uymr.htm)|Motion Sense 60 feet|Perception du mouvement 18 m|officielle|
|[Jf35tOxuG3lkWtlq.htm](abomination-vaults-bestiary-items/Jf35tOxuG3lkWtlq.htm)|Sneak Attack|Attaque sournoise|officielle|
|[JFcLFPpOrpJo01np.htm](abomination-vaults-bestiary-items/JFcLFPpOrpJo01np.htm)|Feather Fall (At Will) (Self Only)|Feuille morte (À volonté) (soi uniquement)|officielle|
|[JfQnN4zwkzfUcPK4.htm](abomination-vaults-bestiary-items/JfQnN4zwkzfUcPK4.htm)|Dread Flickering|Lueur effrayante|officielle|
|[JgGnEERmYAIJ8Hbc.htm](abomination-vaults-bestiary-items/JgGnEERmYAIJ8Hbc.htm)|Primal Innate Spells|Sorts primordiaux innés|officielle|
|[jIbBcQcAKDXMVJzj.htm](abomination-vaults-bestiary-items/jIbBcQcAKDXMVJzj.htm)|Greater Darkvision|Vision dans le noir supérieure|officielle|
|[jiUi7cG1ldGKfHsM.htm](abomination-vaults-bestiary-items/jiUi7cG1ldGKfHsM.htm)|+1 Flaming Striking Hellforged Glaive|+1,striking,flaming|Coutille forgée des Enfers enflammée de frappe +1|officielle|
|[jl8TRoHirSz5Q9Mt.htm](abomination-vaults-bestiary-items/jl8TRoHirSz5Q9Mt.htm)|Sorcerer Bloodline Spells|Sorts de lignage d'ensorceleur|officielle|
|[JLbHt0LM9WGhFhGj.htm](abomination-vaults-bestiary-items/JLbHt0LM9WGhFhGj.htm)|Summon Animal (Swarm creatures only)|Convocation d'animal (nuées uniquement)|officielle|
|[JlHuStw8Ji1vTXMi.htm](abomination-vaults-bestiary-items/JlHuStw8Ji1vTXMi.htm)|Shoulder to Shoulder|Épaule contre épaule|officielle|
|[JMc7RSkWQTx1DQOo.htm](abomination-vaults-bestiary-items/JMc7RSkWQTx1DQOo.htm)|Magic Immunity|Immunité à la magie|officielle|
|[JMhh3dDSE8195tKc.htm](abomination-vaults-bestiary-items/JMhh3dDSE8195tKc.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|officielle|
|[Jn4tXrVKX66VzX4O.htm](abomination-vaults-bestiary-items/Jn4tXrVKX66VzX4O.htm)|+2 Status to All Saves vs. Mental|+2 de statut aux JdS contre mental|officielle|
|[JPj4ayUtkVtkvYCy.htm](abomination-vaults-bestiary-items/JPj4ayUtkVtkvYCy.htm)|Consume Light|Consumer la lumière|officielle|
|[jqJroJQSIk3eYFSA.htm](abomination-vaults-bestiary-items/jqJroJQSIk3eYFSA.htm)|Low-Light Vision|Vision nocturne|officielle|
|[jS78EFSX9XswID7b.htm](abomination-vaults-bestiary-items/jS78EFSX9XswID7b.htm)|Light Blindness|Aveuglé par la lumière|officielle|
|[JSPBoCm4ZF48k1Qw.htm](abomination-vaults-bestiary-items/JSPBoCm4ZF48k1Qw.htm)|Quick Consumption|Consommation rapide|officielle|
|[jTap9wmrEvrgaFTi.htm](abomination-vaults-bestiary-items/jTap9wmrEvrgaFTi.htm)|Darkness (At Will)|Ténèbres (À volonté)|officielle|
|[JuEXPvUywaRE7BnJ.htm](abomination-vaults-bestiary-items/JuEXPvUywaRE7BnJ.htm)|Light Blindness|Aveuglé par la lumière|officielle|
|[JvcM7bdEkBwNZbGd.htm](abomination-vaults-bestiary-items/JvcM7bdEkBwNZbGd.htm)|Survival|Survie|officielle|
|[JxMkMIPvp1ErxI39.htm](abomination-vaults-bestiary-items/JxMkMIPvp1ErxI39.htm)|Negative Healing|Guérison négative|officielle|
|[k0izMsqD3Kbrn6oD.htm](abomination-vaults-bestiary-items/k0izMsqD3Kbrn6oD.htm)|Magic Immunity|Immunité à la magie|officielle|
|[k1oAvKH4tGhwjVr5.htm](abomination-vaults-bestiary-items/k1oAvKH4tGhwjVr5.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|officielle|
|[k2PMRhPXO7eus3sg.htm](abomination-vaults-bestiary-items/k2PMRhPXO7eus3sg.htm)|Necrotic Decay|Décomposition nécrotique|officielle|
|[kaDLtkZWXkQssjdZ.htm](abomination-vaults-bestiary-items/kaDLtkZWXkQssjdZ.htm)|Adjust Shape|Ajustement de forme|officielle|
|[kbtVRkQHlP5ap1MJ.htm](abomination-vaults-bestiary-items/kbtVRkQHlP5ap1MJ.htm)|Blowgun|Sarbacane|officielle|
|[kCh7O0sugL7oCnHv.htm](abomination-vaults-bestiary-items/kCh7O0sugL7oCnHv.htm)|Darkvision|Vision dans le noir|officielle|
|[kcKV8At9w2cFZz1T.htm](abomination-vaults-bestiary-items/kcKV8At9w2cFZz1T.htm)|Darkvision|Vision dans le noir|officielle|
|[khhjOlnkRw4wzweC.htm](abomination-vaults-bestiary-items/khhjOlnkRw4wzweC.htm)|Sunlight Powerlessness|Impuissance solaire|officielle|
|[KJ3YTwQCESnulZkn.htm](abomination-vaults-bestiary-items/KJ3YTwQCESnulZkn.htm)|Athletics|Athlétisme|officielle|
|[KkhGaZRJP4cnQSOE.htm](abomination-vaults-bestiary-items/KkhGaZRJP4cnQSOE.htm)|Heavy Crossbow|Arbalète lourde|officielle|
|[koHANYfuvrmBm3Bc.htm](abomination-vaults-bestiary-items/koHANYfuvrmBm3Bc.htm)|Light Blindness|Aveuglé par la lumière|officielle|
|[kqpPg4HkBr9iLaWM.htm](abomination-vaults-bestiary-items/kqpPg4HkBr9iLaWM.htm)|Musical Instrument (Virtuoso handheld) (theorbo)|Instrument de musique virtuose (théorbe)|officielle|
|[Kvdwxj4PR8US0RUi.htm](abomination-vaults-bestiary-items/Kvdwxj4PR8US0RUi.htm)|Shortbow|Arc court|officielle|
|[kzKe7Q6umOWzUJdN.htm](abomination-vaults-bestiary-items/kzKe7Q6umOWzUJdN.htm)|At-Will Spells|Sorts à volonté|officielle|
|[l18iYVUpNZjCrvzG.htm](abomination-vaults-bestiary-items/l18iYVUpNZjCrvzG.htm)|Necrotic Decay|Décomposition nécrotique|officielle|
|[l2sX5eZSrv6H5EQ4.htm](abomination-vaults-bestiary-items/l2sX5eZSrv6H5EQ4.htm)|Sibilant Whispers|Murmures sifflés|officielle|
|[l9pZATOZeQOajFDs.htm](abomination-vaults-bestiary-items/l9pZATOZeQOajFDs.htm)|Obscuring Mist (At Will)|Brume de dissimulation (À volonté)|officielle|
|[LAMXu0pzRQpS1K2p.htm](abomination-vaults-bestiary-items/LAMXu0pzRQpS1K2p.htm)|Infused Items|Objets imprégnés|officielle|
|[LBtHMsDMDaIqyB7z.htm](abomination-vaults-bestiary-items/LBtHMsDMDaIqyB7z.htm)|Radiant Touch|Contact radieux|officielle|
|[ley6LXSrjfxUxe7T.htm](abomination-vaults-bestiary-items/ley6LXSrjfxUxe7T.htm)|Ward Contract|Protection des contrats|officielle|
|[LFoqWJmgLEvSJxAw.htm](abomination-vaults-bestiary-items/LFoqWJmgLEvSJxAw.htm)|Darkness (At Will)|Ténèbres (À volonté)|officielle|
|[LfqV1vPTJG0eLpId.htm](abomination-vaults-bestiary-items/LfqV1vPTJG0eLpId.htm)|+2 Status to All Saves vs. Disease and Poison|+2 de statut aux JdS contre la maladie et le poison|officielle|
|[LGGpAgf32ukoqCFX.htm](abomination-vaults-bestiary-items/LGGpAgf32ukoqCFX.htm)|Light Blindness|Aveuglé par la lumière|officielle|
|[lGZkyb82Eeg7M96l.htm](abomination-vaults-bestiary-items/lGZkyb82Eeg7M96l.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|officielle|
|[lIpwTEFoDElIQYqN.htm](abomination-vaults-bestiary-items/lIpwTEFoDElIQYqN.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|officielle|
|[lj2wYgitpa7YQNCt.htm](abomination-vaults-bestiary-items/lj2wYgitpa7YQNCt.htm)|Bouncing Slam|Coup rebondissant|officielle|
|[lJpU26bsaZ9RvxhZ.htm](abomination-vaults-bestiary-items/lJpU26bsaZ9RvxhZ.htm)|Crafting|Artisanat|officielle|
|[LjVRW54085z5XE2p.htm](abomination-vaults-bestiary-items/LjVRW54085z5XE2p.htm)|Legal Lore|Connaissance juridique|officielle|
|[LkqFbyPhl9cNZFyq.htm](abomination-vaults-bestiary-items/LkqFbyPhl9cNZFyq.htm)|Constant Spells|Sorts constants|officielle|
|[LLnHvtR9xniSgkOM.htm](abomination-vaults-bestiary-items/LLnHvtR9xniSgkOM.htm)|Tentacle|Tentacule|officielle|
|[Lnc74mXrMbKNeEpe.htm](abomination-vaults-bestiary-items/Lnc74mXrMbKNeEpe.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[lntTBnW0tn7XEMPJ.htm](abomination-vaults-bestiary-items/lntTBnW0tn7XEMPJ.htm)|Dimension Door (At Will)|Porte dimensionnelle (À volonté)|officielle|
|[LOeBCSVBauL8spor.htm](abomination-vaults-bestiary-items/LOeBCSVBauL8spor.htm)|Black Smear Poison|Poison traînée noire|officielle|
|[Loj0LD68R46zC1Tx.htm](abomination-vaults-bestiary-items/Loj0LD68R46zC1Tx.htm)|Rejuvenation|Reconstruction|officielle|
|[lQzCQf7gDhv2S88W.htm](abomination-vaults-bestiary-items/lQzCQf7gDhv2S88W.htm)|Invisibility (At Will) (Self Only)|Invisibilité (À volonté) (soi uniquement)|officielle|
|[LrviNgUYKAYQl4i9.htm](abomination-vaults-bestiary-items/LrviNgUYKAYQl4i9.htm)|At-Will Spells|Sorts à volonté|officielle|
|[LS15ASdQlOSeQgPH.htm](abomination-vaults-bestiary-items/LS15ASdQlOSeQgPH.htm)|Jaws|Mâchoires|officielle|
|[lTaNtOa2ywL54YZJ.htm](abomination-vaults-bestiary-items/lTaNtOa2ywL54YZJ.htm)|Darkvision|Vision dans le noir|officielle|
|[LuDFjIGjFpp0GFmQ.htm](abomination-vaults-bestiary-items/LuDFjIGjFpp0GFmQ.htm)|Composite Longbow|+1|Arc long composite +1|officielle|
|[LUdvij5KTX4oc3yH.htm](abomination-vaults-bestiary-items/LUdvij5KTX4oc3yH.htm)|Sailing Lore|Connaissance de la navigation maritime|officielle|
|[lVTp2LJQAZzKNaIY.htm](abomination-vaults-bestiary-items/lVTp2LJQAZzKNaIY.htm)|Storm of Blades|Tempête de lames|officielle|
|[LwePStc0eHhM9L5l.htm](abomination-vaults-bestiary-items/LwePStc0eHhM9L5l.htm)|Jaws|Mâchoires|officielle|
|[lZIMAORXwdcjvy4H.htm](abomination-vaults-bestiary-items/lZIMAORXwdcjvy4H.htm)|Distracting Shot|Tir déroutant|officielle|
|[LZjWlCLb24UlRVSo.htm](abomination-vaults-bestiary-items/LZjWlCLb24UlRVSo.htm)|Divine Innate Spells|Sorts divins innés|officielle|
|[m2rnilFYTvOg589S.htm](abomination-vaults-bestiary-items/m2rnilFYTvOg589S.htm)|+1 Status to All Saves vs. Positive|bonus de statut de +1 aux JdS contre les effets positifs|officielle|
|[M7VqYd1K43RsOInY.htm](abomination-vaults-bestiary-items/M7VqYd1K43RsOInY.htm)|Grim Glimmering|Scintillement sinistre|officielle|
|[M9wEcuvNMW8Uu4hV.htm](abomination-vaults-bestiary-items/M9wEcuvNMW8Uu4hV.htm)|Fragment of Urevian's Pendant|Fragment de pendentif d'Urévian|officielle|
|[mA7HIkvCJVI5uu1m.htm](abomination-vaults-bestiary-items/mA7HIkvCJVI5uu1m.htm)|Death Light|Lumière de mort|officielle|
|[mC6jPQk1bRvlBdvh.htm](abomination-vaults-bestiary-items/mC6jPQk1bRvlBdvh.htm)|Repeating Hand Crossbow|Arbalète de poing à répétition|officielle|
|[McjPEAjdnUEE3tx6.htm](abomination-vaults-bestiary-items/McjPEAjdnUEE3tx6.htm)|Shauth Blade|Lame shauth|officielle|
|[mcWNQMlHvbDUhInI.htm](abomination-vaults-bestiary-items/mcWNQMlHvbDUhInI.htm)|Purpose Lore|Connaissance de son objectif|officielle|
|[MDttImPA5a6D9APk.htm](abomination-vaults-bestiary-items/MDttImPA5a6D9APk.htm)|Feed on Magic|Se nourrir de la magie|officielle|
|[mEGMPmEBPm6Apx3y.htm](abomination-vaults-bestiary-items/mEGMPmEBPm6Apx3y.htm)|Wearying Touch|Contact harassant|officielle|
|[melBrcTFYaugSkrF.htm](abomination-vaults-bestiary-items/melBrcTFYaugSkrF.htm)|At-Will Spells|Sorts à volonté|officielle|
|[mgluD1d7bZoq5JBg.htm](abomination-vaults-bestiary-items/mgluD1d7bZoq5JBg.htm)|Fangs|Crocs|officielle|
|[MHuoaCCeuKg2qzE4.htm](abomination-vaults-bestiary-items/MHuoaCCeuKg2qzE4.htm)|Haste (Constant)|Rapidité (constant)|officielle|
|[mj7RC0aTfmNbGcIT.htm](abomination-vaults-bestiary-items/mj7RC0aTfmNbGcIT.htm)|Seugathi's Key|Clé du seugathi|officielle|
|[mL8fUkQ6DtVQj0dy.htm](abomination-vaults-bestiary-items/mL8fUkQ6DtVQj0dy.htm)|Enlarge (Self Only)|Agrandissement (soi uniquement)|officielle|
|[mlcDOwUA9F64jE1Y.htm](abomination-vaults-bestiary-items/mlcDOwUA9F64jE1Y.htm)|Mouth|Gueule|officielle|
|[mlcNFWnWIzRngDv0.htm](abomination-vaults-bestiary-items/mlcNFWnWIzRngDv0.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[mmfsGHgB3hcCPClW.htm](abomination-vaults-bestiary-items/mmfsGHgB3hcCPClW.htm)|Apocalypse Beam (from Monster)|Faisceau apocalyptique (du monstre)|officielle|
|[MnAW3prtfTZxnv7T.htm](abomination-vaults-bestiary-items/MnAW3prtfTZxnv7T.htm)|Magic Item Mastery|Maîtrise des objets magiques|officielle|
|[MOqlNipA5276gM0r.htm](abomination-vaults-bestiary-items/MOqlNipA5276gM0r.htm)|Diplomacy|Diplomatie|officielle|
|[mQfS4yOFYXyHwwas.htm](abomination-vaults-bestiary-items/mQfS4yOFYXyHwwas.htm)|Surprise Attacker|Agresseur surprise|officielle|
|[mqqrqczmNE7FLbro.htm](abomination-vaults-bestiary-items/mqqrqczmNE7FLbro.htm)|Fear (At Will)|Terreur (À volonté)|officielle|
|[mS0Mzbjj7mazrPnr.htm](abomination-vaults-bestiary-items/mS0Mzbjj7mazrPnr.htm)|Death Flame|Flamme mortelle|officielle|
|[mSAztmtFp4kF0SdP.htm](abomination-vaults-bestiary-items/mSAztmtFp4kF0SdP.htm)|Occult Innate Spells|Sorts occultes innés|officielle|
|[MsljHqXO1LMWSpK4.htm](abomination-vaults-bestiary-items/MsljHqXO1LMWSpK4.htm)|Spit|Crachat|officielle|
|[mSS5Nol0cifKQmxM.htm](abomination-vaults-bestiary-items/mSS5Nol0cifKQmxM.htm)|Knockdown|Renversement|officielle|
|[mTtnkTVclwbLF3EX.htm](abomination-vaults-bestiary-items/mTtnkTVclwbLF3EX.htm)|Faerie Fire (At Will)|Lueurs féeriques (À volonté)|officielle|
|[mUBZliZ2MS11im0F.htm](abomination-vaults-bestiary-items/mUBZliZ2MS11im0F.htm)|Repeating Hand Crossbow|+1,striking|Arbalète de poing à répétition de frappe +1|officielle|
|[mUJY9mXKNFT1G9Ri.htm](abomination-vaults-bestiary-items/mUJY9mXKNFT1G9Ri.htm)|Negative Healing|Guérison négative|officielle|
|[mxpkzQTw0tzSwaUL.htm](abomination-vaults-bestiary-items/mxpkzQTw0tzSwaUL.htm)|Dimension Door (At Will)|Porte dimensionnelle (À volonté)|officielle|
|[MZelOTUvBCjq2zbz.htm](abomination-vaults-bestiary-items/MZelOTUvBCjq2zbz.htm)|Draft Contract|Rédaction de contrats|officielle|
|[MZkr0UEdl93FQyzP.htm](abomination-vaults-bestiary-items/MZkr0UEdl93FQyzP.htm)|Bloom|Fleur|officielle|
|[N0G4vzuAThb89i17.htm](abomination-vaults-bestiary-items/N0G4vzuAThb89i17.htm)|Rejuvenation|Reconstruction|officielle|
|[N1zdZTTLXEp3x5eJ.htm](abomination-vaults-bestiary-items/N1zdZTTLXEp3x5eJ.htm)|Wicked Bite|Méchante morsure|officielle|
|[n2w1C4HwH3QTV7dw.htm](abomination-vaults-bestiary-items/n2w1C4HwH3QTV7dw.htm)|Swift Leap|Bond rapide|officielle|
|[n5dgRSJ7OB2x6r3Y.htm](abomination-vaults-bestiary-items/n5dgRSJ7OB2x6r3Y.htm)|Infused Bottled Lightning (Moderate)|Foudre en bouteille modéré imprégné|officielle|
|[N7x5OQAAhVdxvKnV.htm](abomination-vaults-bestiary-items/N7x5OQAAhVdxvKnV.htm)|Death Shadows|Ombres mortelles|officielle|
|[NAX30nf7nlVx5vTo.htm](abomination-vaults-bestiary-items/NAX30nf7nlVx5vTo.htm)|Cheek Pouches|Abajoues|officielle|
|[NBuJOqDORlJbI3m9.htm](abomination-vaults-bestiary-items/NBuJOqDORlJbI3m9.htm)|Wisp Form|Forme de flamerolle|officielle|
|[nETM5TEYK8ufocn0.htm](abomination-vaults-bestiary-items/nETM5TEYK8ufocn0.htm)|Low-Light Vision|Vision nocturne|officielle|
|[NflnAPRnfgBSgygz.htm](abomination-vaults-bestiary-items/NflnAPRnfgBSgygz.htm)|Divine Innate Spells|Sorts divins innés|officielle|
|[nhdPKzoIkY6tfcLb.htm](abomination-vaults-bestiary-items/nhdPKzoIkY6tfcLb.htm)|Fragment of Urevian's Pendant|Fragment de pendentif d’Urévian|officielle|
|[NiDXROLnOZXSEpNC.htm](abomination-vaults-bestiary-items/NiDXROLnOZXSEpNC.htm)|Jaws|Mâchoires|officielle|
|[NJwXFlfXwTfxdk7U.htm](abomination-vaults-bestiary-items/NJwXFlfXwTfxdk7U.htm)|Sneak Attack|Attaque sournoise|officielle|
|[nkgGXuCapE0gTVxH.htm](abomination-vaults-bestiary-items/nkgGXuCapE0gTVxH.htm)|Negative Healing|Guérison négative|officielle|
|[nksShZpMIUxollKt.htm](abomination-vaults-bestiary-items/nksShZpMIUxollKt.htm)|Handful of Junk|Poignée de bric-à-brac|officielle|
|[nLKHrHv3rcodZfOf.htm](abomination-vaults-bestiary-items/nLKHrHv3rcodZfOf.htm)|Seugathi Venom|Venin seugathi|officielle|
|[nQoYz6B0Wx0OjS4e.htm](abomination-vaults-bestiary-items/nQoYz6B0Wx0OjS4e.htm)|Undulating Step|Pas ondulant|officielle|
|[nQVh2gtZd534YNJW.htm](abomination-vaults-bestiary-items/nQVh2gtZd534YNJW.htm)|Stealth|Discrétion|officielle|
|[NrJvXEDlrKMp5Eh0.htm](abomination-vaults-bestiary-items/NrJvXEDlrKMp5Eh0.htm)|Fist|Poing|officielle|
|[nSxXMYPDUloleNgn.htm](abomination-vaults-bestiary-items/nSxXMYPDUloleNgn.htm)|Warding Shove|Bourrade protectrice|officielle|
|[NTAt5R3Zz1e8OAHf.htm](abomination-vaults-bestiary-items/NTAt5R3Zz1e8OAHf.htm)|War Flail|Fléau de guerre|officielle|
|[NTmAGY84p18KWTa8.htm](abomination-vaults-bestiary-items/NTmAGY84p18KWTa8.htm)|+2 Status To All Saves vs. Mental|+2 de statut aux JdS contre mental|officielle|
|[NuYsILwFjvqlwWPp.htm](abomination-vaults-bestiary-items/NuYsILwFjvqlwWPp.htm)|Darkvision|Vision dans le noir|officielle|
|[nvaQh9xjojojnL27.htm](abomination-vaults-bestiary-items/nvaQh9xjojojnL27.htm)|Attack Now!|Attaque maintenant !|officielle|
|[NVdOFyf2EscpiksE.htm](abomination-vaults-bestiary-items/NVdOFyf2EscpiksE.htm)|Battle Axe|Hache d'armes|officielle|
|[Nwtd1G94Ikab4Ejf.htm](abomination-vaults-bestiary-items/Nwtd1G94Ikab4Ejf.htm)|Scuttling Attack|Attaque en courant|officielle|
|[NyXCVjhHwWZtpfOI.htm](abomination-vaults-bestiary-items/NyXCVjhHwWZtpfOI.htm)|Consume Flesh|Dévorer la chair|officielle|
|[NzFY8G1K3Mfksj2F.htm](abomination-vaults-bestiary-items/NzFY8G1K3Mfksj2F.htm)|Tremorsense (Imprecise) 30 feet|Perception des vibrations 9 m (imprécis)|officielle|
|[nzWpj7yo8PguGBFG.htm](abomination-vaults-bestiary-items/nzWpj7yo8PguGBFG.htm)|Bite|Morsure|officielle|
|[O2CT9C7oM6YOLZNX.htm](abomination-vaults-bestiary-items/O2CT9C7oM6YOLZNX.htm)|Glow|Lueur|officielle|
|[O9UgZMqET8EKtovt.htm](abomination-vaults-bestiary-items/O9UgZMqET8EKtovt.htm)|Feather Fall (At Will) (Self Only)|Feuille morte (À volonté) (soi uniquement)|officielle|
|[oAOkRgFO0POryMdo.htm](abomination-vaults-bestiary-items/oAOkRgFO0POryMdo.htm)|Percussive Reverberation|Percussion réverbérante|officielle|
|[obfPUaVoDmOFchMR.htm](abomination-vaults-bestiary-items/obfPUaVoDmOFchMR.htm)|Jaws|Mâchoires|officielle|
|[od2Tpdqgyx5vuTIw.htm](abomination-vaults-bestiary-items/od2Tpdqgyx5vuTIw.htm)|Poison Weapon|Arme empoisonnée|officielle|
|[oeWKbVvK3lP66J9I.htm](abomination-vaults-bestiary-items/oeWKbVvK3lP66J9I.htm)|Rejuvenation|Reconstruction|officielle|
|[OHEkKMalMOyLMyRM.htm](abomination-vaults-bestiary-items/OHEkKMalMOyLMyRM.htm)|Draining Touch|Toucher affaiblissant|officielle|
|[ohL0OcnbucaAroHs.htm](abomination-vaults-bestiary-items/ohL0OcnbucaAroHs.htm)|Deft Evasion|Évasion|officielle|
|[OIcVAsJUARlaypEm.htm](abomination-vaults-bestiary-items/OIcVAsJUARlaypEm.htm)|At-Will Spells|Sorts à volonté|officielle|
|[oIJY4ycXpaF4IjIM.htm](abomination-vaults-bestiary-items/oIJY4ycXpaF4IjIM.htm)|See Invisibility (Constant)|Détection de l'invisibilité (constant)|officielle|
|[OjH9zugdZf9plsD2.htm](abomination-vaults-bestiary-items/OjH9zugdZf9plsD2.htm)|Composite Longbow|+1,striking|Arc long composite de frappe +1|officielle|
|[okdtSA4yvXuAw47k.htm](abomination-vaults-bestiary-items/okdtSA4yvXuAw47k.htm)|Tongues (Constant)|Don des langues (constant)|officielle|
|[Okdzr1mvuSTh4c4K.htm](abomination-vaults-bestiary-items/Okdzr1mvuSTh4c4K.htm)|Battle Lute|Luth de combat|officielle|
|[oKefVQZfmh0iqIhc.htm](abomination-vaults-bestiary-items/oKefVQZfmh0iqIhc.htm)|No MAP|Pas de PAM|officielle|
|[OKsFbAOCubRBtq1O.htm](abomination-vaults-bestiary-items/OKsFbAOCubRBtq1O.htm)|Occult Ward|Protection occulte|officielle|
|[OksJxFiBFjS3xboM.htm](abomination-vaults-bestiary-items/OksJxFiBFjS3xboM.htm)|Mark for Death|Condamné à mort|officielle|
|[OlnYDBwZvO3CeHG3.htm](abomination-vaults-bestiary-items/OlnYDBwZvO3CeHG3.htm)|Force Blast|Explosion de force|officielle|
|[oLOpCzPI98yXuw1u.htm](abomination-vaults-bestiary-items/oLOpCzPI98yXuw1u.htm)|Consume Flesh|Dévorer la chair|officielle|
|[om8dBOucNQt5iWeZ.htm](abomination-vaults-bestiary-items/om8dBOucNQt5iWeZ.htm)|Domain Spells|Sorts de domaine|officielle|
|[oMcchH9BcuFxkNKA.htm](abomination-vaults-bestiary-items/oMcchH9BcuFxkNKA.htm)|Haunted Lighthouse|Phare hanté|officielle|
|[OMfPkRtQLgK6lUE8.htm](abomination-vaults-bestiary-items/OMfPkRtQLgK6lUE8.htm)|Gatekeeper's Will|Volonté du gardien|officielle|
|[onlqduhJCOKTLTGj.htm](abomination-vaults-bestiary-items/onlqduhJCOKTLTGj.htm)|At-Will Spells|Sorts à volonté|officielle|
|[OOe8PxQr9xXaWkvc.htm](abomination-vaults-bestiary-items/OOe8PxQr9xXaWkvc.htm)|Hand Crossbow|Arbalète de poing|officielle|
|[OOpmi6uvkCP25AwD.htm](abomination-vaults-bestiary-items/OOpmi6uvkCP25AwD.htm)|Bravery|Bravoure|officielle|
|[OqYpi6JullFs4WMU.htm](abomination-vaults-bestiary-items/OqYpi6JullFs4WMU.htm)|Negative Healing|Guérison négative|officielle|
|[osvhHIWUUPwyQszY.htm](abomination-vaults-bestiary-items/osvhHIWUUPwyQszY.htm)|Tremorsense 30 feet|Perception des vibrations 9 m|officielle|
|[oviUeLIMpj5EZvoL.htm](abomination-vaults-bestiary-items/oviUeLIMpj5EZvoL.htm)|Negative Healing|Guérison négative|officielle|
|[OvlSSz4vVMTtlzue.htm](abomination-vaults-bestiary-items/OvlSSz4vVMTtlzue.htm)|Telepathy 100 feet|Télépathie 30 m|officielle|
|[owQaoWxiXKdJdh64.htm](abomination-vaults-bestiary-items/owQaoWxiXKdJdh64.htm)|Infernal Wound|Blessure infernale|officielle|
|[OxVsLGBZ8k9rqQxY.htm](abomination-vaults-bestiary-items/OxVsLGBZ8k9rqQxY.htm)|Jaws|Mâchoires|officielle|
|[OZ0Sm3t3N9QrDKvO.htm](abomination-vaults-bestiary-items/OZ0Sm3t3N9QrDKvO.htm)|Infused Elixir of Life (Lesser)|Élixir de vie inférieur imprégné|officielle|
|[P0CkVE8kq3xALSWx.htm](abomination-vaults-bestiary-items/P0CkVE8kq3xALSWx.htm)|Negative Healing|Guérison négative|officielle|
|[P1ggE8aYgCQOaac1.htm](abomination-vaults-bestiary-items/P1ggE8aYgCQOaac1.htm)|Death Burst|Mort explosive|officielle|
|[P2nBJI904M5iiMWb.htm](abomination-vaults-bestiary-items/P2nBJI904M5iiMWb.htm)|Shortsword|Épee courte|officielle|
|[p5BKZyaTi06oRBta.htm](abomination-vaults-bestiary-items/p5BKZyaTi06oRBta.htm)|Padli's Key|Clé de Padli|libre|
|[P7RUoae9G01AaAHd.htm](abomination-vaults-bestiary-items/P7RUoae9G01AaAHd.htm)|Negative Healing|Guérison négative|officielle|
|[p9kCNA8Mq9cXm23t.htm](abomination-vaults-bestiary-items/p9kCNA8Mq9cXm23t.htm)|Darkvision|Vision dans le noir|officielle|
|[pb8PZlMfMR2NS3Vt.htm](abomination-vaults-bestiary-items/pb8PZlMfMR2NS3Vt.htm)|War Leader|Chef de guerre|officielle|
|[PBrXRWLJMqALmC7c.htm](abomination-vaults-bestiary-items/PBrXRWLJMqALmC7c.htm)|Greater Darkvision|Vision dans le noir supérieure|officielle|
|[PCXa7Z3d10U79Quj.htm](abomination-vaults-bestiary-items/PCXa7Z3d10U79Quj.htm)|Vengeful Anger|Colère vengeresse|officielle|
|[PDBItJftGnhDq2dg.htm](abomination-vaults-bestiary-items/PDBItJftGnhDq2dg.htm)|At-Will Spells|Sorts à volonté|officielle|
|[PFH04FMn9c52nnhf.htm](abomination-vaults-bestiary-items/PFH04FMn9c52nnhf.htm)|Sneak Attack|Attaque sournoise|officielle|
|[Pgdez88Zqu0dqgGI.htm](abomination-vaults-bestiary-items/Pgdez88Zqu0dqgGI.htm)|Despairing Cry|Cri désespéré|officielle|
|[pi0jucwlFYIo5mn8.htm](abomination-vaults-bestiary-items/pi0jucwlFYIo5mn8.htm)|Discorporate|Dématérialisation|officielle|
|[pIZQ59XrVNVhyiJY.htm](abomination-vaults-bestiary-items/pIZQ59XrVNVhyiJY.htm)|Library Lore|Connaissance des bibliothèques|officielle|
|[PJPuhtPtTcNoLZS4.htm](abomination-vaults-bestiary-items/PJPuhtPtTcNoLZS4.htm)|Tamchal Chakram|Chakram tamchal|officielle|
|[PkDvZBCHiGCocxUn.htm](abomination-vaults-bestiary-items/PkDvZBCHiGCocxUn.htm)|Primal Innate Spells|Sorts primordiaux innés|officielle|
|[pozH1aOlixvNcvU6.htm](abomination-vaults-bestiary-items/pozH1aOlixvNcvU6.htm)|Burn Knowledge|Brûler le savoir|officielle|
|[PpFajj7OukFhYId7.htm](abomination-vaults-bestiary-items/PpFajj7OukFhYId7.htm)|Darkvision|Vision dans le noir|officielle|
|[Pr87C99kuBwyUE2x.htm](abomination-vaults-bestiary-items/Pr87C99kuBwyUE2x.htm)|Spirit Sight (Precise) 30 feet|Perception des esprits 9 m (précis)|officielle|
|[PXA2uFcT0VRiQkBO.htm](abomination-vaults-bestiary-items/PXA2uFcT0VRiQkBO.htm)|Fearful Curse|Malédiction d’effroi|officielle|
|[pyBAuiHwkCVhaTe8.htm](abomination-vaults-bestiary-items/pyBAuiHwkCVhaTe8.htm)|Shock|Foudre|officielle|
|[pZ4b9ZPNlMtoF8P7.htm](abomination-vaults-bestiary-items/pZ4b9ZPNlMtoF8P7.htm)|Bite|Morsure|officielle|
|[Q1GNuBqe5LEkrKBH.htm](abomination-vaults-bestiary-items/Q1GNuBqe5LEkrKBH.htm)|Mind Reading (At Will)|Lecture des pensées (À volonté)|officielle|
|[q2sYrbAFzwVFBmAZ.htm](abomination-vaults-bestiary-items/q2sYrbAFzwVFBmAZ.htm)|Wand of Illusory Creature (Level 2)|Baguette de Créature illusoire (niveau 2)|officielle|
|[Q43gFqCqgbv9y5VO.htm](abomination-vaults-bestiary-items/Q43gFqCqgbv9y5VO.htm)|Skirmish Strike|Frappe d’escarmouche|officielle|
|[Q4CQqu46DoUxnpdv.htm](abomination-vaults-bestiary-items/Q4CQqu46DoUxnpdv.htm)|Invisibility (Self Only)|Invisibilité (soi uniquement)|officielle|
|[Q4ib52AxEVe6nHUx.htm](abomination-vaults-bestiary-items/Q4ib52AxEVe6nHUx.htm)|Gauntlight Beam|Faisceau de Mornelueur|officielle|
|[Q4NBsMEwBKrHlvNK.htm](abomination-vaults-bestiary-items/Q4NBsMEwBKrHlvNK.htm)|Oily Scales|Écailles huileuses|officielle|
|[Q4RY5CXdMVoB93xD.htm](abomination-vaults-bestiary-items/Q4RY5CXdMVoB93xD.htm)|Underground Stride|Marcher rapidement sous terre|officielle|
|[Q8iuI7tt5UHoTXnP.htm](abomination-vaults-bestiary-items/Q8iuI7tt5UHoTXnP.htm)|Swift Leap|Bond rapide|officielle|
|[QaDq4G70qrufNlS5.htm](abomination-vaults-bestiary-items/QaDq4G70qrufNlS5.htm)|Sapping Squeeze|Pression absorbante|officielle|
|[Qc2AY3uXl9fGeDBe.htm](abomination-vaults-bestiary-items/Qc2AY3uXl9fGeDBe.htm)|Hampering Slash|Taillade entravante|officielle|
|[Qc5SGmGRuhj2vMYt.htm](abomination-vaults-bestiary-items/Qc5SGmGRuhj2vMYt.htm)|Loose Bones|Os déjointés|libre|
|[QcH6j6nAL93LSs8M.htm](abomination-vaults-bestiary-items/QcH6j6nAL93LSs8M.htm)|Athletics|Athlétisme|officielle|
|[QcW3MprdYsKVeRT7.htm](abomination-vaults-bestiary-items/QcW3MprdYsKVeRT7.htm)|Flood of Despair|Désespoir envahissant|officielle|
|[QdYWcouo4wSAer3W.htm](abomination-vaults-bestiary-items/QdYWcouo4wSAer3W.htm)|Occult Prepared Spells|Sorts occultes préparés|officielle|
|[qFopdg6xXJDmE4k7.htm](abomination-vaults-bestiary-items/qFopdg6xXJDmE4k7.htm)|Cavern Distortion|Distorsion caverneuse|officielle|
|[QHc3i64UJqwenXAH.htm](abomination-vaults-bestiary-items/QHc3i64UJqwenXAH.htm)|Darkvision|Vision dans le noir|officielle|
|[Qhmy1EmaoDYJuVdZ.htm](abomination-vaults-bestiary-items/Qhmy1EmaoDYJuVdZ.htm)|Grab|Empoignade|officielle|
|[QIxI3Cz1mN1x6wZt.htm](abomination-vaults-bestiary-items/QIxI3Cz1mN1x6wZt.htm)|Starknife|+1|Lamétoile +1|officielle|
|[QJJWPGE3n8ZduC9D.htm](abomination-vaults-bestiary-items/QJJWPGE3n8ZduC9D.htm)|Light the Living Wick|Allumer la mèche vive|officielle|
|[Qkqj9KzvAaoHkxjH.htm](abomination-vaults-bestiary-items/Qkqj9KzvAaoHkxjH.htm)|Darkvision|Vision dans le noir|officielle|
|[Qkr3ZHA5WjD7gYCY.htm](abomination-vaults-bestiary-items/Qkr3ZHA5WjD7gYCY.htm)|Warding Script|Écriture de protection|officielle|
|[QlygOvoO8v1q3qZ1.htm](abomination-vaults-bestiary-items/QlygOvoO8v1q3qZ1.htm)|Swarming Stance|Posture de nuée|officielle|
|[Qmj5gqP1YgGLr1W3.htm](abomination-vaults-bestiary-items/Qmj5gqP1YgGLr1W3.htm)|Rhoka Sword|Épée rhoka|officielle|
|[QOG4x5d8jOfeGb6l.htm](abomination-vaults-bestiary-items/QOG4x5d8jOfeGb6l.htm)|Negative Healing|Guérison négative|officielle|
|[QPbTNI3BxNTdGPuH.htm](abomination-vaults-bestiary-items/QPbTNI3BxNTdGPuH.htm)|Divine Prepared Spells|Sorts divins préparés|officielle|
|[QrVRCf0qabKVREzS.htm](abomination-vaults-bestiary-items/QrVRCf0qabKVREzS.htm)|Darkvision|Vision dans le noir|officielle|
|[QSEVW6fObs2AVajU.htm](abomination-vaults-bestiary-items/QSEVW6fObs2AVajU.htm)|Corrupting Gaze|Regard corrupteur|officielle|
|[qtILk42sO65XzDmW.htm](abomination-vaults-bestiary-items/qtILk42sO65XzDmW.htm)|Consume Flesh|Dévorer la chair|officielle|
|[qTIxm70Zd6ETxfgu.htm](abomination-vaults-bestiary-items/qTIxm70Zd6ETxfgu.htm)|Insightful Swing|Coup pénétrant|officielle|
|[qtqSaGbHHzYr1Zvx.htm](abomination-vaults-bestiary-items/qtqSaGbHHzYr1Zvx.htm)|Rusty Chains|Chaînes rouillées|officielle|
|[qU19blO5Z6vVE060.htm](abomination-vaults-bestiary-items/qU19blO5Z6vVE060.htm)|Infused Wyvern Poison|Poison de vouivre imprégné|officielle|
|[Qw2zvLnJQFja72D2.htm](abomination-vaults-bestiary-items/Qw2zvLnJQFja72D2.htm)|Darkvision|Vision dans le noir|officielle|
|[qwwxBH4SCVdtmTb2.htm](abomination-vaults-bestiary-items/qwwxBH4SCVdtmTb2.htm)|Sneak Attack|Attaque sournoise|officielle|
|[QxLOTtEoMt74dE5J.htm](abomination-vaults-bestiary-items/QxLOTtEoMt74dE5J.htm)|Repeating Hand Crossbow|+1|Arbalète de poing à répétition +1|officielle|
|[qykfSUOQXBbtu4gM.htm](abomination-vaults-bestiary-items/qykfSUOQXBbtu4gM.htm)|Frenzied Attack|Attaque frénétique|officielle|
|[qyWEf4ehAOLXTLjf.htm](abomination-vaults-bestiary-items/qyWEf4ehAOLXTLjf.htm)|Seugathi Venom|Venin seugathi|officielle|
|[QzrBb53s8z16DDpm.htm](abomination-vaults-bestiary-items/QzrBb53s8z16DDpm.htm)|Sound Burst (At Will)|Cacophonie (À volonté)|officielle|
|[R0x59cKzEcWsxbbV.htm](abomination-vaults-bestiary-items/R0x59cKzEcWsxbbV.htm)|Longsword|Épée longue|officielle|
|[R2IrxhTRU85GEr92.htm](abomination-vaults-bestiary-items/R2IrxhTRU85GEr92.htm)|Swallow Whole|Gober|officielle|
|[R6nrltKyDBZG2PNf.htm](abomination-vaults-bestiary-items/R6nrltKyDBZG2PNf.htm)|All-Around Vision|Vision panoramique|officielle|
|[RatED2kNCjAiVv2a.htm](abomination-vaults-bestiary-items/RatED2kNCjAiVv2a.htm)|Fist|Poing|officielle|
|[RBjfyeeaXd5gEMNe.htm](abomination-vaults-bestiary-items/RBjfyeeaXd5gEMNe.htm)|Curse of the Werewolf|Malédiction du loup-garou|officielle|
|[Rc6ZoOmMG3mlyrrV.htm](abomination-vaults-bestiary-items/Rc6ZoOmMG3mlyrrV.htm)|Spore Explosion|Explosion de spores|officielle|
|[rdEPYGmtMUuJ7I3P.htm](abomination-vaults-bestiary-items/rdEPYGmtMUuJ7I3P.htm)|Fearful Strike|Frappe terrorisante|officielle|
|[rdUiajy3hLLrKXt4.htm](abomination-vaults-bestiary-items/rdUiajy3hLLrKXt4.htm)|Azvalvigander's Key|Clé d'Azvalvigandre|officielle|
|[re67Wr8G5ybq0iEY.htm](abomination-vaults-bestiary-items/re67Wr8G5ybq0iEY.htm)|Survivor's Nourishment|Nourriture du rescapé|officielle|
|[rET5SfCVJHTK7d5a.htm](abomination-vaults-bestiary-items/rET5SfCVJHTK7d5a.htm)|Athletics|Athlétisme|officielle|
|[rGX35xgDAZROYYQF.htm](abomination-vaults-bestiary-items/rGX35xgDAZROYYQF.htm)|Rejuvenation|Reconstruction|officielle|
|[RhLJUtGDkZup2Y2e.htm](abomination-vaults-bestiary-items/RhLJUtGDkZup2Y2e.htm)|Fleshy Slap|Claque charnue|officielle|
|[Ri0eYhcAj6M7CX7f.htm](abomination-vaults-bestiary-items/Ri0eYhcAj6M7CX7f.htm)|All-Around Vision|Vision panoramique|officielle|
|[rJLYYTfUR7ew1R0A.htm](abomination-vaults-bestiary-items/rJLYYTfUR7ew1R0A.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|officielle|
|[rKb9CKHwR45IKSQQ.htm](abomination-vaults-bestiary-items/rKb9CKHwR45IKSQQ.htm)|Shauth Blade|+1|Lame shauthe +1|officielle|
|[rLUQOqHBgpcUnpXn.htm](abomination-vaults-bestiary-items/rLUQOqHBgpcUnpXn.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|officielle|
|[rq016ZULtzeq5Jdo.htm](abomination-vaults-bestiary-items/rq016ZULtzeq5Jdo.htm)|Greater Darkvision|Vision dans le noir supérieure|officielle|
|[RQSPQLwfW9A5Ijew.htm](abomination-vaults-bestiary-items/RQSPQLwfW9A5Ijew.htm)|Apocalypse Beam (from Burning City)|Faisceau apocalyptique (de la cité incendiée)|officielle|
|[Rt1kYgHWRK0u7WPS.htm](abomination-vaults-bestiary-items/Rt1kYgHWRK0u7WPS.htm)|Darkvision|Vision dans le noir|officielle|
|[RuO5xdjimVRC9EbR.htm](abomination-vaults-bestiary-items/RuO5xdjimVRC9EbR.htm)|Obscuring Mist (At Will)|Brume de dissimulation (À volonté)|officielle|
|[rV1k0lGcAvoytfnj.htm](abomination-vaults-bestiary-items/rV1k0lGcAvoytfnj.htm)|Tendril|Vrille|officielle|
|[RwS0OFipezRfdrZG.htm](abomination-vaults-bestiary-items/RwS0OFipezRfdrZG.htm)|+2 Status to All Saves vs. Magic|+2 de statut aux JdS contre la magie|officielle|
|[rXLsiUgGNMASCFTX.htm](abomination-vaults-bestiary-items/rXLsiUgGNMASCFTX.htm)|See Invisibility (Constant)|Détection de l'invisibilité (constant)|officielle|
|[rzTC3Hz3hT9xd3W9.htm](abomination-vaults-bestiary-items/rzTC3Hz3hT9xd3W9.htm)|Consume Tattooed Flesh|Dévorer la chair tatouée|officielle|
|[S0eUokfR9NCiHTkD.htm](abomination-vaults-bestiary-items/S0eUokfR9NCiHTkD.htm)|Wolf Empathy|Empathie avec les loups|officielle|
|[S0PyywlB6YgiEU7L.htm](abomination-vaults-bestiary-items/S0PyywlB6YgiEU7L.htm)|Light Blindness|Aveuglé par la lumière|officielle|
|[S31ljAQqgmPf1CMj.htm](abomination-vaults-bestiary-items/S31ljAQqgmPf1CMj.htm)|Constrict (Grabbed by Claws only)|Constriction (empoigné par les griffes uniquement)|officielle|
|[s48Fu4ttbbhaVZUd.htm](abomination-vaults-bestiary-items/s48Fu4ttbbhaVZUd.htm)|Jaws|Mâchoires|officielle|
|[S4gQomqdPJUG2IPl.htm](abomination-vaults-bestiary-items/S4gQomqdPJUG2IPl.htm)|Acid Flask|Fiole d'acide|officielle|
|[s57i0xdwt99HF3Tp.htm](abomination-vaults-bestiary-items/s57i0xdwt99HF3Tp.htm)|Trident|Trident|officielle|
|[S9nPU36oRplQB6mn.htm](abomination-vaults-bestiary-items/S9nPU36oRplQB6mn.htm)|Opportune Step|Pas opportun|officielle|
|[sAuhwgcHiN60xIJT.htm](abomination-vaults-bestiary-items/sAuhwgcHiN60xIJT.htm)|Swarming|Pullulement|officielle|
|[SawmEH90X7bn6dTY.htm](abomination-vaults-bestiary-items/SawmEH90X7bn6dTY.htm)|Darkvision|Vision dans le noir|officielle|
|[SC9Vfc2U7mtfdimN.htm](abomination-vaults-bestiary-items/SC9Vfc2U7mtfdimN.htm)|Death Flame|Flamme mortelle|officielle|
|[sdsJsKNzbvVK8R4I.htm](abomination-vaults-bestiary-items/sdsJsKNzbvVK8R4I.htm)|Wooden Chair|Chaise en bois|officielle|
|[sEiZ1blIoaIiKSvz.htm](abomination-vaults-bestiary-items/sEiZ1blIoaIiKSvz.htm)|Tremorsense (Imprecise) 30 feet|Perception des vibrations 9 m (imprécis)|officielle|
|[sHEWmioIhOpSrSzb.htm](abomination-vaults-bestiary-items/sHEWmioIhOpSrSzb.htm)|Quara's Key|Clé de Quara|libre|
|[SHNbQciOGHNt8z1c.htm](abomination-vaults-bestiary-items/SHNbQciOGHNt8z1c.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|officielle|
|[sIBET4O7QFkxNkyF.htm](abomination-vaults-bestiary-items/sIBET4O7QFkxNkyF.htm)|Swamp Stride|Déplacement facilité dans les marais|officielle|
|[SiVeimRpwflBeUiq.htm](abomination-vaults-bestiary-items/SiVeimRpwflBeUiq.htm)|Consume Masterpiece|Dévorer le chef-d’oeuvre|officielle|
|[sJW19BG0UI1qi3ct.htm](abomination-vaults-bestiary-items/sJW19BG0UI1qi3ct.htm)|Divine Innate Spells|Sorts divins innés|officielle|
|[Ska9kA0i2bCxOPKc.htm](abomination-vaults-bestiary-items/Ska9kA0i2bCxOPKc.htm)|Hateful Memories|Souvenirs détestables|officielle|
|[smJkeUn705exUfuJ.htm](abomination-vaults-bestiary-items/smJkeUn705exUfuJ.htm)|Negative Healing|Guérison négative|officielle|
|[SsAchSsgznoVg1Ws.htm](abomination-vaults-bestiary-items/SsAchSsgznoVg1Ws.htm)|Darkness (At Will)|Ténèbres (À volonté)|officielle|
|[STdfpuDxhppjlflP.htm](abomination-vaults-bestiary-items/STdfpuDxhppjlflP.htm)|Skillful Catch|Saisie habile|officielle|
|[SuNMXHWTyC2lF2Pa.htm](abomination-vaults-bestiary-items/SuNMXHWTyC2lF2Pa.htm)|Claw|Griffes|officielle|
|[sVvkzILqsBCfcw03.htm](abomination-vaults-bestiary-items/sVvkzILqsBCfcw03.htm)|Ghostly Hand|Main spectrale|officielle|
|[SWVNzLPTD6kuaZsY.htm](abomination-vaults-bestiary-items/SWVNzLPTD6kuaZsY.htm)|Wand of Magic Missile (Level 2)|Baguette de Projectile magique (niveau 2)|officielle|
|[sXswvc2qcns84U92.htm](abomination-vaults-bestiary-items/sXswvc2qcns84U92.htm)|Dagger|Dague|officielle|
|[t1m1XIVqXFEnCTKv.htm](abomination-vaults-bestiary-items/t1m1XIVqXFEnCTKv.htm)|Mindfog Aura|Aura de brume mentale|officielle|
|[t4jPFFTR7Eur2Nml.htm](abomination-vaults-bestiary-items/t4jPFFTR7Eur2Nml.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|officielle|
|[t4QXi08ulbzmSfHt.htm](abomination-vaults-bestiary-items/t4QXi08ulbzmSfHt.htm)|Divine Prepared Spells|Sorts divins préparés|officielle|
|[T7FvhRfA6QKYe0b7.htm](abomination-vaults-bestiary-items/T7FvhRfA6QKYe0b7.htm)|Athletics|Athlétisme|officielle|
|[T8KOcWxiqo3StVBo.htm](abomination-vaults-bestiary-items/T8KOcWxiqo3StVBo.htm)|Spit|Crachat|officielle|
|[T9tAbwnyo4ZmiWkk.htm](abomination-vaults-bestiary-items/T9tAbwnyo4ZmiWkk.htm)|Fervent Command|Ordre véhément|officielle|
|[taKy6ih4jlHzjBsw.htm](abomination-vaults-bestiary-items/taKy6ih4jlHzjBsw.htm)|Sapping Squeeze|Pression absorbante|officielle|
|[TBlIXTNQSQ1e3bEo.htm](abomination-vaults-bestiary-items/TBlIXTNQSQ1e3bEo.htm)|Snout|Museau|officielle|
|[tce3ovXnnblqwD77.htm](abomination-vaults-bestiary-items/tce3ovXnnblqwD77.htm)|Shred Flesh|Déchiqueter les chairs|officielle|
|[TdzSpCHa1QM7Auhy.htm](abomination-vaults-bestiary-items/TdzSpCHa1QM7Auhy.htm)|Distracting Declaration|Déclaration troublante|officielle|
|[thNxqBSaHQwe1kqj.htm](abomination-vaults-bestiary-items/thNxqBSaHQwe1kqj.htm)|Primal Prepared Spells|Sorts primordiaux préparés|officielle|
|[tJRkLCUMgqynhzQ9.htm](abomination-vaults-bestiary-items/tJRkLCUMgqynhzQ9.htm)|Infested Shadow|Ombre infestée|officielle|
|[tjRomYaatwAgMUvv.htm](abomination-vaults-bestiary-items/tjRomYaatwAgMUvv.htm)|Partially Technological|Partiellement technologique|officielle|
|[TKCoF7GXS2ldg658.htm](abomination-vaults-bestiary-items/TKCoF7GXS2ldg658.htm)|Jaws|Mâchoires|officielle|
|[tNfhf1JnOxOHfY86.htm](abomination-vaults-bestiary-items/tNfhf1JnOxOHfY86.htm)|Envenom Weapon|Envenimer une arme|officielle|
|[TPGcPPTzpn0NMPGl.htm](abomination-vaults-bestiary-items/TPGcPPTzpn0NMPGl.htm)|Sneak Attack|Attaque sournoise|officielle|
|[Tq0WVPPxrkpFbZn4.htm](abomination-vaults-bestiary-items/Tq0WVPPxrkpFbZn4.htm)|Fist|Poing|officielle|
|[tW1jcJOAZ3y2pThe.htm](abomination-vaults-bestiary-items/tW1jcJOAZ3y2pThe.htm)|Starknife|Lamétoile|officielle|
|[tWnV5NxscBllgsSs.htm](abomination-vaults-bestiary-items/tWnV5NxscBllgsSs.htm)|Magic Sense|Perception de la magie|officielle|
|[TwPr4oH0p59YtoDn.htm](abomination-vaults-bestiary-items/TwPr4oH0p59YtoDn.htm)|Harrow Fortune-Telling Deck|Jeu du tourment|officielle|
|[Tx79k8XzQHlqc3ew.htm](abomination-vaults-bestiary-items/Tx79k8XzQHlqc3ew.htm)|Dagger|Dague|officielle|
|[txASc5iIQvLV4Nxv.htm](abomination-vaults-bestiary-items/txASc5iIQvLV4Nxv.htm)|Bejeweled Necklace featuring a Porpoise|Collier orné d’une pierre précieuse|officielle|
|[U0pfy4tbvu2Y9KYj.htm](abomination-vaults-bestiary-items/U0pfy4tbvu2Y9KYj.htm)|Sneak Attack|Attaque sournoise|officielle|
|[u8Tqev49a0Iwqy80.htm](abomination-vaults-bestiary-items/u8Tqev49a0Iwqy80.htm)|At-Will Spells|Sorts à volonté|officielle|
|[u9UlDj5Q35Cp8lmR.htm](abomination-vaults-bestiary-items/u9UlDj5Q35Cp8lmR.htm)|All-Around Vision|Vision panoramique|officielle|
|[UAaoMzfNeh44Gojl.htm](abomination-vaults-bestiary-items/UAaoMzfNeh44Gojl.htm)|Bone Shard|Esquille d'os|officielle|
|[UbtJOXzzsAmXD5rI.htm](abomination-vaults-bestiary-items/UbtJOXzzsAmXD5rI.htm)|Swarm Shape|Forme de nuée|officielle|
|[uc4ln8R3AB0uWMGh.htm](abomination-vaults-bestiary-items/uc4ln8R3AB0uWMGh.htm)|Darkness (At Will)|Ténèbres (À volonté)|officielle|
|[ufvpAL31F9ZaHsEo.htm](abomination-vaults-bestiary-items/ufvpAL31F9ZaHsEo.htm)|Bounding Swarm|Nuée bondissante|officielle|
|[UI4pCPb4yqFcryJ6.htm](abomination-vaults-bestiary-items/UI4pCPb4yqFcryJ6.htm)|+2 Status To All Saves vs. Mental|+2 de statut aux JdS contre mental|officielle|
|[UIrt3wtjXEhvQtEL.htm](abomination-vaults-bestiary-items/UIrt3wtjXEhvQtEL.htm)|Powerful Stench|Puissante puanteur|officielle|
|[uItSOCTzoCrocBon.htm](abomination-vaults-bestiary-items/uItSOCTzoCrocBon.htm)|Corpse Sense (Precise) 30 feet|Perception des cadavres|officielle|
|[ujbUCBlQf3V81dRB.htm](abomination-vaults-bestiary-items/ujbUCBlQf3V81dRB.htm)|Torture Lore|Connaissance de la torture|officielle|
|[UK5IIgsnGnQ6Oh6K.htm](abomination-vaults-bestiary-items/UK5IIgsnGnQ6Oh6K.htm)|Negative Healing|Guérison négative|officielle|
|[uKvKe5cNgTA0yIxl.htm](abomination-vaults-bestiary-items/uKvKe5cNgTA0yIxl.htm)|Overpowering Jaws|Mâchoires surpuissantes|officielle|
|[UP3MBQg1Px5Bkp7a.htm](abomination-vaults-bestiary-items/UP3MBQg1Px5Bkp7a.htm)|Dagger|Dague|officielle|
|[UQ4kfO1kRCsEtz0l.htm](abomination-vaults-bestiary-items/UQ4kfO1kRCsEtz0l.htm)|Divine Innate Spells|Sorts divins innés|officielle|
|[Usk5KyF9A2RQaJt2.htm](abomination-vaults-bestiary-items/Usk5KyF9A2RQaJt2.htm)|Flurry of Blows|Déluge de coups|officielle|
|[uuED3LvzlH3CHS3S.htm](abomination-vaults-bestiary-items/uuED3LvzlH3CHS3S.htm)|Sudden Throw|Jet surprise|officielle|
|[UV3vQl5jrXwr1wQh.htm](abomination-vaults-bestiary-items/UV3vQl5jrXwr1wQh.htm)|Undulating Step|Pas ondulant|officielle|
|[uWu9iZkKF8LUjUoe.htm](abomination-vaults-bestiary-items/uWu9iZkKF8LUjUoe.htm)|+2 Status to All Saves vs. Disease and Poison|+2 de statut aux JdS contre la maladie et le poison|officielle|
|[UYMJ2nbr7f8EgOHc.htm](abomination-vaults-bestiary-items/UYMJ2nbr7f8EgOHc.htm)|Negative Recovery|Guérison négative|officielle|
|[V0Zof9p6jaQUrOl0.htm](abomination-vaults-bestiary-items/V0Zof9p6jaQUrOl0.htm)|Tremorsense (Imprecise) 30 feet|Perception des vibrations 9 m (imprécis)|officielle|
|[v1N2fY4anfmaOUr3.htm](abomination-vaults-bestiary-items/v1N2fY4anfmaOUr3.htm)|Jaws|Mâchoires|officielle|
|[V5Ok21wotF3DQNBZ.htm](abomination-vaults-bestiary-items/V5Ok21wotF3DQNBZ.htm)|Someone is Watching|Quelqu’un observe|officielle|
|[v7ImH8CplEBf5cVO.htm](abomination-vaults-bestiary-items/v7ImH8CplEBf5cVO.htm)|Warhammer|+1,striking|Marteau de guerre de frappe +1|officielle|
|[vBp53lYo9D4gq1jh.htm](abomination-vaults-bestiary-items/vBp53lYo9D4gq1jh.htm)|All-Around Vision|Vision panoramique|officielle|
|[Vc1oe6EJUXOlITbu.htm](abomination-vaults-bestiary-items/Vc1oe6EJUXOlITbu.htm)|Wand of See Invisibility (Level 2)|Baguette de Détection de l'invisibilité (niveau 2)|officielle|
|[vdPooFytAas2OzRw.htm](abomination-vaults-bestiary-items/vdPooFytAas2OzRw.htm)|Reloading Trick|Recharger en un tour de main|officielle|
|[VGPs2FUB6OC8igcY.htm](abomination-vaults-bestiary-items/VGPs2FUB6OC8igcY.htm)|Web|Toile|officielle|
|[vguesBj7MoOj8ogP.htm](abomination-vaults-bestiary-items/vguesBj7MoOj8ogP.htm)|Jafaki's Round Key|Clé ronde de Jafaki|officielle|
|[vH2uvgGZ6JCaWrIl.htm](abomination-vaults-bestiary-items/vH2uvgGZ6JCaWrIl.htm)|Ghostly Hand|Main spectrale|officielle|
|[VH3uvuMCquGbT6Wx.htm](abomination-vaults-bestiary-items/VH3uvuMCquGbT6Wx.htm)|Regeneration 10 (Deactivated by Good or Silver)|Régénération 10 (désactivé par bon ou argent)|officielle|
|[vhcL0ctFDySpLekK.htm](abomination-vaults-bestiary-items/vhcL0ctFDySpLekK.htm)|Negative Healing|Guérison négative|officielle|
|[VJFdV0JcVaaqVlyL.htm](abomination-vaults-bestiary-items/VJFdV0JcVaaqVlyL.htm)|Divine Innate Spells|Sorts divins innés|officielle|
|[VK6P3hNaRnT5JPUT.htm](abomination-vaults-bestiary-items/VK6P3hNaRnT5JPUT.htm)|Quill|Plume|officielle|
|[VkmU3zCSF1tm61N6.htm](abomination-vaults-bestiary-items/VkmU3zCSF1tm61N6.htm)|Command Confusion|Injonction aux confus|officielle|
|[VkXxpkRx6E7dyCI4.htm](abomination-vaults-bestiary-items/VkXxpkRx6E7dyCI4.htm)|Vermin Empathy|Empathie avec la vermine|officielle|
|[vMlj7QxxkX61zODC.htm](abomination-vaults-bestiary-items/vMlj7QxxkX61zODC.htm)|Sneak Attack|Attaque sournoise|officielle|
|[vmnzh7dfffPn9ub8.htm](abomination-vaults-bestiary-items/vmnzh7dfffPn9ub8.htm)|Crafting|Artisanat|officielle|
|[VNqQVxfzCgmliiPq.htm](abomination-vaults-bestiary-items/VNqQVxfzCgmliiPq.htm)|Tail|Queue|officielle|
|[Vo6AHVWsAnxqkwLW.htm](abomination-vaults-bestiary-items/Vo6AHVWsAnxqkwLW.htm)|Drover's Band|Bracelet du vacher|officielle|
|[vO90s2UOixYIPRSk.htm](abomination-vaults-bestiary-items/vO90s2UOixYIPRSk.htm)|Jaws|Mâchoires|officielle|
|[VOrtrf3kSz1oIQkq.htm](abomination-vaults-bestiary-items/VOrtrf3kSz1oIQkq.htm)|Darkvision|Vision dans le noir|officielle|
|[VPyIMfFo4j5NMPBG.htm](abomination-vaults-bestiary-items/VPyIMfFo4j5NMPBG.htm)|Dagger|Dague|officielle|
|[Vq09pyw5K2mb3YeT.htm](abomination-vaults-bestiary-items/Vq09pyw5K2mb3YeT.htm)|Longsword|Épée longue|officielle|
|[VqTDcQNmGBPYQ6d3.htm](abomination-vaults-bestiary-items/VqTDcQNmGBPYQ6d3.htm)|Greater Darkvision|Vision dans le noir supérieure|officielle|
|[VQXGCkNvdRLDuyH1.htm](abomination-vaults-bestiary-items/VQXGCkNvdRLDuyH1.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|officielle|
|[Vr79KdOVhzih6EMo.htm](abomination-vaults-bestiary-items/Vr79KdOVhzih6EMo.htm)|Rapier|Rapière|officielle|
|[VssG55TESDIyaiPP.htm](abomination-vaults-bestiary-items/VssG55TESDIyaiPP.htm)|Lump of Decayed Flesh Strung on a Necklace|Morceau de chair décomposée attachés à un collier|officielle|
|[vTyv5W3SkeNgrprr.htm](abomination-vaults-bestiary-items/vTyv5W3SkeNgrprr.htm)|Occult Innate Spells|Sorts occultes innés|officielle|
|[VUmbLmAZWhr6cx2b.htm](abomination-vaults-bestiary-items/VUmbLmAZWhr6cx2b.htm)|Longsword|Épée longue|officielle|
|[VwIv1y01lqATWPRT.htm](abomination-vaults-bestiary-items/VwIv1y01lqATWPRT.htm)|Necrotic Decay|Décomposition nécrotique|officielle|
|[VWWosu7Wvs4yAKZy.htm](abomination-vaults-bestiary-items/VWWosu7Wvs4yAKZy.htm)|Grab|Empoignade|officielle|
|[VXZ8QpZ1EGC0hg4D.htm](abomination-vaults-bestiary-items/VXZ8QpZ1EGC0hg4D.htm)|Magic Immunity|Immunité à la magie|officielle|
|[vYamsmUJUWGVxqsL.htm](abomination-vaults-bestiary-items/vYamsmUJUWGVxqsL.htm)|+1 Resilient Full Plate|Harnois de résilience +1|officielle|
|[VYGdMMY84xwadkDM.htm](abomination-vaults-bestiary-items/VYGdMMY84xwadkDM.htm)|Faerie Fire (At Will)|Lueurs féeriques (À volonté)|officielle|
|[VzobMtkoa82xICPm.htm](abomination-vaults-bestiary-items/VzobMtkoa82xICPm.htm)|Darkvision|Vision dans le noir|officielle|
|[W0VQcpug1ZFVNZox.htm](abomination-vaults-bestiary-items/W0VQcpug1ZFVNZox.htm)|Occult Innate Spells|Sorts occultes innés|officielle|
|[w1H2Uu7gjl1fTH9B.htm](abomination-vaults-bestiary-items/w1H2Uu7gjl1fTH9B.htm)|Writing Slate|Ardoise|officielle|
|[W2nnShEx9f5pWTjq.htm](abomination-vaults-bestiary-items/W2nnShEx9f5pWTjq.htm)|Pewter Mug|Gobelet en étain|officielle|
|[W4yMtkEVcpKOM2tE.htm](abomination-vaults-bestiary-items/W4yMtkEVcpKOM2tE.htm)|Broad Swipe|Grande frappe transversale|officielle|
|[W7NJfdI36Wt2pLfo.htm](abomination-vaults-bestiary-items/W7NJfdI36Wt2pLfo.htm)|Camouflaged Step|Pas de camouflage|officielle|
|[w8TbxNNTVBH6j4jP.htm](abomination-vaults-bestiary-items/w8TbxNNTVBH6j4jP.htm)|Shootist's Draw|Dégainer comme un arbalétrier d’élite|officielle|
|[WA3qxd5nl6en4BPF.htm](abomination-vaults-bestiary-items/WA3qxd5nl6en4BPF.htm)|Spore Cloud|Nuage de spores|officielle|
|[WAo4NMhw8o3YpQGq.htm](abomination-vaults-bestiary-items/WAo4NMhw8o3YpQGq.htm)|Wicked Bite|Méchante morsure|officielle|
|[WCcurB9iEOCcU58H.htm](abomination-vaults-bestiary-items/WCcurB9iEOCcU58H.htm)|Paralysis|Paralysie|officielle|
|[wcJ7xyyDuDpmKxyt.htm](abomination-vaults-bestiary-items/wcJ7xyyDuDpmKxyt.htm)|Tremorsense (Imprecise) 30 feet|Perception des vibrations 9 m (imprécis)|officielle|
|[WersQyXKosWIiXXl.htm](abomination-vaults-bestiary-items/WersQyXKosWIiXXl.htm)|Ghostly Hand|Main spectrale|officielle|
|[WfbnKRcDwQ4WCR3g.htm](abomination-vaults-bestiary-items/WfbnKRcDwQ4WCR3g.htm)|Feather Fall (At Will) (Self Only)|Feuille morte (À volonté) (soi uniquement)|officielle|
|[wImzz2Ucpz9gqh2J.htm](abomination-vaults-bestiary-items/wImzz2Ucpz9gqh2J.htm)|Claw|Griffes|officielle|
|[wjbGJGSRPWZ49qZd.htm](abomination-vaults-bestiary-items/wjbGJGSRPWZ49qZd.htm)|Spittle|Salive|officielle|
|[wKmUr6VixtVPXpXR.htm](abomination-vaults-bestiary-items/wKmUr6VixtVPXpXR.htm)|Tamchal Chakram|Chakram tamchal|officielle|
|[WKywaKpRvPLFLVvG.htm](abomination-vaults-bestiary-items/WKywaKpRvPLFLVvG.htm)|Mosh|Pogo|officielle|
|[wNrNVfIbe0JDWhOu.htm](abomination-vaults-bestiary-items/wNrNVfIbe0JDWhOu.htm)|Faerie Fire (At Will)|Lueurs féeriques (À volonté)|officielle|
|[wnV3crCWLBRnytqk.htm](abomination-vaults-bestiary-items/wnV3crCWLBRnytqk.htm)|Death Flame|Flamme mortelle|officielle|
|[WsGIMLD86LgMNUWg.htm](abomination-vaults-bestiary-items/WsGIMLD86LgMNUWg.htm)|Claw|Griffes|officielle|
|[WUQCPwFNjk75gqNw.htm](abomination-vaults-bestiary-items/WUQCPwFNjk75gqNw.htm)|Infused Cognitive Mutagen (Moderate)|Mutagène cognitif modéré imprégné|officielle|
|[WVPQDcIaUdkMfawy.htm](abomination-vaults-bestiary-items/WVPQDcIaUdkMfawy.htm)|Rapier|Rapière|officielle|
|[wVxlFqCWi3T6bOk1.htm](abomination-vaults-bestiary-items/wVxlFqCWi3T6bOk1.htm)|Projectile Launcher|Lance-projectile|officielle|
|[wWufyBbAG3JXOfoE.htm](abomination-vaults-bestiary-items/wWufyBbAG3JXOfoE.htm)|Darkvision|Vision dans le noir|officielle|
|[wyg04TlEIo9tJBCj.htm](abomination-vaults-bestiary-items/wyg04TlEIo9tJBCj.htm)|Athletics|Athlétisme|officielle|
|[wyQSk60Np0UavJiY.htm](abomination-vaults-bestiary-items/wyQSk60Np0UavJiY.htm)|Stasis Field|Champ de stase|officielle|
|[x07lPwTJOL6PzSxR.htm](abomination-vaults-bestiary-items/x07lPwTJOL6PzSxR.htm)|+1 Striking Rhoka Sword|+1,striking|Épée rhoka de frappe +1|officielle|
|[x13RVTp5gtE3XOTa.htm](abomination-vaults-bestiary-items/x13RVTp5gtE3XOTa.htm)|Defensive Shooter|Tireur défensif|officielle|
|[x2mDaJtGWiO1eS56.htm](abomination-vaults-bestiary-items/x2mDaJtGWiO1eS56.htm)|Counterfeit Haunting|Faux fantôme|officielle|
|[X9hw08u8m3fLbSva.htm](abomination-vaults-bestiary-items/X9hw08u8m3fLbSva.htm)|Invisibility (Self Only)|Invisibilité (soi uniquement)|officielle|
|[xA9OK9ybw4WLdQ8o.htm](abomination-vaults-bestiary-items/xA9OK9ybw4WLdQ8o.htm)|Darkvision|Vision dans le noir|officielle|
|[xCCM8tyhLkWqYUpL.htm](abomination-vaults-bestiary-items/xCCM8tyhLkWqYUpL.htm)|Motion Sense 60 feet|Perception du mouvement 18 m|officielle|
|[Xd2kZ5dL4yuq4lr3.htm](abomination-vaults-bestiary-items/Xd2kZ5dL4yuq4lr3.htm)|Athletics|Athlétisme|officielle|
|[XETiXceuUZEG0el1.htm](abomination-vaults-bestiary-items/XETiXceuUZEG0el1.htm)|Rejuvenation|Reconstruction|officielle|
|[XezgRZTc5EmXVys0.htm](abomination-vaults-bestiary-items/XezgRZTc5EmXVys0.htm)|Fist|Poing|officielle|
|[XfR063rnMuiow4bN.htm](abomination-vaults-bestiary-items/XfR063rnMuiow4bN.htm)|Opportune Step|Pas opportun|officielle|
|[xGaxH2mmKH0ttLJY.htm](abomination-vaults-bestiary-items/xGaxH2mmKH0ttLJY.htm)|Shauth Lash|+1,striking|Fouet shauth de frappe +1|officielle|
|[xiTvpYtL10U6KNkT.htm](abomination-vaults-bestiary-items/xiTvpYtL10U6KNkT.htm)|Darkvision|Vision dans le noir|officielle|
|[Xj2woyiQHPYlP7Uc.htm](abomination-vaults-bestiary-items/Xj2woyiQHPYlP7Uc.htm)|Darkness (At Will)|Ténèbres (À volonté)|officielle|
|[xk8dbRocKTe3GSWC.htm](abomination-vaults-bestiary-items/xk8dbRocKTe3GSWC.htm)|Spore Explosion|Explosion de spores|officielle|
|[XKiDDCClxzUDYSO8.htm](abomination-vaults-bestiary-items/XKiDDCClxzUDYSO8.htm)|Ghoul Fever|Fièvre des goules|officielle|
|[XNJnpNGQCkQD00v3.htm](abomination-vaults-bestiary-items/XNJnpNGQCkQD00v3.htm)|Rejuvenation|Reconstruction|officielle|
|[xoFC8JZ1B0deUW2k.htm](abomination-vaults-bestiary-items/xoFC8JZ1B0deUW2k.htm)|Magic Item Mastery|Maîtrise des objets magiques|officielle|
|[xpJLgdCTRRKsEqi3.htm](abomination-vaults-bestiary-items/xpJLgdCTRRKsEqi3.htm)|Magic Item Mastery|Maîtrise des objets magiques|officielle|
|[XrEqDkgs6eaCAzhM.htm](abomination-vaults-bestiary-items/XrEqDkgs6eaCAzhM.htm)|Wicked Bite|Méchante morsure|officielle|
|[XsfO4kZKUmGSNSI0.htm](abomination-vaults-bestiary-items/XsfO4kZKUmGSNSI0.htm)|+1 Status to All Saves vs. Poison|bonus de statut de +1 aux JdS contre le poison|officielle|
|[xTCkHuwFUsvdVThS.htm](abomination-vaults-bestiary-items/xTCkHuwFUsvdVThS.htm)|Aquatic Ambush|Embuscade aquatique|officielle|
|[xuheW5c3xyUS2F9E.htm](abomination-vaults-bestiary-items/xuheW5c3xyUS2F9E.htm)|Kukri|+1,striking|Kukri de frappe +1|officielle|
|[xuzZonbgefgsArAz.htm](abomination-vaults-bestiary-items/xuzZonbgefgsArAz.htm)|Summon Entity (Will-o'-Wisp Only)|Convocation d'entité (feu follet uniquement)|officielle|
|[XvsfsnZoPvQNu6Be.htm](abomination-vaults-bestiary-items/XvsfsnZoPvQNu6Be.htm)|Negative Healing|Guérison négative|officielle|
|[xVxy6iARZLipqA8h.htm](abomination-vaults-bestiary-items/xVxy6iARZLipqA8h.htm)|Command Confusion|Injonction aux confus|officielle|
|[xWD7oLqr7RfHGQVH.htm](abomination-vaults-bestiary-items/xWD7oLqr7RfHGQVH.htm)|Bone Shard|Esquille d'os|officielle|
|[xze5oFK48fMv2xBU.htm](abomination-vaults-bestiary-items/xze5oFK48fMv2xBU.htm)|Tentacle Transfer|Transfert tentaculaire|officielle|
|[XzUUZ1FoJXIkVqZu.htm](abomination-vaults-bestiary-items/XzUUZ1FoJXIkVqZu.htm)|Uncanny Tinker|Bricoleur remarquable|officielle|
|[Y0gOm1Uz3dMu6Q4W.htm](abomination-vaults-bestiary-items/Y0gOm1Uz3dMu6Q4W.htm)|Claim Corpse|Occuper un cadavre|officielle|
|[Y0zIOXUUeYuBTRst.htm](abomination-vaults-bestiary-items/Y0zIOXUUeYuBTRst.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|officielle|
|[y1F32N9fSld9NydJ.htm](abomination-vaults-bestiary-items/y1F32N9fSld9NydJ.htm)|Negative Healing|Guérison négative|officielle|
|[y4g9ZcRWSHKAxsUe.htm](abomination-vaults-bestiary-items/y4g9ZcRWSHKAxsUe.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|officielle|
|[y4um9lgRgEVJYHeJ.htm](abomination-vaults-bestiary-items/y4um9lgRgEVJYHeJ.htm)|Paralysis|Paralysie|officielle|
|[Y7vFj13afzJKpHws.htm](abomination-vaults-bestiary-items/Y7vFj13afzJKpHws.htm)|Pinning Chomp|Coup de dents immobilisant|officielle|
|[y87UDyuJmwKsI7Tq.htm](abomination-vaults-bestiary-items/y87UDyuJmwKsI7Tq.htm)|At-Will Spells|Sorts à volonté|officielle|
|[y8rBowLYE52m6Vb7.htm](abomination-vaults-bestiary-items/y8rBowLYE52m6Vb7.htm)|Go Dark|S'éteindre|officielle|
|[Y8ZISA3GaUO4MtHh.htm](abomination-vaults-bestiary-items/Y8ZISA3GaUO4MtHh.htm)|Jaws|Mâchoires|officielle|
|[Y9Z7x3aVBCSbKMxB.htm](abomination-vaults-bestiary-items/Y9Z7x3aVBCSbKMxB.htm)|Grab|Empoignade|officielle|
|[yafzmxKBfx68zshb.htm](abomination-vaults-bestiary-items/yafzmxKBfx68zshb.htm)|Hand Crossbow|Arbalète de poing|libre|
|[yajcaYxlIfR8IeG6.htm](abomination-vaults-bestiary-items/yajcaYxlIfR8IeG6.htm)|Arcane Prepared Spells|Sorts arcaniques préparés|officielle|
|[Ybx41yIdQO59hKjK.htm](abomination-vaults-bestiary-items/Ybx41yIdQO59hKjK.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|officielle|
|[yCKSVMhk8eLrvD0h.htm](abomination-vaults-bestiary-items/yCKSVMhk8eLrvD0h.htm)|Darkvision|Vision dans le noir|officielle|
|[yCvj5N1H04hmAYuS.htm](abomination-vaults-bestiary-items/yCvj5N1H04hmAYuS.htm)|Gas Release|Diffusion du gaz|officielle|
|[yD0yYDGEVE1rfNJJ.htm](abomination-vaults-bestiary-items/yD0yYDGEVE1rfNJJ.htm)|Ghoul Fever|Fièvre des goules|officielle|
|[yDdnC4S3nrhfHYil.htm](abomination-vaults-bestiary-items/yDdnC4S3nrhfHYil.htm)|Heat Mirage|Mirage chaud|officielle|
|[YE5eTfCKX1ANEEP7.htm](abomination-vaults-bestiary-items/YE5eTfCKX1ANEEP7.htm)|Light Blindness|Aveuglé par la lumière|officielle|
|[YEnM0GJZ6D2RdXBg.htm](abomination-vaults-bestiary-items/YEnM0GJZ6D2RdXBg.htm)|Composite Longbow|Arc long composite|officielle|
|[YfnA27LEjbg8h3rR.htm](abomination-vaults-bestiary-items/YfnA27LEjbg8h3rR.htm)|Blowgun Darts (Poisoned)|Flèchettes (empoisonnées)|officielle|
|[ygCGxbBzOI9gN0fC.htm](abomination-vaults-bestiary-items/ygCGxbBzOI9gN0fC.htm)|Knock It Away|Du vent !|officielle|
|[yHhHaZrYuyLVq7cD.htm](abomination-vaults-bestiary-items/yHhHaZrYuyLVq7cD.htm)|Caliddo's Keys|Clés de Caliddo|libre|
|[yJdnNORoa1KtMM7W.htm](abomination-vaults-bestiary-items/yJdnNORoa1KtMM7W.htm)|Site Bound|Lié à un site|officielle|
|[yJVTcQFJpy86gPDG.htm](abomination-vaults-bestiary-items/yJVTcQFJpy86gPDG.htm)|Claw|Griffes|officielle|
|[YK1tWx8RaI3guzkW.htm](abomination-vaults-bestiary-items/YK1tWx8RaI3guzkW.htm)|Warhammer|Marteau de guerre|officielle|
|[YnER4NJf6wi05tio.htm](abomination-vaults-bestiary-items/YnER4NJf6wi05tio.htm)|Claw|Griffes|officielle|
|[yNjPKC3nPRiaD957.htm](abomination-vaults-bestiary-items/yNjPKC3nPRiaD957.htm)|Dicing Scythes|Faux tranchantes|officielle|
|[ynM8E8eSEza5D8PW.htm](abomination-vaults-bestiary-items/ynM8E8eSEza5D8PW.htm)|Salaisa's Key|Clé de Salaisa|libre|
|[YsiUbsAWRxZMjaWA.htm](abomination-vaults-bestiary-items/YsiUbsAWRxZMjaWA.htm)|Ghoul Fever|Fièvre des goules|officielle|
|[YuDlX3Kr7cepFAW4.htm](abomination-vaults-bestiary-items/YuDlX3Kr7cepFAW4.htm)|Feed on Despair|Se nourrir du désespoir|officielle|
|[YUgXFNB4xn08S3Cf.htm](abomination-vaults-bestiary-items/YUgXFNB4xn08S3Cf.htm)|Club|Gourdin|officielle|
|[yUMwDlPA7DiVRv5u.htm](abomination-vaults-bestiary-items/yUMwDlPA7DiVRv5u.htm)|Negative Healing|Guérison négative|officielle|
|[YuXrcTfctRYHmw90.htm](abomination-vaults-bestiary-items/YuXrcTfctRYHmw90.htm)|Glow|Lueur|officielle|
|[YvcIDxmDZnqrfRBa.htm](abomination-vaults-bestiary-items/YvcIDxmDZnqrfRBa.htm)|Locate (At Will)|Localisation (À volonté)|officielle|
|[yvKEo91RnhVFPZ5L.htm](abomination-vaults-bestiary-items/yvKEo91RnhVFPZ5L.htm)|Negative Healing|Guérison négative|officielle|
|[YvmZQz32k7hFcqzM.htm](abomination-vaults-bestiary-items/YvmZQz32k7hFcqzM.htm)|Darkvision|Vision dans le noir|officielle|
|[YwUDfdiKYGDgdz8Q.htm](abomination-vaults-bestiary-items/YwUDfdiKYGDgdz8Q.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[yxXEQaBGVF13c0rW.htm](abomination-vaults-bestiary-items/yxXEQaBGVF13c0rW.htm)|Dagger|Dague|officielle|
|[z6U85IjlJZGBsdcA.htm](abomination-vaults-bestiary-items/z6U85IjlJZGBsdcA.htm)|Occult Innate Spells|Sorts occultes innés|officielle|
|[z7r4p7kLzt2ZbByW.htm](abomination-vaults-bestiary-items/z7r4p7kLzt2ZbByW.htm)|Violin|Violon|officielle|
|[za8ix1WE9HbNfHvg.htm](abomination-vaults-bestiary-items/za8ix1WE9HbNfHvg.htm)|Grab|Empoignade|officielle|
|[zALaGtcb55Te1IcS.htm](abomination-vaults-bestiary-items/zALaGtcb55Te1IcS.htm)|Darkvision|Vision dans le noir|officielle|
|[Zb3D43B4UbusNqAe.htm](abomination-vaults-bestiary-items/Zb3D43B4UbusNqAe.htm)|Light Blindness|Aveuglé par la lumière|officielle|
|[zb4lD1qa1tSyLe20.htm](abomination-vaults-bestiary-items/zb4lD1qa1tSyLe20.htm)|Claw|Griffes|officielle|
|[ZCynz5sJ6bYK6fY6.htm](abomination-vaults-bestiary-items/ZCynz5sJ6bYK6fY6.htm)|Low-Light Vision|Vision nocturne|officielle|
|[ZfdrndtEZTKXbrq0.htm](abomination-vaults-bestiary-items/ZfdrndtEZTKXbrq0.htm)|Red Ruin Stance|Posture des ravages rouges|officielle|
|[zGg5a7ALqF7zVFCa.htm](abomination-vaults-bestiary-items/zGg5a7ALqF7zVFCa.htm)|Ectoplasmic Web|Toile ectoplasmique|officielle|
|[ZkF8meq1sB3uUIcI.htm](abomination-vaults-bestiary-items/ZkF8meq1sB3uUIcI.htm)|Darkvision|Vision dans le noir|officielle|
|[zKQUDZzcDDJHuOiB.htm](abomination-vaults-bestiary-items/zKQUDZzcDDJHuOiB.htm)|Paralysis|Paralysie|officielle|
|[zm8POWGccsjyfuPN.htm](abomination-vaults-bestiary-items/zm8POWGccsjyfuPN.htm)|Bouncing Crush|Écrasement rebondissant|officielle|
|[ZmA1lKUhd2XoWfNY.htm](abomination-vaults-bestiary-items/ZmA1lKUhd2XoWfNY.htm)|Drumstick|Baguette|officielle|
|[ZnFe1AJx67Diyerp.htm](abomination-vaults-bestiary-items/ZnFe1AJx67Diyerp.htm)|Occult Innate Spells|Sorts occultes innés|officielle|
|[zNFSrrQNB1kgV8Go.htm](abomination-vaults-bestiary-items/zNFSrrQNB1kgV8Go.htm)|Stay in the Fight|Continuer le combat|officielle|
|[ZqBQutg0Cvs5YcxR.htm](abomination-vaults-bestiary-items/ZqBQutg0Cvs5YcxR.htm)|Negative Healing|Guérison négative|officielle|
|[ZrjdCvEH7E5tQb4n.htm](abomination-vaults-bestiary-items/ZrjdCvEH7E5tQb4n.htm)|Frightful Presence|Présence terrifiante|officielle|
|[zSR3MB9KNpQPcxA0.htm](abomination-vaults-bestiary-items/zSR3MB9KNpQPcxA0.htm)|Drumstick|Baguette|officielle|
|[ZtEncTg58fTcUXvv.htm](abomination-vaults-bestiary-items/ZtEncTg58fTcUXvv.htm)|Apocalypse Beam|Faisceau apocalyptique|officielle|
|[ZU5jisN3KgoM2kPq.htm](abomination-vaults-bestiary-items/ZU5jisN3KgoM2kPq.htm)|Darkvision|Vision dans le noir|officielle|
|[zuOWrPyeR35yI1r7.htm](abomination-vaults-bestiary-items/zuOWrPyeR35yI1r7.htm)|Leg Quill|Piquant de patte|officielle|
|[ZVpl1dcKnK3GldQS.htm](abomination-vaults-bestiary-items/ZVpl1dcKnK3GldQS.htm)|Trident|Trident|officielle|
|[zWjx9xYQFt0WhRW5.htm](abomination-vaults-bestiary-items/zWjx9xYQFt0WhRW5.htm)|Divine Innate Spells|Sorts divins innés|officielle|
|[zXmPJ4IvqhUl8bad.htm](abomination-vaults-bestiary-items/zXmPJ4IvqhUl8bad.htm)|Grab|Empoignade|officielle|
|[zyawAgjl2eHH085S.htm](abomination-vaults-bestiary-items/zyawAgjl2eHH085S.htm)|Drumstick|Baguette|officielle|
|[zYMKuVB2VBXOtrfe.htm](abomination-vaults-bestiary-items/zYMKuVB2VBXOtrfe.htm)|Divine Innate Spells|Sorts divins innés|officielle|
|[ZZcgAENc8VCfxb7e.htm](abomination-vaults-bestiary-items/ZZcgAENc8VCfxb7e.htm)|Illusory Disguise (At Will)|Déguisement illusoire (À volonté)|officielle|
|[zzmMQYbPGJRgOH7E.htm](abomination-vaults-bestiary-items/zzmMQYbPGJRgOH7E.htm)|Ghostly Assault|Assaut fantomatique|officielle|
|[zZQXw3WG7NLAQrux.htm](abomination-vaults-bestiary-items/zZQXw3WG7NLAQrux.htm)|Swift Leap|Bond rapide|officielle|
