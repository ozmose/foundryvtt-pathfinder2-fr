# État de la traduction (book-of-the-dead-bestiary)

 * **libre**: 116


Dernière mise à jour: 2023-07-12 20:28 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0PrrwvV1936eSCQy.htm](book-of-the-dead-bestiary/0PrrwvV1936eSCQy.htm)|Tormented (Burning)|Tourmenté (Brûlé)|libre|
|[1854X0YrbGyTnhCy.htm](book-of-the-dead-bestiary/1854X0YrbGyTnhCy.htm)|Sadistic Conductor|Chef d'orchestre sadique|libre|
|[1hdQjYLtup92Jlls.htm](book-of-the-dead-bestiary/1hdQjYLtup92Jlls.htm)|Obrousian|Obrousian|libre|
|[27NxweHr9rB1hDCn.htm](book-of-the-dead-bestiary/27NxweHr9rB1hDCn.htm)|Deathless Acolyte Of Urgathoa|Acolyte non-mort d'Urgathoa|libre|
|[3CdslCv2CMP7O12F.htm](book-of-the-dead-bestiary/3CdslCv2CMP7O12F.htm)|Mummified Cat|Chat momifié|libre|
|[3pQvzrGhxO3bUwYT.htm](book-of-the-dead-bestiary/3pQvzrGhxO3bUwYT.htm)|Iruxi Ossature|Carcasse Iruxie|libre|
|[40P14vZZXKLhBD6q.htm](book-of-the-dead-bestiary/40P14vZZXKLhBD6q.htm)|Sykever|Sykévère (Darvakkas)|libre|
|[5ukWvQqK17BQl31O.htm](book-of-the-dead-bestiary/5ukWvQqK17BQl31O.htm)|Lacedon|Lacédon (Goule)|libre|
|[7JWjoMGf7f7PZgSD.htm](book-of-the-dead-bestiary/7JWjoMGf7f7PZgSD.htm)|Ice Mummy|Momie de glace|libre|
|[7pk1DLc96LMcHBB5.htm](book-of-the-dead-bestiary/7pk1DLc96LMcHBB5.htm)|Vanyver|Vanyvère (Darvakkas)|libre|
|[7WqlOvjoqURmeorA.htm](book-of-the-dead-bestiary/7WqlOvjoqURmeorA.htm)|Death Coach|Carrosse de la mort|libre|
|[8jd6xVMgAeQEfect.htm](book-of-the-dead-bestiary/8jd6xVMgAeQEfect.htm)|Nasurgeth|Nasurgeth (Darvakkas)|libre|
|[8o27tdIZFp0eTs5N.htm](book-of-the-dead-bestiary/8o27tdIZFp0eTs5N.htm)|Zombie Lord|Seigneur Zombie|libre|
|[8qB0gj8salw8746I.htm](book-of-the-dead-bestiary/8qB0gj8salw8746I.htm)|Mummy Prophet Of Set|Momie du prophète de Seth|libre|
|[8tX1seLMsXto5Kni.htm](book-of-the-dead-bestiary/8tX1seLMsXto5Kni.htm)|Ectoplasmic Grasp|Poigne ectoplasmique|libre|
|[A5GOni2mXNhVsdRg.htm](book-of-the-dead-bestiary/A5GOni2mXNhVsdRg.htm)|Faithless Ecclesiarch|Écclésiarche infidèle|libre|
|[abnTemoEnsqZPbKZ.htm](book-of-the-dead-bestiary/abnTemoEnsqZPbKZ.htm)|Last Guard|Dernière garde-armée|libre|
|[avBb4LvR3TsYpXma.htm](book-of-the-dead-bestiary/avBb4LvR3TsYpXma.htm)|Queen Sluagh|Reine sluagh|libre|
|[AXp5EzgP7p8FrYpN.htm](book-of-the-dead-bestiary/AXp5EzgP7p8FrYpN.htm)|Tormented (Crushing)|Tourmenté (Écrasé)|libre|
|[ayCK1wCQY7mCCyxh.htm](book-of-the-dead-bestiary/ayCK1wCQY7mCCyxh.htm)|Locking Door|Porte verouillante|libre|
|[bckYmdaq03CUDdc5.htm](book-of-the-dead-bestiary/bckYmdaq03CUDdc5.htm)|Gholdako|Gholdako|libre|
|[c6qRZuHQ7RHJEAtj.htm](book-of-the-dead-bestiary/c6qRZuHQ7RHJEAtj.htm)|Grappling Spirit|Esprit accrocheur|libre|
|[cEJh8SY1YDULyhI9.htm](book-of-the-dead-bestiary/cEJh8SY1YDULyhI9.htm)|Relictner Eroder|Relicteur érodé (Mortique)|libre|
|[Cl1SZNiURw65rz4p.htm](book-of-the-dead-bestiary/Cl1SZNiURw65rz4p.htm)|Silent Stalker|Harceleur silencieux (Revenant)|libre|
|[dcLHA7tihCF2Mraj.htm](book-of-the-dead-bestiary/dcLHA7tihCF2Mraj.htm)|Tormented (Drowning)|Tourmenté (Noyé)|libre|
|[dgQ9VMuC63T5LW5h.htm](book-of-the-dead-bestiary/dgQ9VMuC63T5LW5h.htm)|Weight of Guilt|Poids de la culpabilité|libre|
|[dKkHFA4aBgk82QJO.htm](book-of-the-dead-bestiary/dKkHFA4aBgk82QJO.htm)|Skeletal Soldier|Soldat squelette|libre|
|[dlizffh8cFUFOhUf.htm](book-of-the-dead-bestiary/dlizffh8cFUFOhUf.htm)|Jitterbone Contortionist|Ossateur contorsionniste (Mortique)|libre|
|[DMZo6DXmScDXJpJd.htm](book-of-the-dead-bestiary/DMZo6DXmScDXJpJd.htm)|Hunter Wight|Chasseur nécrophage|libre|
|[DUFaigFhqeKLbrMG.htm](book-of-the-dead-bestiary/DUFaigFhqeKLbrMG.htm)|Grasping Dead|Cadavres agrippants|libre|
|[DVpP5ObHDoT0OULK.htm](book-of-the-dead-bestiary/DVpP5ObHDoT0OULK.htm)|Prowler Wight|Rôdeur nécrophage|libre|
|[e49MDE5dJQ1XFq3O.htm](book-of-the-dead-bestiary/e49MDE5dJQ1XFq3O.htm)|Predatory Rabbit|Lapin prédateur|libre|
|[EDhme1IKgO34NDrt.htm](book-of-the-dead-bestiary/EDhme1IKgO34NDrt.htm)|Festering Gnasher|Mâchouilleur putréfié (Décapité)|libre|
|[eEE3lRhXCjGzlRLJ.htm](book-of-the-dead-bestiary/eEE3lRhXCjGzlRLJ.htm)|Shredskin|Charpeau|libre|
|[eLAoo8l1I3OiibwW.htm](book-of-the-dead-bestiary/eLAoo8l1I3OiibwW.htm)|Cannibalistic Echoes|Échos cannibalistes|libre|
|[Ez8wXPDKOzxvxnqS.htm](book-of-the-dead-bestiary/Ez8wXPDKOzxvxnqS.htm)|Blood-Soaked Soil|Terre imprégnée de sang|libre|
|[f2dAjBXK55w6rnsh.htm](book-of-the-dead-bestiary/f2dAjBXK55w6rnsh.htm)|Tormented (Dislocation)|Tourmenté (Disloqué)|libre|
|[FVXCVh3Y0LdfoIC5.htm](book-of-the-dead-bestiary/FVXCVh3Y0LdfoIC5.htm)|Siphoning Spirit|Esprit siphonant|libre|
|[GabMKY8QJOulyqAr.htm](book-of-the-dead-bestiary/GabMKY8QJOulyqAr.htm)|Ghost Stampede|Cavalcade de fantômes|libre|
|[GcHzyaMYK5QeKUyM.htm](book-of-the-dead-bestiary/GcHzyaMYK5QeKUyM.htm)|Pale Stranger|Étranger pâle (Revenant)|libre|
|[gXo04F7O4pwOY698.htm](book-of-the-dead-bestiary/gXo04F7O4pwOY698.htm)|Gallowdead|Cadavre de potence|libre|
|[h0OtQsMR4OqnYatd.htm](book-of-the-dead-bestiary/h0OtQsMR4OqnYatd.htm)|Deathless Hierophant Of Urgathoa|Hiérophante non-mort d'Urgathoa|libre|
|[hDQmEtisrgRmufUW.htm](book-of-the-dead-bestiary/hDQmEtisrgRmufUW.htm)|Fluxwraith|Spectre du flot temporel|libre|
|[hhoSyH9QthtvFptC.htm](book-of-the-dead-bestiary/hhoSyH9QthtvFptC.htm)|Siabrae|Siabré|libre|
|[hjBcN7ulP6FSK7ie.htm](book-of-the-dead-bestiary/hjBcN7ulP6FSK7ie.htm)|Tormented (Starvation)|Tourmenté (Affamé)|libre|
|[I2XdTFyxnnRdmWsi.htm](book-of-the-dead-bestiary/I2XdTFyxnnRdmWsi.htm)|Toppling Furniture|Renversement de meubles|libre|
|[iEQOUQk1wVHFsajW.htm](book-of-the-dead-bestiary/iEQOUQk1wVHFsajW.htm)|Geist|Hante-esprit|libre|
|[iL2BkoRa6Zrvg1wN.htm](book-of-the-dead-bestiary/iL2BkoRa6Zrvg1wN.htm)|Desperate Hunger|Faim désespérée|libre|
|[ipVQuGff2OeTVwFK.htm](book-of-the-dead-bestiary/ipVQuGff2OeTVwFK.htm)|Skeletal Mage|Mage squelette|libre|
|[IrzECfuPttgGMbLa.htm](book-of-the-dead-bestiary/IrzECfuPttgGMbLa.htm)|Glimpse Grave|Regard sur la tombe|libre|
|[ishwgxZAlNJxNwGE.htm](book-of-the-dead-bestiary/ishwgxZAlNJxNwGE.htm)|Daqqanoenyent|Daqqanoeuyien (Décapité)|libre|
|[j777BjOqZff6S1v9.htm](book-of-the-dead-bestiary/j777BjOqZff6S1v9.htm)|Bhuta|Bhuta|libre|
|[j88xR2MqqZrmF5Wz.htm](book-of-the-dead-bestiary/j88xR2MqqZrmF5Wz.htm)|Raw Nerve|Nerf-vif|libre|
|[JazJz2crkoG9koQR.htm](book-of-the-dead-bestiary/JazJz2crkoG9koQR.htm)|Fallen Champion|Champion déchu (Hérexen)|libre|
|[jIypNMJE7rVYpItG.htm](book-of-the-dead-bestiary/jIypNMJE7rVYpItG.htm)|Fiddling Bones|Os-violon|libre|
|[KhHVStbsPSuPElFI.htm](book-of-the-dead-bestiary/KhHVStbsPSuPElFI.htm)|Excorion|Excorion|libre|
|[kY8MSttryLjbI5wN.htm](book-of-the-dead-bestiary/kY8MSttryLjbI5wN.htm)|Wight Commander|Commandant nécrophage|libre|
|[L0JAq1IGEsjJwTbl.htm](book-of-the-dead-bestiary/L0JAq1IGEsjJwTbl.htm)|Minister Of Tumult|Ministre du tumulte (Vampire Jiang-shi)|libre|
|[laYufBuih3cT95j4.htm](book-of-the-dead-bestiary/laYufBuih3cT95j4.htm)|Hollow Serpent|Serpent creux|libre|
|[lhzNUX83TTYpJuma.htm](book-of-the-dead-bestiary/lhzNUX83TTYpJuma.htm)|Unrisen|Agonisant|libre|
|[LNgHO51cJF4HPL78.htm](book-of-the-dead-bestiary/LNgHO51cJF4HPL78.htm)|Shattered Window|Fenêtre brisée|libre|
|[LWLUaSHl8YCZdDMH.htm](book-of-the-dead-bestiary/LWLUaSHl8YCZdDMH.htm)|Vetalarana Emergent|Vétalarana naissant (Vampire)|libre|
|[M59XiYnJ4Z3bSwCC.htm](book-of-the-dead-bestiary/M59XiYnJ4Z3bSwCC.htm)|Polong|Polong|libre|
|[mDe4WW1pSWXS113j.htm](book-of-the-dead-bestiary/mDe4WW1pSWXS113j.htm)|Flood of Spirits|Flot d'esprits|libre|
|[mL4l2kwPPSMKwzQo.htm](book-of-the-dead-bestiary/mL4l2kwPPSMKwzQo.htm)|Onryo|Onryo|libre|
|[nbVljZhCnWgGxA18.htm](book-of-the-dead-bestiary/nbVljZhCnWgGxA18.htm)|Zombie Owlbear|Hibours zombie|libre|
|[NcqruxA82SFvTnD1.htm](book-of-the-dead-bestiary/NcqruxA82SFvTnD1.htm)|Child of Urgathoa|Enfant de Urgathoa|libre|
|[Nzf3AfA46cBiWCwN.htm](book-of-the-dead-bestiary/Nzf3AfA46cBiWCwN.htm)|Vetalarana Manipulator|Manipulateur vétalarana (Vampire)|libre|
|[ol2lji9lH7PXh1uw.htm](book-of-the-dead-bestiary/ol2lji9lH7PXh1uw.htm)|Bone Croupier|Croupier des osselets|libre|
|[OsIjZpdtKmKrrBID.htm](book-of-the-dead-bestiary/OsIjZpdtKmKrrBID.htm)|Priest Of Kabriri|Prêtre de Kabriri (Goule)|libre|
|[pDA6tCwQ3pE6ji4U.htm](book-of-the-dead-bestiary/pDA6tCwQ3pE6ji4U.htm)|Shadern Immolator|Shadern immolateur (Mortique)|libre|
|[PH4GHPsekbDdyX3j.htm](book-of-the-dead-bestiary/PH4GHPsekbDdyX3j.htm)|Phantom Jailer|Geôlier fantôme|libre|
|[Pum2HQMMrFS89JnW.htm](book-of-the-dead-bestiary/Pum2HQMMrFS89JnW.htm)|Entombed Spirit|Esprit inhumé|libre|
|[pzh6N7lfVk5261CV.htm](book-of-the-dead-bestiary/pzh6N7lfVk5261CV.htm)|Spirit Cyclone|Cyclone d'esprits|libre|
|[qO20so7Mv2pmsLL1.htm](book-of-the-dead-bestiary/qO20so7Mv2pmsLL1.htm)|Husk Zombie|Zombies dépouillés|libre|
|[S5dXAmvghYCIrQDe.htm](book-of-the-dead-bestiary/S5dXAmvghYCIrQDe.htm)|Decrepit Mummy|Momie délabrée|libre|
|[sgeULAtaLvg0Uhyn.htm](book-of-the-dead-bestiary/sgeULAtaLvg0Uhyn.htm)|Cadaverous Rake|Débauché cadavérique|libre|
|[sucEX2JFrVevTNjU.htm](book-of-the-dead-bestiary/sucEX2JFrVevTNjU.htm)|Drake Skeleton|Drake squelette|libre|
|[T9osQMcC96l4X9lk.htm](book-of-the-dead-bestiary/T9osQMcC96l4X9lk.htm)|Beetle Carapace|Scarabée-carapace|libre|
|[tb2nr2ycARadI3Pg.htm](book-of-the-dead-bestiary/tb2nr2ycARadI3Pg.htm)|Iroran Mummy|Momie Iroran|libre|
|[TipfHGlT5ctn1JV0.htm](book-of-the-dead-bestiary/TipfHGlT5ctn1JV0.htm)|Zombie Mammoth|Mamouth zombie|libre|
|[Tjgk2iMSjSbUnuXO.htm](book-of-the-dead-bestiary/Tjgk2iMSjSbUnuXO.htm)|Cold Spot|Point de froid|libre|
|[ToGAEKKOplpXzaQQ.htm](book-of-the-dead-bestiary/ToGAEKKOplpXzaQQ.htm)|Wolf Skeleton|Loup squelette|libre|
|[tqTcM8VqFMyuQ0hY.htm](book-of-the-dead-bestiary/tqTcM8VqFMyuQ0hY.htm)|Final Words|Mots ultimes|libre|
|[uE6YuCCqU715E1Ay.htm](book-of-the-dead-bestiary/uE6YuCCqU715E1Ay.htm)|Bloodthirsty Toy|Jouet sanguinaire|libre|
|[UMDBlaViAdaw2lnN.htm](book-of-the-dead-bestiary/UMDBlaViAdaw2lnN.htm)|Ghost Pirate Captain|Capitaine pirate fantôme|libre|
|[uqY9TkQTO5n9rCLZ.htm](book-of-the-dead-bestiary/uqY9TkQTO5n9rCLZ.htm)|Llorona|Llorona|libre|
|[Ur3dzfmvtN7lyPNG.htm](book-of-the-dead-bestiary/Ur3dzfmvtN7lyPNG.htm)|Taunting Skull|Crâne railleur (Décapité)|libre|
|[uURI8Netd0ytD9vc.htm](book-of-the-dead-bestiary/uURI8Netd0ytD9vc.htm)|Sluagh Reaper|Faucheur sluagh|libre|
|[vaVIJeQnKFUeaC8K.htm](book-of-the-dead-bestiary/vaVIJeQnKFUeaC8K.htm)|Combusted|Incendié|libre|
|[vHEJOONP1ERjuxxl.htm](book-of-the-dead-bestiary/vHEJOONP1ERjuxxl.htm)|Urveth|Urveth (Darvakkas)|libre|
|[VN2Vz1dxA9ti66bC.htm](book-of-the-dead-bestiary/VN2Vz1dxA9ti66bC.htm)|Tormented (Impalement)|Tourmenté (Empalé)|libre|
|[VNego1px6HN8mtUl.htm](book-of-the-dead-bestiary/VNego1px6HN8mtUl.htm)|Phantom Footsteps|Bruits de pas fantôme|libre|
|[vOuiG3tRcr8yL4jh.htm](book-of-the-dead-bestiary/vOuiG3tRcr8yL4jh.htm)|Ghul|Ghul|libre|
|[VQqdG5PKdCFHXquy.htm](book-of-the-dead-bestiary/VQqdG5PKdCFHXquy.htm)|Disembodied Voices|Voix désincarnées|libre|
|[vvxnRuYBU2uCBPPI.htm](book-of-the-dead-bestiary/vvxnRuYBU2uCBPPI.htm)|Little Man In The Woods|Petit homme dans les bois|libre|
|[VYLTAIVA9qUsbujo.htm](book-of-the-dead-bestiary/VYLTAIVA9qUsbujo.htm)|Waldgeist|Waldgeist (Verdorite)|libre|
|[wGEZUOgoJNkgXx9Z.htm](book-of-the-dead-bestiary/wGEZUOgoJNkgXx9Z.htm)|Horde Lich|Liche de la horde|libre|
|[Wv9iS7JacYjymqlY.htm](book-of-the-dead-bestiary/Wv9iS7JacYjymqlY.htm)|Harlo Krant|Harlo Krant|libre|
|[XrSjNpkJkBO6ysRG.htm](book-of-the-dead-bestiary/XrSjNpkJkBO6ysRG.htm)|Gashadokuro|Gashadokuro|libre|
|[XuHQXwAGBdS1s2Kq.htm](book-of-the-dead-bestiary/XuHQXwAGBdS1s2Kq.htm)|Frenetic Musician|Musicien frénétique|libre|
|[xxFiRpQdvbKd0P12.htm](book-of-the-dead-bestiary/xxFiRpQdvbKd0P12.htm)|Hungry Ghost|Fantôme affamé|libre|
|[xyOxzfIUTrN5c8ex.htm](book-of-the-dead-bestiary/xyOxzfIUTrN5c8ex.htm)|Blood Tears|Larmes de sang|libre|
|[Y2Fiu9T1xwSBPVTV.htm](book-of-the-dead-bestiary/Y2Fiu9T1xwSBPVTV.htm)|Corpseroot|Arbre cadavéreux|libre|
|[ypOgr8kPy6KqXAAd.htm](book-of-the-dead-bestiary/ypOgr8kPy6KqXAAd.htm)|Seetangeist|Sitangeist|libre|
|[Yv9e7NSCzPuhlCnH.htm](book-of-the-dead-bestiary/Yv9e7NSCzPuhlCnH.htm)|Graveknight Warmaster|Maître de guerre sépulcre|libre|
|[z3BpUMkUFoXqOFgf.htm](book-of-the-dead-bestiary/z3BpUMkUFoXqOFgf.htm)|Scorned Hound|Mollosse bafoué|libre|
|[z72hPIIjIiLIxnor.htm](book-of-the-dead-bestiary/z72hPIIjIiLIxnor.htm)|Withered|Zombie délabré|libre|
|[z8WJtCyoVHlcSxYy.htm](book-of-the-dead-bestiary/z8WJtCyoVHlcSxYy.htm)|Skeletal Titan|Titan squelette|libre|
|[Ze8SKqa4T8lhSDup.htm](book-of-the-dead-bestiary/Ze8SKqa4T8lhSDup.htm)|Pale Sovereign|Souverain pâle|libre|
|[zOEuwWcvD7sQA1kc.htm](book-of-the-dead-bestiary/zOEuwWcvD7sQA1kc.htm)|Ichor Slinger|Baratineur fétide|libre|
|[zPQZLswJISQrvPb7.htm](book-of-the-dead-bestiary/zPQZLswJISQrvPb7.htm)|Runecarved Lich|Liche runique|libre|
|[zqftTUpxqkdLx2IY.htm](book-of-the-dead-bestiary/zqftTUpxqkdLx2IY.htm)|Ecorche|Écorcheur|libre|
|[ZqZlji7aCGCGATMP.htm](book-of-the-dead-bestiary/ZqZlji7aCGCGATMP.htm)|Zombie Snake|Serpent zombie|libre|
|[ZSrMFLrb5CzFH5WX.htm](book-of-the-dead-bestiary/ZSrMFLrb5CzFH5WX.htm)|Provincial Jiang-shi|Jiang-shi provincial|libre|
|[ZzKfcOf7CWSZIsAE.htm](book-of-the-dead-bestiary/ZzKfcOf7CWSZIsAE.htm)|Violent Shove|Poussée violente|libre|
