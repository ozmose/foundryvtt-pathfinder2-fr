# État de la traduction (spell-effects)

 * **libre**: 337
 * **changé**: 46
 * **officielle**: 16


Dernière mise à jour: 2023-07-12 20:28 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0Cyf07wboRp4CmcQ.htm](spell-effects/0Cyf07wboRp4CmcQ.htm)|Spell Effect: Dinosaur Form (Ankylosaurus)|Effet : Forme de dinosaure (Ankylosaure)|changé|
|[EKdqKCuyWSkpXpyJ.htm](spell-effects/EKdqKCuyWSkpXpyJ.htm)|Spell Effect: Ooze Form (Black Pudding)|Effet : Forme de vase (Poudding noir)|changé|
|[jp88SCE3VCRAyE6x.htm](spell-effects/jp88SCE3VCRAyE6x.htm)|Spell Effect: Element Embodied (Earth)|Effet : Incarnation élémentaire (Terre)|changé|
|[JrNHFNxJayevlv2G.htm](spell-effects/JrNHFNxJayevlv2G.htm)|Spell Effect: Plant Form (Flytrap)|Effet : Forme de plante (Attrape-mouches)|changé|
|[jtW3VfI5Kktuy3GH.htm](spell-effects/jtW3VfI5Kktuy3GH.htm)|Spell Effect: Dragon Form (Bronze)|Effet : Forme de dragon (Bronze)|changé|
|[jvwKRHtOiPAm4uAP.htm](spell-effects/jvwKRHtOiPAm4uAP.htm)|Spell Effect: Aerial Form (Bat)|Effet : Forme aérienne (Chauve-souris)|changé|
|[KcBqo33ekJHxZLHo.htm](spell-effects/KcBqo33ekJHxZLHo.htm)|Spell Effect: Fey Form (Naiad)|Effet : Forme de fée (Naïade)|changé|
|[KkDRRDuycXwKPa6n.htm](spell-effects/KkDRRDuycXwKPa6n.htm)|Spell Effect: Dinosaur Form (Brontosaurus)|Effet : Forme de dinosaure (Brontosaure)|changé|
|[kxMBdANwCcF841uA.htm](spell-effects/kxMBdANwCcF841uA.htm)|Spell Effect: Elemental Form (Water)|Effet : Forme élémentaire (Eau)|changé|
|[kz3mlFwb9tV9bFwu.htm](spell-effects/kz3mlFwb9tV9bFwu.htm)|Spell Effect: Animal Form (Snake)|Effet : Forme animale (Serpent)|changé|
|[llrOM8rPP9nxIuEN.htm](spell-effects/llrOM8rPP9nxIuEN.htm)|Spell Effect: Insect Form (Mantis)|Effet : Forme d'insecte (Mante)|changé|
|[lmAwCy7isFvLYdGd.htm](spell-effects/lmAwCy7isFvLYdGd.htm)|Spell Effect: Element Embodied (Fire)|Effet : Incarnation élémentaire (Feu)|changé|
|[lywUJljQy2SxRZQt.htm](spell-effects/lywUJljQy2SxRZQt.htm)|Spell Effect: Nature Incarnate (Green Man)|Effet : Incarnation de la nature (Homme vert)|changé|
|[nWEx5kpkE8YlBZvy.htm](spell-effects/nWEx5kpkE8YlBZvy.htm)|Spell Effect: Dragon Form (Green)|Effet : Forme de dragon (Vert)|changé|
|[OeCn76SB92GPOZwr.htm](spell-effects/OeCn76SB92GPOZwr.htm)|Spell Effect: Dragon Form (Brass)|Effet : Forme de dragon (Airain)|changé|
|[oJbcmpBSHwmx6FD4.htm](spell-effects/oJbcmpBSHwmx6FD4.htm)|Spell Effect: Dinosaur Form (Deinonychus)|Effet : Forme de dinosaure (Deinonychus)|changé|
|[phIoucsDa3iplMm2.htm](spell-effects/phIoucsDa3iplMm2.htm)|Spell Effect: Elemental Form (Fire)|Effet : Forme élémentaire (Feu)|changé|
|[PpkOZVoHkBZUmddx.htm](spell-effects/PpkOZVoHkBZUmddx.htm)|Spell Effect: Ooze Form (Ochre Jelly)|Effet : Forme de vase (Gelée ocre)|changé|
|[ptOqsN5FS0nQh7RW.htm](spell-effects/ptOqsN5FS0nQh7RW.htm)|Spell Effect: Animal Form (Cat)|Effet : Forme animale (Félin)|changé|
|[qPaEEhczUWCQo6ux.htm](spell-effects/qPaEEhczUWCQo6ux.htm)|Spell Effect: Animal Form (Shark)|Effet : Forme animale (Requin)|changé|
|[rEsgDhunQ5Yx8KZx.htm](spell-effects/rEsgDhunQ5Yx8KZx.htm)|Spell Effect: Monstrosity Form (Purple Worm)|Effet : Forme monstrueuse (Ver pourpre)|changé|
|[rHXOZAFBdRXIlxt5.htm](spell-effects/rHXOZAFBdRXIlxt5.htm)|Spell Effect: Dragon Form (Black)|Effet : Forme de dragon (Noir)|changé|
|[rTVZ0zwiKeslRw6p.htm](spell-effects/rTVZ0zwiKeslRw6p.htm)|Spell Effect: Wild Morph|Effet : Métamorphose sauvage|changé|
|[S75DOLjKaSJGMc0D.htm](spell-effects/S75DOLjKaSJGMc0D.htm)|Spell Effect: Fey Form (Elananx)|Effet : Forme de fée (Élananxe)|changé|
|[sccNh8j1PKLHCKh1.htm](spell-effects/sccNh8j1PKLHCKh1.htm)|Spell Effect: Angel Form (Choral)|Effet : Forme d'ange (Choral)|changé|
|[ScF0ECWnfXMHYLDL.htm](spell-effects/ScF0ECWnfXMHYLDL.htm)|Spell Effect: Daemon Form (Leukodaemon)|Effet : Forme de daémon (Leukodaémon)|changé|
|[sfJyQKmoxSRo6FyP.htm](spell-effects/sfJyQKmoxSRo6FyP.htm)|Spell Effect: Aberrant Form (Gug)|Effet : Forme d'aberration (Gug)|changé|
|[SjfDoeymtnYKoGUD.htm](spell-effects/SjfDoeymtnYKoGUD.htm)|Spell Effect: Aberrant Form (Otyugh)|Effet : Forme d'aberration (Otyugh)|changé|
|[sN3mQ7YrPBogEJRn.htm](spell-effects/sN3mQ7YrPBogEJRn.htm)|Spell Effect: Animal Form (Canine)|Effet : Forme animale (Canidé)|changé|
|[sXe7cPazOJbX41GU.htm](spell-effects/sXe7cPazOJbX41GU.htm)|Spell Effect: Demon Form (Hezrou)|Effet : Forme de démon (Hezrou)|changé|
|[T6XnxvsgvvOrpien.htm](spell-effects/T6XnxvsgvvOrpien.htm)|Spell Effect: Dinosaur Form (Stegosaurus)|Effet : Forme de Dinosaure (Stégosaure)|changé|
|[tfdDpf9xSWgQer5g.htm](spell-effects/tfdDpf9xSWgQer5g.htm)|Spell Effect: Cosmic Form (Moon)|Effet : Forme cosmique (Lune)|changé|
|[tk3go5Cl6Qt130Dk.htm](spell-effects/tk3go5Cl6Qt130Dk.htm)|Spell Effect: Animal Form (Ape)|Effet : Forme animale (Singe)|changé|
|[tu8FyCtmL3YYR2jL.htm](spell-effects/tu8FyCtmL3YYR2jL.htm)|Spell Effect: Plant Form (Arboreal)|Effet : Forme de plante (Arboréen)|changé|
|[TUyEeLyqdJL6PwbH.htm](spell-effects/TUyEeLyqdJL6PwbH.htm)|Spell Effect: Dragon Form (Silver)|Effet : Forme de dragon (Argent)|changé|
|[U2eD42cGwdvQMdN0.htm](spell-effects/U2eD42cGwdvQMdN0.htm)|Spell Effect: Fiery Body (9th Level)|Effet : Corps enflammé (Niveau 9)|changé|
|[uIMaMzd6pcKmMNPJ.htm](spell-effects/uIMaMzd6pcKmMNPJ.htm)|Spell Effect: Fiery Body|Effet : Corps enflammé|changé|
|[UjoNm3lrhlg4ctAQ.htm](spell-effects/UjoNm3lrhlg4ctAQ.htm)|Spell Effect: Aerial Form (Pterosaur)|Effet : Forme aérienne (Ptérosaure)|changé|
|[V4a9pZHNUlddAwTA.htm](spell-effects/V4a9pZHNUlddAwTA.htm)|Spell Effect: Dragon Form (Red)|Effet : Forme de dragon (Rouge)|changé|
|[w1HwO7huxJoK0gHY.htm](spell-effects/w1HwO7huxJoK0gHY.htm)|Spell Effect: Element Embodied (Water)|Effet : Incarnation élémentaire (Eau)|changé|
|[WEpgIGFwtRb3ef1x.htm](spell-effects/WEpgIGFwtRb3ef1x.htm)|Spell Effect: Angel Form (Balisse)|Effet : Forme d'ange (Balisse)|changé|
|[X1kkbRrh4zJuDGjl.htm](spell-effects/X1kkbRrh4zJuDGjl.htm)|Spell Effect: Demon Form (Babau)|Effet : Forme de démon (Babau)|changé|
|[xgZxYqjDPNtsQ3Qp.htm](spell-effects/xgZxYqjDPNtsQ3Qp.htm)|Spell Effect: Aerial Form (Wasp)|Effet : Forme aérienne (Guêpe)|changé|
|[xsy1yaCj0SVsn502.htm](spell-effects/xsy1yaCj0SVsn502.htm)|Spell Effect: Aberrant Form (Chuul)|Effet : Forme d'aberration (Chuul)|changé|
|[ydsLEGjY89Akc4oZ.htm](spell-effects/ydsLEGjY89Akc4oZ.htm)|Spell Effect: Pest Form|Effet : Forme de nuisible|changé|
|[zIRnnuj4lARq43DA.htm](spell-effects/zIRnnuj4lARq43DA.htm)|Spell Effect: Daemon Form (Meladaemon)|Effet : Forme de daémon (Méladaémon)|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[06zdFoxzuTpPPGyJ.htm](spell-effects/06zdFoxzuTpPPGyJ.htm)|Spell Effect: Rejuvenating Flames|Effet : Flammes rajeunissantes|libre|
|[0bfqYkNaWsdTmtrc.htm](spell-effects/0bfqYkNaWsdTmtrc.htm)|Spell Effect: Juvenile Companion|Effet : Compagnon juvénile|libre|
|[0gv9D5RlrF5cKA3I.htm](spell-effects/0gv9D5RlrF5cKA3I.htm)|Spell Effect: Adapt Self (Darkvision)|Effet : Adaptation de soi (Vision dans le noir)|libre|
|[0o984LjzIFXxeXIF.htm](spell-effects/0o984LjzIFXxeXIF.htm)|Spell Effect: Evolution Surge (Amphibious)|Effet : Flux d'évolution (Amphibie)|libre|
|[0OC945wcZ4H4akLz.htm](spell-effects/0OC945wcZ4H4akLz.htm)|Spell Effect: Summoner's Visage|Effet : Visage du conjurateur|libre|
|[0PO5mFRhh9HxGAtv.htm](spell-effects/0PO5mFRhh9HxGAtv.htm)|Spell Effect: Mirror Image|Effet : Image miroir|libre|
|[0q2716S34XL1y9Hh.htm](spell-effects/0q2716S34XL1y9Hh.htm)|Spell Effect: Rapid Adaptation (Underground)|Effet : Adaptation rapide (Souterrain)|libre|
|[0QVufU5o3xIxiHmP.htm](spell-effects/0QVufU5o3xIxiHmP.htm)|Spell Effect: Aerial Form (Bird)|Effet : Forme aérienne (Oiseau)|libre|
|[0R42NyuEZMVALjQs.htm](spell-effects/0R42NyuEZMVALjQs.htm)|Spell Effect: Traveler's Transit (Swim)|Effet : Voyageur en transit (Nage)|libre|
|[0s6YaL3IjqECmjab.htm](spell-effects/0s6YaL3IjqECmjab.htm)|Spell Effect: Roar of the Wyrm|Effet : Rugissement du Ver|libre|
|[0W87OkYi3qCwNGSj.htm](spell-effects/0W87OkYi3qCwNGSj.htm)|Spell Effect: Daemon Form (Piscodaemon)|Effet : Forme de daémon (Piscodaémon)|libre|
|[0yy4t4UY1HqrEo70.htm](spell-effects/0yy4t4UY1HqrEo70.htm)|Spell Effect: Devil Form (Barbazu)|Effet : Forme de diable (Barbazu)|libre|
|[14AFzcwkN019dzcl.htm](spell-effects/14AFzcwkN019dzcl.htm)|Spell Effect: Shifting Form (Claws)|Effet : Forme changeant (Griffes)|libre|
|[14m4s0FeRSqRlHwL.htm](spell-effects/14m4s0FeRSqRlHwL.htm)|Spell Effect: Arcane Countermeasure|Effet : Contremesure arcanique|libre|
|[16QrxIYal7PJFL2W.htm](spell-effects/16QrxIYal7PJFL2W.htm)|Spell Effect: Euphoric Renewal|Effet : Renouveau euphorique|libre|
|[1gTnFmfLUQnCo3TK.htm](spell-effects/1gTnFmfLUQnCo3TK.htm)|Spell Effect: Dread Ambience (Success)|Effet : Ambiance terrifiante (Succès)|libre|
|[1kelGCsoXyGRqMd9.htm](spell-effects/1kelGCsoXyGRqMd9.htm)|Spell Effect: Diabolic Edict|Effet : Édit diabolique|officielle|
|[1n84AqLtsdT8I64W.htm](spell-effects/1n84AqLtsdT8I64W.htm)|Spell Effect: Daemon Form (Ceustodaemon)|Effet : Forme de daémon (Ceustodaémon)|libre|
|[1RsScTvNdGD9zGWe.htm](spell-effects/1RsScTvNdGD9zGWe.htm)|Spell Effect: Fire Shield|Effet : Bouclier de feu|officielle|
|[1VuHjj32wge2gPOr.htm](spell-effects/1VuHjj32wge2gPOr.htm)|Spell Effect: Animal Feature (Wings)|Effet : Trait animal (Ailes)|libre|
|[1XlF12UbvJsTZxfp.htm](spell-effects/1XlF12UbvJsTZxfp.htm)|Spell Effect: Evolution Surge|Effet : Flux d'évolution|libre|
|[20egTKICPMhibqgn.htm](spell-effects/20egTKICPMhibqgn.htm)|Spell Effect: Demon Form (Vrock)|Effet : Forme de démon (Vrock)|libre|
|[28NvrpZmELvyrHUt.htm](spell-effects/28NvrpZmELvyrHUt.htm)|Spell Effect: Variable Gravity (High Gravity)|Effet : Gravité variable (Gravité élevée)|libre|
|[2KQSsrzUqAxSXOdd.htm](spell-effects/2KQSsrzUqAxSXOdd.htm)|Spell Effect: Dancing Shield|Effet : Bouclier dansant|libre|
|[2sMXAGZfdqiy10kk.htm](spell-effects/2sMXAGZfdqiy10kk.htm)|Spell Effect: Clinging Ice|Effet : Glace tenace|libre|
|[2Ss5VblfZNHg1HjN.htm](spell-effects/2Ss5VblfZNHg1HjN.htm)|Spell Effect: Cosmic Form (Sun)|Effet : Forme cosmique (Soleil)|libre|
|[2SWUzp4JuNK5EX0J.htm](spell-effects/2SWUzp4JuNK5EX0J.htm)|Spell Effect: Adapt Self (Swim)|Effet : Adaptation de soi (Nage)|libre|
|[2wfrhRLmmgPSKbAZ.htm](spell-effects/2wfrhRLmmgPSKbAZ.htm)|Spell Effect: Animal Feature (Claws)|Effet : Trait animal (Griffes)|libre|
|[3hDKcbhn0j6DsRgm.htm](spell-effects/3hDKcbhn0j6DsRgm.htm)|Spell Effect: Touch of the Moon|Effet : Contact lunaire|libre|
|[3HEiYVhqypfc4IsP.htm](spell-effects/3HEiYVhqypfc4IsP.htm)|Spell Effect: Safeguard Secret|Effet : Secret bien gardé|officielle|
|[3Ktyd5F9lOPo4myk.htm](spell-effects/3Ktyd5F9lOPo4myk.htm)|Spell Effect: Illusory Disguise|Effet : Déguisement illusoire|libre|
|[3LyOkV25p7wA181H.htm](spell-effects/3LyOkV25p7wA181H.htm)|Effect: Guidance Immunity|Effet : Immunité à Assistance divine|officielle|
|[3qHKBDF7lrHw8jFK.htm](spell-effects/3qHKBDF7lrHw8jFK.htm)|Spell Effect: Guidance|Effet : Assistance divine|officielle|
|[3vWfew0TIrcGRjLZ.htm](spell-effects/3vWfew0TIrcGRjLZ.htm)|Spell Effect: Angel Form (Monadic Deva)|Effet : Forme d'ange (Deva monadique)|libre|
|[3zdBGENpmaze1bpq.htm](spell-effects/3zdBGENpmaze1bpq.htm)|Spell Effect: Ooze Form (Gelatinous Cube)|Effet : Forme de vase (Cube gélatineux)|libre|
|[41WThj17MZBXTO2X.htm](spell-effects/41WThj17MZBXTO2X.htm)|Spell Effect: Enlarge (Heightened 4th)|Effet : Agrandissement (Intensifié 4e)|libre|
|[46vCC77mBNBWtmx3.htm](spell-effects/46vCC77mBNBWtmx3.htm)|Spell Effect: Litany of Righteousness|Effet : Litanie de vertu|libre|
|[4ag0OHKfjROmR4Pm.htm](spell-effects/4ag0OHKfjROmR4Pm.htm)|Spell Effect: Anticipate Peril|Effet : Anticipation du danger|libre|
|[4dnt1P2SfcePzkrF.htm](spell-effects/4dnt1P2SfcePzkrF.htm)|Spell Effect: Incendiary Ashes (Success)|Effet : Cendres incendiaires (Succès)|libre|
|[4FD4vJqVpuuJjk9Q.htm](spell-effects/4FD4vJqVpuuJjk9Q.htm)|Spell Effect: Mind Swap (Critical Success)|Effet : Échange d'esprit (Succès critique)|libre|
|[4iakL7fDcZ8RT6Tu.htm](spell-effects/4iakL7fDcZ8RT6Tu.htm)|Spell Effect: Face in the Crowd|Effet : Fondu dans la foule|libre|
|[4ktNx3cVz5GkcGJa.htm](spell-effects/4ktNx3cVz5GkcGJa.htm)|Spell Effect: Untwisting Iron Augmentation|Effet : Amélioration du fer qui ne plie pas|libre|
|[4Lo2qb5PmavSsLNk.htm](spell-effects/4Lo2qb5PmavSsLNk.htm)|Spell Effect: Energy Aegis|Effet : Égide énergétique|libre|
|[4Lt69zSeYElEfDrZ.htm](spell-effects/4Lt69zSeYElEfDrZ.htm)|Spell Effect: Dread Ambience (Critical Failure)|Effet : Ambiance terrifiante (Échec critique)|libre|
|[4vlorajqpFcS5Ozi.htm](spell-effects/4vlorajqpFcS5Ozi.htm)|Spell Effect: Flashy Disappearance|Effet : Disparition spectaculaire|libre|
|[4VOZP2ArmS12nvz8.htm](spell-effects/4VOZP2ArmS12nvz8.htm)|Spell Effect: Draw Ire (Critical Failure)|Effet : Attirer la colère (Échec critique)|libre|
|[542Keo6txtq7uvqe.htm](spell-effects/542Keo6txtq7uvqe.htm)|Spell Effect: Dinosaur Form (Tyrannosaurus)|Effet : Forme de dinosaure (Tyrannosaure)|libre|
|[57lnrCzGUcNUBP2O.htm](spell-effects/57lnrCzGUcNUBP2O.htm)|Spell Effect: Athletic Rush|Effet : Athlétisme poussé|libre|
|[5MI2c9IgxfSeGZQo.htm](spell-effects/5MI2c9IgxfSeGZQo.htm)|Spell Effect: Wind Jump|Effet : Saut du vent|libre|
|[5p3bKvWsJgo83FS1.htm](spell-effects/5p3bKvWsJgo83FS1.htm)|Aura: Protective Ward|Aura : Champ protecteur|libre|
|[5R3ewWLFkgqTvZsc.htm](spell-effects/5R3ewWLFkgqTvZsc.htm)|Spell Effect: Elemental Gift (Fire)|Effet : Don élémentaire (Feu)|libre|
|[5xCheSMgtQhQZm00.htm](spell-effects/5xCheSMgtQhQZm00.htm)|Spell Effect: Garden of Death (Critical Success)|Effet : Jardin mortel (Succès critique)|libre|
|[5yCL7InrJDHpaQjz.htm](spell-effects/5yCL7InrJDHpaQjz.htm)|Spell Effect: Ant Haul|Effet : Charge de fourmi|libre|
|[6BjslHgY01cNbKp5.htm](spell-effects/6BjslHgY01cNbKp5.htm)|Spell Effect: Armor of Bones|Effet : Armure d'os|libre|
|[6embuvXCpS3YOD5u.htm](spell-effects/6embuvXCpS3YOD5u.htm)|Spell Effect: Resilient Touch|Effet : Toucher de résilience|libre|
|[6Ev2ytJZfr2t33iy.htm](spell-effects/6Ev2ytJZfr2t33iy.htm)|Spell Effect: Evolution Surge (Sight)|Effet : Flux d'évolution (Vision)|libre|
|[6GAztnHuQSwAp1k1.htm](spell-effects/6GAztnHuQSwAp1k1.htm)|Spell Effect: Adaptive Ablation|Effet : Dissonnance adaptative|libre|
|[6IvTWcispcDaw88N.htm](spell-effects/6IvTWcispcDaw88N.htm)|Spell Effect: Insect Form (Ant)|Effet : Forme d'insecte (Fourmi)|libre|
|[6qvLnIkWAoGvTIWy.htm](spell-effects/6qvLnIkWAoGvTIWy.htm)|Spell Effect: Community Repair (Critical Failure)|Effet : Réparation communautaire (Échec critique)|libre|
|[6SG8wVmppv4oXZtx.htm](spell-effects/6SG8wVmppv4oXZtx.htm)|Spell Effect: Mantle of the Magma Heart (Fiery Grasp)|Effet : Manteau du coeur magmatique (Poigne enflammée)|libre|
|[70qdCBokXBvKIUIQ.htm](spell-effects/70qdCBokXBvKIUIQ.htm)|Spell Effect: Vision of Weakness|Effet : Vision de faiblesse|libre|
|[782NyomkDHyfsUn6.htm](spell-effects/782NyomkDHyfsUn6.htm)|Spell Effect: Insect Form (Spider)|Effet : Forme d'insecte (Araignée)|libre|
|[7tfF8ifVvOKNud8t.htm](spell-effects/7tfF8ifVvOKNud8t.htm)|Spell Effect: Ooze Form (Gray Ooze)|Effet : Forme de vase (Vase grise)|libre|
|[7tYv9lY3ksSUny2h.htm](spell-effects/7tYv9lY3ksSUny2h.htm)|Spell Effect: Gray Shadow|Effet : Ombre grise|libre|
|[7vIUF5zbvHzVcJA0.htm](spell-effects/7vIUF5zbvHzVcJA0.htm)|Spell Effect: Longstrider (8 hours)|Effet : Grande foulée (8 heures)|libre|
|[7zJPd2BsFl82qFRV.htm](spell-effects/7zJPd2BsFl82qFRV.htm)|Spell Effect: Warding Aggression (+3)|Effet : Agression protectrice (+3)|libre|
|[7zy4W2RXQiMEr6cp.htm](spell-effects/7zy4W2RXQiMEr6cp.htm)|Spell Effect: Dragon Claws|Effet : Griffes de dragon|officielle|
|[81TfqzTfIqkQA4Dy.htm](spell-effects/81TfqzTfIqkQA4Dy.htm)|Spell Effect: Thundering Dominance|Effet : Domination tonitruante|libre|
|[8adLKKzJy49USYJt.htm](spell-effects/8adLKKzJy49USYJt.htm)|Spell Effect: Song of Strength|Effet : Chanson de force|libre|
|[8aNZhlkzRTRKlKag.htm](spell-effects/8aNZhlkzRTRKlKag.htm)|Spell Effect: Dragon Form (Gold)|Effet : Forme de dragon (Or)|libre|
|[8ecGfjmxnBY3WWao.htm](spell-effects/8ecGfjmxnBY3WWao.htm)|Spell Effect: Thicket of Knives|Effet : Multitude de couteaux|libre|
|[8eWLR0WCf5258z8X.htm](spell-effects/8eWLR0WCf5258z8X.htm)|Spell Effect: Elemental Form (Earth)|Effet : Forme élémentaire (Terre)|libre|
|[8gqb5FMTaArsKdWB.htm](spell-effects/8gqb5FMTaArsKdWB.htm)|Spell Effect: Prismatic Armor|Effet : Armure prismatique|libre|
|[8GUkKvCeI0xljCOk.htm](spell-effects/8GUkKvCeI0xljCOk.htm)|Spell Effect: Stormwind Flight|Effet : Vol de l'ouragan|libre|
|[8olfnTmWh0GGPDqX.htm](spell-effects/8olfnTmWh0GGPDqX.htm)|Spell Effect: Ki Strike|Effet : Frappe ki|libre|
|[8wCVSzWYcURWewbd.htm](spell-effects/8wCVSzWYcURWewbd.htm)|Spell Effect: Bestial Curse (Failure)|Effet : Malédiction bestiale (Échec)|libre|
|[8XaSpienzVXLmcfp.htm](spell-effects/8XaSpienzVXLmcfp.htm)|Spell Effect: Inspire Heroics (Strength, +3)|Effet : Inspiration héroïque (Force, +3)|libre|
|[9yzlmYUdvdQshTDF.htm](spell-effects/9yzlmYUdvdQshTDF.htm)|Spell Effect: Bullhorn|Effet : Porte-voix|libre|
|[9ZIP6gWSp9OTEu8i.htm](spell-effects/9ZIP6gWSp9OTEu8i.htm)|Spell Effect: Pocket Library|Effet : Bibliothèque de poche|libre|
|[a3uZckqOY9zQWzZ2.htm](spell-effects/a3uZckqOY9zQWzZ2.htm)|Spell Effect: Read the Air|Effet : Lire l'ambiance|libre|
|[A48jNUOAmCljx8Ru.htm](spell-effects/A48jNUOAmCljx8Ru.htm)|Spell Effect: Shifting Form (Darkvision)|Effet : Forme changeante (Vision dans le noir)|libre|
|[a5rWrWwuevTzs9Io.htm](spell-effects/a5rWrWwuevTzs9Io.htm)|Spell Effect: Wild Shape|Effet : Morphologie sauvage|libre|
|[A61eVVVyUuaUl3tz.htm](spell-effects/A61eVVVyUuaUl3tz.htm)|Spell Effect: Celestial Brand|Effet : Marque céleste|libre|
|[aDOL3OAEWf3ka9oT.htm](spell-effects/aDOL3OAEWf3ka9oT.htm)|Spell Effect: Blood Ward|Effet : Protection du sang|libre|
|[afJCG4vC5WF5h5IB.htm](spell-effects/afJCG4vC5WF5h5IB.htm)|Spell Effect: Clawsong (Damage Increase D8)|Effet: Chant de la griffe (Augmentation des dégâts d8)|libre|
|[AJkRUIdYLnt4QOOg.htm](spell-effects/AJkRUIdYLnt4QOOg.htm)|Spell Effect: Tempt Fate|Effet : Tenter le sort|officielle|
|[alyNtkHLNnt98Ewz.htm](spell-effects/alyNtkHLNnt98Ewz.htm)|Spell Effect: Accelerating Touch|Effet : Contact accélérant|libre|
|[AM49w68oKykc2fHI.htm](spell-effects/AM49w68oKykc2fHI.htm)|Spell Effect: Rapid Adaptation (Plains)|Effet : Adaptation rapide (Plaines)|libre|
|[amTa9jSml9ioKduN.htm](spell-effects/amTa9jSml9ioKduN.htm)|Spell Effect: Insect Form (Beetle)|Effet : Forme d'insecte (Coléoptère)|libre|
|[an4yZ6dyIDOFa1wa.htm](spell-effects/an4yZ6dyIDOFa1wa.htm)|Spell Effect: Soothing Words|Effet : Paroles apaisantes|libre|
|[AnawxScxqUiRuGTm.htm](spell-effects/AnawxScxqUiRuGTm.htm)|Spell Effect: Divine Vessel 9th level (Evil)|Effet : Réceptacle divin Niveau 9 (Mauvais)|libre|
|[AnCRD7kDcG0DDGKn.htm](spell-effects/AnCRD7kDcG0DDGKn.htm)|Spell Effect: Fungal Infestation (Failure)|Effet : Infestation fongique(Échec)|libre|
|[b5OyBdc0bolgWZZT.htm](spell-effects/b5OyBdc0bolgWZZT.htm)|Spell Effect: Tempest Form (Air)|Effet : Forme tempêtueuse (Air)|libre|
|[b8bfWIICHOsGVzjp.htm](spell-effects/b8bfWIICHOsGVzjp.htm)|Spell Effect: Monstrosity Form (Phoenix)|Effet : Forme monstrueuse (Phénix)|libre|
|[b8JCq6n1STl3Wkwy.htm](spell-effects/b8JCq6n1STl3Wkwy.htm)|Spell Effect: Illusory Shroud|Effet : Voile illusoire|libre|
|[bBD7HFzBPlSxYrtW.htm](spell-effects/bBD7HFzBPlSxYrtW.htm)|Spell Effect: Wish-Twisted Form (Failure)|Effet : Forme déformée par un souhait (échec)|libre|
|[Bc2Bwuan3716eAyY.htm](spell-effects/Bc2Bwuan3716eAyY.htm)|Spell Effect: Font of Serenity|Effet : Source de sérénité|libre|
|[Bd86oAvK3RLN076H.htm](spell-effects/Bd86oAvK3RLN076H.htm)|Spell Effect: Angel Form (Movanic Deva)|Effet : Forme d'ange (Deva movanique)|libre|
|[BDMEqBsumguTrMXa.htm](spell-effects/BDMEqBsumguTrMXa.htm)|Spell Effect: Devil Form (Erinys)|Effet : Forme de diable (Érinye)|libre|
|[beReeFroAx24hj83.htm](spell-effects/beReeFroAx24hj83.htm)|Spell Effect: Inspire Courage|Effet : Inspiration Vaillante|libre|
|[BfaFe1cI9IkpvmmY.htm](spell-effects/BfaFe1cI9IkpvmmY.htm)|Spell Effect: Countless Eyes|Effet : Yeux innombrables|libre|
|[BKam63zT98iWMJH7.htm](spell-effects/BKam63zT98iWMJH7.htm)|Spell Effect: Inspire Heroics (Defense, +3)|Effet : Inspiration héroïque (Défense, +3)|libre|
|[blBXnWb1Y8q8YYMh.htm](spell-effects/blBXnWb1Y8q8YYMh.htm)|Spell Effect: Primal Summons (Fire)|Effet : Convocations primordiales (Feu)|libre|
|[bOjuEX3qj7XAOoDF.htm](spell-effects/bOjuEX3qj7XAOoDF.htm)|Spell Effect: Insect Form (Scorpion)|Effet : Forme d'insecte (Scorpion)|libre|
|[BsGZdgiEElNlgZVv.htm](spell-effects/BsGZdgiEElNlgZVv.htm)|Spell Effect: Draw Ire (Failure)|Effet : Attirer la colère (Échec)|libre|
|[BT1ofB6RvRocQOWO.htm](spell-effects/BT1ofB6RvRocQOWO.htm)|Spell Effect: Animal Form (Bull)|Effet : Forme animale (Taureau)|libre|
|[buXx8Azr4BYWPtFg.htm](spell-effects/buXx8Azr4BYWPtFg.htm)|Spell Effect: Blood Vendetta (Failure)|Effet : Vendetta du sang (Échec)|libre|
|[byXkHIKFwuKrZ55M.htm](spell-effects/byXkHIKFwuKrZ55M.htm)|Spell Effect: Shifting Form (Scent)|Effet : Forme changeante (Odorat)|libre|
|[C1GAlZ6Gmmw0QIJN.htm](spell-effects/C1GAlZ6Gmmw0QIJN.htm)|Spell Effect: Hymn of Healing|Effet : Hymne de guérison|libre|
|[C3RdbEQTvawqKAhw.htm](spell-effects/C3RdbEQTvawqKAhw.htm)|Spell Effect: Tempest Form (Water)|Effet : Forme tempêtueuse (Eau)|libre|
|[c4cIfS2974nUJDPt.htm](spell-effects/c4cIfS2974nUJDPt.htm)|Spell Effect: Fey Form (Dryad)|Effet : Forme de fée (dryade)|libre|
|[ceEA7nBGNmoR8Sjj.htm](spell-effects/ceEA7nBGNmoR8Sjj.htm)|Spell Effect: Litany of Self-Interest|Effet : Litanie d'égoïsme|libre|
|[Chol7ExtoN2T36mP.htm](spell-effects/Chol7ExtoN2T36mP.htm)|Spell Effect: Inspire Heroics (Defense, +2)|Effet : Inspiration héroïque (Défense, +2)|libre|
|[con2Hzt47JjpuUej.htm](spell-effects/con2Hzt47JjpuUej.htm)|Spell Effect: Resist Energy|Effet : Résistance à l'énergie|libre|
|[cSoL5aMy3PCzM4Yv.htm](spell-effects/cSoL5aMy3PCzM4Yv.htm)|Spell Effect: Return the Favor|Effet : Rendre la faveur|libre|
|[cTBYHfiXDOA09G4b.htm](spell-effects/cTBYHfiXDOA09G4b.htm)|Spell Effect: Traveler's Transit (Fly)|Effet : Voyageur en transit (Vol)|libre|
|[CTdEsMIwVYqqkH50.htm](spell-effects/CTdEsMIwVYqqkH50.htm)|Spell Effect: Litany of Depravity|Effet : Litanie de dépravation|libre|
|[ctMxYPGEpstvhW9C.htm](spell-effects/ctMxYPGEpstvhW9C.htm)|Spell Effect: Forbidding Ward|Effet : Sceau d'interdiction|libre|
|[cVVZXNbV0nElVOPZ.htm](spell-effects/cVVZXNbV0nElVOPZ.htm)|Spell Effect: Light|Effet : Lumière|libre|
|[CWC2fPmlgixoIKy5.htm](spell-effects/CWC2fPmlgixoIKy5.htm)|Spell Effect: Clawsong (Damage Increase D6)|Effet : Chant de la griffe (Augmentation des dégâts d6)|libre|
|[cwetyC5o4dRyFWJZ.htm](spell-effects/cwetyC5o4dRyFWJZ.htm)|Spell Effect: Necromancer's Generosity|Effet : Générosité du nécromant|libre|
|[czteoX2cggQzfkK9.htm](spell-effects/czteoX2cggQzfkK9.htm)|Spell Effect: Evolution Surge (Climb)|Effet : Flux d'évolution (Escalade)|libre|
|[D0Qj5tC1hGUjzQc4.htm](spell-effects/D0Qj5tC1hGUjzQc4.htm)|Spell Effect: Elemental Motion (Water)|Effet : Mobilité élémentaire (Eau)|libre|
|[DBaMtFHRPEg1JeLs.htm](spell-effects/DBaMtFHRPEg1JeLs.htm)|Spell Effect: Mind Blank|Effet : Esprit impénétrable|libre|
|[dCQCzapIk53xmDo5.htm](spell-effects/dCQCzapIk53xmDo5.htm)|Spell Effect: Animal Feature (Cat Eyes)|Effet : Trait animal (Yeux de chats)|libre|
|[deG1dtfuQph03Kkg.htm](spell-effects/deG1dtfuQph03Kkg.htm)|Spell Effect: Shillelagh|Effet : Gourdin magique|libre|
|[DENMzySYANjUBs4O.htm](spell-effects/DENMzySYANjUBs4O.htm)|Spell Effect: Insect Form (Centipede)|Effet : Forme d'insecte (Mille-pattes)|libre|
|[dEsaufFnfYihu5Ex.htm](spell-effects/dEsaufFnfYihu5Ex.htm)|Spell Effect: Discern Secrets (Sense Motive)|Effet : Discerner les secrets (Deviner les intentions)|libre|
|[DHYWmMGmKOpRSqza.htm](spell-effects/DHYWmMGmKOpRSqza.htm)|Spell Effect: Chromatic Armor|Effet : Armure chromatique|libre|
|[dIftJU6Ki2QSLCOD.htm](spell-effects/dIftJU6Ki2QSLCOD.htm)|Spell Effect: Divine Vessel (9th level)|Effet : Réceptacle divin Niveau 9|libre|
|[DliizYpHcmBG130w.htm](spell-effects/DliizYpHcmBG130w.htm)|Spell Effect: Elemental Form (Air)|Effet : Forme élémentaire (Air)|libre|
|[DLwTvjjnqs2sNGuG.htm](spell-effects/DLwTvjjnqs2sNGuG.htm)|Spell Effect: Inspire Defense|Effet : Inspiration défensive|officielle|
|[DrNpuMj14wVj4bWF.htm](spell-effects/DrNpuMj14wVj4bWF.htm)|Spell Effect: Dragon Form (Copper)|Effet : Forme de dragon (Cuivre)|libre|
|[dWbg2gACxMkSnZag.htm](spell-effects/dWbg2gACxMkSnZag.htm)|Spell Effect: Protective Ward|Effet : Champ protecteur|officielle|
|[DwM5qcFp4JgKhXrY.htm](spell-effects/DwM5qcFp4JgKhXrY.htm)|Spell Effect: Fey Form (Unicorn)|Effet : Forme de fée (Licorne)|libre|
|[dXq7z633ve4E0nlX.htm](spell-effects/dXq7z633ve4E0nlX.htm)|Spell Effect: Regenerate|Effet : Régénération|officielle|
|[ei9MIyZbIaP4AZmh.htm](spell-effects/ei9MIyZbIaP4AZmh.htm)|Spell Effect: Flame Wisp|Effet : Feu follet enflammé|libre|
|[Eik8Fj8nGo2GLcbn.htm](spell-effects/Eik8Fj8nGo2GLcbn.htm)|Spell Effect: Monstrosity Form (Sea Serpent)|Effet : Forme monstrueuse (Serpent de mer)|libre|
|[eotqxEWIgaK7nMpD.htm](spell-effects/eotqxEWIgaK7nMpD.htm)|Spell Effect: Blunt the Final Blade (Critical Success)|Effet : Émousser la Lame finale (Succès critique)|libre|
|[EScdpppYsf9KhG4D.htm](spell-effects/EScdpppYsf9KhG4D.htm)|Spell Effect: Ghostly Weapon|Effet : Arme fantomatique|libre|
|[ETgzIIv3M2zvclAR.htm](spell-effects/ETgzIIv3M2zvclAR.htm)|Spell Effect: Dragon Form (Blue)|Effet : Forme de dragon (Bleu)|libre|
|[EUxTav62IXTz5CxW.htm](spell-effects/EUxTav62IXTz5CxW.htm)|Spell Effect: Nature Incarnate (Kaiju)|Effet : Incarnation de la nature (Kaiju)|libre|
|[evK8JR3j2iWGWaug.htm](spell-effects/evK8JR3j2iWGWaug.htm)|Spell Effect: Divine Vessel (Evil)|Effet : Réceptacle divin (Mauvais)|libre|
|[F10ofwC0k1ELIaV4.htm](spell-effects/F10ofwC0k1ELIaV4.htm)|Spell Effect: Impeccable Flow (Critical Failure Effect)|Effet : Flux impeccable (Effet d'échec critique)|libre|
|[F1APSdrw5uv672hf.htm](spell-effects/F1APSdrw5uv672hf.htm)|Spell Effect: Battlefield Persistence|Effet : Persévérance sur le champ de bataille|libre|
|[F4DTpDXNu5IliyhJ.htm](spell-effects/F4DTpDXNu5IliyhJ.htm)|Spell Effect: Animal Form (Deer)|Effet : Forme animale (Cerf)|libre|
|[fCIT9YgGUwIc3Z9G.htm](spell-effects/fCIT9YgGUwIc3Z9G.htm)|Spell Effect: Draw the Lightning|Effet : Attirer la foudre|libre|
|[FD9Ce5pqcZYstcMI.htm](spell-effects/FD9Ce5pqcZYstcMI.htm)|Spell Effect: Blessing of Defiance|Effet : Bénédiction du défi|libre|
|[fEhCbATDNlt6c1Ug.htm](spell-effects/fEhCbATDNlt6c1Ug.htm)|Spell Effect: Extract Poison|Effet : Extraction du poison|libre|
|[fGK6zJ7mWz9D5QYo.htm](spell-effects/fGK6zJ7mWz9D5QYo.htm)|Spell Effect: Rapid Adaptation (Aquatic Base Swim Speed)|Effet : Adaptation rapide (Aquatique : Vitesse de Base de nage)|libre|
|[fIloZhZVH1xTnX4B.htm](spell-effects/fIloZhZVH1xTnX4B.htm)|Spell Effect: Plant Form (Shambler)|Effet : Forme de plante (Grand tertre)|libre|
|[Fjnm1l59KH5YJ7G9.htm](spell-effects/Fjnm1l59KH5YJ7G9.htm)|Spell Effect: Inspire Heroics (Strength, +2)|Effet : Inspiration héroïque (Force, +2)|libre|
|[fKeZDm8kpDFK5HWp.htm](spell-effects/fKeZDm8kpDFK5HWp.htm)|Spell Effect: Devil Form (Sarglagon)|Effet : Forme de diable (Sarglagon)|libre|
|[fLOQMycP5tmXgPv9.htm](spell-effects/fLOQMycP5tmXgPv9.htm)|Spell Effect: Elemental Gift (Earth)|Effet : Don élémentaire (Terre)|libre|
|[fpGDAz2v5PG0zUSl.htm](spell-effects/fpGDAz2v5PG0zUSl.htm)|Spell Effect: True Strike|Effet : Coup au but|libre|
|[FR4ucNi2ceHZdrpB.htm](spell-effects/FR4ucNi2ceHZdrpB.htm)|Spell Effect: Warding Aggression (+1)|Effet : Agression protectrice (+1)|libre|
|[FT5Tt2DKBRutDqbV.htm](spell-effects/FT5Tt2DKBRutDqbV.htm)|Spell Effect: Dread Ambience (Critical Success)|Effet : Ambiance terrifiante (Succès critique)|libre|
|[fvIlSZPwojixVvyZ.htm](spell-effects/fvIlSZPwojixVvyZ.htm)|Spell Effect: Lucky Number|Effet : Nombre porte bonheur|libre|
|[fwaAe71qfnK7SiOB.htm](spell-effects/fwaAe71qfnK7SiOB.htm)|Spell Effect: Primal Summons (Air)|Effet : Convocations primordiales (Air)|libre|
|[g4E9l4uA62LcRBJS.htm](spell-effects/g4E9l4uA62LcRBJS.htm)|Spell Effect: Clawsong (Versatile Piercing)|Effet : Chant de la griffe (Polyvalent P)|libre|
|[GDzn5DToE62ZOTrP.htm](spell-effects/GDzn5DToE62ZOTrP.htm)|Spell Effect: Divine Vessel|Effet : Réceptacle divin|libre|
|[GhGoZdAZtzZTYCzj.htm](spell-effects/GhGoZdAZtzZTYCzj.htm)|Spell Effect: Animal Feature (Jaws)|Effet : Trait animal (Mâchoires)|libre|
|[GhNVAYtoF5hK3AlD.htm](spell-effects/GhNVAYtoF5hK3AlD.htm)|Spell Effect: Touch of Corruption|Effet : Toucher de corruption|libre|
|[gKGErrsS1WoAyWub.htm](spell-effects/gKGErrsS1WoAyWub.htm)|Spell Effect: Aberrant Form (Gogiteth)|Effet : Forme d'aberration (Gogiteth)|libre|
|[GlggmEqkGVj1noOD.htm](spell-effects/GlggmEqkGVj1noOD.htm)|Spell Effect: Bottle the Storm|Effet : Tempête en bouteille|libre|
|[GnWkI3T3LYRlm3X8.htm](spell-effects/GnWkI3T3LYRlm3X8.htm)|Spell Effect: Magic Weapon|Effet : Arme magique|libre|
|[gQnDKDeBTtjwOWAk.htm](spell-effects/gQnDKDeBTtjwOWAk.htm)|Spell Effect: Animal Form (Bear)|Effet : Forme animale (Ours)|libre|
|[Gqy7K6FnbLtwGpud.htm](spell-effects/Gqy7K6FnbLtwGpud.htm)|Spell Effect: Bless|Effet : Bénédiction|libre|
|[gX8O0ArQXbEVDUbW.htm](spell-effects/gX8O0ArQXbEVDUbW.htm)|Spell Effect: Embrace the Pit|Effet : Étreinte de la fosse|libre|
|[h0CKGrgjGNSg21BW.htm](spell-effects/h0CKGrgjGNSg21BW.htm)|Spell Effect: Boost Eidolon|Effet : Booster l'eidolon|libre|
|[h2JzNunzO8hXiNV3.htm](spell-effects/h2JzNunzO8hXiNV3.htm)|Spell Effect: Lifelink Surge|Effet : Afflux du lien vital|libre|
|[H6ndYYYlADWwqVQb.htm](spell-effects/H6ndYYYlADWwqVQb.htm)|Spell Effect: Dragon Form (White)|Effet : Forme de dragon (Blanc)|libre|
|[HDKJAUXMbtxnBdgR.htm](spell-effects/HDKJAUXMbtxnBdgR.htm)|Spell Effect: Tempest Form (Mist)|Effet : Forme tempêtueuse (Brume)|libre|
|[hdOb5Iu6Zd3pHoGI.htm](spell-effects/hdOb5Iu6Zd3pHoGI.htm)|Spell Effect: Discern Secrets (Recall Knowledge)|Effet : Discerner les secrets (Se souvenir)|libre|
|[heAj9paC8ZRh7QEj.htm](spell-effects/heAj9paC8ZRh7QEj.htm)|Spell Effect: Fey Form (Redcap)|Effet : Forme de fée (Bonnet rouge)|libre|
|[HEbbxKtBzsLhFead.htm](spell-effects/HEbbxKtBzsLhFead.htm)|Spell Effect: Devil Form (Osyluth)|Effet : Forme de diable (Osyluth)|libre|
|[HF1r1psnITHD52B9.htm](spell-effects/HF1r1psnITHD52B9.htm)|Aura: Dread Aura|Aura : Aura effroyable|libre|
|[hkLhZsH3T6jc9S1y.htm](spell-effects/hkLhZsH3T6jc9S1y.htm)|Spell Effect: Veil of Dreams|Effet : Voile de rêves|libre|
|[hnfQyf05IIa7WPBB.htm](spell-effects/hnfQyf05IIa7WPBB.htm)|Spell Effect: Demon Form (Nabasu)|Effet : Forme de démon (Nabasu)|libre|
|[HoCUCi2jL1OLfXWR.htm](spell-effects/HoCUCi2jL1OLfXWR.htm)|Spell Effect: Unblinking Flame Aura|Effet : Aura de la flamme qui ne vacille pas|libre|
|[HoOujAdQWCN4E6sQ.htm](spell-effects/HoOujAdQWCN4E6sQ.htm)|Spell Effect: Barkskin|Effet : Peau d'écorce|libre|
|[HtaDbgTIzdiTiKLX.htm](spell-effects/HtaDbgTIzdiTiKLX.htm)|Spell Effect: Triple Time|Effet : À trois temps|libre|
|[hXtK08bTnDBSzGTJ.htm](spell-effects/hXtK08bTnDBSzGTJ.htm)|Spell Effect: Iron Gut|Effet : Boyaux de fer|libre|
|[hya8NfBB1GJofTXm.htm](spell-effects/hya8NfBB1GJofTXm.htm)|Spell Effect: Unblinking Flame Ignition|Effet : Allumage de la flamme qui ne vacille pas|libre|
|[I4PsUAaYSUJ8pwKC.htm](spell-effects/I4PsUAaYSUJ8pwKC.htm)|Spell Effect: Ray of Frost|Effet : Rayon de givre|libre|
|[i9YITDcrq1nKjV5l.htm](spell-effects/i9YITDcrq1nKjV5l.htm)|Spell Effect: Infectious Melody|Effet : Mélodie contagieuse|libre|
|[ib58LaffEUIypuzL.htm](spell-effects/ib58LaffEUIypuzL.htm)|Spell Effect: Evolution Surge (Huge)|Effet : Flux d'évolution (Très grand)|libre|
|[IcQMLYWYDMZbq3XE.htm](spell-effects/IcQMLYWYDMZbq3XE.htm)|Spell Effect: Inscrutable Mask|Effet : Masque insondable|libre|
|[iiV80Kexj6vPmzqU.htm](spell-effects/iiV80Kexj6vPmzqU.htm)|Spell Effect: Rapid Adaptation (Arctic)|Effet : Adaptation rapide (Arctique)|libre|
|[iJ7TVW5tDnZG9DG8.htm](spell-effects/iJ7TVW5tDnZG9DG8.htm)|Spell Effect: Competitive Edge|Effet : Avantage du compétiteur|libre|
|[inNfTmtWpsxeGBI9.htm](spell-effects/inNfTmtWpsxeGBI9.htm)|Spell Effect: Darkvision (24 hours)|Effet : Vision dans le noir (24 heures)|libre|
|[iOKhr2El8R6cz6YI.htm](spell-effects/iOKhr2El8R6cz6YI.htm)|Spell Effect: Dinosaur Form (Triceratops)|Effet : Forme de dinosaure (Tricératops)|libre|
|[iqtjMVl6rGQhX2k8.htm](spell-effects/iqtjMVl6rGQhX2k8.htm)|Spell Effect: Elemental Motion (Air)|Effet : Mobilité élémentaire(Air)|libre|
|[itmiGioGNuVvt4QE.htm](spell-effects/itmiGioGNuVvt4QE.htm)|Spell Effect: Rapid Adaptation (Aquatic Speed Bonus)|Effet : Adaptation rapide (Aquatique: bonus vitesse de nage)|libre|
|[IWD5RehCxZVfgrX9.htm](spell-effects/IWD5RehCxZVfgrX9.htm)|Spell Effect: Elephant Form|Effet : Forme d'éléphant|libre|
|[IXS15IQXYCZ8vsmX.htm](spell-effects/IXS15IQXYCZ8vsmX.htm)|Spell Effect: Darkvision|Effet : Vision dans le noir|libre|
|[iZYjxY0qYvg5yPP3.htm](spell-effects/iZYjxY0qYvg5yPP3.htm)|Spell Effect: Angelic Wings|Effet : Ailes d'ange|libre|
|[j2LhQ7kEQhq3J3zZ.htm](spell-effects/j2LhQ7kEQhq3J3zZ.htm)|Spell Effect: Animal Form (Frog)|Effet : Forme animale (Grenouille)|libre|
|[J60rN48XzBGHmR6m.htm](spell-effects/J60rN48XzBGHmR6m.htm)|Spell Effect: Element Embodied (Air)|Effet : Incarnation élémentaire (Air)|libre|
|[j6po934p4jcUVC6l.htm](spell-effects/j6po934p4jcUVC6l.htm)|Spell Effect: Shifting Form (Speed)|Effet : Forme changeante (Vitesse)|libre|
|[j9l4LDnAwg9xzYsy.htm](spell-effects/j9l4LDnAwg9xzYsy.htm)|Spell Effect: Life Connection|Effet : Connexion vitale|libre|
|[Jemq5UknGdMO7b73.htm](spell-effects/Jemq5UknGdMO7b73.htm)|Spell Effect: Shield|Effet : Bouclier|libre|
|[JhihziXQuoteftdd.htm](spell-effects/JhihziXQuoteftdd.htm)|Spell Effect: Lay on Hands (Vs. Undead)|Effet : Imposition des mains (contre les morts-vivants)|libre|
|[JHpYudY14g0H4VWU.htm](spell-effects/JHpYudY14g0H4VWU.htm)|Spell Effect: Stoneskin|Effet : Peau de pierre|libre|
|[jj0P4eGVpmdwZjlA.htm](spell-effects/jj0P4eGVpmdwZjlA.htm)|Spell Effect: Instant Armor|Effet : Armure instantanée|libre|
|[JqrTrvwV7pYStMXz.htm](spell-effects/JqrTrvwV7pYStMXz.htm)|Spell Effect: Levitate|Effet : Lévitation|libre|
|[jtMo6qS47hPx6EbR.htm](spell-effects/jtMo6qS47hPx6EbR.htm)|Spell Effect: Rapid Adaptation (Forest)|Effet : Adaptation rapide (Forêt)|libre|
|[jy4edd6pvJvJgOSP.htm](spell-effects/jy4edd6pvJvJgOSP.htm)|Spell Effect: Dragon Wings (Own Speed)|Effet : Ailes de dragon (Sa vitesse)|libre|
|[KMF2t3qqzyFP0rxL.htm](spell-effects/KMF2t3qqzyFP0rxL.htm)|Spell Effect: Divine Vessel (Good)|Effet : Réceptacle divin (Bon)|libre|
|[kMoOWWBqDYmPcYyS.htm](spell-effects/kMoOWWBqDYmPcYyS.htm)|Spell Effect: Bathe in Blood|Effet : Baignade de sang|libre|
|[KtAJN4Qr2poTL6BB.htm](spell-effects/KtAJN4Qr2poTL6BB.htm)|Spell Effect: Bestial Curse (Critical Failure)|Effet : Malédiction bestiale (Échec critique)|libre|
|[kZ39XWJA3RBDTnqG.htm](spell-effects/kZ39XWJA3RBDTnqG.htm)|Spell Effect: Inspire Heroics (Courage, +2)|Effet : Inspiration héroïque (Courage, +2)|libre|
|[l8HkOKfiUqd3BUwT.htm](spell-effects/l8HkOKfiUqd3BUwT.htm)|Spell Effect: Ancestral Form|Effet : Forme ancestrale|libre|
|[l9HRQggofFGIxEse.htm](spell-effects/l9HRQggofFGIxEse.htm)|Spell Effect: Heroism|Effet : Héroïsme|libre|
|[lEU3DH1tGjAigpEt.htm](spell-effects/lEU3DH1tGjAigpEt.htm)|Spell Effect: Energy Absorption|Effet : Absorption d'énergie|libre|
|[LHREWCGPkWsc4GGJ.htm](spell-effects/LHREWCGPkWsc4GGJ.htm)|Spell Effect: Faerie Dust (Failure)|Effet : Poussière féerique (Échec)|libre|
|[lIl0yYdS9zojOZhe.htm](spell-effects/lIl0yYdS9zojOZhe.htm)|Spell Effect: Life-Giving Form|Effet : Forme génératrice de vie|libre|
|[LldX5hnNhKzGtOS0.htm](spell-effects/LldX5hnNhKzGtOS0.htm)|Spell Effect: Elemental Absorption|Effet : Absorption élémentaire|libre|
|[LMXxICrByo7XZ3Q3.htm](spell-effects/LMXxICrByo7XZ3Q3.htm)|Spell Effect: Downpour|Effet : Déluge|libre|
|[LMzFBnOEPzDGzHg4.htm](spell-effects/LMzFBnOEPzDGzHg4.htm)|Spell Effect: Unusual Anatomy|Effet : Anatomie étrange|libre|
|[LT5AV9vSN3T9x3J9.htm](spell-effects/LT5AV9vSN3T9x3J9.htm)|Spell Effect: Corrosive Body|Effet : Corps corrosif|libre|
|[lTL5VwNrZ5xiitGV.htm](spell-effects/lTL5VwNrZ5xiitGV.htm)|Spell effect: Nudge the Odds|Effet : Renforcer les chances|libre|
|[LXf1Cqi1zyo4DaLv.htm](spell-effects/LXf1Cqi1zyo4DaLv.htm)|Spell Effect: Shrink|Effet : Rétrécir|libre|
|[lyLMiauxIVUM3oF1.htm](spell-effects/lyLMiauxIVUM3oF1.htm)|Spell Effect: Lay on Hands|Effet : Imposition des mains|libre|
|[m1tQTBrolf7uZBW0.htm](spell-effects/m1tQTBrolf7uZBW0.htm)|Spell Effect: Discern Secrets (Seek)|Effet : Discerner les secrets (Chercher)|libre|
|[m6x0IvoeX0a0bZiQ.htm](spell-effects/m6x0IvoeX0a0bZiQ.htm)|Spell Effect: Unbreaking Wave Vapor|Effet : Vapeur de la vague inexorable|libre|
|[mAofA4oy3cRdT71K.htm](spell-effects/mAofA4oy3cRdT71K.htm)|Spell Effect: Penumbral Disguise|Effet : Déguisement pénombral|libre|
|[mCb9mWAmgWPQrkTY.htm](spell-effects/mCb9mWAmgWPQrkTY.htm)|Spell Effect: Barkskin (Arboreal's Revenge)|Effet : Peau d'écorce (Vengeance de l'arboréen)|libre|
|[Me470HI6inX3Bovh.htm](spell-effects/Me470HI6inX3Bovh.htm)|Spell Effect: Guided Introspection|Effet : Introspection guidée|libre|
|[mhklZ6wjfty0bF44.htm](spell-effects/mhklZ6wjfty0bF44.htm)|Spell Effect: Speaking Sky|Effet : Ciel parlant|libre|
|[MJSoRFfEdM4j5mNG.htm](spell-effects/MJSoRFfEdM4j5mNG.htm)|Spell Effect: Sweet Dream (Glamour)|Effet : Doux rêve (Glamour)|libre|
|[MjtPtndJx31q2N9R.htm](spell-effects/MjtPtndJx31q2N9R.htm)|Spell Effect: Amplifying Touch|Effet : Toucher amplificateur|libre|
|[mmJNE57hC7G3SPae.htm](spell-effects/mmJNE57hC7G3SPae.htm)|Spell Effect: Silence|Effet : Silence|libre|
|[Mp7252yAsSA8lCEA.htm](spell-effects/Mp7252yAsSA8lCEA.htm)|Spell Effect: One With the Land|Effet : Uni à la terre|libre|
|[MqZ6FScbfGtXB8tt.htm](spell-effects/MqZ6FScbfGtXB8tt.htm)|Spell Effect: Magic Fang|Effet : Morsure magique|officielle|
|[mr6mlkUMeStdChxi.htm](spell-effects/mr6mlkUMeStdChxi.htm)|Spell Effect: Animal Feature (Owl Eyes)|Effet : Trait animal (Yeux de hibou)|libre|
|[mrSulUdNbwzGSwfu.htm](spell-effects/mrSulUdNbwzGSwfu.htm)|Spell Effect: Glutton's Jaw|Effet : Mâchoires gloutonnes|libre|
|[MuRBCiZn5IKeaoxi.htm](spell-effects/MuRBCiZn5IKeaoxi.htm)|Spell Effect: Fly|Effet : Vol|libre|
|[mzDgsuuo5wCgqyxR.htm](spell-effects/mzDgsuuo5wCgqyxR.htm)|Spell Effect: Mirecloak|Effet : Cape-miroir|libre|
|[N1EM3jRyT8PCG1Py.htm](spell-effects/N1EM3jRyT8PCG1Py.htm)|Spell Effect: Traveler's Transit (Climb)|Effet : Voyageur en transit (Escalade)|libre|
|[n6NK7wqhTxWr3ij8.htm](spell-effects/n6NK7wqhTxWr3ij8.htm)|Spell Effect: Warding Aggression (+2)|Effet : Agression protectrice (+2)|libre|
|[nbW4udOUTrCGL3Gf.htm](spell-effects/nbW4udOUTrCGL3Gf.htm)|Spell Effect: Shifting Form (Climb Speed)|Effet : Forme changeante (Escalade)|libre|
|[ndj0TpLxyzbyzcm4.htm](spell-effects/ndj0TpLxyzbyzcm4.htm)|Spell Effect: Necrotize (Legs)|Effet : Nécroser (Jambes)|libre|
|[nemThuhp3praALY6.htm](spell-effects/nemThuhp3praALY6.htm)|Spell Effect: Zealous Conviction|Effet : Conviction zélée|libre|
|[nHXKK4pRXAzrLdEP.htm](spell-effects/nHXKK4pRXAzrLdEP.htm)|Spell Effect: Take Its Course (Affliction, Help)|Effet : Suivre son cours (Afflicton, aide)|libre|
|[nIryhRgeiacQw1Em.htm](spell-effects/nIryhRgeiacQw1Em.htm)|Spell Effect: Soothing Blossoms|Effet : Bourgeons apaisants|libre|
|[nkk4O5fyzrC0057i.htm](spell-effects/nkk4O5fyzrC0057i.htm)|Spell Effect: Soothe|Effet : Apaiser|libre|
|[nLige83aiMBh0ylb.htm](spell-effects/nLige83aiMBh0ylb.htm)|Spell Effect: Musical Accompaniment|Effet : Accompagnement musical|libre|
|[npFFTAxN44WWrGnM.htm](spell-effects/npFFTAxN44WWrGnM.htm)|Spell Effect: Wash Your Luck|Effet : Laver votre chance|libre|
|[NQZ88IoKeMBsfjp7.htm](spell-effects/NQZ88IoKeMBsfjp7.htm)|Spell Effect: Life Boost|Effet : Gain de vie|libre|
|[nU4SxAk6XreHUi5h.htm](spell-effects/nU4SxAk6XreHUi5h.htm)|Spell Effect: Infectious Enthusiasm|Effet : Enthousiasme communicatif|libre|
|[nUJSdm4fy6fcwsvv.htm](spell-effects/nUJSdm4fy6fcwsvv.htm)|Spell Effect: Lashunta's Life Bubble|Effet : Bulle de vie du lashunta|libre|
|[NXzo2kdgVixIZ2T1.htm](spell-effects/NXzo2kdgVixIZ2T1.htm)|Spell Effect: Apex Companion|Effet : Compagnon alpha|libre|
|[oaRt210JV4GZIHmJ.htm](spell-effects/oaRt210JV4GZIHmJ.htm)|Spell Effect: Rejuvenating Touch|Effet : Toucher rajeunissant|libre|
|[otK6eG3b3FV7n2xP.htm](spell-effects/otK6eG3b3FV7n2xP.htm)|Spell Effect: Swampcall (Critical Failure)|Effet : Appel du marais (Échec critique)|libre|
|[OxJEUhim6xzsHIyi.htm](spell-effects/OxJEUhim6xzsHIyi.htm)|Spell Effect: Divine Vessel 9th level (Good)|Effet : Réceptacle divin Niveau 9 (Bon)|libre|
|[p8F3MVUkGmpsUDOn.htm](spell-effects/p8F3MVUkGmpsUDOn.htm)|Spell Effect: Untwisting Iron Pillar|Effet : Pilier du fer qui ne plie pas|libre|
|[pcK88HqL6LjBNH2h.htm](spell-effects/pcK88HqL6LjBNH2h.htm)|Spell Effect: Faerie Dust (Critical Failure)|Effet : Poussière féerique (Échec critique)|libre|
|[PDoTV4EhJp63FEaG.htm](spell-effects/PDoTV4EhJp63FEaG.htm)|Spell Effect: Draw Ire (Success)|Effet : Attirer la colère (Succès)|libre|
|[pE2TWhG97XbhgAdH.htm](spell-effects/pE2TWhG97XbhgAdH.htm)|Spell Effect: Incendiary Aura|Effet : Aura incendiaire|libre|
|[Pfllo68qdQjC4Qv6.htm](spell-effects/Pfllo68qdQjC4Qv6.htm)|Spell Effect: Prismatic Shield|Effet : Bouclier prismatique|libre|
|[PhBrHvBwvq8rni9C.htm](spell-effects/PhBrHvBwvq8rni9C.htm)|Spell Effect: Evolution Surge (Large)|Effet : Flux d'évolution (Grande)|libre|
|[PkofF4bxkDUgeIoE.htm](spell-effects/PkofF4bxkDUgeIoE.htm)|Spell Effect: Touch of Corruption (Healing)|Effet : Toucher de corruption (Soins)|libre|
|[PNEGSVYhMKf6kQZ6.htm](spell-effects/PNEGSVYhMKf6kQZ6.htm)|Spell Effect: Call to Arms|Effet : Appel aux armes|libre|
|[pocsoEi84Mr2buOc.htm](spell-effects/pocsoEi84Mr2buOc.htm)|Spell Effect: Evolution Surge (Scent)|Effet : Flux d'évolution (Odorat)|libre|
|[pPMldkAbPVOSOPIF.htm](spell-effects/pPMldkAbPVOSOPIF.htm)|Spell Effect: Protect Companion|Effet : Protéger le compagnon|libre|
|[ppVKJY6AYggn2Fma.htm](spell-effects/ppVKJY6AYggn2Fma.htm)|Spell Effect: Goodberry|Effet : Baie nourricière|libre|
|[PQHP7Oph3BQX1GhF.htm](spell-effects/PQHP7Oph3BQX1GhF.htm)|Spell Effect: Longstrider|Effet : Grande foulée|libre|
|[q4EEYltjqpRGiLsP.htm](spell-effects/q4EEYltjqpRGiLsP.htm)|Spell Effect: Elemental Motion (Fire)|Effet : Mobilité élémentaire (Feu)|libre|
|[qbOpis7pIkXJbM2B.htm](spell-effects/qbOpis7pIkXJbM2B.htm)|Spell Effect: Elemental Motion (Earth)|Effet : Mobilité élémentaire (Terre)|libre|
|[QF6RDlCoTvkVHRo4.htm](spell-effects/QF6RDlCoTvkVHRo4.htm)|Effect: Shield Immunity|Effet : Immunité à Bouclier|libre|
|[qhNUfwpkD8BRw4zj.htm](spell-effects/qhNUfwpkD8BRw4zj.htm)|Spell Effect: Magic Hide|Effet : Peau magique|libre|
|[QJRaVbulmpOzWi6w.htm](spell-effects/QJRaVbulmpOzWi6w.htm)|Spell Effect: Girzanje's March|Effet : Marche de Guirzanjé|libre|
|[qkwb5DD3zmKwvbk0.htm](spell-effects/qkwb5DD3zmKwvbk0.htm)|Spell Effect: Mage Armor|Effet : Armure du mage|libre|
|[qlz0sJIvqc0FdUdr.htm](spell-effects/qlz0sJIvqc0FdUdr.htm)|Spell Effect: Weapon Surge|Effet : Arme améliorée|libre|
|[qo7DoF11Xl9gqmFc.htm](spell-effects/qo7DoF11Xl9gqmFc.htm)|Spell Effect: Rapid Adaptation (Desert)|Effet : Adaptation rapide (Désert)|libre|
|[qQLHPbUFASKFky1W.htm](spell-effects/qQLHPbUFASKFky1W.htm)|Spell Effect: Hyperfocus|Effet : Hyperacuité|libre|
|[Qr5rgoZvI4KmFY0N.htm](spell-effects/Qr5rgoZvI4KmFY0N.htm)|Spell Effect: Calm Emotions|Effet : Apaisement des émotions|libre|
|[qRyynMOflGajgAR3.htm](spell-effects/qRyynMOflGajgAR3.htm)|Spell Effect: Mantle of the Magma Heart (Enlarging Eruption)|Effet : Manteau du coeur magmatique (Éruption élargie)|libre|
|[qzZmVjtc9feqoQwA.htm](spell-effects/qzZmVjtc9feqoQwA.htm)|Spell Effect: Wish-Twisted Form (Success)|Effet : Forme déformée par un souhait (succès)|libre|
|[R27azQfzeFuFc48G.htm](spell-effects/R27azQfzeFuFc48G.htm)|Spell Effect: Take Its Course (Affliction, Hinder)|Effet : Suivre son cours (Affliction, entraver)|libre|
|[R3j6song8sYLY5vG.htm](spell-effects/R3j6song8sYLY5vG.htm)|Spell Effect: Community Repair (Critical Success)|Effet : Réparation communautaire (Succès critique)|libre|
|[r4XX7yzeEOPK7l2a.htm](spell-effects/r4XX7yzeEOPK7l2a.htm)|Spell Effect: Seal Fate|Effet : Sceller le destin|libre|
|[RawLEPwyT5CtCZ4D.htm](spell-effects/RawLEPwyT5CtCZ4D.htm)|Spell Effect: Protection|Effet : Protection|libre|
|[rjM25qfw5BKj9h97.htm](spell-effects/rjM25qfw5BKj9h97.htm)|Spell Effect: Entangle|Effet : Enchevêtrement|libre|
|[RLUhIyqH84Dle4vo.htm](spell-effects/RLUhIyqH84Dle4vo.htm)|Spell Effect: Fungal Infestation (Critical Failure)|Effet : Infestation fongique(Échec critique)|libre|
|[ROuGwXEvFznzGE9k.htm](spell-effects/ROuGwXEvFznzGE9k.htm)|Spell Effect: Swampcall (Failure)|Effet : Appel du marais (Échec)|libre|
|[rQaltMIEi2bn1Z4k.htm](spell-effects/rQaltMIEi2bn1Z4k.htm)|Spell Effect: Ki Form|Effet : Forme ki|libre|
|[s6CwkSsMDGfUmotn.htm](spell-effects/s6CwkSsMDGfUmotn.htm)|Spell Effect: Death Ward|Effet : Protection contre la mort|officielle|
|[sDN9b4bjCGH2nQnG.htm](spell-effects/sDN9b4bjCGH2nQnG.htm)|Spell Effect: Rapid Adaptation (Mountain)|Effet : Adaptation rapide (Montagne)|libre|
|[sE2txm68yZSFMV3v.htm](spell-effects/sE2txm68yZSFMV3v.htm)|Spell Effect: Sweet Dream (Voyaging)|Effet : Doux rêve (Voyage)|libre|
|[sILRkGTwoBywy0BU.htm](spell-effects/sILRkGTwoBywy0BU.htm)|Spell Effect: Gaseous Form|Effet : Forme gazeuse|officielle|
|[slI9P4jUp3ERPCqX.htm](spell-effects/slI9P4jUp3ERPCqX.htm)|Spell Effect: Impeccable Flow|Effet : Flux impeccable|libre|
|[sPCWrhUHqlbGhYSD.htm](spell-effects/sPCWrhUHqlbGhYSD.htm)|Spell Effect: Enlarge|Effet : Agrandissement|libre|
|[T3t9776ataHzrmTs.htm](spell-effects/T3t9776ataHzrmTs.htm)|Spell Effect: Inside Ropes (3rd level)|Effet : Effet : Tripes internes (niveau 3)|libre|
|[T5bk6UH7yuYog1Fp.htm](spell-effects/T5bk6UH7yuYog1Fp.htm)|Spell Effect: See Invisibility|Effet : Détection de l'invisibilité|libre|
|[TAAWbJgfESltn2we.htm](spell-effects/TAAWbJgfESltn2we.htm)|Spell Effect: Primal Summons (Water)|Effet : Convocations primordiales (Eau)|libre|
|[tBgwWblDp1xdxN4D.htm](spell-effects/tBgwWblDp1xdxN4D.htm)|Spell Effect: Divine Vessel (Lawful)|Effet : Réceptacle divin (Loyal)|libre|
|[tC0Qk4AjYRd3csL7.htm](spell-effects/tC0Qk4AjYRd3csL7.htm)|Spell Effect: Swarm Form|Effet : Forme de nuée|libre|
|[ThFug45WHkQQXcoF.htm](spell-effects/ThFug45WHkQQXcoF.htm)|Spell Effect: Fleet Step|Effet : Pas rapide|libre|
|[tjC6JeZgLDPIMHjG.htm](spell-effects/tjC6JeZgLDPIMHjG.htm)|Spell Effect: Malignant Sustenance|Effet : Alimentation maléfique|libre|
|[TjGHxli0edXI6rAg.htm](spell-effects/TjGHxli0edXI6rAg.htm)|Spell Effect: Schadenfreude (Success)|Effet : Joie malsaine (Succès)|libre|
|[tNjimcyUwn8afeH6.htm](spell-effects/tNjimcyUwn8afeH6.htm)|Spell Effect: Gravity Weapon|Effet : Arme pesante|libre|
|[TpVkVALUBrBQjULn.htm](spell-effects/TpVkVALUBrBQjULn.htm)|Spell Effect: Stoke the Heart|Effet : Enflammer les coeurs|libre|
|[TrmNSuv6zWEiceqn.htm](spell-effects/TrmNSuv6zWEiceqn.htm)|Spell Effect: Incendiary Ashes (Failure)|Effet : Cendres incendiaires (Échec)|libre|
|[TwtUIEyenrtAbeiX.htm](spell-effects/TwtUIEyenrtAbeiX.htm)|Spell Effect: Tanglefoot|Effet : Entrave|officielle|
|[u0AznkjTZAVnCMNp.htm](spell-effects/u0AznkjTZAVnCMNp.htm)|Spell Effect: Evolution Surge (Fly)|Effet : Flux d'évolution (Vol)|libre|
|[ubHqpJwUwygkc2dR.htm](spell-effects/ubHqpJwUwygkc2dR.htm)|Spell Effect: Dread Aura|Effet : Aura Effroyable|libre|
|[uDOxq24S7IT2EcXv.htm](spell-effects/uDOxq24S7IT2EcXv.htm)|Spell Effect: Object Memory (Weapon)|Effet : Mémoire de l'objet (Arme ou outil)|libre|
|[UH2sT6eW5e31Xytd.htm](spell-effects/UH2sT6eW5e31Xytd.htm)|Spell Effect: Dutiful Challenge|Effet : Défi de dévouement|libre|
|[Uj9VFXoVMH0mTTdt.htm](spell-effects/Uj9VFXoVMH0mTTdt.htm)|Spell Effect: Organsight|Effet : Vision des organes|libre|
|[Um25D1qLtZWOSBny.htm](spell-effects/Um25D1qLtZWOSBny.htm)|Spell Effect: Shifting Form (Swim Speed)|Effet : Forme changeante (Nage)|libre|
|[UtIOWubq7akdHMOh.htm](spell-effects/UtIOWubq7akdHMOh.htm)|Spell Effect: Augment Summoning|Effet : Convocation améliorée|libre|
|[UTLp7omqsiC36bso.htm](spell-effects/UTLp7omqsiC36bso.htm)|Spell Effect: Bane|Effet : Imprécation|officielle|
|[uUlFic5lnr2FpNiG.htm](spell-effects/uUlFic5lnr2FpNiG.htm)|Spell Effect: Lashunta's Life Bubble (Heightened)|Effet : Bulle de vie du Lashunta (Intensifié 8e)|libre|
|[UVrEe0nukiSmiwfF.htm](spell-effects/UVrEe0nukiSmiwfF.htm)|Spell Effect: Reinforce Eidolon|Effet : Renforcer l'eidolon|libre|
|[UXdt1WVA66oZOoZS.htm](spell-effects/UXdt1WVA66oZOoZS.htm)|Spell Effect: Flame Barrier|Effet : Barrière de flammes|officielle|
|[v051JKN0Dj3ve5cF.htm](spell-effects/v051JKN0Dj3ve5cF.htm)|Spell Effect: Sweet Dream (Insight)|Effet : Doux rêve (Intuition)|libre|
|[v09uwq1eHEAy2bgh.htm](spell-effects/v09uwq1eHEAy2bgh.htm)|Spell Effect: Unbreaking Wave Barrier|Effet : Barrière de la vague inexorable|libre|
|[V7jAnItnVqtfCAKt.htm](spell-effects/V7jAnItnVqtfCAKt.htm)|Spell Effect: Dragon Wings (60 Feet)|Effet : Ailes de dragon (18 mètres)|libre|
|[VFereWC1agrwgzPL.htm](spell-effects/VFereWC1agrwgzPL.htm)|Spell Effect: Inspire Heroics (Courage, +3)|Effet : Inspiration héroïque (Courage, +3)|libre|
|[ViBlOrd6hno3DiPP.htm](spell-effects/ViBlOrd6hno3DiPP.htm)|Spell Effect: Stumbling Curse|Effet : Malédiction titubante|libre|
|[VJpRUgSDtAO2TSRR.htm](spell-effects/VJpRUgSDtAO2TSRR.htm)|Spell Effect: Glimpse Weakness|Effet : Aperçu des faiblesses|libre|
|[vUjRvriyuHDZrsgc.htm](spell-effects/vUjRvriyuHDZrsgc.htm)|Spell Effect: Ghostly Shift|Effet : Transformation fantomatique|libre|
|[VVSOzHV6Rz2YNHRl.htm](spell-effects/VVSOzHV6Rz2YNHRl.htm)|Spell Effect: Contagious Idea (Pleasant Thought)|Effet : Idée contagieuse (Pensée plaisante)|libre|
|[W0PjCMyGOpKAuyKX.htm](spell-effects/W0PjCMyGOpKAuyKX.htm)|Spell Effect: Weapon Surge (Major Striking)|Effet : Arme améliorée (Frappe majeure)|libre|
|[W4lb3417rNDd9tCq.htm](spell-effects/W4lb3417rNDd9tCq.htm)|Spell Effect: Righteous Might|Effet : Force du molosse|libre|
|[w9HpSwBLByNyRXvi.htm](spell-effects/w9HpSwBLByNyRXvi.htm)|Spell Effect: Blessing of Defiance (2 Action)|Effet : Bénédiction du défi (2 actions)|libre|
|[wKdIf5x7zztiFHpL.htm](spell-effects/wKdIf5x7zztiFHpL.htm)|Spell Effect: Divine Vessel 9th level (Lawful)|Effet : Réceptacle divin Niveau 9 (Loyal)|libre|
|[wqh8D9kHGItZBvtQ.htm](spell-effects/wqh8D9kHGItZBvtQ.htm)|Spell Effect: Mantle of the Frozen Heart (Icy Claws)|Effet : Manteau du coeur gelé (Griffes de glace)|libre|
|[WWtSEJGwKY4bQpUn.htm](spell-effects/WWtSEJGwKY4bQpUn.htm)|Spell Effect: Vital Beacon|Effet : Fanal de vie|libre|
|[X7RD0JRxhJV9u2LC.htm](spell-effects/X7RD0JRxhJV9u2LC.htm)|Spell Effect: Disrupting Weapons|Effet : Armes perturbatrices|libre|
|[xKJVqN1ETnNH3tFg.htm](spell-effects/xKJVqN1ETnNH3tFg.htm)|Spell Effect: Corrosive Body (Heightened 9th)|Effet : Corps corrosif (Intensifié (9e))|libre|
|[Xl48OsJ47oDVZAVQ.htm](spell-effects/Xl48OsJ47oDVZAVQ.htm)|Spell Effect: Primal Summons (Earth)|Effet : Convocations primordiales (Terre)|libre|
|[Xlwt1wpjEKWBLUjK.htm](spell-effects/Xlwt1wpjEKWBLUjK.htm)|Spell Effect: Animal Feature (Fish Tail)|Effet : Trait animal (Queue de poisson)|libre|
|[XMBoKRRyooKnGkHk.htm](spell-effects/XMBoKRRyooKnGkHk.htm)|Spell Effect: Practice Makes Perfect|Effet : En forgeant on devient forgeron|libre|
|[XMOWgJV8UXWmpgk0.htm](spell-effects/XMOWgJV8UXWmpgk0.htm)|Spell Effect: Evolution Surge (Speed)|Effet : Flux d'évolution (Vitesse)|libre|
|[xPVOvWNJORvm8EwP.htm](spell-effects/xPVOvWNJORvm8EwP.htm)|Spell Effect: Mimic Undead|Effet : Imiter un mort-vivant|libre|
|[XT3AyRfx4xeXfAjP.htm](spell-effects/XT3AyRfx4xeXfAjP.htm)|Spell Effect: Physical Boost|Effet : Amélioration physique|libre|
|[XTgxkQkhlap66e54.htm](spell-effects/XTgxkQkhlap66e54.htm)|Spell Effect: Iron Gut (3rd Level)|Effet : Boyaux de fer (niveau 3)|libre|
|[Y6aNYnGVXdAMvL7Y.htm](spell-effects/Y6aNYnGVXdAMvL7Y.htm)|Spell Effect: Thermal Stasis|Effet : Stase thermique|libre|
|[y9PJdDYFemhk6Z5o.htm](spell-effects/y9PJdDYFemhk6Z5o.htm)|Spell Effect: Agile Feet|Effet : Pieds Agiles|libre|
|[YCno88Te0nfwFgVo.htm](spell-effects/YCno88Te0nfwFgVo.htm)|Spell Effect: Mantle of the Frozen Heart (Ice Glide)|Effet : Manteau du coeur gelé (Glissement glacé)|libre|
|[yke7fAUDxUNzouQc.htm](spell-effects/yke7fAUDxUNzouQc.htm)|Spell Effect: Elemental Gift (Air)|Effet : Don élémentaire (Air)|libre|
|[yq6iu0Qxg3YEbb6s.htm](spell-effects/yq6iu0Qxg3YEbb6s.htm)|Spell Effect: Elemental Gift (Water)|Effet : Don élémentaire (Eau)|libre|
|[YzZs7r8VUOp7PiAW.htm](spell-effects/YzZs7r8VUOp7PiAW.htm)|Spell Effect: Magic Stone|Effet : Pierre magique|libre|
|[zbTpf11NtbmizuzR.htm](spell-effects/zbTpf11NtbmizuzR.htm)|Spell Effect: Forge (Critical Failure)|Effet : Forge - Échec critique|libre|
|[zdpTPf157rXl3tEJ.htm](spell-effects/zdpTPf157rXl3tEJ.htm)|Spell Effect: Clawsong (Deadly D8)|Effet : Chant de la griffe (Mortel D8)|libre|
|[ZHVtJKnur9PAF5TO.htm](spell-effects/ZHVtJKnur9PAF5TO.htm)|Spell Effect: Enduring Might|Effet : Puissance protectrice|libre|
|[zjFN1cJEl3AMKiVs.htm](spell-effects/zjFN1cJEl3AMKiVs.htm)|Spell Effect: Nymph's Token|Effet : Amulette de la nymphe|libre|
|[ZlsuhS9J0S3PuvCO.htm](spell-effects/ZlsuhS9J0S3PuvCO.htm)|Spell Effect: Blink|Effet : Clignotement|libre|
|[zPGVOLz6xhsQN35C.htm](spell-effects/zPGVOLz6xhsQN35C.htm)|Spell Effect: Envenom Companion|Effet : Compagnon venimeux|libre|
|[zpxIwEjnLUSO1B4z.htm](spell-effects/zpxIwEjnLUSO1B4z.htm)|Spell Effect: Magic's Vessel|Effet : Réceptacle magique|libre|
