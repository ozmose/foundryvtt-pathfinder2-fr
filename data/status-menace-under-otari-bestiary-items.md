# État de la traduction (menace-under-otari-bestiary-items)

 * **officielle**: 60
 * **libre**: 4


Dernière mise à jour: 2023-07-12 20:28 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0c1Yjs4t9BKCpjmr.htm](menace-under-otari-bestiary-items/0c1Yjs4t9BKCpjmr.htm)|Falling Stones|Chute de pierre|officielle|
|[0j2Oa8lKuFbrPVH4.htm](menace-under-otari-bestiary-items/0j2Oa8lKuFbrPVH4.htm)|Wizard Spells|Sorts de magicien|libre|
|[3w5kvSRp3MF3snpw.htm](menace-under-otari-bestiary-items/3w5kvSRp3MF3snpw.htm)|Spring|Ressort|officielle|
|[4736AvChlsLG9HVa.htm](menace-under-otari-bestiary-items/4736AvChlsLG9HVa.htm)|Horn|Corne|officielle|
|[4gDOqdD5endao3AS.htm](menace-under-otari-bestiary-items/4gDOqdD5endao3AS.htm)|Survival|Survie|officielle|
|[94P2S8OyxiovGujz.htm](menace-under-otari-bestiary-items/94P2S8OyxiovGujz.htm)|Twisting Tail|Queue sinueuse|officielle|
|[9yktjCbx8rbvpSZe.htm](menace-under-otari-bestiary-items/9yktjCbx8rbvpSZe.htm)|Spear Barrage|Barrage de lances|officielle|
|[Ae0gibnuQy3PwVCV.htm](menace-under-otari-bestiary-items/Ae0gibnuQy3PwVCV.htm)|Spear|Lance|officielle|
|[B1zHLf1i3W3pgrBz.htm](menace-under-otari-bestiary-items/B1zHLf1i3W3pgrBz.htm)|Hurried Retreat|Retraite précipitée|officielle|
|[C4dwFfQQc0M5gZBd.htm](menace-under-otari-bestiary-items/C4dwFfQQc0M5gZBd.htm)|Fangs|Crocs|officielle|
|[d5UuF56WuHzskPO4.htm](menace-under-otari-bestiary-items/d5UuF56WuHzskPO4.htm)|Draconic Frenzy|Frénésie draconique|officielle|
|[dKlUXGzoOpojAKGv.htm](menace-under-otari-bestiary-items/dKlUXGzoOpojAKGv.htm)|Spear|Lance-épieu|officielle|
|[DW4EvhApIul7gVaU.htm](menace-under-otari-bestiary-items/DW4EvhApIul7gVaU.htm)|Tail|Queue|officielle|
|[eh06w4tCybKWTx8g.htm](menace-under-otari-bestiary-items/eh06w4tCybKWTx8g.htm)|Eggshell Necklace|Collier en coquille d'oeuf|officielle|
|[ELWu5zIODys6k1lo.htm](menace-under-otari-bestiary-items/ELWu5zIODys6k1lo.htm)|Web|Toile|officielle|
|[eU0KcVkea4Q9goDX.htm](menace-under-otari-bestiary-items/eU0KcVkea4Q9goDX.htm)|Dragon Lore|Connaissance des dragons|officielle|
|[fIAhNhYo8hJML5xU.htm](menace-under-otari-bestiary-items/fIAhNhYo8hJML5xU.htm)|Mind Reading|Lecture des pensées|officielle|
|[FRce1Q7000LFf9Gs.htm](menace-under-otari-bestiary-items/FRce1Q7000LFf9Gs.htm)|Iron Key|Clé en fer|officielle|
|[fx6MEhp40JvDajQh.htm](menace-under-otari-bestiary-items/fx6MEhp40JvDajQh.htm)|Spike Trap|Piège à pointes|officielle|
|[gEfOwadNLpPfyfQ8.htm](menace-under-otari-bestiary-items/gEfOwadNLpPfyfQ8.htm)|Claw|Griffe|officielle|
|[GRUZiIR1Uu91DaoY.htm](menace-under-otari-bestiary-items/GRUZiIR1Uu91DaoY.htm)|Stealth|Discrétion|officielle|
|[HAaABYYDbsq8LbrU.htm](menace-under-otari-bestiary-items/HAaABYYDbsq8LbrU.htm)|Jaws|Mâchoires|officielle|
|[haWc7Vo7IDgvIPU9.htm](menace-under-otari-bestiary-items/haWc7Vo7IDgvIPU9.htm)|No MAP|Pas de PAM|officielle|
|[hnjL5DfkOGkcdjPb.htm](menace-under-otari-bestiary-items/hnjL5DfkOGkcdjPb.htm)|Cleric Spells|Sorts de prêtre|libre|
|[iF6CI9twYSN48Gfj.htm](menace-under-otari-bestiary-items/iF6CI9twYSN48Gfj.htm)|Darkvision|Vision dans le noir|officielle|
|[IgVFPfZjnGqYSVAI.htm](menace-under-otari-bestiary-items/IgVFPfZjnGqYSVAI.htm)|Animal Talker|Parle-animal|officielle|
|[iUzYuqnjkPvxjVq6.htm](menace-under-otari-bestiary-items/iUzYuqnjkPvxjVq6.htm)|Stealth|Discrétion|officielle|
|[j90G6J5zvaIVNb1q.htm](menace-under-otari-bestiary-items/j90G6J5zvaIVNb1q.htm)|Breath Weapon|Souffle|officielle|
|[JaJLITtJG6QOyapG.htm](menace-under-otari-bestiary-items/JaJLITtJG6QOyapG.htm)|Darkvision|Vision dans le noir|officielle|
|[kkKlT3cO51Wj2tz5.htm](menace-under-otari-bestiary-items/kkKlT3cO51Wj2tz5.htm)|Draconic Momentum|Impulsion draconique|officielle|
|[KlwGH9hdWl3XpMYw.htm](menace-under-otari-bestiary-items/KlwGH9hdWl3XpMYw.htm)|Web Trap|Piège de toile|officielle|
|[kXXO2coSLlDjiCDu.htm](menace-under-otari-bestiary-items/kXXO2coSLlDjiCDu.htm)|Survival|Survie|officielle|
|[KYeYaMrxJZuncqJp.htm](menace-under-otari-bestiary-items/KYeYaMrxJZuncqJp.htm)|Dragon Lore|Connaissance des dragons|officielle|
|[LPBpvhfnfUxz7nWz.htm](menace-under-otari-bestiary-items/LPBpvhfnfUxz7nWz.htm)|Scythe|Faux|officielle|
|[Mcbgf9KdTTBPuio6.htm](menace-under-otari-bestiary-items/Mcbgf9KdTTBPuio6.htm)|Quick Trap|Piège éclair|officielle|
|[MrC7xvsyL1EpWyqb.htm](menace-under-otari-bestiary-items/MrC7xvsyL1EpWyqb.htm)|Falling Scythes|Chute de faux|officielle|
|[Odl9UZ1OKOYG044E.htm](menace-under-otari-bestiary-items/Odl9UZ1OKOYG044E.htm)|Descend on a Web|Descendre sur un fil de toile|officielle|
|[OmVVuah2jgqtqVVo.htm](menace-under-otari-bestiary-items/OmVVuah2jgqtqVVo.htm)|Shortsword|Épée courte|officielle|
|[p27dLxTbXpewe8ZR.htm](menace-under-otari-bestiary-items/p27dLxTbXpewe8ZR.htm)|Spear|Lance-épieu|officielle|
|[pY7DRATVeNfGLzvD.htm](menace-under-otari-bestiary-items/pY7DRATVeNfGLzvD.htm)|Fire Lore|Connaissance du feu|officielle|
|[qG3VBB7HAWZksxvT.htm](menace-under-otari-bestiary-items/qG3VBB7HAWZksxvT.htm)|Falling Block|Chute de pierre|officielle|
|[QpOsIDOXtvTTXsS8.htm](menace-under-otari-bestiary-items/QpOsIDOXtvTTXsS8.htm)|Staff|Bâton|officielle|
|[rj9VxT0ANFzZgmXI.htm](menace-under-otari-bestiary-items/rj9VxT0ANFzZgmXI.htm)|Evil Damage|Dégâts mauvais|officielle|
|[RX6HAClLi39MgLLI.htm](menace-under-otari-bestiary-items/RX6HAClLi39MgLLI.htm)|Swipe|Frappe transversale|officielle|
|[SKxww784aC4E2gzW.htm](menace-under-otari-bestiary-items/SKxww784aC4E2gzW.htm)|Sneak Attack|Attaque sournoise|officielle|
|[SL9owkerqt7KHiAD.htm](menace-under-otari-bestiary-items/SL9owkerqt7KHiAD.htm)|Spear|Lance|officielle|
|[soQbbQbVd6xMuJl6.htm](menace-under-otari-bestiary-items/soQbbQbVd6xMuJl6.htm)|Incorporeal|Intangible|officielle|
|[sXZ80MEcWSNN1Ct0.htm](menace-under-otari-bestiary-items/sXZ80MEcWSNN1Ct0.htm)|Spike Trap|Pièges à pics|officielle|
|[tC9lSVhNyxDRwaml.htm](menace-under-otari-bestiary-items/tC9lSVhNyxDRwaml.htm)|Wizard Spells|Sorts de magicien|libre|
|[tS9Kv6K4kwYFjHCT.htm](menace-under-otari-bestiary-items/tS9Kv6K4kwYFjHCT.htm)|Incorporeal|Intangible|officielle|
|[UedMQrrYjW7RxjuG.htm](menace-under-otari-bestiary-items/UedMQrrYjW7RxjuG.htm)|Crafting|Artisanat|officielle|
|[vBFHfrDtlZ5vYw2q.htm](menace-under-otari-bestiary-items/vBFHfrDtlZ5vYw2q.htm)|Athletics|Athlétisme|officielle|
|[VDL4WflLI1gxDylZ.htm](menace-under-otari-bestiary-items/VDL4WflLI1gxDylZ.htm)|Spine|Aiguille|officielle|
|[VeqnECTeAGCW8urK.htm](menace-under-otari-bestiary-items/VeqnECTeAGCW8urK.htm)|Low-Light Vision|Vision nocturne|officielle|
|[vnaySgTDfC0slaHW.htm](menace-under-otari-bestiary-items/vnaySgTDfC0slaHW.htm)|Giant Spider Venom|Venin d'araignée géante|officielle|
|[WpW23oqzbYlPDDuP.htm](menace-under-otari-bestiary-items/WpW23oqzbYlPDDuP.htm)|Slam Shut|Claquer|officielle|
|[xa524Pmx1LWbJGLL.htm](menace-under-otari-bestiary-items/xa524Pmx1LWbJGLL.htm)|Wizard Spells|Sorts de magicien|libre|
|[XfysylreSHBRTKzu.htm](menace-under-otari-bestiary-items/XfysylreSHBRTKzu.htm)|Performance|Représentation|officielle|
|[XiR8kBhdXvv7AE8L.htm](menace-under-otari-bestiary-items/XiR8kBhdXvv7AE8L.htm)|Athletics|Athlétisme|officielle|
|[xN6zu0iwyGGunmVu.htm](menace-under-otari-bestiary-items/xN6zu0iwyGGunmVu.htm)|Darkvision|Vision dans le noir|officielle|
|[yDzPXpPBRfYG5UfY.htm](menace-under-otari-bestiary-items/yDzPXpPBRfYG5UfY.htm)|Sneak Attack|Attaque sournoise|officielle|
|[YfiKyZzHH0XtfjFx.htm](menace-under-otari-bestiary-items/YfiKyZzHH0XtfjFx.htm)|Spray|Jet d'eau|officielle|
|[Zdg1lW7CYLRDYorG.htm](menace-under-otari-bestiary-items/Zdg1lW7CYLRDYorG.htm)|Claw|Griffe|officielle|
|[ZY8Teu6y1ruEGOAn.htm](menace-under-otari-bestiary-items/ZY8Teu6y1ruEGOAn.htm)|Pitfall|Chausse-trappe|officielle|
