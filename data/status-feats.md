# État de la traduction (feats)

 * **libre**: 3201
 * **officielle**: 839
 * **changé**: 1


Dernière mise à jour: 2023-07-12 20:28 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[class-08-vHKNJZPBxj66nJzZ.htm](feats/class-08-vHKNJZPBxj66nJzZ.htm)|Rupture Stomp|Piétinement cassant|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[ancestry-01-0198lXWmDdrVWolN.htm](feats/ancestry-01-0198lXWmDdrVWolN.htm)|Wind Pillow|Coussin de vent|libre|
|[ancestry-01-0DSCucjk9WZAw4xT.htm](feats/ancestry-01-0DSCucjk9WZAw4xT.htm)|Automaton Armament|Arsenal de l'automate|libre|
|[ancestry-01-0fTHqIqXCwGKiykR.htm](feats/ancestry-01-0fTHqIqXCwGKiykR.htm)|Sprite's Spark|Étincelle du sprite|libre|
|[ancestry-01-0qeYP84FfQueggkx.htm](feats/ancestry-01-0qeYP84FfQueggkx.htm)|Hard Tail|Queue dure|libre|
|[ancestry-01-0qshtd4mBcjFAxA8.htm](feats/ancestry-01-0qshtd4mBcjFAxA8.htm)|Cantorian Reinforcement|Renfort cantorien|libre|
|[ancestry-01-0YXRPqCaOQ3G73hh.htm](feats/ancestry-01-0YXRPqCaOQ3G73hh.htm)|Folksy Patter|Boniment campagnard|libre|
|[ancestry-01-13zaW8ZHRWnRe2pj.htm](feats/ancestry-01-13zaW8ZHRWnRe2pj.htm)|Hobgoblin Lore|Connaissance des hobgobelins|libre|
|[ancestry-01-1Doigqr1vBzg0tWU.htm](feats/ancestry-01-1Doigqr1vBzg0tWU.htm)|Silent Stone|Pierre silencieuse|libre|
|[ancestry-01-1HsH8hE79MDsi8kK.htm](feats/ancestry-01-1HsH8hE79MDsi8kK.htm)|Orc Warmask|Masque de guerre orc|libre|
|[ancestry-01-1MqvE1FL2mZRCnzo.htm](feats/ancestry-01-1MqvE1FL2mZRCnzo.htm)|Vestigial Wings|Ailes vestigielles|libre|
|[ancestry-01-1newzNV5nkLvZ9KE.htm](feats/ancestry-01-1newzNV5nkLvZ9KE.htm)|Handy with your Paws|Adroit de vos pattes|libre|
|[ancestry-01-1r514EjD9YrZJ5rk.htm](feats/ancestry-01-1r514EjD9YrZJ5rk.htm)|Android Lore|Connaissance des androïdes|libre|
|[ancestry-01-1RygexXEjCKuR3Ps.htm](feats/ancestry-01-1RygexXEjCKuR3Ps.htm)|Undaunted|Téméraire|libre|
|[ancestry-01-1tVC0mcxl8sTCg4U.htm](feats/ancestry-01-1tVC0mcxl8sTCg4U.htm)|Orc Weapon Familiarity|Familiarité avec les armes des orques|officielle|
|[ancestry-01-1xDMyQ8IHuhDHSXy.htm](feats/ancestry-01-1xDMyQ8IHuhDHSXy.htm)|Conrasu Weapon Familiarity|Familiarité avec les armes des conrasus|libre|
|[ancestry-01-1XuytsxHDUMgyVH1.htm](feats/ancestry-01-1XuytsxHDUMgyVH1.htm)|Climbing Tail|Queue de grimpeur|libre|
|[ancestry-01-23F2oqjL7SAMW3Ud.htm](feats/ancestry-01-23F2oqjL7SAMW3Ud.htm)|Ru-shi|Ru-shi|libre|
|[ancestry-01-254C4rqrE8APDfCf.htm](feats/ancestry-01-254C4rqrE8APDfCf.htm)|Orc Lore|Connaissance orque|libre|
|[ancestry-01-2abNd2shBjF2kz2S.htm](feats/ancestry-01-2abNd2shBjF2kz2S.htm)|Undead Empathy|Empathie morte-vivante|libre|
|[ancestry-01-2bNd89jYmEO8wSay.htm](feats/ancestry-01-2bNd89jYmEO8wSay.htm)|Extra Squishy|Extra spongieux|libre|
|[ancestry-01-2ebcYbg68pCZfAFQ.htm](feats/ancestry-01-2ebcYbg68pCZfAFQ.htm)|Halfling Weapon Familiarity|Familiarité avec les armes halfelines|officielle|
|[ancestry-01-2XmdYW8OsAvjGDG3.htm](feats/ancestry-01-2XmdYW8OsAvjGDG3.htm)|Radiant Circuitry|Circuiterie rayonnante|libre|
|[ancestry-01-30e9SdbmI3t5OJQE.htm](feats/ancestry-01-30e9SdbmI3t5OJQE.htm)|Deliberate Death|Mort délibérée|libre|
|[ancestry-01-3J0NxeHcA1h9eToK.htm](feats/ancestry-01-3J0NxeHcA1h9eToK.htm)|Creative Prodigy|Prodige créatif|libre|
|[ancestry-01-3zr5Gt5LgHsMNpSO.htm](feats/ancestry-01-3zr5Gt5LgHsMNpSO.htm)|Gnome Weapon Familiarity|Familiarité avec les armes gnomes|officielle|
|[ancestry-01-48X3xSWxI20RPOr9.htm](feats/ancestry-01-48X3xSWxI20RPOr9.htm)|Burrow Elocutionist|Orateur des terriers|officielle|
|[ancestry-01-49mBVNF4SK6iYdJm.htm](feats/ancestry-01-49mBVNF4SK6iYdJm.htm)|Unconventional Weaponry|Armement non conventionnel|officielle|
|[ancestry-01-4mLlHpD41101H0Iy.htm](feats/ancestry-01-4mLlHpD41101H0Iy.htm)|Changeling Lore|Connaissance des changelins|libre|
|[ancestry-01-4NKyZVkmWjDyyIYZ.htm](feats/ancestry-01-4NKyZVkmWjDyyIYZ.htm)|Elven Weapon Familiarity|Familiarité avec les armes elfiques|officielle|
|[ancestry-01-50CRpoP5XS1MVbu8.htm](feats/ancestry-01-50CRpoP5XS1MVbu8.htm)|Titan Slinger|Frondeur de titans|officielle|
|[ancestry-01-5EpkOj9CFOjt8vsK.htm](feats/ancestry-01-5EpkOj9CFOjt8vsK.htm)|Eyes of the Night|Yeux de la nuit|libre|
|[ancestry-01-5g7OFtvYQ7wPPJHC.htm](feats/ancestry-01-5g7OFtvYQ7wPPJHC.htm)|Fiendish Lore|Connaissance des fiélons|libre|
|[ancestry-01-5lZBacKOZOgIx4Pi.htm](feats/ancestry-01-5lZBacKOZOgIx4Pi.htm)|Eidetic Ear|Oreille éidétique|libre|
|[ancestry-01-5nPtGnXii1WTM5Wt.htm](feats/ancestry-01-5nPtGnXii1WTM5Wt.htm)|Faultspawn|Rejeton fautif|libre|
|[ancestry-01-5qJBjjitpHjkX5Wh.htm](feats/ancestry-01-5qJBjjitpHjkX5Wh.htm)|Embodied Legionary Subjectivity|Subjectivité du légionnaire incarné|libre|
|[ancestry-01-5vipJDU5hGs0SejD.htm](feats/ancestry-01-5vipJDU5hGs0SejD.htm)|Hyena Familiar|Familier hyène|libre|
|[ancestry-01-5VkVWZq7ZqH6RaAW.htm](feats/ancestry-01-5VkVWZq7ZqH6RaAW.htm)|Voice Of The Night|Voix de la nuit|libre|
|[ancestry-01-5VWItcyt3Mx6mbSK.htm](feats/ancestry-01-5VWItcyt3Mx6mbSK.htm)|Grippli Lore|Connaissance des gripplis|libre|
|[ancestry-01-6dymMIqs3dAFqnyR.htm](feats/ancestry-01-6dymMIqs3dAFqnyR.htm)|Plumekith|Plumes serrées|libre|
|[ancestry-01-6EgHimhCuS8L0xDE.htm](feats/ancestry-01-6EgHimhCuS8L0xDE.htm)|Explosive Savant|Explosion savante|libre|
|[ancestry-01-6ENHSzqg88J6dri6.htm](feats/ancestry-01-6ENHSzqg88J6dri6.htm)|Elemental Eyes|Yeux élémentaires|libre|
|[ancestry-01-6oJ1EdANHgYCfCeF.htm](feats/ancestry-01-6oJ1EdANHgYCfCeF.htm)|Sharp Fangs|Crocs aiguisés|libre|
|[ancestry-01-6ZNSMR0lRSLwBJBe.htm](feats/ancestry-01-6ZNSMR0lRSLwBJBe.htm)|Spelunker|Spéléologue|libre|
|[ancestry-01-7BVl9lFf0wuTjBgM.htm](feats/ancestry-01-7BVl9lFf0wuTjBgM.htm)|Elf Atavism|Atavisme elfique|officielle|
|[ancestry-01-7lXCPeGMB3RrDVdS.htm](feats/ancestry-01-7lXCPeGMB3RrDVdS.htm)|Hard To Fool|Difficile à tromper|libre|
|[ancestry-01-7qBuzHY8kEG8SdEP.htm](feats/ancestry-01-7qBuzHY8kEG8SdEP.htm)|Cat's Luck|Chance féline|libre|
|[ancestry-01-8DDwqrYHie33cf6d.htm](feats/ancestry-01-8DDwqrYHie33cf6d.htm)|Genie Weapon Familiarity|Familiarité avec les armes des génies|libre|
|[ancestry-01-8nF3r3NHW2uSRgwb.htm](feats/ancestry-01-8nF3r3NHW2uSRgwb.htm)|Devil's Advocate|Avocat du diable|libre|
|[ancestry-01-8nz6gvymeTvEfdo0.htm](feats/ancestry-01-8nz6gvymeTvEfdo0.htm)|Smashing Tail|Queue fouettante|libre|
|[ancestry-01-8rKlptZ7ArtJ6cI8.htm](feats/ancestry-01-8rKlptZ7ArtJ6cI8.htm)|Vanth's Weapon Familiarity|Familiarité avec les armes des vanths|libre|
|[ancestry-01-95PmHIS041KiYIks.htm](feats/ancestry-01-95PmHIS041KiYIks.htm)|Reassuring Presence|Présence rassurante|libre|
|[ancestry-01-9bnkx6VgcO5mOk5b.htm](feats/ancestry-01-9bnkx6VgcO5mOk5b.htm)|Dwarven Lore|Connaissance des nains|officielle|
|[ancestry-01-9eDA2RLnrBBlCvce.htm](feats/ancestry-01-9eDA2RLnrBBlCvce.htm)|Catch the Details|Capter les détails|libre|
|[ancestry-01-9OdLdxvH5M9FyYSm.htm](feats/ancestry-01-9OdLdxvH5M9FyYSm.htm)|Corgi Mount|Monture corgi|libre|
|[ancestry-01-9Orkgjgfx8AILuqD.htm](feats/ancestry-01-9Orkgjgfx8AILuqD.htm)|Warren Navigator|Navigateur de terrier|libre|
|[ancestry-01-9t4x8vRmQikEl4vP.htm](feats/ancestry-01-9t4x8vRmQikEl4vP.htm)|Well-Met Traveler|Voyageur affable|libre|
|[ancestry-01-A1RzkLCj7mXJI1IY.htm](feats/ancestry-01-A1RzkLCj7mXJI1IY.htm)|Unassuming Dedication|Acharnement sans prétention|libre|
|[ancestry-01-A9HQ2bMAge2aGgWx.htm](feats/ancestry-01-A9HQ2bMAge2aGgWx.htm)|Prairie Rider|Chevaucheur des prairies|libre|
|[ancestry-01-AAYN41WtyMZaLV1N.htm](feats/ancestry-01-AAYN41WtyMZaLV1N.htm)|Native Waters|Eaux natives|libre|
|[ancestry-01-af2fSePLvqMNNp0r.htm](feats/ancestry-01-af2fSePLvqMNNp0r.htm)|Wildborn Magic|Magie sauvage|libre|
|[ancestry-01-AFXY5eM4IQuo5Ygj.htm](feats/ancestry-01-AFXY5eM4IQuo5Ygj.htm)|Speak With Bats|Parler avec les chauves-souris|libre|
|[ancestry-01-aGpH394T1xr2ey88.htm](feats/ancestry-01-aGpH394T1xr2ey88.htm)|Pack Hunter|Chasseur en meute|libre|
|[ancestry-01-AgR1OPBHDvwV5wKD.htm](feats/ancestry-01-AgR1OPBHDvwV5wKD.htm)|Unexpected Shift|Phasage inattendu|libre|
|[ancestry-01-AjdmoGFoSIyx1mxd.htm](feats/ancestry-01-AjdmoGFoSIyx1mxd.htm)|Intuitive Cooperation|Coopération intuitive|libre|
|[ancestry-01-AJHMnoK9Xp1DTj4g.htm](feats/ancestry-01-AJHMnoK9Xp1DTj4g.htm)|Clan Lore|Connaissances du clan|libre|
|[ancestry-01-akCO62yBCJLjjCZJ.htm](feats/ancestry-01-akCO62yBCJLjjCZJ.htm)|Celestial Eyes|Yeux célestes|libre|
|[ancestry-01-akrj9t0lTrndmf0q.htm](feats/ancestry-01-akrj9t0lTrndmf0q.htm)|Canopy Sight|Vision de la canopée|libre|
|[ancestry-01-AKrxQ2JuDObM8coY.htm](feats/ancestry-01-AKrxQ2JuDObM8coY.htm)|Animal Senses|Sens animaux|libre|
|[ancestry-01-AL2KD88ddEl5AetZ.htm](feats/ancestry-01-AL2KD88ddEl5AetZ.htm)|Saber Teeth|Dents de sabre|libre|
|[ancestry-01-AlhyF8LCW011w9kq.htm](feats/ancestry-01-AlhyF8LCW011w9kq.htm)|Bouncy Goblin|Gobelin rebondissant|libre|
|[ancestry-01-AMbYEv9rUt2fcR7F.htm](feats/ancestry-01-AMbYEv9rUt2fcR7F.htm)|Energy Beam|Rayon d'énergie|libre|
|[ancestry-01-AmFv3ClkAVRowHLI.htm](feats/ancestry-01-AmFv3ClkAVRowHLI.htm)|Tengu Weapon Familiarity|Familiarité avec les armes des tengus|libre|
|[ancestry-01-an8sq0RaJ8PW81EW.htm](feats/ancestry-01-an8sq0RaJ8PW81EW.htm)|Puncturing Horn|Corne perforante|libre|
|[ancestry-01-AogIo1gHLdz7DyHx.htm](feats/ancestry-01-AogIo1gHLdz7DyHx.htm)|Forlorn|Délaissé|officielle|
|[ancestry-01-aOhZUMCn0o2ZMkdW.htm](feats/ancestry-01-aOhZUMCn0o2ZMkdW.htm)|Crunch|Croquer|libre|
|[ancestry-01-AoibfCoLi2FaTvcH.htm](feats/ancestry-01-AoibfCoLi2FaTvcH.htm)|Hidden Thorn|Épines cachées|libre|
|[ancestry-01-ARBIhYAREcytOIaL.htm](feats/ancestry-01-ARBIhYAREcytOIaL.htm)|Helpful Poppet|Poupée serviable|libre|
|[ancestry-01-arR15QoJH3xokw12.htm](feats/ancestry-01-arR15QoJH3xokw12.htm)|Wind Tempered|Habitué au vent|libre|
|[ancestry-01-augNhQ51eSlZyead.htm](feats/ancestry-01-augNhQ51eSlZyead.htm)|Sneaky|Discret|libre|
|[ancestry-01-b60ZCgKoaVDgMhBk.htm](feats/ancestry-01-b60ZCgKoaVDgMhBk.htm)|Kitsune Lore|Connaissance des kitsunes|libre|
|[ancestry-01-bEh5qUgX5eFaQwzU.htm](feats/ancestry-01-bEh5qUgX5eFaQwzU.htm)|Catfolk Weapon Familiarity|Familiarité avec les armes des hommes-félins|libre|
|[ancestry-01-BF5B2kDrSruQpqgS.htm](feats/ancestry-01-BF5B2kDrSruQpqgS.htm)|Pyrophilic Recovery|Récupération pyrophilique|libre|
|[ancestry-01-bLSd0y06QRjDYYMw.htm](feats/ancestry-01-bLSd0y06QRjDYYMw.htm)|Mental Sustenance|Subsistance mentale|libre|
|[ancestry-01-Bn9yK3FBndHkbVsu.htm](feats/ancestry-01-Bn9yK3FBndHkbVsu.htm)|Spine Stabber|Poignardeur de piquants|libre|
|[ancestry-01-Bni2NcuQn6Z546RE.htm](feats/ancestry-01-Bni2NcuQn6Z546RE.htm)|Twitchy|Nerveux|libre|
|[ancestry-01-BNIAfaJPNCWKs3FN.htm](feats/ancestry-01-BNIAfaJPNCWKs3FN.htm)|Nocturnal Grippli|Grippli nocturne|libre|
|[ancestry-01-BoqMvGy1jXpsaBbo.htm](feats/ancestry-01-BoqMvGy1jXpsaBbo.htm)|Viking Shieldbearer|Porteur de bouclier viking|libre|
|[ancestry-01-BRGdj5leyVFRHEUM.htm](feats/ancestry-01-BRGdj5leyVFRHEUM.htm)|Waxed Feathers|Plumes cirées|libre|
|[ancestry-01-bWyY9NtLU5wXr03y.htm](feats/ancestry-01-bWyY9NtLU5wXr03y.htm)|Storm's Lash|Fouet de la tempête|libre|
|[ancestry-01-C1R4wd6G46CAVIn7.htm](feats/ancestry-01-C1R4wd6G46CAVIn7.htm)|Unburdened Iron|Fer allégé|officielle|
|[ancestry-01-Cb44J1g1nO43DEBd.htm](feats/ancestry-01-Cb44J1g1nO43DEBd.htm)|Ceremony of the Evened Hand|Cérémonie de la main tendue|libre|
|[ancestry-01-CCmiEmS7ZgyQUfhn.htm](feats/ancestry-01-CCmiEmS7ZgyQUfhn.htm)|Squawk!|Croa !|libre|
|[ancestry-01-ccWf2F5DqiqFwiQ1.htm](feats/ancestry-01-ccWf2F5DqiqFwiQ1.htm)|Gnome Polyglot|Polyglotte gnome|libre|
|[ancestry-01-CF43oiymCFVVEkVS.htm](feats/ancestry-01-CF43oiymCFVVEkVS.htm)|Automaton Lore|Connaissance des automates|libre|
|[ancestry-01-ch3BWG3Z4kDEmuZW.htm](feats/ancestry-01-ch3BWG3Z4kDEmuZW.htm)|Alghollthu Bound|Lié aux alghollthus|libre|
|[ancestry-01-cilZUszwjSGB4p1W.htm](feats/ancestry-01-cilZUszwjSGB4p1W.htm)|Reinforced Chassis|Chassis renforcé|libre|
|[ancestry-01-cixf1uAQF2Y3w1Qx.htm](feats/ancestry-01-cixf1uAQF2Y3w1Qx.htm)|Nightvision Adaptation|Adaptation à la vision de nuit|libre|
|[ancestry-01-ClVSyZxWk5L5KVLd.htm](feats/ancestry-01-ClVSyZxWk5L5KVLd.htm)|Aberration Kinship|Parenté aberrante|libre|
|[ancestry-01-csoGdGuWasEw3boD.htm](feats/ancestry-01-csoGdGuWasEw3boD.htm)|Saoc Astrology|Astrologie saoc|libre|
|[ancestry-01-CUrxG9CzT1hSfuhP.htm](feats/ancestry-01-CUrxG9CzT1hSfuhP.htm)|Web Weaver|Tisseur de toiles|libre|
|[ancestry-01-CvtJ98EZvBGSCLOX.htm](feats/ancestry-01-CvtJ98EZvBGSCLOX.htm)|Grippli Weapon Familiarity|Familiarité avec les armes des gripplis|libre|
|[ancestry-01-CXS0ryG2SODSobm9.htm](feats/ancestry-01-CXS0ryG2SODSobm9.htm)|Dwarven Weapon Familiarity|Familiarité avec les armes naines|officielle|
|[ancestry-01-d1FTY5ai9KjpkX59.htm](feats/ancestry-01-d1FTY5ai9KjpkX59.htm)|Star Orb|Orbe étoilé|libre|
|[ancestry-01-D2tyUKQiDSq3JO1Z.htm](feats/ancestry-01-D2tyUKQiDSq3JO1Z.htm)|Vigorous Health|Santé vigoureuse|libre|
|[ancestry-01-D3SuA5MaKucO1flE.htm](feats/ancestry-01-D3SuA5MaKucO1flE.htm)|General Training|Formation généraliste|officielle|
|[ancestry-01-d8SK0BQmTZiJ0VT7.htm](feats/ancestry-01-d8SK0BQmTZiJ0VT7.htm)|Community Knowledge|Savoir communautaire|libre|
|[ancestry-01-db0RHtFGhCfMx8vT.htm](feats/ancestry-01-db0RHtFGhCfMx8vT.htm)|Internal Cohesion|Cohésion interne|libre|
|[ancestry-01-DGtUIMliflzGXc6E.htm](feats/ancestry-01-DGtUIMliflzGXc6E.htm)|Ghost Hunter|Chasseur de fantômes|libre|
|[ancestry-01-DmcJtpMBSh3R5MHI.htm](feats/ancestry-01-DmcJtpMBSh3R5MHI.htm)|Quick Shape|Forme rapide|libre|
|[ancestry-01-dmEMftRe8P5iPDKo.htm](feats/ancestry-01-dmEMftRe8P5iPDKo.htm)|Brinesoul|Âme-saumure|libre|
|[ancestry-01-Dq4JSejEdCzGNeTc.htm](feats/ancestry-01-Dq4JSejEdCzGNeTc.htm)|Arcane Tattoos|Tatouages arcaniques|libre|
|[ancestry-01-Ds1waj86N4Z2gCMN.htm](feats/ancestry-01-Ds1waj86N4Z2gCMN.htm)|Winter Cat Senses|Perception du chat hivernal|libre|
|[ancestry-01-dWGa6cFSVrASTEfd.htm](feats/ancestry-01-dWGa6cFSVrASTEfd.htm)|Seedpod|Cosse|libre|
|[ancestry-01-DwnnmTNOvpLbp7jJ.htm](feats/ancestry-01-DwnnmTNOvpLbp7jJ.htm)|Courteous Comeback|Retour courtois|libre|
|[ancestry-01-e0Gz3tjd55A5ggYK.htm](feats/ancestry-01-e0Gz3tjd55A5ggYK.htm)|Alabaster Eyes|Yeux d'albâtre|libre|
|[ancestry-01-E1sjYRb4zsZVrzrN.htm](feats/ancestry-01-E1sjYRb4zsZVrzrN.htm)|Innocuous|Inoffensif|libre|
|[ancestry-01-E5TRKeB63rC910PC.htm](feats/ancestry-01-E5TRKeB63rC910PC.htm)|Nagaji Lore|Connaissance des nagajis|libre|
|[ancestry-01-ecJms2jtd6cZ5rQK.htm](feats/ancestry-01-ecJms2jtd6cZ5rQK.htm)|Round Ears|Oreilles arrondies|libre|
|[ancestry-01-eCWQU16hRLfN1KaX.htm](feats/ancestry-01-eCWQU16hRLfN1KaX.htm)|Ancestral Linguistics|Linguistique ancestrale|libre|
|[ancestry-01-eD3KZJV8ACLt2xns.htm](feats/ancestry-01-eD3KZJV8ACLt2xns.htm)|Elven Verve|Verve elfique|libre|
|[ancestry-01-ED58GzldWb82yc2q.htm](feats/ancestry-01-ED58GzldWb82yc2q.htm)|Animal Accomplice|Complice animal|officielle|
|[ancestry-01-EEJKztPOpy5utha9.htm](feats/ancestry-01-EEJKztPOpy5utha9.htm)|Cleansing Subroutine|Sous-routine de nettoyage|libre|
|[ancestry-01-eEVZ19G2BAnHQZpa.htm](feats/ancestry-01-eEVZ19G2BAnHQZpa.htm)|Emotionless|Sans émotion|libre|
|[ancestry-01-EFhh3AenX7wtAmrs.htm](feats/ancestry-01-EFhh3AenX7wtAmrs.htm)|Orc Superstition|Superstition orque|officielle|
|[ancestry-01-ehvHedIwPcxq9cRt.htm](feats/ancestry-01-ehvHedIwPcxq9cRt.htm)|Sure Feet|Pied assuré|officielle|
|[ancestry-01-eMmdBpbMrpIuGowo.htm](feats/ancestry-01-eMmdBpbMrpIuGowo.htm)|Snare Setter|Poseur de pièges artisanaux|libre|
|[ancestry-01-enRKfPyCLU5FMUOX.htm](feats/ancestry-01-enRKfPyCLU5FMUOX.htm)|Proximity Alert|Alerte de proximité|libre|
|[ancestry-01-eqBWab1J5Be24YAl.htm](feats/ancestry-01-eqBWab1J5Be24YAl.htm)|Catfolk Lore|Connaissance des hommes-félins|libre|
|[ancestry-01-eSP2938INGUG9b3w.htm](feats/ancestry-01-eSP2938INGUG9b3w.htm)|Musetouched|Touché par la muse|libre|
|[ancestry-01-eUyZBi8vV5QDxOXD.htm](feats/ancestry-01-eUyZBi8vV5QDxOXD.htm)|Form Of The Fiend|Forme du fiélon|libre|
|[ancestry-01-eWUTE7Ln3MwX6uer.htm](feats/ancestry-01-eWUTE7Ln3MwX6uer.htm)|Axiomatic Lore|Connaissance axiomatique|libre|
|[ancestry-01-EXVHePH8alsTZ5TB.htm](feats/ancestry-01-EXVHePH8alsTZ5TB.htm)|Hellspawn|Rejeton infernal|libre|
|[ancestry-01-eZsMl6rx8Bv6Ccnp.htm](feats/ancestry-01-eZsMl6rx8Bv6Ccnp.htm)|Shiny Button Eyes|Yeux en bouton brillant|libre|
|[ancestry-01-F6C25qZ9UNYPw7pj.htm](feats/ancestry-01-F6C25qZ9UNYPw7pj.htm)|Critter Shape|Forme de bestiole|libre|
|[ancestry-01-f6VPqhOPW9XfBKDr.htm](feats/ancestry-01-f6VPqhOPW9XfBKDr.htm)|Virga May|Vierge Virga|libre|
|[ancestry-01-Ffi8L4EDO5OH5tpA.htm](feats/ancestry-01-Ffi8L4EDO5OH5tpA.htm)|Nimble Hooves|Sabots agiles|libre|
|[ancestry-01-FhCsnHjdIUyKteCM.htm](feats/ancestry-01-FhCsnHjdIUyKteCM.htm)|Lizardfolk Lore|Connaissance des hommes-lézards|libre|
|[ancestry-01-FHK4ZEl8SkGOpKdF.htm](feats/ancestry-01-FHK4ZEl8SkGOpKdF.htm)|Mirror-Risen|Issu d'un miroir|libre|
|[ancestry-01-fLDhS0e6fBDjiCUA.htm](feats/ancestry-01-fLDhS0e6fBDjiCUA.htm)|Bone Magic|Magie osseuse|libre|
|[ancestry-01-FnGOkNyLF4w3FyqZ.htm](feats/ancestry-01-FnGOkNyLF4w3FyqZ.htm)|Fang Sharpener|Aiguiseur de crocs|libre|
|[ancestry-01-FWCULKnVXhSPL0ST.htm](feats/ancestry-01-FWCULKnVXhSPL0ST.htm)|Fey Fellowship|Camaraderie féerique|officielle|
|[ancestry-01-FwMTsYc87uU2q4Ox.htm](feats/ancestry-01-FwMTsYc87uU2q4Ox.htm)|Beastbrood|Bête éduquée|libre|
|[ancestry-01-fYVFBnv9aVHv1UNg.htm](feats/ancestry-01-fYVFBnv9aVHv1UNg.htm)|Leshy Superstition|Superstition léchie|libre|
|[ancestry-01-FZuQPFnQ5UkBWLU9.htm](feats/ancestry-01-FZuQPFnQ5UkBWLU9.htm)|Steady On Stone|Stable sur la pierre|libre|
|[ancestry-01-g388ImzpenYBoiEF.htm](feats/ancestry-01-g388ImzpenYBoiEF.htm)|City Scavenger|Charognard urbain|officielle|
|[ancestry-01-g3oJlWGHc74qX2z5.htm](feats/ancestry-01-g3oJlWGHc74qX2z5.htm)|Lawbringer|Porteur de loi|libre|
|[ancestry-01-G8WgbujrrnMQUQ8E.htm](feats/ancestry-01-G8WgbujrrnMQUQ8E.htm)|Share Thoughts|Partager les pensées|libre|
|[ancestry-01-gC5EnlP38t1vTlWt.htm](feats/ancestry-01-gC5EnlP38t1vTlWt.htm)|Esteemed Visitor|Visiteur estimé|libre|
|[ancestry-01-GEvaoKgQteMrd4ub.htm](feats/ancestry-01-GEvaoKgQteMrd4ub.htm)|Reptile Speaker|Locuteur reptilien|libre|
|[ancestry-01-GF9kkULUYowgjEWM.htm](feats/ancestry-01-GF9kkULUYowgjEWM.htm)|Rough Rider|Cavalier farouche|officielle|
|[ancestry-01-gJDTS7eeIxZws5Lr.htm](feats/ancestry-01-gJDTS7eeIxZws5Lr.htm)|Adhyabhau|Adhyabhau|libre|
|[ancestry-01-GjQZMmw2sz8OyLxj.htm](feats/ancestry-01-GjQZMmw2sz8OyLxj.htm)|Tengu Lore|Connaissance des tengus|libre|
|[ancestry-01-gKDRnsBPBdhJB0FI.htm](feats/ancestry-01-gKDRnsBPBdhJB0FI.htm)|Avenge in Glory|Revanche dans la gloire|libre|
|[ancestry-01-gozOYxLVx7PQvOSj.htm](feats/ancestry-01-gozOYxLVx7PQvOSj.htm)|Fey Cantrips|Tours de magie féeriques|libre|
|[ancestry-01-GPsYtPdkAkk710F3.htm](feats/ancestry-01-GPsYtPdkAkk710F3.htm)|Suli-jann|Suli-jann|libre|
|[ancestry-01-gsDDw5KAb7qlPkms.htm](feats/ancestry-01-gsDDw5KAb7qlPkms.htm)|Shrouded Magic|Magie voilée|libre|
|[ancestry-01-gxQheZ4xuDWwyzy4.htm](feats/ancestry-01-gxQheZ4xuDWwyzy4.htm)|Pitborn|Né de la fosse|libre|
|[ancestry-01-GYdLf1LhvSUeu95Y.htm](feats/ancestry-01-GYdLf1LhvSUeu95Y.htm)|Harmlessly Cute|Inoffensivement mignon|libre|
|[ancestry-01-H95Gh2nKUp9NKFuR.htm](feats/ancestry-01-H95Gh2nKUp9NKFuR.htm)|Shadow Blending|Fondre dans l'ombre|libre|
|[ancestry-01-HAMy8UiUqCGFdhrf.htm](feats/ancestry-01-HAMy8UiUqCGFdhrf.htm)|Lemma Of Vision|Axiome de vision|libre|
|[ancestry-01-HauCZuLvIthFe2we.htm](feats/ancestry-01-HauCZuLvIthFe2we.htm)|Draconic Sycophant|Sycophante draconique|libre|
|[ancestry-01-hc4lWhWekIVb0wjL.htm](feats/ancestry-01-hc4lWhWekIVb0wjL.htm)|Pack Rat|Rat empaqueteur|libre|
|[ancestry-01-Hey5rucsel7apOOi.htm](feats/ancestry-01-Hey5rucsel7apOOi.htm)|Goloma Courage|Courage goloma|libre|
|[ancestry-01-hkMQGiTJCLVAWHy0.htm](feats/ancestry-01-hkMQGiTJCLVAWHy0.htm)|Surface Culture|Culture de la surface|libre|
|[ancestry-01-Hmgy0GJKIawAiqHE.htm](feats/ancestry-01-Hmgy0GJKIawAiqHE.htm)|Maiden's Mending|Guérison de la vierge|libre|
|[ancestry-01-HOUHs5rahwIsQoBf.htm](feats/ancestry-01-HOUHs5rahwIsQoBf.htm)|Stonecunning|Connaissance de la pierre|officielle|
|[ancestry-01-HOXxEa7sAeAxpKHb.htm](feats/ancestry-01-HOXxEa7sAeAxpKHb.htm)|Strix Lore|Connaissance des strix|libre|
|[ancestry-01-hSzNtRNwrma81Eeq.htm](feats/ancestry-01-hSzNtRNwrma81Eeq.htm)|Life-Giving Magic|Magie donneuse de vie|libre|
|[ancestry-01-HTy9bQsVkKnS8bLT.htm](feats/ancestry-01-HTy9bQsVkKnS8bLT.htm)|As in Life, So in Death|Dans la vie comme dans la mort|libre|
|[ancestry-01-hxCqQPjlyVI57vQt.htm](feats/ancestry-01-hxCqQPjlyVI57vQt.htm)|Gravesight|Vision du tombeau|libre|
|[ancestry-01-Hy8SPadSjukKq078.htm](feats/ancestry-01-Hy8SPadSjukKq078.htm)|Sinister Appearance|Apparence sinistre|libre|
|[ancestry-01-HZJqMESaEHTfefz3.htm](feats/ancestry-01-HZJqMESaEHTfefz3.htm)|Cindersoul|Âme-cendre|libre|
|[ancestry-01-i0iyW1I7TylEgpV6.htm](feats/ancestry-01-i0iyW1I7TylEgpV6.htm)|Vampire Lore|Connaissance des vampires|libre|
|[ancestry-01-i5W5aGWEiyo1vt2d.htm](feats/ancestry-01-i5W5aGWEiyo1vt2d.htm)|Leshy Lore|Connaissance des léchis|libre|
|[ancestry-01-IAXWdFXwjFChojeb.htm](feats/ancestry-01-IAXWdFXwjFChojeb.htm)|Harmless Doll|Poupée inoffensive|libre|
|[ancestry-01-ICLUKJc9P0LgwVyt.htm](feats/ancestry-01-ICLUKJc9P0LgwVyt.htm)|Brightsoul|Âme-éclat|libre|
|[ancestry-01-IElFaS5i10MFYIvq.htm](feats/ancestry-01-IElFaS5i10MFYIvq.htm)|Kobold Lore|Connaissance des kobolds|libre|
|[ancestry-01-IFvjnLMw3ht8f84U.htm](feats/ancestry-01-IFvjnLMw3ht8f84U.htm)|Callow May|Vierge novice|libre|
|[ancestry-01-Iqa96LHn3Bs2xEJA.htm](feats/ancestry-01-Iqa96LHn3Bs2xEJA.htm)|Startling Appearance (Fleshwarp)|Apparition étonnante|libre|
|[ancestry-01-Iqv8qj7ym63YjexN.htm](feats/ancestry-01-Iqv8qj7ym63YjexN.htm)|Catfolk Dance|Danse homme-félin|libre|
|[ancestry-01-iRztLGEK5OfjZTPg.htm](feats/ancestry-01-iRztLGEK5OfjZTPg.htm)|Remorseless Lash|Fouetter sans remord|libre|
|[ancestry-01-itjTPh76mZfJCBxQ.htm](feats/ancestry-01-itjTPh76mZfJCBxQ.htm)|Marsh Runner|Coureur des marais|libre|
|[ancestry-01-izuErgHfh90KctAL.htm](feats/ancestry-01-izuErgHfh90KctAL.htm)|Fetchling Lore|Connaissance des fetchelins|libre|
|[ancestry-01-j54VJmwwAQZBlS6J.htm](feats/ancestry-01-j54VJmwwAQZBlS6J.htm)|Anadi Lore|Connaissance des anadis|libre|
|[ancestry-01-J7BtszszVxpETMD7.htm](feats/ancestry-01-J7BtszszVxpETMD7.htm)|Retractable Claws|Griffes rétractiles|libre|
|[ancestry-01-jEJ6AWCctirMT7p0.htm](feats/ancestry-01-jEJ6AWCctirMT7p0.htm)|Brine May|Vierge de saumure|libre|
|[ancestry-01-jmW8aZ5JGH3m6dL6.htm](feats/ancestry-01-jmW8aZ5JGH3m6dL6.htm)|Warren Friend|Ami des terriers|libre|
|[ancestry-01-jNDjLsqpq13RqzD4.htm](feats/ancestry-01-jNDjLsqpq13RqzD4.htm)|Studious Magic|Magie studieuse|libre|
|[ancestry-01-JoeepCWheQChcQ9s.htm](feats/ancestry-01-JoeepCWheQChcQ9s.htm)|Lavasoul|Âme-lave|libre|
|[ancestry-01-jOwZMv5jtwdKSfLS.htm](feats/ancestry-01-jOwZMv5jtwdKSfLS.htm)|Sensitive Nose|Nez sensible|libre|
|[ancestry-01-JP5pptkl1Fx1JK4m.htm](feats/ancestry-01-JP5pptkl1Fx1JK4m.htm)|Iron Fists|Poings de fer|libre|
|[ancestry-01-JQTP0XdI1XVAvBIn.htm](feats/ancestry-01-JQTP0XdI1XVAvBIn.htm)|Ghoran Lore|Connaissance des ghorans|libre|
|[ancestry-01-JS24EjgLYcHB9E3T.htm](feats/ancestry-01-JS24EjgLYcHB9E3T.htm)|Nanite Surge|Poussée nanite|libre|
|[ancestry-01-jxUPlkB2kFuZKXef.htm](feats/ancestry-01-jxUPlkB2kFuZKXef.htm)|Lesser Enhance Venom|Amélioration du venin inférieure|libre|
|[ancestry-01-k31sg0xBIwvkfWyg.htm](feats/ancestry-01-k31sg0xBIwvkfWyg.htm)|Reptile Rider|Chevaucheur de reptile|libre|
|[ancestry-01-kBxgo589ctJsBwJj.htm](feats/ancestry-01-kBxgo589ctJsBwJj.htm)|Conrasu Lore|Connaissance des conrasus|libre|
|[ancestry-01-kCO4r8NOm8E2T3eH.htm](feats/ancestry-01-kCO4r8NOm8E2T3eH.htm)|Nimble Elf|Elfe preste|officielle|
|[ancestry-01-kDklfrprKTuTpcEE.htm](feats/ancestry-01-kDklfrprKTuTpcEE.htm)|Shadow of the Wilds|Ombre des étendues sauvages|libre|
|[ancestry-01-keMP6xVg4fMloczj.htm](feats/ancestry-01-keMP6xVg4fMloczj.htm)|Cel Rau|Cel Rau|libre|
|[ancestry-01-kJfLBPtiVi5LQu0v.htm](feats/ancestry-01-kJfLBPtiVi5LQu0v.htm)|Nocturnal Charm|Charme nocturne|libre|
|[ancestry-01-KKUDZUbX3nDdME4K.htm](feats/ancestry-01-KKUDZUbX3nDdME4K.htm)|Watchful Gaze|Regard vigilant|libre|
|[ancestry-01-kqnFdIhToKTnOpMl.htm](feats/ancestry-01-kqnFdIhToKTnOpMl.htm)|Dream May|Vierge onirique|libre|
|[ancestry-01-kqRFoXfErUFEndIs.htm](feats/ancestry-01-kqRFoXfErUFEndIs.htm)|Hydraulic Deflection|Déflection hydraulique|libre|
|[ancestry-01-KQVE4FsDd9RFpWRz.htm](feats/ancestry-01-KQVE4FsDd9RFpWRz.htm)|Improvisational Defender|Défenseur improvisé|libre|
|[ancestry-01-KRMdfPcNCE7AVsEo.htm](feats/ancestry-01-KRMdfPcNCE7AVsEo.htm)|Scholar's Inheritance|Héritage érudit|libre|
|[ancestry-01-KuD4Yplwcdolhjsu.htm](feats/ancestry-01-KuD4Yplwcdolhjsu.htm)|Overlooked Mastermind|Génie négligé|libre|
|[ancestry-01-KXUVAI6SDtxwjO7t.htm](feats/ancestry-01-KXUVAI6SDtxwjO7t.htm)|Web Walker|Baladeur de toile|libre|
|[ancestry-01-KYTSvAEqK7KAyVwi.htm](feats/ancestry-01-KYTSvAEqK7KAyVwi.htm)|Hunter's Defense|Défense du chasseur|libre|
|[ancestry-01-Kz4MLcrfXFrdKhyS.htm](feats/ancestry-01-Kz4MLcrfXFrdKhyS.htm)|Aquatic Eyes|Yeux aquatiques|libre|
|[ancestry-01-l0VBbymCVT1Qz9t9.htm](feats/ancestry-01-l0VBbymCVT1Qz9t9.htm)|Swift|Véloce|libre|
|[ancestry-01-L7fhh2RTCq4FlVSN.htm](feats/ancestry-01-L7fhh2RTCq4FlVSN.htm)|Strix Defender|Défenseur strix|libre|
|[ancestry-01-Lb8mrOF3W2VGSOpA.htm](feats/ancestry-01-Lb8mrOF3W2VGSOpA.htm)|Arcane Communication|Communication arcanique|libre|
|[ancestry-01-LbyNDCxFEkjp0iG7.htm](feats/ancestry-01-LbyNDCxFEkjp0iG7.htm)|Slink|Déplacement furtif|libre|
|[ancestry-01-liqDAd5xk2b3xzeE.htm](feats/ancestry-01-liqDAd5xk2b3xzeE.htm)|Quah Bond|Lien Quah|libre|
|[ancestry-01-lLO0WSxE3tO3Ovsq.htm](feats/ancestry-01-lLO0WSxE3tO3Ovsq.htm)|Whitecape|Blanchecape|libre|
|[ancestry-01-LLQ5SO2c44uXDFJk.htm](feats/ancestry-01-LLQ5SO2c44uXDFJk.htm)|Tail Whip|Queue fouet|libre|
|[ancestry-01-lLZ9B0KeMDf3BNuA.htm](feats/ancestry-01-lLZ9B0KeMDf3BNuA.htm)|Rivethun Disciple|Disciple rivethun|libre|
|[ancestry-01-lqhhvDJAjPw6LZy5.htm](feats/ancestry-01-lqhhvDJAjPw6LZy5.htm)|Smokesoul|Âme-fumée|libre|
|[ancestry-01-Lug2p9E065L05Rhi.htm](feats/ancestry-01-Lug2p9E065L05Rhi.htm)|Story Crooner|Charmeur d'histoire|libre|
|[ancestry-01-LUXBuTAuK1glHOkJ.htm](feats/ancestry-01-LUXBuTAuK1glHOkJ.htm)|Fiendish Eyes|Yeux fiélons|libre|
|[ancestry-01-LvVg83ZDj8mabcWF.htm](feats/ancestry-01-LvVg83ZDj8mabcWF.htm)|Clan Pistol|Pistolet de clan|libre|
|[ancestry-01-lwLcUHQMOqfaNND4.htm](feats/ancestry-01-lwLcUHQMOqfaNND4.htm)|Cooperative Nature|Nature coopérative|officielle|
|[ancestry-01-lwzspz4dktZLgqlY.htm](feats/ancestry-01-lwzspz4dktZLgqlY.htm)|Wash Out|Purge rapide|libre|
|[ancestry-01-M41PGiFlLE2tByUL.htm](feats/ancestry-01-M41PGiFlLE2tByUL.htm)|Jungle Strider|Arpenteur de la jungle|libre|
|[ancestry-01-M5195FvfCN3X7gi9.htm](feats/ancestry-01-M5195FvfCN3X7gi9.htm)|Tusks (Orc)|Défenses (orc)|libre|
|[ancestry-01-m9tKNsZQQjHdsmEN.htm](feats/ancestry-01-m9tKNsZQQjHdsmEN.htm)|Tupilaq Carver|Graveur tupilaq|libre|
|[ancestry-01-mAAlean6DuWd3wDT.htm](feats/ancestry-01-mAAlean6DuWd3wDT.htm)|Open Mind|Esprit ouvert|libre|
|[ancestry-01-MbvPuMVy2VhftJgd.htm](feats/ancestry-01-MbvPuMVy2VhftJgd.htm)|Perfect Dive|Plongeon parfait|libre|
|[ancestry-01-mmYAiK3x0UMcgiNv.htm](feats/ancestry-01-mmYAiK3x0UMcgiNv.htm)|Dualborn|Naissance duale|libre|
|[ancestry-01-MqDLaBypDyp1VQKg.htm](feats/ancestry-01-MqDLaBypDyp1VQKg.htm)|Shisk Lore|Connaissance des shisks|libre|
|[ancestry-01-mQYO501xyMgtIQ3W.htm](feats/ancestry-01-mQYO501xyMgtIQ3W.htm)|Ratfolk Lore|Connaissance des hommes-rats|libre|
|[ancestry-01-MS53Ds75BT379ZFm.htm](feats/ancestry-01-MS53Ds75BT379ZFm.htm)|Empathetic Plea|Supplique empathique|libre|
|[ancestry-01-mW9uhe3RorEJg6Mn.htm](feats/ancestry-01-mW9uhe3RorEJg6Mn.htm)|Nagaji Spell Familiarity|Familiarité avec les sorts nagajis|libre|
|[ancestry-01-mxhGH4FVYXJwb0BC.htm](feats/ancestry-01-mxhGH4FVYXJwb0BC.htm)|Gnome Obsession|Obsession Gnome|officielle|
|[ancestry-01-MXkklchuimVSHZyd.htm](feats/ancestry-01-MXkklchuimVSHZyd.htm)|Theoretical Acumen|Perspicacité théorique|libre|
|[ancestry-01-N3xyK9keDm00oUY6.htm](feats/ancestry-01-N3xyK9keDm00oUY6.htm)|Inner Fire (Ifrit)|Feu intérieur (Ifrit)|libre|
|[ancestry-01-n7JXquVGxQOfrCsf.htm](feats/ancestry-01-n7JXquVGxQOfrCsf.htm)|Ancestral Insight|Intuition ancestrale|libre|
|[ancestry-01-n9CBjyiB17srkyr4.htm](feats/ancestry-01-n9CBjyiB17srkyr4.htm)|Hobgoblin Weapon Familiarity|Familiarité avec les armes des hobgobelins|libre|
|[ancestry-01-nB8BD9rIg9hfFGns.htm](feats/ancestry-01-nB8BD9rIg9hfFGns.htm)|Eye For Treasure|Oeil appréciateur|libre|
|[ancestry-01-nbWkPLiUxagX8dCw.htm](feats/ancestry-01-nbWkPLiUxagX8dCw.htm)|Shapechanger's Intuition|Intuition du changeur de forme|libre|
|[ancestry-01-nfERPRCITBp970HO.htm](feats/ancestry-01-nfERPRCITBp970HO.htm)|Earned Glory|Gloire méritée|libre|
|[ancestry-01-ng9H9flz4H6agiiV.htm](feats/ancestry-01-ng9H9flz4H6agiiV.htm)|Demonbane Warrior|Combattant fléau des démons|libre|
|[ancestry-01-ngEd8PgsyAARuTQ9.htm](feats/ancestry-01-ngEd8PgsyAARuTQ9.htm)|Vengeful Hatred|Soif de vengeance|officielle|
|[ancestry-01-NIUSBGMmdqhkYtmo.htm](feats/ancestry-01-NIUSBGMmdqhkYtmo.htm)|Rock Runner|Arpenteur des roches|officielle|
|[ancestry-01-NN1U8ifiURc0h4Fx.htm](feats/ancestry-01-NN1U8ifiURc0h4Fx.htm)|Pierce the Darkness|Percer l'obscurité|libre|
|[ancestry-01-Nn80wBZYJZxcuKsJ.htm](feats/ancestry-01-Nn80wBZYJZxcuKsJ.htm)|Stone Face|Visage de pierre|libre|
|[ancestry-01-nnlzI9LSvhl94U16.htm](feats/ancestry-01-nnlzI9LSvhl94U16.htm)|Scuttle Up|Détaler vers le haut|libre|
|[ancestry-01-nqtO5dNnxT4nZDbB.htm](feats/ancestry-01-nqtO5dNnxT4nZDbB.htm)|Surface Skimmer|Écumeur aquatique|libre|
|[ancestry-01-NYcgVwO4xLerJ9lO.htm](feats/ancestry-01-NYcgVwO4xLerJ9lO.htm)|Progenitor Lore|Connaissance du géniteur|libre|
|[ancestry-01-nyhQ9xB0rkoAoNbf.htm](feats/ancestry-01-nyhQ9xB0rkoAoNbf.htm)|Grimspawn|Rejeton sombre|libre|
|[ancestry-01-nyNsIePvpovlDAws.htm](feats/ancestry-01-nyNsIePvpovlDAws.htm)|Alchemical Scholar|Étudiant alchimique|libre|
|[ancestry-01-NySPRgD0FjZY2QGs.htm](feats/ancestry-01-NySPRgD0FjZY2QGs.htm)|Consult The Stars|Consulter les étoiles|libre|
|[ancestry-01-o4LycqplO14zn6It.htm](feats/ancestry-01-o4LycqplO14zn6It.htm)|Know Your Own|Connaître vos semblables|libre|
|[ancestry-01-O5v8yaeCbjKeXfyi.htm](feats/ancestry-01-O5v8yaeCbjKeXfyi.htm)|Duskwalker Lore|Connaissance des crépusculaires|libre|
|[ancestry-01-o6MXyjSgavTzU5AS.htm](feats/ancestry-01-o6MXyjSgavTzU5AS.htm)|Gemsoul|Âme-gemme|libre|
|[ancestry-01-oeGowXO2P6rHbZfY.htm](feats/ancestry-01-oeGowXO2P6rHbZfY.htm)|Very Sneaky|Très furtif|officielle|
|[ancestry-01-oeVRwtBlQjsSVtXV.htm](feats/ancestry-01-oeVRwtBlQjsSVtXV.htm)|Ghoran Weapon Familiarity|Familiarité avec les armes des ghorans|libre|
|[ancestry-01-ojp39fVYqFBGAw38.htm](feats/ancestry-01-ojp39fVYqFBGAw38.htm)|Natural Performer|Réprésentation naturelle|libre|
|[ancestry-01-ojykhaQkT8IU7ouc.htm](feats/ancestry-01-ojykhaQkT8IU7ouc.htm)|Emotional Partitions|Partition émotionnelles|libre|
|[ancestry-01-OKSsFlHY5UKc4dKu.htm](feats/ancestry-01-OKSsFlHY5UKc4dKu.htm)|Elemental Assault|Assaut élémentaire|libre|
|[ancestry-01-OqGNcUTqaZTp2YND.htm](feats/ancestry-01-OqGNcUTqaZTp2YND.htm)|Molten Wit|Esprit en fusion|libre|
|[ancestry-01-oQzyeSUlKx6eHcZp.htm](feats/ancestry-01-oQzyeSUlKx6eHcZp.htm)|Vanara Weapon Familiarity|Familiarité avec les armes vanaras|libre|
|[ancestry-01-OTjRxyCtwPoqNFP2.htm](feats/ancestry-01-OTjRxyCtwPoqNFP2.htm)|Kobold Weapon Familiarity|Familiarité avec les armes des kobolds|libre|
|[ancestry-01-OYjzfTeWU7RJBT7v.htm](feats/ancestry-01-OYjzfTeWU7RJBT7v.htm)|Goblin Weapon Familiarity|Familiarité avec les armes gobelines|officielle|
|[ancestry-01-OZtoTusMmCJymObT.htm](feats/ancestry-01-OZtoTusMmCJymObT.htm)|Leech-Clipper|Coupe-sangsue|libre|
|[ancestry-01-P1dk0LTWkQ1LT1ai.htm](feats/ancestry-01-P1dk0LTWkQ1LT1ai.htm)|Svetocher|Svetocher|libre|
|[ancestry-01-Pat4H0VbmApblZxc.htm](feats/ancestry-01-Pat4H0VbmApblZxc.htm)|Otherworldly Magic|Magie surnaturelle|officielle|
|[ancestry-01-pCck5GgKIiIPGGdy.htm](feats/ancestry-01-pCck5GgKIiIPGGdy.htm)|Veil May|Vierge voilée|libre|
|[ancestry-01-pgsSUJehsKZXlRp7.htm](feats/ancestry-01-pgsSUJehsKZXlRp7.htm)|Vishkanya Lore|Connaissance des vishkanyas|libre|
|[ancestry-01-PGVXjbAi1Fa4uTmD.htm](feats/ancestry-01-PGVXjbAi1Fa4uTmD.htm)|Halo|Halo|libre|
|[ancestry-01-PlhPpdwIV0rIAJ8K.htm](feats/ancestry-01-PlhPpdwIV0rIAJ8K.htm)|Orc Ferocity|Férocité orque|officielle|
|[ancestry-01-PMRfunXzC9YizHNZ.htm](feats/ancestry-01-PMRfunXzC9YizHNZ.htm)|Riftmarked|Marquéfaille|libre|
|[ancestry-01-PodajLVxqYSAqVox.htm](feats/ancestry-01-PodajLVxqYSAqVox.htm)|Natural Ambition|Ambition naturelle|officielle|
|[ancestry-01-Pox93XMBaFmeLIDM.htm](feats/ancestry-01-Pox93XMBaFmeLIDM.htm)|Cheek Pouches|Abajoues|libre|
|[ancestry-01-pP4pbFBg5GAgcOE9.htm](feats/ancestry-01-pP4pbFBg5GAgcOE9.htm)|Snow May|Vierge de neige|libre|
|[ancestry-01-PPUNMjRLQYnmwQvF.htm](feats/ancestry-01-PPUNMjRLQYnmwQvF.htm)|Kobold Breath|Souffle de kobold|libre|
|[ancestry-01-pQQhrcj1u6hUUc8L.htm](feats/ancestry-01-pQQhrcj1u6hUUc8L.htm)|Keep Up Appearances|Conserver les apparences|libre|
|[ancestry-01-PsLne80WUsD4IFa6.htm](feats/ancestry-01-PsLne80WUsD4IFa6.htm)|Dig Quickly|Creuser rapidement|libre|
|[ancestry-01-ptEOt3lqjxUnAW62.htm](feats/ancestry-01-ptEOt3lqjxUnAW62.htm)|Ancient Memories|Souvenirs anciens|libre|
|[ancestry-01-PVkAEBlRSJHe3JCz.htm](feats/ancestry-01-PVkAEBlRSJHe3JCz.htm)|Straveika|Straveika|libre|
|[ancestry-01-Qb25uu1gT5CDMSWb.htm](feats/ancestry-01-Qb25uu1gT5CDMSWb.htm)|Hag Claws|Griffes de guenaude|libre|
|[ancestry-01-qCV04rZMty2TJBrX.htm](feats/ancestry-01-qCV04rZMty2TJBrX.htm)|Foxfire|Feu de renard|libre|
|[ancestry-01-QHwajD5n8P3oS9Wb.htm](feats/ancestry-01-QHwajD5n8P3oS9Wb.htm)|Vicious Incisors|Incisives vicieuses|libre|
|[ancestry-01-qm1lIMLVdsQtVFT0.htm](feats/ancestry-01-qm1lIMLVdsQtVFT0.htm)|Stormsoul|Âme-tempête|libre|
|[ancestry-01-qpVB9F9DURW4Lti1.htm](feats/ancestry-01-qpVB9F9DURW4Lti1.htm)|Uncanny Agility|Agilité instinctive|libre|
|[ancestry-01-QsPvKvt4S1PR7kr7.htm](feats/ancestry-01-QsPvKvt4S1PR7kr7.htm)|Elemental Trade|Métier élémentaire|libre|
|[ancestry-01-qT68NpouO667sUMA.htm](feats/ancestry-01-qT68NpouO667sUMA.htm)|Sudden Mindfulness|Pleine conscience soudaine|libre|
|[ancestry-01-qVk9htKv47niPmXS.htm](feats/ancestry-01-qVk9htKv47niPmXS.htm)|Deep Vision|Vision des profondeurs|libre|
|[ancestry-01-qvxHGcOUYVG7Tqt4.htm](feats/ancestry-01-qvxHGcOUYVG7Tqt4.htm)|Dongun Education|Éducation de Dongun|libre|
|[ancestry-01-qxQf0HiYBh26mCMT.htm](feats/ancestry-01-qxQf0HiYBh26mCMT.htm)|Shrouded Mien|Mine voilée|libre|
|[ancestry-01-QygsOzswFQEYONO3.htm](feats/ancestry-01-QygsOzswFQEYONO3.htm)|Shackleborn|Néentravé|libre|
|[ancestry-01-qzalwa2Ze3dIqIrA.htm](feats/ancestry-01-qzalwa2Ze3dIqIrA.htm)|Lightning Tongue|Langue éclair|libre|
|[ancestry-01-QZb0Utg0WFPf2Qg0.htm](feats/ancestry-01-QZb0Utg0WFPf2Qg0.htm)|Goblin Song|Chant gobelin|officielle|
|[ancestry-01-qZStZXyAj6kSAKQo.htm](feats/ancestry-01-qZStZXyAj6kSAKQo.htm)|Vanara Lore|Connaissance des vanaras|libre|
|[ancestry-01-Rfyhlyql1GSoDkI2.htm](feats/ancestry-01-Rfyhlyql1GSoDkI2.htm)|Natural Skill|Talent naturel|officielle|
|[ancestry-01-RJClD7YYsAFutxNs.htm](feats/ancestry-01-RJClD7YYsAFutxNs.htm)|Elemental Lore|Connaissance élémentaire|libre|
|[ancestry-01-RmiMUZlae6yGUyXY.htm](feats/ancestry-01-RmiMUZlae6yGUyXY.htm)|Haughty Obstinacy|Obstination arrogante|officielle|
|[ancestry-01-roC6o0xJ8hDzHIWY.htm](feats/ancestry-01-roC6o0xJ8hDzHIWY.htm)|Serpent's Tongue|Langue de serpent|libre|
|[ancestry-01-rswkPvZvEBXISH96.htm](feats/ancestry-01-rswkPvZvEBXISH96.htm)|Rimesoul|Âme-frimas|libre|
|[ancestry-01-rWiddU8fHl0TraoN.htm](feats/ancestry-01-rWiddU8fHl0TraoN.htm)|Gnoll Lore|Connaissance des gnolls|libre|
|[ancestry-01-ryRpqixE7cN8svwB.htm](feats/ancestry-01-ryRpqixE7cN8svwB.htm)|Evanescent Wings|Ailes évanescentes|libre|
|[ancestry-01-SCgqIo8VQZZKZ1Ws.htm](feats/ancestry-01-SCgqIo8VQZZKZ1Ws.htm)|Orc Sight|Vision orque|officielle|
|[ancestry-01-shp63QZvw9xvkVvC.htm](feats/ancestry-01-shp63QZvw9xvkVvC.htm)|Know Oneself|Connais toi toi-même|libre|
|[ancestry-01-sJ7WTLDwAbIA9Elc.htm](feats/ancestry-01-sJ7WTLDwAbIA9Elc.htm)|Burn It!|Brûle !|officielle|
|[ancestry-01-SJbokJKkFnA6rKkJ.htm](feats/ancestry-01-SJbokJKkFnA6rKkJ.htm)|Nestling Fall|Chute de l'oisillon|libre|
|[ancestry-01-sKDCoxMz2yKWLGRJ.htm](feats/ancestry-01-sKDCoxMz2yKWLGRJ.htm)|Celestial Lore|Connaissance céleste|libre|
|[ancestry-01-sL2GmYve5NXJ0wc1.htm](feats/ancestry-01-sL2GmYve5NXJ0wc1.htm)|Gloomseer|Voyant pénombral|libre|
|[ancestry-01-sm6Y3fTcltxE8N0p.htm](feats/ancestry-01-sm6Y3fTcltxE8N0p.htm)|Goblin Lore|Connaissance des gobelins|officielle|
|[ancestry-01-sMZBH6ROL44EDpXB.htm](feats/ancestry-01-sMZBH6ROL44EDpXB.htm)|Vishkanya Weapon Familiarity|Familiarité avec les armes vishkanyas|libre|
|[ancestry-01-SPyvwsiSghySIEw2.htm](feats/ancestry-01-SPyvwsiSghySIEw2.htm)|Dragon's Presence|Présence draconique|libre|
|[ancestry-01-SrSYEHqOLXWuj65e.htm](feats/ancestry-01-SrSYEHqOLXWuj65e.htm)|Inventive Offensive|Offensive inventive|libre|
|[ancestry-01-StsFnks3lQU9YYpB.htm](feats/ancestry-01-StsFnks3lQU9YYpB.htm)|Clone-Risen|Issu d'un clonage|libre|
|[ancestry-01-T4ulOYkFh8gq2kY9.htm](feats/ancestry-01-T4ulOYkFh8gq2kY9.htm)|Tide-hardened|Endurci par la marée|libre|
|[ancestry-01-TcUpt0KaDnoYheX8.htm](feats/ancestry-01-TcUpt0KaDnoYheX8.htm)|Tough Skin|Peau robuste|libre|
|[ancestry-01-tFgsBRsEo9ZEA5fU.htm](feats/ancestry-01-tFgsBRsEo9ZEA5fU.htm)|Illusion Sense|Perception des illusions|officielle|
|[ancestry-01-TGlb3gmSKkJBZt5q.htm](feats/ancestry-01-TGlb3gmSKkJBZt5q.htm)|Striking Retribution|Rétribution frappante|libre|
|[ancestry-01-TLuFqQwvnlJNeEsv.htm](feats/ancestry-01-TLuFqQwvnlJNeEsv.htm)|Crystal Luminescence|Luminescence du cristal|libre|
|[ancestry-01-TmaxLWraJrvSQOkY.htm](feats/ancestry-01-TmaxLWraJrvSQOkY.htm)|Mariner's Fire|Feu du marin|libre|
|[ancestry-01-tn7K5lbnF87rZ659.htm](feats/ancestry-01-tn7K5lbnF87rZ659.htm)|Sociable|Sociable|libre|
|[ancestry-01-tP8pws78OOmobWjB.htm](feats/ancestry-01-tP8pws78OOmobWjB.htm)|Willing Death|Mort volontaire|libre|
|[ancestry-01-tQ0FWNYJEXbmPCt7.htm](feats/ancestry-01-tQ0FWNYJEXbmPCt7.htm)|Emberkin|Enfant de braise|libre|
|[ancestry-01-ttgv3PPCJLeI5pUL.htm](feats/ancestry-01-ttgv3PPCJLeI5pUL.htm)|Social Camouflage|Camouflage social|libre|
|[ancestry-01-TtlbpGchHOoWc4HN.htm](feats/ancestry-01-TtlbpGchHOoWc4HN.htm)|Collapse|Effondrement|libre|
|[ancestry-01-tVPxq2qYCcOqvIYZ.htm](feats/ancestry-01-tVPxq2qYCcOqvIYZ.htm)|Ganzi Gaze|Regard ganzi|libre|
|[ancestry-01-TW5TY7kSf50uaX71.htm](feats/ancestry-01-TW5TY7kSf50uaX71.htm)|Slither|Reptation|libre|
|[ancestry-01-txgCXcNMDe9kO7N8.htm](feats/ancestry-01-txgCXcNMDe9kO7N8.htm)|Vibrant Display|Affichage dynamique|libre|
|[ancestry-01-TYoE0GDF0URwQqZI.htm](feats/ancestry-01-TYoE0GDF0URwQqZI.htm)|Mistsoul|Âme-brume|libre|
|[ancestry-01-U09VfgU7alL0acWv.htm](feats/ancestry-01-U09VfgU7alL0acWv.htm)|Water Nagaji|Nagaji aquatique|libre|
|[ancestry-01-U12Sh43QuY85Prdm.htm](feats/ancestry-01-U12Sh43QuY85Prdm.htm)|Parthenogenic Hatchling|Couvée parthénogène|libre|
|[ancestry-01-u7FPgOjbSOmCTSD9.htm](feats/ancestry-01-u7FPgOjbSOmCTSD9.htm)|Flexible Form|Forme souple|libre|
|[ancestry-01-u8gmBNHgb880vN3S.htm](feats/ancestry-01-u8gmBNHgb880vN3S.htm)|Witch Warden|Garde-sorcier|libre|
|[ancestry-01-Ugw1zZStQhg6iz8h.htm](feats/ancestry-01-Ugw1zZStQhg6iz8h.htm)|Adroit Manipulation|Manipulation adroite|libre|
|[ancestry-01-uiNRrdnJe0GOzy6Q.htm](feats/ancestry-01-uiNRrdnJe0GOzy6Q.htm)|Chance Death|Mort accidentelle|libre|
|[ancestry-01-UJ8AqzkkDqRCMNFW.htm](feats/ancestry-01-UJ8AqzkkDqRCMNFW.htm)|Dwarven Doughtiness|Bravoure naine|libre|
|[ancestry-01-upMcjxPDgNOLuu7N.htm](feats/ancestry-01-upMcjxPDgNOLuu7N.htm)|Internal Compartment|Compartiment interne|libre|
|[ancestry-01-UsJtnrqWOs7puRZa.htm](feats/ancestry-01-UsJtnrqWOs7puRZa.htm)|Dragon Spit|Crachat du dragon|libre|
|[ancestry-01-uuD8Z9jUG61GmenX.htm](feats/ancestry-01-uuD8Z9jUG61GmenX.htm)|Unfettered Halfling|Halfelin affranchi|libre|
|[ancestry-01-uW0VSyy75YrsvtWz.htm](feats/ancestry-01-uW0VSyy75YrsvtWz.htm)|Unwavering Mien|Volonté inébranlable|officielle|
|[ancestry-01-UwH0sGIthv8kiPUt.htm](feats/ancestry-01-UwH0sGIthv8kiPUt.htm)|Goblin Scuttle|Précipitation gobeline|officielle|
|[ancestry-01-VatkAzfBYjA6z6OP.htm](feats/ancestry-01-VatkAzfBYjA6z6OP.htm)|Razor Claws|Griffes rasoir|libre|
|[ancestry-01-vcfeHDoaWEZtEcfz.htm](feats/ancestry-01-vcfeHDoaWEZtEcfz.htm)|Distracting Shadows|Ombres distrayantes|officielle|
|[ancestry-01-VcnOEAM3UR7oS0D5.htm](feats/ancestry-01-VcnOEAM3UR7oS0D5.htm)|Razzle-Dazzle|Éblouissement|libre|
|[ancestry-01-VDiMapgJoFI3CCol.htm](feats/ancestry-01-VDiMapgJoFI3CCol.htm)|Ratspeak|Langage des rats|libre|
|[ancestry-01-viFTJfZusRPx0G2q.htm](feats/ancestry-01-viFTJfZusRPx0G2q.htm)|Scamper Underfoot|Détaler par dessous|libre|
|[ancestry-01-vNDCXpumBONpk5JO.htm](feats/ancestry-01-vNDCXpumBONpk5JO.htm)|Goloma Lore|Connaissance des golomas|libre|
|[ancestry-01-VSINzKESKAw2zA20.htm](feats/ancestry-01-VSINzKESKAw2zA20.htm)|Ceremony of Protection|Cérémonie de protection|libre|
|[ancestry-01-VU07hybqzUXIJ6l2.htm](feats/ancestry-01-VU07hybqzUXIJ6l2.htm)|Halfling Lore|Connaissance des halfelins|officielle|
|[ancestry-01-VV7vbDzcO8vdD3OO.htm](feats/ancestry-01-VV7vbDzcO8vdD3OO.htm)|Skull Creeper|Crâne épouvantable|libre|
|[ancestry-01-vWf6uykXkQp1Li0r.htm](feats/ancestry-01-vWf6uykXkQp1Li0r.htm)|Fumesoul|Âme-volute|libre|
|[ancestry-01-Vxs9btOk61C7KpP2.htm](feats/ancestry-01-Vxs9btOk61C7KpP2.htm)|Cynical|Cynique|libre|
|[ancestry-01-VZDJgGm5LGb1Pdck.htm](feats/ancestry-01-VZDJgGm5LGb1Pdck.htm)|Elemental Embellish|Embellissement élémentaire|libre|
|[ancestry-01-vzKvihYNModB7sJ7.htm](feats/ancestry-01-vzKvihYNModB7sJ7.htm)|Play Dead|Faire le mort|libre|
|[ancestry-01-w1UrOqEYVuJDgRZZ.htm](feats/ancestry-01-w1UrOqEYVuJDgRZZ.htm)|Cold Minded|Esprit froid|libre|
|[ancestry-01-W2LmEXJH75tyeCSn.htm](feats/ancestry-01-W2LmEXJH75tyeCSn.htm)|Rat Familiar|Familier rat|libre|
|[ancestry-01-W5hgaJvaCrTKfYbC.htm](feats/ancestry-01-W5hgaJvaCrTKfYbC.htm)|Fire Savvy|Connaisseur du feu|libre|
|[ancestry-01-wAeI18wyWVwfUrIP.htm](feats/ancestry-01-wAeI18wyWVwfUrIP.htm)|Tusks (Half-Orc)|Défenses (demi-orc)|libre|
|[ancestry-01-wEtN2NkqW4z4GUON.htm](feats/ancestry-01-wEtN2NkqW4z4GUON.htm)|Nosoi's Mask|Masque de nosoi|libre|
|[ancestry-01-weYZzyMmlCIC2TZt.htm](feats/ancestry-01-weYZzyMmlCIC2TZt.htm)|Grasping Reach|Allonge agrippante|libre|
|[ancestry-01-WoKiYMCXV27szBdy.htm](feats/ancestry-01-WoKiYMCXV27szBdy.htm)|Cringe|Recroquevillé|libre|
|[ancestry-01-WoLh16gyDp8y9WOZ.htm](feats/ancestry-01-WoLh16gyDp8y9WOZ.htm)|Ancestral Longevity|Longévité ancestrale|officielle|
|[ancestry-01-wTT8ieLvfWsZZaWT.htm](feats/ancestry-01-wTT8ieLvfWsZZaWT.htm)|Tinkering Fingers|Doigts bricoleurs|libre|
|[ancestry-01-wtTye8OrC9cuK7YP.htm](feats/ancestry-01-wtTye8OrC9cuK7YP.htm)|Beast Trainer|Dresseur de bête|libre|
|[ancestry-01-WUMnFrehmFbj2PIm.htm](feats/ancestry-01-WUMnFrehmFbj2PIm.htm)|Azarketi Weapon Familiarity|Familiarité avec les armes des azarketis|libre|
|[ancestry-01-WYEjiUZQJP3uMk4Z.htm](feats/ancestry-01-WYEjiUZQJP3uMk4Z.htm)|Irrepressible (Ganzi)|Irrépressible (Ganzi)|libre|
|[ancestry-01-wylnETwIz32Au46y.htm](feats/ancestry-01-wylnETwIz32Au46y.htm)|Ageless Spirit|Esprit sans âge|libre|
|[ancestry-01-WzJaArukCUf9hpeP.htm](feats/ancestry-01-WzJaArukCUf9hpeP.htm)|Hag's Sight|Vision de la guenaude|libre|
|[ancestry-01-X7wFUWjYjYhzpejU.htm](feats/ancestry-01-X7wFUWjYjYhzpejU.htm)|Clan's Edge|Avantage du clan|libre|
|[ancestry-01-X9tKWtQrAcmn26Nv.htm](feats/ancestry-01-X9tKWtQrAcmn26Nv.htm)|Adapted Cantrip|Tour de magie adapté|officielle|
|[ancestry-01-xjwI1DlJvb7Rg6TG.htm](feats/ancestry-01-xjwI1DlJvb7Rg6TG.htm)|Azarketi Lore|Connaissance des azarketis|libre|
|[ancestry-01-XKDFgBEtFdEzCz8X.htm](feats/ancestry-01-XKDFgBEtFdEzCz8X.htm)|Intuitive Crafting|Artisanat intuitif|libre|
|[ancestry-01-xOCmeskMkd8nCmba.htm](feats/ancestry-01-xOCmeskMkd8nCmba.htm)|Moon May|Vierge lunaire|libre|
|[ancestry-01-xruboxaTF6nw8l7M.htm](feats/ancestry-01-xruboxaTF6nw8l7M.htm)|Miresoul|Âme-boue|libre|
|[ancestry-01-xTe8lNBp76jsrhYh.htm](feats/ancestry-01-xTe8lNBp76jsrhYh.htm)|Slag May|Vierge crasseuse|libre|
|[ancestry-01-XXAqMjml33jnQiDO.htm](feats/ancestry-01-XXAqMjml33jnQiDO.htm)|Old Soul|Âme vieille|libre|
|[ancestry-01-xzpMQ2ZRn9zC23XG.htm](feats/ancestry-01-xzpMQ2ZRn9zC23XG.htm)|Scamper|Détaler|libre|
|[ancestry-01-Y1bEisU8jVCsIYk3.htm](feats/ancestry-01-Y1bEisU8jVCsIYk3.htm)|Woodcraft|Forestier|libre|
|[ancestry-01-y7Or0CbcQBDdS9yG.htm](feats/ancestry-01-y7Or0CbcQBDdS9yG.htm)|Elven Aloofness|Détachement elfique|libre|
|[ancestry-01-Y8c8hAyjNAiqgxAO.htm](feats/ancestry-01-Y8c8hAyjNAiqgxAO.htm)|Pelagic Aptitude|Aptitude pélagique|libre|
|[ancestry-01-Y8sKn8NH1wC7Mrui.htm](feats/ancestry-01-Y8sKn8NH1wC7Mrui.htm)|Fangs|Crocs|libre|
|[ancestry-01-yaacOmfmBuGDcNOs.htm](feats/ancestry-01-yaacOmfmBuGDcNOs.htm)|Skittertalk|Parler avec les araignées|libre|
|[ancestry-01-yaoekizcgPIlqVcC.htm](feats/ancestry-01-yaoekizcgPIlqVcC.htm)|Warped Reflection|Reflet déformé|libre|
|[ancestry-01-yCaWcKlpbAfebqlO.htm](feats/ancestry-01-yCaWcKlpbAfebqlO.htm)|Shoony Lore|Connaissance des shoonis|libre|
|[ancestry-01-ydgCsYsgqSkFWEDK.htm](feats/ancestry-01-ydgCsYsgqSkFWEDK.htm)|Angelkin|Type angélique|libre|
|[ancestry-01-yEbXxbD317IZgtsN.htm](feats/ancestry-01-yEbXxbD317IZgtsN.htm)|Dustsoul|Âme-poussière|libre|
|[ancestry-01-YG6OCTbbqZwqRTr3.htm](feats/ancestry-01-YG6OCTbbqZwqRTr3.htm)|Arcane Eye|Oeil arcanique|libre|
|[ancestry-01-yJ8Ez5dEscIk1xr5.htm](feats/ancestry-01-yJ8Ez5dEscIk1xr5.htm)|First World Magic|Magie du Premier monde|libre|
|[ancestry-01-yMfZulJcoSomQ6dO.htm](feats/ancestry-01-yMfZulJcoSomQ6dO.htm)|Elemental Wrath|Courroux élémentaire|libre|
|[ancestry-01-YMKtEoNwHKA713Cx.htm](feats/ancestry-01-YMKtEoNwHKA713Cx.htm)|Junk Tinker|Bricoleur de rebuts|officielle|
|[ancestry-01-yQZ6naE8AP6JYTSH.htm](feats/ancestry-01-yQZ6naE8AP6JYTSH.htm)|Ember's Eyes|Yeux de braise|libre|
|[ancestry-01-z92LsdpE98QdwILa.htm](feats/ancestry-01-z92LsdpE98QdwILa.htm)|Adaptive Vision|Vision adaptative|libre|
|[ancestry-01-zaGD9Og2p8Opa0oJ.htm](feats/ancestry-01-zaGD9Og2p8Opa0oJ.htm)|Scavenger's Search|Fouille du récupérateur|libre|
|[ancestry-01-zAZJpgeEf5TWvdq4.htm](feats/ancestry-01-zAZJpgeEf5TWvdq4.htm)|Watchful Halfling|Halfelin vigilant|officielle|
|[ancestry-01-zb7F0M3H8PN3XsdX.htm](feats/ancestry-01-zb7F0M3H8PN3XsdX.htm)|Gnoll Weapon Familiarity|Familiarité avec les armes des gnolls|libre|
|[ancestry-01-ZbRVqf14RTJJIZXG.htm](feats/ancestry-01-ZbRVqf14RTJJIZXG.htm)|Halfling Luck|Chance halfeline|officielle|
|[ancestry-01-ZDO7foRCMd9niGsK.htm](feats/ancestry-01-ZDO7foRCMd9niGsK.htm)|Quadruped|Quadrupède|libre|
|[ancestry-01-zi84Xt4dTsLeJ3uD.htm](feats/ancestry-01-zi84Xt4dTsLeJ3uD.htm)|Living Weapon|Arme vivante|libre|
|[ancestry-01-zki5qdM5IQcAiscM.htm](feats/ancestry-01-zki5qdM5IQcAiscM.htm)|Elven Lore|Connaissance des elfes|officielle|
|[ancestry-01-znX4u20IFE7TPi9Y.htm](feats/ancestry-01-znX4u20IFE7TPi9Y.htm)|Morph-Risen|Issu d'une transformation|libre|
|[ancestry-01-Zr1sspa9Q16V8uZV.htm](feats/ancestry-01-Zr1sspa9Q16V8uZV.htm)|Dig Up Secrets|Déterrer les secrets|libre|
|[ancestry-01-zs2FFGI88zB7EaBT.htm](feats/ancestry-01-zs2FFGI88zB7EaBT.htm)|Grim Insight|Sombre introspection|libre|
|[ancestry-01-ZUjjUj9lVGOKrJbp.htm](feats/ancestry-01-ZUjjUj9lVGOKrJbp.htm)|Idyllkin|Type idyllique|libre|
|[ancestry-01-zvtYsz9jk8tPvlpS.htm](feats/ancestry-01-zvtYsz9jk8tPvlpS.htm)|Cat Nap|Sieste féline|libre|
|[ancestry-01-ZxiAMposVPDNPwxI.htm](feats/ancestry-01-ZxiAMposVPDNPwxI.htm)|Forge-Day's Rest|Repos du jour de forge|libre|
|[ancestry-01-Zz7isE8Td2xDWqR8.htm](feats/ancestry-01-Zz7isE8Td2xDWqR8.htm)|Kitsune Spell Familiarity|Familiarité avec les sorts des kitsunes|libre|
|[ancestry-01-ZZzgqFTOtUgnzSLZ.htm](feats/ancestry-01-ZZzgqFTOtUgnzSLZ.htm)|Monstrous Peacemaker|Médiateur monstrueux|officielle|
|[ancestry-05-0FqbyC5tR2DC0DOk.htm](feats/ancestry-05-0FqbyC5tR2DC0DOk.htm)|Pack Stalker|Traqueur en meute|libre|
|[ancestry-05-0ihK3qYItmi8eVZs.htm](feats/ancestry-05-0ihK3qYItmi8eVZs.htm)|Eclectic Obsession|Obsession éclectique|libre|
|[ancestry-05-0Iv3VbR1DMPbZIjD.htm](feats/ancestry-05-0Iv3VbR1DMPbZIjD.htm)|Drag Down|Tirer dans l'eau|libre|
|[ancestry-05-0u1SZ1c6gDo6l0hS.htm](feats/ancestry-05-0u1SZ1c6gDo6l0hS.htm)|Climate Adaptation|Adaptation climatique|libre|
|[ancestry-05-14u6604yUXvoiIf3.htm](feats/ancestry-05-14u6604yUXvoiIf3.htm)|Malicious Bane|Imprécation malicieuse|libre|
|[ancestry-05-1gAehVvstGY885kJ.htm](feats/ancestry-05-1gAehVvstGY885kJ.htm)|Bristle|Hirsute|libre|
|[ancestry-05-1LFbIRhb3Fgk4203.htm](feats/ancestry-05-1LFbIRhb3Fgk4203.htm)|Sealed Poppet|Poupée ignifugée|libre|
|[ancestry-05-1miLVRtvsnZU6TTk.htm](feats/ancestry-05-1miLVRtvsnZU6TTk.htm)|Anchoring Roots|Racines ancrées|libre|
|[ancestry-05-1muxLx8Vacn2SLHc.htm](feats/ancestry-05-1muxLx8Vacn2SLHc.htm)|Kneecap|Rotule|libre|
|[ancestry-05-2IO2W09IvwGHvatH.htm](feats/ancestry-05-2IO2W09IvwGHvatH.htm)|Graceful Guidance|Assistance gracieuse|libre|
|[ancestry-05-2kwLzw618QaIHOap.htm](feats/ancestry-05-2kwLzw618QaIHOap.htm)|Genie Weapon Flourish|Sophistication d'armes de Génie|libre|
|[ancestry-05-2RmsQrLySNYQ4uIn.htm](feats/ancestry-05-2RmsQrLySNYQ4uIn.htm)|Spirit Soother|Pacificateur d'esprit|libre|
|[ancestry-05-2vlQ09QIBli5u9Gz.htm](feats/ancestry-05-2vlQ09QIBli5u9Gz.htm)|Called|Appelé|libre|
|[ancestry-05-3fLfHrJbMy2ayLMQ.htm](feats/ancestry-05-3fLfHrJbMy2ayLMQ.htm)|Blast Resistance|Résistance aux déflagrations|libre|
|[ancestry-05-3HuiLoQuJKLAh5rV.htm](feats/ancestry-05-3HuiLoQuJKLAh5rV.htm)|Springing Leaper|Sauteur bondissant|libre|
|[ancestry-05-3NZSRyoulnzsi3sn.htm](feats/ancestry-05-3NZSRyoulnzsi3sn.htm)|Wildborn Adept|Adepte sauvage|libre|
|[ancestry-05-47ZB8mYBtBt1C7zh.htm](feats/ancestry-05-47ZB8mYBtBt1C7zh.htm)|Histrionic Injury|Blessure histrionique|libre|
|[ancestry-05-48crF8lpg78fRdhJ.htm](feats/ancestry-05-48crF8lpg78fRdhJ.htm)|Feed On Pain|Se nourrir de la douleur|libre|
|[ancestry-05-4M36jGbeSDfZFM38.htm](feats/ancestry-05-4M36jGbeSDfZFM38.htm)|Elven Instincts|Instincts elfiques|libre|
|[ancestry-05-4RUTJC42ZaENYh9T.htm](feats/ancestry-05-4RUTJC42ZaENYh9T.htm)|Embodied Dreadnought Subjectivity|Subjectivité cuirrassée incarnée|libre|
|[ancestry-05-4tZbjb1ote8Nij8I.htm](feats/ancestry-05-4tZbjb1ote8Nij8I.htm)|Darting Monkey|Singe fulgurant|libre|
|[ancestry-05-5AnJTAZnuWrkL8fa.htm](feats/ancestry-05-5AnJTAZnuWrkL8fa.htm)|Defiance Unto Death|Défi jusqu'à la mort|libre|
|[ancestry-05-5iULRBRDOryuWX6t.htm](feats/ancestry-05-5iULRBRDOryuWX6t.htm)|Iruxi Unarmed Cunning|Ruse à mains nues iruxie|libre|
|[ancestry-05-69h9D3syUYLgIPr7.htm](feats/ancestry-05-69h9D3syUYLgIPr7.htm)|Expanded Luck|Chance étendue|libre|
|[ancestry-05-6kC0OuuWHyaqR3UQ.htm](feats/ancestry-05-6kC0OuuWHyaqR3UQ.htm)|Venom Spit|Crachat de venin|libre|
|[ancestry-05-73JyUrJnH3nOQJM5.htm](feats/ancestry-05-73JyUrJnH3nOQJM5.htm)|Ceremony of Knowledge|Cérémonie de connaissance|libre|
|[ancestry-05-7AuOURsTkkjrRekp.htm](feats/ancestry-05-7AuOURsTkkjrRekp.htm)|Loud Singer|Chanteur puissant|libre|
|[ancestry-05-7hwTeZNq6Jmzmtz4.htm](feats/ancestry-05-7hwTeZNq6Jmzmtz4.htm)|Agonizing Rebuke|Réprimande douloureuse|libre|
|[ancestry-05-7tNyXrMXclURtvUY.htm](feats/ancestry-05-7tNyXrMXclURtvUY.htm)|Skillful Tail (Ganzi)|Queue habile (ganzi)|libre|
|[ancestry-05-7w8O3g3KM1HCDBSL.htm](feats/ancestry-05-7w8O3g3KM1HCDBSL.htm)|Ambush Awareness|Conscience des embuscades|libre|
|[ancestry-05-7ZwXUlnqjj9zEYbO.htm](feats/ancestry-05-7ZwXUlnqjj9zEYbO.htm)|Vandal|Vandale|libre|
|[ancestry-05-80TAzC8XeFKRl7t5.htm](feats/ancestry-05-80TAzC8XeFKRl7t5.htm)|Warp Likeness|Semblance déformée|libre|
|[ancestry-05-8CSewrCVEmL8sjnk.htm](feats/ancestry-05-8CSewrCVEmL8sjnk.htm)|Celestial Resistance|Résistance céleste|libre|
|[ancestry-05-8DIzXO1YpsU3DpJw.htm](feats/ancestry-05-8DIzXO1YpsU3DpJw.htm)|Snare Genius|Génie des pièges artisanaux|libre|
|[ancestry-05-8MMYFIudSDkdo8cx.htm](feats/ancestry-05-8MMYFIudSDkdo8cx.htm)|Rat Magic|Magie du rat|libre|
|[ancestry-05-8Mo0fKRAz1TVbrr1.htm](feats/ancestry-05-8Mo0fKRAz1TVbrr1.htm)|Catfolk Weapon Rake|Armes griffantes des hommes-félins|libre|
|[ancestry-05-8NnYg8RUY4DQ8Wkf.htm](feats/ancestry-05-8NnYg8RUY4DQ8Wkf.htm)|Ghoran Weapon Practice|Pratique des armes des ghorans|libre|
|[ancestry-05-8RZ4VKXJgtl1aN27.htm](feats/ancestry-05-8RZ4VKXJgtl1aN27.htm)|Ornate Tattoo|Tatouage ornemental|libre|
|[ancestry-05-969mPoiYANzV2261.htm](feats/ancestry-05-969mPoiYANzV2261.htm)|Ratfolk Roll|Roulade de l'homme-rat|libre|
|[ancestry-05-9CIy4cJJTcRM30Vz.htm](feats/ancestry-05-9CIy4cJJTcRM30Vz.htm)|Tough Tumbler|Cascadeur coriace|libre|
|[ancestry-05-9WsYVbe47aSADs1Q.htm](feats/ancestry-05-9WsYVbe47aSADs1Q.htm)|Halfling Weapon Trickster|Ruse avec les armes halfelines|officielle|
|[ancestry-05-a0DM4VV48EFrga30.htm](feats/ancestry-05-a0DM4VV48EFrga30.htm)|Reveal Hidden Self|Révéler sa personnalité cachée|libre|
|[ancestry-05-a3eKpbAQnxGrlnGq.htm](feats/ancestry-05-a3eKpbAQnxGrlnGq.htm)|Restoring Blood|Sang récupérateur|libre|
|[ancestry-05-a5H6dJSQgWfviUHU.htm](feats/ancestry-05-a5H6dJSQgWfviUHU.htm)|Nothing But Fluff|Rien que du vent|libre|
|[ancestry-05-ab7xlmUzPUOFnAl2.htm](feats/ancestry-05-ab7xlmUzPUOFnAl2.htm)|Water Conjuration|Conjuration d'eau|libre|
|[ancestry-05-AcgxKcA5I3dNTWLr.htm](feats/ancestry-05-AcgxKcA5I3dNTWLr.htm)|Wavetouched Paragon|Parangon touché par la vague|libre|
|[ancestry-05-ACXWB7a38ETc32Qj.htm](feats/ancestry-05-ACXWB7a38ETc32Qj.htm)|Catchy Tune|Mélodie entraînante|libre|
|[ancestry-05-aLgb1x3azE4IKz0o.htm](feats/ancestry-05-aLgb1x3azE4IKz0o.htm)|Gnoll Weapon Practicality|Praticité des armes des gnolls|libre|
|[ancestry-05-ALR9knJdktuMWzr4.htm](feats/ancestry-05-ALR9knJdktuMWzr4.htm)|Azarketi Weapon Aptitude|Aptitude avec les armes Azarketies|libre|
|[ancestry-05-aoz9dCq2NynyUYEf.htm](feats/ancestry-05-aoz9dCq2NynyUYEf.htm)|Formation Training|Combat en formation|libre|
|[ancestry-05-aQNsD2t0Tb4vToA4.htm](feats/ancestry-05-aQNsD2t0Tb4vToA4.htm)|Hold Mark|Marque portée|libre|
|[ancestry-05-aVRuchEAJITim82y.htm](feats/ancestry-05-aVRuchEAJITim82y.htm)|Skillful Tail (Geniekin)|Queue habile (descendant de génie)|libre|
|[ancestry-05-aVRuchEAJIvnd70k.htm](feats/ancestry-05-aVRuchEAJIvnd70k.htm)|Skillful Tail (Tiefling)|Queue habile (Tieffelin)|libre|
|[ancestry-05-aZIdjtIYlLtJJP3g.htm](feats/ancestry-05-aZIdjtIYlLtJJP3g.htm)|Amorphous Aspect|Aspect amorphe|libre|
|[ancestry-05-B4e8V5nExlScjojY.htm](feats/ancestry-05-B4e8V5nExlScjojY.htm)|Shoki's Argument|Argument de Shoki|libre|
|[ancestry-05-B6eoSmowSFNlhj9h.htm](feats/ancestry-05-B6eoSmowSFNlhj9h.htm)|Hobgoblin Weapon Discipline|Discipline des armes hobgobelines|libre|
|[ancestry-05-b8BZl7wbm83ObEtO.htm](feats/ancestry-05-b8BZl7wbm83ObEtO.htm)|Integrated Armament|Arsenal intégré|libre|
|[ancestry-05-B9IytVeJ1SMSJawB.htm](feats/ancestry-05-B9IytVeJ1SMSJawB.htm)|Defensive Needles|Aiguilles défensives|libre|
|[ancestry-05-BaSl8PmfQwESIiY6.htm](feats/ancestry-05-BaSl8PmfQwESIiY6.htm)|Hypnotic Lure|Leurre hypnotique|libre|
|[ancestry-05-BiPQAcvaEO0P2snr.htm](feats/ancestry-05-BiPQAcvaEO0P2snr.htm)|Healer's Halo|Halo du guérisseur|libre|
|[ancestry-05-bJzANqEGTkho1bv6.htm](feats/ancestry-05-bJzANqEGTkho1bv6.htm)|Arcane Safeguards|Protections arcaniques|libre|
|[ancestry-05-bKc8MMFEOpOJJihb.htm](feats/ancestry-05-bKc8MMFEOpOJJihb.htm)|Heat Wave|Vague de chaleur|libre|
|[ancestry-05-BL2nLeClO30QoQGs.htm](feats/ancestry-05-BL2nLeClO30QoQGs.htm)|Spark Fist|Poing à étincelles|libre|
|[ancestry-05-BuaOQzmRNgIhGsfN.htm](feats/ancestry-05-BuaOQzmRNgIhGsfN.htm)|Blessed Blood (Aasimar)|Sang béni (Aasimar)|libre|
|[ancestry-05-bZGCOKLtCzMnrVwk.htm](feats/ancestry-05-bZGCOKLtCzMnrVwk.htm)|Replicate|Réplique|libre|
|[ancestry-05-CdSWMqPOVWIEzyUA.htm](feats/ancestry-05-CdSWMqPOVWIEzyUA.htm)|Fluid Contortionist|Contortionniste fluide|libre|
|[ancestry-05-CnKc1kBejFV5gWtN.htm](feats/ancestry-05-CnKc1kBejFV5gWtN.htm)|Animalistic Resistance|Résistance animalière|libre|
|[ancestry-05-Crcz9cW0To2pkfSy.htm](feats/ancestry-05-Crcz9cW0To2pkfSy.htm)|Murderous Thorns|Épines tueuses|libre|
|[ancestry-05-CXgTn2sE7Do11rlv.htm](feats/ancestry-05-CXgTn2sE7Do11rlv.htm)|Mist Child|Enfant brumeux|libre|
|[ancestry-05-DBWfPOYZaupwo3rz.htm](feats/ancestry-05-DBWfPOYZaupwo3rz.htm)|Adaptive Adept|Adepte de l'adaptation|officielle|
|[ancestry-05-DidzozerKLZ2UYLx.htm](feats/ancestry-05-DidzozerKLZ2UYLx.htm)|Fortify Shield|Bouclier fortifié|libre|
|[ancestry-05-dIIqejy4JAVuF0I8.htm](feats/ancestry-05-dIIqejy4JAVuF0I8.htm)|Fey Influence|Influence féerique|libre|
|[ancestry-05-DmYbGBC2ukp8tYD4.htm](feats/ancestry-05-DmYbGBC2ukp8tYD4.htm)|Firesight|Vision enflammée|libre|
|[ancestry-05-DP5VVZHERQlYuYTa.htm](feats/ancestry-05-DP5VVZHERQlYuYTa.htm)|Protective Subroutine|Sous-routine protectrice|libre|
|[ancestry-05-DSyLFBi2LcUxeORh.htm](feats/ancestry-05-DSyLFBi2LcUxeORh.htm)|Devil In Plain Sight|Diable sous vos yeux|libre|
|[ancestry-05-DtazfQJaRRe46Sej.htm](feats/ancestry-05-DtazfQJaRRe46Sej.htm)|Tranquil Sanctuary|Sanctuaire tranquille|libre|
|[ancestry-05-e3oaaoMZPzyP26QV.htm](feats/ancestry-05-e3oaaoMZPzyP26QV.htm)|Noble Resolve|Résolution noble|libre|
|[ancestry-05-EdVMwFRNV1LX1VWh.htm](feats/ancestry-05-EdVMwFRNV1LX1VWh.htm)|Light Paws|Pattes légères|libre|
|[ancestry-05-ehPnY1PuPK7EXkYc.htm](feats/ancestry-05-ehPnY1PuPK7EXkYc.htm)|Demonblood Frenzy|Frénésie du sang de démon|libre|
|[ancestry-05-EIbppwlEu11ltC7n.htm](feats/ancestry-05-EIbppwlEu11ltC7n.htm)|Reflective Pocket|Poche reflet|libre|
|[ancestry-05-eJ6CytjWx2sGnKnC.htm](feats/ancestry-05-eJ6CytjWx2sGnKnC.htm)|Fiendish Resistance|Résistance fiélone|libre|
|[ancestry-05-espST21gwaZQFxpw.htm](feats/ancestry-05-espST21gwaZQFxpw.htm)|Undead Companion|Compagnon mort-vivant|libre|
|[ancestry-05-eVn6hBjNjTB4liKw.htm](feats/ancestry-05-eVn6hBjNjTB4liKw.htm)|Chosen of Lamashtu|Élu de Lamashtu|libre|
|[ancestry-05-evwCimenReYvcruj.htm](feats/ancestry-05-evwCimenReYvcruj.htm)|Animal Elocutionist|Orateur animalier|officielle|
|[ancestry-05-Ewk7h9aQpKvy1RJo.htm](feats/ancestry-05-Ewk7h9aQpKvy1RJo.htm)|Crystalline Dust|Poussière cristalline|libre|
|[ancestry-05-exJmNR2XH1i6PGw3.htm](feats/ancestry-05-exJmNR2XH1i6PGw3.htm)|Skilled Climber|Grimpeur expérimenté|libre|
|[ancestry-05-F4W5a2vkhUP7Lr4j.htm](feats/ancestry-05-F4W5a2vkhUP7Lr4j.htm)|Powerful Guts|Entrailles résistantes|libre|
|[ancestry-05-f5Vkk2rM6tCe2zQn.htm](feats/ancestry-05-f5Vkk2rM6tCe2zQn.htm)|Shared Luck (Halfling)|Chance partagée|libre|
|[ancestry-05-f9YlMYWMjd0zoyy0.htm](feats/ancestry-05-f9YlMYWMjd0zoyy0.htm)|Protective Claws|Griffes protectrices|libre|
|[ancestry-05-FAUqvzfZDpRG44q0.htm](feats/ancestry-05-FAUqvzfZDpRG44q0.htm)|Renewing Quills|Piquants renouvelés|libre|
|[ancestry-05-FayzcoNaiIdyPS2j.htm](feats/ancestry-05-FayzcoNaiIdyPS2j.htm)|Greater Animal Senses|Sens animaux supérieurs|libre|
|[ancestry-05-FBtm9rZzk0tCQu9H.htm](feats/ancestry-05-FBtm9rZzk0tCQu9H.htm)|Spark of Independence|Étincelle d'indépendance|libre|
|[ancestry-05-fD3RSV9nIkJsW6lD.htm](feats/ancestry-05-fD3RSV9nIkJsW6lD.htm)|One-Toed Hop|Bond sur un orteil|libre|
|[ancestry-05-FGrIFDobGRFBPOuM.htm](feats/ancestry-05-FGrIFDobGRFBPOuM.htm)|Dwarven Reinforcement|Renforcement nain|libre|
|[ancestry-05-Fj3ufCawOM6fZB24.htm](feats/ancestry-05-Fj3ufCawOM6fZB24.htm)|Expert Drill Sergeant|Sergent instructeur expert|libre|
|[ancestry-05-FLuv8uI1KoodNgY4.htm](feats/ancestry-05-FLuv8uI1KoodNgY4.htm)|Goblin Weapon Frenzy|Frénésie avec les armes gobelines|officielle|
|[ancestry-05-g0hHetVM9UmmIDKU.htm](feats/ancestry-05-g0hHetVM9UmmIDKU.htm)|Fighting Horn|Corne de guerrier|libre|
|[ancestry-05-G1BPXzTzrUE4IndV.htm](feats/ancestry-05-G1BPXzTzrUE4IndV.htm)|Defy The Darkness|Défier l'obscurité|libre|
|[ancestry-05-GAh4CwCMS3Gsl8WH.htm](feats/ancestry-05-GAh4CwCMS3Gsl8WH.htm)|Tengu Feather Fan|Éventail à plumes tengu|libre|
|[ancestry-05-GgerQCCsGibaGWq0.htm](feats/ancestry-05-GgerQCCsGibaGWq0.htm)|Necromantic Physiology|Physiologie nécromantique|libre|
|[ancestry-05-gS9FYlD0Vt8yyZkP.htm](feats/ancestry-05-gS9FYlD0Vt8yyZkP.htm)|Grovel|Servilité|libre|
|[ancestry-05-GTA6QM7cH40L8H5Q.htm](feats/ancestry-05-GTA6QM7cH40L8H5Q.htm)|Fey Disguise|Déguisement de fée|libre|
|[ancestry-05-GZrvQo5FcoP5qocX.htm](feats/ancestry-05-GZrvQo5FcoP5qocX.htm)|Gaping Flesh|Chair béante|libre|
|[ancestry-05-HCxmS8QGHL5O7LUf.htm](feats/ancestry-05-HCxmS8QGHL5O7LUf.htm)|Offensive Analysis|Analyse offensive|libre|
|[ancestry-05-HEvk4ja8nJ3RVEqi.htm](feats/ancestry-05-HEvk4ja8nJ3RVEqi.htm)|Skin Split|Fragmentation de la peau|libre|
|[ancestry-05-HKGyFj2w5dzkf3SW.htm](feats/ancestry-05-HKGyFj2w5dzkf3SW.htm)|Natural Illusionist|Illusionniste naturel|libre|
|[ancestry-05-hrITlxkBqHvaiiRS.htm](feats/ancestry-05-hrITlxkBqHvaiiRS.htm)|Right-Hand Blood|Sang de la main droite|libre|
|[ancestry-05-I3EMC4pqkEWrodpq.htm](feats/ancestry-05-I3EMC4pqkEWrodpq.htm)|Practiced Paddler|Pagayeur accompli|libre|
|[ancestry-05-iCsHR5tdSXDHGjCv.htm](feats/ancestry-05-iCsHR5tdSXDHGjCv.htm)|Plague Sniffer|Renifleur de lèpre|libre|
|[ancestry-05-iCzJRywykSFwLto0.htm](feats/ancestry-05-iCzJRywykSFwLto0.htm)|Vanth's Weapon Execution|Exécution avec les armes des vanths|libre|
|[ancestry-05-iliy0ONIb8Hw6muA.htm](feats/ancestry-05-iliy0ONIb8Hw6muA.htm)|Swift Swimmer|Nageur rapide|libre|
|[ancestry-05-iNrR4WOB1UO9iiNE.htm](feats/ancestry-05-iNrR4WOB1UO9iiNE.htm)|Lifesense|Perception de la vie|libre|
|[ancestry-05-iS7MvHewCeQRT78d.htm](feats/ancestry-05-iS7MvHewCeQRT78d.htm)|Skillful Climber|Grimpeur compétent|libre|
|[ancestry-05-IsNs6hXQm9pJcXB1.htm](feats/ancestry-05-IsNs6hXQm9pJcXB1.htm)|Clever Shadow|Ombre Intelligente|libre|
|[ancestry-05-IUVzB39pRVyBFOEx.htm](feats/ancestry-05-IUVzB39pRVyBFOEx.htm)|Elemental Bulwark|Fortification élémentaire|libre|
|[ancestry-05-IY86Kvopp4ACuQsw.htm](feats/ancestry-05-IY86Kvopp4ACuQsw.htm)|Envenom Fangs|Crocs empoisonnés|libre|
|[ancestry-05-j0mlvJcuYGFuMG2S.htm](feats/ancestry-05-j0mlvJcuYGFuMG2S.htm)|Ally's Shelter|Abri de l'allié|libre|
|[ancestry-05-j1620BT5t0RLVB0C.htm](feats/ancestry-05-j1620BT5t0RLVB0C.htm)|Grippli Weapon Innovator|Innovateur avec les armes des gripplis|libre|
|[ancestry-05-jNrpvEqfncdGZPak.htm](feats/ancestry-05-jNrpvEqfncdGZPak.htm)|Halfling Ingenuity|Ingénuité halfeline|libre|
|[ancestry-05-juLtvliSzYPCmMa3.htm](feats/ancestry-05-juLtvliSzYPCmMa3.htm)|Winglets|Ailettes|libre|
|[ancestry-05-JvTSfyCkG70bmY7f.htm](feats/ancestry-05-JvTSfyCkG70bmY7f.htm)|Bloody Blows|Coups sanglants|libre|
|[ancestry-05-k8nWKHLYvAKMuwLd.htm](feats/ancestry-05-k8nWKHLYvAKMuwLd.htm)|Ageless Patience|Patience infinie|officielle|
|[ancestry-05-KcbSxOPYC5CUqbZQ.htm](feats/ancestry-05-KcbSxOPYC5CUqbZQ.htm)|Clever Improviser|Improvisateur astucieux|officielle|
|[ancestry-05-KeE8Ky38dCX8XaTg.htm](feats/ancestry-05-KeE8Ky38dCX8XaTg.htm)|Tree's Ward|Protection des arbres|libre|
|[ancestry-05-kGed3uQbZ7x5SBB8.htm](feats/ancestry-05-kGed3uQbZ7x5SBB8.htm)|Gecko's Grip|Prise du gecko|libre|
|[ancestry-05-ko399VJY2VsKE7iM.htm](feats/ancestry-05-ko399VJY2VsKE7iM.htm)|Unlock Secret|Découvrir le secret|libre|
|[ancestry-05-kpn4R65YlD38iAIS.htm](feats/ancestry-05-kpn4R65YlD38iAIS.htm)|Ritual Reversion|Rituel de réversibilité|libre|
|[ancestry-05-krgoR0Ykqw59MDbg.htm](feats/ancestry-05-krgoR0Ykqw59MDbg.htm)|Uncanny Awareness|Sensibilité instinctive|libre|
|[ancestry-05-KU488rIt9bOdTNMA.htm](feats/ancestry-05-KU488rIt9bOdTNMA.htm)|Well of Potential|Puits de potentiel|libre|
|[ancestry-05-KYKK1vLqGIxXH5Tu.htm](feats/ancestry-05-KYKK1vLqGIxXH5Tu.htm)|Speak with Kindred|Parler avec ses semblables|libre|
|[ancestry-05-l2JaSC3NA9S6Qq46.htm](feats/ancestry-05-l2JaSC3NA9S6Qq46.htm)|Myriad Forms|Myriades de formes|libre|
|[ancestry-05-l3PosipTLXANeoT8.htm](feats/ancestry-05-l3PosipTLXANeoT8.htm)|Step Lively|Pas vif|libre|
|[ancestry-05-LAArv2uv6TOkTzQO.htm](feats/ancestry-05-LAArv2uv6TOkTzQO.htm)|Quick Stow (Ratfolk)|Rangement rapide (Homme-rat)|libre|
|[ancestry-05-LC2EnXQ4MkhDLViM.htm](feats/ancestry-05-LC2EnXQ4MkhDLViM.htm)|Finned Ridges|Crête aileron|libre|
|[ancestry-05-lF5B49zbDG61sxXa.htm](feats/ancestry-05-lF5B49zbDG61sxXa.htm)|Lucky Break|Pause chanceuse|libre|
|[ancestry-05-llWKddRiyUHouaZx.htm](feats/ancestry-05-llWKddRiyUHouaZx.htm)|Empathic Calm|Calme empathique|libre|
|[ancestry-05-LNyTKhIZlM17026W.htm](feats/ancestry-05-LNyTKhIZlM17026W.htm)|Gnaw|Ronger|libre|
|[ancestry-05-M0B0rt6rk5MkHiBN.htm](feats/ancestry-05-M0B0rt6rk5MkHiBN.htm)|Hybrid Shape|Forme hybride (Anadi)|libre|
|[ancestry-05-m5JYglEObpWC3dhP.htm](feats/ancestry-05-m5JYglEObpWC3dhP.htm)|Sense Allies|Perception des alliés|libre|
|[ancestry-05-m64PtRsaiOkylVVk.htm](feats/ancestry-05-m64PtRsaiOkylVVk.htm)|Cunning Tinker|Bricoleur habile|libre|
|[ancestry-05-mfy0nasMIiMLUm3f.htm](feats/ancestry-05-mfy0nasMIiMLUm3f.htm)|Slip With The Breeze|Porté par la brise|libre|
|[ancestry-05-MwjnRpVn3br88Caj.htm](feats/ancestry-05-MwjnRpVn3br88Caj.htm)|Intuitive Illusions|Illusions intuitives|libre|
|[ancestry-05-n3CbbtK4fgBznIMf.htm](feats/ancestry-05-n3CbbtK4fgBznIMf.htm)|Empyreal Blessing|Bénédiction empyréenne|libre|
|[ancestry-05-NBDwiz1NDioc2eMP.htm](feats/ancestry-05-NBDwiz1NDioc2eMP.htm)|Energized Font|Source d'énergie|officielle|
|[ancestry-05-NBwH9wEeUfKfOg8R.htm](feats/ancestry-05-NBwH9wEeUfKfOg8R.htm)|Magpie Snatch|Pie voleuse|libre|
|[ancestry-05-NFsQOa3ynthYLVj6.htm](feats/ancestry-05-NFsQOa3ynthYLVj6.htm)|Ward Against Corruption|Protection contre la corruption|libre|
|[ancestry-05-NHlCta7B1Lmt3S7w.htm](feats/ancestry-05-NHlCta7B1Lmt3S7w.htm)|Jungle Runner|Coureur de la jungle|libre|
|[ancestry-05-NPATYCDg2cryH0Ya.htm](feats/ancestry-05-NPATYCDg2cryH0Ya.htm)|Conrasu Weapon Understanding|Compréhension des armes des conrasus|libre|
|[ancestry-05-Nv9hNKVSioDw5DHC.htm](feats/ancestry-05-Nv9hNKVSioDw5DHC.htm)|Ragdya's Revelry|Rivalité de Radgya|libre|
|[ancestry-05-O89mwKPRg0up0J0I.htm](feats/ancestry-05-O89mwKPRg0up0J0I.htm)|Focused Cat Nap|Sieste féline focalisée|libre|
|[ancestry-05-OCANjuCQ1wcCRBsn.htm](feats/ancestry-05-OCANjuCQ1wcCRBsn.htm)|Vishkanya Weapon Arts|Arts des armes des vishkanyas|libre|
|[ancestry-05-oEvcWFvBnsZ03OGW.htm](feats/ancestry-05-oEvcWFvBnsZ03OGW.htm)|Thrown Voice|Voix projetée|libre|
|[ancestry-05-OhyN9pBHdMpC126F.htm](feats/ancestry-05-OhyN9pBHdMpC126F.htm)|Hybrid Form|Forme hybride (Kitsune)|libre|
|[ancestry-05-OjUfwxMcM91CHLHP.htm](feats/ancestry-05-OjUfwxMcM91CHLHP.htm)|Well-Armed|Bras long|libre|
|[ancestry-05-OK8Fm7EG8wjoYZBX.htm](feats/ancestry-05-OK8Fm7EG8wjoYZBX.htm)|Favorable Winds|Vents favorables|libre|
|[ancestry-05-OK8X6QFRuRxpBMcZ.htm](feats/ancestry-05-OK8X6QFRuRxpBMcZ.htm)|Long Tongue|Langue longue|libre|
|[ancestry-05-oL6f86dy4yyx5N54.htm](feats/ancestry-05-oL6f86dy4yyx5N54.htm)|Tomb-Watcher's Glare|Regard du gardemort|libre|
|[ancestry-05-OOBSMpdAfYuiiQqo.htm](feats/ancestry-05-OOBSMpdAfYuiiQqo.htm)|Recognize Ambush|Reconnaître une embuscade|libre|
|[ancestry-05-OqJMjuK61uST7AlM.htm](feats/ancestry-05-OqJMjuK61uST7AlM.htm)|Gnome Weapon Innovator|Innovation avec les armes gnomes|libre|
|[ancestry-05-oqoftwLB6tvoLjnL.htm](feats/ancestry-05-oqoftwLB6tvoLjnL.htm)|Lightless Litheness|Souplesse sombre|libre|
|[ancestry-05-oSA1Ii6gTRGobeSO.htm](feats/ancestry-05-oSA1Ii6gTRGobeSO.htm)|Shadowy Disguise|Déguisement ombreux|libre|
|[ancestry-05-otr60veuPNygNDFY.htm](feats/ancestry-05-otr60veuPNygNDFY.htm)|Cunning Hair|Cheveux malins|libre|
|[ancestry-05-OXhrwQgnZdOi81Yi.htm](feats/ancestry-05-OXhrwQgnZdOi81Yi.htm)|Defy Death|Défier la mort|libre|
|[ancestry-05-P19AnciwNcSqxU7z.htm](feats/ancestry-05-P19AnciwNcSqxU7z.htm)|Distant Cackle|Ricanement distant|libre|
|[ancestry-05-pc00hoz4ILmqUwSC.htm](feats/ancestry-05-pc00hoz4ILmqUwSC.htm)|Devilish Wiles|Ruses diaboliques|libre|
|[ancestry-05-PdIN91xKsZ4z7p17.htm](feats/ancestry-05-PdIN91xKsZ4z7p17.htm)|Transposable Compliance|Conformité trasnposable|libre|
|[ancestry-05-PncXj46fgSwTWRl6.htm](feats/ancestry-05-PncXj46fgSwTWRl6.htm)|Ankle Bite|Morsure à la cheville|libre|
|[ancestry-05-pqezW5dqha1I32Ld.htm](feats/ancestry-05-pqezW5dqha1I32Ld.htm)|Inspire Imitation|Exemplarité|officielle|
|[ancestry-05-PRlN77xL6Bm2gUIp.htm](feats/ancestry-05-PRlN77xL6Bm2gUIp.htm)|Grippli Glide|Planeur grippli|libre|
|[ancestry-05-psgPbKsbqfz6Qt4P.htm](feats/ancestry-05-psgPbKsbqfz6Qt4P.htm)|Speak With Flowers|Parler avec les fleurs|libre|
|[ancestry-05-PWKFdaNghx1YMKlA.htm](feats/ancestry-05-PWKFdaNghx1YMKlA.htm)|Hunter's Fangs|Crocs du chasseur|libre|
|[ancestry-05-QE8asCPqyrdenll0.htm](feats/ancestry-05-QE8asCPqyrdenll0.htm)|Lab Rat|Rat de labo|libre|
|[ancestry-05-qKNo7Sr1dtVqhhAa.htm](feats/ancestry-05-qKNo7Sr1dtVqhhAa.htm)|Inured to the Heat|Habitué à la chaleur|libre|
|[ancestry-05-qkXRucQCLLS3VoMa.htm](feats/ancestry-05-qkXRucQCLLS3VoMa.htm)|Friendform|Forme amicale|libre|
|[ancestry-05-qUZnrueC2a0zf95N.htm](feats/ancestry-05-qUZnrueC2a0zf95N.htm)|Runtsage|Ami des avortons|libre|
|[ancestry-05-qXl2cOh3wL3QszCy.htm](feats/ancestry-05-qXl2cOh3wL3QszCy.htm)|Martial Experience|Expérience martiale|libre|
|[ancestry-05-qZUdGd2khS9cq4hJ.htm](feats/ancestry-05-qZUdGd2khS9cq4hJ.htm)|Shifting Faces|Visages changeants|libre|
|[ancestry-05-rFmJVDdB313EibTs.htm](feats/ancestry-05-rFmJVDdB313EibTs.htm)|Eat Fortune|Avaler la chance|libre|
|[ancestry-05-RkEvEqwc8pCBcusz.htm](feats/ancestry-05-RkEvEqwc8pCBcusz.htm)|Hopping Stride|Marche bondissante|libre|
|[ancestry-05-rnEfO5eyRw7Fywzb.htm](feats/ancestry-05-rnEfO5eyRw7Fywzb.htm)|Tail Spin|Queue renversante|libre|
|[ancestry-05-rp3mjgFXBVZYCleU.htm](feats/ancestry-05-rp3mjgFXBVZYCleU.htm)|Inoculation Subroutine|Sous-routine d'inoculation|libre|
|[ancestry-05-RP7TlsqNuge0dltp.htm](feats/ancestry-05-RP7TlsqNuge0dltp.htm)|Kitsune Spell Mysteries|Mystères des sorts des kitsunes|libre|
|[ancestry-05-Rr7GdUjXhGhV04Pe.htm](feats/ancestry-05-Rr7GdUjXhGhV04Pe.htm)|Supernatural Charm|Charme surnaturel|officielle|
|[ancestry-05-RrtqD8WmoUumauJD.htm](feats/ancestry-05-RrtqD8WmoUumauJD.htm)|Mask Of Power|Masque de pouvoir|libre|
|[ancestry-05-rTjGshtUMHnlBUfH.htm](feats/ancestry-05-rTjGshtUMHnlBUfH.htm)|Marine Ally|Allié marin|libre|
|[ancestry-05-s1swBWSqtfrXTJHK.htm](feats/ancestry-05-s1swBWSqtfrXTJHK.htm)|Guided by the Stars|Guidé par les étoiles|libre|
|[ancestry-05-SBhzem6n3buoxlG5.htm](feats/ancestry-05-SBhzem6n3buoxlG5.htm)|Extinguish Light|Éteindre la lumière|libre|
|[ancestry-05-scA2b141swxUPw8M.htm](feats/ancestry-05-scA2b141swxUPw8M.htm)|Garuda's Squall|Rafale du Garuda|libre|
|[ancestry-05-scNrNhnGTgPIzoj7.htm](feats/ancestry-05-scNrNhnGTgPIzoj7.htm)|Ancestral Suspicion|Suspicion ancestrale|libre|
|[ancestry-05-sGSf5BdopT0zWOWs.htm](feats/ancestry-05-sGSf5BdopT0zWOWs.htm)|Sheltering Slab|Bloc protecteur|libre|
|[ancestry-05-ShH7Wl7xfJL07DZC.htm](feats/ancestry-05-ShH7Wl7xfJL07DZC.htm)|Leshy Glide|Vol-plané léchi|libre|
|[ancestry-05-sQLgEcqoQ0SYtbTg.htm](feats/ancestry-05-sQLgEcqoQ0SYtbTg.htm)|Tail Snatch|Arrachage avec la queue|libre|
|[ancestry-05-sTqdFqWVL9yxi5wt.htm](feats/ancestry-05-sTqdFqWVL9yxi5wt.htm)|Protective Sheath|Fourreau protecteur|libre|
|[ancestry-05-SWNmYaj0OSPhhIqO.htm](feats/ancestry-05-SWNmYaj0OSPhhIqO.htm)|Mutate Weapon|Arme mutante|libre|
|[ancestry-05-SYcTallEKEaJeNGw.htm](feats/ancestry-05-SYcTallEKEaJeNGw.htm)|Swimming Poppet|Poupée nageuse|libre|
|[ancestry-05-SzZFmcfLoHMAS0gt.htm](feats/ancestry-05-SzZFmcfLoHMAS0gt.htm)|Cloud Gazer|Observateur de nuage|libre|
|[ancestry-05-t3IzY8uSyFj3aGmh.htm](feats/ancestry-05-t3IzY8uSyFj3aGmh.htm)|Shed Tail|Autotomie caudale|libre|
|[ancestry-05-tB6V6rWv8vAFsKsX.htm](feats/ancestry-05-tB6V6rWv8vAFsKsX.htm)|Nanite Shroud|Voile de nanites|libre|
|[ancestry-05-ThqMksZRxNB18ivs.htm](feats/ancestry-05-ThqMksZRxNB18ivs.htm)|Advanced Targeting System|Système de ciblage avancé|libre|
|[ancestry-05-TlMSleKR8Bh0EuSW.htm](feats/ancestry-05-TlMSleKR8Bh0EuSW.htm)|Climbing Claws|Griffes du grimpeur|libre|
|[ancestry-05-TvVqZHp7qvkPakKf.htm](feats/ancestry-05-TvVqZHp7qvkPakKf.htm)|Mistaken Identity|Identité erronée|libre|
|[ancestry-05-twnZopGlB392hmqH.htm](feats/ancestry-05-twnZopGlB392hmqH.htm)|Project Persona|Accoutrement imaginaire|libre|
|[ancestry-05-U5FcfRvveTKtgebq.htm](feats/ancestry-05-U5FcfRvveTKtgebq.htm)|Torch Goblin|Gobelin torche|libre|
|[ancestry-05-ulrGnvF0KAgEaifX.htm](feats/ancestry-05-ulrGnvF0KAgEaifX.htm)|Cornered Fury|Furie acculée|libre|
|[ancestry-05-uQGo33E4haaFNg6u.htm](feats/ancestry-05-uQGo33E4haaFNg6u.htm)|Pride Hunter|Chasseur de la meute|libre|
|[ancestry-05-UuVz1QY7QXD5cnLu.htm](feats/ancestry-05-UuVz1QY7QXD5cnLu.htm)|Fledgling Flight|Vol de l'oiselet|libre|
|[ancestry-05-v0ovKn4ZKmtXfXnu.htm](feats/ancestry-05-v0ovKn4ZKmtXfXnu.htm)|Vanara Weapon Trickery|Ruse avec les armes des vanaras|libre|
|[ancestry-05-V4W2hTr5lm5vS8Dq.htm](feats/ancestry-05-V4W2hTr5lm5vS8Dq.htm)|Nagaji Spell Mysteries|Mystères des sorts des nagajis|libre|
|[ancestry-05-vdowlFFknihiz5pm.htm](feats/ancestry-05-vdowlFFknihiz5pm.htm)|Intercorporate|Intercorporer|libre|
|[ancestry-05-VFBQ1MskOascFDNf.htm](feats/ancestry-05-VFBQ1MskOascFDNf.htm)|Steam Spell|Sort de vapeur|libre|
|[ancestry-05-VK2vAl1SfI4Qrtkt.htm](feats/ancestry-05-VK2vAl1SfI4Qrtkt.htm)|Loyal Empath|Empathe loyal|libre|
|[ancestry-05-VLCAFIXzUtiv1VuE.htm](feats/ancestry-05-VLCAFIXzUtiv1VuE.htm)|Flexible Tail|Queue souple|libre|
|[ancestry-05-VNmmHrMAVk3Tiw1d.htm](feats/ancestry-05-VNmmHrMAVk3Tiw1d.htm)|Kobold Weapon Innovator|Innovateur avec les armes kobolds|libre|
|[ancestry-05-VQmKNh0QPHzYMmee.htm](feats/ancestry-05-VQmKNh0QPHzYMmee.htm)|Treacherous Earth|Terre traîtresse|libre|
|[ancestry-05-vW7tWi3jK7z2Clen.htm](feats/ancestry-05-vW7tWi3jK7z2Clen.htm)|Dwarven Weapon Cunning|Connaissances des armes naines|officielle|
|[ancestry-05-vxE9hBKB6F2ctOX3.htm](feats/ancestry-05-vxE9hBKB6F2ctOX3.htm)|Mask Of Rejection|Masque de rejet|libre|
|[ancestry-05-wigOSPSxXnapFxeh.htm](feats/ancestry-05-wigOSPSxXnapFxeh.htm)|Dogfang Bite|Morsure crocdechien|libre|
|[ancestry-05-wjhhlh82MABhfxCO.htm](feats/ancestry-05-wjhhlh82MABhfxCO.htm)|Strix Vengeance|Vengeance strix|libre|
|[ancestry-05-WPz97m5FNlbLIQ6p.htm](feats/ancestry-05-WPz97m5FNlbLIQ6p.htm)|Long-Nosed Form|Forme au long nez|libre|
|[ancestry-05-Wwr8VilSybQgVtin.htm](feats/ancestry-05-Wwr8VilSybQgVtin.htm)|Athletic Might|Puissance athlétique|libre|
|[ancestry-05-WWSwcvIjGUQOKKuD.htm](feats/ancestry-05-WWSwcvIjGUQOKKuD.htm)|Taste Blood|Gôuter le sang|libre|
|[ancestry-05-X6tVQ5S7H7GuCZux.htm](feats/ancestry-05-X6tVQ5S7H7GuCZux.htm)|Tengu Weapon Study|Étude des armes des tengus|libre|
|[ancestry-05-XM4zR0q9rBxjR3lG.htm](feats/ancestry-05-XM4zR0q9rBxjR3lG.htm)|Darkseer|Voyant ombral|libre|
|[ancestry-05-xoB4RDYkdAALt0U4.htm](feats/ancestry-05-xoB4RDYkdAALt0U4.htm)|Magical Resistance|Résistance magique|libre|
|[ancestry-05-XRLtXJXGswe20QKY.htm](feats/ancestry-05-XRLtXJXGswe20QKY.htm)|Towering Presence|Présence imposante|libre|
|[ancestry-05-xruqgywumtExg8me.htm](feats/ancestry-05-xruqgywumtExg8me.htm)|Debilitating Venom|Venin débilitant|libre|
|[ancestry-05-XYtnVNKt6uPcRrdH.htm](feats/ancestry-05-XYtnVNKt6uPcRrdH.htm)|Tree Climber (Goblin)|Grimpeur aux arbres (Gobelin)|libre|
|[ancestry-05-y0Ru2hRmoRwkRsxZ.htm](feats/ancestry-05-y0Ru2hRmoRwkRsxZ.htm)|Tongue Disarm|Désarmement avec la langue|libre|
|[ancestry-05-Y3IUIsopvN13vEvZ.htm](feats/ancestry-05-Y3IUIsopvN13vEvZ.htm)|Orc Weapon Carnage|Carnage avec les armes orques|officielle|
|[ancestry-05-ya9YlhUA4WspUlHB.htm](feats/ancestry-05-ya9YlhUA4WspUlHB.htm)|Clan Protector|Protecteur de clan|libre|
|[ancestry-05-YeVDITJplindA27l.htm](feats/ancestry-05-YeVDITJplindA27l.htm)|Victorious Vigor|Vigueur dans la victoire|officielle|
|[ancestry-05-YndH82FX7KLawNBW.htm](feats/ancestry-05-YndH82FX7KLawNBW.htm)|Tenacious Net|Filet tenace|libre|
|[ancestry-05-yqtaAZR9jfen6gEW.htm](feats/ancestry-05-yqtaAZR9jfen6gEW.htm)|Iruxi Glide|Planeur iruxi|libre|
|[ancestry-05-z0Bwy7lxU3DIugpo.htm](feats/ancestry-05-z0Bwy7lxU3DIugpo.htm)|Animal Speaker|Orateur animal|libre|
|[ancestry-05-z2z3UTayfUhcGazv.htm](feats/ancestry-05-z2z3UTayfUhcGazv.htm)|Scar-Thick Skin|Peau épaisse balafrée|libre|
|[ancestry-05-Z30nSkai5UmZCyKu.htm](feats/ancestry-05-Z30nSkai5UmZCyKu.htm)|Past Life|Vie passée|libre|
|[ancestry-05-Z56DtGBd3AcZOCeG.htm](feats/ancestry-05-Z56DtGBd3AcZOCeG.htm)|Cultural Adaptability|Adaptabilité culturelle|officielle|
|[ancestry-05-zc32MIxa1E6357D6.htm](feats/ancestry-05-zc32MIxa1E6357D6.htm)|Elven Weapon Elegance|Élégance avec les armes elfiques|officielle|
|[ancestry-05-zgriBCYR4TmBoDqO.htm](feats/ancestry-05-zgriBCYR4TmBoDqO.htm)|Forest Stealth|Discrétion forestière|libre|
|[ancestry-05-ZOJsLNziWQ7Ri3x9.htm](feats/ancestry-05-ZOJsLNziWQ7Ri3x9.htm)|Undead Slayer|Tueur de mort-vivant|libre|
|[ancestry-05-ZOOP2RzRJnpVqnCr.htm](feats/ancestry-05-ZOOP2RzRJnpVqnCr.htm)|Aquatic Conversationalist|Conversant aquatique|libre|
|[ancestry-05-ZRI1OXaaa4ZC6EK1.htm](feats/ancestry-05-ZRI1OXaaa4ZC6EK1.htm)|Easily Dismissed|Facilement négligé|libre|
|[ancestry-05-zwqawaXccARDV0jL.htm](feats/ancestry-05-zwqawaXccARDV0jL.htm)|Feathered Cloak|Cape emplumée|libre|
|[ancestry-05-ZwvSiuFFsyNGJiB3.htm](feats/ancestry-05-ZwvSiuFFsyNGJiB3.htm)|Ceremony of Sunlight|Cérémonie de la lumière solaire|libre|
|[ancestry-05-Zz5A8Yg0jGSK8GNu.htm](feats/ancestry-05-Zz5A8Yg0jGSK8GNu.htm)|Enthralling Allure|Attrait fascinant|libre|
|[ancestry-05-ZZxePfQkBPuTHkt1.htm](feats/ancestry-05-ZZxePfQkBPuTHkt1.htm)|Boulder Roll|Pas du roc|officielle|
|[ancestry-09-0BUSnsCKOeFCJKEp.htm](feats/ancestry-09-0BUSnsCKOeFCJKEp.htm)|Moderate Enhance Venom|Amélioration du venin modérée|libre|
|[ancestry-09-0R15gtdXoXbrD8As.htm](feats/ancestry-09-0R15gtdXoXbrD8As.htm)|Fortified Mind|Esprit fortifié|libre|
|[ancestry-09-0X4HZk036A5meZbo.htm](feats/ancestry-09-0X4HZk036A5meZbo.htm)|Serpentine Swimmer|Nageur serpentin|libre|
|[ancestry-09-1WheVs50iwMBi6KC.htm](feats/ancestry-09-1WheVs50iwMBi6KC.htm)|Archon Magic|Magie des archons|libre|
|[ancestry-09-2GrlSP1xhKIz4G8B.htm](feats/ancestry-09-2GrlSP1xhKIz4G8B.htm)|Glory And Valor!|Gloire et valeur !|libre|
|[ancestry-09-2i82PgD1BEtAVeZt.htm](feats/ancestry-09-2i82PgD1BEtAVeZt.htm)|Call of Elysium|Appel de l'Élysée|libre|
|[ancestry-09-2kAyZ3LB28BDhXKa.htm](feats/ancestry-09-2kAyZ3LB28BDhXKa.htm)|Bark and Tendril|Écorce et liane|libre|
|[ancestry-09-3N6T7AQETVKaAwiR.htm](feats/ancestry-09-3N6T7AQETVKaAwiR.htm)|Demon Magic|Magie des démons|libre|
|[ancestry-09-3TilQ1l97NV0okph.htm](feats/ancestry-09-3TilQ1l97NV0okph.htm)|Invoke The Elements (Veil May)|Invoquer les éléments (Vierge voilée)|libre|
|[ancestry-09-3Y1k2eAMqCdcCGmK.htm](feats/ancestry-09-3Y1k2eAMqCdcCGmK.htm)|Stone Bones|Os de pierre|libre|
|[ancestry-09-3yLdRzh16sT8RgFV.htm](feats/ancestry-09-3yLdRzh16sT8RgFV.htm)|Wheedle and Jig|Roue et gigue|libre|
|[ancestry-09-4Ai8aNHYw7oEj7eE.htm](feats/ancestry-09-4Ai8aNHYw7oEj7eE.htm)|Gripping Limbs|Membres préhensiles|libre|
|[ancestry-09-4cH2RoWsgXYaWIPa.htm](feats/ancestry-09-4cH2RoWsgXYaWIPa.htm)|Eerie Compression|Compression inquiétante|libre|
|[ancestry-09-4EgueMqUm0ebKV4J.htm](feats/ancestry-09-4EgueMqUm0ebKV4J.htm)|Dragonblood Paragon|Parangon sangdragon|libre|
|[ancestry-09-4hFeaF4MlqIHb6gb.htm](feats/ancestry-09-4hFeaF4MlqIHb6gb.htm)|Azata Magic|Magie des azatas|libre|
|[ancestry-09-4Jwtl2FvxskruHQv.htm](feats/ancestry-09-4Jwtl2FvxskruHQv.htm)|Invoke The Elements|Invoquer les éléments|libre|
|[ancestry-09-56HvICglqH7uR3AY.htm](feats/ancestry-09-56HvICglqH7uR3AY.htm)|Cantorian Rejuvenation|Rajeunissement cantorien|libre|
|[ancestry-09-5j3nLvicUVmPqzA7.htm](feats/ancestry-09-5j3nLvicUVmPqzA7.htm)|Tongue Tether|Langue reliée|libre|
|[ancestry-09-5S1nPoxHTgu9MGGV.htm](feats/ancestry-09-5S1nPoxHTgu9MGGV.htm)|Ricocheting Leap|Saut ricochant|libre|
|[ancestry-09-67UXSHUUH0K36xyB.htm](feats/ancestry-09-67UXSHUUH0K36xyB.htm)|Smoke Sight|Vision de fumée|libre|
|[ancestry-09-6eXffyvqxpIzig2O.htm](feats/ancestry-09-6eXffyvqxpIzig2O.htm)|Offensive Subroutine|Sous-routine offensive|libre|
|[ancestry-09-6px0s2nE8fHVWswz.htm](feats/ancestry-09-6px0s2nE8fHVWswz.htm)|Envenom Strike|Frappes envenimées|libre|
|[ancestry-09-6WwxTbX8KvU3Xxak.htm](feats/ancestry-09-6WwxTbX8KvU3Xxak.htm)|Janni Hospitality|Hospitalité jann|libre|
|[ancestry-09-7cM7uKRKQDWz5eeu.htm](feats/ancestry-09-7cM7uKRKQDWz5eeu.htm)|Heroes' Call|Appel des héros|libre|
|[ancestry-09-7eyy63BkMv3enk5r.htm](feats/ancestry-09-7eyy63BkMv3enk5r.htm)|Internal Respirator|Respirateur interne|libre|
|[ancestry-09-7RFu7EwwvjHMP0dq.htm](feats/ancestry-09-7RFu7EwwvjHMP0dq.htm)|Rakshasa Magic|Magie des rakshasas|libre|
|[ancestry-09-7vUOlVqjheZV0Nmc.htm](feats/ancestry-09-7vUOlVqjheZV0Nmc.htm)|Vivacious Conduit|Conduit vivace|officielle|
|[ancestry-09-8aRa9VHoDKl9B1Z1.htm](feats/ancestry-09-8aRa9VHoDKl9B1Z1.htm)|Brightness Seeker|Aspirant à l'illumination|libre|
|[ancestry-09-8buK32r3i3aGyyOR.htm](feats/ancestry-09-8buK32r3i3aGyyOR.htm)|Shaitan Magic|Magie des shaitans|libre|
|[ancestry-09-8ukixWL8MBJOhPbW.htm](feats/ancestry-09-8ukixWL8MBJOhPbW.htm)|Pinch Time|Temps contracté|libre|
|[ancestry-09-8znlUoKgr8mmLPe1.htm](feats/ancestry-09-8znlUoKgr8mmLPe1.htm)|Wing Step|Pas ailé|libre|
|[ancestry-09-9LTBRvuQgXKZFiZc.htm](feats/ancestry-09-9LTBRvuQgXKZFiZc.htm)|Pack Tactics|Tactiques de la meute|libre|
|[ancestry-09-a32r2n9j36khV0Cp.htm](feats/ancestry-09-a32r2n9j36khV0Cp.htm)|Spore Cloud|Nuage de spores|libre|
|[ancestry-09-AGznaQpn2cE6jz9H.htm](feats/ancestry-09-AGznaQpn2cE6jz9H.htm)|Dangle|Suspendre|libre|
|[ancestry-09-aIm2qi4JZerthZmF.htm](feats/ancestry-09-aIm2qi4JZerthZmF.htm)|Elf Step|Pas elfique|libre|
|[ancestry-09-AJVx3sHm6f6i8ZQW.htm](feats/ancestry-09-AJVx3sHm6f6i8ZQW.htm)|Efreeti Magic|Magie des éfrits|libre|
|[ancestry-09-AKMx0GYKLjy7jTXl.htm](feats/ancestry-09-AKMx0GYKLjy7jTXl.htm)|No Evidence|Pas de preuve|libre|
|[ancestry-09-aOIZvx5fx5jVHHOO.htm](feats/ancestry-09-aOIZvx5fx5jVHHOO.htm)|Life Leap|Bond vital|libre|
|[ancestry-09-AQvtqj2h2n5n8YYg.htm](feats/ancestry-09-AQvtqj2h2n5n8YYg.htm)|Lesser Augmentation|Augmentation inférieure|libre|
|[ancestry-09-aSgLhr8mM53DWbFc.htm](feats/ancestry-09-aSgLhr8mM53DWbFc.htm)|Endless Memories|Souvenirs sans fin|libre|
|[ancestry-09-ATiQDz27aiBTAt17.htm](feats/ancestry-09-ATiQDz27aiBTAt17.htm)|Roll with It|Roule avec çà|libre|
|[ancestry-09-AYXqqeLSqCYhgUY7.htm](feats/ancestry-09-AYXqqeLSqCYhgUY7.htm)|Core Attunement|Harmonisation du coeur|libre|
|[ancestry-09-aznyI5mfMdEFSDr8.htm](feats/ancestry-09-aznyI5mfMdEFSDr8.htm)|Hardy Traveler|Voyageur robuste|libre|
|[ancestry-09-B53sWzd5irAoLn2U.htm](feats/ancestry-09-B53sWzd5irAoLn2U.htm)|Bone Caller|Invocateur d'os|libre|
|[ancestry-09-B5HiNholWMwYdHTC.htm](feats/ancestry-09-B5HiNholWMwYdHTC.htm)|Energy Blessed|Énergie bénie|libre|
|[ancestry-09-BaVO8UU5ZkL8OZZj.htm](feats/ancestry-09-BaVO8UU5ZkL8OZZj.htm)|Dragon's Breath|Souffle du dragon|libre|
|[ancestry-09-bJTcHDqHOI6xD4AT.htm](feats/ancestry-09-bJTcHDqHOI6xD4AT.htm)|Unyielding Disguise|Déguisement infaillible|libre|
|[ancestry-09-bo4JG09pkoS7ywSZ.htm](feats/ancestry-09-bo4JG09pkoS7ywSZ.htm)|Cave Climber|Grimpeur des cavernes|officielle|
|[ancestry-09-bpNpqxtsBJIPYSX9.htm](feats/ancestry-09-bpNpqxtsBJIPYSX9.htm)|Ceremony of Strengthened Hand|Cérémonie de la main fortifiée|libre|
|[ancestry-09-ByqxxJkJiNFtjghh.htm](feats/ancestry-09-ByqxxJkJiNFtjghh.htm)|Mask Of Pain|Masque de douleur|libre|
|[ancestry-09-C2PmqTxQgWGTHluf.htm](feats/ancestry-09-C2PmqTxQgWGTHluf.htm)|Studious Adept|Adepte studieux|libre|
|[ancestry-09-C80vQCKQBGRaqcmq.htm](feats/ancestry-09-C80vQCKQBGRaqcmq.htm)|Multitalented|Multiples talents|officielle|
|[ancestry-09-cCv9pgyXzZ0TQmZg.htm](feats/ancestry-09-cCv9pgyXzZ0TQmZg.htm)|Big Mouth|Large bouche|libre|
|[ancestry-09-CCY6VsGjp5fdmM6K.htm](feats/ancestry-09-CCY6VsGjp5fdmM6K.htm)|Pride in Arms|Fierté militaire|libre|
|[ancestry-09-chM3Pya6H8QGnEGo.htm](feats/ancestry-09-chM3Pya6H8QGnEGo.htm)|Water Dancer|Danseur aquatique|libre|
|[ancestry-09-cmhfYMEM4uzrNIiV.htm](feats/ancestry-09-cmhfYMEM4uzrNIiV.htm)|Energize Wings|Énergiser les ailes|libre|
|[ancestry-09-COP89tjrNhEucuRW.htm](feats/ancestry-09-COP89tjrNhEucuRW.htm)|Mountain's Stoutness|Robustesse de la montagne|officielle|
|[ancestry-09-cRMUYCnkkE9lSEhh.htm](feats/ancestry-09-cRMUYCnkkE9lSEhh.htm)|Irrepressible (Halfling)|Irrépressible (Halfelin)|officielle|
|[ancestry-09-d022Gp8PjS4Q0ZAC.htm](feats/ancestry-09-d022Gp8PjS4Q0ZAC.htm)|Shared Luck (Catfolk)|Chance partagée (Homme-félin)|libre|
|[ancestry-09-DhqsKns2SaGcOKO9.htm](feats/ancestry-09-DhqsKns2SaGcOKO9.htm)|Read The Stars|Lecture des astres|libre|
|[ancestry-09-DLE2rr5I1TBAk0I3.htm](feats/ancestry-09-DLE2rr5I1TBAk0I3.htm)|Disorienting Venom|Venin désorientant|libre|
|[ancestry-09-DnfVQGLk3PAl8UWh.htm](feats/ancestry-09-DnfVQGLk3PAl8UWh.htm)|Hungry Goblin|Gobelin affamé|libre|
|[ancestry-09-dtmLxbSy2H8h8e4N.htm](feats/ancestry-09-dtmLxbSy2H8h8e4N.htm)|Skittering Scuttle|Précipitation hâtive|officielle|
|[ancestry-09-DTU4jrz9YNr7e62e.htm](feats/ancestry-09-DTU4jrz9YNr7e62e.htm)|Larcenous Tail|Queue cleptomane|libre|
|[ancestry-09-DunjA0vLZeozcNxu.htm](feats/ancestry-09-DunjA0vLZeozcNxu.htm)|Devil Magic|Magie des diables|libre|
|[ancestry-09-Eoy0zhpf8tYrTHN4.htm](feats/ancestry-09-Eoy0zhpf8tYrTHN4.htm)|Shory Aeromancer|Aéromancien rivain|libre|
|[ancestry-09-esKk5XrnlqRayDPG.htm](feats/ancestry-09-esKk5XrnlqRayDPG.htm)|Angelic Magic|Magie des anges|libre|
|[ancestry-09-Ezk3OgfPaRlEEyAD.htm](feats/ancestry-09-Ezk3OgfPaRlEEyAD.htm)|Virtue-Forged Tattoos|Tatouages vertus-forgés|libre|
|[ancestry-09-fcSzCdQ9y800BdOv.htm](feats/ancestry-09-fcSzCdQ9y800BdOv.htm)|Velstrac Magic|Magie des velstracs|libre|
|[ancestry-09-fEzxwMCNyvooYqdn.htm](feats/ancestry-09-fEzxwMCNyvooYqdn.htm)|Miraculous Repair|Réparation miraculeuse|libre|
|[ancestry-09-FGbrxFmNeOfPHFOG.htm](feats/ancestry-09-FGbrxFmNeOfPHFOG.htm)|Bone Missile|Projectile d'os|libre|
|[ancestry-09-fiA3rfqPcCKFCI83.htm](feats/ancestry-09-fiA3rfqPcCKFCI83.htm)|Towering Growth|Croissance vertigineuse|libre|
|[ancestry-09-FrzskqwNWexKY5BA.htm](feats/ancestry-09-FrzskqwNWexKY5BA.htm)|Terrain Advantage|Avantage du terrain|libre|
|[ancestry-09-ftntYtiKGoPBpvcv.htm](feats/ancestry-09-ftntYtiKGoPBpvcv.htm)|Qlippoth Magic|Magie des qlippoths|libre|
|[ancestry-09-fu1cTh93zgGweduf.htm](feats/ancestry-09-fu1cTh93zgGweduf.htm)|Wary Skulker|Rôdeur prudent|libre|
|[ancestry-09-FUOUyhHufNH4ri7H.htm](feats/ancestry-09-FUOUyhHufNH4ri7H.htm)|Wings Of Air|Ailes d'air|libre|
|[ancestry-09-FZZXgmVMAmvKHWIo.htm](feats/ancestry-09-FZZXgmVMAmvKHWIo.htm)|Breath Like Honey|Haleine de miel|libre|
|[ancestry-09-G6rCbMrHacYWNu1K.htm](feats/ancestry-09-G6rCbMrHacYWNu1K.htm)|Aggravating Scratch|Grattement aggravant|libre|
|[ancestry-09-g9AOSz4QDAlUee9M.htm](feats/ancestry-09-g9AOSz4QDAlUee9M.htm)|Fox Trick|Astuce du renard|libre|
|[ancestry-09-gWyCNTWUhxneOBne.htm](feats/ancestry-09-gWyCNTWUhxneOBne.htm)|Helpful Halfling|Halfelin utile|libre|
|[ancestry-09-h11M3QrIHKLj2ezy.htm](feats/ancestry-09-h11M3QrIHKLj2ezy.htm)|Well-Groomed|Bien entretenu|libre|
|[ancestry-09-HfebybiUNW8mXOfP.htm](feats/ancestry-09-HfebybiUNW8mXOfP.htm)|Returning Throw|Lancer retourné|libre|
|[ancestry-09-hgfRHSS5BsoyQ9Fj.htm](feats/ancestry-09-hgfRHSS5BsoyQ9Fj.htm)|Marid Magic|Magie des marides|libre|
|[ancestry-09-HPW7Qi02lkVprW6V.htm](feats/ancestry-09-HPW7Qi02lkVprW6V.htm)|Silent Step|Pas silencieux|libre|
|[ancestry-09-Ht21JJ95wiHOgZoT.htm](feats/ancestry-09-Ht21JJ95wiHOgZoT.htm)|Telekinetic Slip|Dégagement télékinétique|libre|
|[ancestry-09-ieFjiZSlT9J4boqP.htm](feats/ancestry-09-ieFjiZSlT9J4boqP.htm)|Dangle (Vanara)|Suspendre (Vanara)|libre|
|[ancestry-09-IHFqUHxNChCPZPml.htm](feats/ancestry-09-IHFqUHxNChCPZPml.htm)|Garuda Magic|Magie des garudas|libre|
|[ancestry-09-IlzUi1viWgeRolU8.htm](feats/ancestry-09-IlzUi1viWgeRolU8.htm)|Legendary Size|Taille légendaire|libre|
|[ancestry-09-InTchkd50pzQok3f.htm](feats/ancestry-09-InTchkd50pzQok3f.htm)|Web Hunter|Chasseur de toile|libre|
|[ancestry-09-IQ9C7glCXVgYecz1.htm](feats/ancestry-09-IQ9C7glCXVgYecz1.htm)|Light From Darkness|De la lumière à l'obscurité|libre|
|[ancestry-09-iRpVW1DPKVxlzIzt.htm](feats/ancestry-09-iRpVW1DPKVxlzIzt.htm)|Night Magic|Magie de la nuit|libre|
|[ancestry-09-iXhMlT5rlLngybxX.htm](feats/ancestry-09-iXhMlT5rlLngybxX.htm)|Slip Into Shadow|Se glisser dans l'ombre|libre|
|[ancestry-09-IzNyByUBNH94MDOr.htm](feats/ancestry-09-IzNyByUBNH94MDOr.htm)|Fey Ascension|Ascension féerique|libre|
|[ancestry-09-JeMGDAD5s9AXCZ2G.htm](feats/ancestry-09-JeMGDAD5s9AXCZ2G.htm)|Animal Magic|Magie animale|libre|
|[ancestry-09-jNemcS5GqH8mnjV6.htm](feats/ancestry-09-jNemcS5GqH8mnjV6.htm)|Fiendish Wings|Ailes fiélones|libre|
|[ancestry-09-jpRVp1INDAlYWvlI.htm](feats/ancestry-09-jpRVp1INDAlYWvlI.htm)|Rakshasa Ravaged|Rakshasa ravageur|libre|
|[ancestry-09-jzfV4lJ0721Hfzq1.htm](feats/ancestry-09-jzfV4lJ0721Hfzq1.htm)|Squad Tactics|Tactiques d'escouade|libre|
|[ancestry-09-k0sXAn4PPq5nW9al.htm](feats/ancestry-09-k0sXAn4PPq5nW9al.htm)|Guiding Luck|Chance directrice|libre|
|[ancestry-09-K9ixUl7PrNbBHGdA.htm](feats/ancestry-09-K9ixUl7PrNbBHGdA.htm)|Sodbuster|Briseur de motte|libre|
|[ancestry-09-KgiYiDz1lWoBMRlF.htm](feats/ancestry-09-KgiYiDz1lWoBMRlF.htm)|Otherworldly Acumen|Perspicacité surnaturelle|libre|
|[ancestry-09-kIIcgcc5SWFkyiBj.htm](feats/ancestry-09-kIIcgcc5SWFkyiBj.htm)|Transcendent Realization|Réalisation transcendante|libre|
|[ancestry-09-kjRMXN95lQmIi2hP.htm](feats/ancestry-09-kjRMXN95lQmIi2hP.htm)|Earthsense|Sens de la terre|libre|
|[ancestry-09-kPyyZGD5L6b2Kl8C.htm](feats/ancestry-09-kPyyZGD5L6b2Kl8C.htm)|Constant Gaze|Regard constant|libre|
|[ancestry-09-KRgzuwwjT30KKvV4.htm](feats/ancestry-09-KRgzuwwjT30KKvV4.htm)|Eclectic Sword Training|Entraînement éclectique à l'épée|libre|
|[ancestry-09-KsFrWhIPVLOqxV07.htm](feats/ancestry-09-KsFrWhIPVLOqxV07.htm)|Accursed Claws|Griffes maudites|libre|
|[ancestry-09-KTLdk65OOAVixqtY.htm](feats/ancestry-09-KTLdk65OOAVixqtY.htm)|Stonewalker|Marchepierre|officielle|
|[ancestry-09-Kuv9P0NdtpE8yPJL.htm](feats/ancestry-09-Kuv9P0NdtpE8yPJL.htm)|Solar Rejuvenation (Ghoran)|Récupération solaire (Ghoran)|libre|
|[ancestry-09-l4ux6Mn2fklB2cXM.htm](feats/ancestry-09-l4ux6Mn2fklB2cXM.htm)|Evade Doom|Échapper au destin|libre|
|[ancestry-09-lbiFj4At5BxotaNY.htm](feats/ancestry-09-lbiFj4At5BxotaNY.htm)|Dracomancer|Dracomancien|libre|
|[ancestry-09-lBVzIet6IpufLXZg.htm](feats/ancestry-09-lBVzIet6IpufLXZg.htm)|Swift Application|Application rapide|libre|
|[ancestry-09-lHcDb9oXUdFupRdi.htm](feats/ancestry-09-lHcDb9oXUdFupRdi.htm)|Incredible Improvisation|Improvisation extraordinaire|officielle|
|[ancestry-09-LhpE0NsfNwYP6MOz.htm](feats/ancestry-09-LhpE0NsfNwYP6MOz.htm)|Freeze It!|Gêle-le !|libre|
|[ancestry-09-LK6niBb38mEraYRS.htm](feats/ancestry-09-LK6niBb38mEraYRS.htm)|Overcrowd|Bondé|libre|
|[ancestry-09-llWnSLYALh88iRGQ.htm](feats/ancestry-09-llWnSLYALh88iRGQ.htm)|Rain of Bolts|Pluie de carreaux|libre|
|[ancestry-09-LRBzEzpS19z3Eghd.htm](feats/ancestry-09-LRBzEzpS19z3Eghd.htm)|Sense Thoughts|Perception des pensées|libre|
|[ancestry-09-lT2sOwC6Pi5P4Yq0.htm](feats/ancestry-09-lT2sOwC6Pi5P4Yq0.htm)|Ancillary Motes|Particules auxiliaires|libre|
|[ancestry-09-m25EfSMmEjJPSyJj.htm](feats/ancestry-09-m25EfSMmEjJPSyJj.htm)|Perfume Cloud|Nuage de parfum|libre|
|[ancestry-09-m7aW56wExv2ieMFL.htm](feats/ancestry-09-m7aW56wExv2ieMFL.htm)|Invoke The Elements (Virga May)|Invoquer les éléments (vierge virga)|libre|
|[ancestry-09-mnH68QcFRtkMbNE0.htm](feats/ancestry-09-mnH68QcFRtkMbNE0.htm)|Battleforger|Forgeron de bataille|libre|
|[ancestry-09-MTp2j4N4H4wj07pH.htm](feats/ancestry-09-MTp2j4N4H4wj07pH.htm)|Heir of the Saoc|Héritier de Saoc|libre|
|[ancestry-09-mU8vTzrWX9fIlG0d.htm](feats/ancestry-09-mU8vTzrWX9fIlG0d.htm)|Grandmother's Wisdom|Sagesse de grand-mère|libre|
|[ancestry-09-mXNwdRSM9kZrT2Um.htm](feats/ancestry-09-mXNwdRSM9kZrT2Um.htm)|Fell Rider|Chevaucheur cruel|libre|
|[ancestry-09-N8Ci3w5gQ68rj6a6.htm](feats/ancestry-09-N8Ci3w5gQ68rj6a6.htm)|Arcane Camouflage|Camouflage arcanique|libre|
|[ancestry-09-Nb8iLgQeuHU73hQM.htm](feats/ancestry-09-Nb8iLgQeuHU73hQM.htm)|Mirror Refuge|Refuge miroir|libre|
|[ancestry-09-NEl8G3SlwxOR9Zx1.htm](feats/ancestry-09-NEl8G3SlwxOR9Zx1.htm)|Preemptive Reconfiguration|Reconfiguration préventive|libre|
|[ancestry-09-ngNzsvIpnj1iLfSC.htm](feats/ancestry-09-ngNzsvIpnj1iLfSC.htm)|Anarchic Arcana|Arcanes anarchiques|libre|
|[ancestry-09-nHoRM1gLL7MtIiCS.htm](feats/ancestry-09-nHoRM1gLL7MtIiCS.htm)|Djinni Magic|Magie des djinns|libre|
|[ancestry-09-NjPZbQjJJIygS1ru.htm](feats/ancestry-09-NjPZbQjJJIygS1ru.htm)|Shadow Sight|Vision d'ombre|libre|
|[ancestry-09-nKkbEKbE9vfKWKdd.htm](feats/ancestry-09-nKkbEKbE9vfKWKdd.htm)|Echoes In Stone|Échos dans la roche|libre|
|[ancestry-09-NnpUhj7d4RmfOKTE.htm](feats/ancestry-09-NnpUhj7d4RmfOKTE.htm)|Scorching Disarm|Désarmement brûlant|libre|
|[ancestry-09-nqDhQdkgsnvebUMr.htm](feats/ancestry-09-nqDhQdkgsnvebUMr.htm)|Snare Commando|Commando à pièges artisanaux|libre|
|[ancestry-09-nYSSdczKdRj7pdW6.htm](feats/ancestry-09-nYSSdczKdRj7pdW6.htm)|Ceremony of Aeon's Guidance|Cérémonie de l'assistance de l'aéon|libre|
|[ancestry-09-o1LhDmwymrpEy1u2.htm](feats/ancestry-09-o1LhDmwymrpEy1u2.htm)|Riptide|Contre-courant|libre|
|[ancestry-09-O2iFrAt7hyELJlIR.htm](feats/ancestry-09-O2iFrAt7hyELJlIR.htm)|Slip The Grasp|Échapper à la prise|libre|
|[ancestry-09-O80QCYMqz4VZsfxT.htm](feats/ancestry-09-O80QCYMqz4VZsfxT.htm)|Hefting Shadow|Ombre contenante|libre|
|[ancestry-09-O9z1MJpEaf6Y1Acd.htm](feats/ancestry-09-O9z1MJpEaf6Y1Acd.htm)|Dance Underfoot|Danse sous les pieds|libre|
|[ancestry-09-oGPX29AOsyHj18mK.htm](feats/ancestry-09-oGPX29AOsyHj18mK.htm)|Flower Magic|Magie des fleurs|libre|
|[ancestry-09-ovMIxhiStlPE7tty.htm](feats/ancestry-09-ovMIxhiStlPE7tty.htm)|Divine Countermeasures|Contre mesures divines|libre|
|[ancestry-09-p5dLNQ5HtM9Fq8SN.htm](feats/ancestry-09-p5dLNQ5HtM9Fq8SN.htm)|Invoke The Elements (Brine May)|Invoquer les éléments (Vierge de saumure)|libre|
|[ancestry-09-pe6cHJzzMm5Tr25G.htm](feats/ancestry-09-pe6cHJzzMm5Tr25G.htm)|Juvenile Flight|Vol juvénile|libre|
|[ancestry-09-pr7e8xzrl8OLp6U9.htm](feats/ancestry-09-pr7e8xzrl8OLp6U9.htm)|Coating Of Slime|Couche gluante|libre|
|[ancestry-09-pt4oMtjAVGFQHtVw.htm](feats/ancestry-09-pt4oMtjAVGFQHtVw.htm)|Catrina's Presence|Présence de Catrina|libre|
|[ancestry-09-PxWdf4HAhZ8fUB3R.htm](feats/ancestry-09-PxWdf4HAhZ8fUB3R.htm)|Duskwalker Magic|Magie des crépusculaires|libre|
|[ancestry-09-Q1lGguNI4SqPwgVn.htm](feats/ancestry-09-Q1lGguNI4SqPwgVn.htm)|Dragon Prince|Prince dragon|libre|
|[ancestry-09-Q8fdMjZ2Wv3FawOI.htm](feats/ancestry-09-Q8fdMjZ2Wv3FawOI.htm)|Water Strider|Fouleur d'eau|libre|
|[ancestry-09-Q8XZYvatnYo4VlAQ.htm](feats/ancestry-09-Q8XZYvatnYo4VlAQ.htm)|Agathion Magic|Magie des agathions|libre|
|[ancestry-09-qAdlJvAHkWBisDJ0.htm](feats/ancestry-09-qAdlJvAHkWBisDJ0.htm)|Ceremony of Aeon's Shield|Cérémonie du bouclier de l'aéon|libre|
|[ancestry-09-Qe9IUDVo7gsowMHq.htm](feats/ancestry-09-Qe9IUDVo7gsowMHq.htm)|Fey Magic|Magie des fées|libre|
|[ancestry-09-QNgnwkKZmJR5jT7K.htm](feats/ancestry-09-QNgnwkKZmJR5jT7K.htm)|Laughing Gnoll|Gnoll ricanant|libre|
|[ancestry-09-QpvmQTvYaiT6iDJR.htm](feats/ancestry-09-QpvmQTvYaiT6iDJR.htm)|Guarded Thoughts|Pensées protégées|libre|
|[ancestry-09-qr1E37Tla555tvIO.htm](feats/ancestry-09-qr1E37Tla555tvIO.htm)|Fade Away|Disparition|libre|
|[ancestry-09-qS2VcFLez4PLOpIS.htm](feats/ancestry-09-qS2VcFLez4PLOpIS.htm)|Tree Climber (Elf)|Grimpeur aux arbres (Elfe)|libre|
|[ancestry-09-QUaSGUmRnlMbzw1P.htm](feats/ancestry-09-QUaSGUmRnlMbzw1P.htm)|Wind God's Fan|Éventail du dieu du vent|libre|
|[ancestry-09-RdnzuBhkEv7TKsNi.htm](feats/ancestry-09-RdnzuBhkEv7TKsNi.htm)|Asura Magic|Magie asura|libre|
|[ancestry-09-RFXzsfEgz7WbDCQO.htm](feats/ancestry-09-RFXzsfEgz7WbDCQO.htm)|Unhampered Passage|Passage libre|libre|
|[ancestry-09-RN5aZCzEnmlYmBf5.htm](feats/ancestry-09-RN5aZCzEnmlYmBf5.htm)|Crystalline Cloud|Nuage cristallin|libre|
|[ancestry-09-RTxPL1reRcJhhYeG.htm](feats/ancestry-09-RTxPL1reRcJhhYeG.htm)|Analyze Information|Analyse d'information|libre|
|[ancestry-09-rU9Aw05FFLVq0MTV.htm](feats/ancestry-09-rU9Aw05FFLVq0MTV.htm)|Close Quarters|Exiguïté|libre|
|[ancestry-09-rw4qq4qmCbO0hRfH.htm](feats/ancestry-09-rw4qq4qmCbO0hRfH.htm)|Peri Magic|Magie des péris|libre|
|[ancestry-09-RxA1PdgGbijkieJD.htm](feats/ancestry-09-RxA1PdgGbijkieJD.htm)|Defensive Instincts|Instinct défensif|libre|
|[ancestry-09-rzBC5bHAWWpjHMEw.htm](feats/ancestry-09-rzBC5bHAWWpjHMEw.htm)|Ceremony of Fortification|Cérémonie de fortification|libre|
|[ancestry-09-S7z1LbnSRlBep8rO.htm](feats/ancestry-09-S7z1LbnSRlBep8rO.htm)|Spirit Strikes|Frappes spirituelles|libre|
|[ancestry-09-Sav50NxWdLnbaDWQ.htm](feats/ancestry-09-Sav50NxWdLnbaDWQ.htm)|Ferocious Gust|Bourrasque féroce|libre|
|[ancestry-09-sbAb2a6BzZpcYv8y.htm](feats/ancestry-09-sbAb2a6BzZpcYv8y.htm)|Mother's Mindfulness|Instinct maternel conscient|libre|
|[ancestry-09-sGdREpnSJDzEacub.htm](feats/ancestry-09-sGdREpnSJDzEacub.htm)|Group Aid|Aide de groupe|libre|
|[ancestry-09-SiedJ6hnDLEGeeBj.htm](feats/ancestry-09-SiedJ6hnDLEGeeBj.htm)|Cling|Cramponné|libre|
|[ancestry-09-SjwISllgvlKEcjSv.htm](feats/ancestry-09-SjwISllgvlKEcjSv.htm)|Two Truths|Deux vérités|libre|
|[ancestry-09-sTaeqBJCH1HQy96W.htm](feats/ancestry-09-sTaeqBJCH1HQy96W.htm)|Invoke The Elements (Snow May)|Invoquer les éléments (vierge de neige)|libre|
|[ancestry-09-SuEmijj909yxmYOO.htm](feats/ancestry-09-SuEmijj909yxmYOO.htm)|Rat Form|Forme de rat|libre|
|[ancestry-09-swsMURQBMXZpjWl8.htm](feats/ancestry-09-swsMURQBMXZpjWl8.htm)|Cunning Climber|Escalade ingénieuse|libre|
|[ancestry-09-SxRmlDYhYEkq10Ak.htm](feats/ancestry-09-SxRmlDYhYEkq10Ak.htm)|Serpentcoil Slam|Anneaux de serpent abattant|libre|
|[ancestry-09-T4OSMMEvbymMzlIJ.htm](feats/ancestry-09-T4OSMMEvbymMzlIJ.htm)|Rejuvenation Token|Jeton de récupération|libre|
|[ancestry-09-T8cBEhuHWkh3MqgO.htm](feats/ancestry-09-T8cBEhuHWkh3MqgO.htm)|Sense For Trouble|Perception des problèmes|libre|
|[ancestry-09-tEzwhF3uFIi825xj.htm](feats/ancestry-09-tEzwhF3uFIi825xj.htm)|Scaling Poppet|Poupée escaladeuse|libre|
|[ancestry-09-Thhcli0PR7HBBcPX.htm](feats/ancestry-09-Thhcli0PR7HBBcPX.htm)|Aboleth Transmutation|Transmutation de l'aboleth|libre|
|[ancestry-09-tkY5jXELipWjC8k2.htm](feats/ancestry-09-tkY5jXELipWjC8k2.htm)|Viper Strike|Frappe de la vipère|libre|
|[ancestry-09-Tl8yckXeTCHnwrlM.htm](feats/ancestry-09-Tl8yckXeTCHnwrlM.htm)|Soaring Flight|Voler haut|libre|
|[ancestry-09-TPX0fm9wxndpIqpk.htm](feats/ancestry-09-TPX0fm9wxndpIqpk.htm)|Morrigna's Spider Affinity|Affinité arachnide des Morrignas|libre|
|[ancestry-09-TRC4DgVq07cZO65B.htm](feats/ancestry-09-TRC4DgVq07cZO65B.htm)|Thorned Seedpod|Cosse épineuse|libre|
|[ancestry-09-TwAps3ewk7KFHKDv.htm](feats/ancestry-09-TwAps3ewk7KFHKDv.htm)|Sculpt Shadows|Ombres sculptées|libre|
|[ancestry-09-Twhkz2FfzaZezVnG.htm](feats/ancestry-09-Twhkz2FfzaZezVnG.htm)|Fortuitous Shift|Phasage fortuit|libre|
|[ancestry-09-tyae2vpOiAMxXvQH.htm](feats/ancestry-09-tyae2vpOiAMxXvQH.htm)|Inner Breath|Souffle intérieur|libre|
|[ancestry-09-u94fcPT5Oukqzql5.htm](feats/ancestry-09-u94fcPT5Oukqzql5.htm)|Azarketi Purification|Purification azarketie|libre|
|[ancestry-09-UdwXT24zrLzg2ZIV.htm](feats/ancestry-09-UdwXT24zrLzg2ZIV.htm)|Strong Swimmer|Nageur puissant|libre|
|[ancestry-09-uEaXm7eCfeQgtD38.htm](feats/ancestry-09-uEaXm7eCfeQgtD38.htm)|Rivethun Spiritual Attunement|Harmonisation spirituelle du Rivethun|libre|
|[ancestry-09-UKYO5kiOnCY1hgCD.htm](feats/ancestry-09-UKYO5kiOnCY1hgCD.htm)|Daemon Magic|Magie des daémons|libre|
|[ancestry-09-ulQzdBOnZH9LQu8M.htm](feats/ancestry-09-ulQzdBOnZH9LQu8M.htm)|Scalding Spit|Crachat brûlant|libre|
|[ancestry-09-uOFzs058hy144rzm.htm](feats/ancestry-09-uOFzs058hy144rzm.htm)|Captivating Curiosity|Curiosité captivante|libre|
|[ancestry-09-UojXcKf98oAFJUE0.htm](feats/ancestry-09-UojXcKf98oAFJUE0.htm)|Skeletal Resistance|Résistance du squelette|libre|
|[ancestry-09-ux6kbsqRMsu9VHtn.htm](feats/ancestry-09-ux6kbsqRMsu9VHtn.htm)|Quill Spray|Projection de piquant|libre|
|[ancestry-09-VazbV1s93eZqUZIu.htm](feats/ancestry-09-VazbV1s93eZqUZIu.htm)|Improvisational Warrior|Combattant improvisé|libre|
|[ancestry-09-vB7xdAJiZ1gWW2Yj.htm](feats/ancestry-09-vB7xdAJiZ1gWW2Yj.htm)|Jalmeri Rakshasa Magic|Magie des rakshasa de Jalmeray|libre|
|[ancestry-09-velPTcpjLXPnaYrm.htm](feats/ancestry-09-velPTcpjLXPnaYrm.htm)|Solar Rejuvenation|Rajeunissement solaire|libre|
|[ancestry-09-vfuHVSuExvtyajkW.htm](feats/ancestry-09-vfuHVSuExvtyajkW.htm)|Expert Longevity|Longévité experte|officielle|
|[ancestry-09-VphmK5JRR35SyEhV.htm](feats/ancestry-09-VphmK5JRR35SyEhV.htm)|Pervasive Superstition|Superstition omniprésente|libre|
|[ancestry-09-vtCrMziYxNyj8kP7.htm](feats/ancestry-09-vtCrMziYxNyj8kP7.htm)|Celestial Wings|Ailes célestes|libre|
|[ancestry-09-VtFuhfIIw8OoLF6v.htm](feats/ancestry-09-VtFuhfIIw8OoLF6v.htm)|Alluring Performance|Représentation séduisante|libre|
|[ancestry-09-vyb3BYDiIl2MiZp4.htm](feats/ancestry-09-vyb3BYDiIl2MiZp4.htm)|Absorb Toxin|Absorber la toxine|libre|
|[ancestry-09-wGzKq2cx1b2Ycl4u.htm](feats/ancestry-09-wGzKq2cx1b2Ycl4u.htm)|Kneel for No God|Ne s'agenouiller pour aucun dieu|libre|
|[ancestry-09-WldISqAE5Rw3Ewzn.htm](feats/ancestry-09-WldISqAE5Rw3Ewzn.htm)|Demolitionist|Démolisseur|libre|
|[ancestry-09-WQa6PxkOgyvRpaaM.htm](feats/ancestry-09-WQa6PxkOgyvRpaaM.htm)|Predator's Growl|Grognement du prédateur|libre|
|[ancestry-09-WSxHCapuTn8uRdLI.htm](feats/ancestry-09-WSxHCapuTn8uRdLI.htm)|Dragon Grip|Emprise du dragon|libre|
|[ancestry-09-WUVNZoIZvr9XFv2x.htm](feats/ancestry-09-WUVNZoIZvr9XFv2x.htm)|First World Adept|Adepte du Premier monde|officielle|
|[ancestry-09-xDCtfNtbnaG166cy.htm](feats/ancestry-09-xDCtfNtbnaG166cy.htm)|Janni Magic|Magie des janns|libre|
|[ancestry-09-xfbPSP9tl1N95xDF.htm](feats/ancestry-09-xfbPSP9tl1N95xDF.htm)|Arcane Slam|Plaquage arcanique|libre|
|[ancestry-09-xM8IWff5yfbQGk4s.htm](feats/ancestry-09-xM8IWff5yfbQGk4s.htm)|Briar Battler|Combattant des ronciers|libre|
|[ancestry-09-Xwk41o4fERfM07NR.htm](feats/ancestry-09-Xwk41o4fERfM07NR.htm)|Rokoan Arts|Arts rokoans|libre|
|[ancestry-09-YDHr12qVA3XRjkLP.htm](feats/ancestry-09-YDHr12qVA3XRjkLP.htm)|Bloodletting Fangs|Saignée de Crocs|libre|
|[ancestry-09-YfjuRHzqLFhLLgCc.htm](feats/ancestry-09-YfjuRHzqLFhLLgCc.htm)|Piercing Quills|Piquants perforants|libre|
|[ancestry-09-YgytD4HGjWNFwiev.htm](feats/ancestry-09-YgytD4HGjWNFwiev.htm)|Arcane Propulsion|Propulsion arcanique|libre|
|[ancestry-09-YSEqqNx3McbS7k4n.htm](feats/ancestry-09-YSEqqNx3McbS7k4n.htm)|Embodied Dragoon Subjectivity|Subjectivité incarnée du Dragoon|libre|
|[ancestry-09-yYIyAbwpdkySiCMU.htm](feats/ancestry-09-yYIyAbwpdkySiCMU.htm)|Uncanny Cheeks|Joues remarquables|libre|
|[ancestry-09-Z2EiYMtGoNnwW6Tk.htm](feats/ancestry-09-Z2EiYMtGoNnwW6Tk.htm)|Cooperative Soul|Âme coopérative|officielle|
|[ancestry-09-z54Jl3KRZpA2UaZV.htm](feats/ancestry-09-z54Jl3KRZpA2UaZV.htm)|Tetraelemental Assault|Assaut tétraélémentaire|libre|
|[ancestry-09-ZcKW5n7F0oAqRw5o.htm](feats/ancestry-09-ZcKW5n7F0oAqRw5o.htm)|Death's Drums|Tambours de la mort|libre|
|[ancestry-09-ZGnmky2B3v1pbPDA.htm](feats/ancestry-09-ZGnmky2B3v1pbPDA.htm)|Strand Strider|Arpenteur de toiles|libre|
|[ancestry-09-ZGT7NLpcvREkJsMd.htm](feats/ancestry-09-ZGT7NLpcvREkJsMd.htm)|Repair Module|Module de réparation|libre|
|[ancestry-09-zL6zUrt44tZYyuh4.htm](feats/ancestry-09-zL6zUrt44tZYyuh4.htm)|Between The Scales|Entre les écailles|libre|
|[ancestry-09-zPJ2NyMv97AfkN3P.htm](feats/ancestry-09-zPJ2NyMv97AfkN3P.htm)|Occult Resistance|Résistance occulte|libre|
|[ancestry-09-ZPJbjH5XCp39TVu7.htm](feats/ancestry-09-ZPJbjH5XCp39TVu7.htm)|Ragdya's Dance|Danse de Ragdya|libre|
|[ancestry-09-ZPK2Un5ChzeNc9Dx.htm](feats/ancestry-09-ZPK2Un5ChzeNc9Dx.htm)|Cautious Curiosity|Curiosité précautionneuse|libre|
|[ancestry-09-zpQEOqMDoujexNfA.htm](feats/ancestry-09-zpQEOqMDoujexNfA.htm)|Replenishing Hydration|Hydratation complète|libre|
|[ancestry-09-zsubK6PaY58fOYCb.htm](feats/ancestry-09-zsubK6PaY58fOYCb.htm)|Lucky Keepsake|Grigri porte-bonheur|libre|
|[ancestry-09-zsWIZQeVhIihNw6M.htm](feats/ancestry-09-zsWIZQeVhIihNw6M.htm)|Charred Remains|Restes calcinés|libre|
|[ancestry-09-ZvrzK8NM390k139E.htm](feats/ancestry-09-ZvrzK8NM390k139E.htm)|Drain Emotion|Drainer l'émotion|libre|
|[ancestry-09-zZOycr00XDVTKuXa.htm](feats/ancestry-09-zZOycr00XDVTKuXa.htm)|Undying Ferocity|Férocité immortelle|libre|
|[ancestry-13-05wQPB1Z20DOy4rH.htm](feats/ancestry-13-05wQPB1Z20DOy4rH.htm)|Summon Fiendish Kin|Convocation de fiélon apparenté|libre|
|[ancestry-13-0jJ5FG72lydY3HHR.htm](feats/ancestry-13-0jJ5FG72lydY3HHR.htm)|Hydraulic Maneuvers|Manoeuvres hydrauliques|libre|
|[ancestry-13-14dFcInubWcPnFzR.htm](feats/ancestry-13-14dFcInubWcPnFzR.htm)|Eternal Memories|Souvenirs éternels|libre|
|[ancestry-13-1jZ7f4TJqiFH8Ied.htm](feats/ancestry-13-1jZ7f4TJqiFH8Ied.htm)|Thunder God's Fan|Éventail du dieu du tonnerre|libre|
|[ancestry-13-1lpygRsa487Jto4L.htm](feats/ancestry-13-1lpygRsa487Jto4L.htm)|Revivification Protocol|Protocole de revivification|libre|
|[ancestry-13-1WPKV2sInXtiUaeG.htm](feats/ancestry-13-1WPKV2sInXtiUaeG.htm)|Flame Jump|Saut enflammé|libre|
|[ancestry-13-2jy4uh04yz0ezz6Q.htm](feats/ancestry-13-2jy4uh04yz0ezz6Q.htm)|Reanimating Spark|Étincelle de réanimation|libre|
|[ancestry-13-3X8BkDZEDWp90U4u.htm](feats/ancestry-13-3X8BkDZEDWp90U4u.htm)|Ancestor's Transformation|Transformation ancestrale|libre|
|[ancestry-13-3YByhZJi93ie5F45.htm](feats/ancestry-13-3YByhZJi93ie5F45.htm)|Shaitan Skin|Peau de shaitan|libre|
|[ancestry-13-48z7BPYIZJIgj3x5.htm](feats/ancestry-13-48z7BPYIZJIgj3x5.htm)|We March On|Nous Avançons|libre|
|[ancestry-13-55XNy1TVETEMc0vf.htm](feats/ancestry-13-55XNy1TVETEMc0vf.htm)|Stubborn Persistence|Persévérance obstinée|libre|
|[ancestry-13-569SxRBvTCDiHlbW.htm](feats/ancestry-13-569SxRBvTCDiHlbW.htm)|Summon Air Elemental|Convocation d'élémentaire d'air|libre|
|[ancestry-13-5BEY7VzDVuiAg4PX.htm](feats/ancestry-13-5BEY7VzDVuiAg4PX.htm)|Impossible Gossip|Ragot impossible|libre|
|[ancestry-13-5H2KmhiIGuPxKwBK.htm](feats/ancestry-13-5H2KmhiIGuPxKwBK.htm)|Nagaji Spell expertise|Expertise des sorts des nagajis|libre|
|[ancestry-13-5iHB5ZFJ25XrZHye.htm](feats/ancestry-13-5iHB5ZFJ25XrZHye.htm)|Inspirit Hazard|Danger stimulé|libre|
|[ancestry-13-5q8a36QyYAslgnsk.htm](feats/ancestry-13-5q8a36QyYAslgnsk.htm)|Eldritch Calm|Calme mystique|libre|
|[ancestry-13-607fIUg4JjG0aIRx.htm](feats/ancestry-13-607fIUg4JjG0aIRx.htm)|Goblin Weapon Expertise|Expertise avec les armes gobelines|officielle|
|[ancestry-13-6GGPTYwljGeGL2B3.htm](feats/ancestry-13-6GGPTYwljGeGL2B3.htm)|Grippli Weapon Expertise|Expertise avec les armes des gripplis|libre|
|[ancestry-13-7bVJEZt2vwAAnILV.htm](feats/ancestry-13-7bVJEZt2vwAAnILV.htm)|Suli Amir|Émir Suli|libre|
|[ancestry-13-7Lx8rsEbBJkg6C17.htm](feats/ancestry-13-7Lx8rsEbBJkg6C17.htm)|Gift Of The Moon|Don de la lune|libre|
|[ancestry-13-7NsqI50oBIJ4bFwb.htm](feats/ancestry-13-7NsqI50oBIJ4bFwb.htm)|Monkey Spirits|Esprits simiesques|libre|
|[ancestry-13-8H72RC7QI1i8wjJ1.htm](feats/ancestry-13-8H72RC7QI1i8wjJ1.htm)|Warren Digger|Creuseur de terrier|libre|
|[ancestry-13-8JOLT1UX5BVG4kVY.htm](feats/ancestry-13-8JOLT1UX5BVG4kVY.htm)|Tengu Weapon Expertise|Expertise avec les armes tengu|libre|
|[ancestry-13-8Qn80RunXaChOM5p.htm](feats/ancestry-13-8Qn80RunXaChOM5p.htm)|Shadow's Assault|Assaut de l'ombre|libre|
|[ancestry-13-8VXYwHE5LqAGRGTB.htm](feats/ancestry-13-8VXYwHE5LqAGRGTB.htm)|Kashrishi Revivification|Revitalisation kashrishie|libre|
|[ancestry-13-93vQcuKBESXUKoH5.htm](feats/ancestry-13-93vQcuKBESXUKoH5.htm)|Killing Stone|Pierre tueuse|libre|
|[ancestry-13-99WRahrMC91D6MMe.htm](feats/ancestry-13-99WRahrMC91D6MMe.htm)|Unbreakable-er Goblin|Gobelin encore plus incassable|libre|
|[ancestry-13-9eL7W4rvs4sjhWFT.htm](feats/ancestry-13-9eL7W4rvs4sjhWFT.htm)|Radiant Burst|Explosion radieuse|libre|
|[ancestry-13-ACeqRSWr4CEwLZgO.htm](feats/ancestry-13-ACeqRSWr4CEwLZgO.htm)|Shadow Pact|Pacte d'ombre|libre|
|[ancestry-13-ADDOwO0QWOXAV85x.htm](feats/ancestry-13-ADDOwO0QWOXAV85x.htm)|Telluric Power|Puissance tellurique|libre|
|[ancestry-13-aEhUX2c1xj57CMw5.htm](feats/ancestry-13-aEhUX2c1xj57CMw5.htm)|Vishkanya Weapon Expertise|Expertise avec les armes des vishkanyas|libre|
|[ancestry-13-AP9PXXaP4a2sMdt2.htm](feats/ancestry-13-AP9PXXaP4a2sMdt2.htm)|Enforced Order|Ordre contraint|libre|
|[ancestry-13-aQHB5LhxLaFRkzSt.htm](feats/ancestry-13-aQHB5LhxLaFRkzSt.htm)|Finest Trick|Astuce de qualité|libre|
|[ancestry-13-aqTlEPtd5gzs1Lxg.htm](feats/ancestry-13-aqTlEPtd5gzs1Lxg.htm)|Unconventional Expertise|Expertise non conventionnelle|officielle|
|[ancestry-13-arlZTqfppOAXBhdw.htm](feats/ancestry-13-arlZTqfppOAXBhdw.htm)|Irriseni Ice-Witch|Sorcier de glace irriseni|libre|
|[ancestry-13-aRqXpCBWic6a3DQH.htm](feats/ancestry-13-aRqXpCBWic6a3DQH.htm)|Venom Purge|Purge venin|libre|
|[ancestry-13-AxqnIMh5WbSah5OS.htm](feats/ancestry-13-AxqnIMh5WbSah5OS.htm)|Mischievous Tail|Queue espiègle|libre|
|[ancestry-13-b5j3boj1iEcXSB9f.htm](feats/ancestry-13-b5j3boj1iEcXSB9f.htm)|Reimagine|Réimaginer|libre|
|[ancestry-13-bi77bT9uyAmJXVed.htm](feats/ancestry-13-bi77bT9uyAmJXVed.htm)|Truespeech|Langage universel|libre|
|[ancestry-13-bj0y7JnIboNfCuC8.htm](feats/ancestry-13-bj0y7JnIboNfCuC8.htm)|Gnoll Weapon Expertise|Expertise avec les armes des gnolls|libre|
|[ancestry-13-BTQj2N5erpJDWNFA.htm](feats/ancestry-13-BTQj2N5erpJDWNFA.htm)|Astral Blink|Clignotement astral|libre|
|[ancestry-13-c5xHL6CDFwBqXx2a.htm](feats/ancestry-13-c5xHL6CDFwBqXx2a.htm)|Spiteful Rake|Labourage malveillant|libre|
|[ancestry-13-CAfrSLaDM0OBaNtp.htm](feats/ancestry-13-CAfrSLaDM0OBaNtp.htm)|See the Unseen|Voir l'invisible|libre|
|[ancestry-13-Cf0CDTZvGaYDAXUN.htm](feats/ancestry-13-Cf0CDTZvGaYDAXUN.htm)|Calaca's Showstopper|Remarquable de calaca|libre|
|[ancestry-13-cIJfKlbfKezdDbwK.htm](feats/ancestry-13-cIJfKlbfKezdDbwK.htm)|Summon Fire Elemental|Convocation d'élémentaire de feu|libre|
|[ancestry-13-cpsyKarYRHiOF0Nd.htm](feats/ancestry-13-cpsyKarYRHiOF0Nd.htm)|None Shall Know|Personne ne doit savoir|libre|
|[ancestry-13-d3J74jmGOrPBWUm9.htm](feats/ancestry-13-d3J74jmGOrPBWUm9.htm)|Disruptive Stare|Regard perturbateur|libre|
|[ancestry-13-dgpoTae18H4zc9fH.htm](feats/ancestry-13-dgpoTae18H4zc9fH.htm)|Catfolk Weapon Expertise|Expertise avec les armes des hommes-félins|libre|
|[ancestry-13-DIjpbE2dh5MRGiYO.htm](feats/ancestry-13-DIjpbE2dh5MRGiYO.htm)|Impose Order|Ordre imposé|libre|
|[ancestry-13-DNZlWe2V28KoajoN.htm](feats/ancestry-13-DNZlWe2V28KoajoN.htm)|Eclectic Sword Mastery|Maîtrise éclectique de l'épée|libre|
|[ancestry-13-dor2F3g8gL2KVwX6.htm](feats/ancestry-13-dor2F3g8gL2KVwX6.htm)|Skirt The Light|Longer la lumière|libre|
|[ancestry-13-E0EARGd5iryHJGJD.htm](feats/ancestry-13-E0EARGd5iryHJGJD.htm)|Unbound Freedom|Liberté sans limite|libre|
|[ancestry-13-ECcFHAeAmh5F3mxg.htm](feats/ancestry-13-ECcFHAeAmh5F3mxg.htm)|Azarketi Weapon Expertise|Expertise avec les armes azarketies|libre|
|[ancestry-13-eGfYZoWC6cDa2XWd.htm](feats/ancestry-13-eGfYZoWC6cDa2XWd.htm)|Jinx Glutton|Glouton maudit|libre|
|[ancestry-13-EiiCCJqWnN5RYMV4.htm](feats/ancestry-13-EiiCCJqWnN5RYMV4.htm)|Universal Longevity|Longévité universelle|officielle|
|[ancestry-13-EIyazsXwM7Zc2XGO.htm](feats/ancestry-13-EIyazsXwM7Zc2XGO.htm)|Arcane Sight|Vision arcanique|libre|
|[ancestry-13-ewwhgHZbxHcpMdLn.htm](feats/ancestry-13-ewwhgHZbxHcpMdLn.htm)|Glamour|Enchantement|libre|
|[ancestry-13-FI4MnH0KQfIKJRNT.htm](feats/ancestry-13-FI4MnH0KQfIKJRNT.htm)|Arcane Locomotion|Déplacement arcanique|libre|
|[ancestry-13-Fn8dEIcUZVxuWJgN.htm](feats/ancestry-13-Fn8dEIcUZVxuWJgN.htm)|Summon Water Elemental|Convocation d'élémentaire d'eau|libre|
|[ancestry-13-fnMT0AsZXFW9Ppyp.htm](feats/ancestry-13-fnMT0AsZXFW9Ppyp.htm)|Cloak Of Poison|Cape de poison|libre|
|[ancestry-13-FqZKSSBU7M4zhsXM.htm](feats/ancestry-13-FqZKSSBU7M4zhsXM.htm)|Resist Ruin|Résister à la ruine|libre|
|[ancestry-13-fX5FybM93HIQRRd1.htm](feats/ancestry-13-fX5FybM93HIQRRd1.htm)|Black Cat Curse|Malédiction du chat noir|libre|
|[ancestry-13-g6M5mapOXVf0g9BG.htm](feats/ancestry-13-g6M5mapOXVf0g9BG.htm)|Orc Weapon Expertise|Expertise avec les armes des orques|officielle|
|[ancestry-13-GELhS1k0vHEi1PK3.htm](feats/ancestry-13-GELhS1k0vHEi1PK3.htm)|Incredible Ferocity|Férocité extraordinaire|officielle|
|[ancestry-13-GfpHBiOX02PQz0Pm.htm](feats/ancestry-13-GfpHBiOX02PQz0Pm.htm)|Spew Tentacles|Cracher des tentacules|libre|
|[ancestry-13-Gh0rJNsdxBacK9b8.htm](feats/ancestry-13-Gh0rJNsdxBacK9b8.htm)|Conrasu Weapon Expertise|Expertise avec les armes des conrasus|libre|
|[ancestry-13-GndRoEYD3uWjJji3.htm](feats/ancestry-13-GndRoEYD3uWjJji3.htm)|Purge Sins|Purger les péchés|libre|
|[ancestry-13-gsW6mCMBWquLM3bj.htm](feats/ancestry-13-gsW6mCMBWquLM3bj.htm)|Halfling Weapon Expertise|Expertise avec les armes halfelines|officielle|
|[ancestry-13-gwzjvKQQ6zmgVVmS.htm](feats/ancestry-13-gwzjvKQQ6zmgVVmS.htm)|Daywalker|Marcheur diurne|libre|
|[ancestry-13-H965m1koFvY4FQkF.htm](feats/ancestry-13-H965m1koFvY4FQkF.htm)|Form Of The Bat|Forme de la chauve-souris|libre|
|[ancestry-13-hOD9de1ftfYRSEKn.htm](feats/ancestry-13-hOD9de1ftfYRSEKn.htm)|Airy Step|Pas aérien|libre|
|[ancestry-13-HpAUTAzEAK7mGsJ8.htm](feats/ancestry-13-HpAUTAzEAK7mGsJ8.htm)|Scrutinizing Gaze|Regard scrutateur|libre|
|[ancestry-13-hSHteYr1g1gKHcwM.htm](feats/ancestry-13-hSHteYr1g1gKHcwM.htm)|Fey Skin|Peau de fée|libre|
|[ancestry-13-Ht6b8H9DpA9lWzAg.htm](feats/ancestry-13-Ht6b8H9DpA9lWzAg.htm)|Advanced General Training|Entraînement général avancé|libre|
|[ancestry-13-HWLD3zGFBIjCDATo.htm](feats/ancestry-13-HWLD3zGFBIjCDATo.htm)|Planar Sidestep|Pas de côté planaire|libre|
|[ancestry-13-hXfyIWMtvB2kAEvA.htm](feats/ancestry-13-hXfyIWMtvB2kAEvA.htm)|Aquatic Camouflage|Camouflage aquatique|libre|
|[ancestry-13-I3Md8XYhuMVSJqKI.htm](feats/ancestry-13-I3Md8XYhuMVSJqKI.htm)|Continuous Assault|Assaut continuel|libre|
|[ancestry-13-i3wSkeU7CSyHEi4Y.htm](feats/ancestry-13-i3wSkeU7CSyHEi4Y.htm)|Genie Weapon Expertise|Expertise avec les armes des génies|libre|
|[ancestry-13-I9cJYC7anz7HmpcJ.htm](feats/ancestry-13-I9cJYC7anz7HmpcJ.htm)|Elite Dracomancer|Dracomancien d'élite|libre|
|[ancestry-13-Ih85PWZSVTwU0xkI.htm](feats/ancestry-13-Ih85PWZSVTwU0xkI.htm)|Lifeblood's Call|Appel du sang de la vie|libre|
|[ancestry-13-iTsLr3zEaGZ45zez.htm](feats/ancestry-13-iTsLr3zEaGZ45zez.htm)|Incredible Luck (Halfling)|Chance incroyable (Halfelin)|libre|
|[ancestry-13-j0fgquODHTyyekyO.htm](feats/ancestry-13-j0fgquODHTyyekyO.htm)|Kobold Weapon Expertise|Expertise avec les armes des kobolds|libre|
|[ancestry-13-J2CPfHKPvu6RGfY6.htm](feats/ancestry-13-J2CPfHKPvu6RGfY6.htm)|Fully Flighted|Volant pleinement|libre|
|[ancestry-13-j49fEU2TWJUaxD30.htm](feats/ancestry-13-j49fEU2TWJUaxD30.htm)|Gnome Weapon Expertise|Expertise avec les armes gnomes|officielle|
|[ancestry-13-j8tukQK5FT1Vfx2G.htm](feats/ancestry-13-j8tukQK5FT1Vfx2G.htm)|Crafter's Instinct|Instinct du fabricant|libre|
|[ancestry-13-JAl4t2KINE8nI8KY.htm](feats/ancestry-13-JAl4t2KINE8nI8KY.htm)|Ceremony of Growth|Cérémonie de croissance|libre|
|[ancestry-13-jatfexNkXaTs9s5Q.htm](feats/ancestry-13-jatfexNkXaTs9s5Q.htm)|Instinctive Obfuscation|Obscurcissement instinctif|libre|
|[ancestry-13-JlkkYedPyWLShuka.htm](feats/ancestry-13-JlkkYedPyWLShuka.htm)|Kitsune Spell Expertise|Expertise des sorts des kitsunes|libre|
|[ancestry-13-JmVuumcmqSrAmxdm.htm](feats/ancestry-13-JmVuumcmqSrAmxdm.htm)|Ghoran Weapon Expertise|Expertise avec les armes des ghorans|libre|
|[ancestry-13-JQxFvMHu0ffo56RT.htm](feats/ancestry-13-JQxFvMHu0ffo56RT.htm)|Shinstabber|Se glisser dans les pattes|libre|
|[ancestry-13-k2L9p4cc8RrHufut.htm](feats/ancestry-13-k2L9p4cc8RrHufut.htm)|Look But Don't Touch|Regarde mais ne touche pas|libre|
|[ancestry-13-kBnsLuh3eBLjwGpN.htm](feats/ancestry-13-kBnsLuh3eBLjwGpN.htm)|Hatchling Flight|Vol du nouveau-né|libre|
|[ancestry-13-KboQgBhYXI2WdCt8.htm](feats/ancestry-13-KboQgBhYXI2WdCt8.htm)|Mask Of Fear|Masque de terreur|libre|
|[ancestry-13-KQ5xOAqZ1KPN3FgA.htm](feats/ancestry-13-KQ5xOAqZ1KPN3FgA.htm)|Stronger Debilitating Venom|Venin débilitant plus puissant|libre|
|[ancestry-13-KRfZcToCc5nvcQRa.htm](feats/ancestry-13-KRfZcToCc5nvcQRa.htm)|Harbinger's Caw|Croassement de mauvais augure|libre|
|[ancestry-13-l11OWMFUixCcRYGm.htm](feats/ancestry-13-l11OWMFUixCcRYGm.htm)|Aquatic Adaptation|Adaptation aquatique|libre|
|[ancestry-13-L231BR4815B6hwKT.htm](feats/ancestry-13-L231BR4815B6hwKT.htm)|Summon Celestial Kin|Convocation de céleste apparenté|libre|
|[ancestry-13-l78KwojlT0AWvS4l.htm](feats/ancestry-13-l78KwojlT0AWvS4l.htm)|Toppling Dance|Danse renversante|libre|
|[ancestry-13-lIzsj8XlcL0tqQcm.htm](feats/ancestry-13-lIzsj8XlcL0tqQcm.htm)|Cannibalize Magic|Cannibaliser la magie|libre|
|[ancestry-13-lrTRELs8uDpVpsk0.htm](feats/ancestry-13-lrTRELs8uDpVpsk0.htm)|Summon Earth Elemental|Convocation d'élémentaire de terre|libre|
|[ancestry-13-LTcpBbnngfuYTdB0.htm](feats/ancestry-13-LTcpBbnngfuYTdB0.htm)|Avenge Ally|Venger un allié|libre|
|[ancestry-13-LxAEF9VPwUVttA0F.htm](feats/ancestry-13-LxAEF9VPwUVttA0F.htm)|Delver|Plongeur|libre|
|[ancestry-13-M8pDuRo6tO0Kok9z.htm](feats/ancestry-13-M8pDuRo6tO0Kok9z.htm)|Secret Eyes|Yeux secrets|libre|
|[ancestry-13-MII0ybm76DkDxsId.htm](feats/ancestry-13-MII0ybm76DkDxsId.htm)|Cobble Dancer|Danseur des pavés|libre|
|[ancestry-13-MR4X38qgBj5tmkMw.htm](feats/ancestry-13-MR4X38qgBj5tmkMw.htm)|Bounce Back|Rebondir|libre|
|[ancestry-13-n82gTpAENgr6XjCM.htm](feats/ancestry-13-n82gTpAENgr6XjCM.htm)|Translucent Skin|Peau translucide|libre|
|[ancestry-13-NaY9HXeQ0yhoIorO.htm](feats/ancestry-13-NaY9HXeQ0yhoIorO.htm)|Skittering Sneak|Glissement sournois|libre|
|[ancestry-13-ne7nVluvvVXMvuB1.htm](feats/ancestry-13-ne7nVluvvVXMvuB1.htm)|Wandering Heart|Coeur vagabond|libre|
|[ancestry-13-nePEcAp7lTL35uyx.htm](feats/ancestry-13-nePEcAp7lTL35uyx.htm)|Caterwaul|Feuler|libre|
|[ancestry-13-NN9sKez7Z9R8q1i9.htm](feats/ancestry-13-NN9sKez7Z9R8q1i9.htm)|Vanara Battle Clarity|Clarté de combat vanara|libre|
|[ancestry-13-O65q744snXtC5Uc4.htm](feats/ancestry-13-O65q744snXtC5Uc4.htm)|Consistent Surge|Poussée consistante|libre|
|[ancestry-13-oaqkhZ6c0Dbk78wi.htm](feats/ancestry-13-oaqkhZ6c0Dbk78wi.htm)|Envenomed Edge|Avantage empoisonné|libre|
|[ancestry-13-ODy9tq7NPlD5fxzZ.htm](feats/ancestry-13-ODy9tq7NPlD5fxzZ.htm)|Pit of Snakes|Fosse de serpents|libre|
|[ancestry-13-okJjCvQ6hcCk8FOC.htm](feats/ancestry-13-okJjCvQ6hcCk8FOC.htm)|Malleable Form|Forme malléable|libre|
|[ancestry-13-Ot1jI91ccnB2ayfk.htm](feats/ancestry-13-Ot1jI91ccnB2ayfk.htm)|Celestial Strikes|Frappes célestes|libre|
|[ancestry-13-OwJRuy4EW8vW09AI.htm](feats/ancestry-13-OwJRuy4EW8vW09AI.htm)|Metal-veined Strikes|Frappes de veines de métal|libre|
|[ancestry-13-pedHIDAVLFzzjGO1.htm](feats/ancestry-13-pedHIDAVLFzzjGO1.htm)|Spell Devourer|Dévoreur de sort|libre|
|[ancestry-13-PfHvTR0L9ShjhWiP.htm](feats/ancestry-13-PfHvTR0L9ShjhWiP.htm)|Ceaseless Shadows|Ombres incessantes|officielle|
|[ancestry-13-pivX2SxPQjKDyvHU.htm](feats/ancestry-13-pivX2SxPQjKDyvHU.htm)|Iruxi Spirit Strike|Frappe spirituelle iruxie|libre|
|[ancestry-13-plhQDES7yb6xDAXL.htm](feats/ancestry-13-plhQDES7yb6xDAXL.htm)|Shory Aerialist|Aérialiste rivain|libre|
|[ancestry-13-PvODcTsmFtXffaZn.htm](feats/ancestry-13-PvODcTsmFtXffaZn.htm)|Idol Threat|Menace contre l'idole|libre|
|[ancestry-13-QHPtlFvTpgx1GE7S.htm](feats/ancestry-13-QHPtlFvTpgx1GE7S.htm)|Formation Master|Maître du combat en formation|libre|
|[ancestry-13-QjHZN1uAxl2hSbej.htm](feats/ancestry-13-QjHZN1uAxl2hSbej.htm)|Vicious Snares|Pièges artisanaux vicieux|libre|
|[ancestry-13-rCajlx2KjGxzabAJ.htm](feats/ancestry-13-rCajlx2KjGxzabAJ.htm)|Fiend's Door|Porte du fiélon|libre|
|[ancestry-13-rHBmZi95n0pcQqxs.htm](feats/ancestry-13-rHBmZi95n0pcQqxs.htm)|Skeleton Commander|Commandant squelette|libre|
|[ancestry-13-s0OqtQOeYOGkBMYG.htm](feats/ancestry-13-s0OqtQOeYOGkBMYG.htm)|Steadfast Ally|Allié inébranlable|libre|
|[ancestry-13-S1Z5dFAkMKkFSofk.htm](feats/ancestry-13-S1Z5dFAkMKkFSofk.htm)|War Conditioning|Conditionnement à la guerre|libre|
|[ancestry-13-SzkpazSZ0pi1WZsH.htm](feats/ancestry-13-SzkpazSZ0pi1WZsH.htm)|Squirm Free|Se tortiller|libre|
|[ancestry-13-T3cwVrRT0VMZIwpT.htm](feats/ancestry-13-T3cwVrRT0VMZIwpT.htm)|One With Earth|Uni avec la terre|libre|
|[ancestry-13-t6GBBIwX7hvvxYyV.htm](feats/ancestry-13-t6GBBIwX7hvvxYyV.htm)|Hag Magic|Magie des guenaudes|libre|
|[ancestry-13-TGDy2R6Dq8hwZir4.htm](feats/ancestry-13-TGDy2R6Dq8hwZir4.htm)|Redirect Attention|Rediriger l'attention|libre|
|[ancestry-13-tpkih32Ch2wCA9R5.htm](feats/ancestry-13-tpkih32Ch2wCA9R5.htm)|Dwarven Weapon Expertise|Expertise avec les armes naines|officielle|
|[ancestry-13-TtSLIMiNw1hqMui5.htm](feats/ancestry-13-TtSLIMiNw1hqMui5.htm)|Skeletal Transformation|Transformation du squelette|libre|
|[ancestry-13-TYe3dsNlks91YAks.htm](feats/ancestry-13-TYe3dsNlks91YAks.htm)|Explosive Expert|Explosion experte|libre|
|[ancestry-13-u0DA0gkrZxXb0Hle.htm](feats/ancestry-13-u0DA0gkrZxXb0Hle.htm)|Very, Very Sneaky|Très très furtif|officielle|
|[ancestry-13-uclbKFsrqCW6tQmB.htm](feats/ancestry-13-uclbKFsrqCW6tQmB.htm)|Core Rejuvenation|Rajeunissement du noyau|libre|
|[ancestry-13-uDFgL6uCwJsaTHi3.htm](feats/ancestry-13-uDFgL6uCwJsaTHi3.htm)|Call Of The Green Man|Appel de l'homme vert|libre|
|[ancestry-13-uQpAPkJ4ygzFZcIt.htm](feats/ancestry-13-uQpAPkJ4ygzFZcIt.htm)|Vanth's Weapon Expertise|Expertise avec les armes des vanths|libre|
|[ancestry-13-V6iJ4v15Qc8awVCH.htm](feats/ancestry-13-V6iJ4v15Qc8awVCH.htm)|Aasimar's Mercy|Pitié de l'aasimar|libre|
|[ancestry-13-vWUPSvEBHr5AAPUU.htm](feats/ancestry-13-vWUPSvEBHr5AAPUU.htm)|Violent Vines|Liane violente|libre|
|[ancestry-13-VXAIElMlMnVvz3x5.htm](feats/ancestry-13-VXAIElMlMnVvz3x5.htm)|Webslinger|Lanceur de toile|libre|
|[ancestry-13-wHxXVknciaD4X8Ch.htm](feats/ancestry-13-wHxXVknciaD4X8Ch.htm)|Unrivaled Builder|Constructeur sans rival|libre|
|[ancestry-13-wruf4sMh3P7o8k1P.htm](feats/ancestry-13-wruf4sMh3P7o8k1P.htm)|Elven Weapon Expertise|Expertise avec les armes elfiques|officielle|
|[ancestry-13-wteuGNhOXLvBudRQ.htm](feats/ancestry-13-wteuGNhOXLvBudRQ.htm)|Ferocious Beasts|Bêtes féroces|libre|
|[ancestry-13-WWCZef1PBliiot9Q.htm](feats/ancestry-13-WWCZef1PBliiot9Q.htm)|Enlarged Chassis|Chassis aggrandi|libre|
|[ancestry-13-X1SPiYJqWzqwzPTs.htm](feats/ancestry-13-X1SPiYJqWzqwzPTs.htm)|Primal Rampage|Fléau primordial|libre|
|[ancestry-13-X5NRdQ1W0Y5t6MFi.htm](feats/ancestry-13-X5NRdQ1W0Y5t6MFi.htm)|Dire Form|Forme impressionnante|libre|
|[ancestry-13-xjE123p2rPTsz1eV.htm](feats/ancestry-13-xjE123p2rPTsz1eV.htm)|Fiendish Strikes|Frappes fiélones|libre|
|[ancestry-13-XJfUj2o4HdL5waZL.htm](feats/ancestry-13-XJfUj2o4HdL5waZL.htm)|Can't Fall Here|Tu ne peux tomber ici|libre|
|[ancestry-13-XOudHBESJ192FBwF.htm](feats/ancestry-13-XOudHBESJ192FBwF.htm)|Rehydration|Réhydratation|libre|
|[ancestry-13-XPVqOtHLN6jduHYs.htm](feats/ancestry-13-XPVqOtHLN6jduHYs.htm)|Hobgoblin Weapon Expertise|Expertise avec les armes des hobgobelins|libre|
|[ancestry-13-xTLy2AyVW90TrQk6.htm](feats/ancestry-13-xTLy2AyVW90TrQk6.htm)|Improved Elemental Bulwark|Fortification élémentaire améliorée|libre|
|[ancestry-13-XuL3g2ExgadFtWbb.htm](feats/ancestry-13-XuL3g2ExgadFtWbb.htm)|Bone Investiture|Investiture d'os|libre|
|[ancestry-13-XzTklOArNQle7rQP.htm](feats/ancestry-13-XzTklOArNQle7rQP.htm)|Invisible Trickster|Mystificateur invisible|libre|
|[ancestry-13-Y5ompgSd1JGSHt0C.htm](feats/ancestry-13-Y5ompgSd1JGSHt0C.htm)|Augment Senses|Sens augmentés|libre|
|[ancestry-13-YDfcwp0NVbmpgxej.htm](feats/ancestry-13-YDfcwp0NVbmpgxej.htm)|Vanara Weapoin Expertise|Expertise avec les armes vanara|libre|
|[ancestry-13-YryDlMt3zARg6j7T.htm](feats/ancestry-13-YryDlMt3zARg6j7T.htm)|Arise, Ye Worthy!|Relève toi, Valeureux !|libre|
|[ancestry-13-ytgmam8COVq8B7Do.htm](feats/ancestry-13-ytgmam8COVq8B7Do.htm)|Ancestor's Rage|Rage ancestrale|libre|
|[ancestry-13-Z15QFC45psi0txWg.htm](feats/ancestry-13-Z15QFC45psi0txWg.htm)|Alter Resistance|Altérer la résistance|libre|
|[ancestry-13-ZlPCQ3Qh1cgS9r87.htm](feats/ancestry-13-ZlPCQ3Qh1cgS9r87.htm)|Mist Strider|Fouleur de brume|libre|
|[ancestry-17-2pYCGPCQDHD3o7Jz.htm](feats/ancestry-17-2pYCGPCQDHD3o7Jz.htm)|Fiendish Word|Parole fiélone|libre|
|[ancestry-17-4TndDyCjnF7pc1GC.htm](feats/ancestry-17-4TndDyCjnF7pc1GC.htm)|Ghoran's Wrath|Courroux du ghoran|libre|
|[ancestry-17-5FHLom2tpC0X3nbf.htm](feats/ancestry-17-5FHLom2tpC0X3nbf.htm)|Greater Augmentation|Amélioration supérieure|libre|
|[ancestry-17-5qVCl5MwPcx0sS7T.htm](feats/ancestry-17-5qVCl5MwPcx0sS7T.htm)|Elude Trouble|Éluder les ennuis|libre|
|[ancestry-17-5Unc9AhGAAw1klFN.htm](feats/ancestry-17-5Unc9AhGAAw1klFN.htm)|Animal Swiftness|Rapidité animale|libre|
|[ancestry-17-6NXlg11EeCqjOmSg.htm](feats/ancestry-17-6NXlg11EeCqjOmSg.htm)|Twist Healing|Guérison tordue|libre|
|[ancestry-17-715iDDXFPguSO9Or.htm](feats/ancestry-17-715iDDXFPguSO9Or.htm)|Bend Space|Espace courbé|libre|
|[ancestry-17-7iB1yacjF9fG6Rvn.htm](feats/ancestry-17-7iB1yacjF9fG6Rvn.htm)|Yamaraj's Grandeur|Grandeur des yamarajs|libre|
|[ancestry-17-7YvOqcdp9Z0RALMp.htm](feats/ancestry-17-7YvOqcdp9Z0RALMp.htm)|Shadow Self|Ombre de soi-même|libre|
|[ancestry-17-8l3qDZrfhxUGijjB.htm](feats/ancestry-17-8l3qDZrfhxUGijjB.htm)|Rallying Cry|Cri de ralliement|libre|
|[ancestry-17-8ZWMJYNgyH5zh1yH.htm](feats/ancestry-17-8ZWMJYNgyH5zh1yH.htm)|Eternal Wings (Sylph)|Ailes éternelles (Sylphe)|libre|
|[ancestry-17-9cq4j15EbVyrHKFX.htm](feats/ancestry-17-9cq4j15EbVyrHKFX.htm)|Forge-Blessed Shot|Tir béni par la forge|libre|
|[ancestry-17-9eaUS0jJCpxuNXO5.htm](feats/ancestry-17-9eaUS0jJCpxuNXO5.htm)|Rampaging Ferocity|Férocité dévastatrice|libre|
|[ancestry-17-ABx8keV4c43gEmeN.htm](feats/ancestry-17-ABx8keV4c43gEmeN.htm)|Pierce The Light|Percer la lumière|libre|
|[ancestry-17-ABXUfGoeBgCyFasg.htm](feats/ancestry-17-ABXUfGoeBgCyFasg.htm)|Great Tengu Form|Forme du grand tengu|libre|
|[ancestry-17-bPPcPUZMlJ7m5lYq.htm](feats/ancestry-17-bPPcPUZMlJ7m5lYq.htm)|Tidal Shield|Bouclier de la marée|libre|
|[ancestry-17-BS6UoGLyJ12xVD9P.htm](feats/ancestry-17-BS6UoGLyJ12xVD9P.htm)|Ratfolk Growth|Croissance d'homme-rat|libre|
|[ancestry-17-cayKGa2yK7Gwvk6m.htm](feats/ancestry-17-cayKGa2yK7Gwvk6m.htm)|Olethros's Decree|Décret d'Olethro|libre|
|[ancestry-17-CuyuwFht0rVbGMca.htm](feats/ancestry-17-CuyuwFht0rVbGMca.htm)|Relentless Wings|Ailes sans repos|libre|
|[ancestry-17-dftzUTOrj9dQyN3q.htm](feats/ancestry-17-dftzUTOrj9dQyN3q.htm)|Restitch|Recousu|libre|
|[ancestry-17-Dxac0mgye7EFFVou.htm](feats/ancestry-17-Dxac0mgye7EFFVou.htm)|Radiate Glory|Irradier la gloire|libre|
|[ancestry-17-DYFOlJlMoDuHjrZx.htm](feats/ancestry-17-DYFOlJlMoDuHjrZx.htm)|Channel The Godmind|Canaliser l'esprit divin|libre|
|[ancestry-17-E4MS2wnRQJfyldrT.htm](feats/ancestry-17-E4MS2wnRQJfyldrT.htm)|Bone Swarm|Nuée d'os|libre|
|[ancestry-17-EEnL4zyCDo4HD6Rn.htm](feats/ancestry-17-EEnL4zyCDo4HD6Rn.htm)|Fey Transcendence|Transcendance féerique|libre|
|[ancestry-17-ERCd44zQMYI4rgqM.htm](feats/ancestry-17-ERCd44zQMYI4rgqM.htm)|Animal Swiftness (Land)|Rapidité animale (Sol)|libre|
|[ancestry-17-FBhBnyGX0Uje0tbJ.htm](feats/ancestry-17-FBhBnyGX0Uje0tbJ.htm)|Storm Form|Forme tempêtueuse|libre|
|[ancestry-17-ffjoPXtAoIR2Drwe.htm](feats/ancestry-17-ffjoPXtAoIR2Drwe.htm)|Regrowth|Repousse|libre|
|[ancestry-17-fqw1ELaqavuKLHIj.htm](feats/ancestry-17-fqw1ELaqavuKLHIj.htm)|Reckless Abandon (Goblin)|Dangereux abandon (Gobelin)|libre|
|[ancestry-17-FWfKbAxTct6q3AMp.htm](feats/ancestry-17-FWfKbAxTct6q3AMp.htm)|Vicious Venom|Venin ficieux|libre|
|[ancestry-17-gLOLdDj21bTaLHp7.htm](feats/ancestry-17-gLOLdDj21bTaLHp7.htm)|Stonewall|Mur de pierre|libre|
|[ancestry-17-gXAAJCnjfCDK7YV2.htm](feats/ancestry-17-gXAAJCnjfCDK7YV2.htm)|Cleansing Light|Lumière purifiante|libre|
|[ancestry-17-GYvaR6ZD8ZKdQWrF.htm](feats/ancestry-17-GYvaR6ZD8ZKdQWrF.htm)|Stone Form|Forme de pierre|libre|
|[ancestry-17-h9b5CId7S7gV7j2t.htm](feats/ancestry-17-h9b5CId7S7gV7j2t.htm)|Axial Recall|Rappel d'Axis|libre|
|[ancestry-17-hkU92nqUYBQLQSMt.htm](feats/ancestry-17-hkU92nqUYBQLQSMt.htm)|Blazing Aura|Aura brûlante|libre|
|[ancestry-17-hNr4OrMdCMhQLbtB.htm](feats/ancestry-17-hNr4OrMdCMhQLbtB.htm)|Wyrmling Flight|Vol de dragonnet|libre|
|[ancestry-17-hZUFP1NPwdGMzs1y.htm](feats/ancestry-17-hZUFP1NPwdGMzs1y.htm)|Ceremony of Sun's Gift|Cérémonie du don du soleil|libre|
|[ancestry-17-I7S8Snq8FlrBHmbf.htm](feats/ancestry-17-I7S8Snq8FlrBHmbf.htm)|Ten Lives|Dix vies|libre|
|[ancestry-17-iJrjzLnLJkvQgrbS.htm](feats/ancestry-17-iJrjzLnLJkvQgrbS.htm)|Hurricane Swing|Élan de l'ouragan|libre|
|[ancestry-17-IOlvpmeIwlk0IACr.htm](feats/ancestry-17-IOlvpmeIwlk0IACr.htm)|Bone Rider|Chevaucheur d'os|libre|
|[ancestry-17-iVwsLYjOJbfvL0Pe.htm](feats/ancestry-17-iVwsLYjOJbfvL0Pe.htm)|Heroic Presence|Présence héroïque|libre|
|[ancestry-17-IyFz7kVF8wzPdrfJ.htm](feats/ancestry-17-IyFz7kVF8wzPdrfJ.htm)|Unfettering Prankster|Farceur affranchi|libre|
|[ancestry-17-IzJ76OXe1gl2hfbd.htm](feats/ancestry-17-IzJ76OXe1gl2hfbd.htm)|Stormy Heart|Coeur tempêtueux|libre|
|[ancestry-17-JgGsWppVh4q4HtSy.htm](feats/ancestry-17-JgGsWppVh4q4HtSy.htm)|Dominion Aura|Aura de domination|libre|
|[ancestry-17-JrBqOxHZX20b8gTT.htm](feats/ancestry-17-JrBqOxHZX20b8gTT.htm)|Soaring Poppet|Poupée volante|libre|
|[ancestry-17-KL3Pk10ReItAHTw9.htm](feats/ancestry-17-KL3Pk10ReItAHTw9.htm)|Celestial Word|Parole céleste|libre|
|[ancestry-17-kr537ZL3f8tCSgDK.htm](feats/ancestry-17-kr537ZL3f8tCSgDK.htm)|True Gaze|Regard lucide|libre|
|[ancestry-17-kt5UJEfTzO3LiInN.htm](feats/ancestry-17-kt5UJEfTzO3LiInN.htm)|Crone's Cruelty|Cruauté de vieille fille|libre|
|[ancestry-17-LIEbgqgSXkGIli8u.htm](feats/ancestry-17-LIEbgqgSXkGIli8u.htm)|Animal Swiftness (Fly)|Rapidité animale (Vol)|libre|
|[ancestry-17-LoZY48txEgHZxewj.htm](feats/ancestry-17-LoZY48txEgHZxewj.htm)|Azaersi's Roads|Routes d'Azaersi|libre|
|[ancestry-17-LTIARSoQP8WYE33A.htm](feats/ancestry-17-LTIARSoQP8WYE33A.htm)|Necromantic Heir|Héritier nécromantique|libre|
|[ancestry-17-MzdoJWpqyMmMDI1F.htm](feats/ancestry-17-MzdoJWpqyMmMDI1F.htm)|Fountain of Secrets|Fontaine de secrets|libre|
|[ancestry-17-Nd7C7X9LYyp0TaET.htm](feats/ancestry-17-Nd7C7X9LYyp0TaET.htm)|Animal Swiftness (Swim)|Rapidité animale (Natation)|libre|
|[ancestry-17-nJRcbt72wRk5Rmc4.htm](feats/ancestry-17-nJRcbt72wRk5Rmc4.htm)|Symphony Of Blood|Symphonie de sang|libre|
|[ancestry-17-oItG5dqlPEi8wNnR.htm](feats/ancestry-17-oItG5dqlPEi8wNnR.htm)|Trickster Tengu|Mystificateur tengu|libre|
|[ancestry-17-OpSHTPkoZDrqR0Mq.htm](feats/ancestry-17-OpSHTPkoZDrqR0Mq.htm)|Animal Shape|Forme animale|libre|
|[ancestry-17-OWL6ZNVWMU0AFqvZ.htm](feats/ancestry-17-OWL6ZNVWMU0AFqvZ.htm)|Eternal Wings (Aasimar)|Ailes éternelles (Aasimar)|libre|
|[ancestry-17-Qc9MH7wT182qasSV.htm](feats/ancestry-17-Qc9MH7wT182qasSV.htm)|Cantorian Restoration|Récupération cantorienne|libre|
|[ancestry-17-QgNo1s6nVbKPU4St.htm](feats/ancestry-17-QgNo1s6nVbKPU4St.htm)|Reliable Luck|Chance fiable|libre|
|[ancestry-17-QXZFvsZDduxwTJYM.htm](feats/ancestry-17-QXZFvsZDduxwTJYM.htm)|Magic Rider|Chevaucheur de magie|libre|
|[ancestry-17-rq0lnwZ51AtJ0aux.htm](feats/ancestry-17-rq0lnwZ51AtJ0aux.htm)|Breath of Calamity|Souffle de la calamité|libre|
|[ancestry-17-SFnKHPrd65RORTgN.htm](feats/ancestry-17-SFnKHPrd65RORTgN.htm)|Prismatic Scales|Écailles prismatiques|libre|
|[ancestry-17-U9nJy5suyYDE0b20.htm](feats/ancestry-17-U9nJy5suyYDE0b20.htm)|Flourish And Ruin|Prospérer et ruiner|libre|
|[ancestry-17-UKHIKipBvOGhzcSQ.htm](feats/ancestry-17-UKHIKipBvOGhzcSQ.htm)|Homeward Bound|Rentrer à la maison|libre|
|[ancestry-17-UOL44Qm1rOVD5TFg.htm](feats/ancestry-17-UOL44Qm1rOVD5TFg.htm)|Underwater Volcano|Volcan sous-marin|libre|
|[ancestry-17-UquGIEFKhutgJsEz.htm](feats/ancestry-17-UquGIEFKhutgJsEz.htm)|Stonegate|Passepierre|libre|
|[ancestry-17-VbHuOvF1MM9ls6Tg.htm](feats/ancestry-17-VbHuOvF1MM9ls6Tg.htm)|Favor Of Heaven|Faveur du Paradis|libre|
|[ancestry-17-VPKBjEy8frqDMxyO.htm](feats/ancestry-17-VPKBjEy8frqDMxyO.htm)|Greater Enhance Venom|Amélioration du venin supérieure|libre|
|[ancestry-17-wfOGyLnuVMFo7Rwy.htm](feats/ancestry-17-wfOGyLnuVMFo7Rwy.htm)|Boneyard's Call|Appel du Cimetière|libre|
|[ancestry-17-wFtJlamwRc6rSQmj.htm](feats/ancestry-17-wFtJlamwRc6rSQmj.htm)|Scion Transformation|Transformation du rejeton|libre|
|[ancestry-17-WZC2XXQo6Pu2GQeo.htm](feats/ancestry-17-WZC2XXQo6Pu2GQeo.htm)|Soaring Form|Forme volante|libre|
|[ancestry-17-WZLw9UrIyCz6Eiqo.htm](feats/ancestry-17-WZLw9UrIyCz6Eiqo.htm)|Animal Swiftness (Climb)|Rapidité animale (Escalade)|libre|
|[ancestry-17-xXaKiJbKhClF5eJx.htm](feats/ancestry-17-xXaKiJbKhClF5eJx.htm)|Final Form|Forme finale|libre|
|[ancestry-17-zeyrLJr6b7hPdx4w.htm](feats/ancestry-17-zeyrLJr6b7hPdx4w.htm)|Core Cannon|Noyau canon|libre|
|[ancestry-17-Zl6vDXJEM7Lbp8JQ.htm](feats/ancestry-17-Zl6vDXJEM7Lbp8JQ.htm)|Rampaging Form|Forme Destructrice|libre|
|[ancestry-17-ZOlwVqWa4hfv44xX.htm](feats/ancestry-17-ZOlwVqWa4hfv44xX.htm)|Hero's Wings|Ailes du héros|libre|
|[ancestry-17-zSJ0Ke8HWzG1krlq.htm](feats/ancestry-17-zSJ0Ke8HWzG1krlq.htm)|Reflect Foe|Ennemi reflété|libre|
|[bonus-00-1xpqOpguTUAeFsdO.htm](feats/bonus-00-1xpqOpguTUAeFsdO.htm)|Seek Injustice|Débusquer l'injustice|officielle|
|[bonus-00-AGiIjgLOFuahmwiT.htm](feats/bonus-00-AGiIjgLOFuahmwiT.htm)|Shackles of Law|Chaînes de la Loi|officielle|
|[bonus-00-AxjjAoU1IZaNUGGS.htm](feats/bonus-00-AxjjAoU1IZaNUGGS.htm)|Fear No Law, Fear No One|Peur d'aucune loi, ni de personne|officielle|
|[bonus-00-cMkpLgadlLCzDOv0.htm](feats/bonus-00-cMkpLgadlLCzDOv0.htm)|Blessings of the Five|Bénédiction des cinq|officielle|
|[bonus-00-FlsAYAGEiZg1gg7D.htm](feats/bonus-00-FlsAYAGEiZg1gg7D.htm)|Spiritual Disruption|Perturbation spirituelle|officielle|
|[bonus-00-jrDCPvkruz8y740Z.htm](feats/bonus-00-jrDCPvkruz8y740Z.htm)|Locate Lawbreakers|Localisation des hors-la-loi|officielle|
|[bonus-00-Kz4E8heU12IGcYoi.htm](feats/bonus-00-Kz4E8heU12IGcYoi.htm)|Righteous Resistance|Résistance du vertueux|libre|
|[bonus-00-PpJURSJFEHzhutdp.htm](feats/bonus-00-PpJURSJFEHzhutdp.htm)|Trailblazing Stride|Marche du pionnier|officielle|
|[bonus-00-PPSH5vdf90KC95jJ.htm](feats/bonus-00-PPSH5vdf90KC95jJ.htm)|Reveal Beasts|Dévoiler les bêtes|officielle|
|[bonus-00-RkvAuikI0kIZlQTU.htm](feats/bonus-00-RkvAuikI0kIZlQTU.htm)|Disillusionment|Désillusion|officielle|
|[bonus-00-tPhhaCbaQqwenkzx.htm](feats/bonus-00-tPhhaCbaQqwenkzx.htm)|Silence Heresy|Étouffer l'hérésie|officielle|
|[bonus-00-UHejQS4uCNGRI45t.htm](feats/bonus-00-UHejQS4uCNGRI45t.htm)|Dedication to the Five|Dévotion envers les cinq|officielle|
|[bonus-00-w7dOgJvqAqyqSeSQ.htm](feats/bonus-00-w7dOgJvqAqyqSeSQ.htm)|Devil Allies|Alliés diaboliques|officielle|
|[bonus-00-XOAtP64xiSRIofsY.htm](feats/bonus-00-XOAtP64xiSRIofsY.htm)|Sturdy Bindings|Liens renforcés|officielle|
|[class-01-0BR61rW4JFOfO7T7.htm](feats/class-01-0BR61rW4JFOfO7T7.htm)|Cackle|Gloussement|libre|
|[class-01-0PCDkVnRxVqxsp9j.htm](feats/class-01-0PCDkVnRxVqxsp9j.htm)|Energy Heart|Coeur énergétique|libre|
|[class-01-0yPbPVEESwB6Bdfw.htm](feats/class-01-0yPbPVEESwB6Bdfw.htm)|Magus's Analysis|Analyse du magus|libre|
|[class-01-142QRyK2aPIrJu48.htm](feats/class-01-142QRyK2aPIrJu48.htm)|Holy Castigation|Punition sacrée|libre|
|[class-01-1JnERVwnPtX620f2.htm](feats/class-01-1JnERVwnPtX620f2.htm)|Animal Companion (Ranger)|Compagnon animal (rôdeur)|libre|
|[class-01-1MWL2uEmyiOfYtJn.htm](feats/class-01-1MWL2uEmyiOfYtJn.htm)|Disarming Flair|Élégance désarmante|libre|
|[class-01-1W0a6YCyoYv8dm4e.htm](feats/class-01-1W0a6YCyoYv8dm4e.htm)|Harming Hands|Mains blessantes|officielle|
|[class-01-2bCbfQ0KCSSYpg2q.htm](feats/class-01-2bCbfQ0KCSSYpg2q.htm)|Unfetter Eidolon|Eidolon affranchi|libre|
|[class-01-2c9awqDem5OLK47S.htm](feats/class-01-2c9awqDem5OLK47S.htm)|Weight of Guilt|Poids de la culpabilité|officielle|
|[class-01-2Lcwqwq8CF40TYHd.htm](feats/class-01-2Lcwqwq8CF40TYHd.htm)|Dual Studies|Études duales|libre|
|[class-01-2xk4jdwcCfmasYfT.htm](feats/class-01-2xk4jdwcCfmasYfT.htm)|Power Attack|Attaque en puissance|officielle|
|[class-01-3a07jkWezSuORSpS.htm](feats/class-01-3a07jkWezSuORSpS.htm)|Ki Strike|Frappe ki|officielle|
|[class-01-4HgKmqTrXYZJ4fyj.htm](feats/class-01-4HgKmqTrXYZJ4fyj.htm)|Animal Companion (Druid)|Compagnon animal (druide)|officielle|
|[class-01-4KkzwOu2OKYLKSXQ.htm](feats/class-01-4KkzwOu2OKYLKSXQ.htm)|Variable Core|Noyau alternatif|libre|
|[class-01-5FyvwI24mnROzh61.htm](feats/class-01-5FyvwI24mnROzh61.htm)|Combat Assessment|Évaluation du combat|libre|
|[class-01-5GqjEM22n78Vmdpe.htm](feats/class-01-5GqjEM22n78Vmdpe.htm)|Focused Fascination|Fascination concentrée|libre|
|[class-01-5pGMffGKkBTZKvjw.htm](feats/class-01-5pGMffGKkBTZKvjw.htm)|Raging Thrower|Lanceur enragé|officielle|
|[class-01-6GN1zh3RcnZhrzxP.htm](feats/class-01-6GN1zh3RcnZhrzxP.htm)|Everstand Stance|Posture toujours en position|libre|
|[class-01-6LFBPpPPJjDq07fg.htm](feats/class-01-6LFBPpPPJjDq07fg.htm)|Hit the Dirt!|Mordre la poussière !|libre|
|[class-01-6thEFvU7aMPmLrly.htm](feats/class-01-6thEFvU7aMPmLrly.htm)|False Faith|Foi de façade|libre|
|[class-01-7FRYyKXDKjGoANYj.htm](feats/class-01-7FRYyKXDKjGoANYj.htm)|Stumbling Stance|Posture chancelante|libre|
|[class-01-7y1BCJLrdk9mKXlc.htm](feats/class-01-7y1BCJLrdk9mKXlc.htm)|Leshy Familiar|Familier léchi|officielle|
|[class-01-82ATEfDMPkZDxV5H.htm](feats/class-01-82ATEfDMPkZDxV5H.htm)|Blessed Blood (Sorcerer)|Sang béni (Ensorceleur)|libre|
|[class-01-8sy3sHwOHS4ImwvJ.htm](feats/class-01-8sy3sHwOHS4ImwvJ.htm)|Dragon Stance|Posture du dragon|officielle|
|[class-01-8tS5NzytLmgbq5hF.htm](feats/class-01-8tS5NzytLmgbq5hF.htm)|Exacting Strike|Frappe contrôlée|officielle|
|[class-01-8zHsIubGREIrGfAA.htm](feats/class-01-8zHsIubGREIrGfAA.htm)|Prototype Companion|Compagnon prototype|libre|
|[class-01-9bgl6qYWKHzqWZj0.htm](feats/class-01-9bgl6qYWKHzqWZj0.htm)|Flexible Studies|Études diversifiées|libre|
|[class-01-9CaaU5szcf3mtJXD.htm](feats/class-01-9CaaU5szcf3mtJXD.htm)|Overextending Feint|Feinte précautionneuse|libre|
|[class-01-9oKtrUzXixj58hOg.htm](feats/class-01-9oKtrUzXixj58hOg.htm)|Psychic Rapport|Rapport psychique|libre|
|[class-01-9XALeVNcmlIxf3tJ.htm](feats/class-01-9XALeVNcmlIxf3tJ.htm)|Twin Feint|Feinte jumelée|officielle|
|[class-01-AD2eQu6SjLhUGD6Z.htm](feats/class-01-AD2eQu6SjLhUGD6Z.htm)|Eschew Materials|Dispense de composant matériel|officielle|
|[class-01-AN9jY1JVcU20Qdw6.htm](feats/class-01-AN9jY1JVcU20Qdw6.htm)|Wolf Stance|Posture du loup|officielle|
|[class-01-avCmIwmwJH7d7gri.htm](feats/class-01-avCmIwmwJH7d7gri.htm)|Ancestral Blood Magic|Sang magique ancestral|libre|
|[class-01-b09B0XLNAiFP3gFT.htm](feats/class-01-b09B0XLNAiFP3gFT.htm)|Counterspell (Sorcerer)|Contresort de l'ensorceleur|officielle|
|[class-01-BBj6jrdyff7QOgjH.htm](feats/class-01-BBj6jrdyff7QOgjH.htm)|Adrenaline Rush|Poussée d'adrénaline|libre|
|[class-01-bCizH4ByTwbLcYA1.htm](feats/class-01-bCizH4ByTwbLcYA1.htm)|One For All|Un pour tous|libre|
|[class-01-bcxIg7wi8ZAhvhOD.htm](feats/class-01-bcxIg7wi8ZAhvhOD.htm)|Familiar|Familier|libre|
|[class-01-bf7NCeKqDClaqhTR.htm](feats/class-01-bf7NCeKqDClaqhTR.htm)|Crane Stance|Posture de la grue|officielle|
|[class-01-Bj0GvPgyPiC2kDH1.htm](feats/class-01-Bj0GvPgyPiC2kDH1.htm)|Glider Form|Forme de planeur|libre|
|[class-01-BJHsCiBLdjgJo6zM.htm](feats/class-01-BJHsCiBLdjgJo6zM.htm)|Far Lobber|Lanceur à grande distance|officielle|
|[class-01-BWomK7EVY0WXxWgh.htm](feats/class-01-BWomK7EVY0WXxWgh.htm)|Reach Spell|Sort éloigné|officielle|
|[class-01-c3b7DhnDBC7YEgRG.htm](feats/class-01-c3b7DhnDBC7YEgRG.htm)|Hunted Shot|Tir du chasseur|officielle|
|[class-01-C5tSxOwDILefw4zq.htm](feats/class-01-C5tSxOwDILefw4zq.htm)|Buckler Expertise|Expertise de la targe|libre|
|[class-01-CpjN7v1QN8TQFcvI.htm](feats/class-01-CpjN7v1QN8TQFcvI.htm)|Crossbow Ace|Arbalétrier émérite|officielle|
|[class-01-deoHKUzpzT7iwWhL.htm](feats/class-01-deoHKUzpzT7iwWhL.htm)|Counterspell (Spontaneous)|Contresort spontané|officielle|
|[class-01-DfLkIIg2reyYW3a8.htm](feats/class-01-DfLkIIg2reyYW3a8.htm)|Deadly Simplicity|Simplicité mortelle|officielle|
|[class-01-dFQj6gLrbQi7APiA.htm](feats/class-01-dFQj6gLrbQi7APiA.htm)|Agile Shield Grip|Prise de bouclier agile|libre|
|[class-01-dGFQvkDRmyvvf4IQ.htm](feats/class-01-dGFQvkDRmyvvf4IQ.htm)|Splinter Faith|Foi schismatique|libre|
|[class-01-dNH8OHEvx3vI9NBQ.htm](feats/class-01-dNH8OHEvx3vI9NBQ.htm)|Nimble Dodge|Esquive agile|officielle|
|[class-01-DqD7htz8Sd1dh3BT.htm](feats/class-01-DqD7htz8Sd1dh3BT.htm)|Gorilla Stance|Posture de gorille|libre|
|[class-01-dWbISC0di0r4oPCi.htm](feats/class-01-dWbISC0di0r4oPCi.htm)|Sword and Pistol|Épée et pistolet|libre|
|[class-01-emRfSVvU8ZAH9UdK.htm](feats/class-01-emRfSVvU8ZAH9UdK.htm)|Dangerous Sorcery|Sorcellerie dangereuse|officielle|
|[class-01-EOmTf95t03y4IGdp.htm](feats/class-01-EOmTf95t03y4IGdp.htm)|Draconic Arrogance|Arrogance draconique|libre|
|[class-01-EpBG4CFMNSZQx7vI.htm](feats/class-01-EpBG4CFMNSZQx7vI.htm)|Counterspell (Prepared)|Contresort préparé|officielle|
|[class-01-epfQ2PwNDLwyY31t.htm](feats/class-01-epfQ2PwNDLwyY31t.htm)|Unimpeded Step|Pas sans entrave|officielle|
|[class-01-eww9tuiXPnZFZ3DU.htm](feats/class-01-eww9tuiXPnZFZ3DU.htm)|Plant Evidence|Placer une preuve|libre|
|[class-01-f2Pl5dWEL9ZvEyI1.htm](feats/class-01-f2Pl5dWEL9ZvEyI1.htm)|Animal Companion|Compagnon animal|officielle|
|[class-01-FCzfh8QHMo7QJpAM.htm](feats/class-01-FCzfh8QHMo7QJpAM.htm)|Spellbook Prodigy|Prodige du grimoire|libre|
|[class-01-FhxkU6OftQeecpQW.htm](feats/class-01-FhxkU6OftQeecpQW.htm)|Heal Companion|Guérison du compagnon|libre|
|[class-01-FoWO4RnHRwfEIC7Q.htm](feats/class-01-FoWO4RnHRwfEIC7Q.htm)|Widen Spell|Sort élargi|officielle|
|[class-01-fUR72e3t7p2IcqqG.htm](feats/class-01-fUR72e3t7p2IcqqG.htm)|Divine Disharmony|Désaccord divin|libre|
|[class-01-FXKIALDXAzEBfj5A.htm](feats/class-01-FXKIALDXAzEBfj5A.htm)|Deity's Domain|Domaine de la divinité|officielle|
|[class-01-gFQFgREm3HaFx1mf.htm](feats/class-01-gFQFgREm3HaFx1mf.htm)|Built-In Tools|Outils intégrés|libre|
|[class-01-gjec0ts3wkFbjvHN.htm](feats/class-01-gjec0ts3wkFbjvHN.htm)|You're Next|Tu es le suivant|officielle|
|[class-01-GuEdTz1VMEptQnOd.htm](feats/class-01-GuEdTz1VMEptQnOd.htm)|Stoked Flame Stance|Posture de la flamme ravivée|libre|
|[class-01-Gw0wGXikhAhiGoud.htm](feats/class-01-Gw0wGXikhAhiGoud.htm)|Twin Takedown|Agression jumelée|officielle|
|[class-01-hT4INKGtly4QY8KN.htm](feats/class-01-hT4INKGtly4QY8KN.htm)|Domain Initiate|Initié du domaine|officielle|
|[class-01-hXYnwpi95E77qfAu.htm](feats/class-01-hXYnwpi95E77qfAu.htm)|Goading Feint|Feinte provoquante|libre|
|[class-01-iP01b6eyUm4m6KQp.htm](feats/class-01-iP01b6eyUm4m6KQp.htm)|Haunt Ingenuity|Éthologie des apparitions|libre|
|[class-01-iT39wlCEC1aWaSx7.htm](feats/class-01-iT39wlCEC1aWaSx7.htm)|Gravity Weapon|Arme pesante|libre|
|[class-01-iTdbkP07UFMOo1rI.htm](feats/class-01-iTdbkP07UFMOo1rI.htm)|Ranged Reprisal|Représailles à distance|officielle|
|[class-01-iWvpq3uDZcXvBJj8.htm](feats/class-01-iWvpq3uDZcXvBJj8.htm)|Known Weaknesses|Faiblesses connues|libre|
|[class-01-iX5HEqRImhKzfPR2.htm](feats/class-01-iX5HEqRImhKzfPR2.htm)|Well-Versed|Connaisseur|libre|
|[class-01-IzkL60LlKzbKIhY1.htm](feats/class-01-IzkL60LlKzbKIhY1.htm)|Cover Fire|Tir de couverture|libre|
|[class-01-jBp91q4uzwd4FeSX.htm](feats/class-01-jBp91q4uzwd4FeSX.htm)|Versatile Performance|Polyvalence artistique|officielle|
|[class-01-jGTRRCqxn1FIBxE2.htm](feats/class-01-jGTRRCqxn1FIBxE2.htm)|Hymn Of Healing|Hymne de guérison|libre|
|[class-01-JoxEspM0kbCop7og.htm](feats/class-01-JoxEspM0kbCop7og.htm)|Raise a Tome|Interposer un livre|libre|
|[class-01-KaCpXuYuho3nnDUy.htm](feats/class-01-KaCpXuYuho3nnDUy.htm)|Vicious Vengeance|Vengeance vicieuse|libre|
|[class-01-kFpVgcqREAfDmjXp.htm](feats/class-01-kFpVgcqREAfDmjXp.htm)|Advanced Weaponry|Armement avancé|libre|
|[class-01-KlqKpeq5OmTRxVHb.htm](feats/class-01-KlqKpeq5OmTRxVHb.htm)|Diverse Lore|Connaissance diversifiée|libre|
|[class-01-knZUN4sYExIyRC4F.htm](feats/class-01-knZUN4sYExIyRC4F.htm)|Reflective Ripple Stance|Posture de l'onde réfléchissante|libre|
|[class-01-kS4wxSKrZxcOvSNK.htm](feats/class-01-kS4wxSKrZxcOvSNK.htm)|Shore Step|Pas du littoral|libre|
|[class-01-KYl1rWFOHe0e6VqJ.htm](feats/class-01-KYl1rWFOHe0e6VqJ.htm)|Premonition Of Avoidance|Évitement prémonitoire|libre|
|[class-01-L5nSP8CkHWSLp2vV.htm](feats/class-01-L5nSP8CkHWSLp2vV.htm)|Arcane Fists|Poings arcaniques|libre|
|[class-01-lFVqejlf52cdYrZy.htm](feats/class-01-lFVqejlf52cdYrZy.htm)|Munitions Crafter|Fabricant de munitions|libre|
|[class-01-loC0wIyIrsG43Zrd.htm](feats/class-01-loC0wIyIrsG43Zrd.htm)|Alchemical Familiar|Familier alchimique|officielle|
|[class-01-MJW4VP7PjVAX131C.htm](feats/class-01-MJW4VP7PjVAX131C.htm)|Glean Lore|Glaner le savoir|libre|
|[class-01-MRMW0EiuOO20pzG2.htm](feats/class-01-MRMW0EiuOO20pzG2.htm)|Ongoing Selfishness|Égoïsme continu|libre|
|[class-01-mWCiu9Hl1WxajSLa.htm](feats/class-01-mWCiu9Hl1WxajSLa.htm)|Snagging Strike|Frappe déconcertante|officielle|
|[class-01-oA866uVEFu1OrAX0.htm](feats/class-01-oA866uVEFu1OrAX0.htm)|Trap Finder|Dénicheur de pièges|officielle|
|[class-01-onde0SxLoxLBTnvm.htm](feats/class-01-onde0SxLoxLBTnvm.htm)|Double Slice|Double taille|officielle|
|[class-01-oQVp2UhXVBcELma5.htm](feats/class-01-oQVp2UhXVBcELma5.htm)|Root to Life|Enraciné à la vie|libre|
|[class-01-pD1oDbUDkNtHadGY.htm](feats/class-01-pD1oDbUDkNtHadGY.htm)|Hand of the Apprentice|Main de l'apprenti|officielle|
|[class-01-PGi1EtcZMolnNA1M.htm](feats/class-01-PGi1EtcZMolnNA1M.htm)|Ancestral Mind|Esprit ancestral|libre|
|[class-01-pgIrr8xrlCXbRAeo.htm](feats/class-01-pgIrr8xrlCXbRAeo.htm)|Underworld Investigator|Enquêteur de la pègre|libre|
|[class-01-pIHjH1x0AVtiX5Tv.htm](feats/class-01-pIHjH1x0AVtiX5Tv.htm)|That's Odd|C'est étrange|libre|
|[class-01-PqImBJ2JrPo5oFxc.htm](feats/class-01-PqImBJ2JrPo5oFxc.htm)|Vile Desecration|Vile profanation|libre|
|[class-01-q1iP3SjAF5uceI0M.htm](feats/class-01-q1iP3SjAF5uceI0M.htm)|Martial Performance|Représentation martiale|libre|
|[class-01-QEOe9AhQzdqIx1ei.htm](feats/class-01-QEOe9AhQzdqIx1ei.htm)|Steadying Stone|Pierre stable|libre|
|[class-01-qgNc5XwjsaWET0Op.htm](feats/class-01-qgNc5XwjsaWET0Op.htm)|Flying Blade|Lame volante|libre|
|[class-01-QKC9iiR4Epj1Lyc7.htm](feats/class-01-QKC9iiR4Epj1Lyc7.htm)|Counter Thought|Contrer la pensée|libre|
|[class-01-qPFWEyihvbWsCcUv.htm](feats/class-01-qPFWEyihvbWsCcUv.htm)|Verdant Weapon|Arme verdoyante|libre|
|[class-01-qQt3CMrhLkUV1wCv.htm](feats/class-01-qQt3CMrhLkUV1wCv.htm)|Sudden Charge|Charge soudaine|officielle|
|[class-01-QZ00D2xdJnbWFzml.htm](feats/class-01-QZ00D2xdJnbWFzml.htm)|Subtle Delivery|Libération subtile|libre|
|[class-01-rbiMK71SvGZGRLJ1.htm](feats/class-01-rbiMK71SvGZGRLJ1.htm)|Rain of Embers Stance|Posture de la pluie de braises|libre|
|[class-01-rPzEnABFjkbOARiB.htm](feats/class-01-rPzEnABFjkbOARiB.htm)|Expanded Senses|Sens étendus|libre|
|[class-01-s6h0xkdKf3gecLk6.htm](feats/class-01-s6h0xkdKf3gecLk6.htm)|Crossbow Crack Shot|Tir de l'as de l'arbalète|libre|
|[class-01-s97nKj9Uye1KXr7A.htm](feats/class-01-s97nKj9Uye1KXr7A.htm)|Meld into Eidolon|Fusion dans l'eidolon|libre|
|[class-01-sf73K6f8xlfhHS1n.htm](feats/class-01-sf73K6f8xlfhHS1n.htm)|Scroll Thaumaturgy|Thaumaturgie parcheminée|libre|
|[class-01-SheifYobjKqyK3Fv.htm](feats/class-01-SheifYobjKqyK3Fv.htm)|Tamper|Trafiquer|libre|
|[class-01-sjChYEuEWPqndCSK.htm](feats/class-01-sjChYEuEWPqndCSK.htm)|Dual-Weapon Reload|Rechargement à deux armes|libre|
|[class-01-sVjATEo8eqkAosNp.htm](feats/class-01-sVjATEo8eqkAosNp.htm)|Lingering Composition|Composition prolongée|officielle|
|[class-01-tn1PajS2DrHeBlGg.htm](feats/class-01-tn1PajS2DrHeBlGg.htm)|Ammunition Thaumaturgy|Recharge du thaumaturge|libre|
|[class-01-u5cPwhuCd3xTAlWl.htm](feats/class-01-u5cPwhuCd3xTAlWl.htm)|Extend Boost|Étendre le boost|libre|
|[class-01-UpEjRfQkCJCruAfb.htm](feats/class-01-UpEjRfQkCJCruAfb.htm)|Storm Born|Enfant de la tempête|officielle|
|[class-01-UtRW3hYBTFG8HQLh.htm](feats/class-01-UtRW3hYBTFG8HQLh.htm)|Mental Buffer|Amortisseur mental|libre|
|[class-01-UVsMJwHpQHjVZLTK.htm](feats/class-01-UVsMJwHpQHjVZLTK.htm)|Explosive Leap|Bond explosif|libre|
|[class-01-uVXEZblPRuCyPRua.htm](feats/class-01-uVXEZblPRuCyPRua.htm)|Bardic Lore|Connaissance bardique|officielle|
|[class-01-VaIHQzOE5ibmbtqU.htm](feats/class-01-VaIHQzOE5ibmbtqU.htm)|Wild Shape|Morphologie sauvage|libre|
|[class-01-VCjAlOvjNvFJOsG5.htm](feats/class-01-VCjAlOvjNvFJOsG5.htm)|Tiger Stance|Posture du tigre|officielle|
|[class-01-vRXPsMbyuvoPvbAQ.htm](feats/class-01-vRXPsMbyuvoPvbAQ.htm)|Moment of Clarity|Moment de lucidité|officielle|
|[class-01-w8Ycgeq2zfyshtoS.htm](feats/class-01-w8Ycgeq2zfyshtoS.htm)|Reactive Shield|Bouclier réactif|officielle|
|[class-01-WcY7H7mRiK2VDquV.htm](feats/class-01-WcY7H7mRiK2VDquV.htm)|Dueling Parry (Swashbuckler)|Parade en duel (Bretteur)|libre|
|[class-01-WYaKRREZUSH0jel5.htm](feats/class-01-WYaKRREZUSH0jel5.htm)|Desperate Prayer|Prière désespérée|libre|
|[class-01-xbg1scOIT7RI9Fij.htm](feats/class-01-xbg1scOIT7RI9Fij.htm)|Quick Bomber|Artificier rapide|libre|
|[class-01-XHy9inDi3oGZNd43.htm](feats/class-01-XHy9inDi3oGZNd43.htm)|No! No! I Created You!|Non ! Non ! Je t'ai créé !|libre|
|[class-01-XLoRtMn48f2KKvsu.htm](feats/class-01-XLoRtMn48f2KKvsu.htm)|Blast Lock|Faire sauter la serrure|libre|
|[class-01-XseJI9XhKNtZN8pv.htm](feats/class-01-XseJI9XhKNtZN8pv.htm)|Raging Intimidation|Intimidation enragée|officielle|
|[class-01-XttSGDuAsRDTuvgS.htm](feats/class-01-XttSGDuAsRDTuvgS.htm)|Iron Repercussions|Répercussions de fer|libre|
|[class-01-yA9tsWT9uzL2REnw.htm](feats/class-01-yA9tsWT9uzL2REnw.htm)|Ki Rush|Ruée ki|libre|
|[class-01-YdpGPLN0QFTZIhbk.htm](feats/class-01-YdpGPLN0QFTZIhbk.htm)|Acute Vision|Vision perçante|officielle|
|[class-01-YG2RxXE9SMfwo6wP.htm](feats/class-01-YG2RxXE9SMfwo6wP.htm)|Monastic Archer Stance|Posture de l'archer monastique|libre|
|[class-01-YKqMuuC8j35NFh92.htm](feats/class-01-YKqMuuC8j35NFh92.htm)|Tumble Behind (Rogue)|Passer derrière (Roublard)|libre|
|[class-01-Yl2wYv24v5kw95aS.htm](feats/class-01-Yl2wYv24v5kw95aS.htm)|Point-Blank Shot|Tir à bout portant|officielle|
|[class-01-yOloZIGkulrrPYG4.htm](feats/class-01-yOloZIGkulrrPYG4.htm)|Fire Lung|Poumon de feu|libre|
|[class-01-ypir96PNY6LBSIWT.htm](feats/class-01-ypir96PNY6LBSIWT.htm)|Alchemical Savant|Savant alchimiste|officielle|
|[class-01-yPWNbTqOIKdkwaVq.htm](feats/class-01-yPWNbTqOIKdkwaVq.htm)|Takedown Expert|Expert de la mise au tapis|libre|
|[class-01-YTTJqRKH8QZl6al2.htm](feats/class-01-YTTJqRKH8QZl6al2.htm)|Monster Hunter|Chasseur de monstres|officielle|
|[class-01-yVC5pVZaWczYWGTa.htm](feats/class-01-yVC5pVZaWczYWGTa.htm)|Syncretism|Syncrétisme|libre|
|[class-01-YW4FdWgzTwRFneFF.htm](feats/class-01-YW4FdWgzTwRFneFF.htm)|Monastic Weaponry|Armes monacales|officielle|
|[class-01-ZL5UU9quCTvcWzfY.htm](feats/class-01-ZL5UU9quCTvcWzfY.htm)|Mountain Stance|Posture de la montagne|officielle|
|[class-01-zUtdBd3IbM7UX0AD.htm](feats/class-01-zUtdBd3IbM7UX0AD.htm)|Cauldron|Chaudron|libre|
|[class-01-zxaE7C1NCGqZR8aU.htm](feats/class-01-zxaE7C1NCGqZR8aU.htm)|Haphazard Repair|Réparation à l'arrache|libre|
|[class-01-zY6y802bOouMYYFV.htm](feats/class-01-zY6y802bOouMYYFV.htm)|Wortwitch|Sorcier herbacé|libre|
|[class-01-zYhcEX4JnrZ08HfV.htm](feats/class-01-zYhcEX4JnrZ08HfV.htm)|Healing Hands|Mains guérisseuses|officielle|
|[class-02-0Bm4WMi9u0EOHbkp.htm](feats/class-02-0Bm4WMi9u0EOHbkp.htm)|Resilient Mind|Esprit résistant|libre|
|[class-02-0i4zOM1p0rFxnc5m.htm](feats/class-02-0i4zOM1p0rFxnc5m.htm)|Harrower Dedication|Dévouement : Liseur de Tourment|libre|
|[class-02-0icxuITlxUBfeomX.htm](feats/class-02-0icxuITlxUBfeomX.htm)|Turpin Rowe Lumberjack Dedication|Dévouement : Bûcheron de Turpin Rowe|libre|
|[class-02-0lieFKdwgNVXep7u.htm](feats/class-02-0lieFKdwgNVXep7u.htm)|Poison Resistance|Résistance au poison|officielle|
|[class-02-0NRIP8cDv9Opl5Ls.htm](feats/class-02-0NRIP8cDv9Opl5Ls.htm)|Fiendsbane Oath (Paladin)|Serment du fléau des fiélons (Paladin)|officielle|
|[class-02-0ogbBzSD7fmiWp9d.htm](feats/class-02-0ogbBzSD7fmiWp9d.htm)|Living Vessel Dedication|Dévouement : Réceptacle vivant|libre|
|[class-02-0YjetnLVIUz9zQt5.htm](feats/class-02-0YjetnLVIUz9zQt5.htm)|Champion Dedication|Dévouement : Champion|officielle|
|[class-02-17jjQPpnBkHRkzPE.htm](feats/class-02-17jjQPpnBkHRkzPE.htm)|Turn Away Misfortune|Détourner le mauvais sort|libre|
|[class-02-19OLlTrxvEtDAOHc.htm](feats/class-02-19OLlTrxvEtDAOHc.htm)|Aggressive Block|Blocage agressif|officielle|
|[class-02-1hyC4aiLMBSwM9Z1.htm](feats/class-02-1hyC4aiLMBSwM9Z1.htm)|Trick Driver Dedication|Dévouement : Conducteur astucieux|libre|
|[class-02-1t5479E6bdvFs4E7.htm](feats/class-02-1t5479E6bdvFs4E7.htm)|Talisman Dabbler Dedication|Dévouement : Amateur de talismans|libre|
|[class-02-20jNBiIIxaiOVyi0.htm](feats/class-02-20jNBiIIxaiOVyi0.htm)|Oozemorph Dedication|Dévouement : Vasemorphe|libre|
|[class-02-2AeGnjk4EAqFDvK8.htm](feats/class-02-2AeGnjk4EAqFDvK8.htm)|Spirit Sheath|Fourreau spirituel|libre|
|[class-02-2rJP5TqvfazNeNmY.htm](feats/class-02-2rJP5TqvfazNeNmY.htm)|Revivifying Mutagen|Mutagène revivifiant|officielle|
|[class-02-2yq3l8e2Vzz34yRV.htm](feats/class-02-2yq3l8e2Vzz34yRV.htm)|Monster Warden|Garde-monstres|officielle|
|[class-02-38L5yCVNgRAFFMsZ.htm](feats/class-02-38L5yCVNgRAFFMsZ.htm)|Cantrip Expansion|Expansion de tour de magie (psychiste)|libre|
|[class-02-3kH0fGOIoYvPNQsq.htm](feats/class-02-3kH0fGOIoYvPNQsq.htm)|After You|Après vous|libre|
|[class-02-3r4v6W4abJnLg7dv.htm](feats/class-02-3r4v6W4abJnLg7dv.htm)|Alter Ego Dedication|Dévouement : Imposteur|libre|
|[class-02-3SGS0chf3SKosG5H.htm](feats/class-02-3SGS0chf3SKosG5H.htm)|Shieldmarshal Dedication|Dévouement : Marshal protecteur|libre|
|[class-02-4bvoePh1p3ZGgqhP.htm](feats/class-02-4bvoePh1p3ZGgqhP.htm)|Second Wind|Second souffle|officielle|
|[class-02-4f65nrWVzTG8Njj8.htm](feats/class-02-4f65nrWVzTG8Njj8.htm)|Unbalancing Blow|Coup déséquilibrant|officielle|
|[class-02-4HZTLPKPteEFsa7n.htm](feats/class-02-4HZTLPKPteEFsa7n.htm)|Esoteric Polymath|Touche-à-tout ésotérique|officielle|
|[class-02-4M5zIlJ8EvjQDtg9.htm](feats/class-02-4M5zIlJ8EvjQDtg9.htm)|Dandy Dedication|Dévouement : Dandy|libre|
|[class-02-4MUbwilvb9dI0X59.htm](feats/class-02-4MUbwilvb9dI0X59.htm)|Snarecrafter Dedication|Dévouement : Fabricant de pièges artisanaux|libre|
|[class-02-4QFElZoWjg1X0vsg.htm](feats/class-02-4QFElZoWjg1X0vsg.htm)|Archer Dedication|Dévouement : Archer|libre|
|[class-02-4zaU3GlTGMNqLFS8.htm](feats/class-02-4zaU3GlTGMNqLFS8.htm)|Familiar Master Dedication|Dévouement : Maître familier|libre|
|[class-02-52nzskpmUt9AjUf8.htm](feats/class-02-52nzskpmUt9AjUf8.htm)|Ghost Dedication|Dévouement : Fantôme|libre|
|[class-02-54BhwBHyp2jp3e26.htm](feats/class-02-54BhwBHyp2jp3e26.htm)|Wild Empathy|Empathie sauvage|officielle|
|[class-02-57K9il2Jxs8PmsYL.htm](feats/class-02-57K9il2Jxs8PmsYL.htm)|Nantambu Chime-Ringer Dedication|Dévouement : Sonneur de carillon de Nantambu|libre|
|[class-02-5Cm2TYHzoO2DbQNM.htm](feats/class-02-5Cm2TYHzoO2DbQNM.htm)|Reanimator Dedication|Dévouement : Réanimateur|libre|
|[class-02-5CRt5Dy9eLv5LpRF.htm](feats/class-02-5CRt5Dy9eLv5LpRF.htm)|Herbalist Dedication|Dévouement : Herboriste|libre|
|[class-02-5FOqVC9Q0eEKEf3w.htm](feats/class-02-5FOqVC9Q0eEKEf3w.htm)|Magic Warrior Dedication|Dévouement : Guerrier Magique|officielle|
|[class-02-5gnBhockV7O32jTR.htm](feats/class-02-5gnBhockV7O32jTR.htm)|Furious Finish|Final furieux|officielle|
|[class-02-5npovgLMUlvtot2J.htm](feats/class-02-5npovgLMUlvtot2J.htm)|Wrestler Dedication|Dévouement : Lutteur|libre|
|[class-02-5Q69PI8jdQVkb1ZT.htm](feats/class-02-5Q69PI8jdQVkb1ZT.htm)|Cavalier Dedication|Dévouement : Cavalier|libre|
|[class-02-5SBFayX7JqKYANwa.htm](feats/class-02-5SBFayX7JqKYANwa.htm)|Rebounding Toss|Lancer rebondissant|libre|
|[class-02-5UQagWB13Z8xR5Z6.htm](feats/class-02-5UQagWB13Z8xR5Z6.htm)|Sorcerer Dedication|Dévouement : Ensorceleur|officielle|
|[class-02-63L2iSAxkHyq6qEt.htm](feats/class-02-63L2iSAxkHyq6qEt.htm)|Hallowed Necromancer Dedication|Dévouement : Nécromant sacré|libre|
|[class-02-6b8DiNx4c1zPZ7RP.htm](feats/class-02-6b8DiNx4c1zPZ7RP.htm)|Edgewatch Detective Dedication|Dévouement : Officier de la Garde du Précipice|libre|
|[class-02-6cQSPqXoAO6oJl0i.htm](feats/class-02-6cQSPqXoAO6oJl0i.htm)|Shooting Stars Stance|Posture des étoiles lancées|libre|
|[class-02-6pDM5AjDpoXFwzXH.htm](feats/class-02-6pDM5AjDpoXFwzXH.htm)|Stalwart Defender Dedication|Dévouement : Fidèle défenseur|libre|
|[class-02-6PP57Aa5HmSr0qIZ.htm](feats/class-02-6PP57Aa5HmSr0qIZ.htm)|Sniping Duo Dedication|Dévouement : Duo de tireur d'élite|libre|
|[class-02-6QOcQ8ooP1vjQACX.htm](feats/class-02-6QOcQ8ooP1vjQACX.htm)|Nonlethal Spell|Sort non létal|libre|
|[class-02-7RFjTTzznsdPcaYB.htm](feats/class-02-7RFjTTzznsdPcaYB.htm)|Emblazon Armament|Arsenal blasonné|officielle|
|[class-02-80CEAB05TP5ki9iW.htm](feats/class-02-80CEAB05TP5ki9iW.htm)|Fane's Fourberie|Fourberie de Fane|libre|
|[class-02-84n92oBsRVCxjifM.htm](feats/class-02-84n92oBsRVCxjifM.htm)|Expansive Spellstrike|Frappe de sort étendue|libre|
|[class-02-85eUthbjE9TnF4fo.htm](feats/class-02-85eUthbjE9TnF4fo.htm)|Dragonslayer Oath (Redeemer)|Serment du pourfendeur de dragons (Rédempteur)|libre|
|[class-02-8bMmGBO6gsMfWZk3.htm](feats/class-02-8bMmGBO6gsMfWZk3.htm)|Stunning Fist|Poing étourdissant|officielle|
|[class-02-8foxmfC6FFT3oYpV.htm](feats/class-02-8foxmfC6FFT3oYpV.htm)|Sentinel Dedication|Dévouement : Sentinelle|libre|
|[class-02-8qawcaQAZMD7pC6Y.htm](feats/class-02-8qawcaQAZMD7pC6Y.htm)|Alkenstar Agent Dedication|Dévouement : Agent d'Alkenastre|libre|
|[class-02-9Jbl71C5C6MnOqxV.htm](feats/class-02-9Jbl71C5C6MnOqxV.htm)|Consume Energy|Consommer l'énergie (Déviant)|libre|
|[class-02-9onf3uGvZRnNLPR6.htm](feats/class-02-9onf3uGvZRnNLPR6.htm)|Order Explorer|Explorateur des ordres|officielle|
|[class-02-9WzZc110jCNnjjRz.htm](feats/class-02-9WzZc110jCNnjjRz.htm)|Lightslayer Oath|Serment de tueur de lumière|libre|
|[class-02-A2EBy2L4acrehiAA.htm](feats/class-02-A2EBy2L4acrehiAA.htm)|Pistol Twirl|Moulinet au pistolet|libre|
|[class-02-A4sV0cRU9I8ztbHY.htm](feats/class-02-A4sV0cRU9I8ztbHY.htm)|Bashing Charge|Charge destructrice|libre|
|[class-02-A7ofsoPva0UtjqrX.htm](feats/class-02-A7ofsoPva0UtjqrX.htm)|Cantrip Expansion (Prepared Caster)|Expansion de tour de magie (Incantateur préparé)|libre|
|[class-02-a898miJnjgD93ZsX.htm](feats/class-02-a898miJnjgD93ZsX.htm)|Multifarious Muse|Muse polyvalente|officielle|
|[class-02-aFygWxgSv82WyCsl.htm](feats/class-02-aFygWxgSv82WyCsl.htm)|Acrobat Dedication|Dévouement : Acrobate|libre|
|[class-02-AimSmPyiMhJZVZRq.htm](feats/class-02-AimSmPyiMhJZVZRq.htm)|Mauler Dedication|Dévouement : Cogneur|libre|
|[class-02-auv1lss6LxM0q3gz.htm](feats/class-02-auv1lss6LxM0q3gz.htm)|Shake it Off|Reprends-toi|officielle|
|[class-02-aXdY2wgn0ItWwTr0.htm](feats/class-02-aXdY2wgn0ItWwTr0.htm)|Conceited Mindset|Esprit vaniteux|libre|
|[class-02-bCWieNDC1CD35tin.htm](feats/class-02-bCWieNDC1CD35tin.htm)|Rogue Dedication|Dévouement : Roublard|officielle|
|[class-02-BmAk6o14CutgnIOG.htm](feats/class-02-BmAk6o14CutgnIOG.htm)|Risky Reload|Rechargement risqué|libre|
|[class-02-bPMYOiiqhrb098s6.htm](feats/class-02-bPMYOiiqhrb098s6.htm)|Call Implement|Appeler l'implément|libre|
|[class-02-bRftzbFvSF1pilIo.htm](feats/class-02-bRftzbFvSF1pilIo.htm)|Anoint Ally|Oindre un allié|libre|
|[class-02-BU4NBIBkVZxdWLLH.htm](feats/class-02-BU4NBIBkVZxdWLLH.htm)|Elemental Fist|Poing élémentaire|officielle|
|[class-02-BVxcuJ5TWfN1px4L.htm](feats/class-02-BVxcuJ5TWfN1px4L.htm)|Pactbound Dedication|Dévouement : Lié par un pacte|libre|
|[class-02-BwDIwjHasZwcd61Z.htm](feats/class-02-BwDIwjHasZwcd61Z.htm)|Spellshot Dedication|Dévouement : Sortiléro|libre|
|[class-02-bWYnjNa2RknMoibg.htm](feats/class-02-bWYnjNa2RknMoibg.htm)|Lastwall Sentry Dedication|Dévouement : Sentinelle de Dernier-Rempart|officielle|
|[class-02-c2h9Z8exSFhraJ8j.htm](feats/class-02-c2h9Z8exSFhraJ8j.htm)|Esoteric Oath (Paladin)|Serment ésotérique (Paladin)|libre|
|[class-02-C3ukUk4Js10o5IBA.htm](feats/class-02-C3ukUk4Js10o5IBA.htm)|Ghostly Grasp (Deviant)|Poigne fantomatique (Déviant)|libre|
|[class-02-c4ao7b2T92fiM8jR.htm](feats/class-02-c4ao7b2T92fiM8jR.htm)|Living Monolith Dedication|Dévouement : Monolithe vivant|officielle|
|[class-02-CaeCSWFWytWv8Fgu.htm](feats/class-02-CaeCSWFWytWv8Fgu.htm)|Scrounger Dedication|Dévouement : Bricoleur|libre|
|[class-02-CbqaiI0TKsyDywDr.htm](feats/class-02-CbqaiI0TKsyDywDr.htm)|Spell Trickster Dedication|Dévouement : Mystificateur de sort|libre|
|[class-02-CcQISNjNdD0Fsj9n.htm](feats/class-02-CcQISNjNdD0Fsj9n.htm)|Shining Oath (Paladin)|Serment de lumière (Paladin)|officielle|
|[class-02-CJMkxlxHiHZQYDCz.htm](feats/class-02-CJMkxlxHiHZQYDCz.htm)|Alchemist Dedication|Dévouement : Alchimiste|officielle|
|[class-02-corjPSTUKB02gkqN.htm](feats/class-02-corjPSTUKB02gkqN.htm)|Defensive Armaments|Arsenal défensif|libre|
|[class-02-CowQy8gXP9POjuxq.htm](feats/class-02-CowQy8gXP9POjuxq.htm)|Game Hunter Dedication|Dévouement : Chasseur de gibier|libre|
|[class-02-CrS0iDwHmjKqvKti.htm](feats/class-02-CrS0iDwHmjKqvKti.htm)|Flexible Spellcaster Dedication|Dévouement : Incantateur flexible|libre|
|[class-02-cT18roxM5P5MWDRT.htm](feats/class-02-cT18roxM5P5MWDRT.htm)|Reliable Squire|Écuyer fiable|libre|
|[class-02-cy9jqlHz75GTjg7l.htm](feats/class-02-cy9jqlHz75GTjg7l.htm)|Bone Spikes|Pointes d'os (Déviant)|libre|
|[class-02-CZXhJS55rG5H6PpB.htm](feats/class-02-CZXhJS55rG5H6PpB.htm)|Investigator Dedication|Dévouement : Enquêteur|libre|
|[class-02-D1o7GUraoFFzjaub.htm](feats/class-02-D1o7GUraoFFzjaub.htm)|Clever Gambit|Manoeuvre intelligente|libre|
|[class-02-dF1HR6TqyMz03o6F.htm](feats/class-02-dF1HR6TqyMz03o6F.htm)|Combat Grab|Empoignade en combat|officielle|
|[class-02-DFusBl7CyNkuDTRa.htm](feats/class-02-DFusBl7CyNkuDTRa.htm)|Collapse Armor|Replier l'armure|libre|
|[class-02-dgmjiToPHC3Yf5I3.htm](feats/class-02-dgmjiToPHC3Yf5I3.htm)|Acute Scent|Odorat aiguisé|officielle|
|[class-02-dIH771mt4PcVTyAs.htm](feats/class-02-dIH771mt4PcVTyAs.htm)|Bard Dedication|Dévouement : Barde|libre|
|[class-02-DkoxNw9tsFFXrfJY.htm](feats/class-02-DkoxNw9tsFFXrfJY.htm)|Charmed Life|Vie chanceuse|libre|
|[class-02-dkuY22d3yLUBcqhq.htm](feats/class-02-dkuY22d3yLUBcqhq.htm)|Cathartic Mage Dedication|Dévouement : Mage cathartique|libre|
|[class-02-Dm0YMEvSY0qg0jA0.htm](feats/class-02-Dm0YMEvSY0qg0jA0.htm)|Gladiator Dedication|Dévouement : Gladiateur|libre|
|[class-02-DxcjEcxTbjlOA2C0.htm](feats/class-02-DxcjEcxTbjlOA2C0.htm)|Reinforce Eidolon|Renforcer l'eidolon|libre|
|[class-02-DY6pNO3GzHeKSxmQ.htm](feats/class-02-DY6pNO3GzHeKSxmQ.htm)|Finishing Follow-through|Suivi de la botte|libre|
|[class-02-DYUlMxh7TDgEh9xB.htm](feats/class-02-DYUlMxh7TDgEh9xB.htm)|Brutal Beating|Raclée brutale|officielle|
|[class-02-E8SDesgMD6Zbye2j.htm](feats/class-02-E8SDesgMD6Zbye2j.htm)|Geomancer Dedication|Dévouement : Géomancien|libre|
|[class-02-e9NcDk6ds5YebGvb.htm](feats/class-02-e9NcDk6ds5YebGvb.htm)|Relentless Stalker|Harceleur infatigable|libre|
|[class-02-EaIczkGVI5DUo3c9.htm](feats/class-02-EaIczkGVI5DUo3c9.htm)|Entreat With Forebears|Implorer les ancêtres|libre|
|[class-02-eAlrvPVb8qt8Lruw.htm](feats/class-02-eAlrvPVb8qt8Lruw.htm)|Swashbuckler Dedication|Dévouement : Bretteur|libre|
|[class-02-eBdajOzs8kiJDic2.htm](feats/class-02-eBdajOzs8kiJDic2.htm)|Blessed One Dedication|Dévouement : Élu divin|libre|
|[class-02-eCzIiTjI4mQFYe9D.htm](feats/class-02-eCzIiTjI4mQFYe9D.htm)|Steed Form|Forme de monture|libre|
|[class-02-eHjqNXgylSuvA887.htm](feats/class-02-eHjqNXgylSuvA887.htm)|Student of Perfection Dedication|Dévouement : Étudiant de la perfection|officielle|
|[class-02-EHJRMCBRL4YeBN6l.htm](feats/class-02-EHJRMCBRL4YeBN6l.htm)|Ghoul Dedication|Dévouement : Goule|libre|
|[class-02-eJPu2Sj7XYCM0h0R.htm](feats/class-02-eJPu2Sj7XYCM0h0R.htm)|Warp Space|Déformer l'espace|libre|
|[class-02-eoDDjIGAA67Z7rQt.htm](feats/class-02-eoDDjIGAA67Z7rQt.htm)|Underhanded Assault|Assaut sournois|libre|
|[class-02-eUXgY8W3fShlW7pd.htm](feats/class-02-eUXgY8W3fShlW7pd.htm)|Inspire Competence|Inspiration talentueuse|officielle|
|[class-02-fdSbB0EQVxFgLoEd.htm](feats/class-02-fdSbB0EQVxFgLoEd.htm)|Sterling Dynamo Dedication|Dévouement : Dynamo sterling|libre|
|[class-02-FkN9QX1W2Iv56bkn.htm](feats/class-02-FkN9QX1W2Iv56bkn.htm)|Song Of Strength|Chanson de force|libre|
|[class-02-fL5lO5Odp7iJPkan.htm](feats/class-02-fL5lO5Odp7iJPkan.htm)|Vehicle Mechanic Dedication|Dévouement : Mécanicien de véhicule|libre|
|[class-02-FrlhErsjR6fEn6kX.htm](feats/class-02-FrlhErsjR6fEn6kX.htm)|Minor Magic|Magie mineure|officielle|
|[class-02-Fs9ZMHZNCBo7B5Zc.htm](feats/class-02-Fs9ZMHZNCBo7B5Zc.htm)|Vengeful Oath|Serment de vengeance|officielle|
|[class-02-fU7d5P6WrfAirgip.htm](feats/class-02-fU7d5P6WrfAirgip.htm)|Tumble Behind (Swashbuckler)|Passer derrière (Bretteur)|libre|
|[class-02-GFtNQvpzuqtsdOTG.htm](feats/class-02-GFtNQvpzuqtsdOTG.htm)|Hunter's Aim|Visée du chasseur|officielle|
|[class-02-gKoNWXem1ikEqE2d.htm](feats/class-02-gKoNWXem1ikEqE2d.htm)|Familiar's Language|Langue du familier|libre|
|[class-02-gQAQRHxpFKEkNQFs.htm](feats/class-02-gQAQRHxpFKEkNQFs.htm)|Thaumaturge Dedication|Dévouement : Thaumaturge|libre|
|[class-02-gt9pvIicabSOe6pB.htm](feats/class-02-gt9pvIicabSOe6pB.htm)|Brutish Shove|Poussée brutale|officielle|
|[class-02-hGjmbTNBUiJqYvsE.htm](feats/class-02-hGjmbTNBUiJqYvsE.htm)|Crushing Grab|Poigne écrasante|officielle|
|[class-02-hLYoIjH8gPEVXyWG.htm](feats/class-02-hLYoIjH8gPEVXyWG.htm)|Distracting Feint|Feinte de diversion|officielle|
|[class-02-I2DGiMcof703Cnjc.htm](feats/class-02-I2DGiMcof703Cnjc.htm)|Cantrip Casting|Incantation de tour de magie|libre|
|[class-02-I5hzE0XT2BsjatS3.htm](feats/class-02-I5hzE0XT2BsjatS3.htm)|Vampire Dedication|Dévouement : Vampire|libre|
|[class-02-iBmqKjsq4iTtoqvl.htm](feats/class-02-iBmqKjsq4iTtoqvl.htm)|Assassin Dedication|Dévouement : Assassin|libre|
|[class-02-iS4Vc2zv7vgL5mnX.htm](feats/class-02-iS4Vc2zv7vgL5mnX.htm)|Energy Ablation|Emprunt énergétique|libre|
|[class-02-IUmN2WC55LxPNSBB.htm](feats/class-02-IUmN2WC55LxPNSBB.htm)|Mummy Dedication|Dévouement : Momie|libre|
|[class-02-j1hhTLOq7o80XCyV.htm](feats/class-02-j1hhTLOq7o80XCyV.htm)|Bullet Dancer Dedication|Dévouement : Danseur de balle|libre|
|[class-02-j2XKVhDuhdcI15kD.htm](feats/class-02-j2XKVhDuhdcI15kD.htm)|First Frost|Première gelée|libre|
|[class-02-J3R0vx1lszU3CLp5.htm](feats/class-02-J3R0vx1lszU3CLp5.htm)|Fiendsbane Oath (Redeemer)|Serment du fléau des fiélons (Rédempteur)|libre|
|[class-02-JIpjpZ8VneQomkZw.htm](feats/class-02-JIpjpZ8VneQomkZw.htm)|Inventor Dedication|Dévouement : Inventeur|libre|
|[class-02-jm8kchZRznjxRy0C.htm](feats/class-02-jm8kchZRznjxRy0C.htm)|Clockwork Reanimator Dedication|Dévouement : Réanimateur nécromécanicien|libre|
|[class-02-jPWF8Xe0UqVvbeyv.htm](feats/class-02-jPWF8Xe0UqVvbeyv.htm)|Eerie Flicker|Étrange scintillement (Déviant)|libre|
|[class-02-JuEUNrvsxOYdXeYL.htm](feats/class-02-JuEUNrvsxOYdXeYL.htm)|Viking Dedication|Dévouement : Viking|libre|
|[class-02-juikoiIA0Jy8PboY.htm](feats/class-02-juikoiIA0Jy8PboY.htm)|Mind Smith Dedication|Dévouement : Façonneur spirituel|libre|
|[class-02-K3QkcNWY8qpNEJrk.htm](feats/class-02-K3QkcNWY8qpNEJrk.htm)|Dragonslayer Oath (Liberator)|Serment du pourfendeur de dragons (Libérateur)|libre|
|[class-02-KCwXj3y7Nm4e3NbI.htm](feats/class-02-KCwXj3y7Nm4e3NbI.htm)|Demolition Charge|Explosion de démolition|libre|
|[class-02-l1eCHNahsBb7rUkT.htm](feats/class-02-l1eCHNahsBb7rUkT.htm)|Pirate Dedication|Dévouement : Pirate|libre|
|[class-02-lDfOzhKJoTCkLPtn.htm](feats/class-02-lDfOzhKJoTCkLPtn.htm)|Shining Oath (Liberator)|Serment de lumière (Libérateur)|officielle|
|[class-02-lIrPwGpJk9TldZ4c.htm](feats/class-02-lIrPwGpJk9TldZ4c.htm)|Snare Hopping|Piège artisanal bondissant|libre|
|[class-02-LJzk4Xxxs9IlbWz6.htm](feats/class-02-LJzk4Xxxs9IlbWz6.htm)|Curse Maelstrom Dedication|Dévouement : Maelström maudit|libre|
|[class-02-lkcM4V3VDAtjlR9P.htm](feats/class-02-lkcM4V3VDAtjlR9P.htm)|Intimidating Strike|Frappe intimidante|officielle|
|[class-02-LoeoiYOpxSaEkWKv.htm](feats/class-02-LoeoiYOpxSaEkWKv.htm)|Staff Acrobat Dedication|Dévouement : Funambule|libre|
|[class-02-lotpVdFhxNsNP0Ru.htm](feats/class-02-lotpVdFhxNsNP0Ru.htm)|Unexpected Sharpshooter Dedication|Dévouement : Tireur d'élite inattendu|libre|
|[class-02-lslR2UfP3ze7TFYu.htm](feats/class-02-lslR2UfP3ze7TFYu.htm)|Eldritch Researcher Dedication|Dévouement : Chercheur mystique|libre|
|[class-02-lT8XlX1Ig900BblS.htm](feats/class-02-lT8XlX1Ig900BblS.htm)|No Escape|Pas d'échappatoire|officielle|
|[class-02-luKAFJAvdbAgEwV7.htm](feats/class-02-luKAFJAvdbAgEwV7.htm)|Runescarred Dedication|Dévouement : Scarifié de runes|officielle|
|[class-02-lyD6eXl4wc7Pq61q.htm](feats/class-02-lyD6eXl4wc7Pq61q.htm)|Loremaster Dedication|Dévouement : Maître savant|libre|
|[class-02-LZsTt6zPZfNyjIZl.htm](feats/class-02-LZsTt6zPZfNyjIZl.htm)|Dragonslayer Oath (Paladin)|Serment du pourfendeur de dragons (Paladin)|libre|
|[class-02-M0D2CQgASNfdZBrl.htm](feats/class-02-M0D2CQgASNfdZBrl.htm)|Demolitionist Dedication|Dévouement : Démolisseur|libre|
|[class-02-m3ANSHYfBrFyFUvo.htm](feats/class-02-m3ANSHYfBrFyFUvo.htm)|Animal Trainer Dedication|Dévouement : Dompteur|libre|
|[class-02-M6cWLywN0aWnX5Gl.htm](feats/class-02-M6cWLywN0aWnX5Gl.htm)|Undead Master Dedication|Dévouement : Maître des morts-vivants|libre|
|[class-02-MJg24e9fJd7OASvF.htm](feats/class-02-MJg24e9fJd7OASvF.htm)|Medic Dedication|Dévouement : Médecin|libre|
|[class-02-mkp6lhBbTASEmKwY.htm](feats/class-02-mkp6lhBbTASEmKwY.htm)|Ghost Hunter Dedication|Dévouement : Chasseur de fantômes|libre|
|[class-02-MNArjo5Z5LUYITQm.htm](feats/class-02-MNArjo5Z5LUYITQm.htm)|Celebrity Dedication|Dévouement : Célébrité|libre|
|[class-02-MndcBMz6I7e6SRqx.htm](feats/class-02-MndcBMz6I7e6SRqx.htm)|Weapon Improviser Dedication|Dévouement : Improvisateur d'arme|libre|
|[class-02-mNsNl6w3l5rXx8dL.htm](feats/class-02-mNsNl6w3l5rXx8dL.htm)|Juggler Dedication|Dévouement : Jongleur|libre|
|[class-02-mrxAX7h5ya9Z7QV9.htm](feats/class-02-mrxAX7h5ya9Z7QV9.htm)|Oatia Skysage Dedication|Dévouement : Sagecéleste d'Oatie|libre|
|[class-02-MVbNnjqQOK9d8Ki3.htm](feats/class-02-MVbNnjqQOK9d8Ki3.htm)|Firework Technician Dedication|Dévouement : Pyrotechnicien|libre|
|[class-02-mvNa9KfQooHYEXoA.htm](feats/class-02-mvNa9KfQooHYEXoA.htm)|Fighter Dedication|Dévouement : Guerrier|officielle|
|[class-02-MYzRfNExDYp25rro.htm](feats/class-02-MYzRfNExDYp25rro.htm)|Marshal Dedication|Dévouement : Capitaine|libre|
|[class-02-mz2x4HFrWT8usbEL.htm](feats/class-02-mz2x4HFrWT8usbEL.htm)|Runelord Dedication|Dévouement : Seigneur des runes|libre|
|[class-02-mzpLIuRQ81DCYdKU.htm](feats/class-02-mzpLIuRQ81DCYdKU.htm)|Force Fang|Crocs de force|libre|
|[class-02-N4wCUZH2KG6FoGqh.htm](feats/class-02-N4wCUZH2KG6FoGqh.htm)|Shared Stratagem|Stratagème partagé|libre|
|[class-02-N7dTFxpjXGn4ddq8.htm](feats/class-02-N7dTFxpjXGn4ddq8.htm)|Enhanced Familiar|Familier amélioré|officielle|
|[class-02-n7nQQR940OvFbw7T.htm](feats/class-02-n7nQQR940OvFbw7T.htm)|Dueling Parry (Fighter)|Parade en duel (Guerrier)|libre|
|[class-02-Na1qfHd6AFAXoN1A.htm](feats/class-02-Na1qfHd6AFAXoN1A.htm)|Reverse Engineer|Ingénierie inversée|libre|
|[class-02-ncrNQcwm4gOQRAA3.htm](feats/class-02-ncrNQcwm4gOQRAA3.htm)|Shadowcaster Dedication|Dévouement : Incantateur de l'ombre|libre|
|[class-02-NwMiszlcqNZWtezq.htm](feats/class-02-NwMiszlcqNZWtezq.htm)|Drow Shootist Dedication|Dévouement : Tireur drow|libre|
|[class-02-obGGzuPcgO5Xiz6P.htm](feats/class-02-obGGzuPcgO5Xiz6P.htm)|Searing Restoration|Guérison explosive|libre|
|[class-02-OcBaEnGdDm6CuSnr.htm](feats/class-02-OcBaEnGdDm6CuSnr.htm)|Rapid Response|Réponse rapide|libre|
|[class-02-ODnXQHvFoK7tIVpu.htm](feats/class-02-ODnXQHvFoK7tIVpu.htm)|Smoke Bomb|Bombe fumigène|officielle|
|[class-02-OiY0L3WvjwlQQ4iG.htm](feats/class-02-OiY0L3WvjwlQQ4iG.htm)|Strong Arm|Bras puissant|libre|
|[class-02-oo34CloLefFRi72w.htm](feats/class-02-oo34CloLefFRi72w.htm)|Dragon Disciple Dedication|Dévouement : Disciple draconique|libre|
|[class-02-oQ6EZRFXbHWCx08C.htm](feats/class-02-oQ6EZRFXbHWCx08C.htm)|Wizard Dedication|Dévouement : Magicien|libre|
|[class-02-ot0uyFtnC1Whz5bp.htm](feats/class-02-ot0uyFtnC1Whz5bp.htm)|Directed Audience|Public choisi|libre|
|[class-02-OUNj8nXTHwGcEdlh.htm](feats/class-02-OUNj8nXTHwGcEdlh.htm)|Oracle Dedication|Dévouement : Oracle|libre|
|[class-02-oVSlTmdmho8ZQo2k.htm](feats/class-02-oVSlTmdmho8ZQo2k.htm)|Ranged Combatant|Combattant à distance|libre|
|[class-02-OW3W2vMARiojda7e.htm](feats/class-02-OW3W2vMARiojda7e.htm)|Psychic Dedication|Dévouement : Psychiste|libre|
|[class-02-OWhTAowdMvBZnCrT.htm](feats/class-02-OWhTAowdMvBZnCrT.htm)|Dancing Leaf|Feuille dansante|officielle|
|[class-02-P6cTyDMTXcC8HBDr.htm](feats/class-02-P6cTyDMTXcC8HBDr.htm)|Esoteric Warden|Gardien ésotérique|libre|
|[class-02-pEFcWRYiWLSjxvkW.htm](feats/class-02-pEFcWRYiWLSjxvkW.htm)|Mental Balm|Baume mental|libre|
|[class-02-PFMsj4LtdAIuGn4R.htm](feats/class-02-PFMsj4LtdAIuGn4R.htm)|Pistol Phenom Dedication|Dévouement : Phénomène du pistolet|libre|
|[class-02-PGyzFBwuTgypU8cD.htm](feats/class-02-PGyzFBwuTgypU8cD.htm)|Turn Undead|Renvoi des morts-vivants|officielle|
|[class-02-pI97a5xSg4LbBY1g.htm](feats/class-02-pI97a5xSg4LbBY1g.htm)|Aldori Duelist Dedication|Dévouement : Duelliste Aldori|officielle|
|[class-02-pPA2YF6Dal0PClWA.htm](feats/class-02-pPA2YF6Dal0PClWA.htm)|Undead Slayer Dedication|Dévouement : Tueur de mort-vivant|libre|
|[class-02-PRKe5rWYZMZgEpFU.htm](feats/class-02-PRKe5rWYZMZgEpFU.htm)|Archaeologist Dedication|Dévouement : Archéologue|libre|
|[class-02-pZMBq7gn66SEEA0n.htm](feats/class-02-pZMBq7gn66SEEA0n.htm)|Horizon Walker Dedication|Dévouement : Arpenteur d'horizon|libre|
|[class-02-QFFbmQ5yrBSjbAj3.htm](feats/class-02-QFFbmQ5yrBSjbAj3.htm)|Esoteric Oath (Redeemer)|Serment ésotérique (Rédempteur)|libre|
|[class-02-qFt6zyWVX1njJf1l.htm](feats/class-02-qFt6zyWVX1njJf1l.htm)|Quick Draw|Arme en main|officielle|
|[class-02-qhs0mWbaLKsdckZX.htm](feats/class-02-qhs0mWbaLKsdckZX.htm)|Hellknight Armiger Dedication|Dévouement : Écuyer des Chevaliers infernaux|officielle|
|[class-02-QkP007ESTWtw7UQG.htm](feats/class-02-QkP007ESTWtw7UQG.htm)|Cantrip Expansion (Spontaneous Caster)|Expansion de tour de magie (Incantateur spontané)|libre|
|[class-02-qMa2fIP2nqrFzHrq.htm](feats/class-02-qMa2fIP2nqrFzHrq.htm)|Scout Dedication|Dévouement : Éclaireur|libre|
|[class-02-qmFWCHOuubEl7VpX.htm](feats/class-02-qmFWCHOuubEl7VpX.htm)|Domain Acumen|Sagacité du domaine|libre|
|[class-02-QN7y2CQeiJ2iPioM.htm](feats/class-02-QN7y2CQeiJ2iPioM.htm)|Titan Swing|Coup titanesque (Déviant)|libre|
|[class-02-QnB5ArlO4wlRXQ2E.htm](feats/class-02-QnB5ArlO4wlRXQ2E.htm)|Pathfinder Agent Dedication|Dévouement : Agent des Éclaireurs|libre|
|[class-02-R00qiDE5pBctgtyU.htm](feats/class-02-R00qiDE5pBctgtyU.htm)|Ranger Dedication|Dévouement : Rôdeur|officielle|
|[class-02-r1um0cn7MRIMLqcq.htm](feats/class-02-r1um0cn7MRIMLqcq.htm)|Ghost Eater Dedication|Dévouement : Mangeur de fantômes|libre|
|[class-02-r72qcTupGzyRDiGe.htm](feats/class-02-r72qcTupGzyRDiGe.htm)|Duelist Dedication|Dévouement : Duelliste|libre|
|[class-02-RAymYRO2SLNNzKVk.htm](feats/class-02-RAymYRO2SLNNzKVk.htm)|Mobility|Mobilité|officielle|
|[class-02-renUpSO6MJXPSXow.htm](feats/class-02-renUpSO6MJXPSXow.htm)|Monk Dedication|Dévouement : Moine|libre|
|[class-02-rJ1XxUEACaQA9Lyo.htm](feats/class-02-rJ1XxUEACaQA9Lyo.htm)|Bright Lion Dedication|Dévouement : Lion radieux|libre|
|[class-02-rTkr1EqpAN6YtnAh.htm](feats/class-02-rTkr1EqpAN6YtnAh.htm)|Lunge|Fente|officielle|
|[class-02-ryBj7phZqASQSEjV.htm](feats/class-02-ryBj7phZqASQSEjV.htm)|Psi Burst|Explosion psy|libre|
|[class-02-s6wXcQYmHbew14gC.htm](feats/class-02-s6wXcQYmHbew14gC.htm)|Bastion Dedication|Dévouement : Bastion|libre|
|[class-02-sG9fPQk4w9y6MmnY.htm](feats/class-02-sG9fPQk4w9y6MmnY.htm)|Folklorist Dedication|Dévouement : Folkloriste|libre|
|[class-02-sgo7J9BVofBqwlsF.htm](feats/class-02-sgo7J9BVofBqwlsF.htm)|Favored Terrain|Environnement de prédilection|officielle|
|[class-02-sIeuPW0j39fTZm08.htm](feats/class-02-sIeuPW0j39fTZm08.htm)|Conceal Spell|Sort dissimulé|officielle|
|[class-02-smCDaPlpRDA47xjK.htm](feats/class-02-smCDaPlpRDA47xjK.htm)|Cleric Dedication|Dévouement : Prêtre|officielle|
|[class-02-SNhhx0hPWlERpQRr.htm](feats/class-02-SNhhx0hPWlERpQRr.htm)|Corpse Tender Dedication|Dévouement : Éleveur de cadavres|libre|
|[class-02-SoocjFrWNOpchTVb.htm](feats/class-02-SoocjFrWNOpchTVb.htm)|Soulforger Dedication|Dévouement : Mentallurgiste|libre|
|[class-02-Stydu9VtrhQZFZxt.htm](feats/class-02-Stydu9VtrhQZFZxt.htm)|Fake Out|Leurrer|libre|
|[class-02-SwzPqEsLzZpNufvm.htm](feats/class-02-SwzPqEsLzZpNufvm.htm)|Summoner Dedication|Dévouement : Conjurateur|libre|
|[class-02-t03Tc5I4B8RHsyqs.htm](feats/class-02-t03Tc5I4B8RHsyqs.htm)|Overwatch Dedication|Dévouement : Observateur|libre|
|[class-02-t2Uvf6W8Z116DYvo.htm](feats/class-02-t2Uvf6W8Z116DYvo.htm)|Cantrip Expansion (Magus)|Expansion de tour de magie (magus)|libre|
|[class-02-tdtCwmYoBMKzfhEp.htm](feats/class-02-tdtCwmYoBMKzfhEp.htm)|Shining Oath (Redeemer)|Serment de lumière (Rédempteur)|officielle|
|[class-02-TiNlehXIDEnIl95M.htm](feats/class-02-TiNlehXIDEnIl95M.htm)|Sap Life|Saper la vie|officielle|
|[class-02-tJduF6N83l5khRow.htm](feats/class-02-tJduF6N83l5khRow.htm)|Divine Grace|Grâce divine|officielle|
|[class-02-tKaesXO5nlZ2sspx.htm](feats/class-02-tKaesXO5nlZ2sspx.htm)|Bounty Hunter Dedication|Dévouement : Chasseur de primes|libre|
|[class-02-TtAvM02UvfNaXeXd.htm](feats/class-02-TtAvM02UvfNaXeXd.htm)|Blasting Beams|Rayons explosifs (Déviant)|libre|
|[class-02-Tu1hOEr6Ko9Df54L.htm](feats/class-02-Tu1hOEr6Ko9Df54L.htm)|Athletic Strategist|Stratège athlétique|libre|
|[class-02-tx9pkrpmtqe4FnvS.htm](feats/class-02-tx9pkrpmtqe4FnvS.htm)|Elementalist Dedication|Dévouement : Élémentaliste|libre|
|[class-02-TxKWgxor49xntDlU.htm](feats/class-02-TxKWgxor49xntDlU.htm)|Magus Dedication|Dévouement : Magus|libre|
|[class-02-txWKzKuJus7UBebX.htm](feats/class-02-txWKzKuJus7UBebX.htm)|Artillerist Dedication|Dévouement : Artilleur|libre|
|[class-02-U6lS758rtYGR6aw9.htm](feats/class-02-U6lS758rtYGR6aw9.htm)|Amphibious Form|Forme amphibie|libre|
|[class-02-u6SDVWbzHnBBPNMo.htm](feats/class-02-u6SDVWbzHnBBPNMo.htm)|Linguist Dedication|Dévouement : Linguiste|libre|
|[class-02-uBr7wHtOS6KIhHnd.htm](feats/class-02-uBr7wHtOS6KIhHnd.htm)|Twilight Speaker Dedication|Dévouement : Orateur du crépuscule|libre|
|[class-02-uD8J9wAE3KB2w0Cf.htm](feats/class-02-uD8J9wAE3KB2w0Cf.htm)|Pactbinder Dedication|Dévouement : Pactiseur|libre|
|[class-02-UiQbjeqBUFjUtgUR.htm](feats/class-02-UiQbjeqBUFjUtgUR.htm)|Assisting Shot|Tir de soutien|libre|
|[class-02-uqIiSDbv80TbRwTQ.htm](feats/class-02-uqIiSDbv80TbRwTQ.htm)|Vigilante Dedication|Dévouement : Justicier|libre|
|[class-02-USt0jwK8XI5loG4E.htm](feats/class-02-USt0jwK8XI5loG4E.htm)|Gunslinger Dedication|Dévouement : Franc-tireur|libre|
|[class-02-UuPZ7drPBnSmI8Eo.htm](feats/class-02-UuPZ7drPBnSmI8Eo.htm)|Red Mantis Assassin Dedication|Dévouement : Assassin des Mantes rouges|officielle|
|[class-02-uv235v6hXCITAFej.htm](feats/class-02-uv235v6hXCITAFej.htm)|Fiendsbane Oath (Liberator)|Serment du fléau des fiélons (Libérateur)|libre|
|[class-02-uxHWqFbYD0ZvkeF8.htm](feats/class-02-uxHWqFbYD0ZvkeF8.htm)|Scroll Trickster Dedication|Dévouement : Usurpateur de parchemins|libre|
|[class-02-UyyrFtPWOo0qvXOv.htm](feats/class-02-UyyrFtPWOo0qvXOv.htm)|Zephyr Guard Dedication|Dévouement : Garde zéphyr|officielle|
|[class-02-Veaf8vm2M9w8bcBI.htm](feats/class-02-Veaf8vm2M9w8bcBI.htm)|Alacritous Action|Action empressée|libre|
|[class-02-VeVHWTrzE3aDm3rx.htm](feats/class-02-VeVHWTrzE3aDm3rx.htm)|Brawling Focus|Focalisation du lutteur|officielle|
|[class-02-vhw3n86TEs6laopA.htm](feats/class-02-vhw3n86TEs6laopA.htm)|Druid Dedication|Dévouement : Druide|officielle|
|[class-02-VQz5VypVRLCloapa.htm](feats/class-02-VQz5VypVRLCloapa.htm)|Ancestral Weaponry|Armement ancestral|libre|
|[class-02-VToVEOxiyy53AcEp.htm](feats/class-02-VToVEOxiyy53AcEp.htm)|Loremaster's Etude|Étude du maître savant|officielle|
|[class-02-VU7aZC7L08Mk1GVA.htm](feats/class-02-VU7aZC7L08Mk1GVA.htm)|Esoteric Oath (Liberator)|Serment ésotérique (Libérateur)|libre|
|[class-02-VXA50vhIRCBt4vvP.htm](feats/class-02-VXA50vhIRCBt4vvP.htm)|Unbalancing Finisher|Aboutissement déséquilibrant|libre|
|[class-02-wdmUH6hdat7jpEWt.htm](feats/class-02-wdmUH6hdat7jpEWt.htm)|Beastmaster Dedication|Dévouement : Maître des bêtes|libre|
|[class-02-WfbmFsRxbVyzMmCz.htm](feats/class-02-WfbmFsRxbVyzMmCz.htm)|Magical Understudy|Doublure magique|libre|
|[class-02-wKFQreilUASJkKzV.htm](feats/class-02-wKFQreilUASJkKzV.htm)|Collapse Construct|Replier la créature artificielle|libre|
|[class-02-wNr02jsG5nRF23YO.htm](feats/class-02-wNr02jsG5nRF23YO.htm)|Red Herring|Fausse piste|libre|
|[class-02-wsq8nncD25Q1fRn2.htm](feats/class-02-wsq8nncD25Q1fRn2.htm)|Basic Lesson|Leçon basique|libre|
|[class-02-WVU0c8rgcpGSRqSi.htm](feats/class-02-WVU0c8rgcpGSRqSi.htm)|Barbarian Dedication|Dévouement : Barbare|officielle|
|[class-02-Wx12NUjqTOjFrEoW.htm](feats/class-02-Wx12NUjqTOjFrEoW.htm)|Antagonize|Contrarier|libre|
|[class-02-WZWSaAwuDgne7Z0c.htm](feats/class-02-WZWSaAwuDgne7Z0c.htm)|Solid Lead|Piste solide|libre|
|[class-02-X9AkydrMdFwg7qIn.htm](feats/class-02-X9AkydrMdFwg7qIn.htm)|Spell Parry|Parade de sort|libre|
|[class-02-XHaxSBOaFMnBbBKt.htm](feats/class-02-XHaxSBOaFMnBbBKt.htm)|Living Hair|Cheveux vivants|libre|
|[class-02-xoIxiRtBVHV27Rvd.htm](feats/class-02-xoIxiRtBVHV27Rvd.htm)|Divine Aegis|Égide divine|libre|
|[class-02-XQEoKoFtq8n3wgA3.htm](feats/class-02-XQEoKoFtq8n3wgA3.htm)|Warning Shot|Tir de sommation|libre|
|[class-02-y0vdu6DGhKKElmE6.htm](feats/class-02-y0vdu6DGhKKElmE6.htm)|Witch Dedication|Dévouement : Sorcier|libre|
|[class-02-y7DDs03GtDnmhxFp.htm](feats/class-02-y7DDs03GtDnmhxFp.htm)|Poisoner Dedication|Dévouement : Empoisonneur|libre|
|[class-02-y8VecqdECqyH1h6o.htm](feats/class-02-y8VecqdECqyH1h6o.htm)|Magic Hide|Peau magique|libre|
|[class-02-yAgFDUU8HfVK4KTy.htm](feats/class-02-yAgFDUU8HfVK4KTy.htm)|Dragging Strike|Frappe entraînante|libre|
|[class-02-yExxOkHN1PN37hUa.htm](feats/class-02-yExxOkHN1PN37hUa.htm)|Communal Healing|Guérison collective|officielle|
|[class-02-ygCLN0brunmBYtJR.htm](feats/class-02-ygCLN0brunmBYtJR.htm)|Talisman Esoterica|Ésotéricas talismaniques|libre|
|[class-02-ygdbkfPPgSoWxaBa.htm](feats/class-02-ygdbkfPPgSoWxaBa.htm)|Chronoskimmer Dedication|Dévouement : Écumeur du temps|libre|
|[class-02-YhnCjiHNlS3nCeoC.htm](feats/class-02-YhnCjiHNlS3nCeoC.htm)|Dual-Weapon Warrior Dedication|Dévouement : Combattant à deux armes|libre|
|[class-02-yWawboNWFoJMVl0g.htm](feats/class-02-yWawboNWFoJMVl0g.htm)|Soul Warden Dedication|Dévouement : Gardien de l'âme|libre|
|[class-02-YwVdaszwpDJd6kf9.htm](feats/class-02-YwVdaszwpDJd6kf9.htm)|Devoted Guardian|Gardien dévoué|libre|
|[class-02-yYLVGhedXD7lFQMn.htm](feats/class-02-yYLVGhedXD7lFQMn.htm)|United Assault|Assaut uni|libre|
|[class-02-yZ7EcM9CLddZz8Hl.htm](feats/class-02-yZ7EcM9CLddZz8Hl.htm)|Versatile Font|Source polyvalente|officielle|
|[class-02-ZhToff996PnTESwb.htm](feats/class-02-ZhToff996PnTESwb.htm)|Magaambyan Attendant Dedication|Dévouement : Gardien du Magaambya|libre|
|[class-02-Ziky4XVV7syXVbUg.htm](feats/class-02-Ziky4XVV7syXVbUg.htm)|Powder Punch Stance|Posture du coup de poing à poudre|libre|
|[class-02-zn7arorE3VJLNYsb.htm](feats/class-02-zn7arorE3VJLNYsb.htm)|Martial Artist Dedication|Dévouement : Artiste martial|libre|
|[class-02-zNxbsYgQgPVxdTLV.htm](feats/class-02-zNxbsYgQgPVxdTLV.htm)|Lion Blade Dedication|Dévouement : Lame du Lion|officielle|
|[class-02-ZR5Buon23cDQ1ryB.htm](feats/class-02-ZR5Buon23cDQ1ryB.htm)|Zombie Dedication|Dévouement : Zombie|libre|
|[class-02-zV4vzcl7eoVewz5p.htm](feats/class-02-zV4vzcl7eoVewz5p.htm)|Ursine Avenger Form|Forme de vengeur ursin|libre|
|[class-02-ZvPgibovxwiN8Wse.htm](feats/class-02-ZvPgibovxwiN8Wse.htm)|Call of the Wild|Appel de la nature|officielle|
|[class-02-Zyb3RMGyhsKfTjEG.htm](feats/class-02-Zyb3RMGyhsKfTjEG.htm)|Wellspring Mage Dedication|Dévouement : Mage de la source|libre|
|[class-04-00OnDt8UEMwfoYWH.htm](feats/class-04-00OnDt8UEMwfoYWH.htm)|Ghost Wrangler|Dresseur de fantôme|libre|
|[class-04-011wnYpjIwCEzFtl.htm](feats/class-04-011wnYpjIwCEzFtl.htm)|Elemental Summons|Convocations élémentaires|libre|
|[class-04-0evTnx67DYsxWtg3.htm](feats/class-04-0evTnx67DYsxWtg3.htm)|Ice Crafter|Façonneur de glace|libre|
|[class-04-0FNLI8APwj9NsBDa.htm](feats/class-04-0FNLI8APwj9NsBDa.htm)|Alchemical Discoveries|Découvertes alchimiques|libre|
|[class-04-0haS0qXR9xTYKoTG.htm](feats/class-04-0haS0qXR9xTYKoTG.htm)|Snare Specialist|Spécialiste des pièges artisanaux|officielle|
|[class-04-0IVnIC6gQUdQyM8b.htm](feats/class-04-0IVnIC6gQUdQyM8b.htm)|Rebel's Map|Carte du rebelle|libre|
|[class-04-0ptK0blZehF3ABha.htm](feats/class-04-0ptK0blZehF3ABha.htm)|Tut-Tut|Tut-Tut|libre|
|[class-04-0VsrXyQYdluGRfsY.htm](feats/class-04-0VsrXyQYdluGRfsY.htm)|Pact of Fey Glamour|Pacte d'aspect féerique|libre|
|[class-04-13bMIkqXpKt9pxAm.htm](feats/class-04-13bMIkqXpKt9pxAm.htm)|You're an Embarrassment|Tu es gênant !|libre|
|[class-04-16QBDinolgG2M66d.htm](feats/class-04-16QBDinolgG2M66d.htm)|Spellstriker|Frappeur de sort|libre|
|[class-04-1fBHZpM3Z3MQtzvi.htm](feats/class-04-1fBHZpM3Z3MQtzvi.htm)|Doctor's Visitation|Tournée du médecin|libre|
|[class-04-1FnZhf5UwSb7Lo3t.htm](feats/class-04-1FnZhf5UwSb7Lo3t.htm)|Entity's Strike|Frappe de l'entité|libre|
|[class-04-1HvSkbUjqrIMXLiY.htm](feats/class-04-1HvSkbUjqrIMXLiY.htm)|Hijack Undead|Détournement de mort-vivant|libre|
|[class-04-1j9QCzHHxi5MbIY3.htm](feats/class-04-1j9QCzHHxi5MbIY3.htm)|Big Game Trapper|Piégeur de gros gibier|libre|
|[class-04-1VrV24hhPSEvuKgT.htm](feats/class-04-1VrV24hhPSEvuKgT.htm)|Basic Shooting|Tir basique|libre|
|[class-04-1wTXeBrYU6BVEEOn.htm](feats/class-04-1wTXeBrYU6BVEEOn.htm)|Stage Fighting|Combat chorégraphié|libre|
|[class-04-2pglnWX8q5p3XcqR.htm](feats/class-04-2pglnWX8q5p3XcqR.htm)|Syu Tak-nwa's Skillful Tresses|Tresses agiles de Syu Tak-nwa|libre|
|[class-04-2qR4QAgJVArv63Z2.htm](feats/class-04-2qR4QAgJVArv63Z2.htm)|Prayer-Touched Weapon|Arme touchée par la grâce|libre|
|[class-04-2UKf5IiUbpUbOC9a.htm](feats/class-04-2UKf5IiUbpUbOC9a.htm)|Draconic Scent|Odorat draconique|libre|
|[class-04-2VKV7jLRTxWyVjGa.htm](feats/class-04-2VKV7jLRTxWyVjGa.htm)|Improved Familiar (Witch)|Familier amélioré (Sorcier)|libre|
|[class-04-37uOb0iaWCfTCvBZ.htm](feats/class-04-37uOb0iaWCfTCvBZ.htm)|Harsh Judgement|Jugement sévère|libre|
|[class-04-39CqlOzlHjEhh0E4.htm](feats/class-04-39CqlOzlHjEhh0E4.htm)|Knockdown|Renversement|officielle|
|[class-04-3aG0gkHulBIHqqGE.htm](feats/class-04-3aG0gkHulBIHqqGE.htm)|Turn Back the Clock|Retourner l'horloge|libre|
|[class-04-3EtlXNK4vc44R3Gm.htm](feats/class-04-3EtlXNK4vc44R3Gm.htm)|Reactive Dismissal|Renvoi réactif|libre|
|[class-04-3QLWe5oS9jGJ0Oq4.htm](feats/class-04-3QLWe5oS9jGJ0Oq4.htm)|Observant Explorer|Explorateur attentif|libre|
|[class-04-3R9l2t1ycN8iwmdU.htm](feats/class-04-3R9l2t1ycN8iwmdU.htm)|Student of the Staff|Étudiant du bâton|libre|
|[class-04-3y459uK2qfWtS9q4.htm](feats/class-04-3y459uK2qfWtS9q4.htm)|Everstand Strike|Frappe toujours en position|libre|
|[class-04-4BbfHsGPRHPHfIGY.htm](feats/class-04-4BbfHsGPRHPHfIGY.htm)|Poultice Preparation|Préparation de cataplasme|libre|
|[class-04-4HHw2DjTxdv1jBZd.htm](feats/class-04-4HHw2DjTxdv1jBZd.htm)|Decry Thief|Fléau des voleurs|officielle|
|[class-04-4L5pj2W7Zyf8B3kg.htm](feats/class-04-4L5pj2W7Zyf8B3kg.htm)|Diving Armor|Armure de plongée|libre|
|[class-04-4PvmSnyp3URIqJUM.htm](feats/class-04-4PvmSnyp3URIqJUM.htm)|Parting Shot|Tir en partant|libre|
|[class-04-4Y7wKFogGB0LZ5ZA.htm](feats/class-04-4Y7wKFogGB0LZ5ZA.htm)|Steel Yourself!|Prépare toi !|libre|
|[class-04-5cYFHKQK6OZCwavI.htm](feats/class-04-5cYFHKQK6OZCwavI.htm)|Detective's Readiness|Vivacité du détective|libre|
|[class-04-5EzJVhiHQvr3v72n.htm](feats/class-04-5EzJVhiHQvr3v72n.htm)|Breached Defenses|Brèche dans la défense|libre|
|[class-04-5KXJkEs39y1gaPEm.htm](feats/class-04-5KXJkEs39y1gaPEm.htm)|Stargazer's Eyes|Yeux de l'observateur d'étoiles|libre|
|[class-04-5op3m0gwZjL4udit.htm](feats/class-04-5op3m0gwZjL4udit.htm)|Duelist's Challenge|Défi du duelliste|libre|
|[class-04-6CE1nVGxt192AUGk.htm](feats/class-04-6CE1nVGxt192AUGk.htm)|Scout's Charge|Assaut de l'éclaireur|libre|
|[class-04-6erzXaxxvcXYnL9H.htm](feats/class-04-6erzXaxxvcXYnL9H.htm)|Necrotic Infusion|Injection nécrotique|officielle|
|[class-04-6oObLoUn3MjmwbaW.htm](feats/class-04-6oObLoUn3MjmwbaW.htm)|Cryptic Spell|Sort cryptique|libre|
|[class-04-6RjilN93bgy34y3H.htm](feats/class-04-6RjilN93bgy34y3H.htm)|Basic Red Mantis Magic|Magie basique des Mantes rouges|officielle|
|[class-04-6sbuDCFHt89Uqykn.htm](feats/class-04-6sbuDCFHt89Uqykn.htm)|Mountain Skin|Peau de la montagne|libre|
|[class-04-6TlBZSPr18Y8WiNk.htm](feats/class-04-6TlBZSPr18Y8WiNk.htm)|Instructive Strike|Frappe instructive|libre|
|[class-04-7Hx0i4VchvwrIOV5.htm](feats/class-04-7Hx0i4VchvwrIOV5.htm)|Gear Gnash|Dents des engrenages|libre|
|[class-04-7jqBwXq9jVsghCva.htm](feats/class-04-7jqBwXq9jVsghCva.htm)|Gossip Lore|Connaissance des ragots|libre|
|[class-04-7nnEtLdUKmDljdhm.htm](feats/class-04-7nnEtLdUKmDljdhm.htm)|Wellspring Control|Contrôle de la source|libre|
|[class-04-7Pb1WL8abrPBTPrH.htm](feats/class-04-7Pb1WL8abrPBTPrH.htm)|Basic Dogma|Dogme basique|officielle|
|[class-04-7XcQ8Ygz5cubGxdC.htm](feats/class-04-7XcQ8Ygz5cubGxdC.htm)|Investigator's Stratagem|Stratagème d'enquêteur|libre|
|[class-04-7ycF0fgSw1ovUPit.htm](feats/class-04-7ycF0fgSw1ovUPit.htm)|Basic Oracle Spellcasting|Incantation basique de l'oracle|libre|
|[class-04-81pD03pQup9sPzXv.htm](feats/class-04-81pD03pQup9sPzXv.htm)|Snowcaster|Incantateur nival|libre|
|[class-04-8bDwJieEVCjrceM7.htm](feats/class-04-8bDwJieEVCjrceM7.htm)|Nocturnal Kindred|Parenté nocturne|libre|
|[class-04-8EbIznFgkG7PHqlE.htm](feats/class-04-8EbIznFgkG7PHqlE.htm)|Divine Access|Accès divin|libre|
|[class-04-8pmd9gMl3TamFx3u.htm](feats/class-04-8pmd9gMl3TamFx3u.htm)|Deft Cooperation|Collaboration avisée|officielle|
|[class-04-9j90iE61ZToFR8cu.htm](feats/class-04-9j90iE61ZToFR8cu.htm)|Call Bonded Item|Appeler l'objet lié|libre|
|[class-04-9KvsO72JJ3pfkG4U.htm](feats/class-04-9KvsO72JJ3pfkG4U.htm)|Hurling Charge|Lancer en chargeant|libre|
|[class-04-9SYnbjFgyucjhN5e.htm](feats/class-04-9SYnbjFgyucjhN5e.htm)|Dread Striker|Frappeur d'effroi|officielle|
|[class-04-9yko78REsaw7i2gr.htm](feats/class-04-9yko78REsaw7i2gr.htm)|Suplex|Suplex|libre|
|[class-04-AbsqV1P8OAhChcRl.htm](feats/class-04-AbsqV1P8OAhChcRl.htm)|Inspiring Resilience|Résistance inspirante|libre|
|[class-04-agfosPInBLQXNQfa.htm](feats/class-04-agfosPInBLQXNQfa.htm)|Head Stomp|Piétiner la tête|libre|
|[class-04-aiHbS8FGNYAQBF62.htm](feats/class-04-aiHbS8FGNYAQBF62.htm)|Accelerating Touch|Contact accélérant|libre|
|[class-04-AkV4Jyllo6nlK2Sl.htm](feats/class-04-AkV4Jyllo6nlK2Sl.htm)|Cobra Stance|Posture du cobra|libre|
|[class-04-ALbosSUygdq4T1Yk.htm](feats/class-04-ALbosSUygdq4T1Yk.htm)|Poison Weapon|Arme empoisonnée|officielle|
|[class-04-aLJsBBZzlUK3G8MW.htm](feats/class-04-aLJsBBZzlUK3G8MW.htm)|Quick Study|Étude rapide|libre|
|[class-04-amPQHO9O86G6AC4P.htm](feats/class-04-amPQHO9O86G6AC4P.htm)|Devastating Spellstrike|Frappe de sort dévastatrice|libre|
|[class-04-asPQEfYCcsbYxx6K.htm](feats/class-04-asPQEfYCcsbYxx6K.htm)|Basic Breakthrough|Avancée basique|libre|
|[class-04-ATsP9JCBnzkwVQbl.htm](feats/class-04-ATsP9JCBnzkwVQbl.htm)|Harrow Casting|Incantation du Tourment|libre|
|[class-04-AuIE19F9rY3YvXf6.htm](feats/class-04-AuIE19F9rY3YvXf6.htm)|Magical Scholastics|Études magiques|libre|
|[class-04-AvfswILmh3BwNbyR.htm](feats/class-04-AvfswILmh3BwNbyR.htm)|Basic Sorcerer Spellcasting|Incantation basique de l'ensorceleur|officielle|
|[class-04-B0T6p3kcrOfSvLqQ.htm](feats/class-04-B0T6p3kcrOfSvLqQ.htm)|Triple Time|À trois temps|officielle|
|[class-04-B5f7eCAC3ZEmlR9h.htm](feats/class-04-B5f7eCAC3ZEmlR9h.htm)|Basic Spellcasting|Incantation basique de sort|libre|
|[class-04-B6jXVgfPuPWWLx2K.htm](feats/class-04-B6jXVgfPuPWWLx2K.htm)|Battle Assessment|Évaluation martiale|officielle|
|[class-04-baD02AcIpU7xUBlD.htm](feats/class-04-baD02AcIpU7xUBlD.htm)|Basic Maneuver|Manoeuvre basique|officielle|
|[class-04-baz18CdB13DVMHV9.htm](feats/class-04-baz18CdB13DVMHV9.htm)|Leshy Familiar Secrets|Secrets du familier léchi|libre|
|[class-04-bCAvo59b5RyW12iI.htm](feats/class-04-bCAvo59b5RyW12iI.htm)|Sneak Attacker|Agresseur furtif|officielle|
|[class-04-BEqXsP6UqARzpEFD.htm](feats/class-04-BEqXsP6UqARzpEFD.htm)|Megaton Strike|Frappe mégatonne|libre|
|[class-04-bgnf7USFkqsNh8j1.htm](feats/class-04-bgnf7USFkqsNh8j1.htm)|Investigate Haunting|Enquêter sur les apparitions|libre|
|[class-04-bj5QmGyrt7OYORjo.htm](feats/class-04-bj5QmGyrt7OYORjo.htm)|Senses of the Bear|Sens de l'ours|libre|
|[class-04-bJbWM6zcOeDvCOiZ.htm](feats/class-04-bJbWM6zcOeDvCOiZ.htm)|Sacred Spells|Sorts sacrés|libre|
|[class-04-Bk07joho2dUG3lVw.htm](feats/class-04-Bk07joho2dUG3lVw.htm)|Duelist's Edge|Avantage du duelliste|officielle|
|[class-04-bLj5ufeOdVWDx8Aw.htm](feats/class-04-bLj5ufeOdVWDx8Aw.htm)|Starlit Eyes|Yeux stellaires|libre|
|[class-04-BQkqUrlUVNFp8BEq.htm](feats/class-04-BQkqUrlUVNFp8BEq.htm)|Wild Lights|Lumières morphiques|libre|
|[class-04-BridkNEysTuSOOLM.htm](feats/class-04-BridkNEysTuSOOLM.htm)|Basic Kata|Kata basique|officielle|
|[class-04-Btu0tA6SEBS6K1hE.htm](feats/class-04-Btu0tA6SEBS6K1hE.htm)|Never Tire|Jamais fatigué|libre|
|[class-04-BtZJJClWCpc31Ven.htm](feats/class-04-BtZJJClWCpc31Ven.htm)|Push Back the Dead!|Repoussez les morts !|libre|
|[class-04-bvOsJNeI0ewvQsFa.htm](feats/class-04-bvOsJNeI0ewvQsFa.htm)|Inspiring Marshal Stance|Posture du capitaine inspirant|libre|
|[class-04-C3MgEkPNaIhTddbr.htm](feats/class-04-C3MgEkPNaIhTddbr.htm)|Peafowl Stance|Posture du paon|libre|
|[class-04-c7jNms3ZQ8eaMUqv.htm](feats/class-04-c7jNms3ZQ8eaMUqv.htm)|Efficient Alchemy (Alchemist)|Alchimie efficiente (Alchimiste)|officielle|
|[class-04-CAaXGhHDMRM3Pt4J.htm](feats/class-04-CAaXGhHDMRM3Pt4J.htm)|Cavalier's Banner|Bannière du cavalier|libre|
|[class-04-cB6K0wkiDhduAjtL.htm](feats/class-04-cB6K0wkiDhduAjtL.htm)|Just the Tool|L'outil idéal|libre|
|[class-04-cErltcAC7OVnIyO1.htm](feats/class-04-cErltcAC7OVnIyO1.htm)|Predictable!|Prévisible !|libre|
|[class-04-cEu8BUS41dlPyPGW.htm](feats/class-04-cEu8BUS41dlPyPGW.htm)|Spiritual Guides|Guides spirituels|libre|
|[class-04-chmQgMamyaZX90Tc.htm](feats/class-04-chmQgMamyaZX90Tc.htm)|Magical Edification|Édification magique|libre|
|[class-04-CLKlavik0540j5bl.htm](feats/class-04-CLKlavik0540j5bl.htm)|Wounded Rage|Rage blessée|officielle|
|[class-04-cTR67ZKDD1EKntXw.htm](feats/class-04-cTR67ZKDD1EKntXw.htm)|Advanced Construct Companion|Compagnon créature artificielle avancé|libre|
|[class-04-CX4ISbBwndRWhP55.htm](feats/class-04-CX4ISbBwndRWhP55.htm)|Summon Ensemble|Convocation d'ensemble|libre|
|[class-04-cxwDj2gZ7kJdP4hs.htm](feats/class-04-cxwDj2gZ7kJdP4hs.htm)|Thousand Faces|Mille visages|officielle|
|[class-04-CZQgH17ZxoBiVXLa.htm](feats/class-04-CZQgH17ZxoBiVXLa.htm)|Ka Stone Ritual|Rituel de la pierre Ka|officielle|
|[class-04-d8RfatiK9UOQANLz.htm](feats/class-04-d8RfatiK9UOQANLz.htm)|Lost in the Crowd|Se perdre dans la foule|officielle|
|[class-04-Daap4ugeDZQWoPCx.htm](feats/class-04-Daap4ugeDZQWoPCx.htm)|Spirit Spells|Sorts spirituels|libre|
|[class-04-dbGocwZV9vJslbgC.htm](feats/class-04-dbGocwZV9vJslbgC.htm)|Broadside Buckos|Gars de la bordée|libre|
|[class-04-DBsqWivnSaEo8jz5.htm](feats/class-04-DBsqWivnSaEo8jz5.htm)|Basic Arcana|Arcanes basiques|officielle|
|[class-04-dC14a0DZqDBA9B8g.htm](feats/class-04-dC14a0DZqDBA9B8g.htm)|Instant Backup|Rechange instantanée|libre|
|[class-04-douVMHDuQQv8U8aq.htm](feats/class-04-douVMHDuQQv8U8aq.htm)|Lingering Chill|Froid persistant|libre|
|[class-04-DQN7YC7s7T0pL6Aa.htm](feats/class-04-DQN7YC7s7T0pL6Aa.htm)|Gadget Specialist|Spécialiste des gadgets|libre|
|[class-04-DS0XlHfi3ztb3ET7.htm](feats/class-04-DS0XlHfi3ztb3ET7.htm)|Split Shot|Tir divisé|libre|
|[class-04-dSSwRyuhKTq1VubX.htm](feats/class-04-dSSwRyuhKTq1VubX.htm)|Disarming Block|Blocage désarmant|libre|
|[class-04-dtt6xTOSF8PuoStg.htm](feats/class-04-dtt6xTOSF8PuoStg.htm)|Tracing Sigil|Symbole traçant|libre|
|[class-04-DUuCOQ9FiZf7vS5b.htm](feats/class-04-DUuCOQ9FiZf7vS5b.htm)|Ritual Researcher|Chercheur de rituels|libre|
|[class-04-ELdUj5ihdivlgb3H.htm](feats/class-04-ELdUj5ihdivlgb3H.htm)|Crystal Keeper Dedication|Dévouement : Gardien des cristaux|officielle|
|[class-04-epBOnfEDOd4V9mQ0.htm](feats/class-04-epBOnfEDOd4V9mQ0.htm)|Natural Swimmer|Nageur naturel|libre|
|[class-04-ePKe13FzKWaQqLo6.htm](feats/class-04-ePKe13FzKWaQqLo6.htm)|Basic Psychic Spellcasting|Incantation basique du psychiste|libre|
|[class-04-EsA2F4R3UwUdI8Px.htm](feats/class-04-EsA2F4R3UwUdI8Px.htm)|Cycle Spell|Sort du cycle|libre|
|[class-04-eXuCYDzj0UJOxNu9.htm](feats/class-04-eXuCYDzj0UJOxNu9.htm)|Twin Distraction|Distraction jumelée|libre|
|[class-04-f69C05QokaBrDFjn.htm](feats/class-04-f69C05QokaBrDFjn.htm)|Shadow Spells|Sorts d'ombre|libre|
|[class-04-FbzbZc4LGUTcz9tA.htm](feats/class-04-FbzbZc4LGUTcz9tA.htm)|Gunpowder Gauntlet|Attraction balistique|libre|
|[class-04-FdP21jbjHHGpHut1.htm](feats/class-04-FdP21jbjHHGpHut1.htm)|Tenacious Toxins|Toxines tenaces|libre|
|[class-04-fGBb3VsJwf7osKaL.htm](feats/class-04-fGBb3VsJwf7osKaL.htm)|One With the Land|Un avec la terre|libre|
|[class-04-FItD6HmjasjbLdgS.htm](feats/class-04-FItD6HmjasjbLdgS.htm)|Spell Runes|Rune de sort|officielle|
|[class-04-fO1vRDEfl9pysfLU.htm](feats/class-04-fO1vRDEfl9pysfLU.htm)|Guarded Movement|Déplacement circonspect|officielle|
|[class-04-FoJd0oiEQ9mF5KS9.htm](feats/class-04-FoJd0oiEQ9mF5KS9.htm)|Steady Spellcasting (Magus)|Incantation fiable (magus)|libre|
|[class-04-FSxugo3zTgRhW7Og.htm](feats/class-04-FSxugo3zTgRhW7Og.htm)|Improvised Pummel|Tabassage improvisé|libre|
|[class-04-fT37dtsByEIc3glC.htm](feats/class-04-fT37dtsByEIc3glC.htm)|Scales Of The Dragon|Écailles de dragon|libre|
|[class-04-FVDozRTuCQQzD97D.htm](feats/class-04-FVDozRTuCQQzD97D.htm)|Eye of Ozem|Oeil d'Ozem|officielle|
|[class-04-fVP8jX7yRUpyrcVO.htm](feats/class-04-fVP8jX7yRUpyrcVO.htm)|Basic Cathartic Spellcasting|Incantation cathartique basique|libre|
|[class-04-FWQSyjjYg8h0KpHq.htm](feats/class-04-FWQSyjjYg8h0KpHq.htm)|Initial Eidolon Ability|Capacité d'eidolon initial|libre|
|[class-04-G43fiFtxFR24CWRs.htm](feats/class-04-G43fiFtxFR24CWRs.htm)|Magic Warrior Aspect|Aspect du Guerrier magique|officielle|
|[class-04-GByIIkeHN3JWkZIZ.htm](feats/class-04-GByIIkeHN3JWkZIZ.htm)|Mask Familiar|Familier masque|libre|
|[class-04-gc24C5CyWgqn1Lbl.htm](feats/class-04-gc24C5CyWgqn1Lbl.htm)|Basic Cleric Spellcasting|Incantation basique du prêtre|officielle|
|[class-04-geESDWQVvwScyPph.htm](feats/class-04-geESDWQVvwScyPph.htm)|Basic Witchcraft|Sorcellerie basique|libre|
|[class-04-GkPbsX5RZWyI0qFj.htm](feats/class-04-GkPbsX5RZWyI0qFj.htm)|Psychopomp Familiar|Familier psychopompe|libre|
|[class-04-gL7QZsSMldjwE6te.htm](feats/class-04-gL7QZsSMldjwE6te.htm)|Inured to Alchemy|Endurci par l'alchimie|libre|
|[class-04-gMPnLWLlHoNU9Lqv.htm](feats/class-04-gMPnLWLlHoNU9Lqv.htm)|Dragon Arcana|Arcanes de dragon|libre|
|[class-04-GNMy7NYfF3AQwHpN.htm](feats/class-04-GNMy7NYfF3AQwHpN.htm)|Divine Health|Santé divine|officielle|
|[class-04-gqQK7dDrGPkQDfQQ.htm](feats/class-04-gqQK7dDrGPkQDfQQ.htm)|Mortification|Mortification|officielle|
|[class-04-GV0lOcVgcetsUlLO.htm](feats/class-04-GV0lOcVgcetsUlLO.htm)|Brilliant Crafter|Artisan brillant|libre|
|[class-04-gVXefUaMzg7S1vpm.htm](feats/class-04-gVXefUaMzg7S1vpm.htm)|Knock Sense|Ramener à la raison|libre|
|[class-04-Gz9NQBjSDRQP8YTY.htm](feats/class-04-Gz9NQBjSDRQP8YTY.htm)|Striker's Scroll|Parchemin du frappeur|libre|
|[class-04-gZrlze9HlRYEQNBG.htm](feats/class-04-gZrlze9HlRYEQNBG.htm)|Soul Flare|Éclat de l'âme|libre|
|[class-04-H2GlPobLyqgBYbd9.htm](feats/class-04-H2GlPobLyqgBYbd9.htm)|Hybrid Study Spell|Sort d'études hybridées|libre|
|[class-04-h5ZT9i79BFVJ0VfE.htm](feats/class-04-h5ZT9i79BFVJ0VfE.htm)|Basic Concoction|Concoction basique|officielle|
|[class-04-HdhnAm9SNfDqxRSN.htm](feats/class-04-HdhnAm9SNfDqxRSN.htm)|First Revelation|Première révélation|libre|
|[class-04-hENWnoZNljeJnZBR.htm](feats/class-04-hENWnoZNljeJnZBR.htm)|Elbow Breaker|Briseur d'épaule|libre|
|[class-04-HHAGiBYVv8nyUEsd.htm](feats/class-04-HHAGiBYVv8nyUEsd.htm)|Dual-Handed Assault|Assaut à deux mains|officielle|
|[class-04-HpT1GlcnkCBnDnVF.htm](feats/class-04-HpT1GlcnkCBnDnVF.htm)|Barbarian Resiliency|Résilience du barbare|officielle|
|[class-04-htOsE4hnSj2gzKdi.htm](feats/class-04-htOsE4hnSj2gzKdi.htm)|Mug|Arracher avec violence|libre|
|[class-04-HVP3ZGIOlxlFy0ni.htm](feats/class-04-HVP3ZGIOlxlFy0ni.htm)|Hunter's Sanctum|Sanctuaire du chasseur|libre|
|[class-04-HvPvyeXM2iMK4OYf.htm](feats/class-04-HvPvyeXM2iMK4OYf.htm)|Repeating Hand Crossbow Training|Formation à l'arbalète de poing à répétition|libre|
|[class-04-Hwvrive2vBIqZUcE.htm](feats/class-04-Hwvrive2vBIqZUcE.htm)|Familiar Mascot|Familier mascotte|libre|
|[class-04-hYu6XxARNJYdf8Qe.htm](feats/class-04-hYu6XxARNJYdf8Qe.htm)|Scalpel's Point|Pointe du scalpel|libre|
|[class-04-Hzq8FOtaYWpur2BL.htm](feats/class-04-Hzq8FOtaYWpur2BL.htm)|Safety Measures|Mesures de sécurité|libre|
|[class-04-I10dkdvL6kAnqZWA.htm](feats/class-04-I10dkdvL6kAnqZWA.htm)|Lie Detector|Détecteur de mensonge|libre|
|[class-04-i86JRWsFRpfEJnZP.htm](feats/class-04-i86JRWsFRpfEJnZP.htm)|Pirate Weapon Training|Entraînement avec les armes de pirate|libre|
|[class-04-iAziUKdxgy4k4ypY.htm](feats/class-04-iAziUKdxgy4k4ypY.htm)|Shining Arms|Arme éblouissante|libre|
|[class-04-IeRX0tGbeOF0ev08.htm](feats/class-04-IeRX0tGbeOF0ev08.htm)|Shorthanded|À court de bras|libre|
|[class-04-ifiaewasPbc091BQ.htm](feats/class-04-ifiaewasPbc091BQ.htm)|Ankle Biter|Cheville amère|libre|
|[class-04-iKOcwFCbNX1a2OFT.htm](feats/class-04-iKOcwFCbNX1a2OFT.htm)|Ardent Armiger|Fervent écuyer|officielle|
|[class-04-iMh9Ve8Kt8ZcdKU0.htm](feats/class-04-iMh9Ve8Kt8ZcdKU0.htm)|Bullying Staff|Bâton harcelant|libre|
|[class-04-io6eJGrw701WbmYe.htm](feats/class-04-io6eJGrw701WbmYe.htm)|Surprise Attack|Attaque surprise|libre|
|[class-04-IOdk7bOJ4dgYVh9I.htm](feats/class-04-IOdk7bOJ4dgYVh9I.htm)|Improved Communal Healing|Guérison collective améliorée|officielle|
|[class-04-iojlXjVdbzi1fZGt.htm](feats/class-04-iojlXjVdbzi1fZGt.htm)|Directed Channel|Canalisation dirigée|officielle|
|[class-04-IqTtpxZ48rApy4BN.htm](feats/class-04-IqTtpxZ48rApy4BN.htm)|Executioner Weapon Training|Entraînement avec les armes du bourreau|libre|
|[class-04-irDFSzKCaF4ux3bx.htm](feats/class-04-irDFSzKCaF4ux3bx.htm)|Reflexive Catch|Réception instinctive|libre|
|[class-04-IrxtH1BFlUOS0DnQ.htm](feats/class-04-IrxtH1BFlUOS0DnQ.htm)|Captivator Dedication|Dévouement : Enjôleur|libre|
|[class-04-it2i6OXfGIizokpg.htm](feats/class-04-it2i6OXfGIizokpg.htm)|Animal Feature|Trait animal|libre|
|[class-04-IvgLDtXmZwzbVJj1.htm](feats/class-04-IvgLDtXmZwzbVJj1.htm)|Strategic Assessment|Évaluation stratégique|libre|
|[class-04-IYt6pMqiTocTrixA.htm](feats/class-04-IYt6pMqiTocTrixA.htm)|Familiar Conduit|Familier conducteur|libre|
|[class-04-iyXw5PnevT2jT8kU.htm](feats/class-04-iyXw5PnevT2jT8kU.htm)|Sense Alignment|Perception de l'alignement|libre|
|[class-04-j01dM0ZAC7KzShx0.htm](feats/class-04-j01dM0ZAC7KzShx0.htm)|Rites Of Convocation|Rites de convocation|libre|
|[class-04-J1MURfqf0kbxrKG9.htm](feats/class-04-J1MURfqf0kbxrKG9.htm)|Warding Light|Lumière protectrice|libre|
|[class-04-J8BSHRkmP3QLknwF.htm](feats/class-04-J8BSHRkmP3QLknwF.htm)|Beast Speaker|Locuteur bestial|libre|
|[class-04-j9Rp4fOZIxizyvYy.htm](feats/class-04-j9Rp4fOZIxizyvYy.htm)|Viking Weapon Familiarity|Familiarité avec les armes des vikings|libre|
|[class-04-jBeuyq0Aged45YAc.htm](feats/class-04-jBeuyq0Aged45YAc.htm)|Enduring Alchemy|Alchimie durable|officielle|
|[class-04-JbrVcOf82oFXk3mY.htm](feats/class-04-JbrVcOf82oFXk3mY.htm)|Swipe|Frappe transversale|officielle|
|[class-04-JcXzKwrdMkNszrJQ.htm](feats/class-04-JcXzKwrdMkNszrJQ.htm)|Radiant Infusion|Imprégnation irradiante|libre|
|[class-04-JGfJPx6xkx11zHlW.htm](feats/class-04-JGfJPx6xkx11zHlW.htm)|Linked Focus|Focalisation liée|officielle|
|[class-04-JgfZOEOgnbKNYth4.htm](feats/class-04-JgfZOEOgnbKNYth4.htm)|Irezoko Tattoo|Tatouage irezoko|libre|
|[class-04-Jk6gZzXEABiX5A0S.htm](feats/class-04-Jk6gZzXEABiX5A0S.htm)|Loose Cannon|Canon ample|libre|
|[class-04-JKvP4pFHzwWsHu2n.htm](feats/class-04-JKvP4pFHzwWsHu2n.htm)|Ranger Resiliency|Résilience du rôdeur|officielle|
|[class-04-JlGZFW3mCNxWPKvX.htm](feats/class-04-JlGZFW3mCNxWPKvX.htm)|Silent Spell|Sort silencieux|officielle|
|[class-04-jlLWz8e7PpLFt0Ed.htm](feats/class-04-jlLWz8e7PpLFt0Ed.htm)|Healing Touch|Contact guérisseur/Corrupteur|officielle|
|[class-04-JM0umAUx30mAkuTz.htm](feats/class-04-JM0umAUx30mAkuTz.htm)|Basic Muse's Whispers|Murmures de la muse basiques|officielle|
|[class-04-JO3mcFvxjRp1V8XK.htm](feats/class-04-JO3mcFvxjRp1V8XK.htm)|Contortionist|Contortionniste|libre|
|[class-04-jQ8JntOdBFHPw5S4.htm](feats/class-04-jQ8JntOdBFHPw5S4.htm)|Light of Revelation|Lumière de révélation|libre|
|[class-04-jr2LYiMvjnTNhfMM.htm](feats/class-04-jr2LYiMvjnTNhfMM.htm)|Slayer's Strike|Frappe du tueur|libre|
|[class-04-JStTmD3W8R41WvPg.htm](feats/class-04-JStTmD3W8R41WvPg.htm)|Sleepwalker Dedication|Dévouement : Somnambule|libre|
|[class-04-jsXDbQAAH3yMchPU.htm](feats/class-04-jsXDbQAAH3yMchPU.htm)|Spot Translate|Traduction simultanée|libre|
|[class-04-juTBSRs2jzJzLoth.htm](feats/class-04-juTBSRs2jzJzLoth.htm)|Fake It Till You Make It|Feins le jusqu'à le faire|libre|
|[class-04-jZbhRBR2yawntSmd.htm](feats/class-04-jZbhRBR2yawntSmd.htm)|Agile Hand|Manipulation agile|libre|
|[class-04-JZurhROfi2JfmLfb.htm](feats/class-04-JZurhROfi2JfmLfb.htm)|Unnerving Expansion|Expansion perturbante|libre|
|[class-04-K3TasgeZLJQ84qtZ.htm](feats/class-04-K3TasgeZLJQ84qtZ.htm)|Primal Evolution|Évolution primordiale|officielle|
|[class-04-k4QU2edqSoB23foo.htm](feats/class-04-k4QU2edqSoB23foo.htm)|The Harder They Fall|Plus dure est la chute|libre|
|[class-04-k5C1WNuYQ4u7nSHt.htm](feats/class-04-k5C1WNuYQ4u7nSHt.htm)|Nonlethal Takedown|Mise au tapis non létale|libre|
|[class-04-kh4bTBgi3C9CjwHK.htm](feats/class-04-kh4bTBgi3C9CjwHK.htm)|Melodious Spell|Sort mélodieux|officielle|
|[class-04-kmIgbWLbaarqmMaY.htm](feats/class-04-kmIgbWLbaarqmMaY.htm)|Basic Captivator Spellcasting|Incantation basique d'enjôleur|libre|
|[class-04-KMVXUgFArcftg1jQ.htm](feats/class-04-KMVXUgFArcftg1jQ.htm)|Masquerade of Seasons Stance|Posture : Posture de la mascarade des saisons|libre|
|[class-04-KOf6kmwAZaUJSDW9.htm](feats/class-04-KOf6kmwAZaUJSDW9.htm)|Terrain Scout|Éclaireur de terrain|libre|
|[class-04-kPhym38UCLJpjnJD.htm](feats/class-04-kPhym38UCLJpjnJD.htm)|Angel of Vindication|Ange de vindicte|libre|
|[class-04-kqW6d3Dfk4nApd7y.htm](feats/class-04-kqW6d3Dfk4nApd7y.htm)|Combat Reading|Lecture du combat|libre|
|[class-04-KrYvJ5n06yHCipCZ.htm](feats/class-04-KrYvJ5n06yHCipCZ.htm)|Play To The Crowd|Jouer pour la foule|libre|
|[class-04-kU4K9jj9qnktoAaQ.htm](feats/class-04-kU4K9jj9qnktoAaQ.htm)|Ritualist Dedication|Dévouement : Ritualiste|libre|
|[class-04-KU6nyh8DyQ7NoRQj.htm](feats/class-04-KU6nyh8DyQ7NoRQj.htm)|Ostentatious Reload|Rechargement ostentatoire|libre|
|[class-04-kUa8PBjKmBk04zmc.htm](feats/class-04-kUa8PBjKmBk04zmc.htm)|Basic Wilding|Milieu sauvage basique|officielle|
|[class-04-KWXoo738KuddWMOB.htm](feats/class-04-KWXoo738KuddWMOB.htm)|Ongoing Investigation|Investigation continue|libre|
|[class-04-Kxckxf4G9URXNc07.htm](feats/class-04-Kxckxf4G9URXNc07.htm)|Wolf in Sheep's Clothing|Loup déguisé en agneau|libre|
|[class-04-KZJJ0WxcRd4RZKJR.htm](feats/class-04-KZJJ0WxcRd4RZKJR.htm)|Elemental Familiar|Familier élémentaire|libre|
|[class-04-LJw5tRrX0dMnm9Vq.htm](feats/class-04-LJw5tRrX0dMnm9Vq.htm)|Soothing Mist|Brume apaisante|libre|
|[class-04-lKgpe2JKaeLjgnYF.htm](feats/class-04-lKgpe2JKaeLjgnYF.htm)|Woodland Stride|Déplacement facilité en forêt|officielle|
|[class-04-lkLwEfc3ZhLJSkVr.htm](feats/class-04-lkLwEfc3ZhLJSkVr.htm)|Manipulative Charm|Charme manipulateur|libre|
|[class-04-lL2fQJ2oRyBgga8Q.htm](feats/class-04-lL2fQJ2oRyBgga8Q.htm)|Occult Evolution|Évolution occulte|officielle|
|[class-04-lpG7ZXFDZygmkbH4.htm](feats/class-04-lpG7ZXFDZygmkbH4.htm)|Bloodletting Claws|Griffes hémorragiques|libre|
|[class-04-LQ5YW01UD9hGKk0l.htm](feats/class-04-LQ5YW01UD9hGKk0l.htm)|Corpse Tender's Font|Source de l'éleveur de cadavres|libre|
|[class-04-lyJ3pPvZAr9nRco6.htm](feats/class-04-lyJ3pPvZAr9nRco6.htm)|Hallowed Initiate|Initié sacré|libre|
|[class-04-lYVAGuHU47Ixyuxy.htm](feats/class-04-lYVAGuHU47Ixyuxy.htm)|Focused Juggler|Jongleur concentré|libre|
|[class-04-m4FOYkzuqNjU0ETq.htm](feats/class-04-m4FOYkzuqNjU0ETq.htm)|Eldritch Nails|Ongles mystiques|libre|
|[class-04-M9WIUEPY6IoRtgWN.htm](feats/class-04-M9WIUEPY6IoRtgWN.htm)|Gravelands Herbalist|Herboriste des Terres tombales|libre|
|[class-04-meQJfsKVar9tm6c9.htm](feats/class-04-meQJfsKVar9tm6c9.htm)|Running Reload|Rechargement en courant|officielle|
|[class-04-Mj1KTiAwwovm7K9f.htm](feats/class-04-Mj1KTiAwwovm7K9f.htm)|Stand Still|Ne bouge plus|officielle|
|[class-04-MkLujXEplJUt6Arv.htm](feats/class-04-MkLujXEplJUt6Arv.htm)|Necromantic Resistance (Undead Slayer)|Résistance nécromantique (Tueur de mort-vivant)|libre|
|[class-04-mnHUemd21MtmV9FV.htm](feats/class-04-mnHUemd21MtmV9FV.htm)|Empathic Envoy|Envoyé empathique|libre|
|[class-04-MnqDDUekqd40HTZc.htm](feats/class-04-MnqDDUekqd40HTZc.htm)|Necromantic Resistance|Résistance nécromantique|officielle|
|[class-04-MprbkBZUouqvbKGo.htm](feats/class-04-MprbkBZUouqvbKGo.htm)|Folktales Lore|Connaissance des contes populaires|libre|
|[class-04-mRHkGMLecd5aaj2R.htm](feats/class-04-mRHkGMLecd5aaj2R.htm)|Tools Of The Trade|Outils indispensables|libre|
|[class-04-MU9qS0QuBdcyLkza.htm](feats/class-04-MU9qS0QuBdcyLkza.htm)|Homing Beacon|Balise de guidage|libre|
|[class-04-mV911W6MTGMvHPbE.htm](feats/class-04-mV911W6MTGMvHPbE.htm)|Basic Hunter's Trick|Astuce du chasseur basique|officielle|
|[class-04-mxO7u59ms58q7zyj.htm](feats/class-04-mxO7u59ms58q7zyj.htm)|Magical Trickster|Trompeur magique|libre|
|[class-04-mXp6G4YWCXGvp7Qd.htm](feats/class-04-mXp6G4YWCXGvp7Qd.htm)|Awakened Power|Pouvoir éveillé (Déviant)|libre|
|[class-04-N0YcU8mIJnQ4C2N6.htm](feats/class-04-N0YcU8mIJnQ4C2N6.htm)|Posse|Détachement|libre|
|[class-04-N4TpzEzuFbInSgvz.htm](feats/class-04-N4TpzEzuFbInSgvz.htm)|Hunter's Luck|Chance du chasseur|libre|
|[class-04-n5T4ChWJqDUblYfR.htm](feats/class-04-n5T4ChWJqDUblYfR.htm)|Bless Shield|Bouclier béni|libre|
|[class-04-N8mhYbr5xBI8jydb.htm](feats/class-04-N8mhYbr5xBI8jydb.htm)|Violent Unleash|Déchainement violent|libre|
|[class-04-nAgAICjPd4BSQlAj.htm](feats/class-04-nAgAICjPd4BSQlAj.htm)|Axe Climber|Escalade à la hache|libre|
|[class-04-NagTqSLK8bAlo2nY.htm](feats/class-04-NagTqSLK8bAlo2nY.htm)|Firebrand Braggart Dedication|Dévouement : Agitateur vantard|libre|
|[class-04-nc5G99d20PI9JKCK.htm](feats/class-04-nc5G99d20PI9JKCK.htm)|Monk Resiliency|Résilience du moine|officielle|
|[class-04-ncXK0Cc8dZ9TilSC.htm](feats/class-04-ncXK0Cc8dZ9TilSC.htm)|Devil's Eye|Oeil diabolique|libre|
|[class-04-NdgMxlz5I1ddT0Zi.htm](feats/class-04-NdgMxlz5I1ddT0Zi.htm)|Basic Mysteries|Mystères basiques|libre|
|[class-04-NHheDmNB7L4REmlr.htm](feats/class-04-NHheDmNB7L4REmlr.htm)|Swaggering Initiative|Initiative arrogante|libre|
|[class-04-NlEZ0piDxg9buXCL.htm](feats/class-04-NlEZ0piDxg9buXCL.htm)|Quick Alchemy|Alchimie rapide|officielle|
|[class-04-nqEqvZviKYuGVxLV.htm](feats/class-04-nqEqvZviKYuGVxLV.htm)|Restore Omen|Récupération de présage|libre|
|[class-04-NQtIhIowH1tVywZI.htm](feats/class-04-NQtIhIowH1tVywZI.htm)|Replenishing Consumption|Consommation reconstituante|libre|
|[class-04-NrCloSukzSUCprsM.htm](feats/class-04-NrCloSukzSUCprsM.htm)|Blessed Sacrifice|Sacrifice de l'Élu divin|libre|
|[class-04-nroOy0PBeEUGdUXD.htm](feats/class-04-nroOy0PBeEUGdUXD.htm)|Dousing Spell|Sort trempant|libre|
|[class-04-NrZt98r47sPSQ06j.htm](feats/class-04-NrZt98r47sPSQ06j.htm)|Crystal Ward Spells|Magie des glyphes de cristal|officielle|
|[class-04-NSyzjkDdQU2A75mX.htm](feats/class-04-NSyzjkDdQU2A75mX.htm)|Basic Bard Spellcasting|Incantation basique du barde|officielle|
|[class-04-nT9Z4OPDTOg2AGYc.htm](feats/class-04-nT9Z4OPDTOg2AGYc.htm)|Basic Blood Potency|Pouvoir du sang basique|officielle|
|[class-04-nU5Pow4HMzoDHa8Z.htm](feats/class-04-nU5Pow4HMzoDHa8Z.htm)|Basic Witch Spellcasting|Incantation basique du sorcier|libre|
|[class-04-nU8W1QoA9hl3h6nR.htm](feats/class-04-nU8W1QoA9hl3h6nR.htm)|Watch and Learn|Observer et apprendre|libre|
|[class-04-Nxke8WzifQafSa4I.htm](feats/class-04-Nxke8WzifQafSa4I.htm)|Impressive Mount|Monture impressionnante|libre|
|[class-04-NZgjqVV2HYzLCvvA.htm](feats/class-04-NZgjqVV2HYzLCvvA.htm)|Aldori Parry|Parade aldorie|officielle|
|[class-04-o5q9FBrPsAYqEl5w.htm](feats/class-04-o5q9FBrPsAYqEl5w.htm)|Channel Smite|Châtiment canalisé|officielle|
|[class-04-oGu9AtUAx0SpRXy8.htm](feats/class-04-oGu9AtUAx0SpRXy8.htm)|Favored Enemy|Ennemi juré|officielle|
|[class-04-oHdUwzUUblg3neCT.htm](feats/class-04-oHdUwzUUblg3neCT.htm)|Order Magic|Magie des ordres|officielle|
|[class-04-oIE88rIDEFNm83Mr.htm](feats/class-04-oIE88rIDEFNm83Mr.htm)|Powerful Shove|Poussée en puissance|officielle|
|[class-04-OJxEF5FONTtEdbpP.htm](feats/class-04-OJxEF5FONTtEdbpP.htm)|Heal Animal|Guérison des animaux|libre|
|[class-04-OQAo3Us0ODGYdNNn.htm](feats/class-04-OQAo3Us0ODGYdNNn.htm)|Expert Backstabber|Traître expert|libre|
|[class-04-OqqytsF2RIjB0EPR.htm](feats/class-04-OqqytsF2RIjB0EPR.htm)|Devrin's Cunning Stance|Posture astucieuse de Devrin|libre|
|[class-04-OqU6QXkMrZqToEEi.htm](feats/class-04-OqU6QXkMrZqToEEi.htm)|Opportunist|Opportuniste|officielle|
|[class-04-otLs7XzMXR1cZKGe.htm](feats/class-04-otLs7XzMXR1cZKGe.htm)|Quick Juggler|Jongleur rapide|libre|
|[class-04-OxvZSFGOjGfcSCv8.htm](feats/class-04-OxvZSFGOjGfcSCv8.htm)|Aeon Resonance|Résonance d'éternité|libre|
|[class-04-p4OllLWJ2rV4sjxe.htm](feats/class-04-p4OllLWJ2rV4sjxe.htm)|Semblance of Life|Semblant de vie|libre|
|[class-04-pCVegyXxNibF4ulp.htm](feats/class-04-pCVegyXxNibF4ulp.htm)|Elaborate Flourish|Fioriture élaborée|libre|
|[class-04-pfBVx5xBfwKd1iVL.htm](feats/class-04-pfBVx5xBfwKd1iVL.htm)|Spiritual Explorer|Explorateur spirituel|libre|
|[class-04-pfF5UzT1MLW3KwHd.htm](feats/class-04-pfF5UzT1MLW3KwHd.htm)|Shared Attunement|Harmonisation partagée|libre|
|[class-04-PH5b61x3iJSKP3Xi.htm](feats/class-04-PH5b61x3iJSKP3Xi.htm)|Farabellus Flip|Retournement de Farabellus|libre|
|[class-04-pKoW1X95LjmWn5Jq.htm](feats/class-04-pKoW1X95LjmWn5Jq.htm)|Poisoner's Twist|Ajout de l'empoisonneur|libre|
|[class-04-PLz1oIEGJojVUBsW.htm](feats/class-04-PLz1oIEGJojVUBsW.htm)|Fighter Resiliency|Résilience du guerrier|officielle|
|[class-04-PNG7e39mEhq1MorG.htm](feats/class-04-PNG7e39mEhq1MorG.htm)|Basic Druid Spellcasting|Incantation basique du druide|officielle|
|[class-04-pqVG9mRKcXg5Rsjc.htm](feats/class-04-pqVG9mRKcXg5Rsjc.htm)|Thoughtform Summoning|Invocation mentalisée|libre|
|[class-04-pvFRMbIazwAO0fjH.htm](feats/class-04-pvFRMbIazwAO0fjH.htm)|Echo of the Fallen|Écho du déchu|libre|
|[class-04-PwcmmJOLY8C9JHau.htm](feats/class-04-PwcmmJOLY8C9JHau.htm)|Double Shot|Double tir|officielle|
|[class-04-pwM4RGwCTLiVSic0.htm](feats/class-04-pwM4RGwCTLiVSic0.htm)|Ghostly Resistance|Résistance fantomatique|libre|
|[class-04-Q0cTWUptV3uRIAIr.htm](feats/class-04-Q0cTWUptV3uRIAIr.htm)|Experienced Harrower|Liseur de Tourment expérimenté|libre|
|[class-04-Q1O4P1YIkCfeedHH.htm](feats/class-04-Q1O4P1YIkCfeedHH.htm)|Alchemical Shot|Tir alchimique|libre|
|[class-04-Q6EkzXkbMuuk8f7c.htm](feats/class-04-Q6EkzXkbMuuk8f7c.htm)|Lion's Might|Puissance du Lion|libre|
|[class-04-qav9ec9cR4lFcz3C.htm](feats/class-04-qav9ec9cR4lFcz3C.htm)|Disrupt Prey|Interruption de la proie|officielle|
|[class-04-qAVM65grmny3f8DP.htm](feats/class-04-qAVM65grmny3f8DP.htm)|Coughing Dragon Display|Démonstration du dragon qui tousse|libre|
|[class-04-qbH9ns3HMYBxIvEQ.htm](feats/class-04-qbH9ns3HMYBxIvEQ.htm)|Fire Resistance|Résistance au feu|libre|
|[class-04-Qfn7lmOeXfBtpG4O.htm](feats/class-04-Qfn7lmOeXfBtpG4O.htm)|Impaling Finisher|Aboutissement empalant|libre|
|[class-04-qFR5OddDBmhZe6nl.htm](feats/class-04-qFR5OddDBmhZe6nl.htm)|Chemical Purification|Purification chimique|libre|
|[class-04-qmx8QwVepqI9FqiD.htm](feats/class-04-qmx8QwVepqI9FqiD.htm)|Tandem Movement|Déplacement en tandem|libre|
|[class-04-QMycbf2StuPcUbzO.htm](feats/class-04-QMycbf2StuPcUbzO.htm)|Reach Beyond|Atteindre l'au-delà|libre|
|[class-04-qpoE2KhsPbF1ZDsx.htm](feats/class-04-qpoE2KhsPbF1ZDsx.htm)|Ravenous Charge|Charge vorace|libre|
|[class-04-qQh8wnslOagixxD1.htm](feats/class-04-qQh8wnslOagixxD1.htm)|Careful Explorer|Explorateur vigilant|officielle|
|[class-04-QrShJGrvmWPBj4oN.htm](feats/class-04-QrShJGrvmWPBj4oN.htm)|Arcane School Spell|Sort d'école arcanique|officielle|
|[class-04-qV6EuOI3UJYIL6xa.htm](feats/class-04-qV6EuOI3UJYIL6xa.htm)|Consolidated Overlay Panopticon|Superposition consolidée panoptique|libre|
|[class-04-qWb5IxkBUpJWKSLf.htm](feats/class-04-qWb5IxkBUpJWKSLf.htm)|Champion Resiliency|Résilence du champion|officielle|
|[class-04-qxh4evekG28Gt1vj.htm](feats/class-04-qxh4evekG28Gt1vj.htm)|Arcane Evolution|Évolution arcanique|officielle|
|[class-04-R56DLSqoBG8dX4Zv.htm](feats/class-04-R56DLSqoBG8dX4Zv.htm)|Scion of Domora Dedication|Dévouement : Scion de Domora|libre|
|[class-04-r6dvGxru3FWNLVE2.htm](feats/class-04-r6dvGxru3FWNLVE2.htm)|Far Shot|Tir de loin|officielle|
|[class-04-R7c4PyTNkZb0yvoT.htm](feats/class-04-R7c4PyTNkZb0yvoT.htm)|Dread Marshal Stance|Posture du terrible Capitaine|libre|
|[class-04-RAaUb9MPSDv1CGmF.htm](feats/class-04-RAaUb9MPSDv1CGmF.htm)|Perfect Weaponry|Armement parfait|libre|
|[class-04-RL4GrJ2vTrdJuzW1.htm](feats/class-04-RL4GrJ2vTrdJuzW1.htm)|Clinging Climber|Grimpeur accroché|libre|
|[class-04-RlhvppSmQRqL2RUe.htm](feats/class-04-RlhvppSmQRqL2RUe.htm)|Quick Fix|Fixer rapidement|libre|
|[class-04-rNPeOwFZE5Ma18JJ.htm](feats/class-04-rNPeOwFZE5Ma18JJ.htm)|Social Purview|Identité notoire|libre|
|[class-04-RonS3ZJs4poFTckH.htm](feats/class-04-RonS3ZJs4poFTckH.htm)|Shrink Down|Rétrécir|libre|
|[class-04-rPbh7sOhhL7i3j1z.htm](feats/class-04-rPbh7sOhhL7i3j1z.htm)|Greenwatch Initiate|Initié de la garde verte|libre|
|[class-04-RsluSLtSWq1vN8Hc.htm](feats/class-04-RsluSLtSWq1vN8Hc.htm)|Form Control|Contrôle de la forme|officielle|
|[class-04-RzElsoBGTjKWjPgY.htm](feats/class-04-RzElsoBGTjKWjPgY.htm)|Sheltering Cave|Grotte refuge|libre|
|[class-04-S5vOQ7J8DKR8sEj0.htm](feats/class-04-S5vOQ7J8DKR8sEj0.htm)|Paired Link|Lien apparié|libre|
|[class-04-sahJHnojXO9eEXVE.htm](feats/class-04-sahJHnojXO9eEXVE.htm)|Inspire Defense|Inspiration défensive|officielle|
|[class-04-SASOvOG2Nqs2ekdA.htm](feats/class-04-SASOvOG2Nqs2ekdA.htm)|Forceful Push|Poussée de force|libre|
|[class-04-SCDSgeJU7vULvHmo.htm](feats/class-04-SCDSgeJU7vULvHmo.htm)|Fast Movement|Déplacement rapide|officielle|
|[class-04-sE7x3QAel4VGdkgn.htm](feats/class-04-sE7x3QAel4VGdkgn.htm)|Basic Flair|Élégance basique|libre|
|[class-04-sEWYOllJ6rYoXK4K.htm](feats/class-04-sEWYOllJ6rYoXK4K.htm)|Raging Athlete|Athlète enragé|officielle|
|[class-04-sgaqlDFTVC7Ryurt.htm](feats/class-04-sgaqlDFTVC7Ryurt.htm)|Deflect Arrow|Parade de projectiles|officielle|
|[class-04-shDyS87L0eiabyHw.htm](feats/class-04-shDyS87L0eiabyHw.htm)|Grave's Voice|Voix du tombeau|libre|
|[class-04-sHS5LQfBHdCsv6vZ.htm](feats/class-04-sHS5LQfBHdCsv6vZ.htm)|Basic Thoughtform|Mentalisation basique|libre|
|[class-04-SHUfHzElHZPXJFiP.htm](feats/class-04-SHUfHzElHZPXJFiP.htm)|Basic Wizard Spellcasting|Incantation basique du magicien|libre|
|[class-04-SjJ8BOy5sc8p2H5E.htm](feats/class-04-SjJ8BOy5sc8p2H5E.htm)|Log Roll|Rouleur de troncs|libre|
|[class-04-sM3nmDi3PHWI64SH.htm](feats/class-04-sM3nmDi3PHWI64SH.htm)|Astral Tether|Lien astral|libre|
|[class-04-SnIx3FhBuuq6AZD0.htm](feats/class-04-SnIx3FhBuuq6AZD0.htm)|Quick Reversal|Revirement rapide|officielle|
|[class-04-so4v9xjBFaoJ8EQs.htm](feats/class-04-so4v9xjBFaoJ8EQs.htm)|Supernatural Senses|Sens surnaturels|libre|
|[class-04-Sr6CcCXceV8ALAmB.htm](feats/class-04-Sr6CcCXceV8ALAmB.htm)|Basic Fury|Furie basique|officielle|
|[class-04-sv3ywEHaab9oZ3Nj.htm](feats/class-04-sv3ywEHaab9oZ3Nj.htm)|Courageous Advance|Progression vaillante|libre|
|[class-04-SVdYJW5JsOMhAYd0.htm](feats/class-04-SVdYJW5JsOMhAYd0.htm)|Fleet Tempo|Tempo rapide|libre|
|[class-04-sVEF7j9Wh1KNGPUm.htm](feats/class-04-sVEF7j9Wh1KNGPUm.htm)|Modular Dynamo|Dynamo modulaire|libre|
|[class-04-SVhyfYNAphQPxFjd.htm](feats/class-04-SVhyfYNAphQPxFjd.htm)|Mammoth Lord Dedication|Dévouement : Seigneur des mammouths|libre|
|[class-04-TaoV7ArAuZjxpeQB.htm](feats/class-04-TaoV7ArAuZjxpeQB.htm)|Order Spell|Sort d'ordre|officielle|
|[class-04-TC6zELq2BOqVfgMh.htm](feats/class-04-TC6zELq2BOqVfgMh.htm)|Basic Synergy|Synergie basique|libre|
|[class-04-TgYs2m9scSyEJwdr.htm](feats/class-04-TgYs2m9scSyEJwdr.htm)|Vibration Sense|Perception des vibrations|libre|
|[class-04-tHqlcgcxHXzqLHPZ.htm](feats/class-04-tHqlcgcxHXzqLHPZ.htm)|Shielded Stride|Marche au bouclier|officielle|
|[class-04-tIE2umG4rQOxm8D8.htm](feats/class-04-tIE2umG4rQOxm8D8.htm)|Oversized Throw|Lancer surdimensionné|libre|
|[class-04-tIeVe9jOmxW7NgCK.htm](feats/class-04-tIeVe9jOmxW7NgCK.htm)|Distracting Spellstrike|Frappe de sort distrayante|libre|
|[class-04-TIwk07T0OxSbcOpJ.htm](feats/class-04-TIwk07T0OxSbcOpJ.htm)|Aura of Courage|Aura de courage|officielle|
|[class-04-Tln47zk8F8nswrEI.htm](feats/class-04-Tln47zk8F8nswrEI.htm)|Feast|Festoyer|libre|
|[class-04-TltRTR1e5KGly64k.htm](feats/class-04-TltRTR1e5KGly64k.htm)|Basic Martial Magic|Magie martiale basique|libre|
|[class-04-tmGsnUkPv8SIhBgn.htm](feats/class-04-tmGsnUkPv8SIhBgn.htm)|Flamboyant Athlete|Athlète flamboyant|libre|
|[class-04-TNpoEG0cUEAuSju7.htm](feats/class-04-TNpoEG0cUEAuSju7.htm)|Sun Blade|Lame solaire|libre|
|[class-04-to6s7QanfhHukW5r.htm](feats/class-04-to6s7QanfhHukW5r.htm)|Barreling Charge|Charge irrépressible|libre|
|[class-04-toFhkS9QbObxg6cp.htm](feats/class-04-toFhkS9QbObxg6cp.htm)|Versatile Signature|Polyvalence des sorts emblématiques|officielle|
|[class-04-tor4lzY0wpNcJd2U.htm](feats/class-04-tor4lzY0wpNcJd2U.htm)|Ghost Blade|Lame fantôme|libre|
|[class-04-TORYSZMLMAGgsSEW.htm](feats/class-04-TORYSZMLMAGgsSEW.htm)|Companion's Cry|Appel au compagnon|officielle|
|[class-04-Tpcq3Lk7qEOZ3LDP.htm](feats/class-04-Tpcq3Lk7qEOZ3LDP.htm)|Scout's Warning|Avertissement de l'éclaireur|officielle|
|[class-04-TUIQBw9miDowhezw.htm](feats/class-04-TUIQBw9miDowhezw.htm)|Exorcist Dedication|Dévouement : Exorciste|libre|
|[class-04-tWBK7Zbt80JlPryC.htm](feats/class-04-tWBK7Zbt80JlPryC.htm)|Bespell Weapon|Arme enchantée|officielle|
|[class-04-txo0xFYUfAjdmyjt.htm](feats/class-04-txo0xFYUfAjdmyjt.htm)|Butterfly Blade Dedication|Dévouement : Lame papillon|libre|
|[class-04-U1I87PGViGpGaP7D.htm](feats/class-04-U1I87PGViGpGaP7D.htm)|Attunement Shift|Changement d'harmonisation|libre|
|[class-04-U7dntZ2dAklzsqw8.htm](feats/class-04-U7dntZ2dAklzsqw8.htm)|Blessing of the Sun Gods|Bénédiction des dieux du soleil|libre|
|[class-04-uCElsebJ45ltmZMT.htm](feats/class-04-uCElsebJ45ltmZMT.htm)|Mature Beastmaster Companion|Compagnon animal adulte (maître des bêtes)|libre|
|[class-04-uhU0KajD09h5bw4e.htm](feats/class-04-uhU0KajD09h5bw4e.htm)|Acclimatization|Acclimatation|libre|
|[class-04-uisI6b7Ua5zSHDwj.htm](feats/class-04-uisI6b7Ua5zSHDwj.htm)|Psi Strikes|Frappes psy|libre|
|[class-04-UjWLK86BgHxz3Itd.htm](feats/class-04-UjWLK86BgHxz3Itd.htm)|Calculated Splash|Éclaboussure calculée|officielle|
|[class-04-uKBT0D9gxdwMcwNl.htm](feats/class-04-uKBT0D9gxdwMcwNl.htm)|Basic Devotion|Dévotion basique|officielle|
|[class-04-uMjczqcXuteoP7lf.htm](feats/class-04-uMjczqcXuteoP7lf.htm)|Claws Of The Dragon|Griffes de dragon|libre|
|[class-04-UsEGem9s9ElaTS0d.htm](feats/class-04-UsEGem9s9ElaTS0d.htm)|Voice of Authority|Voix de l'autorité|libre|
|[class-04-uT9RnHLgIIcm7Hhs.htm](feats/class-04-uT9RnHLgIIcm7Hhs.htm)|Trapsmith Dedication|Dévouement : Fabricant de pièges|libre|
|[class-04-UWG1USE0L2ZxEPiO.htm](feats/class-04-UWG1USE0L2ZxEPiO.htm)|Wholeness of Body|Plénitude physique|officielle|
|[class-04-Ux0DSklFlIlcvnhO.htm](feats/class-04-Ux0DSklFlIlcvnhO.htm)|Sacred Ki|Ki sacré|libre|
|[class-04-uY03kVQBA81gbTj9.htm](feats/class-04-uY03kVQBA81gbTj9.htm)|Emergency Targe|Targe de secours|libre|
|[class-04-UyMQ1X8KLSZvm7AT.htm](feats/class-04-UyMQ1X8KLSZvm7AT.htm)|Wayfinder Resonance Tinkerer|Bricoleur de résonance de guide|officielle|
|[class-04-v6wYNnkoVDquzRpw.htm](feats/class-04-v6wYNnkoVDquzRpw.htm)|Spyglass Modification|Modification de lunette|libre|
|[class-04-Vab3XIirjs3KQh3t.htm](feats/class-04-Vab3XIirjs3KQh3t.htm)|Cavalier's Charge|Charge du cavalier|libre|
|[class-04-vbHF6HEC9jQorFGl.htm](feats/class-04-vbHF6HEC9jQorFGl.htm)|Environmental Explorer|Explorateur environnemental|libre|
|[class-04-vCsvT7xqBIolF7zH.htm](feats/class-04-vCsvT7xqBIolF7zH.htm)|Command Corpse|Contrôler les cadavres|libre|
|[class-04-vDeG0N4kzTBDTC2b.htm](feats/class-04-vDeG0N4kzTBDTC2b.htm)|Sabotage|Sabotage|officielle|
|[class-04-vKzAIJuyr9SU2JzU.htm](feats/class-04-vKzAIJuyr9SU2JzU.htm)|Safe House|Planque sûre|libre|
|[class-04-VP2CWUTZ9Edg82uz.htm](feats/class-04-VP2CWUTZ9Edg82uz.htm)|Swift Leap|Bond rapide|libre|
|[class-04-vPZxFpq7XkRmE3Uc.htm](feats/class-04-vPZxFpq7XkRmE3Uc.htm)|Black Powder Boost|Renfort de poudre noire|libre|
|[class-04-vQ4DNfpktmaqdgdM.htm](feats/class-04-vQ4DNfpktmaqdgdM.htm)|Expanded Domain Initiate|Initié du domaine étendu|libre|
|[class-04-vUQ3XwCT0i3ydX1U.htm](feats/class-04-vUQ3XwCT0i3ydX1U.htm)|Always Ready|Toujours prêt|libre|
|[class-04-vVqOBzWTpWirPbrK.htm](feats/class-04-vVqOBzWTpWirPbrK.htm)|Seeker of Truths|Chercheur de vérités|libre|
|[class-04-VVsYBmVi2E1u9E5Z.htm](feats/class-04-VVsYBmVi2E1u9E5Z.htm)|Reactive Pursuit|Poursuite réactive|officielle|
|[class-04-vxA0VRN10OwUkGAr.htm](feats/class-04-vxA0VRN10OwUkGAr.htm)|Cruelty|Cruauté|libre|
|[class-04-W7IfFi6MkTDfO2hb.htm](feats/class-04-W7IfFi6MkTDfO2hb.htm)|Disengaging Twist|Torsion de désengagement|libre|
|[class-04-wB1ONG2uO7RnD1iC.htm](feats/class-04-wB1ONG2uO7RnD1iC.htm)|Flying Kick|Coup de pied sauté|officielle|
|[class-04-WBp5ybj3kLcsGdVr.htm](feats/class-04-WBp5ybj3kLcsGdVr.htm)|Claws Of The Dragon (Draconic Bloodline)|Griffes de dragon (Lignage draconique)|libre|
|[class-04-wBqQsXzqObrZM9Va.htm](feats/class-04-wBqQsXzqObrZM9Va.htm)|Basic Bloodline Spell|Sort de lignage basique|officielle|
|[class-04-wbS8f7R7KqHkwOzM.htm](feats/class-04-wbS8f7R7KqHkwOzM.htm)|Sacral Lord|Seigneur sacré|libre|
|[class-04-WHOCaVobY7N3UTtA.htm](feats/class-04-WHOCaVobY7N3UTtA.htm)|Divine Evolution|Évolution divine|officielle|
|[class-04-wjnfdh6WzN7HbmeE.htm](feats/class-04-wjnfdh6WzN7HbmeE.htm)|Aura Of Despair|Aura de désespoir|libre|
|[class-04-woQhda2ZoO1GMYNz.htm](feats/class-04-woQhda2ZoO1GMYNz.htm)|Devrin's Dazzling Diversion|Diversion éblouissante de Devrin|libre|
|[class-04-WPHq7MWHlGWpTnme.htm](feats/class-04-WPHq7MWHlGWpTnme.htm)|Strange Script|Texte étrange|libre|
|[class-04-wqrOVv9gnqF4nlLR.htm](feats/class-04-wqrOVv9gnqF4nlLR.htm)|Surprise Snare|Piège artisanal surprise|libre|
|[class-04-wscmghwNCXvZtKsz.htm](feats/class-04-wscmghwNCXvZtKsz.htm)|Rescuer's Press|Poussée du sauveteur|libre|
|[class-04-ww5AM2yFs0lqQhmD.htm](feats/class-04-ww5AM2yFs0lqQhmD.htm)|Vision Of Weakness|Vision de faiblesse|libre|
|[class-04-wz2edbLFnDKDNWWZ.htm](feats/class-04-wz2edbLFnDKDNWWZ.htm)|Flurry of Maneuvers|Déluges de manoeuvres|officielle|
|[class-04-X0NFLIn1bqj6bnd0.htm](feats/class-04-X0NFLIn1bqj6bnd0.htm)|Basic Deduction|Déduction basique|libre|
|[class-04-X5gNhaYNx1xu6NoH.htm](feats/class-04-X5gNhaYNx1xu6NoH.htm)|Finishing Precision|Finition précise|libre|
|[class-04-x62kd1CYJNqO2ZsS.htm](feats/class-04-x62kd1CYJNqO2ZsS.htm)|Magic Warrior Transformation|Transformation de guerrier magique|officielle|
|[class-04-Xb8CyW9sYS27ElcC.htm](feats/class-04-Xb8CyW9sYS27ElcC.htm)|Lucky Escape|Évitement chanceux|libre|
|[class-04-XgMV11C8W72SX9Yg.htm](feats/class-04-XgMV11C8W72SX9Yg.htm)|Patch Job|Travail d'assemblage|libre|
|[class-04-XGZUjc9I3sjfniDg.htm](feats/class-04-XGZUjc9I3sjfniDg.htm)|Dual Energy Heart|Coeur énergétique dual|libre|
|[class-04-Xjn4iYzJ6rLIpLV3.htm](feats/class-04-Xjn4iYzJ6rLIpLV3.htm)|Barrier Shield|Bouclier barrière|libre|
|[class-04-xKKyS9nEQqolJ0SF.htm](feats/class-04-xKKyS9nEQqolJ0SF.htm)|Psychic Duelist Dedication|Dévouement : Duelliste psychique|libre|
|[class-04-XkYNCFdZMjZTw6nn.htm](feats/class-04-XkYNCFdZMjZTw6nn.htm)|Giant Hunter|Chasseur de géant|libre|
|[class-04-xlparPCGhkgjdhx2.htm](feats/class-04-xlparPCGhkgjdhx2.htm)|Defend Summoner|Défendre le conjurateur|libre|
|[class-04-xMTC8UOerwGyCtaN.htm](feats/class-04-xMTC8UOerwGyCtaN.htm)|Basic Trickery|Ruse basique|officielle|
|[class-04-xRTlbvvBzURgC6M2.htm](feats/class-04-xRTlbvvBzURgC6M2.htm)|Quick Shot|Tir rapide|libre|
|[class-04-xSYkYRKlSyLzPrH1.htm](feats/class-04-xSYkYRKlSyLzPrH1.htm)|Undying Conviction|Conviction immortelle|libre|
|[class-04-xtRcWpprFBiXeCOB.htm](feats/class-04-xtRcWpprFBiXeCOB.htm)|Triangulate|Triangulation|libre|
|[class-04-xUaEpnfd1FMGNG1z.htm](feats/class-04-xUaEpnfd1FMGNG1z.htm)|Frightful Moan|Gémissement terrifiant|libre|
|[class-04-XWtNGOkMHcrdrRw8.htm](feats/class-04-XWtNGOkMHcrdrRw8.htm)|Dual-Form Weapon|Arme à forme double|libre|
|[class-04-xXHoyccK5ZG2AJKg.htm](feats/class-04-xXHoyccK5ZG2AJKg.htm)|Skilled Partner|Partenaire compétent|libre|
|[class-04-xXHwktc9SymSY8d6.htm](feats/class-04-xXHwktc9SymSY8d6.htm)|Healing Bomb|Bombe de guérison|libre|
|[class-04-xxvKWQa2olCHoJ03.htm](feats/class-04-xxvKWQa2olCHoJ03.htm)|Basic Skysage Divination|Divination basique du Sagecéleste|libre|
|[class-04-xYakFeP6olBsxpZN.htm](feats/class-04-xYakFeP6olBsxpZN.htm)|Command Undead|Contrôle des morts-vivants|officielle|
|[class-04-Y4gYfrxjSir2Enui.htm](feats/class-04-Y4gYfrxjSir2Enui.htm)|Perfect Strike|Coup parfait|officielle|
|[class-04-Y8LHfkzGyOhPlUou.htm](feats/class-04-Y8LHfkzGyOhPlUou.htm)|Twin Parry|Parade jumelée|officielle|
|[class-04-yantkiDNmsT5ONZX.htm](feats/class-04-yantkiDNmsT5ONZX.htm)|Mental Forge|Forge mentale|libre|
|[class-04-YehcIyxY5KnhPlx5.htm](feats/class-04-YehcIyxY5KnhPlx5.htm)|Basic Thaumaturgy|Thaumaturgie basique|libre|
|[class-04-YhclkX1nfyUU8RtO.htm](feats/class-04-YhclkX1nfyUU8RtO.htm)|Remember Your Training|Entraînement mémorable|libre|
|[class-04-YJIzE2RhGRGfbt9j.htm](feats/class-04-YJIzE2RhGRGfbt9j.htm)|Guardian's Deflection (Swashbuckler)|Déviation du gardien (Bretteur)|libre|
|[class-04-yMj9WfPctWQC7be2.htm](feats/class-04-yMj9WfPctWQC7be2.htm)|Thaumaturgic Ritualist|Ritualiste thaumaturgique|libre|
|[class-04-yozSCfdLFHVBbTxj.htm](feats/class-04-yozSCfdLFHVBbTxj.htm)|Mature Animal Companion (Druid)|Compagnon animal adulte (druide)|officielle|
|[class-04-yqMBLt0GUTwiqnZT.htm](feats/class-04-yqMBLt0GUTwiqnZT.htm)|Cathartic Focus Spell|Sort focalisé cathartique|libre|
|[class-04-yUZXOEGqqXKhkGNE.htm](feats/class-04-yUZXOEGqqXKhkGNE.htm)|Advanced Reanimated Companion|Compagnon réanimé avancé|libre|
|[class-04-YYTZFBT2WZMU14om.htm](feats/class-04-YYTZFBT2WZMU14om.htm)|Lifelink Surge|Afflux du lien vital|libre|
|[class-04-z3ycxqT1XvjfQ0Oq.htm](feats/class-04-z3ycxqT1XvjfQ0Oq.htm)|Take the Wheel|Prendre le volant|libre|
|[class-04-zBLrZE5aCkpaTK2N.htm](feats/class-04-zBLrZE5aCkpaTK2N.htm)|Additional Companion|Compagnon supplémentaire|libre|
|[class-04-zbnL5OP4zVaNFcq8.htm](feats/class-04-zbnL5OP4zVaNFcq8.htm)|Snap Out Of It! (Marshal)|Secoue-toi ! (Capitaine)|libre|
|[class-04-Ze9vb0wlRmWWqnXC.htm](feats/class-04-Ze9vb0wlRmWWqnXC.htm)|Jalmeri Heavenseeker Dedication|Dévouement : Chercheur du Paradis du Jalmeray|libre|
|[class-04-zfTmb78yGZzNpgU3.htm](feats/class-04-zfTmb78yGZzNpgU3.htm)|Dual Thrower|Lanceur à deux mains|libre|
|[class-04-zgljg4gVI6i1Fpb5.htm](feats/class-04-zgljg4gVI6i1Fpb5.htm)|Paired Shots|Tirs jumelés|libre|
|[class-04-ZHPSASbvbbshq1zG.htm](feats/class-04-ZHPSASbvbbshq1zG.htm)|Leading Dance|Mener la danse|libre|
|[class-04-zilwynzk8lIujgZo.htm](feats/class-04-zilwynzk8lIujgZo.htm)|Mercy|Soulagement|officielle|
|[class-04-zWEu9xuAxBnPoSrv.htm](feats/class-04-zWEu9xuAxBnPoSrv.htm)|Disturbing Defense|Défense perturbante|libre|
|[class-04-zxlbMEaXglMVsLpQ.htm](feats/class-04-zxlbMEaXglMVsLpQ.htm)|Tunnel Wall|Mur tunnel|libre|
|[class-06-00uTUJPgJ6kuGR8O.htm](feats/class-06-00uTUJPgJ6kuGR8O.htm)|Crown of the Saumen Kar|Couronne du saumen kar|libre|
|[class-06-03mVGvudDLyGEpTZ.htm](feats/class-06-03mVGvudDLyGEpTZ.htm)|Spirit's Absolution|Absolution spirituelle|libre|
|[class-06-04RgXKFVC2A6Ryn6.htm](feats/class-06-04RgXKFVC2A6Ryn6.htm)|Surprise Strike|Frappe surprise|libre|
|[class-06-0Gao1ez4dGH6dIZ2.htm](feats/class-06-0Gao1ez4dGH6dIZ2.htm)|Expeditious Advance|Accélération motrice|officielle|
|[class-06-0idzh4dww7B7cbnW.htm](feats/class-06-0idzh4dww7B7cbnW.htm)|Reloading Trick|Astuce de rechargement|libre|
|[class-06-0qGLCpggCcOVkbtT.htm](feats/class-06-0qGLCpggCcOVkbtT.htm)|Dodge Away|S'esquiver au loin|libre|
|[class-06-0SbHdwYumvmzwWw3.htm](feats/class-06-0SbHdwYumvmzwWw3.htm)|Piston Punch|Coup de poing piston|libre|
|[class-06-0UdHPOv3DX8TY9yb.htm](feats/class-06-0UdHPOv3DX8TY9yb.htm)|Snap Shot|Tir soudain|officielle|
|[class-06-0zSoSPwC4cpqRewj.htm](feats/class-06-0zSoSPwC4cpqRewj.htm)|Song Of Marching|Chanson de marche|libre|
|[class-06-10DbphslCihq8mxQ.htm](feats/class-06-10DbphslCihq8mxQ.htm)|Ki Blast|Explosion ki|officielle|
|[class-06-18UQefmhcNq6tFav.htm](feats/class-06-18UQefmhcNq6tFav.htm)|Rapid Manifestation|Manifestation rapide|libre|
|[class-06-1p5ErCp33nGOzEsk.htm](feats/class-06-1p5ErCp33nGOzEsk.htm)|Disarming Stance|Posture désarmante|officielle|
|[class-06-1YFrl8I6ZGo7BIM9.htm](feats/class-06-1YFrl8I6ZGo7BIM9.htm)|Knight Vigilant Dedication|Dévouement : Chevalier vigilant|libre|
|[class-06-21YWBdoXGmj60vdI.htm](feats/class-06-21YWBdoXGmj60vdI.htm)|Harmonize|Harmoniser|officielle|
|[class-06-2HoDwBAmPIAoKUVF.htm](feats/class-06-2HoDwBAmPIAoKUVF.htm)|Dazing Blow|Coup étourdissant|libre|
|[class-06-2nSk6oOLBXCEbAhc.htm](feats/class-06-2nSk6oOLBXCEbAhc.htm)|Vacate Vision|Vision vide|libre|
|[class-06-37CUdnWwJCfOCC2H.htm](feats/class-06-37CUdnWwJCfOCC2H.htm)|Thunder Clap|Coup de tonnerre|libre|
|[class-06-3PHHiZjX16Dwyt65.htm](feats/class-06-3PHHiZjX16Dwyt65.htm)|Analyze Weakness|Analyse de faiblesse|libre|
|[class-06-3uavnVbCsqTvzpgt.htm](feats/class-06-3uavnVbCsqTvzpgt.htm)|Dragon's Rage Breath|Souffle de rage du dragon|officielle|
|[class-06-3vjOXL9ZD4ibaJL6.htm](feats/class-06-3vjOXL9ZD4ibaJL6.htm)|Green Empathy|Empathie végétale|officielle|
|[class-06-4I1Kq53Qfzrrmg2E.htm](feats/class-06-4I1Kq53Qfzrrmg2E.htm)|Skirmish Strike|Frappe d'escarmouche|officielle|
|[class-06-4o9g5g12yyrfZ3Xd.htm](feats/class-06-4o9g5g12yyrfZ3Xd.htm)|Light Step|Pas léger|officielle|
|[class-06-4T9HHOdTk3yVbeoO.htm](feats/class-06-4T9HHOdTk3yVbeoO.htm)|Instinct Ability|Pouvoir d'instinct|officielle|
|[class-06-515N9nl9ChZwLWKR.htm](feats/class-06-515N9nl9ChZwLWKR.htm)|Defensive Coordination|Coordination défensive|libre|
|[class-06-52cygjzHfSD0YhEA.htm](feats/class-06-52cygjzHfSD0YhEA.htm)|Spell Penetration|Efficacité des sorts accrue|officielle|
|[class-06-52QyoGaysrfBOy5H.htm](feats/class-06-52QyoGaysrfBOy5H.htm)|Witch's Charge|Fardeau du sorcier|libre|
|[class-06-55wEtw47Zl11uqlr.htm](feats/class-06-55wEtw47Zl11uqlr.htm)|Numb|Engourdissement|libre|
|[class-06-588O3jurogttvqgm.htm](feats/class-06-588O3jurogttvqgm.htm)|Eidolon's Opportunity|Opportunité de l'eidolon|libre|
|[class-06-5Jc2ySGLVi053qpz.htm](feats/class-06-5Jc2ySGLVi053qpz.htm)|Twist the Knife|Remuer le couteau|officielle|
|[class-06-5N6rLz4mdJg0NrQH.htm](feats/class-06-5N6rLz4mdJg0NrQH.htm)|Anticipate Ambush|Embuscade anticipée|libre|
|[class-06-5vXc2s2siR1ihpBT.htm](feats/class-06-5vXc2s2siR1ihpBT.htm)|Shield Warden (Fighter)|Gardien au bouclier (Guerrier)|officielle|
|[class-06-62hpJOuvYYSa4X7u.htm](feats/class-06-62hpJOuvYYSa4X7u.htm)|Hellknight Signifer Dedication|Dévouement : Signifer|libre|
|[class-06-64DgI5CHP7K9kbbg.htm](feats/class-06-64DgI5CHP7K9kbbg.htm)|Practiced Opposition|Adversité exercée|libre|
|[class-06-65gjc5KE4ZRoocbi.htm](feats/class-06-65gjc5KE4ZRoocbi.htm)|Abundant Step|Pas chassé|officielle|
|[class-06-6gLWr3xghsHSFwxc.htm](feats/class-06-6gLWr3xghsHSFwxc.htm)|Water Step|Foulée sur l'eau|officielle|
|[class-06-6iDd7CTzxkvMp6lB.htm](feats/class-06-6iDd7CTzxkvMp6lB.htm)|Align Ki|Ki aligné|libre|
|[class-06-6J2ZSGNsXPKPcJGV.htm](feats/class-06-6J2ZSGNsXPKPcJGV.htm)|Tiger Slash|Taillade du tigre|officielle|
|[class-06-6vpgAMj87J6cWN0j.htm](feats/class-06-6vpgAMj87J6cWN0j.htm)|Drenching Mist|Brume détrempeuse|libre|
|[class-06-74PGpEAVNS5xUDA3.htm](feats/class-06-74PGpEAVNS5xUDA3.htm)|Clinch Strike|Frappe de dégagement|libre|
|[class-06-7fU6e3HIT4NvwLYa.htm](feats/class-06-7fU6e3HIT4NvwLYa.htm)|Halcyon Speaker Dedication|Dévouement : Orateur syncrétique|libre|
|[class-06-7GrACprIxZuarGDs.htm](feats/class-06-7GrACprIxZuarGDs.htm)|Grave Strength|Force de la tombe|libre|
|[class-06-7JjNWSuutkjrMrd0.htm](feats/class-06-7JjNWSuutkjrMrd0.htm)|Advanced Devotion|Dévotion avancée|officielle|
|[class-06-7KT4huf0iPaBGD7R.htm](feats/class-06-7KT4huf0iPaBGD7R.htm)|Combination Finisher|Aboutissement combiné|libre|
|[class-06-7NdMHszAiiveihoW.htm](feats/class-06-7NdMHszAiiveihoW.htm)|Armor Specialist|Spécialiste de l'armure|libre|
|[class-06-7O0PrMoXd5L8dRfg.htm](feats/class-06-7O0PrMoXd5L8dRfg.htm)|Archaeologist's Warning|Avertissement de l'archéologue|libre|
|[class-06-7RcCvziQBLL7Bumu.htm](feats/class-06-7RcCvziQBLL7Bumu.htm)|Advanced Bow Training|Entraînement à l'arc avancé|libre|
|[class-06-7SwpVfxSkPMPMazJ.htm](feats/class-06-7SwpVfxSkPMPMazJ.htm)|Feeling Your Oats|Nouvellement enhardi|libre|
|[class-06-7wk6yVM3OJdc4LEU.htm](feats/class-06-7wk6yVM3OJdc4LEU.htm)|Champion's Reaction|Réaction de champion|officielle|
|[class-06-8rKzUpDxAi8tMk7I.htm](feats/class-06-8rKzUpDxAi8tMk7I.htm)|Liberate Soul|Libération d'âme|libre|
|[class-06-8RppI1i4LfI0CYsX.htm](feats/class-06-8RppI1i4LfI0CYsX.htm)|Aldori Riposte|Riposte aldorie|officielle|
|[class-06-8x3wqCZgYzJiSyR1.htm](feats/class-06-8x3wqCZgYzJiSyR1.htm)|Dazzling Bullet|Balle éblouissante|libre|
|[class-06-9C6a6FXuPqWjXy8K.htm](feats/class-06-9C6a6FXuPqWjXy8K.htm)|Improved Familiar (Familiar Master)|Familier amélioré (Maître familier)|libre|
|[class-06-9cHQua33V35JPE3U.htm](feats/class-06-9cHQua33V35JPE3U.htm)|Educate Allies|Alliés instruits|libre|
|[class-06-9CXQhg4YprPhqzoL.htm](feats/class-06-9CXQhg4YprPhqzoL.htm)|Vexing Tumble|Acrobaties vexantes|libre|
|[class-06-9DWxzEavOeymc6Ql.htm](feats/class-06-9DWxzEavOeymc6Ql.htm)|Spiritual Strike|Frappe spirituelle|libre|
|[class-06-9E1FLGp4CNBEwiZE.htm](feats/class-06-9E1FLGp4CNBEwiZE.htm)|Relentless Disarm|Désarmement acharné|officielle|
|[class-06-9p4oFIn791VAmzUn.htm](feats/class-06-9p4oFIn791VAmzUn.htm)|Cauterize|Cautériser|libre|
|[class-06-9xcvJn3lWXwbkU52.htm](feats/class-06-9xcvJn3lWXwbkU52.htm)|Watch This!|Regarde çà !|libre|
|[class-06-a9zzu4kb7vstq0HQ.htm](feats/class-06-a9zzu4kb7vstq0HQ.htm)|Cadence Call|Imprimer la cadence|libre|
|[class-06-adr9buwVuxgZV2B3.htm](feats/class-06-adr9buwVuxgZV2B3.htm)|Share Burden|Partager le fardeau|libre|
|[class-06-ajesR7y0jWzqjAgc.htm](feats/class-06-ajesR7y0jWzqjAgc.htm)|Current Spell|Courant de sort|libre|
|[class-06-aQjWHbjK1pk8HESM.htm](feats/class-06-aQjWHbjK1pk8HESM.htm)|Perfect Ki Adept|Parfait adepte du ki|libre|
|[class-06-AWd7uZBfba5mNXeT.htm](feats/class-06-AWd7uZBfba5mNXeT.htm)|Syu Tak-nwa's Deadly Hair|Cheveux mortel de Syu Tak-nwa|libre|
|[class-06-axGS5bBJ9vl5AePc.htm](feats/class-06-axGS5bBJ9vl5AePc.htm)|Phase Out|Déphasage|libre|
|[class-06-axvGLVoAvuD9jU78.htm](feats/class-06-axvGLVoAvuD9jU78.htm)|Loyal Warhorse|Cheval de bataille fidèle|officielle|
|[class-06-b532qrlTUqWxLd1j.htm](feats/class-06-b532qrlTUqWxLd1j.htm)|Shield Warden (Champion)|Gardien au bouclier (Champion)|officielle|
|[class-06-b7isszc8C75V3okn.htm](feats/class-06-b7isszc8C75V3okn.htm)|Sticky Poison|Poison collant|libre|
|[class-06-Ba6SLqAghsZgqhua.htm](feats/class-06-Ba6SLqAghsZgqhua.htm)|Agile Maneuvers|Manoeuvres agiles|libre|
|[class-06-BBN5G6epRWXGwZHv.htm](feats/class-06-BBN5G6epRWXGwZHv.htm)|Ephemeral Tracking|Pistage éphémère|libre|
|[class-06-bi9w4Z9MQY8VWLZF.htm](feats/class-06-bi9w4Z9MQY8VWLZF.htm)|Animate Net|Animation de filet|libre|
|[class-06-BkEOwv3SRtefczpO.htm](feats/class-06-BkEOwv3SRtefczpO.htm)|Phalanx Breaker|Briseur de phalange|libre|
|[class-06-bmWvMfYxZbZtigDp.htm](feats/class-06-bmWvMfYxZbZtigDp.htm)|Flexible Ritualist|Ritualiste polyvalent|libre|
|[class-06-BQzExsEZrwGsJD66.htm](feats/class-06-BQzExsEZrwGsJD66.htm)|Endemic Herbs|Herbes endémiques|libre|
|[class-06-bSXcyu7ExWq9qUzG.htm](feats/class-06-bSXcyu7ExWq9qUzG.htm)|Convincing Illusion|Illusion convaincante|libre|
|[class-06-bTQVvkEuPj9QAiAi.htm](feats/class-06-bTQVvkEuPj9QAiAi.htm)|Time Mage Dedication|Dévouement : Mage temporel|libre|
|[class-06-bVng7Mkrj4UnQzLo.htm](feats/class-06-bVng7Mkrj4UnQzLo.htm)|Burning Spell|Sort brûlant|libre|
|[class-06-BxO5l9tH9y1xNzzi.htm](feats/class-06-BxO5l9tH9y1xNzzi.htm)|Advanced Thoughtform|Mentalisation avancée|libre|
|[class-06-bzKBUK6CH8tuLCfo.htm](feats/class-06-bzKBUK6CH8tuLCfo.htm)|Advanced Red Mantis Magic|Magie avancée des Mantes rouges|officielle|
|[class-06-c5ns35FLvvxjimuH.htm](feats/class-06-c5ns35FLvvxjimuH.htm)|Swift Tracker|Pistage accéléré|officielle|
|[class-06-c5SfaSn6OEHkHxII.htm](feats/class-06-c5SfaSn6OEHkHxII.htm)|Feverish Enzymes|Enzymes fiévreuses|libre|
|[class-06-c6CS97Zs0DPmInaI.htm](feats/class-06-c6CS97Zs0DPmInaI.htm)|Assured Knowledge|Connaissance assurée|libre|
|[class-06-cBYTVYqw1EFVEuzs.htm](feats/class-06-cBYTVYqw1EFVEuzs.htm)|Bolera's Interrogation|Interrogatoire de Boléra|libre|
|[class-06-Ckglzh4dXcGWPNS3.htm](feats/class-06-Ckglzh4dXcGWPNS3.htm)|Beastmaster's Trance|Transe du Maître des bêtes|libre|
|[class-06-ckukkEJj4Lc3ENjr.htm](feats/class-06-ckukkEJj4Lc3ENjr.htm)|Tap Vitality|Puiser la vitalité|libre|
|[class-06-CmA8t7MRMOzLTeUj.htm](feats/class-06-CmA8t7MRMOzLTeUj.htm)|Advanced Breakthrough|Percée avancée|libre|
|[class-06-cny7ouhsoiNsWJ7X.htm](feats/class-06-cny7ouhsoiNsWJ7X.htm)|Daring Act|Acte culotté|libre|
|[class-06-CoVBrlyMToANvt2v.htm](feats/class-06-CoVBrlyMToANvt2v.htm)|Stone Blood|Sang de pierre|officielle|
|[class-06-cTQMtd2IVlvgJwAn.htm](feats/class-06-cTQMtd2IVlvgJwAn.htm)|Slice and Swipe|Couper et dérober|libre|
|[class-06-CvMCw6JqvgMPE5uk.htm](feats/class-06-CvMCw6JqvgMPE5uk.htm)|Blood Frenzy|Frénésie hémorragique|libre|
|[class-06-cXuycCR29sFLBjbo.htm](feats/class-06-cXuycCR29sFLBjbo.htm)|Betraying Shank|Lame traitresse|libre|
|[class-06-d00Ip4YHVmk2tecD.htm](feats/class-06-d00Ip4YHVmk2tecD.htm)|Sympathetic Vulnerabilities|Vulnérabilités sympathiques|libre|
|[class-06-dCCVqIcYrhCp3Bzl.htm](feats/class-06-dCCVqIcYrhCp3Bzl.htm)|Sound Mirror|Miroir sonore|libre|
|[class-06-dDFQJem5K9Jzxgda.htm](feats/class-06-dDFQJem5K9Jzxgda.htm)|Expert Fireworks Crafter|Fabricant de feux d'artifice expert|libre|
|[class-06-df4cBV3qZn3qNUmP.htm](feats/class-06-df4cBV3qZn3qNUmP.htm)|Rallying Charge|Charge de ralliement|libre|
|[class-06-dHUoQVzDa9Cf4QCG.htm](feats/class-06-dHUoQVzDa9Cf4QCG.htm)|Reflexive Shield|Bouclier instinctif|officielle|
|[class-06-dMKx0T629hpJCN8T.htm](feats/class-06-dMKx0T629hpJCN8T.htm)|Sixth Sense|Sixième sens|libre|
|[class-06-DPk7a0cdFOjDOdn5.htm](feats/class-06-DPk7a0cdFOjDOdn5.htm)|Advanced Elemental Spell|Sort élémentaire avancé|libre|
|[class-06-DpmdmRNMg6LZpNB0.htm](feats/class-06-DpmdmRNMg6LZpNB0.htm)|Simple Crystal Magic|Magie des cristaux basique|officielle|
|[class-06-dPmJ91qawZW2U8K3.htm](feats/class-06-dPmJ91qawZW2U8K3.htm)|Staff Sweep|Balayage au bâton|libre|
|[class-06-DR6vessIpXTLM6Xa.htm](feats/class-06-DR6vessIpXTLM6Xa.htm)|Crowd Mastery|Maître des foules|officielle|
|[class-06-dVzPTpZoGSi5NR6y.htm](feats/class-06-dVzPTpZoGSi5NR6y.htm)|Cast Out|Bannissement|libre|
|[class-06-dwL0Y0P2x4pn2cft.htm](feats/class-06-dwL0Y0P2x4pn2cft.htm)|Bellflower Dedication|Dévouement : Campanule|libre|
|[class-06-dxUbA1dUEcVHnU5s.htm](feats/class-06-dxUbA1dUEcVHnU5s.htm)|Clockwork Celerity|Célérité mécanique|libre|
|[class-06-E5ewlRE6Mh9ZqUMu.htm](feats/class-06-E5ewlRE6Mh9ZqUMu.htm)|Ostentatious Arrival|Arrivée ostentatoire|libre|
|[class-06-Ea6Z5cxeBCCtPD5R.htm](feats/class-06-Ea6Z5cxeBCCtPD5R.htm)|Ranger's Bramble|Roncier du rôdeur|libre|
|[class-06-EBmZyzDWhFSLydlM.htm](feats/class-06-EBmZyzDWhFSLydlM.htm)|Storming Breath|Souffle tempêtueux (Déviant)|libre|
|[class-06-ef57tIj30IaPnSgC.htm](feats/class-06-ef57tIj30IaPnSgC.htm)|Spiral Sworn|Spirale assermentée|libre|
|[class-06-eHkMcQQ4ejRAFJAt.htm](feats/class-06-eHkMcQQ4ejRAFJAt.htm)|Poison Coat|Recouvert de poison|libre|
|[class-06-eHvJqBHYqx7UjpPg.htm](feats/class-06-eHvJqBHYqx7UjpPg.htm)|Perpetual Scout|Éclaireur perpétuel|libre|
|[class-06-Ek1CoyGKxsozDsaD.htm](feats/class-06-Ek1CoyGKxsozDsaD.htm)|Guide the Timeline|Guider le flot temporel|libre|
|[class-06-elbj75qsUerbM725.htm](feats/class-06-elbj75qsUerbM725.htm)|Scout's Speed|Vitesse de l'éclaireur|libre|
|[class-06-em3glccrJ5ZIf8Uq.htm](feats/class-06-em3glccrJ5ZIf8Uq.htm)|Keep Pace (Game Hunter)|Conserver le rythme (chasseur de gros gibier)|libre|
|[class-06-eNeSl5UNaqDwyNkp.htm](feats/class-06-eNeSl5UNaqDwyNkp.htm)|Eidolon's Wrath|Courroux de l'eidolon|libre|
|[class-06-eTqWgfojpvuigdvx.htm](feats/class-06-eTqWgfojpvuigdvx.htm)|Advanced Muse's Whispers|Murmures de la muse avancés|officielle|
|[class-06-euWpgjPNcDjeXAWQ.htm](feats/class-06-euWpgjPNcDjeXAWQ.htm)|Advanced Maneuver|Manoeuvre avancée|officielle|
|[class-06-eXNkcM7gtCGC7udi.htm](feats/class-06-eXNkcM7gtCGC7udi.htm)|Grave Sense|Perception du tombeau|officielle|
|[class-06-exVjUkpAdbzJsxxg.htm](feats/class-06-exVjUkpAdbzJsxxg.htm)|Tandem Strike|Frappe en tandem|libre|
|[class-06-eZrftEihfuJBldG5.htm](feats/class-06-eZrftEihfuJBldG5.htm)|Advanced Bloodline|Lignage avancé|officielle|
|[class-06-F1DVDJRARfdb1Kjz.htm](feats/class-06-F1DVDJRARfdb1Kjz.htm)|Storm Retribution|Châtiment de la tempête|officielle|
|[class-06-f754txt1ZyhVWXHk.htm](feats/class-06-f754txt1ZyhVWXHk.htm)|Advanced Hunter's Trick|Astuce du chasseur avancée|officielle|
|[class-06-FGdKV40UiS6jvBmI.htm](feats/class-06-FGdKV40UiS6jvBmI.htm)|Exploit Opening|Exploiter l'ouverture|libre|
|[class-06-FIVuc2TRaLXiCGkn.htm](feats/class-06-FIVuc2TRaLXiCGkn.htm)|Advanced Thaumaturgy|Thaumaturgie avancée|libre|
|[class-06-FJdcQU6yjDVDBC4r.htm](feats/class-06-FJdcQU6yjDVDBC4r.htm)|Tempest-Sun Redirection|Redirection du soleil tempétueux|libre|
|[class-06-FkOtiB52wIOi7SP7.htm](feats/class-06-FkOtiB52wIOi7SP7.htm)|Adaptive Mask Familiar|Familier masque adaptatif|libre|
|[class-06-fngPLUD4Sltho2kn.htm](feats/class-06-fngPLUD4Sltho2kn.htm)|Staggering Fire|Feu ralentissant|libre|
|[class-06-FNO2hfGmxqJngD4A.htm](feats/class-06-FNO2hfGmxqJngD4A.htm)|Drifter's Juke|Esquive du vagabond|libre|
|[class-06-Fpsd8Y8qJ3Tdbiyz.htm](feats/class-06-Fpsd8Y8qJ3Tdbiyz.htm)|Brains!|Cerveaux !|libre|
|[class-06-FPVe3o7YctBicSQa.htm](feats/class-06-FPVe3o7YctBicSQa.htm)|Advanced Revelation|Révélation avancée|libre|
|[class-06-FqrfyUtoBWJNnSi6.htm](feats/class-06-FqrfyUtoBWJNnSi6.htm)|Pain Tolerance|Tolérance à la douleur|libre|
|[class-06-fSNgVtt8y5uTCYvf.htm](feats/class-06-fSNgVtt8y5uTCYvf.htm)|Advanced Martial Magic|Magie martiale avancée|libre|
|[class-06-fx50Ivl1ERxTijpT.htm](feats/class-06-fx50Ivl1ERxTijpT.htm)|Invigorating Mercy|Soulagement revigorant|libre|
|[class-06-g8ZMeg1YFg9WZj3I.htm](feats/class-06-g8ZMeg1YFg9WZj3I.htm)|Second Shield|Second bouclier|libre|
|[class-06-GE96a0UGPYM74qjI.htm](feats/class-06-GE96a0UGPYM74qjI.htm)|Cleave|Coup tranchant|officielle|
|[class-06-goFxIDlbWd8GN0kj.htm](feats/class-06-goFxIDlbWd8GN0kj.htm)|Clear The Way|Nettoyer le passage|libre|
|[class-06-gOHBzx5Rqa6TZcrm.htm](feats/class-06-gOHBzx5Rqa6TZcrm.htm)|Mesmerizing Gaze|Regard hypnotisant|libre|
|[class-06-grPtqWYbdXXo7yhP.htm](feats/class-06-grPtqWYbdXXo7yhP.htm)|Fey Tracker|Pisteur féerique|libre|
|[class-06-gv7PJVrnODu3qYB0.htm](feats/class-06-gv7PJVrnODu3qYB0.htm)|Shielded Tome|Livre bouclier|libre|
|[class-06-GvQ9FQ02i7GYuRRh.htm](feats/class-06-GvQ9FQ02i7GYuRRh.htm)|Pact of Draconic Fury|Pacte de furie draconique|libre|
|[class-06-gwIgB6bMh0sruyX7.htm](feats/class-06-gwIgB6bMh0sruyX7.htm)|Connect The Dots|Relier les points|libre|
|[class-06-H86VNC6cNWhBI8Ed.htm](feats/class-06-H86VNC6cNWhBI8Ed.htm)|Knight Reclaimant Dedication|Dévouement : Chevalier reconquérant|libre|
|[class-06-HB0jvWCdim1p91q1.htm](feats/class-06-HB0jvWCdim1p91q1.htm)|Spiritual Sense|Sens spirituel|libre|
|[class-06-HBZSP3ABqmsV9FXH.htm](feats/class-06-HBZSP3ABqmsV9FXH.htm)|Advanced Shooter|Tireur avancé|libre|
|[class-06-hCEoINqJ75ConqqA.htm](feats/class-06-hCEoINqJ75ConqqA.htm)|Mammoth Charge|Charge du mammouth|libre|
|[class-06-HCoDbriaPwnurcNP.htm](feats/class-06-HCoDbriaPwnurcNP.htm)|Gear Up|Bien équipé|libre|
|[class-06-hE6fchGuHuPIeKlO.htm](feats/class-06-hE6fchGuHuPIeKlO.htm)|Fleeting Shadow|Ombre flottante|libre|
|[class-06-hiABcPXvcYa9QccF.htm](feats/class-06-hiABcPXvcYa9QccF.htm)|Death Warden|Gardien de la mort|libre|
|[class-06-hjApw8AvYVuqQk2W.htm](feats/class-06-hjApw8AvYVuqQk2W.htm)|Spirits' Interference|Interférence spirituelle|officielle|
|[class-06-hJlLmjW0NNeV1Ous.htm](feats/class-06-hJlLmjW0NNeV1Ous.htm)|Cascade Countermeasure|Contremesure en cascade|libre|
|[class-06-HkbP5zbN2meRiP7w.htm](feats/class-06-HkbP5zbN2meRiP7w.htm)|Psi Development|Développement psy|libre|
|[class-06-hkdP5tsTAoqJDR8v.htm](feats/class-06-hkdP5tsTAoqJDR8v.htm)|Triple Shot|Triple tir|officielle|
|[class-06-hlX7jYoS1s6srZC2.htm](feats/class-06-hlX7jYoS1s6srZC2.htm)|Megavolt|Mégavolt|libre|
|[class-06-HtH8MONAzx4eYuJY.htm](feats/class-06-HtH8MONAzx4eYuJY.htm)|Advanced Concoction|Concoction avancée|officielle|
|[class-06-i5LtFOpsUR5S74pC.htm](feats/class-06-i5LtFOpsUR5S74pC.htm)|Butterfly's Sting|Piqûre du papillon|libre|
|[class-06-IeAbL6fkRsd1hL6r.htm](feats/class-06-IeAbL6fkRsd1hL6r.htm)|Repulse the Wicked|Repousser les impies|libre|
|[class-06-IEbnal1VJySrhxFR.htm](feats/class-06-IEbnal1VJySrhxFR.htm)|Stella's Stab and Snag|Frappe et emporte de Stella|libre|
|[class-06-Ig431EeRy3FKMmMq.htm](feats/class-06-Ig431EeRy3FKMmMq.htm)|Keen Recollection|Souvenirs affûtés|libre|
|[class-06-iJrHJKNGxV4z4Qi7.htm](feats/class-06-iJrHJKNGxV4z4Qi7.htm)|Artokus's Fire|Feu d'Artokus|libre|
|[class-06-J06CgMzMDGOahXjf.htm](feats/class-06-J06CgMzMDGOahXjf.htm)|Uneasy Rest|Repos malaisé|libre|
|[class-06-jaAnxfXVmUQy0IKU.htm](feats/class-06-jaAnxfXVmUQy0IKU.htm)|One-Inch Punch|Coup à distance courte|libre|
|[class-06-JdCRxwgtdQkJ1Ha6.htm](feats/class-06-JdCRxwgtdQkJ1Ha6.htm)|Guardian's Deflection (Fighter)|Déviation du gardien (Guerrier)|officielle|
|[class-06-JgM1DMPcceeHYhQ8.htm](feats/class-06-JgM1DMPcceeHYhQ8.htm)|Harrow Ritualist|Ritualiste du Tourment|libre|
|[class-06-JHcvySfCM9uYNb9N.htm](feats/class-06-JHcvySfCM9uYNb9N.htm)|Revealing Stab|Perforation révélatrice|officielle|
|[class-06-JoZRbbexF3epeREe.htm](feats/class-06-JoZRbbexF3epeREe.htm)|Benevolent Spirit Deck|Jeu spirituel bienveillant|libre|
|[class-06-jRJqKkm6NnHcL8HO.htm](feats/class-06-jRJqKkm6NnHcL8HO.htm)|Rain-Scribe Sustenance|Subsistance du Scribe de la pluie|libre|
|[class-06-jSkJIWPfSZZzvYzq.htm](feats/class-06-jSkJIWPfSZZzvYzq.htm)|Combine Elixirs|Combiner des élixirs|officielle|
|[class-06-JxSxCTJOOayIdO4B.htm](feats/class-06-JxSxCTJOOayIdO4B.htm)|Slinger's Readiness|Rapidité du frondeur|libre|
|[class-06-k0NNa5Ko4XhDdBYB.htm](feats/class-06-k0NNa5Ko4XhDdBYB.htm)|Nocturnal Sense|Sens nocturne|libre|
|[class-06-K5ZONljq5XzS8MQc.htm](feats/class-06-K5ZONljq5XzS8MQc.htm)|Detonating Spell|Sort détonant|libre|
|[class-06-K9SpdinLWc7YRVHP.htm](feats/class-06-K9SpdinLWc7YRVHP.htm)|Captivating Intensity|Intensité captivante|libre|
|[class-06-KgD26HpSrKyciB8f.htm](feats/class-06-KgD26HpSrKyciB8f.htm)|Parallel Breakthrough|Percée parallèle|libre|
|[class-06-kMPnfNzTuXrLTUii.htm](feats/class-06-kMPnfNzTuXrLTUii.htm)|Crushing Step|Pas écrasant|libre|
|[class-06-KW6K5Zv4J7ClWkKA.htm](feats/class-06-KW6K5Zv4J7ClWkKA.htm)|Quick Snares|Pose rapide de pièges artisanaux|officielle|
|[class-06-KWZqHwI82ae8fMML.htm](feats/class-06-KWZqHwI82ae8fMML.htm)|Grave Mummification|Momification de la tombe|libre|
|[class-06-kYA6LkDw4AzKI156.htm](feats/class-06-kYA6LkDw4AzKI156.htm)|Stumbling Feint|Feinte chancelante|libre|
|[class-06-L1gD3VMD5X9JNJzE.htm](feats/class-06-L1gD3VMD5X9JNJzE.htm)|Beast Gunner Dedication|Dévouement : Bestioléro|libre|
|[class-06-L1rCuwsCKWd9zlS3.htm](feats/class-06-L1rCuwsCKWd9zlS3.htm)|Advanced Deduction|Déduction avancée|libre|
|[class-06-l60Ua5Ugv85GnF9b.htm](feats/class-06-l60Ua5Ugv85GnF9b.htm)|Nameless Anonymity|Anonymat|officielle|
|[class-06-l9K62T7qMCvJXUoY.htm](feats/class-06-l9K62T7qMCvJXUoY.htm)|Advanced Wilding|Milieu sauvage avancé|officielle|
|[class-06-l9PYldtyPr7Q8Xow.htm](feats/class-06-l9PYldtyPr7Q8Xow.htm)|Daywalker (Vampire)|Marcheur diurne (vampire)|libre|
|[class-06-L9sRaFl0tHT5AFIQ.htm](feats/class-06-L9sRaFl0tHT5AFIQ.htm)|Scatter Blast|Explosion dispersée|libre|
|[class-06-lGCFVYjL9Lp5m9Ex.htm](feats/class-06-lGCFVYjL9Lp5m9Ex.htm)|Cast Down|Incantation renversante|officielle|
|[class-06-LgpATqbuTIfB4o6G.htm](feats/class-06-LgpATqbuTIfB4o6G.htm)|No Hard Feelings|Sans rancune|libre|
|[class-06-lh3STEvbGnP7jVMr.htm](feats/class-06-lh3STEvbGnP7jVMr.htm)|Munitions Machinist|Machiniste de munitions|libre|
|[class-06-lix0Utu4g8mQ0ZtI.htm](feats/class-06-lix0Utu4g8mQ0ZtI.htm)|Divine Ally|Allié divin|officielle|
|[class-06-lkl5QaDb1mlSD7SC.htm](feats/class-06-lkl5QaDb1mlSD7SC.htm)|Visual Fidelity|Fidélité visuelle|libre|
|[class-06-lknYlp0ekVyBWQK9.htm](feats/class-06-lknYlp0ekVyBWQK9.htm)|Discerning Gaze|Regard de discernement|libre|
|[class-06-lqs4MIlO1N1YglOi.htm](feats/class-06-lqs4MIlO1N1YglOi.htm)|Advanced Synergy|Synergie avancée|libre|
|[class-06-Ls3MiZ5RcAWaiQ7f.htm](feats/class-06-Ls3MiZ5RcAWaiQ7f.htm)|Construct Shell|Créature artificielle carapaçonnée|libre|
|[class-06-lt9bQDI7ZXPA7wPw.htm](feats/class-06-lt9bQDI7ZXPA7wPw.htm)|Advanced Fury|Furie avancée|officielle|
|[class-06-lZ0swL9EEUgbAuaZ.htm](feats/class-06-lZ0swL9EEUgbAuaZ.htm)|Swashbuckler's Riposte|Riposte du bretteur|libre|
|[class-06-m0ot9Qydb9SYWHis.htm](feats/class-06-m0ot9Qydb9SYWHis.htm)|Follow-up Strike|Frappe enchaînée|libre|
|[class-06-M2V1vqAziOkWV30B.htm](feats/class-06-M2V1vqAziOkWV30B.htm)|Crimson Shroud|Voile pourpre|officielle|
|[class-06-MM9NcRyXWX2LFiuF.htm](feats/class-06-MM9NcRyXWX2LFiuF.htm)|Living Rune|Rune vivante|officielle|
|[class-06-MOAThpfl92zO5p08.htm](feats/class-06-MOAThpfl92zO5p08.htm)|Sacred Armaments|Arsenal sacré|libre|
|[class-06-moXYfz806x6uXIW9.htm](feats/class-06-moXYfz806x6uXIW9.htm)|Field Artillery|Artillerie de terrain|libre|
|[class-06-mqLPCNdCSNyY7gyI.htm](feats/class-06-mqLPCNdCSNyY7gyI.htm)|Mage Hunter|Chasseur de mage|libre|
|[class-06-mrM07U3MyElcLEx4.htm](feats/class-06-mrM07U3MyElcLEx4.htm)|Implement Initiate|Initié de l'implément|libre|
|[class-06-MROG87PQmuBTdCaB.htm](feats/class-06-MROG87PQmuBTdCaB.htm)|Jelly Body|Corps gélifié|libre|
|[class-06-mwBb8MlAmpbYH9T4.htm](feats/class-06-mwBb8MlAmpbYH9T4.htm)|It's Alive!|C'est vivant !|libre|
|[class-06-n2hawNmzW7DBn1Lm.htm](feats/class-06-n2hawNmzW7DBn1Lm.htm)|Mountain Stronghold|Bastion de la montagne|officielle|
|[class-06-n3eOMWQd4kdR3A0l.htm](feats/class-06-n3eOMWQd4kdR3A0l.htm)|Mature Trained Companion|Compagnon entraîné adulte|libre|
|[class-06-nDjTJq7PEbvRktnb.htm](feats/class-06-nDjTJq7PEbvRktnb.htm)|Advanced Weapon Training|Formation au maniement d'armes évoluées|officielle|
|[class-06-NIaNTFlPwi2ng1rZ.htm](feats/class-06-NIaNTFlPwi2ng1rZ.htm)|Superior Propulsion|Propulsion supérieure|libre|
|[class-06-nluD4gFLWePrBK5f.htm](feats/class-06-nluD4gFLWePrBK5f.htm)|Explosion|Explosion|libre|
|[class-06-Nm4yeoJVXRy0Wyth.htm](feats/class-06-Nm4yeoJVXRy0Wyth.htm)|Inertial Barrier|Barrière inertielle|libre|
|[class-06-Nm8n3urzpDqXni1i.htm](feats/class-06-Nm8n3urzpDqXni1i.htm)|Disrupting Strikes|Frappes perturbatrices|libre|
|[class-06-NMWXHGWUcZGoLDKb.htm](feats/class-06-NMWXHGWUcZGoLDKb.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[class-06-NQTpuGhB7Rq9zJkt.htm](feats/class-06-NQTpuGhB7Rq9zJkt.htm)|Polearm Tricks|Astuces à l'arme d'hast|libre|
|[class-06-nRjyyDulHnP5OewA.htm](feats/class-06-nRjyyDulHnP5OewA.htm)|Gorilla Pound|Martèlement du gorille|libre|
|[class-06-nU0r77AZXMXIlti6.htm](feats/class-06-nU0r77AZXMXIlti6.htm)|Additional Recollection|Souvenirs supplémentaires|libre|
|[class-06-nuBY8Ek4JBFQJzoh.htm](feats/class-06-nuBY8Ek4JBFQJzoh.htm)|Mastermind's Eye|Oeil du cerveau|libre|
|[class-06-O0POcPD2aELYTcIK.htm](feats/class-06-O0POcPD2aELYTcIK.htm)|Rough Terrain Stance|Posture du terrain accidenté|libre|
|[class-06-o3J79hnr00ztcwtT.htm](feats/class-06-o3J79hnr00ztcwtT.htm)|Fulminating Shot|Tir fulminant|libre|
|[class-06-of3G33qoA8oJZ0Le.htm](feats/class-06-of3G33qoA8oJZ0Le.htm)|Infiltrate Dream|Infiltration onirique|libre|
|[class-06-oGOxrqT7DHI43SVk.htm](feats/class-06-oGOxrqT7DHI43SVk.htm)|Mounted Shield|Bouclier monté|libre|
|[class-06-oIfCpkpH0Jb1mzj9.htm](feats/class-06-oIfCpkpH0Jb1mzj9.htm)|Basic Magus Spellcasting|Incantation basique du magus|libre|
|[class-06-OINfbwNZGnlyMqPR.htm](feats/class-06-OINfbwNZGnlyMqPR.htm)|Return Fire|Tir retourné|libre|
|[class-06-OjvE7gaQgWiBqOhY.htm](feats/class-06-OjvE7gaQgWiBqOhY.htm)|Discerning Strike|Frappe sagace|libre|
|[class-06-oNh2vedij8xbDbph.htm](feats/class-06-oNh2vedij8xbDbph.htm)|Hellknight Dedication|Dévouement : Chevalier infernal|libre|
|[class-06-opP9j7RP7JPyt8Zj.htm](feats/class-06-opP9j7RP7JPyt8Zj.htm)|Mature Megafauna Companion|Compagnon mégafaune adulte|libre|
|[class-06-OqObuRB8oVSAEKFR.htm](feats/class-06-OqObuRB8oVSAEKFR.htm)|Scroll Esoterica|Ésotérica parcheminée|libre|
|[class-06-oUhwrijg4rClCplO.htm](feats/class-06-oUhwrijg4rClCplO.htm)|Brutal Bully|Cogneur brutal|officielle|
|[class-06-OWedlrKGsVZVkSnT.htm](feats/class-06-OWedlrKGsVZVkSnT.htm)|Insect Shape|Morphologie d'insecte|officielle|
|[class-06-owJorCBZmUi5lIV0.htm](feats/class-06-owJorCBZmUi5lIV0.htm)|Expert Herbalism|Herboriste expert|libre|
|[class-06-OY1Ewg0dbCp52Hl5.htm](feats/class-06-OY1Ewg0dbCp52Hl5.htm)|Inner Strength|Force intérieure|libre|
|[class-06-oyLkqhDGwGGj40ME.htm](feats/class-06-oyLkqhDGwGGj40ME.htm)|Eldritch Archer Dedication|Dévouement : Archer mystique|libre|
|[class-06-Ozm0xy2lrOq6GiWU.htm](feats/class-06-Ozm0xy2lrOq6GiWU.htm)|Ghostly Grasp|Étreinte fantomatique|libre|
|[class-06-OzvvsyjAWWij4mmm.htm](feats/class-06-OzvvsyjAWWij4mmm.htm)|Keep Pace (Bounty Hunter)|Conserver le rythme (Chasseur de primes)|libre|
|[class-06-ozvYhY4hG1deXly8.htm](feats/class-06-ozvYhY4hG1deXly8.htm)|Directional Bombs|Bombes directionnelles|officielle|
|[class-06-p6j4nLPRwksjfwPW.htm](feats/class-06-p6j4nLPRwksjfwPW.htm)|Snow Step|Marche sur la neige|libre|
|[class-06-PdQAAKtCW5dS9IYj.htm](feats/class-06-PdQAAKtCW5dS9IYj.htm)|Inner Fire (Monk)|Feu intérieur (moine)|libre|
|[class-06-pewPAMlURmTqBqJx.htm](feats/class-06-pewPAMlURmTqBqJx.htm)|Axe Thrower|Lanceur de haches|libre|
|[class-06-PiBXXCeDNQGfQVoJ.htm](feats/class-06-PiBXXCeDNQGfQVoJ.htm)|Divine Emissary|Émissaire divin|libre|
|[class-06-pIG5hWjZtzZJ3VOZ.htm](feats/class-06-pIG5hWjZtzZJ3VOZ.htm)|Advanced Witchcraft|Sorcellerie avancée|libre|
|[class-06-PjsIiVM7yI0XgaFi.htm](feats/class-06-PjsIiVM7yI0XgaFi.htm)|Deathly Secrets|Secrets mortels|libre|
|[class-06-pkH4DPmMWcimMov7.htm](feats/class-06-pkH4DPmMWcimMov7.htm)|Westyr's Wayfinder Repository|Dépôt de guide de Westyr|libre|
|[class-06-pM0g4ColXTiQ3gTa.htm](feats/class-06-pM0g4ColXTiQ3gTa.htm)|Ghost Strike|Frappe spectrale|libre|
|[class-06-PMvwXlZHHFuPGrPD.htm](feats/class-06-PMvwXlZHHFuPGrPD.htm)|Nightwave Springing Reload|Rechargement bondissant du Vague nocturne|libre|
|[class-06-pph43ZrfvEnQjJXE.htm](feats/class-06-pph43ZrfvEnQjJXE.htm)|Cannon Corner Shot|Tir d'angle du canon|libre|
|[class-06-PqZZSo06BH5N7x7C.htm](feats/class-06-PqZZSo06BH5N7x7C.htm)|Diverting Vortex|Vortex déroutant|libre|
|[class-06-PxTRE0mFEO3tyt8h.htm](feats/class-06-PxTRE0mFEO3tyt8h.htm)|Advanced Mysteries|Mystères avancés|libre|
|[class-06-qDfTqetM9UEpp8ty.htm](feats/class-06-qDfTqetM9UEpp8ty.htm)|Greater Lesson|Leçon supérieure|libre|
|[class-06-qeLpqH2cMSmIrILV.htm](feats/class-06-qeLpqH2cMSmIrILV.htm)|Precise Finisher|Aboutissement précis|libre|
|[class-06-qg8TlLJRgvjzW9YK.htm](feats/class-06-qg8TlLJRgvjzW9YK.htm)|Startling Appearance (Vigilante)|Apparition surprenante (Justicier)|libre|
|[class-06-qJdbK8vgIqeHU7bu.htm](feats/class-06-qJdbK8vgIqeHU7bu.htm)|Heaven's Thunder|Tonnerre du Paradis|libre|
|[class-06-QpRzvfWdj6YH9TyE.htm](feats/class-06-QpRzvfWdj6YH9TyE.htm)|Shield Wall|Barricade de boucliers|libre|
|[class-06-QSuwyX84U26OLzZI.htm](feats/class-06-QSuwyX84U26OLzZI.htm)|Predictive Purchase (Investigator)|Achat intuitif (Enquêteur)|libre|
|[class-06-QUnN2wUfvOD6Tz69.htm](feats/class-06-QUnN2wUfvOD6Tz69.htm)|Defy Fey|Défier les fées|libre|
|[class-06-RCjMbLyRnG70R7cO.htm](feats/class-06-RCjMbLyRnG70R7cO.htm)|Frighten Undead|Effrayer un mort-vivant|libre|
|[class-06-reoylyGMDPl7H6L5.htm](feats/class-06-reoylyGMDPl7H6L5.htm)|Macabre Virtuoso|Virtuose macabre|libre|
|[class-06-rFlBoYGI5OmfMvaO.htm](feats/class-06-rFlBoYGI5OmfMvaO.htm)|Scholarly Defense|Défense érudite|libre|
|[class-06-Rh3KSd7BUfV12GBT.htm](feats/class-06-Rh3KSd7BUfV12GBT.htm)|Swift Intervention|Intervention rapide|libre|
|[class-06-RlKGaxQWWLa7xJSc.htm](feats/class-06-RlKGaxQWWLa7xJSc.htm)|Pirouette|Pirouette|libre|
|[class-06-ROAUR1GhC19Pjk9C.htm](feats/class-06-ROAUR1GhC19Pjk9C.htm)|Basic Scroll Cache|Réserve basique de parchemins|libre|
|[class-06-rS8uNb0C5GBHnKHH.htm](feats/class-06-rS8uNb0C5GBHnKHH.htm)|Viking Weapon Specialist|Spécialiste des armes vikings|libre|
|[class-06-rXY2fhyteYhaQnMl.htm](feats/class-06-rXY2fhyteYhaQnMl.htm)|Advanced Trickery|Ruse avancée|officielle|
|[class-06-S14S52HjszTgIy4l.htm](feats/class-06-S14S52HjszTgIy4l.htm)|Crane Flutter|Battement d'aile de la grue|officielle|
|[class-06-S25iRw2X9hmGRMyO.htm](feats/class-06-S25iRw2X9hmGRMyO.htm)|Advanced Blood Potency|Pouvoir du sang avancé|officielle|
|[class-06-SD3RlgQMVL6aWjtW.htm](feats/class-06-SD3RlgQMVL6aWjtW.htm)|Triggerbrand Salvo|Salve du pistolame|libre|
|[class-06-SELSj1vvVLx5cP72.htm](feats/class-06-SELSj1vvVLx5cP72.htm)|Dragon Roar|Rugissement du dragon|officielle|
|[class-06-sflJhnFzYfqZ2tDy.htm](feats/class-06-sflJhnFzYfqZ2tDy.htm)|Defend Mount|Défendre sa monture|libre|
|[class-06-SHhiLn0OSILEXNOj.htm](feats/class-06-SHhiLn0OSILEXNOj.htm)|Advanced Flair|Élégance avancée|libre|
|[class-06-SHpVHkPxtQggD9Cf.htm](feats/class-06-SHpVHkPxtQggD9Cf.htm)|Swordmaster Dedication|Dévouement : Maître épéiste|libre|
|[class-06-sk5HspGGnLW8b6bX.htm](feats/class-06-sk5HspGGnLW8b6bX.htm)|Remote Trigger|Déclenchement à distance|libre|
|[class-06-sKuhYCfCbXeRWivv.htm](feats/class-06-sKuhYCfCbXeRWivv.htm)|Nimble Shield Hand|Main du bouclier agile|libre|
|[class-06-SOG0yVNtiDsaNbIO.htm](feats/class-06-SOG0yVNtiDsaNbIO.htm)|Performative Weapons Training|Entraînement avec les armes de scène|libre|
|[class-06-soHLtpMM9h3AE7PD.htm](feats/class-06-soHLtpMM9h3AE7PD.htm)|Expert Alchemy|Alchimie experte|officielle|
|[class-06-Su4nbNnR0mjgusTT.htm](feats/class-06-Su4nbNnR0mjgusTT.htm)|Magic Hands|Mains magiques|libre|
|[class-06-SUNLm99CgsS5M3Eq.htm](feats/class-06-SUNLm99CgsS5M3Eq.htm)|Frightful Condemnation|Condamnation effrayante|libre|
|[class-06-sv4LeEbkOJyLen10.htm](feats/class-06-sv4LeEbkOJyLen10.htm)|Debilitating Bomb|Bombe incapacitante|officielle|
|[class-06-T5xFirAE8VLL5Lbu.htm](feats/class-06-T5xFirAE8VLL5Lbu.htm)|Smite Good|Châtiment du bien|libre|
|[class-06-t5zeg3m9rEnWnYXY.htm](feats/class-06-t5zeg3m9rEnWnYXY.htm)|Counter Perform|Contre-représentation|officielle|
|[class-06-TCLDccG80M5GeqGw.htm](feats/class-06-TCLDccG80M5GeqGw.htm)|Urgent Upwelling|Source d'urgence|libre|
|[class-06-TfDvkTNaC1DmsB2C.htm](feats/class-06-TfDvkTNaC1DmsB2C.htm)|Elysium's Cadence|Cadence élyséenne|libre|
|[class-06-Tg3Iyq55xW9PTSW9.htm](feats/class-06-Tg3Iyq55xW9PTSW9.htm)|Advanced Arcana|Arcanes avancés|officielle|
|[class-06-TjNkvawfWCqb1alg.htm](feats/class-06-TjNkvawfWCqb1alg.htm)|Night's Glow|Lueur de la nuit|libre|
|[class-06-tkMEdh0gM07teWkx.htm](feats/class-06-tkMEdh0gM07teWkx.htm)|Advanced Dogma|Dogme avancé|officielle|
|[class-06-Tlqqim5TmijoPRRT.htm](feats/class-06-Tlqqim5TmijoPRRT.htm)|Soul Arsenal|Arsenal spirituel|libre|
|[class-06-tpkJXDpSuGznfzGJ.htm](feats/class-06-tpkJXDpSuGznfzGJ.htm)|Mature Animal Companion (Ranger)|Compagnon animal adulte (rôdeur)|officielle|
|[class-06-tRHjUCl0xqG97nok.htm](feats/class-06-tRHjUCl0xqG97nok.htm)|Ricochet Stance (Fighter)|Posture du ricochet (Guerrier)|libre|
|[class-06-Trj5azJlaOk5jgBi.htm](feats/class-06-Trj5azJlaOk5jgBi.htm)|Divine Weapon|Arme divine|officielle|
|[class-06-TszXKspPffCzCD0X.htm](feats/class-06-TszXKspPffCzCD0X.htm)|Disciple of Shade|Disciple de l'ombre|libre|
|[class-06-TYP0Ee4o3p9LDodd.htm](feats/class-06-TYP0Ee4o3p9LDodd.htm)|Advanced Kata|Kata avancé|officielle|
|[class-06-U3A0kqJ2HKBYiu7X.htm](feats/class-06-U3A0kqJ2HKBYiu7X.htm)|Bear Hug|Étreinte de l'ours|libre|
|[class-06-u4idmXH5dd2gU9uA.htm](feats/class-06-u4idmXH5dd2gU9uA.htm)|Warding Rune|Rune de garde|libre|
|[class-06-u8YnTCS2EGoJl90W.htm](feats/class-06-u8YnTCS2EGoJl90W.htm)|Strain Mind|Stress mental|libre|
|[class-06-uBOToHKQJr5JBEsg.htm](feats/class-06-uBOToHKQJr5JBEsg.htm)|Mummy's Despair|Désespoir de la momie|libre|
|[class-06-uc6JcNrI31wzSC2h.htm](feats/class-06-uc6JcNrI31wzSC2h.htm)|Bullet Dancer Burn|Brûlure du danseur de balle|libre|
|[class-06-UEbBOljjXvKsGKFu.htm](feats/class-06-UEbBOljjXvKsGKFu.htm)|Disk Rider|Chevaucheur de disque|libre|
|[class-06-uH3ZRkXPsXi1ChO2.htm](feats/class-06-uH3ZRkXPsXi1ChO2.htm)|Corrupted Shield|Bouclier corrompu|libre|
|[class-06-uhfZtjbfJ8pZIWrF.htm](feats/class-06-uhfZtjbfJ8pZIWrF.htm)|Scrollmaster Dedication|Dévouement : Maître des parchemins|libre|
|[class-06-uiGsVmvRfujQQRlK.htm](feats/class-06-uiGsVmvRfujQQRlK.htm)|Steady Spellcasting|Incantation fiable|officielle|
|[class-06-UjEeHamC2C8JfgJz.htm](feats/class-06-UjEeHamC2C8JfgJz.htm)|Sky and Heaven Stance|Posture du Ciel et du Paradis|libre|
|[class-06-uJpghjJtNbqKUxRd.htm](feats/class-06-uJpghjJtNbqKUxRd.htm)|Wolf Drag|Traction du loup|officielle|
|[class-06-UtUT6JngJbQRHySX.htm](feats/class-06-UtUT6JngJbQRHySX.htm)|Boaster's Challenge|Défi du vantard|libre|
|[class-06-V3lFDAQr3PfnAxMC.htm](feats/class-06-V3lFDAQr3PfnAxMC.htm)|Confounding Image|Image confondante|libre|
|[class-06-V7bwuYADV8huWeF7.htm](feats/class-06-V7bwuYADV8huWeF7.htm)|Unnerving Prowess|Prouesse désarçonnante|officielle|
|[class-06-VAxtUenSWEBWYBRt.htm](feats/class-06-VAxtUenSWEBWYBRt.htm)|Enervating Wail|Plainte innervante (Déviant)|libre|
|[class-06-vKFg7HMNu4cCDD8b.htm](feats/class-06-vKFg7HMNu4cCDD8b.htm)|Volatile Grease|Graisse volatile|libre|
|[class-06-vqLt1qjdrflTmdsw.htm](feats/class-06-vqLt1qjdrflTmdsw.htm)|Sun's Fury|Furie du soleil|libre|
|[class-06-VqVgcqmG6xmYuDbK.htm](feats/class-06-VqVgcqmG6xmYuDbK.htm)|Scouring Rage|Rage débordante|libre|
|[class-06-VruIzuysxw4tY6rk.htm](feats/class-06-VruIzuysxw4tY6rk.htm)|Expert Poisoner|Empoisonneur expert|libre|
|[class-06-vsSbYfsRTHveef24.htm](feats/class-06-vsSbYfsRTHveef24.htm)|Duel Spell Advantage|Avantage de sort de duel|libre|
|[class-06-vuApM8xHOZs4o6oS.htm](feats/class-06-vuApM8xHOZs4o6oS.htm)|Spellmaster Dedication|Dévouement : Maître des sorts|libre|
|[class-06-vW8dGtOD3rZVOJoq.htm](feats/class-06-vW8dGtOD3rZVOJoq.htm)|High-quality Scrounger|Bricolage de haute qualité|libre|
|[class-06-vWrGwqy4AhHMPz8V.htm](feats/class-06-vWrGwqy4AhHMPz8V.htm)|Dirge of Doom|Chant funeste|officielle|
|[class-06-VYilg64xX9XpHeJr.htm](feats/class-06-VYilg64xX9XpHeJr.htm)|Far Throw|Lancer lointain|libre|
|[class-06-w0nSRBNwexM5Dh0D.htm](feats/class-06-w0nSRBNwexM5Dh0D.htm)|Whirling Throw|Projection en rotation|officielle|
|[class-06-W21jKAcG0GtEtBiK.htm](feats/class-06-W21jKAcG0GtEtBiK.htm)|Giant's Stature|Stature de géant|officielle|
|[class-06-WAx7RABHDvVVcRI8.htm](feats/class-06-WAx7RABHDvVVcRI8.htm)|Split Slot|Emplacement divisé|libre|
|[class-06-wHwjoK3E1Ot9kkV0.htm](feats/class-06-wHwjoK3E1Ot9kkV0.htm)|Obscured Terrain|Terrain dissimulé|libre|
|[class-06-wios5UDRwKXoUYUD.htm](feats/class-06-wios5UDRwKXoUYUD.htm)|Master Summoner|Conjurateur maître|libre|
|[class-06-WjEwsu4kkexNvDcN.htm](feats/class-06-WjEwsu4kkexNvDcN.htm)|Thorough Research|Recherche minutieuse|libre|
|[class-06-Wn3DWAXo2TRxrhI6.htm](feats/class-06-Wn3DWAXo2TRxrhI6.htm)|Sniper's Aim|Visée du tireur d'élite|libre|
|[class-06-x7EGJYZQuxHbP50X.htm](feats/class-06-x7EGJYZQuxHbP50X.htm)|Guided Skill|Compétence guidée|libre|
|[class-06-xaSlCFYUXlu5f0zw.htm](feats/class-06-xaSlCFYUXlu5f0zw.htm)|Mind Shards|Échardes spirituelles|libre|
|[class-06-xDTjr415ZZM8x2WW.htm](feats/class-06-xDTjr415ZZM8x2WW.htm)|High-Speed Regeneration|Régénération à haute vitesse (Déviant)|libre|
|[class-06-xgvKXeTxns0gIdAn.htm](feats/class-06-xgvKXeTxns0gIdAn.htm)|Smite Evil|Châtiment du mal|officielle|
|[class-06-Xhphe5Lsa4kuU4RG.htm](feats/class-06-Xhphe5Lsa4kuU4RG.htm)|Crossbow Terror|Terreur à l'arbalète|libre|
|[class-06-XkemuXgSQtxFAhZ8.htm](feats/class-06-XkemuXgSQtxFAhZ8.htm)|Advanced Shooting|Tir avancé|libre|
|[class-06-XLIJXGJ1JdJJZQHG.htm](feats/class-06-XLIJXGJ1JdJJZQHG.htm)|Vision of Foresight|Vision prémonitoire|libre|
|[class-06-xqyN4Nk5mLgUm09l.htm](feats/class-06-xqyN4Nk5mLgUm09l.htm)|Targeted Redirection|Redirection ciblée|libre|
|[class-06-XRahcvEPEAEdGUn8.htm](feats/class-06-XRahcvEPEAEdGUn8.htm)|Gang Up|Attaque groupée|officielle|
|[class-06-XusuxqmXWPYc0JYA.htm](feats/class-06-XusuxqmXWPYc0JYA.htm)|Narrative Conduit|Conduit narratif|libre|
|[class-06-XYaaj872JOO9CAws.htm](feats/class-06-XYaaj872JOO9CAws.htm)|Blazing Talon Surge|Déferlement de serres enflammées|libre|
|[class-06-y61mDkTqk2k77b4x.htm](feats/class-06-y61mDkTqk2k77b4x.htm)|Furious Focus|Focalisation furieuse|officielle|
|[class-06-Y8Rdt4gHBGAUf2zL.htm](feats/class-06-Y8Rdt4gHBGAUf2zL.htm)|Advantageous Assault|Assaut avantageux|officielle|
|[class-06-yfzpmSF9IYsMeKxo.htm](feats/class-06-yfzpmSF9IYsMeKxo.htm)|One More Activation|Une activation de plus|libre|
|[class-06-YhpR5bOquHx2JuIj.htm](feats/class-06-YhpR5bOquHx2JuIj.htm)|Litany Against Wrath|Litanie contre la colère|officielle|
|[class-06-YOU5eCD5S4cS6Qeu.htm](feats/class-06-YOU5eCD5S4cS6Qeu.htm)|Cascade Bearer's Flexibility|Flexibilité du Tisseur des flots|libre|
|[class-06-yOyMlFGAgLfRas8m.htm](feats/class-06-yOyMlFGAgLfRas8m.htm)|Pistolero's Challenge|Défi du pistoléro|libre|
|[class-06-yUpZcrQHrz4mflKQ.htm](feats/class-06-yUpZcrQHrz4mflKQ.htm)|Energetic Resonance|Résonance énergétique|libre|
|[class-06-YY7wmYQ9ccAO8fut.htm](feats/class-06-YY7wmYQ9ccAO8fut.htm)|Basic Summoner Spellcasting|Incantation basique du conjurateur|libre|
|[class-06-yyt2I2lGbRndXjbc.htm](feats/class-06-yyt2I2lGbRndXjbc.htm)|Selective Energy|Énergie sélective|officielle|
|[class-06-z5fUX9jeqfAViOd8.htm](feats/class-06-z5fUX9jeqfAViOd8.htm)|Shove Down|Envoyer au tapis|libre|
|[class-06-z8bozNJvUjBoKLPA.htm](feats/class-06-z8bozNJvUjBoKLPA.htm)|Arcane Sensitivity|Sensitivité arcanique|libre|
|[class-06-z8NUh2FayDa7Ctnp.htm](feats/class-06-z8NUh2FayDa7Ctnp.htm)|Vengeful Spirit Deck|Jeu de l'esprit vengeur|libre|
|[class-06-zbxqYhmn7KbqR2Sb.htm](feats/class-06-zbxqYhmn7KbqR2Sb.htm)|Shatter Defenses|Briser les défenses|officielle|
|[class-06-ZGgFrQAQWk0keaFW.htm](feats/class-06-ZGgFrQAQWk0keaFW.htm)|Disarming Smile|Sourire désarmant|libre|
|[class-06-ZhhITE2ZMX7UZUge.htm](feats/class-06-ZhhITE2ZMX7UZUge.htm)|Predatory Claws|Griffes prédatrices|libre|
|[class-06-zkN2FndJxGt711fj.htm](feats/class-06-zkN2FndJxGt711fj.htm)|Gravel Guts|Tripes en graviers|libre|
|[class-06-zMT3etCcdPdtAdOn.htm](feats/class-06-zMT3etCcdPdtAdOn.htm)|Butterfly's Kiss|Baiser du papillon|libre|
|[class-06-ZOr1FUlpJj1q6q3H.htm](feats/class-06-ZOr1FUlpJj1q6q3H.htm)|Knowledge is Power|Le pouvoire de la connaissance|libre|
|[class-06-ZPclfDmiHzEqblry.htm](feats/class-06-ZPclfDmiHzEqblry.htm)|Animal Skin|Cuir animal|officielle|
|[class-06-zwEaXGKqnlBTllfE.htm](feats/class-06-zwEaXGKqnlBTllfE.htm)|Spell Relay|Relais de sort|libre|
|[class-06-zZCyJhsaugHB6mZW.htm](feats/class-06-zZCyJhsaugHB6mZW.htm)|Watch Your Back|Fais attention|libre|
|[class-06-zZZmRwvtwQazhU0b.htm](feats/class-06-zZZmRwvtwQazhU0b.htm)|Upset Balance|Équilibre rompu|libre|
|[class-07-i6eWwJ67qBIPJZoK.htm](feats/class-07-i6eWwJ67qBIPJZoK.htm)|Explosive Entry|Entrée explosive|libre|
|[class-07-v1PtAazmEFhTp6fZ.htm](feats/class-07-v1PtAazmEFhTp6fZ.htm)|Quick Change|Changement rapide|libre|
|[class-08-0Bu48kn3Deq9gHQE.htm](feats/class-08-0Bu48kn3Deq9gHQE.htm)|Practiced Guidance|Guide expérimenté|libre|
|[class-08-0PcVi7eav6PMLOPl.htm](feats/class-08-0PcVi7eav6PMLOPl.htm)|Channeled Succor|Secours canalisé|officielle|
|[class-08-0XGLdVbEIISOOuuO.htm](feats/class-08-0XGLdVbEIISOOuuO.htm)|Bullseye|Dans le mille|libre|
|[class-08-16MOW7deoOoDwE9z.htm](feats/class-08-16MOW7deoOoDwE9z.htm)|Hazard Finder|Dénicheur de dangers|officielle|
|[class-08-1AgirzUGkyDdmENy.htm](feats/class-08-1AgirzUGkyDdmENy.htm)|Shadowdancer Dedication|Dévouement : Danseur de l'ombre|libre|
|[class-08-1NS6AM2oVNKb4LhX.htm](feats/class-08-1NS6AM2oVNKb4LhX.htm)|Flashing Shield|Bouclier éclatant|libre|
|[class-08-1qJCMbs3zcPMWDux.htm](feats/class-08-1qJCMbs3zcPMWDux.htm)|Know-It-All (Bard)|Je-sais-tout (Barde)|officielle|
|[class-08-1VLOhyq0IFMY2rqh.htm](feats/class-08-1VLOhyq0IFMY2rqh.htm)|Animal Rage|Rage animale|officielle|
|[class-08-264KzmKMK4zqi6AR.htm](feats/class-08-264KzmKMK4zqi6AR.htm)|Clue Them All In|Mise en commun des indices|libre|
|[class-08-27R8yZcY2uXH6pZN.htm](feats/class-08-27R8yZcY2uXH6pZN.htm)|Powerful Snares|Redoutables pièges artisanaux|officielle|
|[class-08-2leK1eGVcIBTmx6J.htm](feats/class-08-2leK1eGVcIBTmx6J.htm)|Unseen Passage|Passage inaperçu|libre|
|[class-08-2MHzEh1KUQEbhjUf.htm](feats/class-08-2MHzEh1KUQEbhjUf.htm)|Vivacious Bravado|Bravade vivifiante|libre|
|[class-08-2mww0DmokYJXUEoA.htm](feats/class-08-2mww0DmokYJXUEoA.htm)|Ooze Empathy|Empathie avec les vases|libre|
|[class-08-2tUdsoPEnW9yS8so.htm](feats/class-08-2tUdsoPEnW9yS8so.htm)|Tangled Forest Stance|Posture de la forêt enchevêtrée|officielle|
|[class-08-31ozQ8lwNtiQi2N0.htm](feats/class-08-31ozQ8lwNtiQi2N0.htm)|Guardian Ghosts|Fantômes gardiens|libre|
|[class-08-3hhCDWPm021hvicR.htm](feats/class-08-3hhCDWPm021hvicR.htm)|Renewed Vigor|Regain de vigueur|officielle|
|[class-08-3xD4LtEgpGZU0MNx.htm](feats/class-08-3xD4LtEgpGZU0MNx.htm)|Syu Tak-nwa's Hexed Locks|Boucles de maléfices de Syu Tak-nwa|libre|
|[class-08-3XFKXB3ffeIkrQYe.htm](feats/class-08-3XFKXB3ffeIkrQYe.htm)|Sense Chaos|Perception du chaos|libre|
|[class-08-3xiWBDSR8miAotpa.htm](feats/class-08-3xiWBDSR8miAotpa.htm)|Viking Vindicator|Vindicateur viking|libre|
|[class-08-3xkFb2qlAdgLmdSf.htm](feats/class-08-3xkFb2qlAdgLmdSf.htm)|Counter Curse|Contrer la malédiction|libre|
|[class-08-49TMxb8OA1Pp7AiF.htm](feats/class-08-49TMxb8OA1Pp7AiF.htm)|Scholar's Hunch|Pressentiment de l'érudit|libre|
|[class-08-4Bum44iMs6tQz90v.htm](feats/class-08-4Bum44iMs6tQz90v.htm)|Waking Dream|Rêve éveillé|libre|
|[class-08-4Ek3Kyle2DsCPQQm.htm](feats/class-08-4Ek3Kyle2DsCPQQm.htm)|Great Boaster|Grande vantardise|libre|
|[class-08-4HqPkJeSDpqYeGNn.htm](feats/class-08-4HqPkJeSDpqYeGNn.htm)|Enhanced Psychopomp Familiar|Familier psychopompe amélioré|libre|
|[class-08-4jaCuX2JSjTSJ3wp.htm](feats/class-08-4jaCuX2JSjTSJ3wp.htm)|Phalanx Formation|Formation en phalange|libre|
|[class-08-4nFsGmPdWvFrwjgF.htm](feats/class-08-4nFsGmPdWvFrwjgF.htm)|Advanced Hallowed Spell|Sort sacré avancé|libre|
|[class-08-4osf0dNeZ19KBoTb.htm](feats/class-08-4osf0dNeZ19KBoTb.htm)|Manifold Modifications|Modifications multiples|libre|
|[class-08-4xeDjt8eARzAGARP.htm](feats/class-08-4xeDjt8eARzAGARP.htm)|Masked Casting|Incantation masquée|libre|
|[class-08-4YeHOPzo3zOhZQCh.htm](feats/class-08-4YeHOPzo3zOhZQCh.htm)|Mantis Form|Forme de la mante|officielle|
|[class-08-505CCsBRft1P53gP.htm](feats/class-08-505CCsBRft1P53gP.htm)|Lingering Flames|Flammes prolongées|libre|
|[class-08-58LYCoLrCzG2Ll8b.htm](feats/class-08-58LYCoLrCzG2Ll8b.htm)|Incredible Construct Companion|Compagnon créature artificielle formidable|libre|
|[class-08-5d4AyZ0Y6Ht1OwIa.htm](feats/class-08-5d4AyZ0Y6Ht1OwIa.htm)|Sidestep|Pas de côté|officielle|
|[class-08-5hUj7glY8YnO5sBI.htm](feats/class-08-5hUj7glY8YnO5sBI.htm)|Advanced School Spell|Sort d'école perfectionné|officielle|
|[class-08-5MG4dBTsFZVbHcX7.htm](feats/class-08-5MG4dBTsFZVbHcX7.htm)|Basic Eldritch Archer Spellcasting|Incantation basique de l'archer mystique|libre|
|[class-08-5O6G488xu1p8ZHsS.htm](feats/class-08-5O6G488xu1p8ZHsS.htm)|Slayer's Blessing|Bénédiction du tueur|libre|
|[class-08-5Pj6pQ7N1qXCQLal.htm](feats/class-08-5Pj6pQ7N1qXCQLal.htm)|Accursed Touch|Contact maudissant|libre|
|[class-08-60o2Pf4IunqP6J0Z.htm](feats/class-08-60o2Pf4IunqP6J0Z.htm)|Equitable Defense|Défense équitable|libre|
|[class-08-6biVVoaqqUdQmQ37.htm](feats/class-08-6biVVoaqqUdQmQ37.htm)|Fiery Retort|Retour de flammes|libre|
|[class-08-6KiB0SLYB1p8Th5U.htm](feats/class-08-6KiB0SLYB1p8Th5U.htm)|Safeguarded Spell|Protégé du sort|libre|
|[class-08-6qHoUiEudUgUB1Uq.htm](feats/class-08-6qHoUiEudUgUB1Uq.htm)|Divine Healing|Guérison divine|libre|
|[class-08-6VWLyjurMGi33XAT.htm](feats/class-08-6VWLyjurMGi33XAT.htm)|Reading the Signs|Lire les signes|libre|
|[class-08-7IsHNime3WneCan6.htm](feats/class-08-7IsHNime3WneCan6.htm)|Wall Run|Course à la verticale|officielle|
|[class-08-7p2tNqYHsg6u05cU.htm](feats/class-08-7p2tNqYHsg6u05cU.htm)|Shoving Sweep|Balayage repoussant|libre|
|[class-08-7PlGRHizgieuYzDR.htm](feats/class-08-7PlGRHizgieuYzDR.htm)|Mental Static|Brouillage mental|libre|
|[class-08-7vOVPzsVuyE5a3Rp.htm](feats/class-08-7vOVPzsVuyE5a3Rp.htm)|Fortified Flesh|Renforcement de la chair|officielle|
|[class-08-80DU0IvIzOIBGuUa.htm](feats/class-08-80DU0IvIzOIBGuUa.htm)|Loaner Spell|Sort d'emprunt|libre|
|[class-08-8CLbJAtgSfwxk2rk.htm](feats/class-08-8CLbJAtgSfwxk2rk.htm)|Murksight|Vision dans la brume|libre|
|[class-08-8cq6NO087Te3P9yw.htm](feats/class-08-8cq6NO087Te3P9yw.htm)|Bonds of Death|Liens de mort|libre|
|[class-08-8rE5zLEVe4putosB.htm](feats/class-08-8rE5zLEVe4putosB.htm)|Fey Caller|Celui qui appelle les fées|officielle|
|[class-08-8Sdw31YToKhBJ4v4.htm](feats/class-08-8Sdw31YToKhBJ4v4.htm)|Physical Training|Entraînement physique|libre|
|[class-08-8YLCu791osZNFKN2.htm](feats/class-08-8YLCu791osZNFKN2.htm)|Leap and Fire|Bondir et faire feu|libre|
|[class-08-8z9XkfZalQ5tUjfy.htm](feats/class-08-8z9XkfZalQ5tUjfy.htm)|Paralyzing Slash|Coupure paralysante|libre|
|[class-08-94PGauGdzrVARMLc.htm](feats/class-08-94PGauGdzrVARMLc.htm)|Grit and Tenacity|Audace et ténacité|libre|
|[class-08-9EmJElnNVmXQ7Rzn.htm](feats/class-08-9EmJElnNVmXQ7Rzn.htm)|Greenwatch Veteran|Vétéran de la garde verte|libre|
|[class-08-9EqUTnbV8WHE2aKm.htm](feats/class-08-9EqUTnbV8WHE2aKm.htm)|Dualistic Synergy|Synergie duale|libre|
|[class-08-9Eufa07qvXG41QmG.htm](feats/class-08-9Eufa07qvXG41QmG.htm)|Improved Poison Weapon|Arme empoisonnée améliorée|officielle|
|[class-08-9LvJo3K2AjKcVvTc.htm](feats/class-08-9LvJo3K2AjKcVvTc.htm)|Exude Abyssal Corruption|Exsuder la corruption abyssale|libre|
|[class-08-9LwOCcutlLxd4bfS.htm](feats/class-08-9LwOCcutlLxd4bfS.htm)|Reversing Charge|Charge réversible|libre|
|[class-08-9MiK0Lyro5dQgHij.htm](feats/class-08-9MiK0Lyro5dQgHij.htm)|Rippling Spin|Tourbillon réfléchissant|libre|
|[class-08-9pTQrhbeF348bYky.htm](feats/class-08-9pTQrhbeF348bYky.htm)|Inspired Stratagem|Stratagème inspiré|libre|
|[class-08-9Q0tPGtOawPTU2TU.htm](feats/class-08-9Q0tPGtOawPTU2TU.htm)|Follow-up Assault|Assaut complémentaire|libre|
|[class-08-9sl2t3jb5ZdQA3K4.htm](feats/class-08-9sl2t3jb5ZdQA3K4.htm)|Can't You See?|Ne peux tu pas le voir ?|libre|
|[class-08-9tXGk4h9Bw3Wcnra.htm](feats/class-08-9tXGk4h9Bw3Wcnra.htm)|Helt's Spelldance|Danse de sort de Helt|libre|
|[class-08-9WMLIcHpwkQpUQfz.htm](feats/class-08-9WMLIcHpwkQpUQfz.htm)|Magic Finder|Découvreur de magie|libre|
|[class-08-A0keRhzNlcB1u4gD.htm](feats/class-08-A0keRhzNlcB1u4gD.htm)|Stasian Smash|Coup de poing stasien|libre|
|[class-08-A981119DMdqE9Pg1.htm](feats/class-08-A981119DMdqE9Pg1.htm)|Running Tackle|Plaquage en courant|libre|
|[class-08-AbgHCPWOKULeXrJ2.htm](feats/class-08-AbgHCPWOKULeXrJ2.htm)|Sly Striker|Frappe perfide|officielle|
|[class-08-Ad0XBuETAkMD6doj.htm](feats/class-08-Ad0XBuETAkMD6doj.htm)|Felling Strike|Frappe renversante|officielle|
|[class-08-ADgFB8hjUgcXS4bF.htm](feats/class-08-ADgFB8hjUgcXS4bF.htm)|Mutable Familiar|Familier altérable|libre|
|[class-08-aEws1NR19Lbu1Kio.htm](feats/class-08-aEws1NR19Lbu1Kio.htm)|Incredible Beastmaster Companion|Formidable compagnon (Maître des bêtes)|libre|
|[class-08-AgMFfp6fdNZ1mAxn.htm](feats/class-08-AgMFfp6fdNZ1mAxn.htm)|Order Training|Entraînement de l'Ordre|libre|
|[class-08-agWNAYqgyV58jlxm.htm](feats/class-08-agWNAYqgyV58jlxm.htm)|Foolproof Instructions|Instructions infaillibles|libre|
|[class-08-AiV2xFhYB90KHt2x.htm](feats/class-08-AiV2xFhYB90KHt2x.htm)|Pinning Fire|Tir fichant|libre|
|[class-08-aKFYL4b5Bi7qla2j.htm](feats/class-08-aKFYL4b5Bi7qla2j.htm)|Peer Beyond|Vision de l'au-delà|libre|
|[class-08-AqVyKiIjISDBFRA0.htm](feats/class-08-AqVyKiIjISDBFRA0.htm)|Unkillable|Intuable|libre|
|[class-08-Ar6W97iun6yYI8Df.htm](feats/class-08-Ar6W97iun6yYI8Df.htm)|Delay Trap|Retardement de piège|officielle|
|[class-08-ASAXLrckAyTBYi8E.htm](feats/class-08-ASAXLrckAyTBYi8E.htm)|Forgotten Presence|Présence oubliée|libre|
|[class-08-ASWqQ6RB7cfCsUo0.htm](feats/class-08-ASWqQ6RB7cfCsUo0.htm)|Protect Ally|Protéger un allié|libre|
|[class-08-Aww98EQXcigRhY3v.htm](feats/class-08-Aww98EQXcigRhY3v.htm)|Sickening Bite|Morsure répugnante|libre|
|[class-08-AYBfwGImT28lUdue.htm](feats/class-08-AYBfwGImT28lUdue.htm)|Incredible Mount|Formidable monture|libre|
|[class-08-b1eGMNjBY3iqIt2S.htm](feats/class-08-b1eGMNjBY3iqIt2S.htm)|Magical Adept|Adepte magique|libre|
|[class-08-BaKEnNbzbGlenmRv.htm](feats/class-08-BaKEnNbzbGlenmRv.htm)|Eerie Traces|Traces mystérieuses|libre|
|[class-08-BASCKOPvNGgoHGid.htm](feats/class-08-BASCKOPvNGgoHGid.htm)|Achaekek's Grip|Étreinte d'Achaékek|libre|
|[class-08-BBvmmULFPLlHCeIK.htm](feats/class-08-BBvmmULFPLlHCeIK.htm)|Heightened Captivation|Captivation intensifiée|libre|
|[class-08-bcExqs4CsG2Kc5Bs.htm](feats/class-08-bcExqs4CsG2Kc5Bs.htm)|Miniaturize|Miniaturiser|libre|
|[class-08-BFgrPAK2v3GSKQ5e.htm](feats/class-08-BFgrPAK2v3GSKQ5e.htm)|Lore Seeker|Chercheur de connaissances|libre|
|[class-08-BkKWTt3ufaCN2ZdI.htm](feats/class-08-BkKWTt3ufaCN2ZdI.htm)|Sudden Leap|Bond soudain|officielle|
|[class-08-bkX8v744C62W8hol.htm](feats/class-08-bkX8v744C62W8hol.htm)|Attuned Stride|Démarche harmonisée|libre|
|[class-08-bPqRneuJPqeXc65G.htm](feats/class-08-bPqRneuJPqeXc65G.htm)|Sense Good|Perception du bien|libre|
|[class-08-BVHDkBa4JMmmj5Sn.htm](feats/class-08-BVHDkBa4JMmmj5Sn.htm)|Opportunistic Grapple|Prise opportuniste|libre|
|[class-08-bXoGskH0SYfdcEtJ.htm](feats/class-08-bXoGskH0SYfdcEtJ.htm)|Garden Path|Chemin du jardin|libre|
|[class-08-bYijGvCvCmJnW6aA.htm](feats/class-08-bYijGvCvCmJnW6aA.htm)|Sacrifice Armor|Armure sacrifiée|libre|
|[class-08-c1QTcMtI9957gQoB.htm](feats/class-08-c1QTcMtI9957gQoB.htm)|Remove Presence|Supprimer la présence|libre|
|[class-08-C3zKTQecexSbezhT.htm](feats/class-08-C3zKTQecexSbezhT.htm)|Grievous Blow|Coup douloureux|libre|
|[class-08-C4ugUzUuQ4UznNhl.htm](feats/class-08-C4ugUzUuQ4UznNhl.htm)|Basic Modification|Modification basique|libre|
|[class-08-c9rhGmKft1BVT4JO.htm](feats/class-08-c9rhGmKft1BVT4JO.htm)|Skill Mastery (Rogue)|Maîtrise des compétences (Roublard)|libre|
|[class-08-cA1IIy6UEsgETXiX.htm](feats/class-08-cA1IIy6UEsgETXiX.htm)|Furious Bully|Cogneur furieux|libre|
|[class-08-cdeVm8fTDzrP71Sw.htm](feats/class-08-cdeVm8fTDzrP71Sw.htm)|Lorefinder|Découvreur de connaissances|libre|
|[class-08-cDNxHLIgvyD7yBfM.htm](feats/class-08-cDNxHLIgvyD7yBfM.htm)|Scattered Fire|Feu éparpillé|libre|
|[class-08-CIRseixX8dr36ZQK.htm](feats/class-08-CIRseixX8dr36ZQK.htm)|Inspirational Performance|Représentation inspirante|officielle|
|[class-08-CL43gGiErw5FUtFQ.htm](feats/class-08-CL43gGiErw5FUtFQ.htm)|Living for the Applause|Vivre pour les applaudissements|libre|
|[class-08-CL9pFxxMXqzIyg4j.htm](feats/class-08-CL9pFxxMXqzIyg4j.htm)|Wild Winds Initiate|Initié des vents violents|officielle|
|[class-08-CN7tu5H6wTe9ENmO.htm](feats/class-08-CN7tu5H6wTe9ENmO.htm)|Crossblooded Evolution|Évolution métisée|officielle|
|[class-08-cU5NdcwnMkFQNPjh.htm](feats/class-08-cU5NdcwnMkFQNPjh.htm)|To Battle!|Aux armes !|libre|
|[class-08-d4B8uKyffvnkzUOw.htm](feats/class-08-d4B8uKyffvnkzUOw.htm)|Unshaken in Iron|Inébranlable dans le fer|libre|
|[class-08-D7g9z4MuIeS4iRwN.htm](feats/class-08-D7g9z4MuIeS4iRwN.htm)|Public Execution|Exécution publique|libre|
|[class-08-d81PT3w97SkyAiXQ.htm](feats/class-08-d81PT3w97SkyAiXQ.htm)|Lion's Fury|Furie du lion|libre|
|[class-08-dCoX0TfsasMfwYnz.htm](feats/class-08-dCoX0TfsasMfwYnz.htm)|Larcenous Hand|Manipulation chapardeuse|libre|
|[class-08-DGO6kyjw2bQG7dbY.htm](feats/class-08-DGO6kyjw2bQG7dbY.htm)|Incredible Aim|Visée extraordinaire|officielle|
|[class-08-dHJw5Yv0gY3suBZo.htm](feats/class-08-dHJw5Yv0gY3suBZo.htm)|Reminder of the Greater Fear|Souvenir d'une plus grande peur|libre|
|[class-08-DMjfZvWD20SGuIO4.htm](feats/class-08-DMjfZvWD20SGuIO4.htm)|Show-Off|Frimer|libre|
|[class-08-dmXd68ilbuGR6eUP.htm](feats/class-08-dmXd68ilbuGR6eUP.htm)|Archaeologist's Luck|Chance de l'archéologue|libre|
|[class-08-dTO1ShJovbzrKUY4.htm](feats/class-08-dTO1ShJovbzrKUY4.htm)|Resounding Bravery|Bravoure retentissante|libre|
|[class-08-dTPVRVzfVBlBUV2l.htm](feats/class-08-dTPVRVzfVBlBUV2l.htm)|Arrow Snatching|Capture de projectiles|officielle|
|[class-08-dtPXuS8MiWrz5UNK.htm](feats/class-08-dtPXuS8MiWrz5UNK.htm)|Hot Foot|Pied chaud|libre|
|[class-08-DwU6CmK4KsH8A3hu.htm](feats/class-08-DwU6CmK4KsH8A3hu.htm)|Arcane Breadth|Amplitude arcanique|officielle|
|[class-08-dxujgA0NgiEvA0H8.htm](feats/class-08-dxujgA0NgiEvA0H8.htm)|Bleeding Finisher|Aboutissement hémorragique|libre|
|[class-08-DYc108IqRBP9N9W6.htm](feats/class-08-DYc108IqRBP9N9W6.htm)|Soulsight (Bard)|Vision de l'âme (Barde)|libre|
|[class-08-E3NsAdbp7kkOvfnr.htm](feats/class-08-E3NsAdbp7kkOvfnr.htm)|Quick Positioning|Positionnement rapide|libre|
|[class-08-E7eceezD3NDmBVBb.htm](feats/class-08-E7eceezD3NDmBVBb.htm)|Back To Back|Dos à dos|libre|
|[class-08-EG27noJj9KzyB2i4.htm](feats/class-08-EG27noJj9KzyB2i4.htm)|Out of Hand|Hors de portée|libre|
|[class-08-eGgSGU1LOaRNRYR7.htm](feats/class-08-eGgSGU1LOaRNRYR7.htm)|Shadow Spell|Sort d'ombre|libre|
|[class-08-egmb8p3ZIYtx5aQN.htm](feats/class-08-egmb8p3ZIYtx5aQN.htm)|Archer's Aim|Visée de l'Archer|libre|
|[class-08-EGtuOZ3E9y0qZ1oJ.htm](feats/class-08-EGtuOZ3E9y0qZ1oJ.htm)|Familiar Form|Forme du familier|libre|
|[class-08-enPAJ1oSDutts7ic.htm](feats/class-08-enPAJ1oSDutts7ic.htm)|Deadly Aim|Visée mortelle|officielle|
|[class-08-EnpbhZr94ZyZI4hb.htm](feats/class-08-EnpbhZr94ZyZI4hb.htm)|Sense Evil|Perception du mal|libre|
|[class-08-enxzAkPICXT4sSFU.htm](feats/class-08-enxzAkPICXT4sSFU.htm)|Fused Staff|Bâton imprégné|libre|
|[class-08-EoKgJXfNfHwsy2sk.htm](feats/class-08-EoKgJXfNfHwsy2sk.htm)|Superimpose Time Duplicates|Superposer les doubles temporels|libre|
|[class-08-F6VlPyZZpqV6d2CS.htm](feats/class-08-F6VlPyZZpqV6d2CS.htm)|Flensing Slice|Taille dépeçante|libre|
|[class-08-F6ZAceuDpiM9bUiF.htm](feats/class-08-F6ZAceuDpiM9bUiF.htm)|Nimble Roll|Roulade agile|libre|
|[class-08-FEMXDBCf8gXBYdez.htm](feats/class-08-FEMXDBCf8gXBYdez.htm)|Dark Persona's Presence|Présence de la personnalité sombre|libre|
|[class-08-ffdXSxl4lVFrOvyQ.htm](feats/class-08-ffdXSxl4lVFrOvyQ.htm)|Ambushing Knockdown|Embuscade assommante|libre|
|[class-08-fFfRsvDavUsTBDF2.htm](feats/class-08-fFfRsvDavUsTBDF2.htm)|Martyr|Martyr|libre|
|[class-08-fhwO3N3VoZ0ZpfjU.htm](feats/class-08-fhwO3N3VoZ0ZpfjU.htm)|Accursed Magic|Magie maudite|libre|
|[class-08-fHWYICk6cSePr30c.htm](feats/class-08-fHWYICk6cSePr30c.htm)|Surreptitious Spellcaster|Lanceur de sort subreptice|libre|
|[class-08-FjuuX0vUWlYchRNM.htm](feats/class-08-FjuuX0vUWlYchRNM.htm)|Inspire Heroics|Inspiration héroïque|officielle|
|[class-08-fJxIdcg7kWPwlULY.htm](feats/class-08-fJxIdcg7kWPwlULY.htm)|Guardian's Embrace|Étreinte du gardien|libre|
|[class-08-FNW66GfWXaiBSexb.htm](feats/class-08-FNW66GfWXaiBSexb.htm)|Impervious Vehicle|Véhicule étanche|libre|
|[class-08-Fs88vjez9px2mmrC.htm](feats/class-08-Fs88vjez9px2mmrC.htm)|Spell Swipe|Balayage de sort|libre|
|[class-08-gepQGtV8Ftr0JJ6O.htm](feats/class-08-gepQGtV8Ftr0JJ6O.htm)|Skyseeker|Cherche-ciel|libre|
|[class-08-gHc9mqHiMqayiOIx.htm](feats/class-08-gHc9mqHiMqayiOIx.htm)|Boost Summons|Boostez les conjurations|libre|
|[class-08-gHcVxdMksOaGaiCx.htm](feats/class-08-gHcVxdMksOaGaiCx.htm)|Chronomancer's Secrets|Secrets du chronomancien|libre|
|[class-08-gQ2EvesPqLbISLQV.htm](feats/class-08-gQ2EvesPqLbISLQV.htm)|Feral Mutagen|Mutagène sauvage|officielle|
|[class-08-gTg3D9Dv9L4NEVhV.htm](feats/class-08-gTg3D9Dv9L4NEVhV.htm)|Gardener's Resolve|Résolution du jardinier|libre|
|[class-08-gtHTr77BkK4CckEH.htm](feats/class-08-gtHTr77BkK4CckEH.htm)|Basic Beast Gunner Spellcasting|Incantation basique du bestioléro|libre|
|[class-08-gYcmow7HM8J3giwL.htm](feats/class-08-gYcmow7HM8J3giwL.htm)|Grand Dance|Grand bal|libre|
|[class-08-H6qletvAJUCC1aIa.htm](feats/class-08-H6qletvAJUCC1aIa.htm)|Hulking Size|Taille considérable|libre|
|[class-08-HIyuVIh2XSDz3h2j.htm](feats/class-08-HIyuVIh2XSDz3h2j.htm)|Skim Scroll|Parchemin parcouru|libre|
|[class-08-HLC9g1pwluDl6vy7.htm](feats/class-08-HLC9g1pwluDl6vy7.htm)|Read Disaster|Lire le désastre|libre|
|[class-08-hLMsARkiwRf4SwqZ.htm](feats/class-08-hLMsARkiwRf4SwqZ.htm)|Improved Command Corpse|Contrôle amélioré des cadavres|libre|
|[class-08-HlSwpxreIfsglTJ8.htm](feats/class-08-HlSwpxreIfsglTJ8.htm)|Lethargy Poisoner|Empoisonneur soporifique|libre|
|[class-08-i7hNUqiJsB8hgIET.htm](feats/class-08-i7hNUqiJsB8hgIET.htm)|Quick Stow (Swordmaster)|Rangement rapide (Maître épéiste)|libre|
|[class-08-ivSf8wSIUkK8gwej.htm](feats/class-08-ivSf8wSIUkK8gwej.htm)|Live Ammunition|Munition vivante|libre|
|[class-08-iy9XKih5jIAdv67c.htm](feats/class-08-iy9XKih5jIAdv67c.htm)|Debilitating Dichotomy|Dichotomie débilitante|libre|
|[class-08-IZupJre7o5We2VrK.htm](feats/class-08-IZupJre7o5We2VrK.htm)|Vicious Fangs|Crocs vicieux|libre|
|[class-08-j4zGMRiTi5t6guMF.htm](feats/class-08-j4zGMRiTi5t6guMF.htm)|Disorienting Opening|Ouverture déroutante|libre|
|[class-08-jaekke9HomT4PZ9b.htm](feats/class-08-jaekke9HomT4PZ9b.htm)|Reflexive Grip|Poigne réflexe|libre|
|[class-08-jCIBYryi6Y3JwmqH.htm](feats/class-08-jCIBYryi6Y3JwmqH.htm)|Mixed Maneuver|Manoeuvre mixte|officielle|
|[class-08-jfsmtOx4QgXB19BL.htm](feats/class-08-jfsmtOx4QgXB19BL.htm)|Tumbling Strike|Frappe acrobatique|libre|
|[class-08-JHJBmiyILzWdFRJO.htm](feats/class-08-JHJBmiyILzWdFRJO.htm)|Advanced Deity's Domain|Domaine avancé de la divinité|officielle|
|[class-08-jIMeialR9CBo1bx9.htm](feats/class-08-jIMeialR9CBo1bx9.htm)|Levering Strike|Frappe levier|libre|
|[class-08-Jj6sVfIX81tgvSY4.htm](feats/class-08-Jj6sVfIX81tgvSY4.htm)|Wind Caller|Celui qui appelle le vent|officielle|
|[class-08-jkBzlMB4TS1sS2Fm.htm](feats/class-08-jkBzlMB4TS1sS2Fm.htm)|Stunning Finisher|Aboutissement étourdissant|libre|
|[class-08-JOq4Xe49A04YycRz.htm](feats/class-08-JOq4Xe49A04YycRz.htm)|Call And Response|Appel et réponse|libre|
|[class-08-Joy1e6pdqx6fN9mH.htm](feats/class-08-Joy1e6pdqx6fN9mH.htm)|Incredible Familiar (Thaumaturge)|Formidable familier (Thaumaturge)|libre|
|[class-08-jTOxxy19o2VRJTbH.htm](feats/class-08-jTOxxy19o2VRJTbH.htm)|Golden League Xun Dedication|Dévouement : Xun de la Ligue dorée|libre|
|[class-08-jwcNyPDVw313KXZU.htm](feats/class-08-jwcNyPDVw313KXZU.htm)|Armored Rebuff|Repousser avec l'armure|libre|
|[class-08-Jwq5o13uZF3ooln1.htm](feats/class-08-Jwq5o13uZF3ooln1.htm)|Jellyfish Stance|Posture de la méduse|libre|
|[class-08-jYKKnr41OqQrf7hv.htm](feats/class-08-jYKKnr41OqQrf7hv.htm)|Efficient Rituals|Rituels efficaces|libre|
|[class-08-jZalt2bFGjK8XXcP.htm](feats/class-08-jZalt2bFGjK8XXcP.htm)|Many Guises|Aspects nombreux|libre|
|[class-08-jZy91ekcS9ZqmdEH.htm](feats/class-08-jZy91ekcS9ZqmdEH.htm)|Knight's Retaliation|Représailles du chevalier|libre|
|[class-08-kPjBGlHMvBqFXNq2.htm](feats/class-08-kPjBGlHMvBqFXNq2.htm)|Cursed Effigy|Effigie maudite|libre|
|[class-08-kQfqJLpKOPORq7I6.htm](feats/class-08-kQfqJLpKOPORq7I6.htm)|Work Yourself Up|Laissez vous aller|libre|
|[class-08-kYYB7ziQZjlgQWWu.htm](feats/class-08-kYYB7ziQZjlgQWWu.htm)|Call Ursine Ally|Invoquer un allié ursin|libre|
|[class-08-kZdcoaTD849QalR9.htm](feats/class-08-kZdcoaTD849QalR9.htm)|Greater Mercy|Soulagement supérieur|officielle|
|[class-08-l8KQgN8icNrzYIav.htm](feats/class-08-l8KQgN8icNrzYIav.htm)|Form Retention|Conservation de forme|libre|
|[class-08-L9aBLPG2veBkVow9.htm](feats/class-08-L9aBLPG2veBkVow9.htm)|Persistent Creation|Création persistante|libre|
|[class-08-Le30algCdKIsxmeK.htm](feats/class-08-Le30algCdKIsxmeK.htm)|Ferocious Shape|Morphologie féroce|officielle|
|[class-08-lH8FwUM9bX9rpVsa.htm](feats/class-08-lH8FwUM9bX9rpVsa.htm)|Dream Magic|Magie onirique|libre|
|[class-08-lMA3F9SGzGV79P5C.htm](feats/class-08-lMA3F9SGzGV79P5C.htm)|Strangle|Étranglement|libre|
|[class-08-LnSMRHjMArCkE4w1.htm](feats/class-08-LnSMRHjMArCkE4w1.htm)|Heal Mount|Guérison de destrier|officielle|
|[class-08-lUjWE40fjAjHecNQ.htm](feats/class-08-lUjWE40fjAjHecNQ.htm)|Incredible Megafauna Companion|Formidable compagnon mégafaune|libre|
|[class-08-LVTquA3DpqCJDika.htm](feats/class-08-LVTquA3DpqCJDika.htm)|Instinctive Strike|Frappe instinctive|libre|
|[class-08-lVXk0fhZqjqKilhB.htm](feats/class-08-lVXk0fhZqjqKilhB.htm)|Energy Resistance|Résistance à l'énergie|libre|
|[class-08-lvxzgLNzmScMY0uC.htm](feats/class-08-lvxzgLNzmScMY0uC.htm)|Stalwart Mind|Esprit robuste|libre|
|[class-08-m8iP2OCzit9WUrMD.htm](feats/class-08-m8iP2OCzit9WUrMD.htm)|Enchanting Arrow|Flèche enchantée|libre|
|[class-08-mf2cdCRV8uowOMOm.htm](feats/class-08-mf2cdCRV8uowOMOm.htm)|Dueling Riposte|Riposte en duel|officielle|
|[class-08-MhoGCLKI5zxQ4SFD.htm](feats/class-08-MhoGCLKI5zxQ4SFD.htm)|Tactical Entry|Entrée tactique|libre|
|[class-08-MjPqgBQU9W4kelfz.htm](feats/class-08-MjPqgBQU9W4kelfz.htm)|Interrupt Charge|Charge interrompue|libre|
|[class-08-MlxfNv98LZKfYl64.htm](feats/class-08-MlxfNv98LZKfYl64.htm)|Precious Arrow|Flèche précieuse|libre|
|[class-08-MRxQDZFNPpUKC0CL.htm](feats/class-08-MRxQDZFNPpUKC0CL.htm)|Surging Focus|Focalisation déferlante|libre|
|[class-08-MSqgBffcAXTg700A.htm](feats/class-08-MSqgBffcAXTg700A.htm)|Preventative Treatment|Traitement préventif|libre|
|[class-08-mUL0C7O4HSnPSXea.htm](feats/class-08-mUL0C7O4HSnPSXea.htm)|Warped Constriction|Constriction déformée|libre|
|[class-08-Mvay7CiSN8snJ7DK.htm](feats/class-08-Mvay7CiSN8snJ7DK.htm)|Perpetual Breadth|Élargissement perpétuel|libre|
|[class-08-MxNb97qr1yMhbjiP.htm](feats/class-08-MxNb97qr1yMhbjiP.htm)|Golem Grafter Dedication|Dévouement : Greffeur de golem|libre|
|[class-08-n7LTwKUSATLaQ9FD.htm](feats/class-08-n7LTwKUSATLaQ9FD.htm)|Survivor of Desolation|Survivant de la désolation|libre|
|[class-08-nAVLB5MLYWUu8N71.htm](feats/class-08-nAVLB5MLYWUu8N71.htm)|Storm Shroud|Voile de tempête|libre|
|[class-08-NBvhmNqneeok7ZOr.htm](feats/class-08-NBvhmNqneeok7ZOr.htm)|Overdrive Ally|Allié en surrégime|libre|
|[class-08-nEmaHLsZEBru1Jjv.htm](feats/class-08-nEmaHLsZEBru1Jjv.htm)|Courageous Opportunity|Opportunité vaillante|libre|
|[class-08-niKCBA11zzgGT1PU.htm](feats/class-08-niKCBA11zzgGT1PU.htm)|Primal Breadth|Amplitude primordiale|officielle|
|[class-08-nL82Dzh0QwkNkJDA.htm](feats/class-08-nL82Dzh0QwkNkJDA.htm)|Disarming Assault|Assaut désarmant|libre|
|[class-08-nLTTph2tgwcQghVq.htm](feats/class-08-nLTTph2tgwcQghVq.htm)|Cremate Undead|Incinération de morts-vivants|officielle|
|[class-08-nlXyh7828TgZIewv.htm](feats/class-08-nlXyh7828TgZIewv.htm)|Capture Magic|Magie capturée|libre|
|[class-08-NLyldMwxUWaanlzH.htm](feats/class-08-NLyldMwxUWaanlzH.htm)|Thoughtsense|Perception des pensées|libre|
|[class-08-nwcsv54Vs4YBGFs9.htm](feats/class-08-nwcsv54Vs4YBGFs9.htm)|Whispering Steps|Pas induit|libre|
|[class-08-nx2nB10rypAuspAa.htm](feats/class-08-nx2nB10rypAuspAa.htm)|Acquired Tolerance|Tolérance acquise|libre|
|[class-08-ny0nfGTDUE4p8TtO.htm](feats/class-08-ny0nfGTDUE4p8TtO.htm)|Ubiquitous Gadgets|Gadgets omniprésents|libre|
|[class-08-o98nJriWE95xgweg.htm](feats/class-08-o98nJriWE95xgweg.htm)|Wayfinder Resonance Infiltrator|Infiltrateur de résonance de guide|libre|
|[class-08-oEjRfI4ATIFxDCzL.htm](feats/class-08-oEjRfI4ATIFxDCzL.htm)|Witch's Bottle|Bouteille du sorcier|libre|
|[class-08-OeUP3ASaS5deBsWw.htm](feats/class-08-OeUP3ASaS5deBsWw.htm)|Accurate Swing|Swing précis|libre|
|[class-08-oHra9QanDFpAZ4hh.htm](feats/class-08-oHra9QanDFpAZ4hh.htm)|Warden's Boon|Aide du protecteur|officielle|
|[class-08-Olx796SgBbHUFeHc.htm](feats/class-08-Olx796SgBbHUFeHc.htm)|Armiger's Mobility|Mobilité de l'écuyer|officielle|
|[class-08-oSi9mUtYWYekxX0B.htm](feats/class-08-oSi9mUtYWYekxX0B.htm)|Able Ritualist|Ritualiste habile|libre|
|[class-08-ossGwqEvC6ifPwgT.htm](feats/class-08-ossGwqEvC6ifPwgT.htm)|Swap Reflections|Échanger les reflets|libre|
|[class-08-OtM4pvPeIeEEdpuS.htm](feats/class-08-OtM4pvPeIeEEdpuS.htm)|Rule of Three|Règle des trois|libre|
|[class-08-oTTddwzF9TPNkMyd.htm](feats/class-08-oTTddwzF9TPNkMyd.htm)|Accompany|Accompagnement|libre|
|[class-08-P092yzkGN4UYYVXB.htm](feats/class-08-P092yzkGN4UYYVXB.htm)|Statement Strut|Pavane élégante|libre|
|[class-08-P5Zu46vfO4ptBBoj.htm](feats/class-08-P5Zu46vfO4ptBBoj.htm)|Mind Projectiles|Projectiles spirituels|libre|
|[class-08-PaUpesy5lyDLlwud.htm](feats/class-08-PaUpesy5lyDLlwud.htm)|Transcribe Moment|Transcrire l'instant|libre|
|[class-08-PccekOihIbRWdDky.htm](feats/class-08-PccekOihIbRWdDky.htm)|Malleable Mental Forge|Forge mentale malléable|libre|
|[class-08-PcDvw2vWTzBEIX7k.htm](feats/class-08-PcDvw2vWTzBEIX7k.htm)|Necromantic Tenacity|Ténacité contre la nécromancie|officielle|
|[class-08-PmJHC91WEPwrMDNW.htm](feats/class-08-PmJHC91WEPwrMDNW.htm)|Know-It-All|Je-sais-tout|libre|
|[class-08-po0WNUVwtaEBK8LH.htm](feats/class-08-po0WNUVwtaEBK8LH.htm)|Eerie Environs|Environs mystérieux|libre|
|[class-08-PP1gfRCc1YwnQGxp.htm](feats/class-08-PP1gfRCc1YwnQGxp.htm)|Dual Finisher|Aboutissement dual|libre|
|[class-08-pRqcm5P2ZFihSpVI.htm](feats/class-08-pRqcm5P2ZFihSpVI.htm)|Quick Shield Block|Blocage au bouclier éclair|officielle|
|[class-08-PTXZ2C3AV8tZf0iX.htm](feats/class-08-PTXZ2C3AV8tZf0iX.htm)|Deeper Dabbler|Amateur approfondi|libre|
|[class-08-pu8Mg2FIrhBYTNnB.htm](feats/class-08-pu8Mg2FIrhBYTNnB.htm)|Duo's Aim|Visée du duo|libre|
|[class-08-PuyvasWeofGMrhpu.htm](feats/class-08-PuyvasWeofGMrhpu.htm)|Opportune Backstab|Coup de poignard opportuniste|officielle|
|[class-08-Q21KzuubUBBkoges.htm](feats/class-08-Q21KzuubUBBkoges.htm)|Gigaton Strike|Frappe gigatonne|libre|
|[class-08-qgW8uHJXJGl3DKBS.htm](feats/class-08-qgW8uHJXJGl3DKBS.htm)|Incredible Companion (Druid)|Formidable compagnon (Druide)|officielle|
|[class-08-QkKMile0qqmuVY67.htm](feats/class-08-QkKMile0qqmuVY67.htm)|Clinging Shadows Initiate|Initié des ombres tenaces|libre|
|[class-08-QN55ToyJIjxgPmhs.htm](feats/class-08-QN55ToyJIjxgPmhs.htm)|Perfect Resistance|Résistance parfaite|libre|
|[class-08-qnMdgukNGmLWtSbZ.htm](feats/class-08-qnMdgukNGmLWtSbZ.htm)|The Harder They Fall (Kingmaker)|Plus dure est la chute (Kingmaker)|libre|
|[class-08-QQCxSQaX8UEZHfUz.htm](feats/class-08-QQCxSQaX8UEZHfUz.htm)|Unbelievable Luck|Chance incroyable|libre|
|[class-08-qUBr1YsQw3BSNy9c.htm](feats/class-08-qUBr1YsQw3BSNy9c.htm)|Glyph Expert|Expert des glyphes|libre|
|[class-08-qXWcmyHLkMlzRffC.htm](feats/class-08-qXWcmyHLkMlzRffC.htm)|Vantage Shot|Tir d'observation|libre|
|[class-08-QYZovCDjOhBb1u01.htm](feats/class-08-QYZovCDjOhBb1u01.htm)|Innate Magical Intuition|Intuition magique innée|libre|
|[class-08-R337W7VveJxlEddE.htm](feats/class-08-R337W7VveJxlEddE.htm)|Necromantic Bulwark|Rempart nécromantique|libre|
|[class-08-r7srDh7Iz94vfwwN.htm](feats/class-08-r7srDh7Iz94vfwwN.htm)|Countercharm|Contre charme|libre|
|[class-08-rByA8NDI6ZtNgBeT.htm](feats/class-08-rByA8NDI6ZtNgBeT.htm)|Mobile Shot Stance|Posture de tir mobile|officielle|
|[class-08-RHEdvEFz3QKiRKlr.htm](feats/class-08-RHEdvEFz3QKiRKlr.htm)|Topple Giants|Renverser les géants|libre|
|[class-08-RivbJYEBUyfLwPh7.htm](feats/class-08-RivbJYEBUyfLwPh7.htm)|Scarecrow|Épouvantail|libre|
|[class-08-rKZE8BA9IQHSSWoW.htm](feats/class-08-rKZE8BA9IQHSSWoW.htm)|Emblazon Energy|Énergie blasonnée|officielle|
|[class-08-rMPL11JRcmlutvRi.htm](feats/class-08-rMPL11JRcmlutvRi.htm)|Thrash|Rosser|libre|
|[class-08-rpxFVUp8BuF31DYg.htm](feats/class-08-rpxFVUp8BuF31DYg.htm)|Universal Versatility|Polyvalence universelle|officielle|
|[class-08-Rqj1kmrnF9M1E6pv.htm](feats/class-08-Rqj1kmrnF9M1E6pv.htm)|Supreme Psychic Center|Centre psychique suprême|libre|
|[class-08-RsNvCSrCN7czHC0G.htm](feats/class-08-RsNvCSrCN7czHC0G.htm)|Ricochet Stance (Rogue)|Posture du ricochet (Roublard)|libre|
|[class-08-RU86cGTryRAdaEqx.htm](feats/class-08-RU86cGTryRAdaEqx.htm)|Swashbuckler's Speed|Vitesse du bretteur|libre|
|[class-08-s2GbcUIXG1ZBurSd.htm](feats/class-08-s2GbcUIXG1ZBurSd.htm)|Hamstringing Strike|Frappe aux ischio-jambiers|libre|
|[class-08-SAOtGk9k8veaX3Ww.htm](feats/class-08-SAOtGk9k8veaX3Ww.htm)|Flamboyant Cruelty|Cruauté flamboyante|libre|
|[class-08-sbYDDDUWYN6Qx71k.htm](feats/class-08-sbYDDDUWYN6Qx71k.htm)|Crude Communication|Ébauche de communication|libre|
|[class-08-sCkzwTBLyE8FdzWI.htm](feats/class-08-sCkzwTBLyE8FdzWI.htm)|Smoldering Explosion|Explosion fumante|libre|
|[class-08-SE38R6zpv2XelzZk.htm](feats/class-08-SE38R6zpv2XelzZk.htm)|Call Gun|Appel du pistolet|libre|
|[class-08-SfBXPmADMFiZIBQt.htm](feats/class-08-SfBXPmADMFiZIBQt.htm)|Call Your Shot|Annoncer votre coup|libre|
|[class-08-si8FGX2ZRxetdVHp.htm](feats/class-08-si8FGX2ZRxetdVHp.htm)|Constricting Hold|Prise de constriction|libre|
|[class-08-sIN6t7nCWdI5u1HK.htm](feats/class-08-sIN6t7nCWdI5u1HK.htm)|Spirit's Anguish|Angoisse spirituelle|libre|
|[class-08-Sr75bQtqmCM6dyAM.htm](feats/class-08-Sr75bQtqmCM6dyAM.htm)|Breath Of The Dragon|Souffle de dragon|libre|
|[class-08-T6Twdesb5niBhHuZ.htm](feats/class-08-T6Twdesb5niBhHuZ.htm)|Jumping Jenny Display|Démonstration de Saut de Njini|libre|
|[class-08-tBg3FZST3nX5TfLf.htm](feats/class-08-tBg3FZST3nX5TfLf.htm)|Safeguard Soul|Âme à l'abri|libre|
|[class-08-TDaf3DbWymxPmHrO.htm](feats/class-08-TDaf3DbWymxPmHrO.htm)|What Could Have Been|Ce qui aurait pu être|libre|
|[class-08-TEH73yqZBqByO624.htm](feats/class-08-TEH73yqZBqByO624.htm)|Positioning Assault|Assaut de positionnement|officielle|
|[class-08-TGFbFyv0AUi5gAGO.htm](feats/class-08-TGFbFyv0AUi5gAGO.htm)|Monk Moves|Déplacement du moine|officielle|
|[class-08-tH5w8vIfFCFNVP7T.htm](feats/class-08-tH5w8vIfFCFNVP7T.htm)|Invoke the Crimson Oath|Invoquer le Serment écarlate|libre|
|[class-08-thNurw2OnN9jpBGV.htm](feats/class-08-thNurw2OnN9jpBGV.htm)|World-Wise Vigilance|Vigilance mondiale|libre|
|[class-08-TOyqtUUnOkOLl1Pm.htm](feats/class-08-TOyqtUUnOkOLl1Pm.htm)|Eclectic Skill|Compétence éclectique|officielle|
|[class-08-tPb0FVkNDE89ACbC.htm](feats/class-08-tPb0FVkNDE89ACbC.htm)|Incredible Familiar (Witch)|Formidable familier (Sorcier)|libre|
|[class-08-Tr2SnOE2WqFIIWIK.htm](feats/class-08-Tr2SnOE2WqFIIWIK.htm)|Enlarge Companion|Compagnon aggrandi|libre|
|[class-08-TzBP8yiZQHNhei1V.htm](feats/class-08-TzBP8yiZQHNhei1V.htm)|Walk The Plank|Marcher sur la planche|libre|
|[class-08-u2rvvAqZBugZgcYg.htm](feats/class-08-u2rvvAqZBugZgcYg.htm)|Greater Cruelty|Cruauté supérieure|libre|
|[class-08-u8hKEb71pI45XP1f.htm](feats/class-08-u8hKEb71pI45XP1f.htm)|Minor Omen|Présage mineur|libre|
|[class-08-UcIyf7bTDf6RwydU.htm](feats/class-08-UcIyf7bTDf6RwydU.htm)|Makeshift Strike|Frappe expéditive|libre|
|[class-08-UfuYHdozZD586RWd.htm](feats/class-08-UfuYHdozZD586RWd.htm)|Bloodline Breadth|Amplitude de lignage|officielle|
|[class-08-UFVw57jWNC4UCfyN.htm](feats/class-08-UFVw57jWNC4UCfyN.htm)|Deimatic Display|Intimidation déimatique|libre|
|[class-08-UIMZe4QDJQ9k4npN.htm](feats/class-08-UIMZe4QDJQ9k4npN.htm)|Stab and Blast|Poignardez et tirez|libre|
|[class-08-uR44wELN9OlU68cL.htm](feats/class-08-uR44wELN9OlU68cL.htm)|Advanced Domain|Domaine avancé|officielle|
|[class-08-URtcR1BU2OgfKHfm.htm](feats/class-08-URtcR1BU2OgfKHfm.htm)|Metabolize Element|Élément métabolisé|libre|
|[class-08-V3nNlrdU2OxYJAjx.htm](feats/class-08-V3nNlrdU2OxYJAjx.htm)|Blessed Spell|Sort de l'Élu divin|libre|
|[class-08-v4O6eDiSOkzQZHmT.htm](feats/class-08-v4O6eDiSOkzQZHmT.htm)|Skill Mastery (Investigator)|Maîtrise des compétences (Enquêteur)|libre|
|[class-08-V9kShXu84NlORfcg.htm](feats/class-08-V9kShXu84NlORfcg.htm)|Friendly Toss|Lancer amical|libre|
|[class-08-vayNZR1bTzU1oUa3.htm](feats/class-08-vayNZR1bTzU1oUa3.htm)|Share Rage|Rage partagée|officielle|
|[class-08-vhHKUooXX3PYqGaU.htm](feats/class-08-vhHKUooXX3PYqGaU.htm)|Bond Conservation|Conservation du lien|officielle|
|[class-08-VKNxblSUxYXQYlLr.htm](feats/class-08-VKNxblSUxYXQYlLr.htm)|Terrain Master|Maître de l'environnement|officielle|
|[class-08-vNIimhmP636VOP01.htm](feats/class-08-vNIimhmP636VOP01.htm)|Bullet Split|Projectile fendu|libre|
|[class-08-VO8HbMQ79NULE4FQ.htm](feats/class-08-VO8HbMQ79NULE4FQ.htm)|Elaborate Talisman Esoterica|Ésotéricas talismaniques élaborés|libre|
|[class-08-VqzXfdbQQ2e8Hndr.htm](feats/class-08-VqzXfdbQQ2e8Hndr.htm)|Unshakable Idealism|Idéalisme inébranlable|libre|
|[class-08-vUFvcrvszXlHvz2Y.htm](feats/class-08-vUFvcrvszXlHvz2Y.htm)|Magic Arrow|Flèche magique|libre|
|[class-08-vVyX9IG8flxg81mc.htm](feats/class-08-vVyX9IG8flxg81mc.htm)|Elude the Divine|Éluder le divin|libre|
|[class-08-VZczZNj3ozCj1Lzk.htm](feats/class-08-VZczZNj3ozCj1Lzk.htm)|Second Ally|Deuxième allié|officielle|
|[class-08-WDid62NmmC6NiTE6.htm](feats/class-08-WDid62NmmC6NiTE6.htm)|Smoke Curtain|Rideau de fumée|libre|
|[class-08-wIAiDaNztd52ltT2.htm](feats/class-08-wIAiDaNztd52ltT2.htm)|Positive Luminance|Luminosité positive|libre|
|[class-08-WmSggxmf4iAe2aRD.htm](feats/class-08-WmSggxmf4iAe2aRD.htm)|Recycled Cogwheels|Recyclage de roues dentées|libre|
|[class-08-wNHUryoRzlfDCFAd.htm](feats/class-08-wNHUryoRzlfDCFAd.htm)|Soaring Shape|Morphologie volante|officielle|
|[class-08-Wz0LLKjEi8GfKloV.htm](feats/class-08-Wz0LLKjEi8GfKloV.htm)|Patron's Breadth|Amplitude du patron|libre|
|[class-08-X6nwiA2uJ2UQIOKB.htm](feats/class-08-X6nwiA2uJ2UQIOKB.htm)|Drive Back|Retour en arrière|libre|
|[class-08-X71qmyGpKN1XAoT6.htm](feats/class-08-X71qmyGpKN1XAoT6.htm)|Shamble|Tituber|libre|
|[class-08-x9cYkB8DrUBBwqJd.htm](feats/class-08-x9cYkB8DrUBBwqJd.htm)|Ironblood Stance|Posture du sang-de-fer|officielle|
|[class-08-xhFsHEsMTAvzwJhr.htm](feats/class-08-xhFsHEsMTAvzwJhr.htm)|Incredible Reanimated Companion|Formidable compagnon réanimé|libre|
|[class-08-XkmrLhyAoxQTLnza.htm](feats/class-08-XkmrLhyAoxQTLnza.htm)|Lobbed Attack|Attaque lobbée|libre|
|[class-08-XoxawV3Fmn61VJcS.htm](feats/class-08-XoxawV3Fmn61VJcS.htm)|Steal Vitality|Voler la vitalité|libre|
|[class-08-XRIWWrXfghsQce4S.htm](feats/class-08-XRIWWrXfghsQce4S.htm)|Divine Breadth|Amplitude divine|officielle|
|[class-08-XtIPmZ3Ihq5NJHP2.htm](feats/class-08-XtIPmZ3Ihq5NJHP2.htm)|Pinpoint Poisoner|Empoisonneur opportun|libre|
|[class-08-xtXWw3cUnVB25XSV.htm](feats/class-08-xtXWw3cUnVB25XSV.htm)|Align Armament|Arsenal aligné|officielle|
|[class-08-XY2uS7pMKRLVNQKG.htm](feats/class-08-XY2uS7pMKRLVNQKG.htm)|Fey's Trickery|Tromperie de la fée|libre|
|[class-08-y2XeMe1F18lIyo59.htm](feats/class-08-y2XeMe1F18lIyo59.htm)|Blind-Fight|Combat en aveugle|libre|
|[class-08-y4Cws9vZj3Bf9uqH.htm](feats/class-08-y4Cws9vZj3Bf9uqH.htm)|Mysterious Breadth|Élargissement mystérieux|libre|
|[class-08-y7SYHv0DWkkwjT95.htm](feats/class-08-y7SYHv0DWkkwjT95.htm)|Retaliatory Cleansing|Purification vengeresse|libre|
|[class-08-YDpSnLmnSLsItP45.htm](feats/class-08-YDpSnLmnSLsItP45.htm)|Improvised Critical|Critique improvisé|libre|
|[class-08-YeyOqNFKaeuOTiJr.htm](feats/class-08-YeyOqNFKaeuOTiJr.htm)|Impassable Wall Stance|Posture du mur infranchissable|libre|
|[class-08-yg1ZTjHuSiQJFO0i.htm](feats/class-08-yg1ZTjHuSiQJFO0i.htm)|Future Spell Learning|Apprentissage futur de sort|libre|
|[class-08-YGBPIpHaOgCsa2qO.htm](feats/class-08-YGBPIpHaOgCsa2qO.htm)|Soulsight (Sorcerer)|Vision de l'âme (Ensorceleur)|libre|
|[class-08-yIDJTe4kimRoZN0L.htm](feats/class-08-yIDJTe4kimRoZN0L.htm)|Controlled Blast|Explosion contrôlée|libre|
|[class-08-Yj0IKXM49Ver5Smc.htm](feats/class-08-Yj0IKXM49Ver5Smc.htm)|Siphoning Touch|Contact siphonnant|libre|
|[class-08-YMrcnO2l3mA71ohM.htm](feats/class-08-YMrcnO2l3mA71ohM.htm)|Frozen Breadth|Amplitude givrée|libre|
|[class-08-yOybxBkebeeuHWuy.htm](feats/class-08-yOybxBkebeeuHWuy.htm)|Magical Knowledge|Connaissance magique|libre|
|[class-08-YrRlbIzjsFlGGmVN.htm](feats/class-08-YrRlbIzjsFlGGmVN.htm)|Black Powder Blaze|Flamme de poudre noire|libre|
|[class-08-yTUIiE9LXBZaA7aG.htm](feats/class-08-yTUIiE9LXBZaA7aG.htm)|Whodunnit?|Quiafaitçà ?|libre|
|[class-08-YTZhLWtrEnV9Pjf2.htm](feats/class-08-YTZhLWtrEnV9Pjf2.htm)|Bravo's Determination|Détermination du bravache|libre|
|[class-08-Yvxr1Q2TslWiKqqi.htm](feats/class-08-Yvxr1Q2TslWiKqqi.htm)|Occult Breadth|Amplitude occulte|officielle|
|[class-08-Yw0qVCDu94Y5TgxQ.htm](feats/class-08-Yw0qVCDu94Y5TgxQ.htm)|Predictive Purchase (Rogue)|Achat intuitif (Roublard)|libre|
|[class-08-Yy9LXo2GaLT7eCOL.htm](feats/class-08-Yy9LXo2GaLT7eCOL.htm)|Deadly Butterfly|Papillon mortel|libre|
|[class-08-YYPjndZ7tqRQLtAH.htm](feats/class-08-YYPjndZ7tqRQLtAH.htm)|Beacon Mark|Marque balise|libre|
|[class-08-zAJY8rLvZjzOK0Mt.htm](feats/class-08-zAJY8rLvZjzOK0Mt.htm)|Sin Reservoir|Réservoir du pécheur|libre|
|[class-08-ZdL8pPPV0QCkBML1.htm](feats/class-08-ZdL8pPPV0QCkBML1.htm)|Subtle Shank|Surin subtil|libre|
|[class-08-zf6Poru1jNmrO3kk.htm](feats/class-08-zf6Poru1jNmrO3kk.htm)|Bloodline Resistance|Résistance du lignage|officielle|
|[class-08-zJXsGF61lH0WHw5v.htm](feats/class-08-zJXsGF61lH0WHw5v.htm)|Ghost Flight|Vol du fantôme|libre|
|[class-08-ZlL18xGsa76isqkX.htm](feats/class-08-ZlL18xGsa76isqkX.htm)|Runic Impression|Impression runique|libre|
|[class-08-Zn2uUL5i3MAZ0Zwc.htm](feats/class-08-Zn2uUL5i3MAZ0Zwc.htm)|Standby Spell|Sort en attente|libre|
|[class-08-zQeglKcmBXvqfABR.htm](feats/class-08-zQeglKcmBXvqfABR.htm)|Brain Drain|Drainer le cerveau|libre|
|[class-08-ztxbGySxIEeWvsAT.htm](feats/class-08-ztxbGySxIEeWvsAT.htm)|Pact of Infernal Prowess|Pacte de prouesse infernale|libre|
|[class-08-ZXaDS4OJvsQYvhBZ.htm](feats/class-08-ZXaDS4OJvsQYvhBZ.htm)|Submission Hold|Prise de soumission|libre|
|[class-08-zXKfKKOxht0b0XNL.htm](feats/class-08-zXKfKKOxht0b0XNL.htm)|Sticky Bomb|Bombe collante|officielle|
|[class-08-ZyQYP7i26DWhMNux.htm](feats/class-08-ZyQYP7i26DWhMNux.htm)|Unfazed Assessment|Estimation imperturbable|officielle|
|[class-08-zytTsipimVTmPc5U.htm](feats/class-08-zytTsipimVTmPc5U.htm)|Selfless Parry|Parade désintéressée|libre|
|[class-10-04Smj4ZsEBD8WIXv.htm](feats/class-10-04Smj4ZsEBD8WIXv.htm)|Dual-Weapon Blitz|Attaque éclair à deux armes|libre|
|[class-10-07jxXvRZ8nD3JLF4.htm](feats/class-10-07jxXvRZ8nD3JLF4.htm)|Imposing Destrier|Destrier imposant|officielle|
|[class-10-1F4FD5OdDCEyiEvk.htm](feats/class-10-1F4FD5OdDCEyiEvk.htm)|Deny the Songs of War|Faire taire les chants de guerre|libre|
|[class-10-1k3H7cnARIzAVCsm.htm](feats/class-10-1k3H7cnARIzAVCsm.htm)|Fearsome Brute|Brute effrayante|officielle|
|[class-10-23QgyEYjoslBvkra.htm](feats/class-10-23QgyEYjoslBvkra.htm)|Invigorating Breath|Souffle revigorant|libre|
|[class-10-2Nu4ZdKQM8hx8x5D.htm](feats/class-10-2Nu4ZdKQM8hx8x5D.htm)|Sneak Savant|Science de la furtivité|libre|
|[class-10-2OYJOFaEkhc8dFbl.htm](feats/class-10-2OYJOFaEkhc8dFbl.htm)|Knockback Strike|Frappe repoussante|officielle|
|[class-10-2uQbQgz1AbjzcFSp.htm](feats/class-10-2uQbQgz1AbjzcFSp.htm)|Runic Mind Smithing|Façonnage spirituel de rune|libre|
|[class-10-2xxFg9yRuCDpME3z.htm](feats/class-10-2xxFg9yRuCDpME3z.htm)|Spy's Countermeasures|Contremesures d'espion|officielle|
|[class-10-39RJF47FLYr5gZ8p.htm](feats/class-10-39RJF47FLYr5gZ8p.htm)|Unseat|Désarçonner|libre|
|[class-10-3poGYUYCBTmbeCUs.htm](feats/class-10-3poGYUYCBTmbeCUs.htm)|Angel Of Death|Ange de la mort|libre|
|[class-10-3R09Hl6IDMgPcSs0.htm](feats/class-10-3R09Hl6IDMgPcSs0.htm)|Beastmaster Bond|Lien du maître des bêtes|libre|
|[class-10-4AezsqaQRFtX024w.htm](feats/class-10-4AezsqaQRFtX024w.htm)|Shall not Falter, Shall not Rout|Tu ne faibliras pas, tu ne prendras pas la fuite|libre|
|[class-10-4Cc0gQauzUqcYdLw.htm](feats/class-10-4Cc0gQauzUqcYdLw.htm)|Uncanny Dodge|Esquive instinctive|libre|
|[class-10-4IeAAmx2vZEHmRLX.htm](feats/class-10-4IeAAmx2vZEHmRLX.htm)|Witch's Communion|Communion du sorcier|libre|
|[class-10-4VbfFPuFpbGLMMKF.htm](feats/class-10-4VbfFPuFpbGLMMKF.htm)|Disarming Twist|Torsion désarmante|officielle|
|[class-10-4w73cxvqhGRiozsK.htm](feats/class-10-4w73cxvqhGRiozsK.htm)|Bear Empathy|Empathie ursine|libre|
|[class-10-4YqmeJQwnrG1Lg07.htm](feats/class-10-4YqmeJQwnrG1Lg07.htm)|Phenom's Verve|Verve du phénomène|libre|
|[class-10-5csEOLLbYUWJDJoS.htm](feats/class-10-5csEOLLbYUWJDJoS.htm)|Master Monster Hunter|Maître chasseur de monstres|officielle|
|[class-10-5GC2iGtVp3UAH2nm.htm](feats/class-10-5GC2iGtVp3UAH2nm.htm)|Glutton For Flesh|Avide de chair|libre|
|[class-10-5nzeyuvzKL4T8eLt.htm](feats/class-10-5nzeyuvzKL4T8eLt.htm)|Weapon-Rune Shifter|Changerune d'arme|officielle|
|[class-10-5tlTRfyPXkGS9Coq.htm](feats/class-10-5tlTRfyPXkGS9Coq.htm)|Lunging Spellstrike|Frappe de sort à allonge|libre|
|[class-10-5TPKikTyN7lrCvzY.htm](feats/class-10-5TPKikTyN7lrCvzY.htm)|Mighty Bulwark|Rempart puissant|libre|
|[class-10-647GZOyhWgcjw6Jg.htm](feats/class-10-647GZOyhWgcjw6Jg.htm)|Elucidating Mercy|Soulagement clarifiant|libre|
|[class-10-6dJokwhIMvjAHL52.htm](feats/class-10-6dJokwhIMvjAHL52.htm)|Practiced Reloads|Rechargements exercés|libre|
|[class-10-6Hv4kpHj8IexNUey.htm](feats/class-10-6Hv4kpHj8IexNUey.htm)|Tiller's Aid|Aide du Laboureur|libre|
|[class-10-6IrNSD0G5AAXPTJq.htm](feats/class-10-6IrNSD0G5AAXPTJq.htm)|Borrow Time|Emprunter du temps|libre|
|[class-10-6xBu4BewIkOIt9M0.htm](feats/class-10-6xBu4BewIkOIt9M0.htm)|Cut From The Air|Découper en l'air|libre|
|[class-10-7fyWKASX6uNByJF5.htm](feats/class-10-7fyWKASX6uNByJF5.htm)|Pass Through|Passer à travers|libre|
|[class-10-7nAoHfB7nsRwWDmF.htm](feats/class-10-7nAoHfB7nsRwWDmF.htm)|Expert Skysage Divination|Divination experte de Sagecéleste|libre|
|[class-10-7x1r2w7C7A4Uy7wG.htm](feats/class-10-7x1r2w7C7A4Uy7wG.htm)|Topple Foe|Renverser l'adversaire|libre|
|[class-10-81xlcIFJIAtsmXmd.htm](feats/class-10-81xlcIFJIAtsmXmd.htm)|Armor Rune Shifter|Changerune d'armure|officielle|
|[class-10-85V3vdew0gykEtmu.htm](feats/class-10-85V3vdew0gykEtmu.htm)|Healing Transformation|Transformation curative|officielle|
|[class-10-85xm82Z005CUNBMB.htm](feats/class-10-85xm82Z005CUNBMB.htm)|Twin Weakness|Faiblesse jumelle|libre|
|[class-10-8d3QxoKmqSkB9Mcj.htm](feats/class-10-8d3QxoKmqSkB9Mcj.htm)|Endure Death's Touch|Endurer le contact de la mort|libre|
|[class-10-8NQaqtHheeMUNGYr.htm](feats/class-10-8NQaqtHheeMUNGYr.htm)|Shared Synergy|Synergie partagée|libre|
|[class-10-9kY9B5WgtEleOicn.htm](feats/class-10-9kY9B5WgtEleOicn.htm)|Resounding Blow|Coup retentissant|libre|
|[class-10-9Qn5E7Ujye9KdxOj.htm](feats/class-10-9Qn5E7Ujye9KdxOj.htm)|Recover Spell|Récupération de sort|libre|
|[class-10-9uvymmdphxsD3yEd.htm](feats/class-10-9uvymmdphxsD3yEd.htm)|Dazzling Block|Blocage éblouissant|libre|
|[class-10-9VGmE7X4aK2W8YWj.htm](feats/class-10-9VGmE7X4aK2W8YWj.htm)|Dueling Dance (Swashbuckler)|Danse en duel (Bretteur)|libre|
|[class-10-9XXtDeRF2egCCzcx.htm](feats/class-10-9XXtDeRF2egCCzcx.htm)|Tectonic Stomp|Piétinement tectonique (Déviant)|libre|
|[class-10-A0ZQpGSB7pvIDiou.htm](feats/class-10-A0ZQpGSB7pvIDiou.htm)|Improved Command Undead|Contrôle des morts-vivants amélioré|officielle|
|[class-10-a2VAaXMzlQqALJeH.htm](feats/class-10-a2VAaXMzlQqALJeH.htm)|Share Weakness|Partager la faiblesse|libre|
|[class-10-aajcuwKYihsinPCt.htm](feats/class-10-aajcuwKYihsinPCt.htm)|Wronged Monk's Wrath|Courroux du moine lésé|libre|
|[class-10-ADgQzThbtGKvp6hy.htm](feats/class-10-ADgQzThbtGKvp6hy.htm)|Bestiary Scholar|Érudit du bestiaire|libre|
|[class-10-AGovcdAzmUqxgiOq.htm](feats/class-10-AGovcdAzmUqxgiOq.htm)|Fused Polearm|Arme d'hast fusionnée|libre|
|[class-10-aMef2VM4mSxl0pmy.htm](feats/class-10-aMef2VM4mSxl0pmy.htm)|Replenishment of War|Récupération martiale|officielle|
|[class-10-amf2zZuW299eiHAZ.htm](feats/class-10-amf2zZuW299eiHAZ.htm)|Everyone Duck!|À couvert !|libre|
|[class-10-AMgTG6TwfoNmseWm.htm](feats/class-10-AMgTG6TwfoNmseWm.htm)|Channel Rot|Canaliser la pourriture|libre|
|[class-10-Asb0UsQqeATsxqFJ.htm](feats/class-10-Asb0UsQqeATsxqFJ.htm)|Courageous Assault|Assaut vaillant|libre|
|[class-10-aVzkG1UhNjQr22pE.htm](feats/class-10-aVzkG1UhNjQr22pE.htm)|I Meant to do That|Je cherchais à faire çà|libre|
|[class-10-aWk0JaqVaUf2Cz9a.htm](feats/class-10-aWk0JaqVaUf2Cz9a.htm)|Twin Shot Knockdown|Tir doublé de renversement|libre|
|[class-10-AYByEEgPXk3QbCiF.htm](feats/class-10-AYByEEgPXk3QbCiF.htm)|Silent Sting|Piqûre silencieuse|libre|
|[class-10-b7KZ7Fg5u5z2gqvt.htm](feats/class-10-b7KZ7Fg5u5z2gqvt.htm)|Called Shot|Tir ciblé|libre|
|[class-10-B7VMXObJSNVI0ZGJ.htm](feats/class-10-B7VMXObJSNVI0ZGJ.htm)|Shattering Strike (Weapon Improviser)|Frappe fracassante (Improvisateur d'arme)|libre|
|[class-10-bBORql3kKsSyXllk.htm](feats/class-10-bBORql3kKsSyXllk.htm)|Monk's Flurry|Déluge de coups du moine|libre|
|[class-10-BBPrlPncXg86I42D.htm](feats/class-10-BBPrlPncXg86I42D.htm)|Counterclockwork Focus|Focalisation antimécanique|libre|
|[class-10-bJc477EbUYW2TlBx.htm](feats/class-10-bJc477EbUYW2TlBx.htm)|Drain Vitality|Drainer la vitalité|libre|
|[class-10-BJKTUGplI9nwhJxg.htm](feats/class-10-BJKTUGplI9nwhJxg.htm)|Agile Grace|Agilité gracieuse|officielle|
|[class-10-bqBCatllklPceA34.htm](feats/class-10-bqBCatllklPceA34.htm)|Turn to Mist|Se changer en brume|libre|
|[class-10-BV9k3nmVrWDLv8z6.htm](feats/class-10-BV9k3nmVrWDLv8z6.htm)|Emphatic Emissary|Émissaire empathique|libre|
|[class-10-C0ozuEhrKh9A1wMO.htm](feats/class-10-C0ozuEhrKh9A1wMO.htm)|Overpowering Charge|Charge surpuissante|libre|
|[class-10-C6BIHPqYiWM0wlvv.htm](feats/class-10-C6BIHPqYiWM0wlvv.htm)|Signature Spell Expansion (Psychic)|Expansion des sorts emblématiques (Psychiste)|libre|
|[class-10-CAk1NNG4aO0VuHnZ.htm](feats/class-10-CAk1NNG4aO0VuHnZ.htm)|Ongoing Strategy|Stratégie continue|libre|
|[class-10-CkK7WwaWnrLXK9sW.htm](feats/class-10-CkK7WwaWnrLXK9sW.htm)|Methodical Debilitations|Handicaps méthodiques|libre|
|[class-10-CMDCWPC8m2b6HvEN.htm](feats/class-10-CMDCWPC8m2b6HvEN.htm)|Spiritual Flurry|Déluge spirituel|libre|
|[class-10-Cn4w9U7uk5m1bb2S.htm](feats/class-10-Cn4w9U7uk5m1bb2S.htm)|Greater Debilitating Bomb|Bombe incapacitante supérieure|officielle|
|[class-10-cQ2jy4Utoxmk3MkG.htm](feats/class-10-cQ2jy4Utoxmk3MkG.htm)|Reactive Charm|Charme réactif|libre|
|[class-10-cTeRwExCGhfwJbkl.htm](feats/class-10-cTeRwExCGhfwJbkl.htm)|Sixth Pillar Dedication|Dévouement : Sixième pilier|libre|
|[class-10-cvo3DIL0BIRrDkQ6.htm](feats/class-10-cvo3DIL0BIRrDkQ6.htm)|Ode To Ouroboros|Ode à Ouroboros|libre|
|[class-10-cxAvcFiZRT3ZVhie.htm](feats/class-10-cxAvcFiZRT3ZVhie.htm)|Emerald Boughs Hideaway|Cachette des Branches d'Émeraude|libre|
|[class-10-cznEQ1W61MSaXW0u.htm](feats/class-10-cznEQ1W61MSaXW0u.htm)|Great Cleave|Grand coup tranchant|officielle|
|[class-10-D6bncWAOvFYX7hCE.htm](feats/class-10-D6bncWAOvFYX7hCE.htm)|Strident Command|Injonction stridente|libre|
|[class-10-d8DI7wLxtUy99g9K.htm](feats/class-10-d8DI7wLxtUy99g9K.htm)|Emotional Surge|Surcharge émotionnelle|libre|
|[class-10-DFtbxytrOrmkQRfm.htm](feats/class-10-DFtbxytrOrmkQRfm.htm)|Steal the Sky|Voler le ciel|libre|
|[class-10-DLkMoVb8qb4qxnx3.htm](feats/class-10-DLkMoVb8qb4qxnx3.htm)|Targeting Finisher|Aboutissement ciblé|libre|
|[class-10-dnm6c3nzGDHt3A76.htm](feats/class-10-dnm6c3nzGDHt3A76.htm)|Greenwatcher|Veilleur vert|libre|
|[class-10-DPaZurhC9uyIWPcu.htm](feats/class-10-DPaZurhC9uyIWPcu.htm)|Magical Adaptation|Adaptation magique|libre|
|[class-10-DpRMdytpPiCypmkJ.htm](feats/class-10-DpRMdytpPiCypmkJ.htm)|Greater Bloodline|Lignage supérieur|officielle|
|[class-10-dutiFC41YFllm8fM.htm](feats/class-10-dutiFC41YFllm8fM.htm)|House of Imaginary Walls|Demeure aux murs imaginaires|officielle|
|[class-10-dY0jhJOEj6DHc0ud.htm](feats/class-10-dY0jhJOEj6DHc0ud.htm)|Destructive Block|Blocage destructif|libre|
|[class-10-DZcy4sY07w0zPsDb.htm](feats/class-10-DZcy4sY07w0zPsDb.htm)|Rapid Recharge|Recharge rapide|libre|
|[class-10-dzeiVab4Cogzl5XS.htm](feats/class-10-dzeiVab4Cogzl5XS.htm)|Gift of the Hoard|Don du trésor|libre|
|[class-10-E1WXnYE2QwhHQxQb.htm](feats/class-10-E1WXnYE2QwhHQxQb.htm)|Sleeper Hold|Prise du sommeil|officielle|
|[class-10-EBqnbiuVL0ULFJWX.htm](feats/class-10-EBqnbiuVL0ULFJWX.htm)|Know It All (Eldritch Researcher)|Je-sais-tout (Chercheur mystique)|libre|
|[class-10-eceQmgvmoPplFKId.htm](feats/class-10-eceQmgvmoPplFKId.htm)|Rope Mastery|Maîtrise des cordes|libre|
|[class-10-EpWgrMznGbm8gceW.htm](feats/class-10-EpWgrMznGbm8gceW.htm)|Cautious Delver|Fouilleur précautionneux|libre|
|[class-10-ErKwliHplziJY2BW.htm](feats/class-10-ErKwliHplziJY2BW.htm)|Greater Magical Scholastics|Études magiques supérieures|libre|
|[class-10-F0MYBfiyOD8YHq5t.htm](feats/class-10-F0MYBfiyOD8YHq5t.htm)|Elemental Shape|Morphologie élémentaire|officielle|
|[class-10-F3ZBkDEWZ24NOR2j.htm](feats/class-10-F3ZBkDEWZ24NOR2j.htm)|Saving Slash|Parade salvatrice|officielle|
|[class-10-fCsIyglmpb7NYwiy.htm](feats/class-10-fCsIyglmpb7NYwiy.htm)|Harrying Strike|Frappe harassante|libre|
|[class-10-FIsMdgvGji5Nci8l.htm](feats/class-10-FIsMdgvGji5Nci8l.htm)|Devoted Focus|Focalisation dévouée|officielle|
|[class-10-FLQWJ2CIv9mCfSAx.htm](feats/class-10-FLQWJ2CIv9mCfSAx.htm)|Elastic Mutagen|Mutagène élastique|officielle|
|[class-10-FOk8xTCHcHYyENu2.htm](feats/class-10-FOk8xTCHcHYyENu2.htm)|Bat Form|Forme de chauve-souris|libre|
|[class-10-FWIQ3m5KZDWzDg47.htm](feats/class-10-FWIQ3m5KZDWzDg47.htm)|Wind Jump|Saut du vent|officielle|
|[class-10-fzERYW7BJQoxlvoD.htm](feats/class-10-fzERYW7BJQoxlvoD.htm)|Corpse-Killer's Defiance|Défi du tueur de cadavre|libre|
|[class-10-Gcliatty0MGYbTVV.htm](feats/class-10-Gcliatty0MGYbTVV.htm)|Oracular Warning|Avertissement oraculaire|libre|
|[class-10-gdGusdEKt5zmh3rR.htm](feats/class-10-gdGusdEKt5zmh3rR.htm)|Pushing Attack|Attaque repoussante|libre|
|[class-10-GJIAecRq1bD2r8O0.htm](feats/class-10-GJIAecRq1bD2r8O0.htm)|Twin Riposte|Riposte jumelée|officielle|
|[class-10-GLbl3qoWCvvjJr4S.htm](feats/class-10-GLbl3qoWCvvjJr4S.htm)|Tangle Of Battle|Enchevêtrement de la bataille|libre|
|[class-10-gSc4ZUXkesN5vKrm.htm](feats/class-10-gSc4ZUXkesN5vKrm.htm)|Debilitating Shot|Tir incapacitant|officielle|
|[class-10-gxzTEt37M0z1WY1M.htm](feats/class-10-gxzTEt37M0z1WY1M.htm)|Spinebreaker|Brise-vertèbre|libre|
|[class-10-gyVcJfZTmBytLsXq.htm](feats/class-10-gyVcJfZTmBytLsXq.htm)|Expanded Splash|Éclaboussure élargie|officielle|
|[class-10-H0tffYs7ODMQ3JJj.htm](feats/class-10-H0tffYs7ODMQ3JJj.htm)|Terrain Transposition|Transposition de terrain|libre|
|[class-10-h5DzKmKDADGhvmF9.htm](feats/class-10-h5DzKmKDADGhvmF9.htm)|Certain Strike|Frappe certaine|officielle|
|[class-10-h5ksUZlrVGBjq6p4.htm](feats/class-10-h5ksUZlrVGBjq6p4.htm)|Radiant Blade Spirit|Esprit lame radieux|officielle|
|[class-10-HgBksiMTUibPK36M.htm](feats/class-10-HgBksiMTUibPK36M.htm)|Halcyon Spellcasting Initiate|Initié de l'incantation syncrétique|libre|
|[class-10-HjinlKihkadhkQ4Z.htm](feats/class-10-HjinlKihkadhkQ4Z.htm)|Terrifying Howl|Hurlement terrifiant|officielle|
|[class-10-HKNOA5WGBWGEpdmH.htm](feats/class-10-HKNOA5WGBWGEpdmH.htm)|Redirecting Shot|Tir redirigé|libre|
|[class-10-HlqAdfxmcd9gdgHa.htm](feats/class-10-HlqAdfxmcd9gdgHa.htm)|Draw From the Land|Tiré de la terre|libre|
|[class-10-hRJV7byfPUHx1b9P.htm](feats/class-10-hRJV7byfPUHx1b9P.htm)|Greater Spell Runes|Rune de sort supérieure|officielle|
|[class-10-hsMcKK92ho39djgI.htm](feats/class-10-hsMcKK92ho39djgI.htm)|Communal Tale|Conte partagé|libre|
|[class-10-i8MnyasCDo3j65Xd.htm](feats/class-10-i8MnyasCDo3j65Xd.htm)|Quicken Heartbeat|Battements de coeur accélérés|libre|
|[class-10-I9rSWQyueWHQyNxe.htm](feats/class-10-I9rSWQyueWHQyNxe.htm)|Plant Shape|Morphologie végétales|officielle|
|[class-10-IeiTuTZExH5DQOqH.htm](feats/class-10-IeiTuTZExH5DQOqH.htm)|Burrowing Form|Forme fouisseuse|libre|
|[class-10-j5Xjr8vZuBhCixIr.htm](feats/class-10-j5Xjr8vZuBhCixIr.htm)|Side by Side (Druid)|Côte à côte (Druide)|officielle|
|[class-10-jBHDVsTVOBeoMoO4.htm](feats/class-10-jBHDVsTVOBeoMoO4.htm)|Greater Magical Edification|Édification magique supérieure|libre|
|[class-10-Jnhkl2BOhxxRCTpp.htm](feats/class-10-Jnhkl2BOhxxRCTpp.htm)|Vigil's Walls Rise Anew|Les murs de Vigie se dressent de nouveau|libre|
|[class-10-JQs2O2TTgKWXgJgZ.htm](feats/class-10-JQs2O2TTgKWXgJgZ.htm)|Consecrate Spell|Sort consacré|libre|
|[class-10-Jv24QkykqdPB7brL.htm](feats/class-10-Jv24QkykqdPB7brL.htm)|Unbreakable Bond|Lien indéfectible|libre|
|[class-10-jvQoupE76OeUpjZp.htm](feats/class-10-jvQoupE76OeUpjZp.htm)|Scout's Pounce|Bond de l'éclaireur|libre|
|[class-10-JwosaDYoqfPiFMYa.htm](feats/class-10-JwosaDYoqfPiFMYa.htm)|Eidetic Memorization|Mémorisation éidétique|libre|
|[class-10-JxZobgFhLBNcFCwE.htm](feats/class-10-JxZobgFhLBNcFCwE.htm)|Dream Guise|Déguisement chimérique|libre|
|[class-10-K0gyvvX0S2FdJZ5T.htm](feats/class-10-K0gyvvX0S2FdJZ5T.htm)|Weighty Impact|Impact pesant|libre|
|[class-10-K0x90HNHi5hONdDl.htm](feats/class-10-K0x90HNHi5hONdDl.htm)|Beast Dynamo Howl|Dynamo de hurlement bestial|libre|
|[class-10-KcNXoSvULnuQjC9a.htm](feats/class-10-KcNXoSvULnuQjC9a.htm)|Incredible Familiar (Familiar Master)|Formidable familier (Maître familier)|libre|
|[class-10-Km6YO7Ky2bEwhAFD.htm](feats/class-10-Km6YO7Ky2bEwhAFD.htm)|Greater Awakened Power|Pouvoir éveillé supérieur|libre|
|[class-10-KNhmeUpZCSW5eHqg.htm](feats/class-10-KNhmeUpZCSW5eHqg.htm)|Scour The Library|Parcourir la bibliothèque|libre|
|[class-10-knJlRpltciLNTZba.htm](feats/class-10-knJlRpltciLNTZba.htm)|Protective Bond|Lien protecteur|libre|
|[class-10-KNtnb9HwPnPDY2Mv.htm](feats/class-10-KNtnb9HwPnPDY2Mv.htm)|Wide Overwatch|Observateur large|libre|
|[class-10-kQEIPYoKTt69yXxV.htm](feats/class-10-kQEIPYoKTt69yXxV.htm)|Mirror Shield|Bouclier miroir|officielle|
|[class-10-KR78kinMmAZQHeoa.htm](feats/class-10-KR78kinMmAZQHeoa.htm)|Heroic Recovery|Récupération héroïque|officielle|
|[class-10-kUv9eiP8Zhck70WZ.htm](feats/class-10-kUv9eiP8Zhck70WZ.htm)|Eldritch Debilitations|Handicaps mystiques|libre|
|[class-10-L7hs5XOCbJmh0H0e.htm](feats/class-10-L7hs5XOCbJmh0H0e.htm)|Expand Spiral|Spirale étendue|libre|
|[class-10-lG4dYrnkE42IgnGG.htm](feats/class-10-lG4dYrnkE42IgnGG.htm)|Energy Ward|Protection énergétique|libre|
|[class-10-lIr2kC561L7oX290.htm](feats/class-10-lIr2kC561L7oX290.htm)|Rebounding Assault|Assaut rebondissant|libre|
|[class-10-lk80TBn933RECRD7.htm](feats/class-10-lk80TBn933RECRD7.htm)|Dazzling Display|Démonstration éblouissante|libre|
|[class-10-lKeJruYQutWlNXyZ.htm](feats/class-10-lKeJruYQutWlNXyZ.htm)|Martial Exercise|Exercice martial|libre|
|[class-10-lszcn7eO3olp5vEt.htm](feats/class-10-lszcn7eO3olp5vEt.htm)|Grave Sight|Vision de la tombe|libre|
|[class-10-lTwi4lyVk1UXomhK.htm](feats/class-10-lTwi4lyVk1UXomhK.htm)|Sustaining Steel|Acier durable|libre|
|[class-10-LvmYfUGX3uDCpIHY.htm](feats/class-10-LvmYfUGX3uDCpIHY.htm)|Electrify Armor|Armure électrifiée|libre|
|[class-10-McnLGEZnUbtYCNDW.htm](feats/class-10-McnLGEZnUbtYCNDW.htm)|Lock On|Verrouillage|libre|
|[class-10-MgLUbsvAkIA4fsZW.htm](feats/class-10-MgLUbsvAkIA4fsZW.htm)|Gaze of Veracity|Regard de véracité|libre|
|[class-10-MgqRwyL8PWyYvoZs.htm](feats/class-10-MgqRwyL8PWyYvoZs.htm)|Energy Fusion|Fusion d'énergies|libre|
|[class-10-MLiEMjyZXE43wmrq.htm](feats/class-10-MLiEMjyZXE43wmrq.htm)|Instant Armor|Armure instantanée|libre|
|[class-10-mnhsG4l53YJkJIeY.htm](feats/class-10-mnhsG4l53YJkJIeY.htm)|Widen the Gap|Élargir la faille|libre|
|[class-10-muDbZAyrE1ObyuTL.htm](feats/class-10-muDbZAyrE1ObyuTL.htm)|Beneath Notice|Sans intérêt|libre|
|[class-10-muyEI60L0FmCHuWb.htm](feats/class-10-muyEI60L0FmCHuWb.htm)|Ensnaring Wrappings|Bandelettes piégeuses|libre|
|[class-10-Mvw3ZFrdwMHedAxY.htm](feats/class-10-Mvw3ZFrdwMHedAxY.htm)|Merciful Elixir|Élixir miséricordieux|officielle|
|[class-10-n0693gmx3k9wvb1N.htm](feats/class-10-n0693gmx3k9wvb1N.htm)|Demanding Challenge|Défi exigeant|libre|
|[class-10-N0gJ4Q69nslbdXHg.htm](feats/class-10-N0gJ4Q69nslbdXHg.htm)|Command Attention|Attirer l'attention|libre|
|[class-10-N16lctDPZpvk9Khq.htm](feats/class-10-N16lctDPZpvk9Khq.htm)|Shadow Sneak Attack|Attaque sournoise de l'ombre|libre|
|[class-10-nDNmqez9McmrjBAV.htm](feats/class-10-nDNmqez9McmrjBAV.htm)|Attunement to Stone|Harmonisation avec la pierre|officielle|
|[class-10-NLjxAO9Mhx3k7gpc.htm](feats/class-10-NLjxAO9Mhx3k7gpc.htm)|Winter's Embrace|Étreinte de l'hiver|libre|
|[class-10-nobsCgNmsDX6aKR5.htm](feats/class-10-nobsCgNmsDX6aKR5.htm)|Rain-Scribe Mobility|Mobilité du Scribe de la pluie|libre|
|[class-10-NPhH9XkHV0DG4WS9.htm](feats/class-10-NPhH9XkHV0DG4WS9.htm)|Transpose|Transposition|libre|
|[class-10-nWxNV9pFeBHV671W.htm](feats/class-10-nWxNV9pFeBHV671W.htm)|Pristine Weapon|Arme immaculée|libre|
|[class-10-nx7UPAwAyno2rM9f.htm](feats/class-10-nx7UPAwAyno2rM9f.htm)|Thaumaturge's Investiture|Investiture du thaumaturge|libre|
|[class-10-nZK5oGakaSZcSOs1.htm](feats/class-10-nZK5oGakaSZcSOs1.htm)|Unbelievable Escape|Évitement incroyable|libre|
|[class-10-O1C6pMTOxIrWBO3G.htm](feats/class-10-O1C6pMTOxIrWBO3G.htm)|Daring Flourish|Sophistication culottée|libre|
|[class-10-o8ogNJ53l1JDIJud.htm](feats/class-10-o8ogNJ53l1JDIJud.htm)|Font of Knowledge|Puits de connaissances|libre|
|[class-10-o8qiAUuDHfurmgpP.htm](feats/class-10-o8qiAUuDHfurmgpP.htm)|Dimensional Disappearance|Disparition dimensionnelle|libre|
|[class-10-Ob4w36tsd1WKSxZo.htm](feats/class-10-Ob4w36tsd1WKSxZo.htm)|Eerie Proclamation|Proclamation sinistre|libre|
|[class-10-OCvgmAFz4qgj2Scf.htm](feats/class-10-OCvgmAFz4qgj2Scf.htm)|Stalwart Standard|Étendard fidèle|libre|
|[class-10-oD4JyvTJj4kwe5vb.htm](feats/class-10-oD4JyvTJj4kwe5vb.htm)|Warden's Step|Foulée du protecteur|officielle|
|[class-10-OEGhbRgW6wRbccns.htm](feats/class-10-OEGhbRgW6wRbccns.htm)|Disruptive Stance|Posture perturbatrice|officielle|
|[class-10-oiauCibmdgJ91kNI.htm](feats/class-10-oiauCibmdgJ91kNI.htm)|Cascading Ray|Rayon en cascade|libre|
|[class-10-oltE3dIYZlM8s5BI.htm](feats/class-10-oltE3dIYZlM8s5BI.htm)|Blazing Streak|Course enflammée|libre|
|[class-10-OmjTt8eR1Q3SmkPp.htm](feats/class-10-OmjTt8eR1Q3SmkPp.htm)|Armored Rest|Repos en armure|libre|
|[class-10-orTWiRwIQEc9FGJQ.htm](feats/class-10-orTWiRwIQEc9FGJQ.htm)|Fading|Effacement|libre|
|[class-10-otBBb0ndASgPdAXW.htm](feats/class-10-otBBb0ndASgPdAXW.htm)|Penetrating Shot|Tir pénétrant|officielle|
|[class-10-p2I4o9Cc6UrXvjhO.htm](feats/class-10-p2I4o9Cc6UrXvjhO.htm)|Furious Sprint|Course furieuse|officielle|
|[class-10-PfnlKlLuqKOOhLyK.htm](feats/class-10-PfnlKlLuqKOOhLyK.htm)|Big Debut|Grande première|libre|
|[class-10-phFRWFgeHdBzio2V.htm](feats/class-10-phFRWFgeHdBzio2V.htm)|Terrain Form|Forme de terrain|libre|
|[class-10-pI5cG6x49ZQLchuy.htm](feats/class-10-pI5cG6x49ZQLchuy.htm)|Goblin Jubilee Display|Démonstration du jubilé gobelin|libre|
|[class-10-PIVC14saumGNKWbo.htm](feats/class-10-PIVC14saumGNKWbo.htm)|Annotate Composition|Composition annotée|libre|
|[class-10-PlRAfeNof3xgJttZ.htm](feats/class-10-PlRAfeNof3xgJttZ.htm)|Surrounding Flames|Flammes environnantes|libre|
|[class-10-PSqxbLzKKzbMUnU9.htm](feats/class-10-PSqxbLzKKzbMUnU9.htm)|Entwined Energy Ki|Énergie ki entrelacée|libre|
|[class-10-pT6r5BVJjALJUmb3.htm](feats/class-10-pT6r5BVJjALJUmb3.htm)|Tag Team|Marquage d'équipe|libre|
|[class-10-pVLdMOqYwul745k3.htm](feats/class-10-pVLdMOqYwul745k3.htm)|Knockback|Repousser|officielle|
|[class-10-q8c0LINVNcJrdK91.htm](feats/class-10-q8c0LINVNcJrdK91.htm)|Propulsive Leap|Bond propulsif (Déviant)|libre|
|[class-10-qbFRPTHP96Q9kGpk.htm](feats/class-10-qbFRPTHP96Q9kGpk.htm)|Flash Your Badge|Montrer votre insigne|libre|
|[class-10-QHh442n2CEtJr23B.htm](feats/class-10-QHh442n2CEtJr23B.htm)|Greater Deathly Secrets|Secrets mortels supérieurs|libre|
|[class-10-QhKLXGJPLZaX1qDp.htm](feats/class-10-QhKLXGJPLZaX1qDp.htm)|Expert Captivator Spellcasting|Incantation experte d'enjôleur|libre|
|[class-10-Qjn9ErWY6wuIK9z6.htm](feats/class-10-Qjn9ErWY6wuIK9z6.htm)|Ward Casting|Incantation protégée|libre|
|[class-10-qKpaZF0U9VV0YwTJ.htm](feats/class-10-qKpaZF0U9VV0YwTJ.htm)|Litany Of Self-Interest|Litanie d'égoïsme|libre|
|[class-10-qo6oKL8mE32hSjSC.htm](feats/class-10-qo6oKL8mE32hSjSC.htm)|Slayer's Presence|Présence du tueur|libre|
|[class-10-QO8l5Dao8HnaFQE4.htm](feats/class-10-QO8l5Dao8HnaFQE4.htm)|Impressive Landing|Atterrissage impressionnant|libre|
|[class-10-qqe5VmcO1s8iTOfd.htm](feats/class-10-qqe5VmcO1s8iTOfd.htm)|Camouflage|Camouflage|officielle|
|[class-10-qUSWPWxYF8gfhfHM.htm](feats/class-10-qUSWPWxYF8gfhfHM.htm)|Helpful Tinkering|Ajustements utiles|libre|
|[class-10-R40U8hF0hWyRUze8.htm](feats/class-10-R40U8hF0hWyRUze8.htm)|Through Spell|Sort traversant|libre|
|[class-10-R7EaZPYtsy5H8lwn.htm](feats/class-10-R7EaZPYtsy5H8lwn.htm)|Merciless Rend|Déchirure sans pitié|libre|
|[class-10-REgTwIVgI3j1FQiJ.htm](feats/class-10-REgTwIVgI3j1FQiJ.htm)|Harden Flesh|Peau durcie|libre|
|[class-10-RfOnsZrmT6z2ajBN.htm](feats/class-10-RfOnsZrmT6z2ajBN.htm)|Siphon Life|Siphonner la vie|libre|
|[class-10-RLWbx2itF1jmW0OL.htm](feats/class-10-RLWbx2itF1jmW0OL.htm)|Shadow Reservoir|Réservoir d'ombre|libre|
|[class-10-RtYOsmb9R71J9ce2.htm](feats/class-10-RtYOsmb9R71J9ce2.htm)|Hilt Hammer|Pommeau marteau|libre|
|[class-10-RWccAJJ2PaOI0Byp.htm](feats/class-10-RWccAJJ2PaOI0Byp.htm)|Litany Against Sloth|Litanie contre la paresse|officielle|
|[class-10-rzaoi5Roef9zO22G.htm](feats/class-10-rzaoi5Roef9zO22G.htm)|Combat Reflexes|Attaque réflexe|officielle|
|[class-10-s0P4JXagA3085wLW.htm](feats/class-10-s0P4JXagA3085wLW.htm)|Toppling Tentacles|Tentacules renversants|libre|
|[class-10-S36WQ8o2MvPxQp0p.htm](feats/class-10-S36WQ8o2MvPxQp0p.htm)|Menacing Prowess|Prouesse menaçante|libre|
|[class-10-Sc9clbAXe97vlzxM.htm](feats/class-10-Sc9clbAXe97vlzxM.htm)|Suspect of Opportunity|Suspect opportun|libre|
|[class-10-SGBLDcT4wI5VUDCZ.htm](feats/class-10-SGBLDcT4wI5VUDCZ.htm)|Emissary of Peace|Émissaire de paix|libre|
|[class-10-SgXvw6rzk2lhTpXL.htm](feats/class-10-SgXvw6rzk2lhTpXL.htm)|Giant Snare|Piège artisanal géant|libre|
|[class-10-SlMkuKMny7hWdNxL.htm](feats/class-10-SlMkuKMny7hWdNxL.htm)|Derring-do|Bravoure|libre|
|[class-10-sTUicpQkhiFVtMK1.htm](feats/class-10-sTUicpQkhiFVtMK1.htm)|Tempest-Sun Shielding|Protection du soleil-tempétueux|libre|
|[class-10-stwRJTOKUru9AmQC.htm](feats/class-10-stwRJTOKUru9AmQC.htm)|Meteoric Spellstrike|Frappe de sort météorique|libre|
|[class-10-SVfD887ocfUFikVA.htm](feats/class-10-SVfD887ocfUFikVA.htm)|Starlit Spells|Sorts étoilés|libre|
|[class-10-SY6bU7DOyfs22cJX.htm](feats/class-10-SY6bU7DOyfs22cJX.htm)|Temporary Potions|Potions temporaires|libre|
|[class-10-SYVM6Z3sS50e5Vbd.htm](feats/class-10-SYVM6Z3sS50e5Vbd.htm)|Deny Support|Dénier le soutien|libre|
|[class-10-t6sey3cyV8n7a78l.htm](feats/class-10-t6sey3cyV8n7a78l.htm)|Trampling Charge|Charge piétinante|libre|
|[class-10-tCuMXQ0yMrCNwzqW.htm](feats/class-10-tCuMXQ0yMrCNwzqW.htm)|Resilient Touch|Toucher de résilience|libre|
|[class-10-tDWc2LQNl0Op1Auq.htm](feats/class-10-tDWc2LQNl0Op1Auq.htm)|Buckler Dance|Danse de la targe|libre|
|[class-10-tha0L3Z6608JrUwN.htm](feats/class-10-tha0L3Z6608JrUwN.htm)|Shadow Magic|Magie de l'ombre|libre|
|[class-10-tMsAj0H0B9XZQjtH.htm](feats/class-10-tMsAj0H0B9XZQjtH.htm)|Accursed Clay Fist|Poing de glaise maudit|libre|
|[class-10-tonJdHGheKZ16tMI.htm](feats/class-10-tonJdHGheKZ16tMI.htm)|Unusual Composition|Composition insolite|officielle|
|[class-10-ToZw6ZjB0JhWwMeR.htm](feats/class-10-ToZw6ZjB0JhWwMeR.htm)|Deflecting Shot|Tir détourné|libre|
|[class-10-ts2A1XeuPRzaCZgA.htm](feats/class-10-ts2A1XeuPRzaCZgA.htm)|Roadkill|Chauffard|libre|
|[class-10-tSmd0cxq9wokSCh4.htm](feats/class-10-tSmd0cxq9wokSCh4.htm)|Castigating Weapon|Arme Punitive|officielle|
|[class-10-TW0fUdqqB69rIbRx.htm](feats/class-10-TW0fUdqqB69rIbRx.htm)|Lead the Way|Ouvrir la voie|libre|
|[class-10-tXNfWDa6P7bCKrCt.htm](feats/class-10-tXNfWDa6P7bCKrCt.htm)|Overwhelming Energy|Énergie écrasante|officielle|
|[class-10-tY3Zg0l14CdoKPpt.htm](feats/class-10-tY3Zg0l14CdoKPpt.htm)|Unstable Concoction|Concoction instable|libre|
|[class-10-TyjW9VGtlH0Zkm0I.htm](feats/class-10-TyjW9VGtlH0Zkm0I.htm)|Signifer's Sight|Vision du signifer|libre|
|[class-10-u2fgdFIdQDplKOS3.htm](feats/class-10-u2fgdFIdQDplKOS3.htm)|Peafowl Strut|Pavane du paon|libre|
|[class-10-U52NMdeSNbjSSRHE.htm](feats/class-10-U52NMdeSNbjSSRHE.htm)|Incredible Companion (Ranger)|Formidable compagnon (Rôdeur)|officielle|
|[class-10-u5DBg0LrBUKP0JsJ.htm](feats/class-10-u5DBg0LrBUKP0JsJ.htm)|Scroll Savant|Virtuose des parchemins|officielle|
|[class-10-U9SrWgLJ8JoAKIy0.htm](feats/class-10-U9SrWgLJ8JoAKIy0.htm)|Rockslide Spell|Sort à éboulement|libre|
|[class-10-UCFGUbmDnzRGmN9I.htm](feats/class-10-UCFGUbmDnzRGmN9I.htm)|Steal Time|Voler du temps|libre|
|[class-10-UEqntGzFrFA7ncUO.htm](feats/class-10-UEqntGzFrFA7ncUO.htm)|Aerial Piledriver|Pilonneur aérien|libre|
|[class-10-UJcQgQpZRqXBw0Nb.htm](feats/class-10-UJcQgQpZRqXBw0Nb.htm)|Repurposed Parts|Parties réutilisées|libre|
|[class-10-UjYHf7rlWTFJ0v0A.htm](feats/class-10-UjYHf7rlWTFJ0v0A.htm)|Signature Spell Expansion|Expansion des sorts emblématiques|libre|
|[class-10-uotQ9yqetPoAWrfW.htm](feats/class-10-uotQ9yqetPoAWrfW.htm)|Reflexive Riposte|Riposte machinale|libre|
|[class-10-v482u7QboZEbhgvv.htm](feats/class-10-v482u7QboZEbhgvv.htm)|Assured Ritualist|Ritualiste assuré|libre|
|[class-10-V5FKAmcYQF4KRELG.htm](feats/class-10-V5FKAmcYQF4KRELG.htm)|Cascade Bearer's Spellcasting|Incantation du Tisseur des flots|libre|
|[class-10-V5Y9r31BCTaCiNzi.htm](feats/class-10-V5Y9r31BCTaCiNzi.htm)|Lastwall Warden|Gardien du Dernier-Rempart|officielle|
|[class-10-V7n3kVSl0G6a29KG.htm](feats/class-10-V7n3kVSl0G6a29KG.htm)|Flailtongue|Langue fléau|libre|
|[class-10-VC8qdcCxtzCmG98M.htm](feats/class-10-VC8qdcCxtzCmG98M.htm)|Impose Order (Psychic)|Ordre imposé (psychiste)|libre|
|[class-10-VF5XFpzBlqUFd8Mw.htm](feats/class-10-VF5XFpzBlqUFd8Mw.htm)|Glass Skin|Peau de verre|libre|
|[class-10-vgsMKjAbRDNxT5TK.htm](feats/class-10-vgsMKjAbRDNxT5TK.htm)|Determined Dash|Élan déterminé|libre|
|[class-10-VJ3wIsfmabK02SNg.htm](feats/class-10-VJ3wIsfmabK02SNg.htm)|Provocator Dedication|Dévouement : Provocator|libre|
|[class-10-VJl2xHDKr0HxTUrs.htm](feats/class-10-VJl2xHDKr0HxTUrs.htm)|Symphony Of The Unfettered Heart|Symphonie du coeur affranchi|libre|
|[class-10-VsGdiAuVbYmDTR82.htm](feats/class-10-VsGdiAuVbYmDTR82.htm)|Black Powder Flash|Flash de poudre noire|libre|
|[class-10-VsTmB32x9673ONJ0.htm](feats/class-10-VsTmB32x9673ONJ0.htm)|Shield of Reckoning|Bouclier du jugement|officielle|
|[class-10-vu0R9VfRZ6RWTczZ.htm](feats/class-10-vu0R9VfRZ6RWTczZ.htm)|Practiced Defender|Défenseur expérimenté|libre|
|[class-10-vxDL78Yz0dPZdAJ5.htm](feats/class-10-vxDL78Yz0dPZdAJ5.htm)|Precious Munitions|Munitions précieuses|libre|
|[class-10-vXH0HWMHzevA1Wox.htm](feats/class-10-vXH0HWMHzevA1Wox.htm)|Winding Flow|Fluidité détournée|officielle|
|[class-10-w3qyriA1YnzXaas3.htm](feats/class-10-w3qyriA1YnzXaas3.htm)|Shield Of Faith|Bouclier de la foi|libre|
|[class-10-W7aT1UJOVFkYdQti.htm](feats/class-10-W7aT1UJOVFkYdQti.htm)|Hunter's Vision|Vision du chasseur|libre|
|[class-10-waZzVgfyr1Vcxfqk.htm](feats/class-10-waZzVgfyr1Vcxfqk.htm)|Staggering Blow|Coup stupéfiant|libre|
|[class-10-wIJC00ODLq9WYc1m.htm](feats/class-10-wIJC00ODLq9WYc1m.htm)|Shared Avoidance|Évitement partagé|libre|
|[class-10-WlgaSpTSGQQrHKlx.htm](feats/class-10-WlgaSpTSGQQrHKlx.htm)|Mockingbird's Disarm|Désarmement du Geai moqueur|libre|
|[class-10-Wo0HNBh6CCgPN3Co.htm](feats/class-10-Wo0HNBh6CCgPN3Co.htm)|Tiller's Drive|Conduite du Laboureur|libre|
|[class-10-WSndnu3scUYWmbki.htm](feats/class-10-WSndnu3scUYWmbki.htm)|Hook 'Em|Attrapez les|libre|
|[class-10-wUHnauB3atxO1RIo.htm](feats/class-10-wUHnauB3atxO1RIo.htm)|Rubbery Skin|Peau caoutchouteuse|libre|
|[class-10-wuMa6iJyZ83LYJXH.htm](feats/class-10-wuMa6iJyZ83LYJXH.htm)|Into the Future|Dans le futur|libre|
|[class-10-wvJBIzgS298ZRp6w.htm](feats/class-10-wvJBIzgS298ZRp6w.htm)|Precise Debilitations|Handicaps précis|officielle|
|[class-10-wxIOuPFjVBADNYDP.htm](feats/class-10-wxIOuPFjVBADNYDP.htm)|Torrential Backlash|Contrecoup torrentiel|libre|
|[class-10-wZZyasfIqwiJBQAQ.htm](feats/class-10-wZZyasfIqwiJBQAQ.htm)|Whirlwind Stance|Posture tourbillonnante|libre|
|[class-10-xCuPob9TuSBKz6Gn.htm](feats/class-10-xCuPob9TuSBKz6Gn.htm)|Perfect Ki Expert|Expert de la perfection du ki|libre|
|[class-10-Xdmz09kzLjIxWz9C.htm](feats/class-10-Xdmz09kzLjIxWz9C.htm)|Tactical Debilitations|Handicaps tactiques|officielle|
|[class-10-XeFMWjJkZy2O7lou.htm](feats/class-10-XeFMWjJkZy2O7lou.htm)|Cloud Walk|Marche sur les nuages|libre|
|[class-10-xmf6oUYarFJGajtr.htm](feats/class-10-xmf6oUYarFJGajtr.htm)|Holy Light|Lumière sainte|libre|
|[class-10-XPTSCUoA8c4o9RgQ.htm](feats/class-10-XPTSCUoA8c4o9RgQ.htm)|Distracting Explosion|Distraction explosive|libre|
|[class-10-xQuNswWB3eg1UM28.htm](feats/class-10-xQuNswWB3eg1UM28.htm)|Cobra Envenom|Venin du cobra|libre|
|[class-10-Xw7qG0SHepXx24vl.htm](feats/class-10-Xw7qG0SHepXx24vl.htm)|Prevailing Position|Position prédominante|libre|
|[class-10-Yazy4gex46FLwsph.htm](feats/class-10-Yazy4gex46FLwsph.htm)|Tumbling Opportunist|Opportuniste acrobatique|libre|
|[class-10-YdGHQjhUrNvP18AA.htm](feats/class-10-YdGHQjhUrNvP18AA.htm)|Spellmaster's Ward|Protection du maître des sorts|libre|
|[class-10-YFPmD8BHv0XaF55G.htm](feats/class-10-YFPmD8BHv0XaF55G.htm)|Distant Wandering|Errance à distance (Déviant)|libre|
|[class-10-yg02rHaDPpGSgkrk.htm](feats/class-10-yg02rHaDPpGSgkrk.htm)|Gathering Moss|Recouvert de mousse|libre|
|[class-10-Yk3QGpalWDn5MhBV.htm](feats/class-10-Yk3QGpalWDn5MhBV.htm)|Silencing Strike|Frappe muselante|libre|
|[class-10-YlaLePtgDXGB0Fsf.htm](feats/class-10-YlaLePtgDXGB0Fsf.htm)|Penetrating Fire|Feu pénétrant|libre|
|[class-10-YluQPhevo0LKdF1p.htm](feats/class-10-YluQPhevo0LKdF1p.htm)|Just One More Thing|Encore une chose|libre|
|[class-10-YNk0BekymS3bBvCT.htm](feats/class-10-YNk0BekymS3bBvCT.htm)|Ancestral Mage|Mage ancestral|libre|
|[class-10-ySr510TkpUO3N6Ty.htm](feats/class-10-ySr510TkpUO3N6Ty.htm)|Trick Shot|Tir astucieux|libre|
|[class-10-yTh9QwAf0hadP91j.htm](feats/class-10-yTh9QwAf0hadP91j.htm)|Improved Knockdown|Renversement amélioré|libre|
|[class-10-YV4X9u5Yuf0xvoCh.htm](feats/class-10-YV4X9u5Yuf0xvoCh.htm)|Surging Might|Puissance déferlante|libre|
|[class-10-z2ptq23nNBOeEI7H.htm](feats/class-10-z2ptq23nNBOeEI7H.htm)|Quickened Casting|Incantation accélérée|officielle|
|[class-10-Z9gzWFk3qom2z904.htm](feats/class-10-Z9gzWFk3qom2z904.htm)|Come and Get Me|Je t'attends|officielle|
|[class-10-ZFkCMl63ogK55Otq.htm](feats/class-10-ZFkCMl63ogK55Otq.htm)|Major Lesson|Leçon majeure|libre|
|[class-10-zKL1lRcIbFblp2M2.htm](feats/class-10-zKL1lRcIbFblp2M2.htm)|Potent Poisoner|Empoisonneur puissant|libre|
|[class-10-zt5CWn3UrPViwaB3.htm](feats/class-10-zt5CWn3UrPViwaB3.htm)|Vicious Debilitations|Handicaps cruels|officielle|
|[class-12-07YYaQBknzDTcbFz.htm](feats/class-12-07YYaQBknzDTcbFz.htm)|Directed Poison|Poison dirigé|libre|
|[class-12-0Qr7Y1IOxny0vIQf.htm](feats/class-12-0Qr7Y1IOxny0vIQf.htm)|Tunneling Claws|Griffes de tunnelier|libre|
|[class-12-0RB3f3J7gOEv3fni.htm](feats/class-12-0RB3f3J7gOEv3fni.htm)|Expert Bard Spellcasting|Incantation experte du barde|officielle|
|[class-12-178rqdgJBF9Rl0Gi.htm](feats/class-12-178rqdgJBF9Rl0Gi.htm)|Master of the Dead|Maître de la mort|libre|
|[class-12-1JTEWgonWlmeCE3w.htm](feats/class-12-1JTEWgonWlmeCE3w.htm)|Liberating Stride|Foulée libératrice|officielle|
|[class-12-1k5PZAYth8u4Fqyr.htm](feats/class-12-1k5PZAYth8u4Fqyr.htm)|Expert Sorcerer Spellcasting|Incantation experte de l'ensorceleur|officielle|
|[class-12-1ZbLYY5m2YALrrgA.htm](feats/class-12-1ZbLYY5m2YALrrgA.htm)|Perfection's Path (Fortitude)|Voie de la perfection (Vigueur)|officielle|
|[class-12-2aFtxqRPnC4OXUGC.htm](feats/class-12-2aFtxqRPnC4OXUGC.htm)|Warden's Focus|Focalisation du gardien|libre|
|[class-12-2UogDoEJ2M2tQFxn.htm](feats/class-12-2UogDoEJ2M2tQFxn.htm)|Expert Spellcasting|Incantation experte|libre|
|[class-12-3WUL8ExEkZDRYeBu.htm](feats/class-12-3WUL8ExEkZDRYeBu.htm)|Diviner Sense|Sens du devin|libre|
|[class-12-4bB3N36DmqllGJNx.htm](feats/class-12-4bB3N36DmqllGJNx.htm)|Shield Salvation|Sauvetage du bouclier|libre|
|[class-12-4l0ewDg4gMfkU2pi.htm](feats/class-12-4l0ewDg4gMfkU2pi.htm)|Link Focus|Focalisation double|libre|
|[class-12-4st3fSYSrJrxwHOP.htm](feats/class-12-4st3fSYSrJrxwHOP.htm)|Contingency Gadgets|Gadgets de secours|libre|
|[class-12-56BMXlQlZtg39SMV.htm](feats/class-12-56BMXlQlZtg39SMV.htm)|Lasting Doubt|Doute Tenace|officielle|
|[class-12-5kua1Kf5Ca85lbzb.htm](feats/class-12-5kua1Kf5Ca85lbzb.htm)|Space-Time Shift|Déphasage spatio-temporel|libre|
|[class-12-5kUQuIP8N57MXhuz.htm](feats/class-12-5kUQuIP8N57MXhuz.htm)|Beastmaster's Call|Appel du Maître des bêtes|libre|
|[class-12-5YcnoTYKvEtkWiHh.htm](feats/class-12-5YcnoTYKvEtkWiHh.htm)|Side by Side (Ranger)|Côte à côte (Rôdeur)|officielle|
|[class-12-5ZX4btrw5yjBr8IM.htm](feats/class-12-5ZX4btrw5yjBr8IM.htm)|Bullet Dancer Reload|Rechargement du danseur de balle|libre|
|[class-12-6DkylvU5RF1O6DTT.htm](feats/class-12-6DkylvU5RF1O6DTT.htm)|Daredevil's Gambit|Manoeuvre téméraire|libre|
|[class-12-7aHYM9fawA3PQwtM.htm](feats/class-12-7aHYM9fawA3PQwtM.htm)|Self Destruct|Auto destruction|libre|
|[class-12-7HPXQvPH3ovwtVae.htm](feats/class-12-7HPXQvPH3ovwtVae.htm)|Furious Grab|Empoignade furieuse|officielle|
|[class-12-7IrZIPP5ePtzzKcI.htm](feats/class-12-7IrZIPP5ePtzzKcI.htm)|Oneiric Influence|Influence onirique|libre|
|[class-12-7r79fZBr37qBN2EF.htm](feats/class-12-7r79fZBr37qBN2EF.htm)|Shattering Shot|Tir fracassant|libre|
|[class-12-7sFhBYoz5GSBFNbY.htm](feats/class-12-7sFhBYoz5GSBFNbY.htm)|Aura of Faith|Aura de foi|officielle|
|[class-12-7spk6rZPiNk2S0yA.htm](feats/class-12-7spk6rZPiNk2S0yA.htm)|Blood in the Air|Du sang dans l'air|libre|
|[class-12-8cbSVw8RnVzy5USe.htm](feats/class-12-8cbSVw8RnVzy5USe.htm)|Enigma's Knowledge|Connaissance de l'énigme|libre|
|[class-12-8FxKcuFtOrqsl1FH.htm](feats/class-12-8FxKcuFtOrqsl1FH.htm)|Roll With it (Kingmaker)|Roule avec çà (Kingmaker)|libre|
|[class-12-8INrcMUv5vzWMG3X.htm](feats/class-12-8INrcMUv5vzWMG3X.htm)|Sunder Spell|Destruction de sort|libre|
|[class-12-8KUvJuAWCoxWg5FH.htm](feats/class-12-8KUvJuAWCoxWg5FH.htm)|Metallic Envisionment|Visualisation métallique|libre|
|[class-12-9Ht1eyBHsB1swpeE.htm](feats/class-12-9Ht1eyBHsB1swpeE.htm)|Uncanny Bombs|Bombes incroyables|libre|
|[class-12-9u0uW1vZThRayXk2.htm](feats/class-12-9u0uW1vZThRayXk2.htm)|Shaped Contaminant|Contaminant formé|libre|
|[class-12-a2Owk9WI4pjSPuHf.htm](feats/class-12-a2Owk9WI4pjSPuHf.htm)|Educated Assessment|Évaluation érudite|libre|
|[class-12-a2wXdQHiIoj3lHoe.htm](feats/class-12-a2wXdQHiIoj3lHoe.htm)|Communal Sustain|Maintien partagé|libre|
|[class-12-ADVrDjp75fr2BYMa.htm](feats/class-12-ADVrDjp75fr2BYMa.htm)|Charged Creation|Création chargée|libre|
|[class-12-aFUxNGur3Hma8DKy.htm](feats/class-12-aFUxNGur3Hma8DKy.htm)|Predator's Pounce|Bond de prédateur|officielle|
|[class-12-AmV13b9ncALtWJFt.htm](feats/class-12-AmV13b9ncALtWJFt.htm)|Redirect Elements|Rediriger les éléments|libre|
|[class-12-Ano4tRq88V39eyPq.htm](feats/class-12-Ano4tRq88V39eyPq.htm)|Frightening Appearance|Apparition effrayante|libre|
|[class-12-APfPNpUQlKlCAJkS.htm](feats/class-12-APfPNpUQlKlCAJkS.htm)|Miraculous Intervention|Intervention miraculeuse|libre|
|[class-12-arnSyCl9XfN1qviJ.htm](feats/class-12-arnSyCl9XfN1qviJ.htm)|Acknowledge Fan|Reconnu par un fan|libre|
|[class-12-asRbkgW59DZUpvAq.htm](feats/class-12-asRbkgW59DZUpvAq.htm)|Extend Elixir|Élixir étendu|officielle|
|[class-12-AtidbY3lU49taaUR.htm](feats/class-12-AtidbY3lU49taaUR.htm)|Rejuvenation|Reconstruction|libre|
|[class-12-AvN95M5eVLLEu2qk.htm](feats/class-12-AvN95M5eVLLEu2qk.htm)|Splendid Companion|Formidable compagnon|libre|
|[class-12-aWHOcGLA7AhX4xpm.htm](feats/class-12-aWHOcGLA7AhX4xpm.htm)|Interfering Surge|Afflux interférant|libre|
|[class-12-AXy4A7zTYk1JAiOV.htm](feats/class-12-AXy4A7zTYk1JAiOV.htm)|Magic Sense|Perception de la magie|libre|
|[class-12-AYXherMu9gFTyXjp.htm](feats/class-12-AYXherMu9gFTyXjp.htm)|Deadly Poison Weapon|Arme empoisonnée mortelle|libre|
|[class-12-bbpFExz1RXCkOhnb.htm](feats/class-12-bbpFExz1RXCkOhnb.htm)|Sense Ki|Perception du ki|libre|
|[class-12-BJfIGuUMItalNYet.htm](feats/class-12-BJfIGuUMItalNYet.htm)|Evasiveness (Swashbuckler)|Dérobade (Bretteur)|libre|
|[class-12-BlfgmJHjDyTVGdPs.htm](feats/class-12-BlfgmJHjDyTVGdPs.htm)|Wandering Oasis|Oasis itinérante|libre|
|[class-12-BQrhDpLIp9zjbjEP.htm](feats/class-12-BQrhDpLIp9zjbjEP.htm)|Distracting Shot|Tir déroutant|officielle|
|[class-12-bqZkAFS6eq9TKXMO.htm](feats/class-12-bqZkAFS6eq9TKXMO.htm)|Dragon's Rage Wings|Ailes de rage du dragon|officielle|
|[class-12-bTNgHDqzfoqOLWu3.htm](feats/class-12-bTNgHDqzfoqOLWu3.htm)|Speaking Sky|Ciel parlant|libre|
|[class-12-C4m59yjuDmZLnTqu.htm](feats/class-12-C4m59yjuDmZLnTqu.htm)|Shadow Illusion|Illusion de l'ombre|libre|
|[class-12-c7mCj6wUdb47rrnw.htm](feats/class-12-c7mCj6wUdb47rrnw.htm)|Ricochet Shot|Tir ricochet|libre|
|[class-12-COe0bYyVCyC78rzP.htm](feats/class-12-COe0bYyVCyC78rzP.htm)|Second Sting|Second dard|officielle|
|[class-12-cppQZwnuoXqX8mgF.htm](feats/class-12-cppQZwnuoXqX8mgF.htm)|Vernai Training|Entraînement Vernai|libre|
|[class-12-CQfxxsRxf1BuUH4o.htm](feats/class-12-CQfxxsRxf1BuUH4o.htm)|Janatimo's Lessons|Leçon secrète de Janatimo|libre|
|[class-12-cV2CZxD6HB7HFeFv.htm](feats/class-12-cV2CZxD6HB7HFeFv.htm)|Sing to the Steel|Chanter pour l'acier|libre|
|[class-12-Cy5W8U4yN9P1EvBy.htm](feats/class-12-Cy5W8U4yN9P1EvBy.htm)|Amplifying Touch|Toucher amplificateur|libre|
|[class-12-d1jQ0HyIOyUdCCaN.htm](feats/class-12-d1jQ0HyIOyUdCCaN.htm)|Spring Attack|Attaque éclair|officielle|
|[class-12-d1ktdX1Fk37dG5ms.htm](feats/class-12-d1ktdX1Fk37dG5ms.htm)|Defensive Recovery|Récupération défensive|officielle|
|[class-12-D2KSVHPRlBEibrV8.htm](feats/class-12-D2KSVHPRlBEibrV8.htm)|Cheat Death|Trompe la mort|libre|
|[class-12-D3ypq5kAmLbwrgjq.htm](feats/class-12-D3ypq5kAmLbwrgjq.htm)|No Stranger to Death|Pas étranger à la mort|libre|
|[class-12-d6Vb8D9yOX93mdUI.htm](feats/class-12-d6Vb8D9yOX93mdUI.htm)|Flinging Shove|Poussée catapulteuse|officielle|
|[class-12-d7DQhCJKYcLxpHen.htm](feats/class-12-d7DQhCJKYcLxpHen.htm)|Focused Shot|Tir concentré|libre|
|[class-12-dLof0i6LPcChJSBR.htm](feats/class-12-dLof0i6LPcChJSBR.htm)|Pale Horse|Cheval pâle|libre|
|[class-12-dWkf6LhYBfBkeyOA.htm](feats/class-12-dWkf6LhYBfBkeyOA.htm)|Spellmaster's Resilience|Résilience du maître des sorts|libre|
|[class-12-Dwxi1q1OWB1ufFvy.htm](feats/class-12-Dwxi1q1OWB1ufFvy.htm)|Overwhelming Breath|Souffle surpuissant|libre|
|[class-12-E7PFg8xua1nGIqHQ.htm](feats/class-12-E7PFg8xua1nGIqHQ.htm)|Competitive Eater|Mangeur compétitif|libre|
|[class-12-ecV3Nljvs4FOBS27.htm](feats/class-12-ecV3Nljvs4FOBS27.htm)|Reverberate|Entrer en résonance|libre|
|[class-12-Eg7YZBmeNJeY9wkD.htm](feats/class-12-Eg7YZBmeNJeY9wkD.htm)|Drive-By Attack|Attaque en conduisant|libre|
|[class-12-eGZzifQWCf5gSq2y.htm](feats/class-12-eGZzifQWCf5gSq2y.htm)|Unshakable Grit|Audace inébranlable|libre|
|[class-12-emjWa77ltL5FytvA.htm](feats/class-12-emjWa77ltL5FytvA.htm)|Spring from the Shadows|Bondir depuis les ombres|officielle|
|[class-12-ENoRkTXtdfsbs98S.htm](feats/class-12-ENoRkTXtdfsbs98S.htm)|Domain Fluency|Maîtrise du domaine|libre|
|[class-12-eoEYZJNdmvA5GfyK.htm](feats/class-12-eoEYZJNdmvA5GfyK.htm)|Expert Eldritch Archer Spellcasting|Incantation experte de l'archer mystique|libre|
|[class-12-eSoMkHGw38ld4gj2.htm](feats/class-12-eSoMkHGw38ld4gj2.htm)|Shared Warding|Protection partagée|libre|
|[class-12-etaixAdHNlHnLH0i.htm](feats/class-12-etaixAdHNlHnLH0i.htm)|Guarded Advance|Avance gardée|libre|
|[class-12-f6k9lIrIS4SfnCnG.htm](feats/class-12-f6k9lIrIS4SfnCnG.htm)|Master Alchemy|Alchimie maître|officielle|
|[class-12-FA1s2jjZqoBy58Xx.htm](feats/class-12-FA1s2jjZqoBy58Xx.htm)|Secrets of Shadow|Secrets de l'ombre|libre|
|[class-12-fCDC53WOOYrsyVIR.htm](feats/class-12-fCDC53WOOYrsyVIR.htm)|Incredible Ricochet|Ricochet extraordinaire|officielle|
|[class-12-fcFrxvqbIX6k71os.htm](feats/class-12-fcFrxvqbIX6k71os.htm)|Meditative Focus|Focalisation méditative|officielle|
|[class-12-fLlCodqKXyXbZR7C.htm](feats/class-12-fLlCodqKXyXbZR7C.htm)|Wave Spiral|Spirale de vague|libre|
|[class-12-FtO8DjjMLBtWiRhZ.htm](feats/class-12-FtO8DjjMLBtWiRhZ.htm)|Expert Oracle Spellcasting|Incantation experte de l'oracle|libre|
|[class-12-FYz5eQeTox9IDkSd.htm](feats/class-12-FYz5eQeTox9IDkSd.htm)|Dueling Dance (Fighter)|Danse en duel (Guerrier)|libre|
|[class-12-gHHnMCBi1gvG5wTL.htm](feats/class-12-gHHnMCBi1gvG5wTL.htm)|Student Of The Dueling Arts|Étudiant des arts du duel|libre|
|[class-12-gipC9eWrBDFRpNkQ.htm](feats/class-12-gipC9eWrBDFRpNkQ.htm)|Frostbite Runes|Runes de froid mordant|libre|
|[class-12-GMrJdGwajADbL1y5.htm](feats/class-12-GMrJdGwajADbL1y5.htm)|Diamond Soul|Âme de diamant|officielle|
|[class-12-gO729iC9b5ypes2K.htm](feats/class-12-gO729iC9b5ypes2K.htm)|Spirit's Wrath|Colère de l'esprit|officielle|
|[class-12-Gu8IAYxwNdQM603P.htm](feats/class-12-Gu8IAYxwNdQM603P.htm)|Read the Land|Lire le paysage|libre|
|[class-12-gVLICIDQMvWN5D89.htm](feats/class-12-gVLICIDQMvWN5D89.htm)|Greater Spiritual Evolution|Évolution spirituelle supérieure|libre|
|[class-12-HbBNuT1bqdcCU9hM.htm](feats/class-12-HbBNuT1bqdcCU9hM.htm)|Greater Sun Blessing|Bénédiction du soleil supérieure|libre|
|[class-12-HJBDFHIaJ3lfxcbs.htm](feats/class-12-HJBDFHIaJ3lfxcbs.htm)|Maneuvering Spell|Sort manoeuvrant|libre|
|[class-12-hPanopG3TbXKr52O.htm](feats/class-12-hPanopG3TbXKr52O.htm)|Pesh Skin|Peau de pesh|libre|
|[class-12-hpCBELEKGA4ynYv4.htm](feats/class-12-hpCBELEKGA4ynYv4.htm)|Expert Wizard Spellcasting|Incantation experte du magicien|officielle|
|[class-12-hPDerDCYmag3s0dP.htm](feats/class-12-hPDerDCYmag3s0dP.htm)|Paragon's Guard|Protection du parangon|officielle|
|[class-12-HPETR6zq8L6YJyi1.htm](feats/class-12-HPETR6zq8L6YJyi1.htm)|Improved Knockback|Repoussement amélioré|officielle|
|[class-12-HSW3N9pfHhM7upRB.htm](feats/class-12-HSW3N9pfHhM7upRB.htm)|Greater Revelation|Révélation supérieure|libre|
|[class-12-hT0pVPqFuiEsmRb8.htm](feats/class-12-hT0pVPqFuiEsmRb8.htm)|Six Pillars Stance|Posture des six piliers|libre|
|[class-12-HxTqYtXRqFkkLfDQ.htm](feats/class-12-HxTqYtXRqFkkLfDQ.htm)|Gigantic Megafauna Companion|Compagnon mégafaune gigantesque|libre|
|[class-12-hZueHQeYR5bgDh5W.htm](feats/class-12-hZueHQeYR5bgDh5W.htm)|Secret Eater|Mangeur de secret|libre|
|[class-12-I00uuseTfPypVgLQ.htm](feats/class-12-I00uuseTfPypVgLQ.htm)|Primal Summons|Convocations primordiales|officielle|
|[class-12-I6su6Z9sJTgIrhQV.htm](feats/class-12-I6su6Z9sJTgIrhQV.htm)|Spiritual Aid|Aide spirituelle|libre|
|[class-12-iFEecf9o6uhJxWcG.htm](feats/class-12-iFEecf9o6uhJxWcG.htm)|Hellknight Order Cross-Training|Entraînement croisé de l'Ordre de Chevalier infernal|libre|
|[class-12-Ij6BBPzZvOFZ3prs.htm](feats/class-12-Ij6BBPzZvOFZ3prs.htm)|Felling Shot|Tir renversant|officielle|
|[class-12-iJxbrXAdxhLqdT5E.htm](feats/class-12-iJxbrXAdxhLqdT5E.htm)|Assassinate|Assassinat|libre|
|[class-12-IMPP5pa8AmvCby4W.htm](feats/class-12-IMPP5pa8AmvCby4W.htm)|Clever Counterspell|Contresort astucieux|officielle|
|[class-12-INqBWbbDHF4DIg8i.htm](feats/class-12-INqBWbbDHF4DIg8i.htm)|Escape Timeline|Échapper au flot temporel|libre|
|[class-12-isdTXU8bV7ZVOAuQ.htm](feats/class-12-isdTXU8bV7ZVOAuQ.htm)|Flourishing Finish|Mise à mort spectaculaire|libre|
|[class-12-ixIKF2LMmbFthI8Z.htm](feats/class-12-ixIKF2LMmbFthI8Z.htm)|Towering Size|Taille imposante|libre|
|[class-12-IZFw3Do0kBdgwZX0.htm](feats/class-12-IZFw3Do0kBdgwZX0.htm)|Expert Cathartic Spellcasting|Incantation cathartique experte|libre|
|[class-12-j20djiiuVwUf8MqL.htm](feats/class-12-j20djiiuVwUf8MqL.htm)|Embrace The Pain|Épouser la douleur|libre|
|[class-12-J5s7NeFHYuFSdhrX.htm](feats/class-12-J5s7NeFHYuFSdhrX.htm)|Hell's Armaments|Arsenal infernal|libre|
|[class-12-JddrdXv4pfdePsGK.htm](feats/class-12-JddrdXv4pfdePsGK.htm)|Petrified Skin|Peau pétrifiée|libre|
|[class-12-JfLIWyqgEVggWaBL.htm](feats/class-12-JfLIWyqgEVggWaBL.htm)|Magic Sense (Magus)|Perception de la magie (magus)|libre|
|[class-12-JJPoMcxUf3QoKA6h.htm](feats/class-12-JJPoMcxUf3QoKA6h.htm)|Combat Premonition|Prémonition du combat|libre|
|[class-12-JmHHdGC1p53BBedu.htm](feats/class-12-JmHHdGC1p53BBedu.htm)|Expert Magus Spellcasting|Incantation experte du magus|libre|
|[class-12-jNeIaFUFSGUXoSON.htm](feats/class-12-jNeIaFUFSGUXoSON.htm)|Divine Wall|Mur divin|officielle|
|[class-12-jNzjecRGyyAqkkrm.htm](feats/class-12-jNzjecRGyyAqkkrm.htm)|Deep Freeze|Congélation profonde|libre|
|[class-12-jPBqvEH2jLlvDr6M.htm](feats/class-12-jPBqvEH2jLlvDr6M.htm)|Juggernaut's Fortitude|Vigueur du juggernaut|officielle|
|[class-12-jsbe2d9lYGJ2MksT.htm](feats/class-12-jsbe2d9lYGJ2MksT.htm)|Banshee Cry Display|Démonstration de pleur de la banshie|libre|
|[class-12-K5pXeeJLmdE8XuvM.htm](feats/class-12-K5pXeeJLmdE8XuvM.htm)|Tense Negotiator|Négociateur tendu|libre|
|[class-12-k9B7gDit3pXbq2XF.htm](feats/class-12-k9B7gDit3pXbq2XF.htm)|Finessed Features|Caractéristiques définies|libre|
|[class-12-kKoqqXOTdRYROmVV.htm](feats/class-12-kKoqqXOTdRYROmVV.htm)|Blessed Denial|Déni de l'Élu|libre|
|[class-12-KOslKkFO9N1kZY87.htm](feats/class-12-KOslKkFO9N1kZY87.htm)|Night's Shine|Éclat de la nuit|libre|
|[class-12-KvKg9pBOpk2oLeO1.htm](feats/class-12-KvKg9pBOpk2oLeO1.htm)|Advanced Order Training|Entraînement de l'Ordre avancé|libre|
|[class-12-KYF9e4oeSjHKgbwM.htm](feats/class-12-KYF9e4oeSjHKgbwM.htm)|Coordinated Charge|Charge coordonnée|libre|
|[class-12-LI9VtCaL5ZRk0Wo8.htm](feats/class-12-LI9VtCaL5ZRk0Wo8.htm)|Stance Savant (Monk)|Maître des postures (Moine)|officielle|
|[class-12-lIpfj1JeFFS7Zn6D.htm](feats/class-12-lIpfj1JeFFS7Zn6D.htm)|Vigilant Benediction|Bénédiction du vigilant|libre|
|[class-12-llDw5DqnB4LbIUCV.htm](feats/class-12-llDw5DqnB4LbIUCV.htm)|Expert Snowcasting|Incantation nivale experte|libre|
|[class-12-lmAuoHPxzQdaSUmN.htm](feats/class-12-lmAuoHPxzQdaSUmN.htm)|Cartwheel Dodge|Esquive des chars|libre|
|[class-12-lPTcPIshChHWz4J6.htm](feats/class-12-lPTcPIshChHWz4J6.htm)|Critical Debilitation|Handicap critique|officielle|
|[class-12-lv8jRWK7bv7dR6SM.htm](feats/class-12-lv8jRWK7bv7dR6SM.htm)|Gigavolt|Gigavolt|libre|
|[class-12-lx8Wt813qavwLISv.htm](feats/class-12-lx8Wt813qavwLISv.htm)|Expert Beast Gunner Spellcasting|Incantation experte du bestioléro|libre|
|[class-12-MD33E76f2olLnDZb.htm](feats/class-12-MD33E76f2olLnDZb.htm)|Uncanny Suction|Succion étrange|libre|
|[class-12-mgs7vxq6d3hQoswa.htm](feats/class-12-mgs7vxq6d3hQoswa.htm)|Improved Dueling Riposte|Riposte en duel améliorée|officielle|
|[class-12-MrBHGo9nmzcVii3k.htm](feats/class-12-MrBHGo9nmzcVii3k.htm)|Desiccating Inhalation|Inhalation désséchante|libre|
|[class-12-mSDKKRgK6sRMjqQo.htm](feats/class-12-mSDKKRgK6sRMjqQo.htm)|Summoner's Call|Appel du conjurateur|libre|
|[class-12-MsieJK35tAP6gFGv.htm](feats/class-12-MsieJK35tAP6gFGv.htm)|Coffin Bound|Lié au cercueil|libre|
|[class-12-mTkbgFOHJUBl0Qwg.htm](feats/class-12-mTkbgFOHJUBl0Qwg.htm)|Shared Replenishment|Récupération partagée|officielle|
|[class-12-mZcI1NKtQhteAQLn.htm](feats/class-12-mZcI1NKtQhteAQLn.htm)|Silver's Refrain|Refrain d'argent|libre|
|[class-12-n1t6foOyrN48OVPK.htm](feats/class-12-n1t6foOyrN48OVPK.htm)|Collapse Wall|Effondrement de mur|libre|
|[class-12-nBWoZ311FXFJC8Zl.htm](feats/class-12-nBWoZ311FXFJC8Zl.htm)|Dodging Roll|Dérobade|libre|
|[class-12-nI67dTzKYg5kKMsd.htm](feats/class-12-nI67dTzKYg5kKMsd.htm)|Titan's Stature|Stature de titan|libre|
|[class-12-NIzuFBqmHURIy2oI.htm](feats/class-12-NIzuFBqmHURIy2oI.htm)|Ward Slumber|Protection du dormeur|libre|
|[class-12-NJyyxInJ743OotKf.htm](feats/class-12-NJyyxInJ743OotKf.htm)|Deadeye|Précis|libre|
|[class-12-NM5a1tF0OW5mVYdR.htm](feats/class-12-NM5a1tF0OW5mVYdR.htm)|Additional Shadow Magic|Magie de l'ombre supplémentaire|libre|
|[class-12-nPwzElPvV29eM5as.htm](feats/class-12-nPwzElPvV29eM5as.htm)|Chain Reaction|Réaction en chaîne|libre|
|[class-12-nsFnOLqYSkGWFhLD.htm](feats/class-12-nsFnOLqYSkGWFhLD.htm)|Affliction Mercy|Soulagement d'affliction|officielle|
|[class-12-NtaOLg9meDYfg8aV.htm](feats/class-12-NtaOLg9meDYfg8aV.htm)|Blade of Justice|Lame de justice|libre|
|[class-12-nWd7m0yRcIEVUy7O.htm](feats/class-12-nWd7m0yRcIEVUy7O.htm)|Elaborate Scroll Esoterica|Ésotérica parcheminée élaborée|libre|
|[class-12-O0QrBJfiMCTR0n0z.htm](feats/class-12-O0QrBJfiMCTR0n0z.htm)|Greater Despair|Désespoir supérieur|libre|
|[class-12-O1qdoz5N3G4yvHcH.htm](feats/class-12-O1qdoz5N3G4yvHcH.htm)|Greater Physical Evolution|Évolution physique supérieure|libre|
|[class-12-Oa41bfBRO36lf1aE.htm](feats/class-12-Oa41bfBRO36lf1aE.htm)|Shoulder Catastrophe|Endosser la catastrophe|libre|
|[class-12-OrNcWu1Y7c2O5zU6.htm](feats/class-12-OrNcWu1Y7c2O5zU6.htm)|Lightning Snares|Pose éclair de pièges artisanaux|officielle|
|[class-12-oUcB71V1jVaM8SFx.htm](feats/class-12-oUcB71V1jVaM8SFx.htm)|Shared Assault|Assaut partagé|libre|
|[class-12-OyOpHPOXC08bffVR.htm](feats/class-12-OyOpHPOXC08bffVR.htm)|Determined Lore Seeker|Chercheur de connaissances déterminé|libre|
|[class-12-p0jZhb8PSswUsZaz.htm](feats/class-12-p0jZhb8PSswUsZaz.htm)|Dragon Shape|Morphologie draconique|officielle|
|[class-12-P13ZhZcR67Ev0vrS.htm](feats/class-12-P13ZhZcR67Ev0vrS.htm)|Disrupt Ki|Perturbation du ki|officielle|
|[class-12-p2tFR4yBauu8t3mC.htm](feats/class-12-p2tFR4yBauu8t3mC.htm)|Hex Focus|Refocalisation de maléfice|libre|
|[class-12-pbD4lfAPkK1NNag0.htm](feats/class-12-pbD4lfAPkK1NNag0.htm)|Double Prey|Deux proies|libre|
|[class-12-PeBz8f9h8Y4OFdws.htm](feats/class-12-PeBz8f9h8Y4OFdws.htm)|Legs of Stone|Jambes de pierre|libre|
|[class-12-PEszRpnrcB7VPS9G.htm](feats/class-12-PEszRpnrcB7VPS9G.htm)|Gruesome Strike|Frappe horrible|libre|
|[class-12-peUBtdoG5LyVaf5g.htm](feats/class-12-peUBtdoG5LyVaf5g.htm)|Lich Dedication|Dévouement : liche|libre|
|[class-12-pKttKO84dxzRLBIC.htm](feats/class-12-pKttKO84dxzRLBIC.htm)|Expert Combat Eidolon|Eidolon de combat expert|libre|
|[class-12-PLTOCEAvqBS05pZu.htm](feats/class-12-PLTOCEAvqBS05pZu.htm)|Expert Cleric Spellcasting|Incantantation experte du prêtre|officielle|
|[class-12-pM57xTqy5CMYZeqD.htm](feats/class-12-pM57xTqy5CMYZeqD.htm)|Obscured Emergence|Émergence obscurcie|libre|
|[class-12-pm9PS32YNLJ2wp4o.htm](feats/class-12-pm9PS32YNLJ2wp4o.htm)|Reason Rapidly|Déduction rapide|libre|
|[class-12-PmhprI0vr3zTOkok.htm](feats/class-12-PmhprI0vr3zTOkok.htm)|Foreseen Failure|Échec pressenti|libre|
|[class-12-pmVaj53saxLE28Pl.htm](feats/class-12-pmVaj53saxLE28Pl.htm)|Overwhelming Spellstrike|Frappe de sort accablante|libre|
|[class-12-pmz1itHp13JtcrjW.htm](feats/class-12-pmz1itHp13JtcrjW.htm)|Unbalancing Sweep|Balayage déséquilibrant|libre|
|[class-12-pu1U9ZWVVG1Lc94t.htm](feats/class-12-pu1U9ZWVVG1Lc94t.htm)|Diverse Weapon Expert|Expert en armes diverses|libre|
|[class-12-pVDgiaqu1RbCOhuv.htm](feats/class-12-pVDgiaqu1RbCOhuv.htm)|Foresee Danger|Danger anticipé|libre|
|[class-12-Px1QZY0NdO9WAQQS.htm](feats/class-12-Px1QZY0NdO9WAQQS.htm)|Mobile Finisher|Aboutissement mobile|libre|
|[class-12-PxBQ4JdaPS2KTAG7.htm](feats/class-12-PxBQ4JdaPS2KTAG7.htm)|Necromancer's Visage|Visage du nécromant|libre|
|[class-12-pZUmoCqxBJfZmeqm.htm](feats/class-12-pZUmoCqxBJfZmeqm.htm)|Festering Wounds|Blessures suppurantes|libre|
|[class-12-Q4puGx4kBMXy45fa.htm](feats/class-12-Q4puGx4kBMXy45fa.htm)|Familiar's Eyes|Yeux du familier|libre|
|[class-12-qgahXDIjevUmk5Ci.htm](feats/class-12-qgahXDIjevUmk5Ci.htm)|Pact of the Final Breath|Pacte du dernier souffle|libre|
|[class-12-qI5ZyuNVME95iXhJ.htm](feats/class-12-qI5ZyuNVME95iXhJ.htm)|Fantastic Leap|Bond fantastique|officielle|
|[class-12-QicYF43HqgpOBLzo.htm](feats/class-12-QicYF43HqgpOBLzo.htm)|Bloody Debilitation|Handicap sanglant|libre|
|[class-12-qmORiUubF2CVgIva.htm](feats/class-12-qmORiUubF2CVgIva.htm)|Reclaimant Plea|Plaidoyer du reconquérant|libre|
|[class-12-Qss6GKzSYGqKgY4b.htm](feats/class-12-Qss6GKzSYGqKgY4b.htm)|Resolute|Résolu|libre|
|[class-12-quqG3jfWFdopF0G2.htm](feats/class-12-quqG3jfWFdopF0G2.htm)|No!!!|Non !!!|libre|
|[class-12-qZsTI97BQUwoPgKF.htm](feats/class-12-qZsTI97BQUwoPgKF.htm)|Psi Catastrophe|Catastrophe psy|libre|
|[class-12-r0twuF5nxXN5lkLk.htm](feats/class-12-r0twuF5nxXN5lkLk.htm)|Cut the Bonds|Couper les liens|libre|
|[class-12-rHV8GUqqt8WLgTJp.htm](feats/class-12-rHV8GUqqt8WLgTJp.htm)|Thaumaturge's Demesne|Protectorat du thaumaturge|libre|
|[class-12-RJmnFuaGLXfhX3It.htm](feats/class-12-RJmnFuaGLXfhX3It.htm)|Expert Psychic Spellcasting|Incantation experte du psychiste|libre|
|[class-12-rm8NgrdZNjqpGlC1.htm](feats/class-12-rm8NgrdZNjqpGlC1.htm)|Instigate Psychic Duel|Instiguer un duel psychique|libre|
|[class-12-rqUJULinzDCUgimM.htm](feats/class-12-rqUJULinzDCUgimM.htm)|Shooter's Camouflage|Camouflage du tireur|libre|
|[class-12-RSUmrIiFBEchdM8B.htm](feats/class-12-RSUmrIiFBEchdM8B.htm)|Primal Focus|Focalisation primordiale|officielle|
|[class-12-RSwrDo9i0RKoAI6D.htm](feats/class-12-RSwrDo9i0RKoAI6D.htm)|Shepherd of Desolation|Berger de la désolation|libre|
|[class-12-rW1q7x5CMf9Rh1bi.htm](feats/class-12-rW1q7x5CMf9Rh1bi.htm)|Master Spotter (Ranger)|Dénicheur maître (Rôdeur)|officielle|
|[class-12-s6y6JzPW2K8k4m8k.htm](feats/class-12-s6y6JzPW2K8k4m8k.htm)|Expert Summoner Spellcasting|Incantation experte du conjurateur|libre|
|[class-12-saEwTvJuiemEIfLm.htm](feats/class-12-saEwTvJuiemEIfLm.htm)|Expert Druid Spellcasting|Incantation experte du druide|officielle|
|[class-12-sjqnusj5Py7AiofF.htm](feats/class-12-sjqnusj5Py7AiofF.htm)|Perfection's Path (Will)|Voie de la perfection (Volonté)|officielle|
|[class-12-ssrublnppwFSvVcb.htm](feats/class-12-ssrublnppwFSvVcb.htm)|Reach for the Sky|Haut les mains !|libre|
|[class-12-SyxXnSk2R0AM9HSn.htm](feats/class-12-SyxXnSk2R0AM9HSn.htm)|Flicker|Image vacillante|libre|
|[class-12-t8CAK8ylu23PUxbn.htm](feats/class-12-t8CAK8ylu23PUxbn.htm)|Master Spotter (Investigator)|Dénicheur maître (Enquêteur)|libre|
|[class-12-tBfalnbUZLkG9gs1.htm](feats/class-12-tBfalnbUZLkG9gs1.htm)|Blade of Law|Lame de la Loi|libre|
|[class-12-tGXJU6yx7bYuyLvd.htm](feats/class-12-tGXJU6yx7bYuyLvd.htm)|Brutal Finish|Final brutal|officielle|
|[class-12-U0XX0Mm2MbZcuNK2.htm](feats/class-12-U0XX0Mm2MbZcuNK2.htm)|Shared Overdrive|Surrégime partagé|libre|
|[class-12-U4AoJMBhJaFq5O1S.htm](feats/class-12-U4AoJMBhJaFq5O1S.htm)|Champion's Sacrifice|Sacrifice du champion|officielle|
|[class-12-U5hjS5qFrtGHWlVG.htm](feats/class-12-U5hjS5qFrtGHWlVG.htm)|Corpse Stench|Puanteur de cadavre|libre|
|[class-12-U9uRb2TezYlQPNJd.htm](feats/class-12-U9uRb2TezYlQPNJd.htm)|Boost Modulation|Modulation de renfort|libre|
|[class-12-U9zIjPHPe2PyrtlU.htm](feats/class-12-U9zIjPHPe2PyrtlU.htm)|Flexible Transmogrification|Transmogrification souple|libre|
|[class-12-uAh31Hnp1EZSjd40.htm](feats/class-12-uAh31Hnp1EZSjd40.htm)|Invincible Mutagen|Mutagène invincible|officielle|
|[class-12-UH8pDxrq1xJq4Sid.htm](feats/class-12-UH8pDxrq1xJq4Sid.htm)|Master Spotter|Maître observateur|libre|
|[class-12-UhmPekgw2HO40zKC.htm](feats/class-12-UhmPekgw2HO40zKC.htm)|Eagle Eyes|Yeux d'aigle|libre|
|[class-12-UIRcjHxuSedoDOj4.htm](feats/class-12-UIRcjHxuSedoDOj4.htm)|Inspirational Focus|Focalisation inspirante|officielle|
|[class-12-UJafwv306v75Syy7.htm](feats/class-12-UJafwv306v75Syy7.htm)|Forcible Energy|Énergie vigoureuse|libre|
|[class-12-uJgATfMW3kumS6Y0.htm](feats/class-12-uJgATfMW3kumS6Y0.htm)|Intensify Investiture|Investiture intensifiée|libre|
|[class-12-UotFrqA7zAxtpJdE.htm](feats/class-12-UotFrqA7zAxtpJdE.htm)|Firearm Expert|Expert en arme à feu|libre|
|[class-12-UqA9GdO2pGQwg9cd.htm](feats/class-12-UqA9GdO2pGQwg9cd.htm)|Flexible Halcyon Spellcasting|Incantation syncrétique flexible|libre|
|[class-12-uQExVrBNPJeS66sO.htm](feats/class-12-uQExVrBNPJeS66sO.htm)|For Love, For Lightning|Pour l'amour, pour la foudre|libre|
|[class-12-UrOj9TROtn8nuxPf.htm](feats/class-12-UrOj9TROtn8nuxPf.htm)|Expert Scroll Cache|Réserve experte de parchemins|libre|
|[class-12-uvbPZR6dsQBimIUo.htm](feats/class-12-uvbPZR6dsQBimIUo.htm)|Imbue Mindlessness|Ôter l'intelligence|libre|
|[class-12-UxjAszOYAhUvCDt2.htm](feats/class-12-UxjAszOYAhUvCDt2.htm)|Inescapable Grasp|Prise incontournable|libre|
|[class-12-UZDiqc5bBJzOTxUQ.htm](feats/class-12-UZDiqc5bBJzOTxUQ.htm)|Advanced Seeker of Truths|Chercheur de vérités avancé|libre|
|[class-12-Uzz7oZit3FNO9FxO.htm](feats/class-12-Uzz7oZit3FNO9FxO.htm)|Wings Of The Dragon|Ailes du dragon|libre|
|[class-12-va7YMidXZW21oFwA.htm](feats/class-12-va7YMidXZW21oFwA.htm)|Blood Component Substitution|Substitution d'une composante sanguine|libre|
|[class-12-VD446AflrQ3kO1al.htm](feats/class-12-VD446AflrQ3kO1al.htm)|Evasiveness (Rogue)|Dérobade (Roublard)|officielle|
|[class-12-vFa9crHKkNkPUjFl.htm](feats/class-12-vFa9crHKkNkPUjFl.htm)|Shared Sight|Lumière partagée|libre|
|[class-12-VIjI8PtkTFjeAA6a.htm](feats/class-12-VIjI8PtkTFjeAA6a.htm)|Ricochet Feint|Feinte de ricochet|libre|
|[class-12-VN3OHDYcnLaw0nW1.htm](feats/class-12-VN3OHDYcnLaw0nW1.htm)|Wild Strider|Arpenteur des milieux naturels|libre|
|[class-12-vPA0EGXBaNzGxlxM.htm](feats/class-12-vPA0EGXBaNzGxlxM.htm)|Forewarn|Prévenir|libre|
|[class-12-vqHF9U2RkL2NVgMF.htm](feats/class-12-vqHF9U2RkL2NVgMF.htm)|Enticing Dwelling|Demeure attrayante|libre|
|[class-12-VS28L98uFhr3j1HC.htm](feats/class-12-VS28L98uFhr3j1HC.htm)|Their Master's Call|Appel de leur maître|libre|
|[class-12-vVhgYkOU9mPTGTxF.htm](feats/class-12-vVhgYkOU9mPTGTxF.htm)|Domain Focus|Focalisation du domaine|officielle|
|[class-12-vwBD55BRDOatp4ZV.htm](feats/class-12-vwBD55BRDOatp4ZV.htm)|Green Tongue|Langue végétale|officielle|
|[class-12-vYmun4LyWtWtoycP.htm](feats/class-12-vYmun4LyWtWtoycP.htm)|Cracked Mountain|Montagne craquelée|libre|
|[class-12-VzQtyHSjq12E4Dzh.htm](feats/class-12-VzQtyHSjq12E4Dzh.htm)|Purifying Spell|Sort purificateur|libre|
|[class-12-W7Rkw1L5QxHvgeUW.htm](feats/class-12-W7Rkw1L5QxHvgeUW.htm)|Eagle Eye|Oeil d'aigle|libre|
|[class-12-wDo5dsSmyJqfmPgj.htm](feats/class-12-wDo5dsSmyJqfmPgj.htm)|Signifer Armor Expertise|Expertise de l'armure de signifer|libre|
|[class-12-wGaxWwJhIXbMJft1.htm](feats/class-12-wGaxWwJhIXbMJft1.htm)|Plentiful Snares|Pièges artisanaux nombreux|libre|
|[class-12-wkJ6EjtXUztOqTwH.htm](feats/class-12-wkJ6EjtXUztOqTwH.htm)|Perfection's Path (Reflex)|Voie de la perfection (Réflexes)|officielle|
|[class-12-WO23FvCo8IYVB5RF.htm](feats/class-12-WO23FvCo8IYVB5RF.htm)|Blade of the Crimson Oath|Lame du Serment écarlate|libre|
|[class-12-WsEVkMFe8ZEIRKLu.htm](feats/class-12-WsEVkMFe8ZEIRKLu.htm)|Grasping Limbs|Membres agrippants|libre|
|[class-12-WyGeBz9U6Hovozdl.htm](feats/class-12-WyGeBz9U6Hovozdl.htm)|Reverse Curse|Renverser la malédiction|libre|
|[class-12-X62yfg4vky1JMnf1.htm](feats/class-12-X62yfg4vky1JMnf1.htm)|Illusory Identity|Identité illusoire|libre|
|[class-12-x7vMKBSrxXmfs5C2.htm](feats/class-12-x7vMKBSrxXmfs5C2.htm)|Expert Witch Spellcasting|Incantation experte du sorcier|libre|
|[class-12-xAFdoKl7aOP9rVkl.htm](feats/class-12-xAFdoKl7aOP9rVkl.htm)|Emblazon Antimagic|Antimagie blasonnée|officielle|
|[class-12-xBqDeQFzvuDfqhZC.htm](feats/class-12-xBqDeQFzvuDfqhZC.htm)|Reactive Interference|Interférence réactive|officielle|
|[class-12-Xdf00Kmv5C3qqrtK.htm](feats/class-12-Xdf00Kmv5C3qqrtK.htm)|Flesh Wound|Blessure superficielle|libre|
|[class-12-XgUQ6Tm9LKxcZGHW.htm](feats/class-12-XgUQ6Tm9LKxcZGHW.htm)|Knight in Shining Armor|Chevalier en armure étincelante|libre|
|[class-12-xmccXo6U7P0IMM3z.htm](feats/class-12-xmccXo6U7P0IMM3z.htm)|Reaper of Repose|Violeur de repos|libre|
|[class-12-xNejAvuRXKYq2D6A.htm](feats/class-12-xNejAvuRXKYq2D6A.htm)|Swap Investment|Transfert d'investiture|libre|
|[class-12-XWmlGnFNfxyJWw9V.htm](feats/class-12-XWmlGnFNfxyJWw9V.htm)|Choking Smoke|Fumée étouffante|libre|
|[class-12-xWWcxZUgQTaHZHkY.htm](feats/class-12-xWWcxZUgQTaHZHkY.htm)|School Counterspell|Contresort d'école|libre|
|[class-12-XyQpqznuO5LGFvhz.htm](feats/class-12-XyQpqznuO5LGFvhz.htm)|Coordinated Distraction|Diversion coordonnée|libre|
|[class-12-xzesXSIXTqsVxm1e.htm](feats/class-12-xzesXSIXTqsVxm1e.htm)|Golem Dynamo|Dynamo golem|libre|
|[class-12-yaxf1Tpk5iwPCSpW.htm](feats/class-12-yaxf1Tpk5iwPCSpW.htm)|Eclectic Polymath|Touche-à-tout éclectique|officielle|
|[class-12-YidYY7k2gvny9eSY.htm](feats/class-12-YidYY7k2gvny9eSY.htm)|Judgement of the Monolith|Jugement du monolithe|libre|
|[class-12-ysXXa8K9fa383HAD.htm](feats/class-12-ysXXa8K9fa383HAD.htm)|Great Bear|Ours géant|libre|
|[class-12-YTHsakqXumdMU0dn.htm](feats/class-12-YTHsakqXumdMU0dn.htm)|Conflux Focus|Focalisation de confluence|libre|
|[class-12-YZ138OqflDhrkqmR.htm](feats/class-12-YZ138OqflDhrkqmR.htm)|Enforce Oath|Serment renforcé|libre|
|[class-12-YZEG7LsiIIhwRB91.htm](feats/class-12-YZEG7LsiIIhwRB91.htm)|Necromantic Deflection|Déviation nécromantique|libre|
|[class-12-ZghzLmYgeE19GqjP.htm](feats/class-12-ZghzLmYgeE19GqjP.htm)|Lunging Stance|Posture de fente|libre|
|[class-12-Zn2ySapQ2gtgyWgW.htm](feats/class-12-Zn2ySapQ2gtgyWgW.htm)|Aegis of Arnisant|Égide d'Arnisant|libre|
|[class-12-zSTPB1FFWMfA1JPi.htm](feats/class-12-zSTPB1FFWMfA1JPi.htm)|Bloodline Focus|Focalisation du lignage|officielle|
|[class-12-zybYSfeM0PLPpAaa.htm](feats/class-12-zybYSfeM0PLPpAaa.htm)|Infectious Emotions|Émotions infectieuses|libre|
|[class-12-zzMugLCUkQQPa2qT.htm](feats/class-12-zzMugLCUkQQPa2qT.htm)|Preparation|Préparation|libre|
|[class-14-0EY2WQC3Hb6Mitgz.htm](feats/class-14-0EY2WQC3Hb6Mitgz.htm)|Form Lock|Verrouillage de forme|libre|
|[class-14-0kkiE74cyHyxdPe6.htm](feats/class-14-0kkiE74cyHyxdPe6.htm)|Vengeful Strike|Frappe vindicative|officielle|
|[class-14-0qL4a3CarG1e0pfB.htm](feats/class-14-0qL4a3CarG1e0pfB.htm)|Forceful Shot|Tir percutant|libre|
|[class-14-0S92aZtljjTAwLdO.htm](feats/class-14-0S92aZtljjTAwLdO.htm)|Drain Soul Cage|Drain de phylactère|libre|
|[class-14-0xh9ISHFUFHqngK0.htm](feats/class-14-0xh9ISHFUFHqngK0.htm)|Execution|Exécution|libre|
|[class-14-1DaSVLJEdJWYOWek.htm](feats/class-14-1DaSVLJEdJWYOWek.htm)|Guiding Riposte|Riposte directive|officielle|
|[class-14-1fVKWWYjlVtOECku.htm](feats/class-14-1fVKWWYjlVtOECku.htm)|Shape of the Cloud Dragon|Forme de dragon des nuages|libre|
|[class-14-1SvBUzVH5tp0lmn5.htm](feats/class-14-1SvBUzVH5tp0lmn5.htm)|Two-Weapon Flurry|Déluge à deux armes|officielle|
|[class-14-2FJwXMTJycSZY80Q.htm](feats/class-14-2FJwXMTJycSZY80Q.htm)|Target Of Opportunity|Cible d'opportunité|libre|
|[class-14-2h8a6pKhXTXwpJjP.htm](feats/class-14-2h8a6pKhXTXwpJjP.htm)|Premonition Of Clarity|Clarté prémonitoire|libre|
|[class-14-2HMvAB6mIVwvwtjT.htm](feats/class-14-2HMvAB6mIVwvwtjT.htm)|True Debilitating Bomb|Bombe incapacitante ultime|officielle|
|[class-14-2KBKXkRthBXpw48X.htm](feats/class-14-2KBKXkRthBXpw48X.htm)|Stay Down!|Reste à terre !|libre|
|[class-14-2l15VfVHMw3ttgJ3.htm](feats/class-14-2l15VfVHMw3ttgJ3.htm)|Incredible Recollection|Souvenirs incroyables|libre|
|[class-14-2VZxwS5LTi9YxikG.htm](feats/class-14-2VZxwS5LTi9YxikG.htm)|Reset the Past|Remettre le passé à zéro|libre|
|[class-14-2wLfchKVDSKBOIpV.htm](feats/class-14-2wLfchKVDSKBOIpV.htm)|Soaring Armor|Armure volante|libre|
|[class-14-3hnmQrhv4Bru5GKR.htm](feats/class-14-3hnmQrhv4Bru5GKR.htm)|Grand Talisman Esoterica|Ésotérica talismanique ultime|libre|
|[class-14-3r5rg0BCqSh5RBNS.htm](feats/class-14-3r5rg0BCqSh5RBNS.htm)|Specialized Companion (Druid)|Compagnon spécialisé (druide)|officielle|
|[class-14-3tm0TzyjO1I378fw.htm](feats/class-14-3tm0TzyjO1I378fw.htm)|Conscious Spell Specialization|Spécialisation de sort conscient|libre|
|[class-14-45F4nNN8gxoBdSnk.htm](feats/class-14-45F4nNN8gxoBdSnk.htm)|Enshroud Soul Cage|Obfuscation du phylactère|libre|
|[class-14-4dQcLroKQ13QYIT3.htm](feats/class-14-4dQcLroKQ13QYIT3.htm)|Tactical Cadence|Cadence tactique|libre|
|[class-14-4n59y5tb9bxffKsi.htm](feats/class-14-4n59y5tb9bxffKsi.htm)|Esoteric Reflexes|Réflexes ésotériques|libre|
|[class-14-4Q9Q41KLPYJMdV4b.htm](feats/class-14-4Q9Q41KLPYJMdV4b.htm)|Reactive Transformation|Transformation réactive|libre|
|[class-14-4SKech3n0F38HrR5.htm](feats/class-14-4SKech3n0F38HrR5.htm)|Vigorous Inspiration|Inspiration vigoureuse|libre|
|[class-14-5cxkVY3mBsbYWd5K.htm](feats/class-14-5cxkVY3mBsbYWd5K.htm)|Timeless Body|Éternelle jeunesse|officielle|
|[class-14-5dAFkOYPz8PPdFrw.htm](feats/class-14-5dAFkOYPz8PPdFrw.htm)|Leave an Opening|Laisser une ouverture|officielle|
|[class-14-5NyX1WnXEO40yEaT.htm](feats/class-14-5NyX1WnXEO40yEaT.htm)|Wyrmbane Aura|Aura anti-draconique|officielle|
|[class-14-5zeKULjKDkiIenhu.htm](feats/class-14-5zeKULjKDkiIenhu.htm)|Unstable Redundancies|Redondances instables|libre|
|[class-14-6gtm0YCaDFpyVy35.htm](feats/class-14-6gtm0YCaDFpyVy35.htm)|Warden's Guidance|Directive du protecteur|officielle|
|[class-14-8nCxI2SZ64UmMuRZ.htm](feats/class-14-8nCxI2SZ64UmMuRZ.htm)|Entities From Afar|Entités du lointain|libre|
|[class-14-8PMxl8o5YXET58Pn.htm](feats/class-14-8PMxl8o5YXET58Pn.htm)|Iron Lung|Poumon d'acier|libre|
|[class-14-9DECwTTiVpHJc4B6.htm](feats/class-14-9DECwTTiVpHJc4B6.htm)|Specialized Megafauna Companion|Compagnon mégafaune spécialisé|libre|
|[class-14-9t6Kfk8Yw8WJYY8Z.htm](feats/class-14-9t6Kfk8Yw8WJYY8Z.htm)|Guiding Finish|Final directif|officielle|
|[class-14-9zfcIXDG2mDpiypp.htm](feats/class-14-9zfcIXDG2mDpiypp.htm)|Armored Exercise|Exercice en armure|libre|
|[class-14-9zH7IOsmhRBEqXAV.htm](feats/class-14-9zH7IOsmhRBEqXAV.htm)|Instant Opening|Ouverture instantanée|officielle|
|[class-14-A7uJCDT9odsuwlqW.htm](feats/class-14-A7uJCDT9odsuwlqW.htm)|Triggerbrand Blitz|Attaque éclair du pistolame|libre|
|[class-14-AfTMuAln2f0Pa3Lj.htm](feats/class-14-AfTMuAln2f0Pa3Lj.htm)|Shadow's Web|Toile de l'ombre|libre|
|[class-14-AGydz5DKJ2KHSO4S.htm](feats/class-14-AGydz5DKJ2KHSO4S.htm)|Whirlwind Strike|Frappe tourbillonnante|officielle|
|[class-14-AiDelwfA0uVT2lN3.htm](feats/class-14-AiDelwfA0uVT2lN3.htm)|Signature Synergy|Synergie signature|libre|
|[class-14-AOLf6QX068LR9L9e.htm](feats/class-14-AOLf6QX068LR9L9e.htm)|Earworm|Rengaine|libre|
|[class-14-aOYnK0xe9DKFtx7d.htm](feats/class-14-aOYnK0xe9DKFtx7d.htm)|Innocent Butterfly|Papillon innocent|libre|
|[class-14-aPt0WfFoeLTzyQRA.htm](feats/class-14-aPt0WfFoeLTzyQRA.htm)|Defensive Roll|Roulade défensive|officielle|
|[class-14-Aqhsx5duEpBgaPB0.htm](feats/class-14-Aqhsx5duEpBgaPB0.htm)|Shared Prey|Proie commune|officielle|
|[class-14-As8cRK5jVzf62fEd.htm](feats/class-14-As8cRK5jVzf62fEd.htm)|Peculiar Anatomy|Anatomie bizarre|libre|
|[class-14-AtpMrGXaMPJtDIDR.htm](feats/class-14-AtpMrGXaMPJtDIDR.htm)|Prayer Attack|Attaque prière|libre|
|[class-14-AV6KZvUed7GdhHzc.htm](feats/class-14-AV6KZvUed7GdhHzc.htm)|Bizarre Transformation|Transformation bizarre|libre|
|[class-14-b5K067Pma4Il9IeD.htm](feats/class-14-b5K067Pma4Il9IeD.htm)|Anchoring Aura|Aura d'ancrage|officielle|
|[class-14-B912Vru18B6orzcs.htm](feats/class-14-B912Vru18B6orzcs.htm)|Trespass Teleportation|Effraction de la téléportation|libre|
|[class-14-bEdFgywri7fABhBT.htm](feats/class-14-bEdFgywri7fABhBT.htm)|Absorb Spell|Absorption de sort|libre|
|[class-14-Bi4rdz48MgkSY7su.htm](feats/class-14-Bi4rdz48MgkSY7su.htm)|Crimson Oath Devotion|Dévotion au Serment écarlate|libre|
|[class-14-bjNeSAldeTzRcEaQ.htm](feats/class-14-bjNeSAldeTzRcEaQ.htm)|Interweave Dispel|Dissipation entremêlée|officielle|
|[class-14-bki36RiEM5FR4aiT.htm](feats/class-14-bki36RiEM5FR4aiT.htm)|Specialized Beastmaster Companion|Compagnon spécialisé (maître des bêtes)|libre|
|[class-14-bSC18SbdaNXfBHu9.htm](feats/class-14-bSC18SbdaNXfBHu9.htm)|Dance of Thunder|Danse de tonnerre|libre|
|[class-14-BuaTJxALqxM5EZav.htm](feats/class-14-BuaTJxALqxM5EZav.htm)|Perfect Finisher|Aboutissement parfait|libre|
|[class-14-cgtVYaZRVaTIW4sk.htm](feats/class-14-cgtVYaZRVaTIW4sk.htm)|Spell-Repelling Form|Forme repousse-sort|libre|
|[class-14-CR9NcAIPTT4oWSEy.htm](feats/class-14-CR9NcAIPTT4oWSEy.htm)|Hammer Quake|Séisme du marteau|libre|
|[class-14-ct4dJHBn1Dj4cx4B.htm](feats/class-14-ct4dJHBn1Dj4cx4B.htm)|Tongue of Sun and Moon|Langue du soleil et de la lune|officielle|
|[class-14-d8yggbcJsKKyHip7.htm](feats/class-14-d8yggbcJsKKyHip7.htm)|Consume Spell|Consommer un sort|libre|
|[class-14-dBB71rps7dbat4Vo.htm](feats/class-14-dBB71rps7dbat4Vo.htm)|Dream Logic|Logique onirique|libre|
|[class-14-dloGUhZYG1xUPVE4.htm](feats/class-14-dloGUhZYG1xUPVE4.htm)|Whirling Knockdown|Moulinet renversant|libre|
|[class-14-DM9rjXZrmx2MFX7k.htm](feats/class-14-DM9rjXZrmx2MFX7k.htm)|Sense The Unseen|Percevoir l'imperceptible|libre|
|[class-14-DT1O80hPD7MX6oWp.htm](feats/class-14-DT1O80hPD7MX6oWp.htm)|Spell Shroud|Suaire de sort|libre|
|[class-14-DUb1VWSbTjdsbAkQ.htm](feats/class-14-DUb1VWSbTjdsbAkQ.htm)|Deity's Protection|Protection de la divinité|officielle|
|[class-14-Ec9Q8cvOKFgeezx6.htm](feats/class-14-Ec9Q8cvOKFgeezx6.htm)|Terrible Transformation|Transformation terrible|libre|
|[class-14-EeSP1SNlgAuASvkP.htm](feats/class-14-EeSP1SNlgAuASvkP.htm)|Preternatural Parry|Parade surnaturelle|libre|
|[class-14-Ek3nCIFRreqnSxAQ.htm](feats/class-14-Ek3nCIFRreqnSxAQ.htm)|Mysterious Repertoire|Répertoire mystérieux|libre|
|[class-14-eR0sifECG27CC4do.htm](feats/class-14-eR0sifECG27CC4do.htm)|Shape Of The Dragon|Forme de dragon|libre|
|[class-14-erCOcFZJPT2O3gwC.htm](feats/class-14-erCOcFZJPT2O3gwC.htm)|Aura of Righteousness|Aura de vertu|officielle|
|[class-14-eUv2L0CLidxtA3sh.htm](feats/class-14-eUv2L0CLidxtA3sh.htm)|Dormant Eruption|Éruption en sommeil|libre|
|[class-14-EVNd9hZs49b1pScR.htm](feats/class-14-EVNd9hZs49b1pScR.htm)|Dual Onslaught|Massacre à deux armes|libre|
|[class-14-EvSfoYmuCDCRAvaF.htm](feats/class-14-EvSfoYmuCDCRAvaF.htm)|Divine Reflexes|Réflexes divins|officielle|
|[class-14-ewbt80Yin18k6oLq.htm](feats/class-14-ewbt80Yin18k6oLq.htm)|Tangled Forest Rake|Ratissage de la forêt enchevêtrée|officielle|
|[class-14-F7Ao9p17ocf3JVvy.htm](feats/class-14-F7Ao9p17ocf3JVvy.htm)|Shadow Power|Pouvoir de l'ombre|libre|
|[class-14-gQKPKSS5KyK3uUfs.htm](feats/class-14-gQKPKSS5KyK3uUfs.htm)|Resolute Defender|Défenseur résolu|libre|
|[class-14-GsrwoQ7DIjERXuPf.htm](feats/class-14-GsrwoQ7DIjERXuPf.htm)|Superior Bond|Lien supérieur|officielle|
|[class-14-guSjEQS3WuXJqQxf.htm](feats/class-14-guSjEQS3WuXJqQxf.htm)|Impaling Thrust|Poussée empalante|libre|
|[class-14-h7KZXNRm1gLV1yTt.htm](feats/class-14-h7KZXNRm1gLV1yTt.htm)|Mist Escape|Fuite brumeuse|libre|
|[class-14-h8OB6zcdIqUPS6PC.htm](feats/class-14-h8OB6zcdIqUPS6PC.htm)|Airborne Form|Forme aérienne|libre|
|[class-14-hO4sKslTrSQMLbGx.htm](feats/class-14-hO4sKslTrSQMLbGx.htm)|Mountain Quake|Tremblement de la montagne|officielle|
|[class-14-hPfqQpiq6W8RPCxz.htm](feats/class-14-hPfqQpiq6W8RPCxz.htm)|It Was Me All Along!|C'était moi tout du long !|libre|
|[class-14-HquaVwjOLSPzcJgB.htm](feats/class-14-HquaVwjOLSPzcJgB.htm)|Impossible Riposte|Riposte invraisemblable|libre|
|[class-14-HVwmYfSLhrnCksHV.htm](feats/class-14-HVwmYfSLhrnCksHV.htm)|Targeting Shot|Tir de ciblage|officielle|
|[class-14-HxkAhxcGvua6SkfS.htm](feats/class-14-HxkAhxcGvua6SkfS.htm)|Glib Mutagen|Mutagène de bagou|officielle|
|[class-14-I1xq9TDYRHNtqzGz.htm](feats/class-14-I1xq9TDYRHNtqzGz.htm)|Sink and Swim|Coule et nage|libre|
|[class-14-IaiEZaA8erufMUCr.htm](feats/class-14-IaiEZaA8erufMUCr.htm)|Whirling Blade Stance|Posture de la lame tournoyante|libre|
|[class-14-ibaxh77mm8ttObdk.htm](feats/class-14-ibaxh77mm8ttObdk.htm)|Thwart Evil|Déjouer le mal|libre|
|[class-14-Ice8oNOTbPFXyOww.htm](feats/class-14-Ice8oNOTbPFXyOww.htm)|Aura of Vengeance|Aura de vengeance|officielle|
|[class-14-IdKfWg48qMieuzl7.htm](feats/class-14-IdKfWg48qMieuzl7.htm)|Murderer's Circle|Cercle meurtrier|libre|
|[class-14-IqDbNiwHQH1xApo9.htm](feats/class-14-IqDbNiwHQH1xApo9.htm)|Ironblood Surge|Afflux de sang de fer|officielle|
|[class-14-j4QSlswoBCVrPYa8.htm](feats/class-14-j4QSlswoBCVrPYa8.htm)|Consecrated Aura|Aura consacrée|libre|
|[class-14-jdrdkxqautAtszCX.htm](feats/class-14-jdrdkxqautAtszCX.htm)|Share Eidolon Magic|Partager la magie de l'eidolon|libre|
|[class-14-jEq4JcZb0LpKOZy1.htm](feats/class-14-jEq4JcZb0LpKOZy1.htm)|Diverse Armor Expert|Expert en armures diverses|libre|
|[class-14-JgidFws6bOoGQcti.htm](feats/class-14-JgidFws6bOoGQcti.htm)|Words of Unraveling|Mots de résolution|libre|
|[class-14-jhNkuXg8vJ29GSUJ.htm](feats/class-14-jhNkuXg8vJ29GSUJ.htm)|Deep Roots|Racines profondes|libre|
|[class-14-jK0hjx4qsRyKnpBd.htm](feats/class-14-jK0hjx4qsRyKnpBd.htm)|Starlight Armor|Armure de lumière étoilée|libre|
|[class-14-jwQERVkjtnlFp3Ec.htm](feats/class-14-jwQERVkjtnlFp3Ec.htm)|Fast Channel|Canalisation rapide|officielle|
|[class-14-k0h4jvjxK0fYFvOU.htm](feats/class-14-k0h4jvjxK0fYFvOU.htm)|Pact of Eldritch Eyes|Pactes des yeux mystiques|libre|
|[class-14-k42ntHdg70ZMEKrs.htm](feats/class-14-k42ntHdg70ZMEKrs.htm)|Purifying Breeze|Brise purifiante|libre|
|[class-14-k72W0qMXsX5ekJTF.htm](feats/class-14-k72W0qMXsX5ekJTF.htm)|Reflect Spell|Sort réfléchi|libre|
|[class-14-kC92uxCvrxDkrCpO.htm](feats/class-14-kC92uxCvrxDkrCpO.htm)|Borrow Memories|Emprunter les souvenirs|libre|
|[class-14-kTRGAST9J9ZxJZ4A.htm](feats/class-14-kTRGAST9J9ZxJZ4A.htm)|Twinned Defense (Swashbuckler)|Défense jumelée (Bretteur)|libre|
|[class-14-l5nqNPmZPG6lYFnZ.htm](feats/class-14-l5nqNPmZPG6lYFnZ.htm)|Hasted Assault|Assaut hâtif|libre|
|[class-14-LDv6RVuDXJ9nOfhj.htm](feats/class-14-LDv6RVuDXJ9nOfhj.htm)|Halcyon Spellcasting Adept|Adepte de l'incantation syncrétique|libre|
|[class-14-LhqWrgeBH2sdRxND.htm](feats/class-14-LhqWrgeBH2sdRxND.htm)|Unlimited Ghost Flight|Vol du fantôme illimité|libre|
|[class-14-llawV63qzdynbOkx.htm](feats/class-14-llawV63qzdynbOkx.htm)|Stone Communion|Communion avec la pierre|libre|
|[class-14-LmdOWCDffhBiyzM3.htm](feats/class-14-LmdOWCDffhBiyzM3.htm)|Soothing Ballad|Ballade apaisante|officielle|
|[class-14-lPoP5TFfq266kR6g.htm](feats/class-14-lPoP5TFfq266kR6g.htm)|Spellmaster's Tenacity|Tenacité du maître des sorts|libre|
|[class-14-lZA7SYBmSPFlKNr2.htm](feats/class-14-lZA7SYBmSPFlKNr2.htm)|Effortless Captivation|Captivant sans effort|libre|
|[class-14-matJDIUDvgaJqyiF.htm](feats/class-14-matJDIUDvgaJqyiF.htm)|Hand of the Lich|Main de la liche|libre|
|[class-14-MFqFvuiYDAoADcft.htm](feats/class-14-MFqFvuiYDAoADcft.htm)|Giant's Lunge|Fente de géant|officielle|
|[class-14-MfvkZ9efD013H34Z.htm](feats/class-14-MfvkZ9efD013H34Z.htm)|Come at Me!|Viens à moi !|libre|
|[class-14-mprVzno2BR2VhRSJ.htm](feats/class-14-mprVzno2BR2VhRSJ.htm)|Winter's Kiss|Baiser de l'hiver|libre|
|[class-14-MwozkE6aj42WZ7Z1.htm](feats/class-14-MwozkE6aj42WZ7Z1.htm)|Stealthy Companion|Compagnon furtif|officielle|
|[class-14-MyDluIbnX2sjm3pB.htm](feats/class-14-MyDluIbnX2sjm3pB.htm)|Ranged Disarm|Désarmement à distance|libre|
|[class-14-N02sHmzuF9XQj93m.htm](feats/class-14-N02sHmzuF9XQj93m.htm)|Resilient Shell|Carapace résistante|libre|
|[class-14-N7CM5CmHuZ1cylV9.htm](feats/class-14-N7CM5CmHuZ1cylV9.htm)|Plot The Future|Planifier l'avenir|libre|
|[class-14-NCsjkjEp6kHFS07h.htm](feats/class-14-NCsjkjEp6kHFS07h.htm)|Concentrated Assault|Assaut concentré|libre|
|[class-14-NDbNNMouxnnwBqwm.htm](feats/class-14-NDbNNMouxnnwBqwm.htm)|Explosive Maneuver|Manoeuvre explosive|libre|
|[class-14-NgUB5toKxBd8RJmm.htm](feats/class-14-NgUB5toKxBd8RJmm.htm)|Strategic Bypass|Contournement stratégique|libre|
|[class-14-NNeRv9Gcua1kMp4s.htm](feats/class-14-NNeRv9Gcua1kMp4s.htm)|Forestall Curse|Malédiction contrecarrée|libre|
|[class-14-nvPxCUOCMaYdhLp1.htm](feats/class-14-nvPxCUOCMaYdhLp1.htm)|Desperate Finisher|Aboutissement désespéré|libre|
|[class-14-nY8HtHNJMqP0hz3v.htm](feats/class-14-nY8HtHNJMqP0hz3v.htm)|Phase Bullet|Balle de phase|libre|
|[class-14-OEwNLolzBarx8icm.htm](feats/class-14-OEwNLolzBarx8icm.htm)|Explosive Death Drop|Projection mortelle explosive|libre|
|[class-14-orjVLLoziFTmf1mz.htm](feats/class-14-orjVLLoziFTmf1mz.htm)|Verdant Metamorphosis|Métamorphose verdoyante|officielle|
|[class-14-Pk6qGMlef5SMhhwE.htm](feats/class-14-Pk6qGMlef5SMhhwE.htm)|Night's Warning|Avertissement nocturne|libre|
|[class-14-PSpwdvuddC9kXONz.htm](feats/class-14-PSpwdvuddC9kXONz.htm)|Death's Door|Porte de la mort|libre|
|[class-14-psr9lxsnj9aOiqlK.htm](feats/class-14-psr9lxsnj9aOiqlK.htm)|Sacral Monarch|Monarque sacré|libre|
|[class-14-q0DGOuLuJNLzgM8d.htm](feats/class-14-q0DGOuLuJNLzgM8d.htm)|Unending Emptiness|Vide sans fin|libre|
|[class-14-q2t5qfgQHFJbbV8d.htm](feats/class-14-q2t5qfgQHFJbbV8d.htm)|Shift Spell|Déplacer le sort|libre|
|[class-14-Q4NiHmThMtk8razS.htm](feats/class-14-Q4NiHmThMtk8razS.htm)|Headshot|Tir à la tête|libre|
|[class-14-QC2ecMZ57MRJlxco.htm](feats/class-14-QC2ecMZ57MRJlxco.htm)|Bonded Focus|Focaliseur lié|officielle|
|[class-14-QpnZwabXOVICJL5i.htm](feats/class-14-QpnZwabXOVICJL5i.htm)|Greater Merciful Elixir|Élixir miséricordieux supérieur|officielle|
|[class-14-r4ZVpDjX2X0yrmhw.htm](feats/class-14-r4ZVpDjX2X0yrmhw.htm)|Blast Tackle|Abordage explosif|libre|
|[class-14-R5hdEbfKdIVAQs24.htm](feats/class-14-R5hdEbfKdIVAQs24.htm)|Paragon Companion|Parangon compagnon créature artificielle|libre|
|[class-14-RbHacJSoe6XHT8Ks.htm](feats/class-14-RbHacJSoe6XHT8Ks.htm)|Litany of Righteousness|Litanie de vertu|officielle|
|[class-14-rCnaBbk0M1gBVHjG.htm](feats/class-14-rCnaBbk0M1gBVHjG.htm)|Awesome Blow|Coup fabuleux|libre|
|[class-14-RHdY0xczRYzkIdJt.htm](feats/class-14-RHdY0xczRYzkIdJt.htm)|Instinctual Interception|Interception instinctive|libre|
|[class-14-RHLfM9NlIlHTH85w.htm](feats/class-14-RHLfM9NlIlHTH85w.htm)|Triumphant Inspiration|Inspiration triomphante|libre|
|[class-14-RnxullWsNdbU7fuH.htm](feats/class-14-RnxullWsNdbU7fuH.htm)|Pivot Strike|Frappe pivotante|libre|
|[class-14-roItUHUbBqhHfwJr.htm](feats/class-14-roItUHUbBqhHfwJr.htm)|Seeker Arrow|Flèche chercheuse|libre|
|[class-14-rOx7r8ygmPHPC6qF.htm](feats/class-14-rOx7r8ygmPHPC6qF.htm)|Allegro|Allégro|officielle|
|[class-14-RpXWOgLWQLGdx74I.htm](feats/class-14-RpXWOgLWQLGdx74I.htm)|Sunder Enchantment|Destruction d'enchantement|libre|
|[class-14-S450JMWfF90oOcv9.htm](feats/class-14-S450JMWfF90oOcv9.htm)|Path Of Iron|Passage de fer|libre|
|[class-14-SA8rnheHFtjkATrJ.htm](feats/class-14-SA8rnheHFtjkATrJ.htm)|Shifting Terrain|Terrain changeant|libre|
|[class-14-sFAqKrzqXQhtrfqN.htm](feats/class-14-sFAqKrzqXQhtrfqN.htm)|Sixth Pillar Mastery|Maîtrise du sixième pilier|libre|
|[class-14-ST6AhCbEDSMxXf20.htm](feats/class-14-ST6AhCbEDSMxXf20.htm)|Perfect Ki Exemplar|Modèle de la perfection du ki|libre|
|[class-14-t3unBu3PX6AO0uIW.htm](feats/class-14-t3unBu3PX6AO0uIW.htm)|Swift Banishment|Bannissement rapide|officielle|
|[class-14-T3XFrLIBzir9IqD5.htm](feats/class-14-T3XFrLIBzir9IqD5.htm)|Extend Armament Alignment|Prolongement de l'arsenal aligné|officielle|
|[class-14-TdA3oVj79KxOm2Kd.htm](feats/class-14-TdA3oVj79KxOm2Kd.htm)|Wind-Tossed Spell|Sort balayé par le vent|libre|
|[class-14-TdwC9rTGgtF4CQ25.htm](feats/class-14-TdwC9rTGgtF4CQ25.htm)|Flamboyant Leap|Bond flamboyant|libre|
|[class-14-TNRB8IY6Wtk9BoMp.htm](feats/class-14-TNRB8IY6Wtk9BoMp.htm)|Terrain Shield|Terrain protecteur|libre|
|[class-14-TpWS2b9ISHnXVfZg.htm](feats/class-14-TpWS2b9ISHnXVfZg.htm)|Timeless Nature|Nature intemporelle|officielle|
|[class-14-TQCpXi1hwYX6VIhp.htm](feats/class-14-TQCpXi1hwYX6VIhp.htm)|Speedy Rituals|Rituels rapides|libre|
|[class-14-TyWFsX9DliAdAVs8.htm](feats/class-14-TyWFsX9DliAdAVs8.htm)|Sow Spell|Sort semé|libre|
|[class-14-UA3iZTAZrugKClKE.htm](feats/class-14-UA3iZTAZrugKClKE.htm)|Shatter Space|Briser l'espace|libre|
|[class-14-ubF6nGJrnfW7ocSg.htm](feats/class-14-ubF6nGJrnfW7ocSg.htm)|Graveshift|Déplacement par la tombe|libre|
|[class-14-UJcuACMlspc1raL1.htm](feats/class-14-UJcuACMlspc1raL1.htm)|Curse of the Saumen Kar|Malédiction du saumen kar|libre|
|[class-14-uPikeCzrTrgzEJT8.htm](feats/class-14-uPikeCzrTrgzEJT8.htm)|Talismanic Sage|Sage talismanique|libre|
|[class-14-UxurtbOOvCkngsKN.htm](feats/class-14-UxurtbOOvCkngsKN.htm)|Paragon Reanimated Companion|Parangon compagnon réanimé|libre|
|[class-14-UZpBsHE57rmS0x6S.htm](feats/class-14-UZpBsHE57rmS0x6S.htm)|Showstopper|Clou du spectacle|libre|
|[class-14-UzuMXY8G88ULDRex.htm](feats/class-14-UzuMXY8G88ULDRex.htm)|Disruptive Blur|Flou interruptif|libre|
|[class-14-VW0Tp5rJDRjaJuSn.htm](feats/class-14-VW0Tp5rJDRjaJuSn.htm)|Two-Weapon Fusillade|Fusillade à deux armes|libre|
|[class-14-Vzbu8tclrsS2IBYU.htm](feats/class-14-Vzbu8tclrsS2IBYU.htm)|Seize|Prise|libre|
|[class-14-w2v5LZmpJy0MBxo5.htm](feats/class-14-w2v5LZmpJy0MBxo5.htm)|Improved Twin Riposte (Fighter)|Riposte jumelée améliorée (Guerrier)|officielle|
|[class-14-wa9ZGBTlFuwOjPpH.htm](feats/class-14-wa9ZGBTlFuwOjPpH.htm)|Rites Of Transfiguration|Rites de transfiguration|libre|
|[class-14-wBL5h0hTmVD4EJLw.htm](feats/class-14-wBL5h0hTmVD4EJLw.htm)|Control Tower|Tour de contrôle|libre|
|[class-14-wdkbfWKEjAFXAxto.htm](feats/class-14-wdkbfWKEjAFXAxto.htm)|Determination|Détermination|officielle|
|[class-14-WEUFs37Ids3ZRrqa.htm](feats/class-14-WEUFs37Ids3ZRrqa.htm)|Litany Of Depravity|Litanie de dépravation|libre|
|[class-14-wijzB1FDUT7SC86a.htm](feats/class-14-wijzB1FDUT7SC86a.htm)|Specialized Mount|Monture spécialisée|libre|
|[class-14-WKRSBu7H9miAwUaR.htm](feats/class-14-WKRSBu7H9miAwUaR.htm)|Spirit Guide Form|Forme de guide spirituelle|libre|
|[class-14-wKx7BWdQu5sEjL9j.htm](feats/class-14-wKx7BWdQu5sEjL9j.htm)|Wild Winds Gust|Bourrasque des vents violents|officielle|
|[class-14-wnrlBkwVr8BSVAZt.htm](feats/class-14-wnrlBkwVr8BSVAZt.htm)|Sepulchral Sublimation|Sublimation sépulcrale|libre|
|[class-14-xdv75qkv5TOFlHmM.htm](feats/class-14-xdv75qkv5TOFlHmM.htm)|Keep up the Good Fight|Poursuite du combat pour le bien|libre|
|[class-14-Xk9inG3pln4UKbs3.htm](feats/class-14-Xk9inG3pln4UKbs3.htm)|True Hypercognition|Hypercognition ultime|officielle|
|[class-14-xSJaOcdhqDF1CBs3.htm](feats/class-14-xSJaOcdhqDF1CBs3.htm)|Ebb And Flow|Flux et reflux|libre|
|[class-14-XydqgTE4J119J5JV.htm](feats/class-14-XydqgTE4J119J5JV.htm)|Arcane Shroud|Éclat arcanique|libre|
|[class-14-YawVDUc9uzREIAnO.htm](feats/class-14-YawVDUc9uzREIAnO.htm)|Aura of Life|Aura de vie|officielle|
|[class-14-yeSyGnYDkl2GUNmu.htm](feats/class-14-yeSyGnYDkl2GUNmu.htm)|Stance Savant (Fighter)|Maître des postures (Guerrier)|officielle|
|[class-14-ygPrwqiyDr1frUHw.htm](feats/class-14-ygPrwqiyDr1frUHw.htm)|Greater Interpose|Interposition supérieure|libre|
|[class-14-z7kwVNaCB4oJs3Fe.htm](feats/class-14-z7kwVNaCB4oJs3Fe.htm)|Aura Of Preservation|Aura de préservation|libre|
|[class-14-zK0Dr1FZDOq2DMn8.htm](feats/class-14-zK0Dr1FZDOq2DMn8.htm)|Improved Hijack Undead|Détournement amélioré de mort-vivant|libre|
|[class-14-ZNq7Qgubi9gUqR0L.htm](feats/class-14-ZNq7Qgubi9gUqR0L.htm)|Bodysnatcher|Détrousseur de cadavre|libre|
|[class-14-ZTxiM8NExDmxHJDf.htm](feats/class-14-ZTxiM8NExDmxHJDf.htm)|Pin to the Spot|Épingler sur place|libre|
|[class-16-05k4nkjazjjEUoGu.htm](feats/class-16-05k4nkjazjjEUoGu.htm)|Blank Slate|Page blanche|officielle|
|[class-16-1re3J4hWW7raXIRB.htm](feats/class-16-1re3J4hWW7raXIRB.htm)|Spell Tinker|Bricolage de sort|officielle|
|[class-16-2HeRmbcHcsRMccir.htm](feats/class-16-2HeRmbcHcsRMccir.htm)|Diverse Mystery|Mystère diversifié|libre|
|[class-16-2LdncNMDNJW5Oeyu.htm](feats/class-16-2LdncNMDNJW5Oeyu.htm)|Opportune Throw|Lancer opportun|libre|
|[class-16-4caP26xpkQajkaDp.htm](feats/class-16-4caP26xpkQajkaDp.htm)|One-Millimeter Punch|Coup à infime distance|libre|
|[class-16-54JzsYCx3uoj7Wlz.htm](feats/class-16-54JzsYCx3uoj7Wlz.htm)|Monstrosity Shape|Morphologie monstrueuse|officielle|
|[class-16-5ZoIJImgvpdhGcDR.htm](feats/class-16-5ZoIJImgvpdhGcDR.htm)|Just the Thing!|Juste ce qu'il faut !|libre|
|[class-16-6AXAJuMEMpxU27PJ.htm](feats/class-16-6AXAJuMEMpxU27PJ.htm)|Dispelling Slice|Coupe dissipante|officielle|
|[class-16-6fojN9yBJByTZ1Q9.htm](feats/class-16-6fojN9yBJByTZ1Q9.htm)|Didactic Strike|Frappe didactique|libre|
|[class-16-7GGxxoYNA4YrtML9.htm](feats/class-16-7GGxxoYNA4YrtML9.htm)|Trample|Piétinement|libre|
|[class-16-7IDFHh2ZJLaB1y59.htm](feats/class-16-7IDFHh2ZJLaB1y59.htm)|Shield of Grace|Bouclier de grâce|officielle|
|[class-16-8C9qo5LL9J0UTNGc.htm](feats/class-16-8C9qo5LL9J0UTNGc.htm)|Courageous Onslaught|Charge vaillante|libre|
|[class-16-8HYfYT4fHtxXP199.htm](feats/class-16-8HYfYT4fHtxXP199.htm)|Greater Mental Evolution|Évolution mentale supérieure|officielle|
|[class-16-9wd9hhtfjayeZ6zF.htm](feats/class-16-9wd9hhtfjayeZ6zF.htm)|Giant Slayer|Tueur de géant|libre|
|[class-16-a6xUSibsRaclbSz3.htm](feats/class-16-a6xUSibsRaclbSz3.htm)|Ever-Vigilant Senses|Sens toujours en éveil|libre|
|[class-16-Aeea44xyJqYArFun.htm](feats/class-16-Aeea44xyJqYArFun.htm)|Lead The Pack|Mener la meute|libre|
|[class-16-AObopXQkCIsaBAID.htm](feats/class-16-AObopXQkCIsaBAID.htm)|Implement's Flight|Vol de l'implément|libre|
|[class-16-aoZYZm2PrTKEK0Ji.htm](feats/class-16-aoZYZm2PrTKEK0Ji.htm)|Converge|Converger|libre|
|[class-16-AV9NkSLNvv0UcdcP.htm](feats/class-16-AV9NkSLNvv0UcdcP.htm)|Breath of Hungry Death|Souffle de la mort affamée|libre|
|[class-16-aviDW7htxA77iarV.htm](feats/class-16-aviDW7htxA77iarV.htm)|Hair Trigger|Gâchette sensible|libre|
|[class-16-AyP23H3WJkEAIEKd.htm](feats/class-16-AyP23H3WJkEAIEKd.htm)|Mobile Magical Combat|Combat magique mobile|libre|
|[class-16-AYVf9MU8oo1QWbGv.htm](feats/class-16-AYVf9MU8oo1QWbGv.htm)|Wave the Flag|Agiter l'étendard|libre|
|[class-16-Bp02C07s4RTS4vsV.htm](feats/class-16-Bp02C07s4RTS4vsV.htm)|Reconstruct The Scene|Reconstituer la scène|libre|
|[class-16-bX2WI5k0afqPpCfm.htm](feats/class-16-bX2WI5k0afqPpCfm.htm)|Ubiquitous Snares|Profusion de pièges artisanaux|officielle|
|[class-16-Cgk4By6gEomD2bJ0.htm](feats/class-16-Cgk4By6gEomD2bJ0.htm)|Improved Twin Riposte (Ranger)|Riposte jumelée améliorée (Rôdeur)|officielle|
|[class-16-cHOALlKY1XsCj3Fe.htm](feats/class-16-cHOALlKY1XsCj3Fe.htm)|Disciple's Breath|Souffle du disciple|libre|
|[class-16-CtC7DM1poHDwwu52.htm](feats/class-16-CtC7DM1poHDwwu52.htm)|Fearsome Fangs|Crocs terrifiants|libre|
|[class-16-D71A2ZQfz9MVndqI.htm](feats/class-16-D71A2ZQfz9MVndqI.htm)|Felicitous Riposte|Riposte heureuse|libre|
|[class-16-DAFF7zJphhDPcAws.htm](feats/class-16-DAFF7zJphhDPcAws.htm)|Medusa's Wrath|Courroux de la méduse|libre|
|[class-16-Dr6h8WRW6xnLRfxr.htm](feats/class-16-Dr6h8WRW6xnLRfxr.htm)|Resurrectionist|Résurrecteur|officielle|
|[class-16-DSmYJvCHMvZCP0aD.htm](feats/class-16-DSmYJvCHMvZCP0aD.htm)|Scintillating Spell|Sort scintillant|libre|
|[class-16-E4xubBMtj81kX5Bk.htm](feats/class-16-E4xubBMtj81kX5Bk.htm)|Shattering Strike (Monk)|Frappe fracassante (Moine)|officielle|
|[class-16-e8ChYAaQ9Aa8aZES.htm](feats/class-16-e8ChYAaQ9Aa8aZES.htm)|Deadly Grace|Grace mortelle|libre|
|[class-16-EDdJFwarNIJIkP2E.htm](feats/class-16-EDdJFwarNIJIkP2E.htm)|Master's Counterspell|Contresort du maître|libre|
|[class-16-eOwjwVAbl99ZTy5D.htm](feats/class-16-eOwjwVAbl99ZTy5D.htm)|Overwhelming Blow|Coup surpuissant|libre|
|[class-16-EwCDbWg8yPxDWF4a.htm](feats/class-16-EwCDbWg8yPxDWF4a.htm)|Stunning Appearance|Apparition étourdissante|libre|
|[class-16-EWeso1zDkCLGlnsW.htm](feats/class-16-EWeso1zDkCLGlnsW.htm)|Reckless Abandon (Barbarian)|Dangereux abandon (Barbare)|libre|
|[class-16-Fai5VMyrtOrYC5JL.htm](feats/class-16-Fai5VMyrtOrYC5JL.htm)|Specialized Companion (Ranger)|Compagnon spécialisé (rôdeur)|officielle|
|[class-16-fEfEabn53bubYVVT.htm](feats/class-16-fEfEabn53bubYVVT.htm)|Improved Reflexive Shield|Bouclier instinctif amélioré|officielle|
|[class-16-fgnfXwFcn9jZlXGD.htm](feats/class-16-fgnfXwFcn9jZlXGD.htm)|Advanced Runic Mind Smithing|Façonnage spirituel de rune avancé|libre|
|[class-16-g1wBP9Z5HRqDe9FE.htm](feats/class-16-g1wBP9Z5HRqDe9FE.htm)|Quivering Palm|Paume vibratoire|officielle|
|[class-16-giOEclnMp8txkRSU.htm](feats/class-16-giOEclnMp8txkRSU.htm)|Eternal Elixir|Élixir éternel|officielle|
|[class-16-giupBd3dyOwdeoFl.htm](feats/class-16-giupBd3dyOwdeoFl.htm)|Performance Weapon Expert|Expert avec les armes de scène|libre|
|[class-16-GKP2dHkgQw1o0k8g.htm](feats/class-16-GKP2dHkgQw1o0k8g.htm)|Persistent Mutagen|Mutagène persistant|officielle|
|[class-16-gqISxjbTtZSYndZ4.htm](feats/class-16-gqISxjbTtZSYndZ4.htm)|Quickened Attunement|Harmonisation accélérée|libre|
|[class-16-h08Vfel5iIAARWdy.htm](feats/class-16-h08Vfel5iIAARWdy.htm)|Instrument of Zeal|Instrument de zèle|officielle|
|[class-16-H5uZqYVClk3s62ce.htm](feats/class-16-H5uZqYVClk3s62ce.htm)|Remediate|Rééquilibrer|libre|
|[class-16-H7Ocx80td7Sx7Cqn.htm](feats/class-16-H7Ocx80td7Sx7Cqn.htm)|Expand Aura|Aura étendue|libre|
|[class-16-hKemfam7Hvkllsgp.htm](feats/class-16-hKemfam7Hvkllsgp.htm)|Purge of Moments|Purge des moments|libre|
|[class-16-hOZP7qOMsNYOe90o.htm](feats/class-16-hOZP7qOMsNYOe90o.htm)|Wandering Thoughts|Pensées errantes|libre|
|[class-16-hr9maYUbtrNxpBPw.htm](feats/class-16-hr9maYUbtrNxpBPw.htm)|Exploitive Bomb|Bombe d'érosion|officielle|
|[class-16-i98NcWSAbWmNmBik.htm](feats/class-16-i98NcWSAbWmNmBik.htm)|Instrument Of Slaughter|Instrument du massacre|libre|
|[class-16-IA8p9oYmsCbipmhw.htm](feats/class-16-IA8p9oYmsCbipmhw.htm)|Chemical Contagion|Contagion chimique|libre|
|[class-16-IevdA9OVFZE6FOVK.htm](feats/class-16-IevdA9OVFZE6FOVK.htm)|Sever Magic|Couper la magie|libre|
|[class-16-JezNf3xbCi8h2qKe.htm](feats/class-16-JezNf3xbCi8h2qKe.htm)|Vessel's Form|Forme du réceptacle|libre|
|[class-16-jI4Eoi6m0ogjXkGK.htm](feats/class-16-jI4Eoi6m0ogjXkGK.htm)|Master Captivator Spellcasting|Incantation maître d'enjôleur|libre|
|[class-16-jSu9h6UvZtUJ7InD.htm](feats/class-16-jSu9h6UvZtUJ7InD.htm)|Fey Life|Vie féerique|libre|
|[class-16-kMLvQnx2vY7F3bjI.htm](feats/class-16-kMLvQnx2vY7F3bjI.htm)|Eternal Blessing|Bénédiction éternelle|officielle|
|[class-16-kVSZQvsz1cYjOwxL.htm](feats/class-16-kVSZQvsz1cYjOwxL.htm)|Master Siege Engineer|Maître ingénieur de siège|libre|
|[class-16-L0f3c0DkT7FLQF9W.htm](feats/class-16-L0f3c0DkT7FLQF9W.htm)|Siphon Power|Pouvoir siphonné|libre|
|[class-16-LbMPCAyUrLRHykGA.htm](feats/class-16-LbMPCAyUrLRHykGA.htm)|Persistent Boost|Renfort persistant|libre|
|[class-16-lgEihn7deZwHczGE.htm](feats/class-16-lgEihn7deZwHczGE.htm)|Dragon Transformation|Transformation en dragon|officielle|
|[class-16-lhSqWHXK1JShUabF.htm](feats/class-16-lhSqWHXK1JShUabF.htm)|Resounding Finale|Final retentissant|libre|
|[class-16-LiLnDkoFcwW1RxqZ.htm](feats/class-16-LiLnDkoFcwW1RxqZ.htm)|Withstand Death|Résister la mort|libre|
|[class-16-lleedxE6fTDSK6og.htm](feats/class-16-lleedxE6fTDSK6og.htm)|Resuscitate|Ressusciter|libre|
|[class-16-LRSTjjBNNlD0XZX8.htm](feats/class-16-LRSTjjBNNlD0XZX8.htm)|Phase Arrow|Flèche de phase|libre|
|[class-16-mB7ENhF10qoOwQyG.htm](feats/class-16-mB7ENhF10qoOwQyG.htm)|Resounding Cascade|Cascade retentissante|libre|
|[class-16-mDczSj3GolIbkhGJ.htm](feats/class-16-mDczSj3GolIbkhGJ.htm)|Instant Return|Retour instantané|libre|
|[class-16-MGtlUc6cy4nCM4Lk.htm](feats/class-16-MGtlUc6cy4nCM4Lk.htm)|Bound in Ice|Lié à la glace|libre|
|[class-16-Mix4IwZRuCz7JS3T.htm](feats/class-16-Mix4IwZRuCz7JS3T.htm)|Greater Snow Step|Marche sur la neige supérieure|libre|
|[class-16-mj1pVVFtqGLKgCQM.htm](feats/class-16-mj1pVVFtqGLKgCQM.htm)|Genius Mutagen|Mutagène de génie|officielle|
|[class-16-MjhCcwGKyI5dpNIY.htm](feats/class-16-MjhCcwGKyI5dpNIY.htm)|Perfect Distraction|Diversion parfaite|officielle|
|[class-16-MUwIeDV0pBtqeU3p.htm](feats/class-16-MUwIeDV0pBtqeU3p.htm)|Walk on the Wind|Marche sur le vent|libre|
|[class-16-ND3nKsXCDBShUgYc.htm](feats/class-16-ND3nKsXCDBShUgYc.htm)|Greater Vital Evolution|Évolution vitale supérieure|officielle|
|[class-16-Ndxu9YVGgenYmZSb.htm](feats/class-16-Ndxu9YVGgenYmZSb.htm)|Dispelling Spellstrike|Frappe de sort dissipante|libre|
|[class-16-Oyml3OGNy468z3XI.htm](feats/class-16-Oyml3OGNy468z3XI.htm)|Furious Vengeance|Vengeance furieuse|libre|
|[class-16-PiKWzPloAR2nqIcC.htm](feats/class-16-PiKWzPloAR2nqIcC.htm)|Constant Levitation|Lévitation constante|libre|
|[class-16-pzmob1HqVKZfL0BY.htm](feats/class-16-pzmob1HqVKZfL0BY.htm)|Eternal Bane|Imprécation éternelle|officielle|
|[class-16-QGpcyvIezLMgmTia.htm](feats/class-16-QGpcyvIezLMgmTia.htm)|Studious Capacity|Grande capacité d'étude|officielle|
|[class-16-qkHVva51N6H8NNGR.htm](feats/class-16-qkHVva51N6H8NNGR.htm)|Ricochet Master|Maître du ricochet|libre|
|[class-16-qwnyuCsl5YvX0fdY.htm](feats/class-16-qwnyuCsl5YvX0fdY.htm)|Fatal Bullet|Cartouche fatale|libre|
|[class-16-qX9ZtfaAj6rxrVA7.htm](feats/class-16-qX9ZtfaAj6rxrVA7.htm)|Master of Many Styles|Maître des multiples postures|officielle|
|[class-16-qZzRXAa9mNQPUXoW.htm](feats/class-16-qZzRXAa9mNQPUXoW.htm)|Controlled Bullet|Balle controlée|libre|
|[class-16-r7FGPKl5e0xB4tuj.htm](feats/class-16-r7FGPKl5e0xB4tuj.htm)|Cloud Step|Pas nuageux|officielle|
|[class-16-rFaUJtB46scuAidY.htm](feats/class-16-rFaUJtB46scuAidY.htm)|Graceful Poise|Aisance gracieuse|officielle|
|[class-16-rgs6OZJYCgi5At8J.htm](feats/class-16-rgs6OZJYCgi5At8J.htm)|Effortless Concentration|Concentration aisée|officielle|
|[class-16-RL7faGkymMFLAqTU.htm](feats/class-16-RL7faGkymMFLAqTU.htm)|Shattering Blows|Coups fracassants|libre|
|[class-16-RMSVWMFoLUk0P1cC.htm](feats/class-16-RMSVWMFoLUk0P1cC.htm)|Terraforming Spell|Sort de terraformage|libre|
|[class-16-ROnjdPMvH0vkkWjQ.htm](feats/class-16-ROnjdPMvH0vkkWjQ.htm)|Enlightened Presence|Présence éclairée|officielle|
|[class-16-RqccUKf8DCPnsYXJ.htm](feats/class-16-RqccUKf8DCPnsYXJ.htm)|Implausible Purchase (Investigator)|Achat improbable (Enquêteur)|libre|
|[class-16-rRbMOxX1QTHIIwAi.htm](feats/class-16-rRbMOxX1QTHIIwAi.htm)|Portentous Spell|Sort de mauvais augure|libre|
|[class-16-Ruee0G3oZG5n5Auk.htm](feats/class-16-Ruee0G3oZG5n5Auk.htm)|Master Skysage Divination|Divination maître du Sagecéleste|libre|
|[class-16-RzhnxgiAopWILCvs.htm](feats/class-16-RzhnxgiAopWILCvs.htm)|Multishot Stance|Posture de tirs multiples|officielle|
|[class-16-S8Hda5OtajS9gpqM.htm](feats/class-16-S8Hda5OtajS9gpqM.htm)|Ward Mind|Protection de l'esprit|libre|
|[class-16-SGgK4BoUooA0HhTj.htm](feats/class-16-SGgK4BoUooA0HhTj.htm)|Avalanche Strike|Frappe avalanche|libre|
|[class-16-tBMMKWYfzD2OMD30.htm](feats/class-16-tBMMKWYfzD2OMD30.htm)|Penetrating Projectile|Projectile pénétrant|libre|
|[class-16-TqODgJL1VS3eokhX.htm](feats/class-16-TqODgJL1VS3eokhX.htm)|Swift Elusion|Échappatoire rapide|libre|
|[class-16-UgZDirfFb4CFhuWh.htm](feats/class-16-UgZDirfFb4CFhuWh.htm)|Seven-Part Link|Lien en sept parties|libre|
|[class-16-uixN9wbfe0veOHRn.htm](feats/class-16-uixN9wbfe0veOHRn.htm)|Auspicious Mount|Monture de bon augure|officielle|
|[class-16-uL6q4wtwvuP8I4po.htm](feats/class-16-uL6q4wtwvuP8I4po.htm)|Flinging Blow|Coup catapultant|libre|
|[class-16-urYHB6VHhqvPMSy7.htm](feats/class-16-urYHB6VHhqvPMSy7.htm)|Impaling Briars|Ronces empaleuses|officielle|
|[class-16-vDJRIKS27md3LudA.htm](feats/class-16-vDJRIKS27md3LudA.htm)|Greater Distracting Shot|Tir déroutant supérieur|officielle|
|[class-16-Veyt4x2Kb5YcPGTv.htm](feats/class-16-Veyt4x2Kb5YcPGTv.htm)|Legendary Monster Hunter|Chasseur de monstres légendaires|officielle|
|[class-16-VYdZmTifZRkRF7ey.htm](feats/class-16-VYdZmTifZRkRF7ey.htm)|Deflecting Cloud|Nuage déflecteur|libre|
|[class-16-w1GFspPIQ9f5MbFL.htm](feats/class-16-w1GFspPIQ9f5MbFL.htm)|Touch Focus|Contact focalisé|libre|
|[class-16-WoUwDqhA6i6abwen.htm](feats/class-16-WoUwDqhA6i6abwen.htm)|Song of Grace and Speed|Chanson de grâce et de rapidité|libre|
|[class-16-wPJFEUOXwf7y5jN3.htm](feats/class-16-wPJFEUOXwf7y5jN3.htm)|Cognitive Loophole|Échappatoire cognitive|officielle|
|[class-16-wwvz79Hwp9qx96lL.htm](feats/class-16-wwvz79Hwp9qx96lL.htm)|Target of Psychic Ire|Cible de colère psychique|libre|
|[class-16-WY7CjISdz6uwXwIb.htm](feats/class-16-WY7CjISdz6uwXwIb.htm)|Collateral Thrash|Maltraitance collatérale|officielle|
|[class-16-X5LxcrZbjHfmX58a.htm](feats/class-16-X5LxcrZbjHfmX58a.htm)|Bolster Soul Cage|Consolider le phylactère|libre|
|[class-16-xgg9eNF6MsJPMjyH.htm](feats/class-16-xgg9eNF6MsJPMjyH.htm)|Stone Body|Corps de pierre|libre|
|[class-16-xjLbabfyQzBNT4y1.htm](feats/class-16-xjLbabfyQzBNT4y1.htm)|Twinned Defense (Fighter)|Défense jumelée (Guerrier)|officielle|
|[class-16-xjwlP306nuda2z03.htm](feats/class-16-xjwlP306nuda2z03.htm)|Steal Spell|Sort volé|libre|
|[class-16-XpZkzUV9PwUHvmyq.htm](feats/class-16-XpZkzUV9PwUHvmyq.htm)|Fulminating Synergy|Synergie fulminante|libre|
|[class-16-XRCqj74dG27MHNxQ.htm](feats/class-16-XRCqj74dG27MHNxQ.htm)|Spell Gem|Gemme de sort|libre|
|[class-16-XXOc3MxbkEBMgFeT.htm](feats/class-16-XXOc3MxbkEBMgFeT.htm)|Cross the Threshold|Franchir le seuil|libre|
|[class-16-xYNMWGEmpbtrtWXQ.htm](feats/class-16-xYNMWGEmpbtrtWXQ.htm)|Unwind Death|Remonter la mort|libre|
|[class-16-yqIorA6QGWmbKoOz.htm](feats/class-16-yqIorA6QGWmbKoOz.htm)|Dominating Gaze|Regard dominateur|libre|
|[class-16-zBS1qFyIpFuCGhWW.htm](feats/class-16-zBS1qFyIpFuCGhWW.htm)|You Failed to Account For... This!|Vous n'avez pas tenu compte... de çà !|libre|
|[class-16-zfnZki2CxmZXdNBO.htm](feats/class-16-zfnZki2CxmZXdNBO.htm)|Electric Counter|Contrer l'électricité|libre|
|[class-16-zodscjgydZRUSOLO.htm](feats/class-16-zodscjgydZRUSOLO.htm)|Focus Ally|Allié reconcentré|libre|
|[class-16-ZXLbIgL3NNplgnuB.htm](feats/class-16-ZXLbIgL3NNplgnuB.htm)|Shared Dream|Rêve partagé|libre|
|[class-18-218WeRqUqdnguRdS.htm](feats/class-18-218WeRqUqdnguRdS.htm)|Master Druid Spellcasting|Incantation maître du druide|officielle|
|[class-18-2RaDe6Fi4t7S2IDF.htm](feats/class-18-2RaDe6Fi4t7S2IDF.htm)|Invoke Disaster|Invocation de désastre|officielle|
|[class-18-5fd95NhJJ55hm0Qt.htm](feats/class-18-5fd95NhJJ55hm0Qt.htm)|Link Wellspring|Source liée|libre|
|[class-18-5JG0kjxukERBeayd.htm](feats/class-18-5JG0kjxukERBeayd.htm)|Frightful Aura|Aura effrayante|libre|
|[class-18-5NWZct5OvSiVvMn8.htm](feats/class-18-5NWZct5OvSiVvMn8.htm)|Master Cathartic Spellcasting|Incantation cathartique maître|libre|
|[class-18-62glnJI2o0KnHULB.htm](feats/class-18-62glnJI2o0KnHULB.htm)|Triangle Shot|Tir triangulé|libre|
|[class-18-6A1WTVY1PUlXy3rW.htm](feats/class-18-6A1WTVY1PUlXy3rW.htm)|Versatile Spellstrike|Frappe de sort polyvalente|libre|
|[class-18-6GUl9WG7OKvfVQo4.htm](feats/class-18-6GUl9WG7OKvfVQo4.htm)|Empty Body|Désertion de l'âme|officielle|
|[class-18-6iOLxitjqHujH1Tj.htm](feats/class-18-6iOLxitjqHujH1Tj.htm)|Arrow of Death|Flèche de mort|libre|
|[class-18-6kz7FPdxDrsPiNti.htm](feats/class-18-6kz7FPdxDrsPiNti.htm)|Master Eldritch Archer Spellcasting|Incantation maître de l'archer mystique|libre|
|[class-18-6qNRVKwbnX381jVj.htm](feats/class-18-6qNRVKwbnX381jVj.htm)|Master Beast Gunner Spellcasting|Incantation maître de bestioléro|libre|
|[class-18-72qOqbwShnT2Apaw.htm](feats/class-18-72qOqbwShnT2Apaw.htm)|Implement's Assault|Assaut implémentaire|libre|
|[class-18-7ATVpDUM6pRq6HOR.htm](feats/class-18-7ATVpDUM6pRq6HOR.htm)|Smash From The Air|Fracasser en l'air|libre|
|[class-18-7mGMHb7irGKZ0eQo.htm](feats/class-18-7mGMHb7irGKZ0eQo.htm)|Miraculous Possibility|Possibilité miraculeuse|libre|
|[class-18-7uofkNynjFy3ofk2.htm](feats/class-18-7uofkNynjFy3ofk2.htm)|Ever Dreaming|Rêve sans fin|libre|
|[class-18-8JMOgtB3XG7o6ffW.htm](feats/class-18-8JMOgtB3XG7o6ffW.htm)|Discordant Voice|Voix discordante|libre|
|[class-18-9v3wpgvEEiZHO0SJ.htm](feats/class-18-9v3wpgvEEiZHO0SJ.htm)|Perfect Ki Grandmaster|Grand maître du ki parfait|libre|
|[class-18-a1TSGGsA6b5gjP3H.htm](feats/class-18-a1TSGGsA6b5gjP3H.htm)|Reprepare Spell|Recyclage de sort|officielle|
|[class-18-AbjVJIBdNjbQbVnV.htm](feats/class-18-AbjVJIBdNjbQbVnV.htm)|Primal Aegis|Égide primordiale|libre|
|[class-18-AJRWIcBGCIxXG0RS.htm](feats/class-18-AJRWIcBGCIxXG0RS.htm)|Master Magus Spellcasting|Incantation maître de magus|libre|
|[class-18-aoIEVpJrQEold2Mi.htm](feats/class-18-aoIEVpJrQEold2Mi.htm)|Magical Master|Maître magique|libre|
|[class-18-AwxJcaIrutqMcUC8.htm](feats/class-18-AwxJcaIrutqMcUC8.htm)|Masterful Companion|Compagnon magistral|officielle|
|[class-18-bc6tcXSzakyCbQsS.htm](feats/class-18-bc6tcXSzakyCbQsS.htm)|Master Cleric Spellcasting|Incantation maître du prêtre|officielle|
|[class-18-blOiU4LPlBjVHcgR.htm](feats/class-18-blOiU4LPlBjVHcgR.htm)|Look Again|Regarde mieux|libre|
|[class-18-BqcAwmNjDlKEI84X.htm](feats/class-18-BqcAwmNjDlKEI84X.htm)|Shadow Master|Maître de l'ombre|libre|
|[class-18-BtMemftiktI0Mn6X.htm](feats/class-18-BtMemftiktI0Mn6X.htm)|Manifold Edge (Precision)|Avantages multiples (Précision)|libre|
|[class-18-C9nb7H5u2ElBXvCR.htm](feats/class-18-C9nb7H5u2ElBXvCR.htm)|Mindblank Mutagen|Mutagène d'esprit vide|officielle|
|[class-18-Cs0hRKBfWn2gOYzK.htm](feats/class-18-Cs0hRKBfWn2gOYzK.htm)|Devastating Weaponry|Armement dévastateur|libre|
|[class-18-Dcr63tofZUome1Ze.htm](feats/class-18-Dcr63tofZUome1Ze.htm)|Manifold Edge|Avantages multiples|libre|
|[class-18-DFY3X7Mgl9rESQuu.htm](feats/class-18-DFY3X7Mgl9rESQuu.htm)|Mighty Dragon Shape|Puissante forme de dragon|libre|
|[class-18-doD3jZylVXZ0oHWO.htm](feats/class-18-doD3jZylVXZ0oHWO.htm)|Greater Crossblooded Evolution|Évolution métisée supérieure|officielle|
|[class-18-EcQvuGYDJdqTfGuX.htm](feats/class-18-EcQvuGYDJdqTfGuX.htm)|Manifold Edge (Outwit)|Avantages multiples (Ruse)|officielle|
|[class-18-eJNzP21lFPV3zWkm.htm](feats/class-18-eJNzP21lFPV3zWkm.htm)|Master Bard Spellcasting|Incantation maître du barde|officielle|
|[class-18-f1acyuIGYVp2BpKc.htm](feats/class-18-f1acyuIGYVp2BpKc.htm)|Shadow Hunter|Chasseur de l'ombre|officielle|
|[class-18-fLrwddS607eRFfHA.htm](feats/class-18-fLrwddS607eRFfHA.htm)|Implausible Infiltration|Infiltration invraisemblable|officielle|
|[class-18-fyJ2slL98hnQH3On.htm](feats/class-18-fyJ2slL98hnQH3On.htm)|Soaring Dynamo|Dynamo volante|libre|
|[class-18-G8iGrx1qBDwLk1HO.htm](feats/class-18-G8iGrx1qBDwLk1HO.htm)|Piercing Critical|Critique perforant|libre|
|[class-18-GLHh9MWlcINLC6Q0.htm](feats/class-18-GLHh9MWlcINLC6Q0.htm)|Engine of Destruction|Machine de destruction|libre|
|[class-18-gtXy5gMNU0NDvkBL.htm](feats/class-18-gtXy5gMNU0NDvkBL.htm)|Fiendish Form|Forme fiélone|libre|
|[class-18-GWnQMjSAIG6qcV8J.htm](feats/class-18-GWnQMjSAIG6qcV8J.htm)|Reach for the Stars|Atteindre les étoiles|libre|
|[class-18-HBhLR980Q0cb2rxp.htm](feats/class-18-HBhLR980Q0cb2rxp.htm)|Perfect Debilitation|Neutralisation parfaite|officielle|
|[class-18-hdt3RHZljLrO49kq.htm](feats/class-18-hdt3RHZljLrO49kq.htm)|Specialized Companion (Animal Trainer)|Compagnon spécialisé (dompteur)|libre|
|[class-18-Hzzf7bi8xBMi6DCL.htm](feats/class-18-Hzzf7bi8xBMi6DCL.htm)|Powerful Sneak|Puissante furtivité|officielle|
|[class-18-i3hALsjbjk9FdbGN.htm](feats/class-18-i3hALsjbjk9FdbGN.htm)|Vicious Evisceration|Éviscération vicieuse|libre|
|[class-18-i7ibUqJCl1GXRFEa.htm](feats/class-18-i7ibUqJCl1GXRFEa.htm)|Master Witch Spellcasting|Incantation maître du sorcier|libre|
|[class-18-iTtnN49D8ZJ2Ilur.htm](feats/class-18-iTtnN49D8ZJ2Ilur.htm)|Deep Lore|Savoir approfondi|officielle|
|[class-18-iuLLw1X5RjRcR4rH.htm](feats/class-18-iuLLw1X5RjRcR4rH.htm)|Halcyon Spellcasting Sage|Sage de l'incantation syncrétique|libre|
|[class-18-IxggfXunfldeVOsQ.htm](feats/class-18-IxggfXunfldeVOsQ.htm)|Swift River|Libre comme l'eau|libre|
|[class-18-J8HLcsOkAcXfTxYy.htm](feats/class-18-J8HLcsOkAcXfTxYy.htm)|Negate Damage|Supprimer les dégâts|libre|
|[class-18-JkQjKyzfhMWLr9Gs.htm](feats/class-18-JkQjKyzfhMWLr9Gs.htm)|Perfect Clarity|Lucidité parfaite|officielle|
|[class-18-JSkaMCO6pzKYZrZe.htm](feats/class-18-JSkaMCO6pzKYZrZe.htm)|Unerring Shot|Tir infaillible|libre|
|[class-18-K9hM3AdWGbU3VE8L.htm](feats/class-18-K9hM3AdWGbU3VE8L.htm)|Final Shot|Tir final|libre|
|[class-18-KpF7RenGBXIMMGPX.htm](feats/class-18-KpF7RenGBXIMMGPX.htm)|Split Hex|Maléfice divisé|libre|
|[class-18-kPsJIz19viZy8YjO.htm](feats/class-18-kPsJIz19viZy8YjO.htm)|Master Summoning Spellcasting|Incantation maître du conjurateur|libre|
|[class-18-kwz8el0Z8kK0D5az.htm](feats/class-18-kwz8el0Z8kK0D5az.htm)|True Transmogrification|Véritable transmogrification|libre|
|[class-18-Ky7ZEtt4TchQNmFc.htm](feats/class-18-Ky7ZEtt4TchQNmFc.htm)|Timeline-Splitting Spell|Sort scindant la ligne temporelle|libre|
|[class-18-kYLtH4PYfvqruLyo.htm](feats/class-18-kYLtH4PYfvqruLyo.htm)|All in Your Head|Tout dans votre tête|libre|
|[class-18-L5n4PvQYhpl2WM9e.htm](feats/class-18-L5n4PvQYhpl2WM9e.htm)|Impossible Polymath|Touche-à-tout invraisemblable|libre|
|[class-18-LAHiW98iPJKplFyK.htm](feats/class-18-LAHiW98iPJKplFyK.htm)|Infinite Possibilities|Possibilités infinies|officielle|
|[class-18-LHjPTV5vP3MOsPPJ.htm](feats/class-18-LHjPTV5vP3MOsPPJ.htm)|Grand Scroll Esoterica|Ésotérica parcheminée grandiose|libre|
|[class-18-lIg5Gzz7W70jfbk1.htm](feats/class-18-lIg5Gzz7W70jfbk1.htm)|Master Scroll Cache|Réserve maîtresse de parchemins|libre|
|[class-18-LKxAuH0mLyzNygIY.htm](feats/class-18-LKxAuH0mLyzNygIY.htm)|Hex Wellspring|Source de maléfice|libre|
|[class-18-LLCf2xPXA7FVQT1D.htm](feats/class-18-LLCf2xPXA7FVQT1D.htm)|Effortless Reach|Allonge sans effort|libre|
|[class-18-m2pHkmyGvkwqfSSN.htm](feats/class-18-m2pHkmyGvkwqfSSN.htm)|Miracle Worker|Fabricateur de miracles|officielle|
|[class-18-mCXoiMLAbGHGsZS3.htm](feats/class-18-mCXoiMLAbGHGsZS3.htm)|Improved Swift Banishment|Bannissement rapide amélioré|officielle|
|[class-18-NAgaDfwUSjPAon4o.htm](feats/class-18-NAgaDfwUSjPAon4o.htm)|Master Spellcasting|Incantateur maître|libre|
|[class-18-nLidn7L2z61Ktjzk.htm](feats/class-18-nLidn7L2z61Ktjzk.htm)|Blaze Of Revelation|Brasier de révélation|libre|
|[class-18-nnsoFOtuHnpz2QHc.htm](feats/class-18-nnsoFOtuHnpz2QHc.htm)|Improbable Elixirs|Élixirs improbables|officielle|
|[class-18-NvEYf0jIETEu2LtP.htm](feats/class-18-NvEYf0jIETEu2LtP.htm)|Echoing Spell|Sort en écho|libre|
|[class-18-NY2AkQscVIHEC8hQ.htm](feats/class-18-NY2AkQscVIHEC8hQ.htm)|Impossible Volley (Fighter)|Volée invraisemblable (Guerrier)|officielle|
|[class-18-o1Vo4PvU09UY8sj7.htm](feats/class-18-o1Vo4PvU09UY8sj7.htm)|Conflux Wellspring|Source de confluence|libre|
|[class-18-oNHmfpe8ezZ3eKDD.htm](feats/class-18-oNHmfpe8ezZ3eKDD.htm)|Brutal Critical|Critique brutal|officielle|
|[class-18-OqHfUQQorVBkx34j.htm](feats/class-18-OqHfUQQorVBkx34j.htm)|Impossible Flurry|Déluge invraisemblable|officielle|
|[class-18-ouKDey5RHQKN9YBT.htm](feats/class-18-ouKDey5RHQKN9YBT.htm)|Domain Wellspring|Source du domaine|officielle|
|[class-18-oYFsw3LdgsN2QKVs.htm](feats/class-18-oYFsw3LdgsN2QKVs.htm)|Ki Form|Forme ki|libre|
|[class-18-phD0PbElkEeldZ2U.htm](feats/class-18-phD0PbElkEeldZ2U.htm)|Master Sorcerer Spellcasting|Incantation maître de l'ensorceleur|officielle|
|[class-18-PMckhnGYMyiwUNiL.htm](feats/class-18-PMckhnGYMyiwUNiL.htm)|Master Wizard Spellcasting|Incantation maître du magicien|officielle|
|[class-18-POrE3ZgBRdBL9MsW.htm](feats/class-18-POrE3ZgBRdBL9MsW.htm)|Trickster's Ace|Atout du mystificateur|libre|
|[class-18-pWbNhfBiskV4n58a.htm](feats/class-18-pWbNhfBiskV4n58a.htm)|Black Powder Embodiment|Incorporation à la poudre noire|libre|
|[class-18-PzbfpOQXOjpUWKkL.htm](feats/class-18-PzbfpOQXOjpUWKkL.htm)|Master Psychic Spellcasting|Incantation maître du psychiste|libre|
|[class-18-QWPFf6NOObxaZJwW.htm](feats/class-18-QWPFf6NOObxaZJwW.htm)|Manifold Edge (Flurry)|Avantages multiples (Déluge)|officielle|
|[class-18-Rb16bcCiovwRqVgN.htm](feats/class-18-Rb16bcCiovwRqVgN.htm)|Ultimate Mercy|Soulagement suprême|officielle|
|[class-18-Rbp08BSSzwpkWVjh.htm](feats/class-18-Rbp08BSSzwpkWVjh.htm)|Terrifying Countenance|Contenance terrifiante|libre|
|[class-18-rmO7FP410nvCjFBB.htm](feats/class-18-rmO7FP410nvCjFBB.htm)|Stave off Catastrophe|Éviter la catastrophe|libre|
|[class-18-S3hN7qK7aiCDTrpM.htm](feats/class-18-S3hN7qK7aiCDTrpM.htm)|Retain Absorbed Spell|Absorption de sort maintenu|libre|
|[class-18-SlXLsuxBHeUyUPII.htm](feats/class-18-SlXLsuxBHeUyUPII.htm)|Mighty Wings|Ailes puissantes|libre|
|[class-18-syxJQ48bxE8NY91a.htm](feats/class-18-syxJQ48bxE8NY91a.htm)|Master Snowcasting|Maître de l'incantation neigeuse|libre|
|[class-18-SzWeWBuzg3e0k98A.htm](feats/class-18-SzWeWBuzg3e0k98A.htm)|Primal Wellspring|Source primordiale|officielle|
|[class-18-t1sM6Xj9T07fqpwN.htm](feats/class-18-t1sM6Xj9T07fqpwN.htm)|Divine Effusion|Effusion divine|libre|
|[class-18-T4Xm8vYtnGMOM0Cw.htm](feats/class-18-T4Xm8vYtnGMOM0Cw.htm)|Echoing Channel|Canalisation répétée|officielle|
|[class-18-uAdnQZSkUuxcpEwz.htm](feats/class-18-uAdnQZSkUuxcpEwz.htm)|Incredible Luck (Swashbuckler)|Chance incroyable (Bretteur)|libre|
|[class-18-uIL1pbp7jhYMjYLS.htm](feats/class-18-uIL1pbp7jhYMjYLS.htm)|Parry And Riposte|Parade et riposte|libre|
|[class-18-v22FQuw17Dlr1b3x.htm](feats/class-18-v22FQuw17Dlr1b3x.htm)|Miraculous Flight|Vol miraculeux|libre|
|[class-18-v88bFLoJEF3YfJKb.htm](feats/class-18-v88bFLoJEF3YfJKb.htm)|Savage Critical|Critique violent|officielle|
|[class-18-VAbfepohLNtubfi3.htm](feats/class-18-VAbfepohLNtubfi3.htm)|Celestial Form|Forme céleste|officielle|
|[class-18-vCsprcXVTwgtUYAZ.htm](feats/class-18-vCsprcXVTwgtUYAZ.htm)|Cranial Detonation|Détonation crânienne|libre|
|[class-18-W1CthcTSbsmOo7lP.htm](feats/class-18-W1CthcTSbsmOo7lP.htm)|Impossible Volley (Ranger)|Volée invraisemblable (Rôdeur)|officielle|
|[class-18-wBCt1wcgrjduwHbi.htm](feats/class-18-wBCt1wcgrjduwHbi.htm)|Warden's Wellspring|Source du gardien|libre|
|[class-18-wIRZDXOacqWfI670.htm](feats/class-18-wIRZDXOacqWfI670.htm)|Ki Center|Centre du ki|libre|
|[class-18-wqAdzjRUOvTpKFKq.htm](feats/class-18-wqAdzjRUOvTpKFKq.htm)|All In My Head|C'est ton imagination|libre|
|[class-18-XafnXTx9Bn0kN1RG.htm](feats/class-18-XafnXTx9Bn0kN1RG.htm)|Second Chance Spell|Sort de la seconde chance|libre|
|[class-18-xAIUuSw5A85XEStY.htm](feats/class-18-xAIUuSw5A85XEStY.htm)|Perfect Form Control|Contrôle parfait de la forme|officielle|
|[class-18-XfCPAoNdF2XMnH7K.htm](feats/class-18-XfCPAoNdF2XMnH7K.htm)|Lethal Finisher|Aboutissement létal|libre|
|[class-18-Xlz96xFxueCk47pb.htm](feats/class-18-Xlz96xFxueCk47pb.htm)|Deepest Wellspring|Source la plus profonde|libre|
|[class-18-xZrTjUub7V09sXZF.htm](feats/class-18-xZrTjUub7V09sXZF.htm)|Rejuvenating Touch|Toucher rajeunissant|libre|
|[class-18-Y5irKSCSBn8z1Qgx.htm](feats/class-18-Y5irKSCSBn8z1Qgx.htm)|Meditative Wellspring|Source méditative|officielle|
|[class-18-YaJ1JTBMhpu1RWXV.htm](feats/class-18-YaJ1JTBMhpu1RWXV.htm)|Mighty Bear|Ours puissant|libre|
|[class-18-ybrx1nsg5J0L8d3j.htm](feats/class-18-ybrx1nsg5J0L8d3j.htm)|Shared Clarity|Clarté partagée|libre|
|[class-18-yFoBVSOCnC2R2r8s.htm](feats/class-18-yFoBVSOCnC2R2r8s.htm)|Eternal Composition|Composition perpétuelle|officielle|
|[class-18-yRRM1dsY6jakEMaC.htm](feats/class-18-yRRM1dsY6jakEMaC.htm)|Intense Implement|Implément intense|libre|
|[class-18-yUuvixlhM4mcjKMb.htm](feats/class-18-yUuvixlhM4mcjKMb.htm)|Bloodline Wellspring|Source du lignage|officielle|
|[class-18-YWRdk9oGMY6WmgIC.htm](feats/class-18-YWRdk9oGMY6WmgIC.htm)|Lead Investigator|Enquêteur principal|libre|
|[class-18-ZB2WMbhALgQTGY3c.htm](feats/class-18-ZB2WMbhALgQTGY3c.htm)|School Spell Redirection|Redirection de sort d'école|libre|
|[class-18-zCASpQconMmtJKQN.htm](feats/class-18-zCASpQconMmtJKQN.htm)|Implausible Purchase (Rogue)|Achat improbable (Roublard)|libre|
|[class-18-ZkGhDEpqe7fzJuSr.htm](feats/class-18-ZkGhDEpqe7fzJuSr.htm)|Master Oracle Spellcasting|Incantation maître de l'oracle|libre|
|[class-18-zrIrpVOvbGS6a3ux.htm](feats/class-18-zrIrpVOvbGS6a3ux.htm)|Perfect Shot|Tir parfait|officielle|
|[class-18-zTulA4sVXwLRm28Z.htm](feats/class-18-zTulA4sVXwLRm28Z.htm)|Diamond Fists|Poings de diamant|officielle|
|[class-20-0208T5UrkTY2ombM.htm](feats/class-20-0208T5UrkTY2ombM.htm)|Perfect Mutagen|Mutagène parfait|officielle|
|[class-20-0VUx8g8GJzuxvLSa.htm](feats/class-20-0VUx8g8GJzuxvLSa.htm)|Steal Essence|Essence dérobée|libre|
|[class-20-1BAJxKpeQc8xGaxZ.htm](feats/class-20-1BAJxKpeQc8xGaxZ.htm)|All the Time in the World|Tout le temps du monde|libre|
|[class-20-1sD5Gu8jQL09Yz2j.htm](feats/class-20-1sD5Gu8jQL09Yz2j.htm)|Sever Space|Couper la distance|libre|
|[class-20-1ul2dasQBdlaMEC5.htm](feats/class-20-1ul2dasQBdlaMEC5.htm)|Heart of the Kaiju|Coeur de kaiju|libre|
|[class-20-20Yax5lEqjftKBHZ.htm](feats/class-20-20Yax5lEqjftKBHZ.htm)|Vigilant Mask|Masque vigilant|libre|
|[class-20-2FBZ0apnmZ7b61ct.htm](feats/class-20-2FBZ0apnmZ7b61ct.htm)|Efficient Alchemy (Paragon)|Alchimie efficiente (Parangon)|libre|
|[class-20-2sCzFjq8sKvBR3Jh.htm](feats/class-20-2sCzFjq8sKvBR3Jh.htm)|Ultimate Skirmisher|Escarmoucheur suprême|officielle|
|[class-20-3w2SktSOZdG8f6Qr.htm](feats/class-20-3w2SktSOZdG8f6Qr.htm)|Fatal Aria|Aria fatale|officielle|
|[class-20-4E4121lbfWgxui4y.htm](feats/class-20-4E4121lbfWgxui4y.htm)|Song of the Fallen|Chanson des morts au combat|libre|
|[class-20-5C0XMnfTuvgSKD7o.htm](feats/class-20-5C0XMnfTuvgSKD7o.htm)|Become Thought|Devenir pensée|libre|
|[class-20-5f0EbNl7DHkiKCIr.htm](feats/class-20-5f0EbNl7DHkiKCIr.htm)|Empyreal Aura|Aura empyréenne|libre|
|[class-20-5H3Sk1yhalQQzUys.htm](feats/class-20-5H3Sk1yhalQQzUys.htm)|Hex Master|Maître des maléfices|libre|
|[class-20-5uhKRkYDzLP7v3XY.htm](feats/class-20-5uhKRkYDzLP7v3XY.htm)|Zombie Horde|Horde zombie|libre|
|[class-20-62USpCx1ewK03wzm.htm](feats/class-20-62USpCx1ewK03wzm.htm)|Ricochet Legend|Légende du ricochet|libre|
|[class-20-6PCNYExygF5890Fl.htm](feats/class-20-6PCNYExygF5890Fl.htm)|Thick Hide Mask|Masque de peau épaisse|libre|
|[class-20-6SEDoht4dXEJE5SW.htm](feats/class-20-6SEDoht4dXEJE5SW.htm)|Bloodline Perfection|Perfection du lignage|officielle|
|[class-20-6yJxUx0W2hwHckNi.htm](feats/class-20-6yJxUx0W2hwHckNi.htm)|Full Automation|Automatisation complète|libre|
|[class-20-79pGj9RC1bAt82UD.htm](feats/class-20-79pGj9RC1bAt82UD.htm)|Unlimited Demesne|Protectorat illimité|libre|
|[class-20-7QLLwcSKNGPWdOGG.htm](feats/class-20-7QLLwcSKNGPWdOGG.htm)|Annihilating Swing|Coup annihilateur|libre|
|[class-20-84ML8enTqOOdXA8O.htm](feats/class-20-84ML8enTqOOdXA8O.htm)|Accurate Flurry|Déluge précis|libre|
|[class-20-85boZA8KRMu4rihN.htm](feats/class-20-85boZA8KRMu4rihN.htm)|Ubiquitous Overdrive|Surrégime omniprésent|libre|
|[class-20-8YSwzLNlmBLoEyUj.htm](feats/class-20-8YSwzLNlmBLoEyUj.htm)|Cross the Final Horizon|Traverser l'horizon final|libre|
|[class-20-9Slu8lSOYnDtKsIb.htm](feats/class-20-9Slu8lSOYnDtKsIb.htm)|Ruby Resurrection|Résurrection rubis|libre|
|[class-20-AmfO4FHmfFr0oNi9.htm](feats/class-20-AmfO4FHmfFr0oNi9.htm)|Bloodline Conduit|Conduit du lignage|officielle|
|[class-20-AXXlinOc2lq08NPH.htm](feats/class-20-AXXlinOc2lq08NPH.htm)|Celestial Mount|Monture céleste|officielle|
|[class-20-aZ5JdFKA7L8NYl4o.htm](feats/class-20-aZ5JdFKA7L8NYl4o.htm)|Tower Shield Mastery|Maîtrise du pavois|libre|
|[class-20-B3Fz46wzUIzrxWsA.htm](feats/class-20-B3Fz46wzUIzrxWsA.htm)|Legendary Shot|Tir légendaire|officielle|
|[class-20-banx3uX4JjjcHEc8.htm](feats/class-20-banx3uX4JjjcHEc8.htm)|Superior Sight (Low-Light Vision)|Vision supérieure (Vision nocturne)|libre|
|[class-20-BQkk7qSSRTFc5jNG.htm](feats/class-20-BQkk7qSSRTFc5jNG.htm)|Leyline Conduit|Conduit tellurique|officielle|
|[class-20-bwbPuv4JsilmMnPz.htm](feats/class-20-bwbPuv4JsilmMnPz.htm)|Immortal Bear|Ours immortel|libre|
|[class-20-Chu6s3xVnpOB64GH.htm](feats/class-20-Chu6s3xVnpOB64GH.htm)|Hierophant's Power|Puissance du hiérophante|officielle|
|[class-20-csjkzb5dsyZPeOtY.htm](feats/class-20-csjkzb5dsyZPeOtY.htm)|Wrath of the First Ghoul|Courroux de la Première goule|libre|
|[class-20-CuJzCxGwd1EZDift.htm](feats/class-20-CuJzCxGwd1EZDift.htm)|Cunning Trickster Mask|Masque du mystificateur rusé|libre|
|[class-20-czdqBRpzpml23la9.htm](feats/class-20-czdqBRpzpml23la9.htm)|Eternal Boost|Boost éternel|libre|
|[class-20-D8XoWk1brpyW6oO2.htm](feats/class-20-D8XoWk1brpyW6oO2.htm)|Plum Deluge|Déluge empoisonné|libre|
|[class-20-dj0aOPPcDOqmptpX.htm](feats/class-20-dj0aOPPcDOqmptpX.htm)|Storyteller's Mask|Masque du conteur|libre|
|[class-20-dJ1ZviNMpt4ID7lc.htm](feats/class-20-dJ1ZviNMpt4ID7lc.htm)|Spell Combination|Combinaison de sorts|officielle|
|[class-20-DMECB9RwLAhY0T9o.htm](feats/class-20-DMECB9RwLAhY0T9o.htm)|Emblazon Divinity|Divinité blasonnée|libre|
|[class-20-dwloLQzWgwjJWzXt.htm](feats/class-20-dwloLQzWgwjJWzXt.htm)|Banishing Blow|Coup bannissant|libre|
|[class-20-EHorYedQ8r05qAtk.htm](feats/class-20-EHorYedQ8r05qAtk.htm)|Triple Threat|Triple menace|officielle|
|[class-20-eIW5p8qqvsx2MFkY.htm](feats/class-20-eIW5p8qqvsx2MFkY.htm)|Mystery Conduit|Conduit mystérieux|libre|
|[class-20-EP8kaXNmrMfxOFAf.htm](feats/class-20-EP8kaXNmrMfxOFAf.htm)|Everdistant Defense|Défense toujours plus lointaine|libre|
|[class-20-epNrbgmjZDjJe7Ry.htm](feats/class-20-epNrbgmjZDjJe7Ry.htm)|Legendary Rider|Chevaucheur légendaire|libre|
|[class-20-epzeES7xJxvIXDdj.htm](feats/class-20-epzeES7xJxvIXDdj.htm)|Dance of Intercession|Danse de l'intercession|libre|
|[class-20-evMhKTjzdiuDKwMX.htm](feats/class-20-evMhKTjzdiuDKwMX.htm)|Aura of Unbreakable Virtue|Aura de vertu indéfectible|libre|
|[class-20-f4k5ripShCn5orZB.htm](feats/class-20-f4k5ripShCn5orZB.htm)|Grand Medic's Mask|Masque du grand médecin|libre|
|[class-20-flRXjabqedf6GjuU.htm](feats/class-20-flRXjabqedf6GjuU.htm)|Extradimensional Stash|Cachette extradimensionnelle|libre|
|[class-20-fMAUsLdBd5SDoHBz.htm](feats/class-20-fMAUsLdBd5SDoHBz.htm)|Paradoxical Mystery|Mystère paradoxal|libre|
|[class-20-FMjihpGLn9eQ14Gw.htm](feats/class-20-FMjihpGLn9eQ14Gw.htm)|Quaking Stomp|Piétinement sismique|officielle|
|[class-20-GIKySPq1n7xUmICw.htm](feats/class-20-GIKySPq1n7xUmICw.htm)|Bloodline Metamorphosis|Métamorphose du lignage|libre|
|[class-20-H63SJLkenhLDkVnN.htm](feats/class-20-H63SJLkenhLDkVnN.htm)|The Tyrant Falls!|Le tyran chute !|libre|
|[class-20-HEZeZcBWQR1QeWDo.htm](feats/class-20-HEZeZcBWQR1QeWDo.htm)|Astonishing Explosion|Explosion époustouflante|libre|
|[class-20-HLCeP87w7qEy8PUH.htm](feats/class-20-HLCeP87w7qEy8PUH.htm)|To the Ends of the Earth|Jusqu'aux confins du monde|officielle|
|[class-20-HxO0Sh3hNFMoSsTB.htm](feats/class-20-HxO0Sh3hNFMoSsTB.htm)|Emancipator's Mask|Masque de l'émancipateur|libre|
|[class-20-hYW6MsPk1UcFROFD.htm](feats/class-20-hYW6MsPk1UcFROFD.htm)|Twin Eidolon|Eidolon jumeau|libre|
|[class-20-iHUuWrvkT2uR0PnK.htm](feats/class-20-iHUuWrvkT2uR0PnK.htm)|Whirlwind Spell|Sort tourbillonnant|libre|
|[class-20-IMArawT1Sc2PTcYM.htm](feats/class-20-IMArawT1Sc2PTcYM.htm)|Boundless Reprisals|Représailles illimitées|officielle|
|[class-20-JaBDNtNYYDfTGYad.htm](feats/class-20-JaBDNtNYYDfTGYad.htm)|Sacred Defender|Défenseur sacré|libre|
|[class-20-JEFPufbvaCeiA0Zo.htm](feats/class-20-JEFPufbvaCeiA0Zo.htm)|Weapon Supremacy|Suprématie martiale|officielle|
|[class-20-jG9YwAAvNbCShumf.htm](feats/class-20-jG9YwAAvNbCShumf.htm)|Inexhaustible Countermoves|Contremesures infatigables|libre|
|[class-20-jYEMVfrXJLpXS6aC.htm](feats/class-20-jYEMVfrXJLpXS6aC.htm)|Radiant Blade Master|Maître lame radieux|officielle|
|[class-20-k2hxQ9SPOM7aFWQZ.htm](feats/class-20-k2hxQ9SPOM7aFWQZ.htm)|Vivacious Afterimage|Image rémanente exubérante|libre|
|[class-20-kceciNwoldkzAMbq.htm](feats/class-20-kceciNwoldkzAMbq.htm)|Fuse Stance|Fusion de postures|officielle|
|[class-20-kKZ4gnT7okaWS6tB.htm](feats/class-20-kKZ4gnT7okaWS6tB.htm)|Metamagic Mastery|Maîtrise de la métamagie|officielle|
|[class-20-Kl1O0WK37KMTumv1.htm](feats/class-20-Kl1O0WK37KMTumv1.htm)|Hidden Paragon|Parangon de discrétion|officielle|
|[class-20-kOqW5ZtnOiWxKl9M.htm](feats/class-20-kOqW5ZtnOiWxKl9M.htm)|Icy Apotheosis|Apothéose glacée|libre|
|[class-20-KTMzVCd6xAqCvxa5.htm](feats/class-20-KTMzVCd6xAqCvxa5.htm)|Fiendish Mount|Monture fiélone|libre|
|[class-20-l0Qy74a7CILdE4Th.htm](feats/class-20-l0Qy74a7CILdE4Th.htm)|Metamagic Channel|Canalisation métamagique|officielle|
|[class-20-Lbpm0OrQb4u2LVtj.htm](feats/class-20-Lbpm0OrQb4u2LVtj.htm)|Verdant Presence|Présence verdoyante|libre|
|[class-20-LCrBGoMGat2ZXuOo.htm](feats/class-20-LCrBGoMGat2ZXuOo.htm)|Ubiquitous Weakness|Faiblesse omniprésente|libre|
|[class-20-LDIZtE7saDLSBduG.htm](feats/class-20-LDIZtE7saDLSBduG.htm)|Reactive Distraction|Diversion réactive|officielle|
|[class-20-LLrGafdJij7qiWZi.htm](feats/class-20-LLrGafdJij7qiWZi.htm)|Reclaim Spell|Sort Repris|libre|
|[class-20-lQpY6E5Zvc1kRnyC.htm](feats/class-20-lQpY6E5Zvc1kRnyC.htm)|Unlimited Potential|Potentiel illimité|libre|
|[class-20-mGzPR7M9H733j2wN.htm](feats/class-20-mGzPR7M9H733j2wN.htm)|True Shapeshifter|Métamorphe ultime|officielle|
|[class-20-mjdLmXLCNaRgMLVw.htm](feats/class-20-mjdLmXLCNaRgMLVw.htm)|Mimic Protections|Imiter les protections|libre|
|[class-20-mMMIHLVSr8fyvVQL.htm](feats/class-20-mMMIHLVSr8fyvVQL.htm)|Mega Bomb|Mégabombe|officielle|
|[class-20-mSqzGGttJvj4LxK9.htm](feats/class-20-mSqzGGttJvj4LxK9.htm)|Impossible Striker|Frappeur impossible|officielle|
|[class-20-N1ajKcWRo3O0oMQg.htm](feats/class-20-N1ajKcWRo3O0oMQg.htm)|Bloodline Mutation|Mutation du lignage|libre|
|[class-20-NGv7sphkVgF3CtXK.htm](feats/class-20-NGv7sphkVgF3CtXK.htm)|Wonder Worker|Travailleur de merveille|libre|
|[class-20-nJe8eQUrIpKWLXh5.htm](feats/class-20-nJe8eQUrIpKWLXh5.htm)|Contagious Rage|Rage contagieuse|officielle|
|[class-20-nn7DiYYWinsSYrZy.htm](feats/class-20-nn7DiYYWinsSYrZy.htm)|Protective Spirit Mask|Masque de l'esprit protecteur|libre|
|[class-20-OliKxFIqzky2o6vk.htm](feats/class-20-OliKxFIqzky2o6vk.htm)|Ringmaster's Introduction|Introduction du maître de la piste|libre|
|[class-20-Oo3yRbmrgqi8dmVs.htm](feats/class-20-Oo3yRbmrgqi8dmVs.htm)|Panache Paragon|Parangon de panache|libre|
|[class-20-opeP0JF9WGmNG0pb.htm](feats/class-20-opeP0JF9WGmNG0pb.htm)|Avatar's Audience|Audience de l'avatar|officielle|
|[class-20-OYrcbyaV3v8ycksj.htm](feats/class-20-OYrcbyaV3v8ycksj.htm)|Head of the Night Parade|Tête de la Parade nocturne|libre|
|[class-20-p353WH847errsNvh.htm](feats/class-20-p353WH847errsNvh.htm)|Apex Companion|Compagnon alpha|libre|
|[class-20-P9swngiLXbhMegQ8.htm](feats/class-20-P9swngiLXbhMegQ8.htm)|Shield Paragon|Paragon du bouclier|officielle|
|[class-20-PDFbfCrV2z0wfMz0.htm](feats/class-20-PDFbfCrV2z0wfMz0.htm)|Demon's Hair|Cheveux du démon|libre|
|[class-20-PExiZZTSP4p7TZaW.htm](feats/class-20-PExiZZTSP4p7TZaW.htm)|Oracular Providence|Providence oraculaire|libre|
|[class-20-PTAdxHcTS7TjyBTg.htm](feats/class-20-PTAdxHcTS7TjyBTg.htm)|Twin Psyche|Psyché jumelle|libre|
|[class-20-QDjpZKOrWIV1G8XJ.htm](feats/class-20-QDjpZKOrWIV1G8XJ.htm)|Maker of Miracles|Faiseur de miracles|officielle|
|[class-20-QSBuAkJ5GMLcuZg9.htm](feats/class-20-QSBuAkJ5GMLcuZg9.htm)|Ultimate Polymath|Touche-à-tout ultime|libre|
|[class-20-rMjlDss3Km1RQ8DE.htm](feats/class-20-rMjlDss3Km1RQ8DE.htm)|Slinger's Reflexes|Réflexes du tireur|libre|
|[class-20-rwTbN2A2ZO7CdKoC.htm](feats/class-20-rwTbN2A2ZO7CdKoC.htm)|Impossible Snares|Pièges artisanaux invraisemblables|libre|
|[class-20-RYUb5oxd46Yvdypz.htm](feats/class-20-RYUb5oxd46Yvdypz.htm)|Endurance of the Rooted Tree|Endurance de l'arbre enraciné|libre|
|[class-20-RzfWrOqHL2GcK0rr.htm](feats/class-20-RzfWrOqHL2GcK0rr.htm)|Enduring Debilitation|Incapacité persistante|libre|
|[class-20-sayRIHE2V6vuxr4r.htm](feats/class-20-sayRIHE2V6vuxr4r.htm)|Ultimate Flexibility|Flexibilité ultime|libre|
|[class-20-SelPslNtTfzxp7fs.htm](feats/class-20-SelPslNtTfzxp7fs.htm)|Patron's Truth|Vérité du patron|libre|
|[class-20-sfxLo9kz2WkCQiy4.htm](feats/class-20-sfxLo9kz2WkCQiy4.htm)|Symphony of the Muse|Symphonie de la muse|officielle|
|[class-20-siegOEdEpevAJNFw.htm](feats/class-20-siegOEdEpevAJNFw.htm)|Denier of Destruction|Négateur de destruction|libre|
|[class-20-Sq9muixjFptJO8Ae.htm](feats/class-20-Sq9muixjFptJO8Ae.htm)|Witch's Hut|Hutte du sorcier|libre|
|[class-20-srWsvDDdz77yieY1.htm](feats/class-20-srWsvDDdz77yieY1.htm)|Impossible Technique|Technique invraisemblable|officielle|
|[class-20-SS8JSDB2P0SFX1KH.htm](feats/class-20-SS8JSDB2P0SFX1KH.htm)|Sky Master Mask|Masque du maître du ciel|libre|
|[class-20-tBWSxVxrojRcEzJt.htm](feats/class-20-tBWSxVxrojRcEzJt.htm)|Scapegoat Parallel Self|Bouc émissaire parallèle|libre|
|[class-20-tFUo2tsdreWBxMfs.htm](feats/class-20-tFUo2tsdreWBxMfs.htm)|Stalking Feline Mask|Masque du félin traqueur|libre|
|[class-20-Tj79ePSD212EZjRM.htm](feats/class-20-Tj79ePSD212EZjRM.htm)|Vitality-Manipulating Stance|Posture de manipulation de la vitalité|libre|
|[class-20-TLCeFMDRXFB46sa8.htm](feats/class-20-TLCeFMDRXFB46sa8.htm)|Deadly Strikes|Frappes mortelles|libre|
|[class-20-tP26mgaFPpr6df1i.htm](feats/class-20-tP26mgaFPpr6df1i.htm)|Archwizard's Might|Puissance de l'archimage|officielle|
|[class-20-TsgW87kYudNr6Bfp.htm](feats/class-20-TsgW87kYudNr6Bfp.htm)|Wish Alchemy|Alchimie des souhaits|libre|
|[class-20-txLcSHu6kEfmrJj1.htm](feats/class-20-txLcSHu6kEfmrJj1.htm)|Enduring Quickness|Rapidité persistante|officielle|
|[class-20-UlCbjWWVEmfvaf5a.htm](feats/class-20-UlCbjWWVEmfvaf5a.htm)|Tenacious Blood Magic|Sang magique tenace|libre|
|[class-20-Uof5QNeGklGnks1h.htm](feats/class-20-Uof5QNeGklGnks1h.htm)|Superior Sight|Vision supérieure|libre|
|[class-20-UOxDJt8Y7SiCR4xq.htm](feats/class-20-UOxDJt8Y7SiCR4xq.htm)|Pied Piping|Flûte de Hamelin|libre|
|[class-20-uwp7Y4LNtPbhELjS.htm](feats/class-20-uwp7Y4LNtPbhELjS.htm)|Golden Body|Corps doré|libre|
|[class-20-Ux9cBtDRP92EM6rl.htm](feats/class-20-Ux9cBtDRP92EM6rl.htm)|Mind Over Matter|Esprit sur la matière|libre|
|[class-20-VSyuTWRuxdmgq2HS.htm](feats/class-20-VSyuTWRuxdmgq2HS.htm)|Invulnerable Juggernaut|Juggernaut invulnérable|libre|
|[class-20-w4dijKncXx0ssBOQ.htm](feats/class-20-w4dijKncXx0ssBOQ.htm)|Everyone's A Suspect|Chacun est un suspect|libre|
|[class-20-WAWaew53zpTFVwQM.htm](feats/class-20-WAWaew53zpTFVwQM.htm)|Synergistic Spell|Sort synergique|libre|
|[class-20-wPjNjyh60fYKrDXl.htm](feats/class-20-wPjNjyh60fYKrDXl.htm)|Legendary Summoner|Conjurateur légendaire|libre|
|[class-20-Wukrctjz2e8W4bbS.htm](feats/class-20-Wukrctjz2e8W4bbS.htm)|Perfect Readiness|Réactivité parfaite|libre|
|[class-20-WVm2dxPOI4tK2wsJ.htm](feats/class-20-WVm2dxPOI4tK2wsJ.htm)|Impossible Volley (Eldritch Archer)|Volée invraisemblable (Archer mystique)|libre|
|[class-20-XenKYUBMWZQQ7niM.htm](feats/class-20-XenKYUBMWZQQ7niM.htm)|Whirlwind Toss|Lancer tournoyant|libre|
|[class-20-xhiwito5kneP4sjV.htm](feats/class-20-xhiwito5kneP4sjV.htm)|Spell Mastery|Maîtrise des sorts|libre|
|[class-20-XHOhAHv2HLdJQz2q.htm](feats/class-20-XHOhAHv2HLdJQz2q.htm)|Tireless Guide's Mask|Masque du guide infatigable|libre|
|[class-20-xhuXpOFOxDpJgngm.htm](feats/class-20-xhuXpOFOxDpJgngm.htm)|Perfect Encore|Rappel parfait|officielle|
|[class-20-xM0vwRFLZgmmI4YJ.htm](feats/class-20-xM0vwRFLZgmmI4YJ.htm)|Time Dilation Cascade|Dilatation temporelle en cascade|libre|
|[class-20-xqjPVZezw1a73JAO.htm](feats/class-20-xqjPVZezw1a73JAO.htm)|Worldsphere Gravity|Gravité du globe terrestre|libre|
|[class-20-YbneCzvDEr76mrsS.htm](feats/class-20-YbneCzvDEr76mrsS.htm)|Superior Sight (Darkvision)|Vision supérieure (Vision dans le noir)|libre|
|[class-20-YCqHaqn0TxdiGxiW.htm](feats/class-20-YCqHaqn0TxdiGxiW.htm)|Unstoppable Juggernaut|Juggernaut inarrêtable|libre|
|[class-20-Yec6UwJf5FLvAbZ4.htm](feats/class-20-Yec6UwJf5FLvAbZ4.htm)|Reflecting Riposte|Riposte réfléchissante|libre|
|[class-20-ZfycfbXlPXZlSqw5.htm](feats/class-20-ZfycfbXlPXZlSqw5.htm)|Just The Facts|Juste les faits|libre|
|[class-20-zOK6IFSz3DIBRjEw.htm](feats/class-20-zOK6IFSz3DIBRjEw.htm)|Craft Philosopher's Stone|Fabrication de la pierre philosophale|officielle|
|[class-20-zy7lx4SWkfDxqH6m.htm](feats/class-20-zy7lx4SWkfDxqH6m.htm)|Supreme Spellstrike|Frappe de sort suprème|libre|
|[general-01-2kwXTUjYYhoAGySr.htm](feats/general-01-2kwXTUjYYhoAGySr.htm)|Incredible Initiative|Initiative extraordinaire|officielle|
|[general-01-aJUXbe9HJVvv0Mxa.htm](feats/general-01-aJUXbe9HJVvv0Mxa.htm)|Breath Control|Contrôle de la respiration|officielle|
|[general-01-AmP0qu7c5dlBSath.htm](feats/general-01-AmP0qu7c5dlBSath.htm)|Toughness|Robustesse|officielle|
|[general-01-BStw1cANwx5baL6d.htm](feats/general-01-BStw1cANwx5baL6d.htm)|Armor Proficiency|Maîtrise d'armure|officielle|
|[general-01-c9fHUSI5lRdXu1Ic.htm](feats/general-01-c9fHUSI5lRdXu1Ic.htm)|Feather Step|Pas de plume|officielle|
|[general-01-I0BhPWqYf1bbzEYg.htm](feats/general-01-I0BhPWqYf1bbzEYg.htm)|Diehard|Dur à cuir|officielle|
|[general-01-i4yRvVwvXbGZDsD1.htm](feats/general-01-i4yRvVwvXbGZDsD1.htm)|Canny Acumen|Perspicacité astucieuse|libre|
|[general-01-ihN8gkHSdPG9Trte.htm](feats/general-01-ihN8gkHSdPG9Trte.htm)|Adopted Ancestry|Ascendance adoptive|officielle|
|[general-01-jM72TjJ965jocBV8.htm](feats/general-01-jM72TjJ965jocBV8.htm)|Shield Block|Blocage au bouclier|officielle|
|[general-01-N8Xz5fuW6o7GW124.htm](feats/general-01-N8Xz5fuW6o7GW124.htm)|Fast Recovery|Récupération rapide|officielle|
|[general-01-Rq5wkA8DtsmbzoGV.htm](feats/general-01-Rq5wkA8DtsmbzoGV.htm)|Ride|Chevaucher|libre|
|[general-01-Ux73dmoF8KnavyUD.htm](feats/general-01-Ux73dmoF8KnavyUD.htm)|Fleet|Vélocité|officielle|
|[general-01-x9wxQ61HNkAVbDHr.htm](feats/general-01-x9wxQ61HNkAVbDHr.htm)|Weapon Proficiency|Maîtrise d'arme|officielle|
|[general-01-z1Z22gTp7J1VRLSR.htm](feats/general-01-z1Z22gTp7J1VRLSR.htm)|Different Worlds|Mondes différents|libre|
|[general-03-38uOVS8fLZxraUrg.htm](feats/general-03-38uOVS8fLZxraUrg.htm)|Pick Up The Pace|Donner la cadence|libre|
|[general-03-9jGaBxLUtevZYcZO.htm](feats/general-03-9jGaBxLUtevZYcZO.htm)|Untrained Improvisation|Improvisation inexpérimentée|officielle|
|[general-03-9QQnv7nFpsNCGE89.htm](feats/general-03-9QQnv7nFpsNCGE89.htm)|Thorough Search|Fouille minutieuse|libre|
|[general-03-bh2jHyyYrkLMsIdX.htm](feats/general-03-bh2jHyyYrkLMsIdX.htm)|Prescient Planner|Planificateur prescient|libre|
|[general-03-fD9xjrnPfJ8aQxYA.htm](feats/general-03-fD9xjrnPfJ8aQxYA.htm)|Keen Follower|Suivant appliqué|libre|
|[general-03-jFmdevE4nKevovzo.htm](feats/general-03-jFmdevE4nKevovzo.htm)|Steel Your Resolve|Puiser dans votre résolution|libre|
|[general-03-m7KjpkAAh9PptJsY.htm](feats/general-03-m7KjpkAAh9PptJsY.htm)|Ancestral Paragon|Parangon ancestral|officielle|
|[general-03-Wb3FHiDuY6Nuc0N0.htm](feats/general-03-Wb3FHiDuY6Nuc0N0.htm)|Hireling Manager|Chef des employés|libre|
|[general-03-wPHZhgKzNw4VcCFt.htm](feats/general-03-wPHZhgKzNw4VcCFt.htm)|Skitter|Glisseur|libre|
|[general-03-xT593tHyPkumPuzz.htm](feats/general-03-xT593tHyPkumPuzz.htm)|Improvised Repair|Réparation improvisée|libre|
|[general-07-fOIUmDGa9gkeCHA0.htm](feats/general-07-fOIUmDGa9gkeCHA0.htm)|Supertaster|Supergoûteur|libre|
|[general-07-GdZLxDtFXaQI3Fop.htm](feats/general-07-GdZLxDtFXaQI3Fop.htm)|Expeditious Search|Fouille accélérée|officielle|
|[general-07-gfMP2aMs3YGONVeB.htm](feats/general-07-gfMP2aMs3YGONVeB.htm)|Numb to Death|Insensible à la mort|libre|
|[general-07-lHFz4MmebvPqTb0A.htm](feats/general-07-lHFz4MmebvPqTb0A.htm)|Prescient Consumable|Consommable prescient|libre|
|[general-11-4vu6P3cYoOOeUbLK.htm](feats/general-11-4vu6P3cYoOOeUbLK.htm)|Incredible Investiture|Investiture extraordinaire|officielle|
|[general-11-5ZsmRm7HvFAw2XDZ.htm](feats/general-11-5ZsmRm7HvFAw2XDZ.htm)|Caravan Leader|Chef de caravane|libre|
|[general-11-aFoVHsuInMOkTZoQ.htm](feats/general-11-aFoVHsuInMOkTZoQ.htm)|Incredible Scout|Éclaireur incroyable|libre|
|[general-11-CxTtzIPni048jK3U.htm](feats/general-11-CxTtzIPni048jK3U.htm)|Axuma's Awakening|Éveil d'Axuma|libre|
|[general-11-muMOxZyduEFv8UT6.htm](feats/general-11-muMOxZyduEFv8UT6.htm)|A Home In Every Port|Une maison dans chaque port|libre|
|[general-11-tVQPg1p1pCm5puNc.htm](feats/general-11-tVQPg1p1pCm5puNc.htm)|Axuma's Vigor|Vigueur d'Axuma|libre|
|[general-19-uudiUylT09WnHN7e.htm](feats/general-19-uudiUylT09WnHN7e.htm)|True Perception|Perception véritable|libre|
|[skill-01-09PurtIanNUPfNRq.htm](feats/skill-01-09PurtIanNUPfNRq.htm)|Combat Climber|Combattant-grimpeur|officielle|
|[skill-01-0GF2j54roPFIDmXf.htm](feats/skill-01-0GF2j54roPFIDmXf.htm)|Bon Mot|Bon mot|libre|
|[skill-01-0N8TtGSk5enoLBZ8.htm](feats/skill-01-0N8TtGSk5enoLBZ8.htm)|Eye For Numbers|Oeil pour les nombres|libre|
|[skill-01-1Bt7uCW2WI4sM84P.htm](feats/skill-01-1Bt7uCW2WI4sM84P.htm)|Dubious Knowledge|Savoir douteux|libre|
|[skill-01-1Eceqc6zbMj2x0q9.htm](feats/skill-01-1Eceqc6zbMj2x0q9.htm)|Seasoned|Assaisonné|libre|
|[skill-01-22P7IFyhrF7Fbw8B.htm](feats/skill-01-22P7IFyhrF7Fbw8B.htm)|Root Magic|Magie des racines|libre|
|[skill-01-3G8xUlgCjRmRJNfP.htm](feats/skill-01-3G8xUlgCjRmRJNfP.htm)|Quick Squeeze|Se faufiler rapidement|officielle|
|[skill-01-3HChkcD1IRqv4DbA.htm](feats/skill-01-3HChkcD1IRqv4DbA.htm)|Improvise Tool|Outil improvisé|libre|
|[skill-01-4RjDxgvNXNl5GG9d.htm](feats/skill-01-4RjDxgvNXNl5GG9d.htm)|Hobnobber|Animal social|officielle|
|[skill-01-4tTkRyOQ0VuRBac3.htm](feats/skill-01-4tTkRyOQ0VuRBac3.htm)|Terrain Expertise|Expertise du terrain|officielle|
|[skill-01-5nc5ridFBfYpn2Om.htm](feats/skill-01-5nc5ridFBfYpn2Om.htm)|Bargain Hunter|Chasseur de bonnes affaires|officielle|
|[skill-01-5s8FqK4YZTVOvP0v.htm](feats/skill-01-5s8FqK4YZTVOvP0v.htm)|Reveal True Name|Révéler le véritable nom|libre|
|[skill-01-6GO3dtFJnsNnSwWz.htm](feats/skill-01-6GO3dtFJnsNnSwWz.htm)|Medical Researcher|Chercheur en médecine|libre|
|[skill-01-6O8MoMheHs5hNHX1.htm](feats/skill-01-6O8MoMheHs5hNHX1.htm)|Crystal Healing|Cristallothérapie|libre|
|[skill-01-6ON8DjFXSMITZleX.htm](feats/skill-01-6ON8DjFXSMITZleX.htm)|No Cause For Alarm|Pas besoin de s'alarmer|libre|
|[skill-01-7LB00jkh6JaJr3vS.htm](feats/skill-01-7LB00jkh6JaJr3vS.htm)|Fascinating Performance|Représentation fascinante|officielle|
|[skill-01-8qebBeOJsyRIchcu.htm](feats/skill-01-8qebBeOJsyRIchcu.htm)|Forager|Glaneur|officielle|
|[skill-01-aAoFc10cOpxGypOY.htm](feats/skill-01-aAoFc10cOpxGypOY.htm)|Sign Language|Langue des signes|officielle|
|[skill-01-ar2DUlvDK4LDcH9J.htm](feats/skill-01-ar2DUlvDK4LDcH9J.htm)|Quick Coercion|Contrainte rapide|officielle|
|[skill-01-ASy9AKEIRxPYUi5o.htm](feats/skill-01-ASy9AKEIRxPYUi5o.htm)|Quick Repair|Réparation rapide|officielle|
|[skill-01-B6HbYsLBWb1RR6Fx.htm](feats/skill-01-B6HbYsLBWb1RR6Fx.htm)|Charming Liar|Menteur charismatique|officielle|
|[skill-01-B9cQLRHtXoLlF0iR.htm](feats/skill-01-B9cQLRHtXoLlF0iR.htm)|Concealing Legerdemain|Tour de passe-passe|libre|
|[skill-01-beyw5bdA5hkQbmaG.htm](feats/skill-01-beyw5bdA5hkQbmaG.htm)|Terrain Stalker|Traqueur de terrain|officielle|
|[skill-01-bkZgWFSFV4cAf5Ot.htm](feats/skill-01-bkZgWFSFV4cAf5Ot.htm)|Risky Surgery|Chirurgie risquée|libre|
|[skill-01-blMeVamjGz4ODWxq.htm](feats/skill-01-blMeVamjGz4ODWxq.htm)|Arcane Sense|Sens arcanique|officielle|
|[skill-01-BocFD2KV0qgUC76x.htm](feats/skill-01-BocFD2KV0qgUC76x.htm)|Additional Lore|Connaissances supplémentaires|officielle|
|[skill-01-BqceQIKE0lwIS98s.htm](feats/skill-01-BqceQIKE0lwIS98s.htm)|Pilgrim's Token|Amulette du pélerin|libre|
|[skill-01-BujTSrQI7o0dZolY.htm](feats/skill-01-BujTSrQI7o0dZolY.htm)|Contract Negotiator|Négociateur de contrat|libre|
|[skill-01-C0Tcelg3BAPhML6J.htm](feats/skill-01-C0Tcelg3BAPhML6J.htm)|Hefty Hauler|Portefaix|officielle|
|[skill-01-CnqMJR8e9jqJR7MM.htm](feats/skill-01-CnqMJR8e9jqJR7MM.htm)|Steady Balance|Équilibre stable|officielle|
|[skill-01-CwGMDf0akn0VbNPb.htm](feats/skill-01-CwGMDf0akn0VbNPb.htm)|Vicious Critique|Critique vicieux|libre|
|[skill-01-d8AjCqU30z7IOpos.htm](feats/skill-01-d8AjCqU30z7IOpos.htm)|Ravening's Desperation|Énergie du désespoir|libre|
|[skill-01-DMetdzt1VJL2Y62i.htm](feats/skill-01-DMetdzt1VJL2Y62i.htm)|Snare Crafting|Fabrication de pièges artisanaux|officielle|
|[skill-01-Dvz54d6aPhjsmUux.htm](feats/skill-01-Dvz54d6aPhjsmUux.htm)|Lie To Me|Ose me mentir|libre|
|[skill-01-dZDmWXzZfIoBJ53Q.htm](feats/skill-01-dZDmWXzZfIoBJ53Q.htm)|Tame Animal|Apprivoiser un animal|libre|
|[skill-01-f0faBEUPtspdutKx.htm](feats/skill-01-f0faBEUPtspdutKx.htm)|Acrobatic Performer|Représentation acrobatique|libre|
|[skill-01-gArdEleFCvUHtdGk.htm](feats/skill-01-gArdEleFCvUHtdGk.htm)|Express Rider|Chevaucheur rapide|libre|
|[skill-01-gnH9SpdNQegDqIar.htm](feats/skill-01-gnH9SpdNQegDqIar.htm)|All of the Animal|Tout de l'animal|libre|
|[skill-01-gUqvezs2zzoTXFAI.htm](feats/skill-01-gUqvezs2zzoTXFAI.htm)|Group Coercion|Contraindre un groupe|officielle|
|[skill-01-hDGosy2ZTwnyctEP.htm](feats/skill-01-hDGosy2ZTwnyctEP.htm)|Oddity Identification|Identification de bizarreries|officielle|
|[skill-01-HEBXaS656MZTiWFu.htm](feats/skill-01-HEBXaS656MZTiWFu.htm)|Lengthy Diversion|Diversion interminable|officielle|
|[skill-01-hVZbnsDuXihggylt.htm](feats/skill-01-hVZbnsDuXihggylt.htm)|Subtle Theft|Vol subtil|officielle|
|[skill-01-IlOQuCQIhjJpig3S.htm](feats/skill-01-IlOQuCQIhjJpig3S.htm)|Quick Identification|Identification rapide|officielle|
|[skill-01-iOY6YfGBaOvMNAor.htm](feats/skill-01-iOY6YfGBaOvMNAor.htm)|Underwater Marauder|Maraudeur aquatique|officielle|
|[skill-01-is3Oz9wt11lNq62K.htm](feats/skill-01-is3Oz9wt11lNq62K.htm)|Alchemical Crafting|Artisanat alchimique|officielle|
|[skill-01-jDdOqFmZLwE4dblQ.htm](feats/skill-01-jDdOqFmZLwE4dblQ.htm)|Pickpocket|Vol à la tire|officielle|
|[skill-01-JtjnFsOToBLnSRO9.htm](feats/skill-01-JtjnFsOToBLnSRO9.htm)|Courtly Graces|Manières courtoises|officielle|
|[skill-01-KpFetnUqTiweypZk.htm](feats/skill-01-KpFetnUqTiweypZk.htm)|Group Impression|Bonne impression de groupe|officielle|
|[skill-01-KxaYlC50zzHysJj8.htm](feats/skill-01-KxaYlC50zzHysJj8.htm)|Titan Wrestler|Lutteur contre les titans|officielle|
|[skill-01-Lc4dJZivRwU3QEmT.htm](feats/skill-01-Lc4dJZivRwU3QEmT.htm)|Armor Assist|Aide armure|libre|
|[skill-01-lEgYzFHransLkSvI.htm](feats/skill-01-lEgYzFHransLkSvI.htm)|Schooled In Secrets|Éduqué dans les secrets|libre|
|[skill-01-lFwfUvH2708rl6i3.htm](feats/skill-01-lFwfUvH2708rl6i3.htm)|Virtuosic Performer|Artiste virtuose|officielle|
|[skill-01-lQs2i9L09MQiZSPC.htm](feats/skill-01-lQs2i9L09MQiZSPC.htm)|Charlatan|Charlatan|libre|
|[skill-01-LQw0yIMDUJJkq1nD.htm](feats/skill-01-LQw0yIMDUJJkq1nD.htm)|Cat Fall|Chute féline|officielle|
|[skill-01-MjQyTcV8Jiv1Jtln.htm](feats/skill-01-MjQyTcV8Jiv1Jtln.htm)|Recognize Spell|Reconnaître des sorts|officielle|
|[skill-01-nowEaHgIyij7im8F.htm](feats/skill-01-nowEaHgIyij7im8F.htm)|Train Animal|Dressage animalier|officielle|
|[skill-01-p5Bmj3d0uAGnrzIn.htm](feats/skill-01-p5Bmj3d0uAGnrzIn.htm)|Read Psychometric Resonance|Lecture de la résonance psychométrique|libre|
|[skill-01-P6icK2DbRoZ3H6kc.htm](feats/skill-01-P6icK2DbRoZ3H6kc.htm)|Skill Training|Compétence qualifiante|officielle|
|[skill-01-P9HCz0uR6xPHuw72.htm](feats/skill-01-P9HCz0uR6xPHuw72.htm)|Multilingual|Polyglotte|officielle|
|[skill-01-QLeMH5mQgh28sa5o.htm](feats/skill-01-QLeMH5mQgh28sa5o.htm)|Specialty Crafting|Artisanat spécialisé|officielle|
|[skill-01-QShgLWlfKYJO750P.htm](feats/skill-01-QShgLWlfKYJO750P.htm)|Stitch Flesh|Recoudre la chair|libre|
|[skill-01-r7cgrrHh75R8UEqN.htm](feats/skill-01-r7cgrrHh75R8UEqN.htm)|Deceptive Worship|Culte trompeur|libre|
|[skill-01-sMCpihnBEpx18GBD.htm](feats/skill-01-sMCpihnBEpx18GBD.htm)|Inoculation|Vaccination|libre|
|[skill-01-sMm0UfYxEPpq2Yzd.htm](feats/skill-01-sMm0UfYxEPpq2Yzd.htm)|Experienced Professional|Professionnel expérimenté|officielle|
|[skill-01-t3btih0O5RUwWynt.htm](feats/skill-01-t3btih0O5RUwWynt.htm)|Experienced Smuggler|Contrebandier expérimenté|officielle|
|[skill-01-tGIXuk0XeWmG04CX.htm](feats/skill-01-tGIXuk0XeWmG04CX.htm)|Survey Wildlife|Observation de la nature|officielle|
|[skill-01-uR62fVC9FyQAMCO1.htm](feats/skill-01-uR62fVC9FyQAMCO1.htm)|Trick Magic Item|Usurpation d'objet magique|officielle|
|[skill-01-urQZwmzg2kS53vd5.htm](feats/skill-01-urQZwmzg2kS53vd5.htm)|Experienced Tracker|Pisteur expérimenté|officielle|
|[skill-01-voYr7ygVcWmlg1f4.htm](feats/skill-01-voYr7ygVcWmlg1f4.htm)|Crafter's Appraisal|Évaluation de l'artisan|libre|
|[skill-01-W6Gl9ePmItfDHji0.htm](feats/skill-01-W6Gl9ePmItfDHji0.htm)|Assurance|Assurance|officielle|
|[skill-01-wbjTkaKRygpaZS0r.htm](feats/skill-01-wbjTkaKRygpaZS0r.htm)|Secret Speech|Langage secret|libre|
|[skill-01-WC4xLBGmBsdOdHWu.htm](feats/skill-01-WC4xLBGmBsdOdHWu.htm)|Natural Medicine|Médecine naturelle|officielle|
|[skill-01-WeQGWvlWdeLeOlCN.htm](feats/skill-01-WeQGWvlWdeLeOlCN.htm)|Glean Contents|Glaner des informations|libre|
|[skill-01-Ws9JlysHcFoz6WAQ.htm](feats/skill-01-Ws9JlysHcFoz6WAQ.htm)|Forensic Acumen|Perspicacité forensique|libre|
|[skill-01-wYerMk6F1RZb0Fwt.htm](feats/skill-01-wYerMk6F1RZb0Fwt.htm)|Battle Medicine|Médecine militaire|libre|
|[skill-01-X2jGFfLU5qI5XVot.htm](feats/skill-01-X2jGFfLU5qI5XVot.htm)|Streetwise|Savoir urbain|officielle|
|[skill-01-x7EMZNMavris2aHY.htm](feats/skill-01-x7EMZNMavris2aHY.htm)|Student of the Canon|Étudiant du droit canon|officielle|
|[skill-01-xqAdXRd2gSQcqp5E.htm](feats/skill-01-xqAdXRd2gSQcqp5E.htm)|Impressive Performance|Représentation impressionnante|officielle|
|[skill-01-xQMz6eDgX75WX2ce.htm](feats/skill-01-xQMz6eDgX75WX2ce.htm)|Intimidating Glare|Regard intimidant|officielle|
|[skill-01-yUuU9xyotrpwpTyC.htm](feats/skill-01-yUuU9xyotrpwpTyC.htm)|Read Lips|Lire sur les lèvres|officielle|
|[skill-01-ZBhvJ9O8MvBFAlhq.htm](feats/skill-01-ZBhvJ9O8MvBFAlhq.htm)|Quick Jump|Saut rapide|officielle|
|[skill-02-3ZerjLH8ls3JT6cD.htm](feats/skill-02-3ZerjLH8ls3JT6cD.htm)|Robust Recovery|Récupération vigoureuse|officielle|
|[skill-02-4UXyMtXLaOxuH6Js.htm](feats/skill-02-4UXyMtXLaOxuH6Js.htm)|Distracting Performance|Représentation de diversion|libre|
|[skill-02-5f8gQxVdioUcgsTD.htm](feats/skill-02-5f8gQxVdioUcgsTD.htm)|Familiar Oddities|Étrangetés familières|libre|
|[skill-02-5I97q0FfAeXcUQhs.htm](feats/skill-02-5I97q0FfAeXcUQhs.htm)|Nimble Crawl|Ramper agilement|officielle|
|[skill-02-5YlHrBCZJmb6Q4Lz.htm](feats/skill-02-5YlHrBCZJmb6Q4Lz.htm)|Slippery Prey|Proie fuyante|libre|
|[skill-02-6vnbC90UQ3I57RrQ.htm](feats/skill-02-6vnbC90UQ3I57RrQ.htm)|Lasting Coercion|Contrainte durable|officielle|
|[skill-02-6Z4e3B9vePYpibcy.htm](feats/skill-02-6Z4e3B9vePYpibcy.htm)|Confabulator|Affabulateur|officielle|
|[skill-02-70SaR6jce34yuTnQ.htm](feats/skill-02-70SaR6jce34yuTnQ.htm)|What's That Up Your Sleeve|Qu'est-ce qu'il y a dans ta manche ?|libre|
|[skill-02-7F3sTNRoNsQgD8tX.htm](feats/skill-02-7F3sTNRoNsQgD8tX.htm)|Backup Disguise|Déguisement de secours|libre|
|[skill-02-7t2VGjVhYNU3MsEm.htm](feats/skill-02-7t2VGjVhYNU3MsEm.htm)|Spirit Speaker|Parle-esprit|libre|
|[skill-02-8c61nOIr5AM3KxZi.htm](feats/skill-02-8c61nOIr5AM3KxZi.htm)|Ward Medic|Infirmier|officielle|
|[skill-02-aMI39DZhWgNgJTAn.htm](feats/skill-02-aMI39DZhWgNgJTAn.htm)|Malleable Movement|Mouvement malléable|libre|
|[skill-02-AYb8PmGJ37HwIMwj.htm](feats/skill-02-AYb8PmGJ37HwIMwj.htm)|Juggle|Jonglage|libre|
|[skill-02-bJ4tF95Byy7wCS65.htm](feats/skill-02-bJ4tF95Byy7wCS65.htm)|Rolling Landing|Atterrissage en roulade|libre|
|[skill-02-bmtf49jRKBCfdC5W.htm](feats/skill-02-bmtf49jRKBCfdC5W.htm)|Half-Truths|Demi-vérités|libre|
|[skill-02-BV5jpSifVJsTwoO7.htm](feats/skill-02-BV5jpSifVJsTwoO7.htm)|Wilderness Spotter|Observateur des étendues sauvages|libre|
|[skill-02-c85a69mB1urW2Se2.htm](feats/skill-02-c85a69mB1urW2Se2.htm)|Continual Recovery|Récupération continue|libre|
|[skill-02-cc8O47KFsODReoBe.htm](feats/skill-02-cc8O47KFsODReoBe.htm)|Glad-Hand|Cordialité forcée|officielle|
|[skill-02-cmuvvPJvt2R16vGe.htm](feats/skill-02-cmuvvPJvt2R16vGe.htm)|Fresh Ingredients|Ingrédients frais|libre|
|[skill-02-dUnT3HWMFD3d2eBJ.htm](feats/skill-02-dUnT3HWMFD3d2eBJ.htm)|Encouraging Words|Paroles encourageantes|libre|
|[skill-02-e4KB4pSkx2lDBNw3.htm](feats/skill-02-e4KB4pSkx2lDBNw3.htm)|Quick Disguise|Déguisement rapide|officielle|
|[skill-02-EZrxp0XxYh9rjghB.htm](feats/skill-02-EZrxp0XxYh9rjghB.htm)|Embed Aeon Stone|Pierre d'éternité implantée|libre|
|[skill-02-FJK7JTLSgugRIlvS.htm](feats/skill-02-FJK7JTLSgugRIlvS.htm)|Rapid Mantel|Accroche rapide|libre|
|[skill-02-gydOsP9VsdRw3Wg1.htm](feats/skill-02-gydOsP9VsdRw3Wg1.htm)|Underground Network|Réseau souterrain|libre|
|[skill-02-H3I2X0f7v4EzwxuN.htm](feats/skill-02-H3I2X0f7v4EzwxuN.htm)|Automatic Knowledge|Savoir automatique|officielle|
|[skill-02-HJYQlmGTdtyGWr6a.htm](feats/skill-02-HJYQlmGTdtyGWr6a.htm)|Powerful Leap|Bond puissant|officielle|
|[skill-02-iCv8KTZ5PcF4GqeV.htm](feats/skill-02-iCv8KTZ5PcF4GqeV.htm)|Connections|Relations|libre|
|[skill-02-iDimfGmQHacwxeh2.htm](feats/skill-02-iDimfGmQHacwxeh2.htm)|Terrifying Resistance|Résistance terrifiante|libre|
|[skill-02-IyJyZ1VQxS58Nk6V.htm](feats/skill-02-IyJyZ1VQxS58Nk6V.htm)|Eyes of the City|Yeux de la cité|libre|
|[skill-02-K2R1xGTgBnSCDMUl.htm](feats/skill-02-K2R1xGTgBnSCDMUl.htm)|Express Driver|Conducteur rapide|libre|
|[skill-02-K3Pgj5z9OpLFzxgt.htm](feats/skill-02-K3Pgj5z9OpLFzxgt.htm)|Cooperative Crafting|Artisanat en coopération|libre|
|[skill-02-lB9MVGCJr7aJQuIH.htm](feats/skill-02-lB9MVGCJr7aJQuIH.htm)|Armored Stealth|Discrétion en armure|libre|
|[skill-02-LDUWw2TeEEH0KA6M.htm](feats/skill-02-LDUWw2TeEEH0KA6M.htm)|Criminal Connections|Réseau criminel|officielle|
|[skill-02-mEk2POFNU1Q0TQg2.htm](feats/skill-02-mEk2POFNU1Q0TQg2.htm)|Mortal Healing|Soins mortels|libre|
|[skill-02-N7IsnLDFt73r7x56.htm](feats/skill-02-N7IsnLDFt73r7x56.htm)|Shadow Mark|Marque d'ombre|libre|
|[skill-02-nJ3EBRat9yUgeWwv.htm](feats/skill-02-nJ3EBRat9yUgeWwv.htm)|Automatic Writing|Écriture automatique|libre|
|[skill-02-NUHosufuAhQCnF7N.htm](feats/skill-02-NUHosufuAhQCnF7N.htm)|Triumphant Boast|Vantardise triomphante|libre|
|[skill-02-OtV7esAwza1U6Kwr.htm](feats/skill-02-OtV7esAwza1U6Kwr.htm)|Eye of the Arclords|Oeil des Seigneurs de l'Arc|officielle|
|[skill-02-pekKtubQTkG9m1xK.htm](feats/skill-02-pekKtubQTkG9m1xK.htm)|Quiet Allies|Alliés silencieux|officielle|
|[skill-02-Peyf3L7c9esRTsgR.htm](feats/skill-02-Peyf3L7c9esRTsgR.htm)|Predict Weather|Prédire le temps|libre|
|[skill-02-pLjvgeqwHrYdg411.htm](feats/skill-02-pLjvgeqwHrYdg411.htm)|Bonded Animal|Animal lié|libre|
|[skill-02-QvBIYW6aAqoiyim3.htm](feats/skill-02-QvBIYW6aAqoiyim3.htm)|Exhort The Faithful|Exhorter les croyants|libre|
|[skill-02-qvLcZGsV0HP2O0CG.htm](feats/skill-02-qvLcZGsV0HP2O0CG.htm)|Battle Planner|Planificateur de bataille|libre|
|[skill-02-RLBYJiGMVkaGL5w9.htm](feats/skill-02-RLBYJiGMVkaGL5w9.htm)|Wary Disarmament|Désarmement prudent|officielle|
|[skill-02-RlFZ648UR0Q0YECL.htm](feats/skill-02-RlFZ648UR0Q0YECL.htm)|Chromotherapy|Chromothérapie|libre|
|[skill-02-sLEawQueTV1wGn0B.htm](feats/skill-02-sLEawQueTV1wGn0B.htm)|Sow Rumor|Semer des rumeurs|libre|
|[skill-02-ThoOsKjn5xCuZUqM.htm](feats/skill-02-ThoOsKjn5xCuZUqM.htm)|Quick Contacts|Contacts rapides|libre|
|[skill-02-uF0ATN2Zw1Q67ew2.htm](feats/skill-02-uF0ATN2Zw1Q67ew2.htm)|Discreet Inquiry|Enquête discrète|libre|
|[skill-02-v7Bt6hjmzYnLFLeG.htm](feats/skill-02-v7Bt6hjmzYnLFLeG.htm)|Magical Shorthand|Sténographie magique|officielle|
|[skill-02-vjQ3VUpYlTAAIx3b.htm](feats/skill-02-vjQ3VUpYlTAAIx3b.htm)|Lead Climber|Premier de cordée|libre|
|[skill-02-vWtPxwND60EpxBAU.htm](feats/skill-02-vWtPxwND60EpxBAU.htm)|Tweak Appearances|Modifier les apparences|libre|
|[skill-02-XFJiGllNZp8Xebda.htm](feats/skill-02-XFJiGllNZp8Xebda.htm)|Intimidating Prowess|Prouesse intimidante|officielle|
|[skill-02-XmF4q4rzKWg55vG4.htm](feats/skill-02-XmF4q4rzKWg55vG4.htm)|Assured Identification|Identification assurée|libre|
|[skill-02-XvX1EyxWbbBF32NV.htm](feats/skill-02-XvX1EyxWbbBF32NV.htm)|Unmistakable Lore|Connaissances incontestables|officielle|
|[skill-02-xWY5omyIcILNR7y1.htm](feats/skill-02-xWY5omyIcILNR7y1.htm)|Magical Crafting|Artisanat magique|officielle|
|[skill-02-yTLGclKtWVFZLKIz.htm](feats/skill-02-yTLGclKtWVFZLKIz.htm)|Godless Healing|Guérison athée|libre|
|[skill-02-z0HX5L7bOOrKi0dD.htm](feats/skill-02-z0HX5L7bOOrKi0dD.htm)|Tattoo Artist|Artiste tatoueur|libre|
|[skill-02-Z7M3ImHWqUyGu6cN.htm](feats/skill-02-Z7M3ImHWqUyGu6cN.htm)|Fleeing Diversion|Diversion fuyante|libre|
|[skill-02-zdjPTg6vRwg8r2Lm.htm](feats/skill-02-zdjPTg6vRwg8r2Lm.htm)|Aura Sight|Vision des auras|libre|
|[skill-03-9lyFxaoZjF1ZjVN9.htm](feats/skill-03-9lyFxaoZjF1ZjVN9.htm)|Know the Beat|Connaissance du milieu|officielle|
|[skill-03-FbGPETHJR9VKxf9i.htm](feats/skill-03-FbGPETHJR9VKxf9i.htm)|Folk Dowsing|Sourcier populaire|libre|
|[skill-03-xduV7Kyoi5OWvMEK.htm](feats/skill-03-xduV7Kyoi5OWvMEK.htm)|We're on the List|Nous sommes sur la liste|libre|
|[skill-04-0Dy8RlFqrzCVOTl4.htm](feats/skill-04-0Dy8RlFqrzCVOTl4.htm)|Thorough Reports|Rapports détaillés|officielle|
|[skill-04-0UzxiSrTfVs0jvBa.htm](feats/skill-04-0UzxiSrTfVs0jvBa.htm)|Familiar Foe|Adversaire familier|libre|
|[skill-04-5qXw5Gl9TxbPMZLB.htm](feats/skill-04-5qXw5Gl9TxbPMZLB.htm)|Orthographic Mastery|Maîtrise orthographique|libre|
|[skill-04-68Kc4UyhnP4l8mxq.htm](feats/skill-04-68Kc4UyhnP4l8mxq.htm)|Dead Reckoning|Calcul de position|libre|
|[skill-04-7WBIXGqZbAKbqEU6.htm](feats/skill-04-7WBIXGqZbAKbqEU6.htm)|Named Artillery|Pièce baptisée|libre|
|[skill-04-9SdFlVQW4vM8ggh8.htm](feats/skill-04-9SdFlVQW4vM8ggh8.htm)|Fane's Escape|Diversion de Fane|libre|
|[skill-04-B4nuabUvA1rk7Hej.htm](feats/skill-04-B4nuabUvA1rk7Hej.htm)|Multilingual Cipher|Chiffrage polyglotte|libre|
|[skill-04-cQptGH6RUYZmS41Q.htm](feats/skill-04-cQptGH6RUYZmS41Q.htm)|Hideous Ululation|Ululement hideux|libre|
|[skill-04-f0s3WwaJN5f2UTYY.htm](feats/skill-04-f0s3WwaJN5f2UTYY.htm)|Reverse Engineering|Déduction de formule|libre|
|[skill-04-G9l2g7sDpPVbZJza.htm](feats/skill-04-G9l2g7sDpPVbZJza.htm)|Quick Mount|Vite en selle|libre|
|[skill-04-HRVCODLOrhjRDtGb.htm](feats/skill-04-HRVCODLOrhjRDtGb.htm)|Sure Foot|Pied sûr|libre|
|[skill-04-Jivt1iaqKfT6EcwV.htm](feats/skill-04-Jivt1iaqKfT6EcwV.htm)|That's Not Natural!|Ce n'est pas naturel !|libre|
|[skill-04-jzflcD1XnBp2bSZI.htm](feats/skill-04-jzflcD1XnBp2bSZI.htm)|Distracting Flattery|Flatterie de diversion|libre|
|[skill-04-K3Au5071pfvNwGob.htm](feats/skill-04-K3Au5071pfvNwGob.htm)|Hidden Magic|Magie cachée|libre|
|[skill-04-kIXFNPFBStOKunq4.htm](feats/skill-04-kIXFNPFBStOKunq4.htm)|Kreighton's Cognitive Crossover|Mélange cognitif de Kreighton|libre|
|[skill-04-knrawm4bMbDL9XS3.htm](feats/skill-04-knrawm4bMbDL9XS3.htm)|Ambush Tactics|Tactiques d'embuscade|libre|
|[skill-04-MWjbG8L5JWg888kJ.htm](feats/skill-04-MWjbG8L5JWg888kJ.htm)|Efficient Explorer|Explorateur efficace|libre|
|[skill-04-mwZzcwYVcTvxbXDl.htm](feats/skill-04-mwZzcwYVcTvxbXDl.htm)|Fancy Moves|Mouvements élaborés|libre|
|[skill-04-NADqgn78Rvl7TUG8.htm](feats/skill-04-NADqgn78Rvl7TUG8.htm)|Sociable Vow|Vœu de sociabilité|libre|
|[skill-04-p17VlHs0I4Yc4m34.htm](feats/skill-04-p17VlHs0I4Yc4m34.htm)|Engine Bay|Compartiment moteur|libre|
|[skill-04-R7BO8br4BjCmpjit.htm](feats/skill-04-R7BO8br4BjCmpjit.htm)|Settlement Scholastics|Études d'une implantation|libre|
|[skill-04-rAbfuZ1mc3lUYH41.htm](feats/skill-04-rAbfuZ1mc3lUYH41.htm)|Minion Guise|Aspect de sbire|libre|
|[skill-04-rfnEcjxIFqwlJwJT.htm](feats/skill-04-rfnEcjxIFqwlJwJT.htm)|Treat Condition|Soigner un état|libre|
|[skill-04-SmePERlF5BZl6cTo.htm](feats/skill-04-SmePERlF5BZl6cTo.htm)|Blessed Medicine|Médecine bénie|libre|
|[skill-04-smYXWVI9WXmqpiCs.htm](feats/skill-04-smYXWVI9WXmqpiCs.htm)|Eclipsed Vitality|Vitalité éclipsée|libre|
|[skill-04-SXXyAwe9FGBwhJIW.htm](feats/skill-04-SXXyAwe9FGBwhJIW.htm)|Power Slide|Dérapage puissant|libre|
|[skill-04-SZ8J3NbtiizFaNzz.htm](feats/skill-04-SZ8J3NbtiizFaNzz.htm)|Change of Face|Changer de visage|libre|
|[skill-04-uxwHHjWs3ehqtG4b.htm](feats/skill-04-uxwHHjWs3ehqtG4b.htm)|Steel Skin|Peau d'acier|libre|
|[skill-04-wpqKltAoJjRQgWow.htm](feats/skill-04-wpqKltAoJjRQgWow.htm)|Rope Runner|Gabier éprouvé|libre|
|[skill-04-Wsrw68pklQyaScMX.htm](feats/skill-04-Wsrw68pklQyaScMX.htm)|Phonetic Training|Entraînement phonétique|libre|
|[skill-04-WxL8NMW9JQ5igu0C.htm](feats/skill-04-WxL8NMW9JQ5igu0C.htm)|Diabolic Certitude|Certitude diabolique|officielle|
|[skill-04-ZC9C6rxPJKrw6Ktx.htm](feats/skill-04-ZC9C6rxPJKrw6Ktx.htm)|In Plain Sight|En pleine vue|libre|
|[skill-04-ZiSmhTsnQMLqsmyw.htm](feats/skill-04-ZiSmhTsnQMLqsmyw.htm)|Final Rest|Repos final|libre|
|[skill-05-1MZ4WNoNoJ4jj5Z0.htm](feats/skill-05-1MZ4WNoNoJ4jj5Z0.htm)|Cutting Flattery|Flatterie blessante|libre|
|[skill-06-1WfvvjjObPKeZyid.htm](feats/skill-06-1WfvvjjObPKeZyid.htm)|Holistic Care|Soins holistiques|libre|
|[skill-06-AnTBWhLiIA1c7jkg.htm](feats/skill-06-AnTBWhLiIA1c7jkg.htm)|Megafauna Veterinarian|Vétérinaire de mégafaune|libre|
|[skill-06-dAckQkpg1qyTz8od.htm](feats/skill-06-dAckQkpg1qyTz8od.htm)|Resourceful Ritualist|Ritualiste plein de ressources|libre|
|[skill-06-EZ24QwnFteLCrgLg.htm](feats/skill-06-EZ24QwnFteLCrgLg.htm)|Emerald Boughs Accustomation|Habitudes des Branches d'émeraude|libre|
|[skill-06-hAsaNx1dd3xvvAsE.htm](feats/skill-06-hAsaNx1dd3xvvAsE.htm)|Say That Again!|Répète çà !|libre|
|[skill-06-my4uFR8cnDC4mJE2.htm](feats/skill-06-my4uFR8cnDC4mJE2.htm)|Uzunjati Storytelling|Narration Uzunjati|libre|
|[skill-06-TZASOwBqVveGjw77.htm](feats/skill-06-TZASOwBqVveGjw77.htm)|Analyze Idiolect|Analyser l'idiolecte|libre|
|[skill-06-uwJQUFLymAWtJu1a.htm](feats/skill-06-uwJQUFLymAWtJu1a.htm)|Forced Entry|Entrée forcée|officielle|
|[skill-06-xLD9EmkEmT8hgwv7.htm](feats/skill-06-xLD9EmkEmT8hgwv7.htm)|Craft Facsimile|Fabriquer un fac-similé|libre|
|[skill-07-17FAYfreumeKbSGr.htm](feats/skill-07-17FAYfreumeKbSGr.htm)|Advanced First Aid|Premiers soins avancés|libre|
|[skill-07-2rSyTfPgAmNAo01r.htm](feats/skill-07-2rSyTfPgAmNAo01r.htm)|Quick Recognition|Reconnaissance rapide|officielle|
|[skill-07-63sSYk5yqiAyZGb9.htm](feats/skill-07-63sSYk5yqiAyZGb9.htm)|Graceful Leaper|Sauteur gracieux|libre|
|[skill-07-6vwLzzrFfeiR9pm0.htm](feats/skill-07-6vwLzzrFfeiR9pm0.htm)|Entourage|Entourage|libre|
|[skill-07-7GmXKrkzmInkFyEr.htm](feats/skill-07-7GmXKrkzmInkFyEr.htm)|Quick Unlock|Déverrouillage rapide|officielle|
|[skill-07-80AC6Q1m0eQiE7Et.htm](feats/skill-07-80AC6Q1m0eQiE7Et.htm)|Tumbling Theft|Dérobade Bousculade|libre|
|[skill-07-9o2VSlMQVPB4LN09.htm](feats/skill-07-9o2VSlMQVPB4LN09.htm)|Bizarre Magic|Magie bizarre|officielle|
|[skill-07-AHchBBO8lXCCuVxT.htm](feats/skill-07-AHchBBO8lXCCuVxT.htm)|Grudging Compliment|Compliments à contrecoeur|libre|
|[skill-07-bcEI0iIFhZXqjoaR.htm](feats/skill-07-bcEI0iIFhZXqjoaR.htm)|Quick Setup|Réalisation rapide|libre|
|[skill-07-bFoh3267kNLk68cU.htm](feats/skill-07-bFoh3267kNLk68cU.htm)|Quick Swim|Nage rapide|officielle|
|[skill-07-C5CweUNPP7HlRfBM.htm](feats/skill-07-C5CweUNPP7HlRfBM.htm)|Muscle Mimicry|Imitation musculaire|libre|
|[skill-07-Crd3qMecF9FYHjuH.htm](feats/skill-07-Crd3qMecF9FYHjuH.htm)|Read Shibboleths|Lire les signes distinctifs|libre|
|[skill-07-dc8X2Mbtwq6kGp7F.htm](feats/skill-07-dc8X2Mbtwq6kGp7F.htm)|Terrified Retreat|Retraite terrifiée|officielle|
|[skill-07-dz1rHYk3n9pUFfgm.htm](feats/skill-07-dz1rHYk3n9pUFfgm.htm)|Bless Toxin|Toxine bénie|libre|
|[skill-07-e6s2nIvlTycuzlR9.htm](feats/skill-07-e6s2nIvlTycuzlR9.htm)|Fabricated Connections|Réalisations inventées|libre|
|[skill-07-ePObIpaJDgDb9CQj.htm](feats/skill-07-ePObIpaJDgDb9CQj.htm)|Battle Cry|Cri de guerre|officielle|
|[skill-07-f5JOSyW1tKMpz6hU.htm](feats/skill-07-f5JOSyW1tKMpz6hU.htm)|Influence Nature|Influencer la nature|libre|
|[skill-07-fvYwsHM9O1twQa5N.htm](feats/skill-07-fvYwsHM9O1twQa5N.htm)|Consult The Spirits|Consulter les esprits|libre|
|[skill-07-g3zmkEVJJIjE32fY.htm](feats/skill-07-g3zmkEVJJIjE32fY.htm)|Scholastic Identification|Études de l'identification|libre|
|[skill-07-gBSPbQRXdagZTUwY.htm](feats/skill-07-gBSPbQRXdagZTUwY.htm)|Kip Up|Saut carpé|officielle|
|[skill-07-gEj8aJmCThMzAjKY.htm](feats/skill-07-gEj8aJmCThMzAjKY.htm)|Ashen Veil|Voile cendreux|libre|
|[skill-07-gHBdjbEnIK8clK8u.htm](feats/skill-07-gHBdjbEnIK8clK8u.htm)|Sacred Defense|Défense sacrée|libre|
|[skill-07-HBosqD4ijMVTflCt.htm](feats/skill-07-HBosqD4ijMVTflCt.htm)|Master of Apprentice|Maître d'apprentissage|libre|
|[skill-07-hkSuxXOc9qBleJbd.htm](feats/skill-07-hkSuxXOc9qBleJbd.htm)|Disturbing Knowledge|Connaissances perturbantes|libre|
|[skill-07-IJQJBnD5CjKvFYEx.htm](feats/skill-07-IJQJBnD5CjKvFYEx.htm)|Foil Senses|Déjouer les sens|officielle|
|[skill-07-Imvu2RV2ggjJ2HZt.htm](feats/skill-07-Imvu2RV2ggjJ2HZt.htm)|Swift Sneak|Furtivité rapide|officielle|
|[skill-07-JEzjyNbpsh05iymG.htm](feats/skill-07-JEzjyNbpsh05iymG.htm)|Prepare Papers|Préparer les papiers|libre|
|[skill-07-Jk9XEMfMXoPT0ua2.htm](feats/skill-07-Jk9XEMfMXoPT0ua2.htm)|Skeptic's Defense|Défense du sceptique|libre|
|[skill-07-KIK2Eza9TK47MEb2.htm](feats/skill-07-KIK2Eza9TK47MEb2.htm)|Shameless Request|Requête sans vergogne|libre|
|[skill-07-KP5VugClDb7I8enS.htm](feats/skill-07-KP5VugClDb7I8enS.htm)|That Was a Close One, Huh?|C'était juste, non ?|libre|
|[skill-07-MTO0spetPKyIa4sT.htm](feats/skill-07-MTO0spetPKyIa4sT.htm)|Water Sprint|Courir sur l'eau|libre|
|[skill-07-nBlzWZnmYuFHrMyV.htm](feats/skill-07-nBlzWZnmYuFHrMyV.htm)|Battle Prayer|Prière de bataille|libre|
|[skill-07-oXoQ9wwOmDe0hwbU.htm](feats/skill-07-oXoQ9wwOmDe0hwbU.htm)|Subjective Truth|Vérité subjective|libre|
|[skill-07-P04Hw8E6WAWARKHP.htm](feats/skill-07-P04Hw8E6WAWARKHP.htm)|Quick Climb|Escalade rapide|officielle|
|[skill-07-P9dVBWB8nYZt4AFA.htm](feats/skill-07-P9dVBWB8nYZt4AFA.htm)|Sanctify Water|Sanctifier l'eau|libre|
|[skill-07-PiUe3tpv7UVtnfvS.htm](feats/skill-07-PiUe3tpv7UVtnfvS.htm)|Impeccable Crafting|Artisanat impeccable|officielle|
|[skill-07-pKnX7MGZayqZui0Z.htm](feats/skill-07-pKnX7MGZayqZui0Z.htm)|Aerobatics Mastery|Maître trapéziste|libre|
|[skill-07-qibU5SZPFHMNnpAP.htm](feats/skill-07-qibU5SZPFHMNnpAP.htm)|Doublespeak|Double langage|libre|
|[skill-07-RiuZT3H4QZIIEQXJ.htm](feats/skill-07-RiuZT3H4QZIIEQXJ.htm)|Slippery Secrets|Secrets insaisissables|officielle|
|[skill-07-TkCy7jZUjhD8IypE.htm](feats/skill-07-TkCy7jZUjhD8IypE.htm)|Expert Disassembler|Démontage expert|libre|
|[skill-07-TXXrkkFfNSWgHrn5.htm](feats/skill-07-TXXrkkFfNSWgHrn5.htm)|Chronocognizance|Chronoconnaissance|libre|
|[skill-07-u6tLp3zTBweq7CxO.htm](feats/skill-07-u6tLp3zTBweq7CxO.htm)|Environmental Grace|Grâce environnementale|libre|
|[skill-07-UaCHd5SpYsZwf2hM.htm](feats/skill-07-UaCHd5SpYsZwf2hM.htm)|Push It|Pousse le|libre|
|[skill-07-ucmJ2Z5VXUtCiE3q.htm](feats/skill-07-ucmJ2Z5VXUtCiE3q.htm)|Efficient Controls|Commandes efficaces|libre|
|[skill-07-UHyoXbp8O6idQ6ee.htm](feats/skill-07-UHyoXbp8O6idQ6ee.htm)|Bless Tonic|Tonique béni|libre|
|[skill-07-UkMG3wMvrw8X0I98.htm](feats/skill-07-UkMG3wMvrw8X0I98.htm)|Propeller Attachment|Fixer un propulseur|libre|
|[skill-07-wlbfINUTHDPqbV7v.htm](feats/skill-07-wlbfINUTHDPqbV7v.htm)|Morphic Manipulation|Manipulation morphique|libre|
|[skill-07-wqhxZwB1TR8fvpHP.htm](feats/skill-07-wqhxZwB1TR8fvpHP.htm)|Party Crasher|Parasite mondain|libre|
|[skill-07-WqTqHPDbargixuej.htm](feats/skill-07-WqTqHPDbargixuej.htm)|Biographical Eye|Oeil biographique|libre|
|[skill-07-WQtt44keeBP8t25P.htm](feats/skill-07-WQtt44keeBP8t25P.htm)|Voice Cold as Death|Voix froide comme la mort|libre|
|[skill-07-X8iSUF1m0eezmrjs.htm](feats/skill-07-X8iSUF1m0eezmrjs.htm)|Wall Jump|Saut de mur|officielle|
|[skill-07-x9xA8P2Vlz98He7C.htm](feats/skill-07-x9xA8P2Vlz98He7C.htm)|Rapid Affixture|Fixation rapide|libre|
|[skill-07-xOMwuKCf02aFzyp3.htm](feats/skill-07-xOMwuKCf02aFzyp3.htm)|Paragon Battle Medicine|Médecine militaire modèle|libre|
|[skill-07-XR95taODq1sq82Du.htm](feats/skill-07-XR95taODq1sq82Du.htm)|Inventor|Inventeur|officielle|
|[skill-07-YBge8sTgeY5jncX2.htm](feats/skill-07-YBge8sTgeY5jncX2.htm)|Speech of the Mammoth Lords|Discours des Seigneurs des mammouths|libre|
|[skill-07-YgbcLfAEdi4xxvX5.htm](feats/skill-07-YgbcLfAEdi4xxvX5.htm)|Evangelize|Évangélisation|libre|
|[skill-07-Yj4mpROEjdCjQzMd.htm](feats/skill-07-Yj4mpROEjdCjQzMd.htm)|Planar Survival|Survie planaire|officielle|
|[skill-08-2Tla5D1vpGioh42x.htm](feats/skill-08-2Tla5D1vpGioh42x.htm)|Unravel Mysteries|Résolution des mystères|libre|
|[skill-08-5vRXZcGAbqKRoaqL.htm](feats/skill-08-5vRXZcGAbqKRoaqL.htm)|Recognize Threat|Reconnaître la menace|libre|
|[skill-08-n3vpCWPjXAInRTyR.htm](feats/skill-08-n3vpCWPjXAInRTyR.htm)|Snap Out Of It! (Pathfinder Agent)|Secoue-toi ! (Agent des Éclaireurs)|libre|
|[skill-08-RcQv16RK80R6c4id.htm](feats/skill-08-RcQv16RK80R6c4id.htm)|Improvised Crafting|Artisanat improvisé|libre|
|[skill-08-rhVL28qFl760qJQe.htm](feats/skill-08-rhVL28qFl760qJQe.htm)|Insistent Command|Ordre insistant|libre|
|[skill-08-tjnL4wsriZugnHLn.htm](feats/skill-08-tjnL4wsriZugnHLn.htm)|Ilverani Purist|Puriste Ilvérani|libre|
|[skill-08-yoeMOIgH8Snw1JCQ.htm](feats/skill-08-yoeMOIgH8Snw1JCQ.htm)|Diverse Recognition|Reconnaissance multiple|libre|
|[skill-10-TNV1cs1VFqdj4D2M.htm](feats/skill-10-TNV1cs1VFqdj4D2M.htm)|Masterful Obfuscation|Obscurcissement magistral|libre|
|[skill-10-UCen3Tq6BJlNI7rx.htm](feats/skill-10-UCen3Tq6BJlNI7rx.htm)|Uzunjati Recollection|Mémoire Uzunjati|libre|
|[skill-12-9AZjpeeS824VsYv8.htm](feats/skill-12-9AZjpeeS824VsYv8.htm)|Emergency Medical Assistance|Aide médicale d'urgence|libre|
|[skill-12-m7AOg13xEJRHyoTt.htm](feats/skill-12-m7AOg13xEJRHyoTt.htm)|Rugged Survivalist|Survivant aguerri|libre|
|[skill-12-oFcn7SDOH6W2QhJl.htm](feats/skill-12-oFcn7SDOH6W2QhJl.htm)|Too Angry to Die|Trop en colère pour mourir|libre|
|[skill-12-QoPooHpBjPh1sjRD.htm](feats/skill-12-QoPooHpBjPh1sjRD.htm)|Recollect Studies|Se souvenir des enseignements|libre|
|[skill-13-g0fyway0FkdSo7ZE.htm](feats/skill-13-g0fyway0FkdSo7ZE.htm)|Quick Spring|Passer rapidement à l'action|libre|
|[skill-15-467qQoiy6bjWU1G8.htm](feats/skill-15-467qQoiy6bjWU1G8.htm)|Legendary Performer|Artiste légendaire|officielle|
|[skill-15-A0TNeMNvyY8QpmLz.htm](feats/skill-15-A0TNeMNvyY8QpmLz.htm)|Legendary Negotiation|Négociation légendaire|officielle|
|[skill-15-dYMxP8SsHrwOze8v.htm](feats/skill-15-dYMxP8SsHrwOze8v.htm)|Divine Guidance|Assistance divine|officielle|
|[skill-15-GZba7ped7ZxYHchf.htm](feats/skill-15-GZba7ped7ZxYHchf.htm)|Legendary Professional|Professionnel légendaire|libre|
|[skill-15-hrq3NOpS6148aVY1.htm](feats/skill-15-hrq3NOpS6148aVY1.htm)|Unified Theory|Théorie unifiée|officielle|
|[skill-15-IZbjUaZI5zHTd1Vp.htm](feats/skill-15-IZbjUaZI5zHTd1Vp.htm)|Legendary Thief|Voleur légendaire|officielle|
|[skill-15-Kk4AMZtpQnLEgN0b.htm](feats/skill-15-Kk4AMZtpQnLEgN0b.htm)|Legendary Medic|Infirmier légendaire|officielle|
|[skill-15-mZttsiWl1ql5NvrH.htm](feats/skill-15-mZttsiWl1ql5NvrH.htm)|Scare to Death|Mort de peur|libre|
|[skill-15-n0S0tJiOJPQk0Rne.htm](feats/skill-15-n0S0tJiOJPQk0Rne.htm)|Legendary Linguist|Linguiste légendaire|officielle|
|[skill-15-n0urrOL8YlnVBVRQ.htm](feats/skill-15-n0urrOL8YlnVBVRQ.htm)|Legendary Sneak|Furtivité légendaire|officielle|
|[skill-15-Ta61ObC8Lk7BxTFO.htm](feats/skill-15-Ta61ObC8Lk7BxTFO.htm)|Legendary Survivalist|Survivaliste légendaire|officielle|
|[skill-15-TVFfTP9fHRidwBlW.htm](feats/skill-15-TVFfTP9fHRidwBlW.htm)|Cloud Jump|Saut plané|officielle|
|[skill-15-v62QzTwHOT3t86cL.htm](feats/skill-15-v62QzTwHOT3t86cL.htm)|Craft Anything|Fabrication facilitée|officielle|
|[skill-15-Vk7BzAb3D9r226sI.htm](feats/skill-15-Vk7BzAb3D9r226sI.htm)|Legendary Guide|Guide légendaire|libre|
|[skill-15-VRCBTEyrcBf7auGz.htm](feats/skill-15-VRCBTEyrcBf7auGz.htm)|Legendary Tattoo Artist|Artiste tatoueur légendaire|libre|
|[skill-15-XZcd1wFHy111klu2.htm](feats/skill-15-XZcd1wFHy111klu2.htm)|Reveal Machinations|Révéler les machinations|libre|
|[skill-15-Zf4yiLDdxHPovEQI.htm](feats/skill-15-Zf4yiLDdxHPovEQI.htm)|Legendary Codebreaker|Cryptanalyste légendaire|officielle|
