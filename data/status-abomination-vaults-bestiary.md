# État de la traduction (abomination-vaults-bestiary)

 * **officielle**: 112


Dernière mise à jour: 2023-07-12 20:28 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[00s3MhFQ4yOp2rTf.htm](abomination-vaults-bestiary/00s3MhFQ4yOp2rTf.htm)|Elder Child of Belcorra|Enfant aîné de Belcorra|officielle|
|[0jvmec4yJH1ASfRy.htm](abomination-vaults-bestiary/0jvmec4yJH1ASfRy.htm)|Urthagul|Urthagul|officielle|
|[277uvPqG9RLMQUcO.htm](abomination-vaults-bestiary/277uvPqG9RLMQUcO.htm)|Warped Brew Morlock|Morlock de la Bière Tordue|officielle|
|[2K0oJcLauMwyRNZQ.htm](abomination-vaults-bestiary/2K0oJcLauMwyRNZQ.htm)|Murschen|Murschen|officielle|
|[2TOXR7nKS6xPng2Y.htm](abomination-vaults-bestiary/2TOXR7nKS6xPng2Y.htm)|Dread Wisp|Follet de l'effroi|officielle|
|[3d3NAcPfvn07mcGN.htm](abomination-vaults-bestiary/3d3NAcPfvn07mcGN.htm)|Afflicted Irnakurse|Irnakurse maudit|officielle|
|[3F3fPq5hFbej40T2.htm](abomination-vaults-bestiary/3F3fPq5hFbej40T2.htm)|Gibtas Spawn Swarm|Nuée de rejetons de Gibta|officielle|
|[3H1rBpUQwTcNd6xZ.htm](abomination-vaults-bestiary/3H1rBpUQwTcNd6xZ.htm)|Chandriu Invisar|Chandriu Invisar|officielle|
|[3Ll0LCZcmQmb7aV3.htm](abomination-vaults-bestiary/3Ll0LCZcmQmb7aV3.htm)|Galudu|Galudu|officielle|
|[3ry9WSvMMXHUe3kE.htm](abomination-vaults-bestiary/3ry9WSvMMXHUe3kE.htm)|Beluthus|Beluthus|officielle|
|[3vn9W5SThovdsEnY.htm](abomination-vaults-bestiary/3vn9W5SThovdsEnY.htm)|Sacuishu|Sacuishu|officielle|
|[4bznEiwsJvInwZwA.htm](abomination-vaults-bestiary/4bznEiwsJvInwZwA.htm)|Torture Chamber Barbazu|Barbazu de la chambre de torture|officielle|
|[55SmPtsUXsridUjJ.htm](abomination-vaults-bestiary/55SmPtsUXsridUjJ.htm)|Dragon's Blood Puffball|Vesse-de-loup mortelle|officielle|
|[5iuvJLceeLJPlR8O.htm](abomination-vaults-bestiary/5iuvJLceeLJPlR8O.htm)|Viscous Black Pudding|Pouding noir visqueux|officielle|
|[6NijRSpkIuQpSxUp.htm](abomination-vaults-bestiary/6NijRSpkIuQpSxUp.htm)|Spike Launcher|Lance-pointe|officielle|
|[78Vf1Lk9ph2RGDgL.htm](abomination-vaults-bestiary/78Vf1Lk9ph2RGDgL.htm)|Deepwater Dhuthorex|Duthorexe de l'abîme|officielle|
|[8AVy2qIK6vh13sSj.htm](abomination-vaults-bestiary/8AVy2qIK6vh13sSj.htm)|Ysondkhelir|Ysondkhelir|officielle|
|[8Iozx4jbIGHxK4yf.htm](abomination-vaults-bestiary/8Iozx4jbIGHxK4yf.htm)|Barcumbuk|Barcumbuk|officielle|
|[8VXEF3cnjzcokCTL.htm](abomination-vaults-bestiary/8VXEF3cnjzcokCTL.htm)|Stonescale Spirits|Esprits des Pierrécailles|officielle|
|[9BEHjBWRs0st7IVv.htm](abomination-vaults-bestiary/9BEHjBWRs0st7IVv.htm)|Images of Failure|Images d'échec|officielle|
|[9Tr4aUNr2wxxMDcg.htm](abomination-vaults-bestiary/9Tr4aUNr2wxxMDcg.htm)|Cratonys|Cratonys|officielle|
|[A4MusxxoLxwMVZua.htm](abomination-vaults-bestiary/A4MusxxoLxwMVZua.htm)|Drill Field Barbazu|Barbazu de la salle d'entraînement|officielle|
|[AdFuuUS7bIVqp5K7.htm](abomination-vaults-bestiary/AdFuuUS7bIVqp5K7.htm)|Rusty Grate Pit|Grille rouillée|officielle|
|[aIDLQY5mBPyxTjZ2.htm](abomination-vaults-bestiary/aIDLQY5mBPyxTjZ2.htm)|Witchfire Warden|Gardienne sorcière de feu|officielle|
|[bfuIEdKBj9bhuOft.htm](abomination-vaults-bestiary/bfuIEdKBj9bhuOft.htm)|Groetan Candle|Chandelle Groétane|officielle|
|[bif3iQcDPi27rx6x.htm](abomination-vaults-bestiary/bif3iQcDPi27rx6x.htm)|Salaisa Malthulas|Salaisa Malthulas|officielle|
|[BJYrYqkV7PkXgSfk.htm](abomination-vaults-bestiary/BJYrYqkV7PkXgSfk.htm)|Gibtas Bounder|Gibtas Sauteur|officielle|
|[BOaM3pAuWl06Q6IZ.htm](abomination-vaults-bestiary/BOaM3pAuWl06Q6IZ.htm)|Poisoning Room Specter|Spectre de la salle d'empoisonnement|officielle|
|[ceLvlSQsYNORH8oM.htm](abomination-vaults-bestiary/ceLvlSQsYNORH8oM.htm)|Voidglutton|Glouton du vide|officielle|
|[chOtDyemBuw2yNN2.htm](abomination-vaults-bestiary/chOtDyemBuw2yNN2.htm)|Watching Wall|Mur d'observation|officielle|
|[ChRgdkplhO1D81Lg.htm](abomination-vaults-bestiary/ChRgdkplhO1D81Lg.htm)|Bright Walker|Marcheur lumineux|officielle|
|[cMpgGvq1fGxh8wI0.htm](abomination-vaults-bestiary/cMpgGvq1fGxh8wI0.htm)|Seugathi Researcher|Chercheur seugathi|officielle|
|[czQFet5Qo63IXtHl.htm](abomination-vaults-bestiary/czQFet5Qo63IXtHl.htm)|Reaper Skull Puffball|Vesse-de-loup faucheuse|officielle|
|[DawBQWRyrM4cKfGI.htm](abomination-vaults-bestiary/DawBQWRyrM4cKfGI.htm)|Drow Warden|Gardien Drow|officielle|
|[DDJGNAh3rfyIupAb.htm](abomination-vaults-bestiary/DDJGNAh3rfyIupAb.htm)|Belcorra Haruvex|Belcorra Haruvex|officielle|
|[DnAeqlJRZc5N7hve.htm](abomination-vaults-bestiary/DnAeqlJRZc5N7hve.htm)|Dreshkan|Dreshkan|officielle|
|[dWOK0nzGWyc5NkNz.htm](abomination-vaults-bestiary/dWOK0nzGWyc5NkNz.htm)|Lady's Whisper|Murmure de la Dame|officielle|
|[E0FMRiGNCv5n7AVH.htm](abomination-vaults-bestiary/E0FMRiGNCv5n7AVH.htm)|Paralyzing Light Trap|Piège de lumière paralysante|officielle|
|[EN3mp0sVObP8ou3p.htm](abomination-vaults-bestiary/EN3mp0sVObP8ou3p.htm)|Jaul Mezmin|Jaul Mezmin|officielle|
|[eoQgRltsgJWa4aeC.htm](abomination-vaults-bestiary/eoQgRltsgJWa4aeC.htm)|Nhimbaloth's Cutter|Découpeur de Nhimbaloth|officielle|
|[fvijt2whssxJzxCF.htm](abomination-vaults-bestiary/fvijt2whssxJzxCF.htm)|Images of Powerlessness|Images d'impuissance|officielle|
|[gUihlg28MEloIqE3.htm](abomination-vaults-bestiary/gUihlg28MEloIqE3.htm)|Dune Candle|Chandelle des Dunes|officielle|
|[h0Ztbh36be4rpJCz.htm](abomination-vaults-bestiary/h0Ztbh36be4rpJCz.htm)|Dulac|Dulac|officielle|
|[HBRz8BVLVN9u9Odp.htm](abomination-vaults-bestiary/HBRz8BVLVN9u9Odp.htm)|Corpselight|Lueur de corps|officielle|
|[hia81Ut7fEREbhkq.htm](abomination-vaults-bestiary/hia81Ut7fEREbhkq.htm)|Jarelle Kaldrian|Jarelle Kaldrian|officielle|
|[HnIyJuqKNOvK7eOJ.htm](abomination-vaults-bestiary/HnIyJuqKNOvK7eOJ.htm)|Nox|Nox|officielle|
|[hnYckrT72oIKAuHJ.htm](abomination-vaults-bestiary/hnYckrT72oIKAuHJ.htm)|Vengeful Furnace|Incinérateur vengeur|officielle|
|[jE8BEe6pcnGraw2p.htm](abomination-vaults-bestiary/jE8BEe6pcnGraw2p.htm)|Jafaki|Jafaki|officielle|
|[JrowrtDilEG8dN2s.htm](abomination-vaults-bestiary/JrowrtDilEG8dN2s.htm)|Quara Orshendiel|Quara Orshendiel|officielle|
|[k4fVLtVrgIEg9xij.htm](abomination-vaults-bestiary/k4fVLtVrgIEg9xij.htm)|Bloodsiphon|Siphon de sang|officielle|
|[knoWZfTiLY7xGwhB.htm](abomination-vaults-bestiary/knoWZfTiLY7xGwhB.htm)|Painful Suggestion Trap|Douloureuse suggestion|officielle|
|[KSFGGxU3qxYJzWpe.htm](abomination-vaults-bestiary/KSFGGxU3qxYJzWpe.htm)|Morlock Scavenger|Récupérateur morlock|officielle|
|[kzHRJGLvjOJk5WU7.htm](abomination-vaults-bestiary/kzHRJGLvjOJk5WU7.htm)|Drow Cavern Seer|Prophète des cavernes drow|officielle|
|[kzX588Hjb3w4QPOj.htm](abomination-vaults-bestiary/kzX588Hjb3w4QPOj.htm)|Mister Beak|Monsieur Bec|officielle|
|[lH2rdwiyOPStkQvZ.htm](abomination-vaults-bestiary/lH2rdwiyOPStkQvZ.htm)|Morlock Cultist|Cultiste morlock|officielle|
|[lMCEVxKkQ7XK6Nid.htm](abomination-vaults-bestiary/lMCEVxKkQ7XK6Nid.htm)|Canker Cultist|Cultiste du Chancre|officielle|
|[Mk6Uo4Vt1HkG9EvL.htm](abomination-vaults-bestiary/Mk6Uo4Vt1HkG9EvL.htm)|Urevian|Urévian|officielle|
|[mlifDVJJWwjFtUxv.htm](abomination-vaults-bestiary/mlifDVJJWwjFtUxv.htm)|Murmur|Murmure|officielle|
|[mrxDc0fj5t5CvjJQ.htm](abomination-vaults-bestiary/mrxDc0fj5t5CvjJQ.htm)|Shanrigol Behemoth|Shanrigol béhémoth|officielle|
|[neNlIrZOs6zOODoe.htm](abomination-vaults-bestiary/neNlIrZOs6zOODoe.htm)|Blast Tumbler|Poignée explosive|officielle|
|[njfwxMPXTPA5AegD.htm](abomination-vaults-bestiary/njfwxMPXTPA5AegD.htm)|Summoning Chamber Erinys|Érinye de la chambre de convocation|officielle|
|[Nkz8Z5TrEqRePGlk.htm](abomination-vaults-bestiary/Nkz8Z5TrEqRePGlk.htm)|Scalathrax|Scalathraxe|officielle|
|[Np787X9Z4aQxOdCg.htm](abomination-vaults-bestiary/Np787X9Z4aQxOdCg.htm)|Khurfel|Khurfel|officielle|
|[NXdpFypPPmRwYBT1.htm](abomination-vaults-bestiary/NXdpFypPPmRwYBT1.htm)|Spellvoid|Coupesort|officielle|
|[OErk9kO3PhYwMXoJ.htm](abomination-vaults-bestiary/OErk9kO3PhYwMXoJ.htm)|Siora Fallowglade|Siora Clairefriche|officielle|
|[OloMMRPtTQKF0x16.htm](abomination-vaults-bestiary/OloMMRPtTQKF0x16.htm)|Caligni Defender|Défenseur caligni|officielle|
|[OqoWbYOyHDGHEeHV.htm](abomination-vaults-bestiary/OqoWbYOyHDGHEeHV.htm)|Drow Hunter|Chasseur drow|officielle|
|[OTT7CCkonkfwhJ8Y.htm](abomination-vaults-bestiary/OTT7CCkonkfwhJ8Y.htm)|Urdefhan Blood Mage|Mage de sang urdefhan|officielle|
|[oXnpdJVN6NIE58W3.htm](abomination-vaults-bestiary/oXnpdJVN6NIE58W3.htm)|Caliddo Haruvex|Caliddo Haruvex|officielle|
|[PphyArSCoxkaI6IS.htm](abomination-vaults-bestiary/PphyArSCoxkaI6IS.htm)|Befuddling Gas Trap|Piège à gaz abrutissant|officielle|
|[qjuLgWr2VhPcuylI.htm](abomination-vaults-bestiary/qjuLgWr2VhPcuylI.htm)|Doom of Tomorrow|Destin tragique à venir|officielle|
|[qOkxxiM4tNf96CHQ.htm](abomination-vaults-bestiary/qOkxxiM4tNf96CHQ.htm)|Seugathi Guard|Garde seugathi|officielle|
|[qoyopMMlRdgPAbXZ.htm](abomination-vaults-bestiary/qoyopMMlRdgPAbXZ.htm)|Vischari|Vischari|officielle|
|[qw2pk1zjvgxbeGBO.htm](abomination-vaults-bestiary/qw2pk1zjvgxbeGBO.htm)|Padli|Padli|officielle|
|[qXT1SQDtGqMkVl7Q.htm](abomination-vaults-bestiary/qXT1SQDtGqMkVl7Q.htm)|Shanrigol Heap|Monceau shanrigol|officielle|
|[R0EEgMDKcynpAWoa.htm](abomination-vaults-bestiary/R0EEgMDKcynpAWoa.htm)|Otari Ilvashti|Otari Ilvashti|officielle|
|[rArZ2y5xqSWeUU0G.htm](abomination-vaults-bestiary/rArZ2y5xqSWeUU0G.htm)|Drow Shootist|Arbalétrier d'élite drow|officielle|
|[rketcmqDQJbFFYfq.htm](abomination-vaults-bestiary/rketcmqDQJbFFYfq.htm)|Bone Gladiator|Gladiateur osseux|officielle|
|[ro9oVsu1cuCP8OQH.htm](abomination-vaults-bestiary/ro9oVsu1cuCP8OQH.htm)|Deadtide Skeleton Guard|Garde squelette de Morte-marée|officielle|
|[s8Ofcsub5QJJmgA5.htm](abomination-vaults-bestiary/s8Ofcsub5QJJmgA5.htm)|Daemonic Fog|Brouillard démonique|officielle|
|[saEUzIgUtV2AzKhl.htm](abomination-vaults-bestiary/saEUzIgUtV2AzKhl.htm)|Augrael|Augrael|officielle|
|[Sbxt8YT0AOFfVCPX.htm](abomination-vaults-bestiary/Sbxt8YT0AOFfVCPX.htm)|Urdefhan Lasher|Flagelleur urdefhan|officielle|
|[T6vOuhM1KV5Fr75F.htm](abomination-vaults-bestiary/T6vOuhM1KV5Fr75F.htm)|Gibtanius|Gibtanius|officielle|
|[TiAzR8SnYwhACWbj.htm](abomination-vaults-bestiary/TiAzR8SnYwhACWbj.htm)|Observation Deck Seugathi Researcher|Chercheur Seugathi de la plateforme d'observation|officielle|
|[To0MLA0arpkiE6Cz.htm](abomination-vaults-bestiary/To0MLA0arpkiE6Cz.htm)|Deep End Sarglagon|Sarglagon du grand bassin|officielle|
|[tOL4rWj2oYWZ4ow2.htm](abomination-vaults-bestiary/tOL4rWj2oYWZ4ow2.htm)|Aller Rosk|Aller Rosk|officielle|
|[tXfiVIThQlBT6B1H.htm](abomination-vaults-bestiary/tXfiVIThQlBT6B1H.htm)|Voidbracken Chuul|Chuul de Vide-fougère|officielle|
|[tYzzLLUv9WBhHhQY.htm](abomination-vaults-bestiary/tYzzLLUv9WBhHhQY.htm)|Carman Rajani|Carman Rajani|officielle|
|[tZCFmJonHkhGg1Vs.htm](abomination-vaults-bestiary/tZCFmJonHkhGg1Vs.htm)|Urdefhan Death Scout|Éclaireur de la mort urdefhan|officielle|
|[uWn6DCzthgUt97d7.htm](abomination-vaults-bestiary/uWn6DCzthgUt97d7.htm)|Gulzash|Gulzash|officielle|
|[uyGxJOnJ1gYwOpy5.htm](abomination-vaults-bestiary/uyGxJOnJ1gYwOpy5.htm)|Azvalvigander|Azvalvigandre|officielle|
|[V73Oqm1EL1KOoXOl.htm](abomination-vaults-bestiary/V73Oqm1EL1KOoXOl.htm)|Morlock Engineer|Ingénieur morlock|officielle|
|[v9B0hB5sm4YZxebY.htm](abomination-vaults-bestiary/v9B0hB5sm4YZxebY.htm)|Seugathi Servant|Serviteur seugathi|officielle|
|[VeyuTE1GUJ71oMMn.htm](abomination-vaults-bestiary/VeyuTE1GUJ71oMMn.htm)|Kragala|Kragala|officielle|
|[vHei0y2PKlXfxQ8Z.htm](abomination-vaults-bestiary/vHei0y2PKlXfxQ8Z.htm)|Child of Belcorra|Enfant de Belcorra|officielle|
|[vS1YISLmSnkNotkL.htm](abomination-vaults-bestiary/vS1YISLmSnkNotkL.htm)|Seugathi Reality Warper|Distordeur de réalité seugathi|officielle|
|[VsRKgjKolLsJMd0I.htm](abomination-vaults-bestiary/VsRKgjKolLsJMd0I.htm)|Shuffling Scythe Blades|Lames de faux mobiles|officielle|
|[vX4SBXIICKfrM4pF.htm](abomination-vaults-bestiary/vX4SBXIICKfrM4pF.htm)|Hellforge Barbazu|Barbazu de la forge des enfers|officielle|
|[w2N0foudBFcRCaHK.htm](abomination-vaults-bestiary/w2N0foudBFcRCaHK.htm)|Nhakazarin|Nhakazarin|officielle|
|[WR07Z6MjvebSHzI7.htm](abomination-vaults-bestiary/WR07Z6MjvebSHzI7.htm)|Ryta|Ryta|officielle|
|[x0NDgH3EMLTLh02r.htm](abomination-vaults-bestiary/x0NDgH3EMLTLh02r.htm)|Chafkhem|Chafkhem|officielle|
|[x3eOZvKUginj5Blh.htm](abomination-vaults-bestiary/x3eOZvKUginj5Blh.htm)|Flickerwisp|Follet clignotant|officielle|
|[xAfkUwJYq5JLmSrW.htm](abomination-vaults-bestiary/xAfkUwJYq5JLmSrW.htm)|Boss Skrawng|Boss Skrawng|officielle|
|[xj1Qn0VA4H4aKSjW.htm](abomination-vaults-bestiary/xj1Qn0VA4H4aKSjW.htm)|Jaul's Wolf|Loup de Jaul|officielle|
|[XLTPZ592Z9MXXBlA.htm](abomination-vaults-bestiary/XLTPZ592Z9MXXBlA.htm)|Blood of Belcorra|Sang de Belcorra|officielle|
|[YHYdZemNHp0fnWca.htm](abomination-vaults-bestiary/YHYdZemNHp0fnWca.htm)|Dread Dhuthorex|Duthorex de l'effroi|officielle|
|[YXF12ssz8tEh2YUe.htm](abomination-vaults-bestiary/YXF12ssz8tEh2YUe.htm)|Will-o'-the-Deep|Feu follet des profondeurs|officielle|
|[ZAXuvUW6kl6v3SuW.htm](abomination-vaults-bestiary/ZAXuvUW6kl6v3SuW.htm)|Volluk Azrinae|Volluk Azrinae|officielle|
|[ZDYMKYZVyR8Fqakp.htm](abomination-vaults-bestiary/ZDYMKYZVyR8Fqakp.htm)|Wrin Sivinxi|Wrin Sivinxi|officielle|
|[ZFP8RyQW4SNtJ3AE.htm](abomination-vaults-bestiary/ZFP8RyQW4SNtJ3AE.htm)|Nyzuros|Nyzuros|officielle|
|[zkiaelYxDB1ttlCI.htm](abomination-vaults-bestiary/zkiaelYxDB1ttlCI.htm)|Dhuthorex Sage|Sage duthorex|officielle|
|[zsgW8hHm3JfZUgwF.htm](abomination-vaults-bestiary/zsgW8hHm3JfZUgwF.htm)|Mulventok|Mulventok|officielle|
