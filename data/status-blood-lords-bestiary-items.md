# État de la traduction (blood-lords-bestiary-items)

 * **libre**: 1067
 * **aucune**: 15
 * **vide**: 7
 * **changé**: 2
 * **officielle**: 1


Dernière mise à jour: 2023-07-12 20:28 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[0xB9P0NcEOqV1pwH.htm](blood-lords-bestiary-items/0xB9P0NcEOqV1pwH.htm)|Cautionary Tale|
|[2Oa7DDtmDZUOLl5B.htm](blood-lords-bestiary-items/2Oa7DDtmDZUOLl5B.htm)|Bravery|
|[3Pew2VezftLEnHuA.htm](blood-lords-bestiary-items/3Pew2VezftLEnHuA.htm)|Scythe|+2,greaterStriking|
|[A5bMStZHdbQ9yIaL.htm](blood-lords-bestiary-items/A5bMStZHdbQ9yIaL.htm)|Biting Snakes|
|[aPjHCeJ7PdtCACQB.htm](blood-lords-bestiary-items/aPjHCeJ7PdtCACQB.htm)|Ritual Specialist|
|[EV8XJXl8deCUfqhK.htm](blood-lords-bestiary-items/EV8XJXl8deCUfqhK.htm)|Acid Shower|
|[lF9zmnxnCY2H8yr9.htm](blood-lords-bestiary-items/lF9zmnxnCY2H8yr9.htm)|Spin|
|[ocN9NYWASQWJbWm7.htm](blood-lords-bestiary-items/ocN9NYWASQWJbWm7.htm)|Ghoul Fever|
|[P2JwQEyauIftkgAZ.htm](blood-lords-bestiary-items/P2JwQEyauIftkgAZ.htm)|Share Pain|
|[qnrhodnMyKhRGpTo.htm](blood-lords-bestiary-items/qnrhodnMyKhRGpTo.htm)|Earthglide|
|[ROm20WTjjAsPDOXi.htm](blood-lords-bestiary-items/ROm20WTjjAsPDOXi.htm)|Composite Longbow|+1,striking|
|[VoSJvMRJRCq4iW4H.htm](blood-lords-bestiary-items/VoSJvMRJRCq4iW4H.htm)|Righteous Wound|
|[vt5Qg4ue9Z9dSXww.htm](blood-lords-bestiary-items/vt5Qg4ue9Z9dSXww.htm)|Bayonet Charge|
|[WDFdvCXmXpXv57rO.htm](blood-lords-bestiary-items/WDFdvCXmXpXv57rO.htm)|Ravenous Earth|
|[zo2CJTR1ec7NuCvC.htm](blood-lords-bestiary-items/zo2CJTR1ec7NuCvC.htm)|+2 Resilient Leather Armor|

## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[E3MhiIzWlFAP4xx5.htm](blood-lords-bestiary-items/E3MhiIzWlFAP4xx5.htm)|Liberate the Earth|Libérer la terre|changé|
|[NhmoLxndj4300vjf.htm](blood-lords-bestiary-items/NhmoLxndj4300vjf.htm)|Cloying Shadows|Ombres écoeurantes|changé|

## Liste des éléments vides dont le nom doit être traduit

| Fichier   | Nom (EN)    | État |
|-----------|-------------|:----:|
|[avByQ87fokjFJrlH.htm](blood-lords-bestiary-items/avByQ87fokjFJrlH.htm)|Stealth|vide|
|[I9kAte991gYU52qO.htm](blood-lords-bestiary-items/I9kAte991gYU52qO.htm)|Beak|vide|
|[n5d5od1O6okOuv05.htm](blood-lords-bestiary-items/n5d5od1O6okOuv05.htm)|Athletics|vide|
|[nJNxKg8kYps6FB6P.htm](blood-lords-bestiary-items/nJNxKg8kYps6FB6P.htm)|Diplomacy|vide|
|[TVxiSk6E2zpA1PKv.htm](blood-lords-bestiary-items/TVxiSk6E2zpA1PKv.htm)|Stealth|vide|
|[wDpqGLAG0LJHG4lV.htm](blood-lords-bestiary-items/wDpqGLAG0LJHG4lV.htm)|Stealth|vide|
|[Xq4X0gvbkHZe6r7T.htm](blood-lords-bestiary-items/Xq4X0gvbkHZe6r7T.htm)|Stealth|vide|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[01HhFrxkR7nPEB4Y.htm](blood-lords-bestiary-items/01HhFrxkR7nPEB4Y.htm)|Dish|Assiette|libre|
|[04iwDHWYCbKI46bP.htm](blood-lords-bestiary-items/04iwDHWYCbKI46bP.htm)|Ghoul Fever|Fièvre des goules|libre|
|[05J2pORn696tl5W4.htm](blood-lords-bestiary-items/05J2pORn696tl5W4.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[06BCfzYevXveZRG3.htm](blood-lords-bestiary-items/06BCfzYevXveZRG3.htm)|All-Around Vision|Vision panoramique|libre|
|[06qMU7kYZDHHRKSo.htm](blood-lords-bestiary-items/06qMU7kYZDHHRKSo.htm)|Gaseous Smash|Smash gazeux|libre|
|[0A2oL9wUXBkFmI72.htm](blood-lords-bestiary-items/0A2oL9wUXBkFmI72.htm)|Drink Blood|Boire le sang|libre|
|[0ajlAWp4Dl2A2Mc4.htm](blood-lords-bestiary-items/0ajlAWp4Dl2A2Mc4.htm)|Smite|Châtiment|libre|
|[0GLpQLe6dDBfIEqv.htm](blood-lords-bestiary-items/0GLpQLe6dDBfIEqv.htm)|Memory Shard|Éclat de mémoire|libre|
|[0hDIbO8ulF3CgT4z.htm](blood-lords-bestiary-items/0hDIbO8ulF3CgT4z.htm)|Bladed Scarf|+1,striking,thundering|Écharpe à lames de tonnerre de frappe +1|libre|
|[0I3WNM6HYK4tGzhk.htm](blood-lords-bestiary-items/0I3WNM6HYK4tGzhk.htm)|Fast Healing 15|Guérison accélérée 15|libre|
|[0J9rlUbx4oCGObeU.htm](blood-lords-bestiary-items/0J9rlUbx4oCGObeU.htm)|Dagger|+1,striking|Dague de frappe +1|libre|
|[0jJ566980I7T1Lvm.htm](blood-lords-bestiary-items/0jJ566980I7T1Lvm.htm)|Spirit Needle|Aiguille esprit|libre|
|[0LawcBuLq6bvz3YR.htm](blood-lords-bestiary-items/0LawcBuLq6bvz3YR.htm)|Consume Flesh|Dévorer la chair|libre|
|[0LSu58skyqBjAro8.htm](blood-lords-bestiary-items/0LSu58skyqBjAro8.htm)|Jaws|Mâchoire|libre|
|[0lWpDMSeWHOeidNI.htm](blood-lords-bestiary-items/0lWpDMSeWHOeidNI.htm)|Destructive Vengeance|Vindicte destructrice|libre|
|[0olHx7gZxjFk95E8.htm](blood-lords-bestiary-items/0olHx7gZxjFk95E8.htm)|Shadow Plane Lore|Connaissance du plan de l'Ombre|libre|
|[0qLV4XXhIJyka38A.htm](blood-lords-bestiary-items/0qLV4XXhIJyka38A.htm)|Personality Fragments|Fragments de personnalité|libre|
|[0qXBH3VIVEaqjJ58.htm](blood-lords-bestiary-items/0qXBH3VIVEaqjJ58.htm)|True Seeing (Constant)|Vision lucide (constant)|libre|
|[0RpyW8U1GFAP8qfw.htm](blood-lords-bestiary-items/0RpyW8U1GFAP8qfw.htm)|Form Up|Se reformer|libre|
|[0x4TKpquc5SHkuUh.htm](blood-lords-bestiary-items/0x4TKpquc5SHkuUh.htm)|Iron Claw|Griffe de fer|libre|
|[0ZOO09WrCAHhjEvd.htm](blood-lords-bestiary-items/0ZOO09WrCAHhjEvd.htm)|Necrotic Pustule|Pustule nécrotique|libre|
|[157IkusFj1wUDofI.htm](blood-lords-bestiary-items/157IkusFj1wUDofI.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[1C9t97QNnXidFZsj.htm](blood-lords-bestiary-items/1C9t97QNnXidFZsj.htm)|Thrash|Saccage|libre|
|[1ERaIV11KiskZhb9.htm](blood-lords-bestiary-items/1ERaIV11KiskZhb9.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[1hGao82GHZbkFICr.htm](blood-lords-bestiary-items/1hGao82GHZbkFICr.htm)|Clutching Hands|Mains agrippantes|libre|
|[1N4Tr8uUJisZL317.htm](blood-lords-bestiary-items/1N4Tr8uUJisZL317.htm)|Talon|Serre|libre|
|[1PIqVUQtr0qrtNKw.htm](blood-lords-bestiary-items/1PIqVUQtr0qrtNKw.htm)|Scent (Imprecise) 60 feet|Odorat 18 m (imprécis)|libre|
|[1PqiUfvdG8OopsPb.htm](blood-lords-bestiary-items/1PqiUfvdG8OopsPb.htm)|Grab|Empoignade/Agrippement|libre|
|[1SOF01NAoVFMZvSs.htm](blood-lords-bestiary-items/1SOF01NAoVFMZvSs.htm)|Sneak Attack|Attaque sournoise|libre|
|[1vSfiqei9YJPJGSP.htm](blood-lords-bestiary-items/1vSfiqei9YJPJGSP.htm)|Uncanny Necromancy|Nécromancie troublante|libre|
|[1W10esM2Gyv2thOd.htm](blood-lords-bestiary-items/1W10esM2Gyv2thOd.htm)|Bat Fangs|Crocs de chauve-souris|libre|
|[21JIzAuT9NbbwX3N.htm](blood-lords-bestiary-items/21JIzAuT9NbbwX3N.htm)|Jaws|Mâchoire|libre|
|[282VFotcGKIwl4Ix.htm](blood-lords-bestiary-items/282VFotcGKIwl4Ix.htm)|Negative Healing|Guérison négative|libre|
|[2aKMkFmn4gFQSHPE.htm](blood-lords-bestiary-items/2aKMkFmn4gFQSHPE.htm)|Divine Spontaneous Spells|Sorts divins spontanés|libre|
|[2APESXFLe2wLwX0Y.htm](blood-lords-bestiary-items/2APESXFLe2wLwX0Y.htm)|Lifesense 80 feet|Perception de la vie 24 m|libre|
|[2ASbUwJOy0sUxmDZ.htm](blood-lords-bestiary-items/2ASbUwJOy0sUxmDZ.htm)|Negative Energy Pulse|Rayon d'énergie négative|libre|
|[2ESchU6Js7pT2QaM.htm](blood-lords-bestiary-items/2ESchU6Js7pT2QaM.htm)|Pike Master|Maîtrise de la pique|libre|
|[2GevPjZZezfqRA8E.htm](blood-lords-bestiary-items/2GevPjZZezfqRA8E.htm)|Throw Rock|Projection de rochers|libre|
|[2hmm9t0udoU8DtQ3.htm](blood-lords-bestiary-items/2hmm9t0udoU8DtQ3.htm)|Tongues (Constant)|Don des langues (constant)|libre|
|[2KvJH5DlBNKZ3xwD.htm](blood-lords-bestiary-items/2KvJH5DlBNKZ3xwD.htm)|Chomp at the Bone|Morsure profonde|libre|
|[2mWEC6TWEMx7pGud.htm](blood-lords-bestiary-items/2mWEC6TWEMx7pGud.htm)|Flaming Javelin|Javeline de feu|libre|
|[2uBOm2j5cGaxr2cN.htm](blood-lords-bestiary-items/2uBOm2j5cGaxr2cN.htm)|Negative Healing|Guérison négative|libre|
|[2VjzSO1Lu2NfxSvA.htm](blood-lords-bestiary-items/2VjzSO1Lu2NfxSvA.htm)|Devastator|Dévastatrice|libre|
|[2W8gctzT96KjqgB7.htm](blood-lords-bestiary-items/2W8gctzT96KjqgB7.htm)|Wolverine's Protection|Protection du glouton|libre|
|[2xCy48XLd52czdWX.htm](blood-lords-bestiary-items/2xCy48XLd52czdWX.htm)|Gluttonous Gulp|Bouchée gloutonne|libre|
|[34ESKpFVPKD2K0fc.htm](blood-lords-bestiary-items/34ESKpFVPKD2K0fc.htm)|Glaive|+2,greaterStriking|Coutille de frappe supérieure +2|libre|
|[3ALLpVbvOIjvYxoz.htm](blood-lords-bestiary-items/3ALLpVbvOIjvYxoz.htm)|Fetid Screech|Cri fétide|libre|
|[3fIFun4nXyFVM5A0.htm](blood-lords-bestiary-items/3fIFun4nXyFVM5A0.htm)|+2 Greater Resilient Full Plate|Harnois de résilience supérieure +2|libre|
|[3fpilJg3JPYwn8F5.htm](blood-lords-bestiary-items/3fpilJg3JPYwn8F5.htm)|Negative Healing|Guérison négative|libre|
|[3GMuYwRCpwabX2Jv.htm](blood-lords-bestiary-items/3GMuYwRCpwabX2Jv.htm)|Breath of Phantasms|Souffle de fantasmes|libre|
|[3mnW0yMXJgTzv0Va.htm](blood-lords-bestiary-items/3mnW0yMXJgTzv0Va.htm)|Putrid Sap|Sève putride|libre|
|[3NHzkM0Zz6Qhyfrx.htm](blood-lords-bestiary-items/3NHzkM0Zz6Qhyfrx.htm)|Change Shape|Changement de forme|libre|
|[3q929POGtipyAAWE.htm](blood-lords-bestiary-items/3q929POGtipyAAWE.htm)|Spectral Combat|Combat spectral|libre|
|[3vodsjkVOyxE8FoE.htm](blood-lords-bestiary-items/3vodsjkVOyxE8FoE.htm)|Jaws|Mâchoire|libre|
|[3zcdAbAjTvIA8IUN.htm](blood-lords-bestiary-items/3zcdAbAjTvIA8IUN.htm)|Divine Prepared Spells|Sorts préparés divins|libre|
|[3ZCQLxhf1lgCinva.htm](blood-lords-bestiary-items/3ZCQLxhf1lgCinva.htm)|Soul Lock|Verrou d'âme|libre|
|[3ZYR6leOVw7OHPLI.htm](blood-lords-bestiary-items/3ZYR6leOVw7OHPLI.htm)|Dread Striker|Attaquant d'effroi|libre|
|[42LlXmqaBOnT8ReR.htm](blood-lords-bestiary-items/42LlXmqaBOnT8ReR.htm)|Fast Healing 10|Guérison accélérée 10|libre|
|[48j97MLA0l8mz59e.htm](blood-lords-bestiary-items/48j97MLA0l8mz59e.htm)|+1 Resilient Studded Leather Armor|Armure en cuir clouté de résilience +1|libre|
|[4aM9vFLoPyyxbgSF.htm](blood-lords-bestiary-items/4aM9vFLoPyyxbgSF.htm)|Necrotic Rot|Pourriture nécrotique|libre|
|[4faFVfkfTzv0U7Jt.htm](blood-lords-bestiary-items/4faFVfkfTzv0U7Jt.htm)|Focus Spells|Sorts focalisés|libre|
|[4fe312SpJQZW5oxm.htm](blood-lords-bestiary-items/4fe312SpJQZW5oxm.htm)|Shriek of Despair|Hurlement de désespoir|libre|
|[4iW6J0hXOfo1k46A.htm](blood-lords-bestiary-items/4iW6J0hXOfo1k46A.htm)|Claw|Griffe|libre|
|[4jCvZADBa2etfzw7.htm](blood-lords-bestiary-items/4jCvZADBa2etfzw7.htm)|Warded Ground|Zone protégée|libre|
|[4JQCEMoeJlcDamCi.htm](blood-lords-bestiary-items/4JQCEMoeJlcDamCi.htm)|Bride's Bargain|Marchandage d'épouse|libre|
|[4JTIu0Yv4Lvvber8.htm](blood-lords-bestiary-items/4JTIu0Yv4Lvvber8.htm)|Acrobatics|Acrobaties|libre|
|[4MYOaNB8XUgk2Yft.htm](blood-lords-bestiary-items/4MYOaNB8XUgk2Yft.htm)|Negative Healing|Guérison négative|libre|
|[4P32DbgWBWcMIZm5.htm](blood-lords-bestiary-items/4P32DbgWBWcMIZm5.htm)|Create Spawn|Création de rejeton|libre|
|[4p9Pceo9x0gXZt43.htm](blood-lords-bestiary-items/4p9Pceo9x0gXZt43.htm)|Low-Light Vision|vision nocturne|libre|
|[4rgmSONkt7E5Klck.htm](blood-lords-bestiary-items/4rgmSONkt7E5Klck.htm)|Negative Healing|Guérison négative|libre|
|[4SNLNMTzQxzhH1op.htm](blood-lords-bestiary-items/4SNLNMTzQxzhH1op.htm)|Defensive Dispel|Dissipation de la magie défensive|libre|
|[4T0Fw9VxC698O1jE.htm](blood-lords-bestiary-items/4T0Fw9VxC698O1jE.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[4ucQqsarDzmNeEnw.htm](blood-lords-bestiary-items/4ucQqsarDzmNeEnw.htm)|Swift Leap|Bond rapide|libre|
|[4vu636kxS4VGzdBb.htm](blood-lords-bestiary-items/4vu636kxS4VGzdBb.htm)|Aquatic Advantage|Avantage aquatique|libre|
|[4Yld2NeqEa0y1Cr3.htm](blood-lords-bestiary-items/4Yld2NeqEa0y1Cr3.htm)|Claw|Griffe|libre|
|[4YvbajJkgzdJPYie.htm](blood-lords-bestiary-items/4YvbajJkgzdJPYie.htm)|Alchemical Cartridge|Capsule alchimique|libre|
|[53nspqw7nXEZ4lce.htm](blood-lords-bestiary-items/53nspqw7nXEZ4lce.htm)|Constrict|Constriction|libre|
|[5dMHJWq6aav9pceN.htm](blood-lords-bestiary-items/5dMHJWq6aav9pceN.htm)|Dreamscythe|Faux imaginaire|libre|
|[5GXnNfNwoPtx9p4a.htm](blood-lords-bestiary-items/5GXnNfNwoPtx9p4a.htm)|Recall Memory|Se rappeler d'un souvenir|libre|
|[5JJOaC8DhIMUfGvU.htm](blood-lords-bestiary-items/5JJOaC8DhIMUfGvU.htm)|Consume Flesh|Dévorer la chair|libre|
|[5Juk1XbFenchTkEV.htm](blood-lords-bestiary-items/5Juk1XbFenchTkEV.htm)|Consecration Vulnerability|Vulnérable à la consécration|libre|
|[5K7VwTwYMLUK1GrB.htm](blood-lords-bestiary-items/5K7VwTwYMLUK1GrB.htm)|Lifesense (Imprecise) 60 feet|Perception de la vie 18 m (imprécis)|libre|
|[5KPDnYNQ8pvieHTT.htm](blood-lords-bestiary-items/5KPDnYNQ8pvieHTT.htm)|Propulsive Pestilence|Pestilence propulsive|libre|
|[5lI9YwiEJT7ostfD.htm](blood-lords-bestiary-items/5lI9YwiEJT7ostfD.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[5LvRwLFvFzENTXyc.htm](blood-lords-bestiary-items/5LvRwLFvFzENTXyc.htm)|Shudder|Vibration|libre|
|[5m851MBF5JZspKoX.htm](blood-lords-bestiary-items/5m851MBF5JZspKoX.htm)|Harness Wellspring|Dompter la source|libre|
|[5N85punRSLnEN6T1.htm](blood-lords-bestiary-items/5N85punRSLnEN6T1.htm)|+1 Status to All Saves vs. Magic|Bonus de statut de +1 aux jets de sauvegarde contre la magie|libre|
|[5nVE2Xaxc35lkDA5.htm](blood-lords-bestiary-items/5nVE2Xaxc35lkDA5.htm)|Rhino Charge|Charge du rhino|libre|
|[5PTVNpo9AAUUIzlB.htm](blood-lords-bestiary-items/5PTVNpo9AAUUIzlB.htm)|Divine Prepared Spells|Sorts divins préparés|libre|
|[5QDAOEZkWIjzDQPo.htm](blood-lords-bestiary-items/5QDAOEZkWIjzDQPo.htm)|Tenebral Form|Forme ombrale|libre|
|[5T3fpMO6YFdnpnqe.htm](blood-lords-bestiary-items/5T3fpMO6YFdnpnqe.htm)|Solar Wrath|Vengeance solaire|libre|
|[5TkBuliiyElFEvhl.htm](blood-lords-bestiary-items/5TkBuliiyElFEvhl.htm)|Warfare Lore|Connaissance de la guerre|libre|
|[5uLkIbng9ZJDAfy2.htm](blood-lords-bestiary-items/5uLkIbng9ZJDAfy2.htm)|+2 Resilient Half Plate|Armure de plate de résilience +2|libre|
|[5XkUdy0enHjVg7ZI.htm](blood-lords-bestiary-items/5XkUdy0enHjVg7ZI.htm)|Engulf|Engloutir|libre|
|[5XrRZx7q3tHang76.htm](blood-lords-bestiary-items/5XrRZx7q3tHang76.htm)|Dimension Door (At Will)|Porte dimensionnelle (à volonté)|libre|
|[5Xu7iZKSWyZYRQpl.htm](blood-lords-bestiary-items/5Xu7iZKSWyZYRQpl.htm)|Detect Alignment (Constant)|Détection de l'alignement (constant)|libre|
|[5ynSyNxRrPYLHX2S.htm](blood-lords-bestiary-items/5ynSyNxRrPYLHX2S.htm)|Divided Faith|Foi divisée|libre|
|[60HWZ84AuHWbWtEZ.htm](blood-lords-bestiary-items/60HWZ84AuHWbWtEZ.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[61D4WvEG2kFCEVEA.htm](blood-lords-bestiary-items/61D4WvEG2kFCEVEA.htm)|Create Undead (Crawling Hands, Zombies)|Création de mort-vivant (mains rampante, zombies)|libre|
|[65rn8RYmcgwdJo5p.htm](blood-lords-bestiary-items/65rn8RYmcgwdJo5p.htm)|Flurry of Blows|Déluge de coups|libre|
|[6DzMp4Z45GeyaIHY.htm](blood-lords-bestiary-items/6DzMp4Z45GeyaIHY.htm)|Sacred Hammer|Marteau sacré|libre|
|[6GKslxzXKEVmhTWd.htm](blood-lords-bestiary-items/6GKslxzXKEVmhTWd.htm)|Trundling Gore|Lit de sang|libre|
|[6HaF8caXkGjKoXgx.htm](blood-lords-bestiary-items/6HaF8caXkGjKoXgx.htm)|Corpse Eater|Mangeur de cadavres|libre|
|[6Ih85Wgt3B9pEfAR.htm](blood-lords-bestiary-items/6Ih85Wgt3B9pEfAR.htm)|Ghost Touch Arquebus|Arquebuse spectrale|libre|
|[6Ku9JLx9tFrA39RI.htm](blood-lords-bestiary-items/6Ku9JLx9tFrA39RI.htm)|Skull Charge|Charge crânienne|libre|
|[6li6INB9J0NSbkvG.htm](blood-lords-bestiary-items/6li6INB9J0NSbkvG.htm)|Reconstitution|Reconstitution|libre|
|[6mcuch5miksS88N3.htm](blood-lords-bestiary-items/6mcuch5miksS88N3.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[6PugTFezx0IQVLSv.htm](blood-lords-bestiary-items/6PugTFezx0IQVLSv.htm)|Pummeling Barrage|Rouée de projections|libre|
|[6rXVfeeAzvtD8HZZ.htm](blood-lords-bestiary-items/6rXVfeeAzvtD8HZZ.htm)|Warfare Lore|Connaissance de la guerre|libre|
|[6TE0Cc8z0z26I9ql.htm](blood-lords-bestiary-items/6TE0Cc8z0z26I9ql.htm)|At-Will Spells|Sorts à volonté|libre|
|[6TFb64fByugUXj9s.htm](blood-lords-bestiary-items/6TFb64fByugUXj9s.htm)|Gravechoke|Refoulement tombal|libre|
|[6TyxpLhpnbTQuAxd.htm](blood-lords-bestiary-items/6TyxpLhpnbTQuAxd.htm)|Hatred of Red|Haine du rouge|libre|
|[6vv0VDnWbHi1JTse.htm](blood-lords-bestiary-items/6vv0VDnWbHi1JTse.htm)|Consume Flesh|Dévorer la chair|libre|
|[6wf31nCD7PeCUntg.htm](blood-lords-bestiary-items/6wf31nCD7PeCUntg.htm)|Necromantic Revitalization|Revitalisation nécromantique|libre|
|[6yPWg0YxnzeI878G.htm](blood-lords-bestiary-items/6yPWg0YxnzeI878G.htm)|Poison Lore|Connaissance des poison|libre|
|[6zNsyu1WRFyMdwSK.htm](blood-lords-bestiary-items/6zNsyu1WRFyMdwSK.htm)|Legal Lore|Connaissance juridique|libre|
|[71d0ShYsmDyLvDr9.htm](blood-lords-bestiary-items/71d0ShYsmDyLvDr9.htm)|Boneyard Lore|Connaissance Cimetière|libre|
|[72Wd4SQEtNwJIeQT.htm](blood-lords-bestiary-items/72Wd4SQEtNwJIeQT.htm)|Soul Facsimile|Fac-simile d'âme|libre|
|[7bleURu8R5FzBx08.htm](blood-lords-bestiary-items/7bleURu8R5FzBx08.htm)|Shadow Gate|Portail ombral|libre|
|[7Iep9zf7QTyaROnC.htm](blood-lords-bestiary-items/7Iep9zf7QTyaROnC.htm)|Acid Spew|Projection d'acide|libre|
|[7kpEaJyec38TdIvd.htm](blood-lords-bestiary-items/7kpEaJyec38TdIvd.htm)|Call Spirit (Doesn't Require Secondary Casters)|Appel d'un esprit (ne nécessite pas d'incantateurs secondaires)|libre|
|[7kwxdgKmWzY7f5ii.htm](blood-lords-bestiary-items/7kwxdgKmWzY7f5ii.htm)|Poison Dart Spray|Projection de fléchette empoisonnée|libre|
|[7meJLaHrwQRpsjKP.htm](blood-lords-bestiary-items/7meJLaHrwQRpsjKP.htm)|Tongues (Constant)|Don des langues (constant)|libre|
|[7Mi919w7h4kCFTsl.htm](blood-lords-bestiary-items/7Mi919w7h4kCFTsl.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|libre|
|[7OWmmc8bIlxGJOne.htm](blood-lords-bestiary-items/7OWmmc8bIlxGJOne.htm)|Charm (At Will)|Charme (à volonté)|libre|
|[7pGWH61hagxPWRYm.htm](blood-lords-bestiary-items/7pGWH61hagxPWRYm.htm)|Cleric Domain Spells|Sorts de domaine de prêtre|libre|
|[7QucXZYzQND9xM58.htm](blood-lords-bestiary-items/7QucXZYzQND9xM58.htm)|Bonded Vessel|Récipient lié|libre|
|[7R3EkHVT54uZdAQV.htm](blood-lords-bestiary-items/7R3EkHVT54uZdAQV.htm)|Lifesense 100 feet|Perception de la vie 30 m|libre|
|[7Tej26wp3GwzSdpy.htm](blood-lords-bestiary-items/7Tej26wp3GwzSdpy.htm)|Coven|Cercle|libre|
|[7ZifpRXFQEtktT73.htm](blood-lords-bestiary-items/7ZifpRXFQEtktT73.htm)|Catharsis|Catharsis|libre|
|[82uSXqw6iZpwvIev.htm](blood-lords-bestiary-items/82uSXqw6iZpwvIev.htm)|Vomit|Vomit|libre|
|[8ctZ8B8H72ZSyj8l.htm](blood-lords-bestiary-items/8ctZ8B8H72ZSyj8l.htm)|Daring Gambit|Pari osé|libre|
|[8cZQxv2MunxRajgq.htm](blood-lords-bestiary-items/8cZQxv2MunxRajgq.htm)|Dishes|Assiettes|libre|
|[8FjfApMjKiijwosA.htm](blood-lords-bestiary-items/8FjfApMjKiijwosA.htm)|Arcane Spontaneous Spells|Sorts arcaniques spontanés|libre|
|[8gsiZ7MLrXyMrS7p.htm](blood-lords-bestiary-items/8gsiZ7MLrXyMrS7p.htm)|At-Will Spells|Sorts à volonté|libre|
|[8HBRyEneqU4oAH3V.htm](blood-lords-bestiary-items/8HBRyEneqU4oAH3V.htm)|Grab|Empoignade/Agrippement|libre|
|[8hi0eEaBxZgPq85e.htm](blood-lords-bestiary-items/8hi0eEaBxZgPq85e.htm)|Claw|Griffe|libre|
|[8NaZvJ0qWl2L1Iny.htm](blood-lords-bestiary-items/8NaZvJ0qWl2L1Iny.htm)|Breath Weapon|Arme de souffle|libre|
|[8nCobbYbkwiXGeND.htm](blood-lords-bestiary-items/8nCobbYbkwiXGeND.htm)|Sanguine Shroud|Voile sanguin|libre|
|[8qbbiiWYLOBisWao.htm](blood-lords-bestiary-items/8qbbiiWYLOBisWao.htm)|Lifesense 60 feet|Perception de la vie 18 m|libre|
|[8snFi0LNsnT8Nwzh.htm](blood-lords-bestiary-items/8snFi0LNsnT8Nwzh.htm)|+2 Resilient Standard-Grade Adamantine Full Plate|Harnois en adamantine standard résiliente +2|libre|
|[8T7KE2HThU3bdcfP.htm](blood-lords-bestiary-items/8T7KE2HThU3bdcfP.htm)|At-Will Spells|Sorts à volonté|libre|
|[8TrnL7sPP2KCO75w.htm](blood-lords-bestiary-items/8TrnL7sPP2KCO75w.htm)|Coffin Restoration|Cercueil restaurateur|libre|
|[8VIR7PpQKZ9j58op.htm](blood-lords-bestiary-items/8VIR7PpQKZ9j58op.htm)|+2 Status to All Saves vs. Mental and Possession|Bonus de statut de +2 à tous les jets de sauvegarde contre mental et possession|libre|
|[8wKFwbEYEMGokXXX.htm](blood-lords-bestiary-items/8wKFwbEYEMGokXXX.htm)|Skittering Surge|Charge hâtive|libre|
|[8WuFvwktGn2YZ6cs.htm](blood-lords-bestiary-items/8WuFvwktGn2YZ6cs.htm)|Negative Healing|Guérison négative|libre|
|[94dzQwwbqZcAARkH.htm](blood-lords-bestiary-items/94dzQwwbqZcAARkH.htm)|Negative Healing|Guérison négative|libre|
|[96iffHzRugyTWiwm.htm](blood-lords-bestiary-items/96iffHzRugyTWiwm.htm)|Dominate (At Will)|Domination (à volonté)|libre|
|[99ctaTm3fPACVDDl.htm](blood-lords-bestiary-items/99ctaTm3fPACVDDl.htm)|Jaws|Mâchoire|libre|
|[9Cc36Upj4BWfiWkA.htm](blood-lords-bestiary-items/9Cc36Upj4BWfiWkA.htm)|Quick Responses|Réaction éclair|libre|
|[9dZuXJblaIkWqW1x.htm](blood-lords-bestiary-items/9dZuXJblaIkWqW1x.htm)|Tongues (Constant)|Don des langues (constant)|libre|
|[9Eg5BAmFIPTbLCSF.htm](blood-lords-bestiary-items/9Eg5BAmFIPTbLCSF.htm)|Undead Virulence|Virulence de mort-vivant|libre|
|[9i1q4VlS4hBFVsow.htm](blood-lords-bestiary-items/9i1q4VlS4hBFVsow.htm)|Lifesense 60 feet|Perception de la vie 18 m|libre|
|[9kbgRdQ3ETNB56f2.htm](blood-lords-bestiary-items/9kbgRdQ3ETNB56f2.htm)|Water Pressure|Pression de l'eau|libre|
|[9NVL1DhhshHzfQw6.htm](blood-lords-bestiary-items/9NVL1DhhshHzfQw6.htm)|Quick Shield Block|Blocage au bouclier éclair|libre|
|[9uNaroDBSTOrGrOa.htm](blood-lords-bestiary-items/9uNaroDBSTOrGrOa.htm)|Catch Rock|Interception de rocher|libre|
|[9vuH8rOTjPJDczqQ.htm](blood-lords-bestiary-items/9vuH8rOTjPJDczqQ.htm)|Rock Tunneler|Foreur de roche|libre|
|[9WeizevdPH15V8ne.htm](blood-lords-bestiary-items/9WeizevdPH15V8ne.htm)|Repulse|Répulsion|libre|
|[9WgYnHqDFHWo4fRp.htm](blood-lords-bestiary-items/9WgYnHqDFHWo4fRp.htm)|Evasion|Évasion|libre|
|[9whNKbBSkdTgebc5.htm](blood-lords-bestiary-items/9whNKbBSkdTgebc5.htm)|Passwall (At Will)|Passe-muraille (à volonté)|libre|
|[9WJjcaGqRKE34XS6.htm](blood-lords-bestiary-items/9WJjcaGqRKE34XS6.htm)|Rend|Éventration|libre|
|[9WwvYnGWQ2A6L5Xx.htm](blood-lords-bestiary-items/9WwvYnGWQ2A6L5Xx.htm)|Collapse|Effondrement|libre|
|[9yfwQGf7Fj956UQS.htm](blood-lords-bestiary-items/9yfwQGf7Fj956UQS.htm)|Shimmer Step|Pas miroitant|libre|
|[A2tnJdS0eDxzVqEg.htm](blood-lords-bestiary-items/A2tnJdS0eDxzVqEg.htm)|Antidote (Lesser, Infused)|Antidote inférieur imprégné|libre|
|[a98WQBZnrw05bkW0.htm](blood-lords-bestiary-items/a98WQBZnrw05bkW0.htm)|Rejuvenation|Reconstruction|libre|
|[a9Q7NY2Ttinwmfv6.htm](blood-lords-bestiary-items/a9Q7NY2Ttinwmfv6.htm)|Tongue|Langue|libre|
|[AAu8bAnrWTFWWdOx.htm](blood-lords-bestiary-items/AAu8bAnrWTFWWdOx.htm)|Dramatic Return|Retour dramatique|libre|
|[ABK4UQk15fsR8KQa.htm](blood-lords-bestiary-items/ABK4UQk15fsR8KQa.htm)|At-Will Spells|Sorts à volonté|libre|
|[ADrQBVUQrZ1uGx5s.htm](blood-lords-bestiary-items/ADrQBVUQrZ1uGx5s.htm)|Tentacle Transfer|Transfer tentaculaire|libre|
|[aDsvouTNWXnx84Qo.htm](blood-lords-bestiary-items/aDsvouTNWXnx84Qo.htm)|Negative Healing|Guérison négative|libre|
|[ae3SxCMfvZIgesia.htm](blood-lords-bestiary-items/ae3SxCMfvZIgesia.htm)|Give Them the Slip|Fausser la compagnie|libre|
|[aev0i5CXNfVE9i1x.htm](blood-lords-bestiary-items/aev0i5CXNfVE9i1x.htm)|+2 Resilient Padded Armor|Armure matelassée de résilience +2|libre|
|[AfDO8pafi2fzO6LF.htm](blood-lords-bestiary-items/AfDO8pafi2fzO6LF.htm)|Ghoul Fever|Fièvre des goules|libre|
|[Ag4x9A75hMyYFEAx.htm](blood-lords-bestiary-items/Ag4x9A75hMyYFEAx.htm)|Lifesense 40 Feet|Perception de la vie 12 m|libre|
|[aGiFX1ea44sy9rU9.htm](blood-lords-bestiary-items/aGiFX1ea44sy9rU9.htm)|Improved Push|Bousculade améliorée|libre|
|[aHI8UzG3NpisiP0y.htm](blood-lords-bestiary-items/aHI8UzG3NpisiP0y.htm)|Grab|Empoignade/Agrippement|libre|
|[AHQiIz8zmOWm0z9a.htm](blood-lords-bestiary-items/AHQiIz8zmOWm0z9a.htm)|Coven|Cercle|libre|
|[aMJUziFriyOV9ggf.htm](blood-lords-bestiary-items/aMJUziFriyOV9ggf.htm)|Scattered Coven Spells|Sorts de Cercle éparpillé|libre|
|[aMuXgfA3cfKKBrtp.htm](blood-lords-bestiary-items/aMuXgfA3cfKKBrtp.htm)|Paralytic Venom|Venin paralysant|libre|
|[aNcn4jjNM7XwX7vj.htm](blood-lords-bestiary-items/aNcn4jjNM7XwX7vj.htm)|Swift Steps|Foulées rapides|libre|
|[AnNPoVPNPxdGpS1o.htm](blood-lords-bestiary-items/AnNPoVPNPxdGpS1o.htm)|Dimension Door (At Will)|Portes dimensionnelles (à volonté)|libre|
|[aOapZpsCstjGBgJY.htm](blood-lords-bestiary-items/aOapZpsCstjGBgJY.htm)|Create Undead (Doesn't Require Secondary Casters)|Création de mort-vivant (aucun incantateur secondaire requis)|libre|
|[appxgNKAFjSpzmS6.htm](blood-lords-bestiary-items/appxgNKAFjSpzmS6.htm)|Psychic Assault|Assaut psychique|libre|
|[aSH9TN7Iyc5Cb0U5.htm](blood-lords-bestiary-items/aSH9TN7Iyc5Cb0U5.htm)|Illusory Scene (At Will)|Scène illusoire (à volonté)|libre|
|[asrpf610EZDEH1n6.htm](blood-lords-bestiary-items/asrpf610EZDEH1n6.htm)|Soul Focus Gem|Gemme de focalisation d'âme|libre|
|[AtKgpEAF92gRVNeI.htm](blood-lords-bestiary-items/AtKgpEAF92gRVNeI.htm)|Negative Healing|Guérison négative|libre|
|[AVE4gXE6GDmGnEon.htm](blood-lords-bestiary-items/AVE4gXE6GDmGnEon.htm)|Negative Healing|Guérison négative|libre|
|[Aw7xYRs3bfeQ7fih.htm](blood-lords-bestiary-items/Aw7xYRs3bfeQ7fih.htm)|Fangs|Crocs|libre|
|[AX33xWHfrDzlXb4S.htm](blood-lords-bestiary-items/AX33xWHfrDzlXb4S.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[AXYnozEuP0CCywE8.htm](blood-lords-bestiary-items/AXYnozEuP0CCywE8.htm)|Broken Gems|Gemmes brisées|libre|
|[AYJEYlmYJ663pwnK.htm](blood-lords-bestiary-items/AYJEYlmYJ663pwnK.htm)|Dagger|+2,greaterStriking|Dague de frappe supérieure +2|libre|
|[AyKi1BeNLjETFGAu.htm](blood-lords-bestiary-items/AyKi1BeNLjETFGAu.htm)|Mobile Combatant|Mobilité du combattant|libre|
|[AyZjm3INSJewsJ2K.htm](blood-lords-bestiary-items/AyZjm3INSJewsJ2K.htm)|Halberd|+2,striking|Hallebarde de frappe +2|libre|
|[B3ANiSJpKPeKx484.htm](blood-lords-bestiary-items/B3ANiSJpKPeKx484.htm)|Swift Leap|Bond rapide|libre|
|[B4dvyMiNXi1vMXzz.htm](blood-lords-bestiary-items/B4dvyMiNXi1vMXzz.htm)|Paralysis|Paralysie|libre|
|[B54lYjmZMb2wQdZC.htm](blood-lords-bestiary-items/B54lYjmZMb2wQdZC.htm)|Warfare Lore|Connaissance de la guerre|libre|
|[b7x80DBcFt4v0vDn.htm](blood-lords-bestiary-items/b7x80DBcFt4v0vDn.htm)|Negative Healing|Guérison négative|libre|
|[BA2wGRTwk4sOnnGP.htm](blood-lords-bestiary-items/BA2wGRTwk4sOnnGP.htm)|Change Shape|Changement de forme|libre|
|[badAyHRqjGf2RfLO.htm](blood-lords-bestiary-items/badAyHRqjGf2RfLO.htm)|Dagger|+2,greaterStriking|Dague de frappe supérieure +2|libre|
|[bbAMD3kYtYQ8Ro7M.htm](blood-lords-bestiary-items/bbAMD3kYtYQ8Ro7M.htm)|Plane Shift (To Material Plane, Negative Energy Plane, or Shadow Plane Only)|Changement de plan (plan Matériel, plan d'Énergie négative ou plan de l'Ombre uniquement)|libre|
|[BduBR0VK9bVL5eNX.htm](blood-lords-bestiary-items/BduBR0VK9bVL5eNX.htm)|Vetalarana Vulnerabilities|Vulnérabilités d'un vétalarana|libre|
|[beAjeusjFlxVDilL.htm](blood-lords-bestiary-items/beAjeusjFlxVDilL.htm)|Talon|Serre|libre|
|[BemYYTuyMTOrShL8.htm](blood-lords-bestiary-items/BemYYTuyMTOrShL8.htm)|Collapse|Éffondrement|libre|
|[BgEjPvbQmNuq0OwY.htm](blood-lords-bestiary-items/BgEjPvbQmNuq0OwY.htm)|Gaseous Form (At Will)|Forme gazeuse (à volonté)|libre|
|[bGmv8aXwRXCXsCZp.htm](blood-lords-bestiary-items/bGmv8aXwRXCXsCZp.htm)|Feed|Alimentation|libre|
|[BHMFrv9DRVacWw7j.htm](blood-lords-bestiary-items/BHMFrv9DRVacWw7j.htm)|Constant Spells|Sorts constants|libre|
|[Binv4Tk6XcbbVrUf.htm](blood-lords-bestiary-items/Binv4Tk6XcbbVrUf.htm)|Constant Spells|Sorts constants|libre|
|[biOcJMlILER0LUTI.htm](blood-lords-bestiary-items/biOcJMlILER0LUTI.htm)|Flute|Flûte|libre|
|[BiPaWvBOjCN0qmWx.htm](blood-lords-bestiary-items/BiPaWvBOjCN0qmWx.htm)|Wizard Prepared Spells|Sorts de mage préparés|libre|
|[BjKk70s0LfhQYgkS.htm](blood-lords-bestiary-items/BjKk70s0LfhQYgkS.htm)|+1 Status to All Saves vs. Good|Bonus de statut de +1 à tous les JdS contre bon|libre|
|[bkFEZF8oJt6rVmBL.htm](blood-lords-bestiary-items/bkFEZF8oJt6rVmBL.htm)|Cooking Lore|Connaissance culinaire|libre|
|[BLauoJ2WIWrsFGfv.htm](blood-lords-bestiary-items/BLauoJ2WIWrsFGfv.htm)|Trip|Croc-en-jambe|libre|
|[BLjOZaLPnUo4yMLo.htm](blood-lords-bestiary-items/BLjOZaLPnUo4yMLo.htm)|Greater Darkvision|Vision dans le noir supérieur|libre|
|[bmmxyDFE4P8Hxgiv.htm](blood-lords-bestiary-items/bmmxyDFE4P8Hxgiv.htm)|Aquatic Ambush|Embuscade aquatique|libre|
|[BnL7zXAssLvRsvul.htm](blood-lords-bestiary-items/BnL7zXAssLvRsvul.htm)|Familiar (Rat Named Iskessous)|Familier (Rat nommé Iskessous)|libre|
|[bODMkF6HiQEvjnaj.htm](blood-lords-bestiary-items/bODMkF6HiQEvjnaj.htm)|Drain Bonded Item|Drain d'objet lié|libre|
|[bpogTlDeNIcDPRhi.htm](blood-lords-bestiary-items/bpogTlDeNIcDPRhi.htm)|Rend (Claw)|Éventration (griffes)|libre|
|[bQ9KwlZyFYWXB95l.htm](blood-lords-bestiary-items/bQ9KwlZyFYWXB95l.htm)|Fiery Spokes|Rayons de feu|libre|
|[BsGczgbZh31zNCzF.htm](blood-lords-bestiary-items/BsGczgbZh31zNCzF.htm)|Local Lore|Connaissance locale|libre|
|[BtiBFcTdtiRdw0mg.htm](blood-lords-bestiary-items/BtiBFcTdtiRdw0mg.htm)|Undead Mien|Aspect de mort-vivant|libre|
|[BuQEwWHKfdIAeWgj.htm](blood-lords-bestiary-items/BuQEwWHKfdIAeWgj.htm)|Acid Flask|Fiole d'acide|libre|
|[BvSY2UcdIiDeZpPO.htm](blood-lords-bestiary-items/BvSY2UcdIiDeZpPO.htm)|Soul Shards|Éclats d'âme|libre|
|[BwMRXsWD8i0AKeLT.htm](blood-lords-bestiary-items/BwMRXsWD8i0AKeLT.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[bY2346FMb02WfLK5.htm](blood-lords-bestiary-items/bY2346FMb02WfLK5.htm)|Panic!|Panique !|libre|
|[bYRfIo8T4JW1lFKD.htm](blood-lords-bestiary-items/bYRfIo8T4JW1lFKD.htm)|Grab|Empoignade/Agrippement|libre|
|[BYVB4SCFrBOAwNUp.htm](blood-lords-bestiary-items/BYVB4SCFrBOAwNUp.htm)|Ventriloquism (At Will)|Ventriloquisme (à volonté)|libre|
|[BzbrjnwZSVD3awuz.htm](blood-lords-bestiary-items/BzbrjnwZSVD3awuz.htm)|Sorcerer Bloodline Spells|Sorts de lignage d'ensorceleur|libre|
|[C3OYZgUpNsICPQ9Z.htm](blood-lords-bestiary-items/C3OYZgUpNsICPQ9Z.htm)|Children of the Night|Enfants de la nuit|libre|
|[C51bmvnDjDInPgH9.htm](blood-lords-bestiary-items/C51bmvnDjDInPgH9.htm)|True Seeing (Constant)|Vision lucide (constant)|libre|
|[c89WP8wLt3Svnyq3.htm](blood-lords-bestiary-items/c89WP8wLt3Svnyq3.htm)|Bolster Dead|Consolider les morts|libre|
|[C8j1c9O2VqXOJ8ZI.htm](blood-lords-bestiary-items/C8j1c9O2VqXOJ8ZI.htm)|Electrocution|Électrocution|libre|
|[C8Q6XMuXU7BL34xL.htm](blood-lords-bestiary-items/C8Q6XMuXU7BL34xL.htm)|Swift Leap|Bond rapide|libre|
|[c8suJEtDONGnDih9.htm](blood-lords-bestiary-items/c8suJEtDONGnDih9.htm)|At-Will Spells|Sorts à volonté|libre|
|[ccbErCFmGhGCVOea.htm](blood-lords-bestiary-items/ccbErCFmGhGCVOea.htm)|Invisibility (At Will, Self Only)|Invisibilité (à volonté, soi seulement)|libre|
|[cD3sKBhXsFxiROYa.htm](blood-lords-bestiary-items/cD3sKBhXsFxiROYa.htm)|Claw|Griffe|libre|
|[CdlaOXXN3HLpmzam.htm](blood-lords-bestiary-items/CdlaOXXN3HLpmzam.htm)|Scythe|+1|Faux +1|libre|
|[CErzkFNcldp2WE38.htm](blood-lords-bestiary-items/CErzkFNcldp2WE38.htm)|Graydirge Lore|Connaissance de Graydirge|libre|
|[CFQGvUvrvBt4MZmn.htm](blood-lords-bestiary-items/CFQGvUvrvBt4MZmn.htm)|Bola|+1,striking,returning|Bolas boomerang de frappe +1|libre|
|[CGM8F88qaHGfQqu8.htm](blood-lords-bestiary-items/CGM8F88qaHGfQqu8.htm)|Arcane Spontaneous Spells|Sorts arcaniques spontanés|libre|
|[chuH6s0oOex7XpFd.htm](blood-lords-bestiary-items/chuH6s0oOex7XpFd.htm)|Undead Master Link|Lien au maître mort-vivant|libre|
|[cI431Dx0ib1ik3yK.htm](blood-lords-bestiary-items/cI431Dx0ib1ik3yK.htm)|Shadow Shift|Manipulation des ombres|libre|
|[colAAfebpTFd5hUl.htm](blood-lords-bestiary-items/colAAfebpTFd5hUl.htm)|Constrict|Constriction|libre|
|[CqqRbppW3YzLhWby.htm](blood-lords-bestiary-items/CqqRbppW3YzLhWby.htm)|Harmony's Harp (Virtuoso)|Harpe d'Harmony (Virtuose)|libre|
|[CQVVgl8AKrEnT917.htm](blood-lords-bestiary-items/CQVVgl8AKrEnT917.htm)|Grab|Empoignade/Agrippement|libre|
|[crVZMBjqhd4IWodl.htm](blood-lords-bestiary-items/crVZMBjqhd4IWodl.htm)|Grab|Empoignade/Agrippement|libre|
|[cswdvwqMZbvUdHQm.htm](blood-lords-bestiary-items/cswdvwqMZbvUdHQm.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[cT3KWKRJkHa9bbfD.htm](blood-lords-bestiary-items/cT3KWKRJkHa9bbfD.htm)|Familiar|Familier|libre|
|[CTmKRBGWMooBiEl5.htm](blood-lords-bestiary-items/CTmKRBGWMooBiEl5.htm)|Coffin Restoration|Cercueil restaurateur|libre|
|[cuPF6nTQTHSvNMH5.htm](blood-lords-bestiary-items/cuPF6nTQTHSvNMH5.htm)|Athletics|Athlétisme|libre|
|[cvWs4iv6YT2XjiNA.htm](blood-lords-bestiary-items/cvWs4iv6YT2XjiNA.htm)|True Seeing (Constant)|Vision lucide (constant)|libre|
|[CvzsTrIxfGzyc75Q.htm](blood-lords-bestiary-items/CvzsTrIxfGzyc75Q.htm)|Pyrokinetic Whirlwind|Tornade pyrokinetique|libre|
|[cwvXwSSEj5ik6ZF1.htm](blood-lords-bestiary-items/cwvXwSSEj5ik6ZF1.htm)|Dominate (At Will)|Domination (à volonté)|libre|
|[CX6Zle74rzAF8AJD.htm](blood-lords-bestiary-items/CX6Zle74rzAF8AJD.htm)|Negative Healing|Guérison négative|libre|
|[cxlYWwwFeVPsqYiv.htm](blood-lords-bestiary-items/cxlYWwwFeVPsqYiv.htm)|Kabriri Lore|Connaissance de Kabriri|libre|
|[CxShG0xsLiP72AB6.htm](blood-lords-bestiary-items/CxShG0xsLiP72AB6.htm)|Counterattack|Contre-attaque|libre|
|[cY3MYfSOj2vFY35p.htm](blood-lords-bestiary-items/cY3MYfSOj2vFY35p.htm)|+2 Resilient Breastplate|Cuirasse de résilience +2|libre|
|[CYgmnellsZOTsXPx.htm](blood-lords-bestiary-items/CYgmnellsZOTsXPx.htm)|Bardic Lore|Connaissance bardique|libre|
|[Czn4l1zVeOQTQumP.htm](blood-lords-bestiary-items/Czn4l1zVeOQTQumP.htm)|Formula Book|Livre de formules|libre|
|[czYSmZ3rvLprjpZo.htm](blood-lords-bestiary-items/czYSmZ3rvLprjpZo.htm)|Arcane Spontaneous Spells|Sorts arcanique spontanés|libre|
|[D1cMaK9JDg78UTKJ.htm](blood-lords-bestiary-items/D1cMaK9JDg78UTKJ.htm)|Infused Items|Objets imprégnés|libre|
|[D2hp0VWVMMVmH5aZ.htm](blood-lords-bestiary-items/D2hp0VWVMMVmH5aZ.htm)|Stained Glass Frame|Corps de verre tâché|libre|
|[d2MUxTvu28FOtSWa.htm](blood-lords-bestiary-items/d2MUxTvu28FOtSWa.htm)|Tremorsense (Imprecise) 60 feet|Perception des vibrations 18 mètres (imprécis)|libre|
|[D3hVCC34jyaRE49l.htm](blood-lords-bestiary-items/D3hVCC34jyaRE49l.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[d3WA0s0O9v9khS93.htm](blood-lords-bestiary-items/d3WA0s0O9v9khS93.htm)|Shadow Healing|Guérison ombrale|libre|
|[d5IvrBFX9sJwE0Mf.htm](blood-lords-bestiary-items/d5IvrBFX9sJwE0Mf.htm)|Grab|Empoignade|libre|
|[D6B66UdCjYK3Ln2m.htm](blood-lords-bestiary-items/D6B66UdCjYK3Ln2m.htm)|Feast|Festin|libre|
|[D9I9Pa6Y6YFqsikI.htm](blood-lords-bestiary-items/D9I9Pa6Y6YFqsikI.htm)|Electrify|Électrification|libre|
|[DbJuTvhgPXEqgKCk.htm](blood-lords-bestiary-items/DbJuTvhgPXEqgKCk.htm)|Troop Movement|Mouvement de troupe|libre|
|[DcdmHNGBOeQzVix3.htm](blood-lords-bestiary-items/DcdmHNGBOeQzVix3.htm)|Knockdown|Renversement|libre|
|[Dd8dRATEQRg0zzmI.htm](blood-lords-bestiary-items/Dd8dRATEQRg0zzmI.htm)|At-Will Spells|Sorts à volonté|libre|
|[debIobAdwTNhKkux.htm](blood-lords-bestiary-items/debIobAdwTNhKkux.htm)|Earth Glide|Glissement de terrain|libre|
|[DEFEGOYbazU1avTn.htm](blood-lords-bestiary-items/DEFEGOYbazU1avTn.htm)|Energy Shift|Déplacement d'énergie|libre|
|[Dfiv7AQwyDEF4qXu.htm](blood-lords-bestiary-items/Dfiv7AQwyDEF4qXu.htm)|Hand|Main|libre|
|[DFO5TG9a9XSW1ZCQ.htm](blood-lords-bestiary-items/DFO5TG9a9XSW1ZCQ.htm)|Aquatic Echolocation (Precise) 120 feet|Écholocalisation aquatique 36 m précise|libre|
|[dgo5bOyuntQs7dwi.htm](blood-lords-bestiary-items/dgo5bOyuntQs7dwi.htm)|Frightful Chorus|Choeur effrayant|libre|
|[dGvioEXrVX8ZzuCR.htm](blood-lords-bestiary-items/dGvioEXrVX8ZzuCR.htm)|Jaws|Mâchoire|libre|
|[dhjqTfHmviGldgJE.htm](blood-lords-bestiary-items/dhjqTfHmviGldgJE.htm)|Jaws|Mâchoire|libre|
|[DiLDaBx2W3I051fs.htm](blood-lords-bestiary-items/DiLDaBx2W3I051fs.htm)|Anchored Soul|Âme ancrée|libre|
|[dIQeigMOdEkEQEcj.htm](blood-lords-bestiary-items/dIQeigMOdEkEQEcj.htm)|Mechitar Lore|Connaissance de Méchitar|libre|
|[dk4DK0IWdOkcnqZT.htm](blood-lords-bestiary-items/dk4DK0IWdOkcnqZT.htm)|Soul Shred|Déchirement d'âme|libre|
|[dk95kwgwdycZXgIj.htm](blood-lords-bestiary-items/dk95kwgwdycZXgIj.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[DkbSS1GajTAp7Cmg.htm](blood-lords-bestiary-items/DkbSS1GajTAp7Cmg.htm)|Death Knell (At Will)|Mise à mort (à volonté)|libre|
|[dKevXPU98KPaR2FZ.htm](blood-lords-bestiary-items/dKevXPU98KPaR2FZ.htm)|Slink in Shadows|Se fondre dans les ombres|libre|
|[dLen45IzG0PQpW9I.htm](blood-lords-bestiary-items/dLen45IzG0PQpW9I.htm)|Claws|Griffe|libre|
|[DMGAbSO3WnGvqtGP.htm](blood-lords-bestiary-items/DMGAbSO3WnGvqtGP.htm)|Lifesense 60 Feet|Perception de la vie 18 m|libre|
|[dmwxfILqNCpDimvy.htm](blood-lords-bestiary-items/dmwxfILqNCpDimvy.htm)|Occult Focus Spells|Sorts occultes focalisés|libre|
|[DNBRg2690YFrNIae.htm](blood-lords-bestiary-items/DNBRg2690YFrNIae.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[DNy8MgUnfMYMspyu.htm](blood-lords-bestiary-items/DNy8MgUnfMYMspyu.htm)|Divine Prepared Spells|Sorts divins préparés|libre|
|[DOr8AePD22yCeJqd.htm](blood-lords-bestiary-items/DOr8AePD22yCeJqd.htm)|Unyielding Block|Blocage infaillible|libre|
|[dQBobrGFUHBrrW9b.htm](blood-lords-bestiary-items/dQBobrGFUHBrrW9b.htm)|Coffin Restoration|Cercueil restaurateur|libre|
|[dqyPhiS1zVgPGlc6.htm](blood-lords-bestiary-items/dqyPhiS1zVgPGlc6.htm)|Mist Escape|Fuite brumeuse|libre|
|[DR7EKIvFEexu92yS.htm](blood-lords-bestiary-items/DR7EKIvFEexu92yS.htm)|Bardic Lore|Connaissance bardique|libre|
|[DRg8mqWXs2XtBhxq.htm](blood-lords-bestiary-items/DRg8mqWXs2XtBhxq.htm)|Breath Weapon|Arme de souffle|libre|
|[dRMCIL9tHEatSIqt.htm](blood-lords-bestiary-items/dRMCIL9tHEatSIqt.htm)|Coral Reef|Récif de corail|libre|
|[DSg26CbIfxkrLUWg.htm](blood-lords-bestiary-items/DSg26CbIfxkrLUWg.htm)|Petrifying Gaze|Regard pétrifiant|libre|
|[DsunEAp3eLhuNtzi.htm](blood-lords-bestiary-items/DsunEAp3eLhuNtzi.htm)|Frost Vial|Fiole de givre|libre|
|[Dud3hio1DduyVU9z.htm](blood-lords-bestiary-items/Dud3hio1DduyVU9z.htm)|Jaws|Mâchoire|libre|
|[dUKNUHl6zUilhtbR.htm](blood-lords-bestiary-items/dUKNUHl6zUilhtbR.htm)|Bardic Lore|Connaissance bardique|libre|
|[dvfKEC0Lh8HTPG5D.htm](blood-lords-bestiary-items/dvfKEC0Lh8HTPG5D.htm)|Predetermined Path|Chemin prédéterminé|libre|
|[DVRJwMbDdsZOtsoJ.htm](blood-lords-bestiary-items/DVRJwMbDdsZOtsoJ.htm)|Witch Hexes|Maléfices de sorcier|libre|
|[dZ7gox9bINwa4OaH.htm](blood-lords-bestiary-items/dZ7gox9bINwa4OaH.htm)|Sneak Attack|Attaque sournoise|libre|
|[E0P6jMbhRa8iA1xN.htm](blood-lords-bestiary-items/E0P6jMbhRa8iA1xN.htm)|Quick Bomber|Artificier rapide|libre|
|[E6CK2SNJqqGlCXkQ.htm](blood-lords-bestiary-items/E6CK2SNJqqGlCXkQ.htm)|Scattered Coven|Cercle éparpillé|libre|
|[e93NF0rsNxgkFJlD.htm](blood-lords-bestiary-items/e93NF0rsNxgkFJlD.htm)|Bat Fangs|Crocs de chauve-souris|libre|
|[EbbGmXFaRCF4eAbE.htm](blood-lords-bestiary-items/EbbGmXFaRCF4eAbE.htm)|Spirit Grasp|Empoignade spirituelle|libre|
|[EdMz9Oaiq1CLqKRd.htm](blood-lords-bestiary-items/EdMz9Oaiq1CLqKRd.htm)|Drain Thoughts|Drain de pensées|libre|
|[EGCqTcy88a9ny4va.htm](blood-lords-bestiary-items/EGCqTcy88a9ny4va.htm)|Wellspring Guardian|Gardien de la source|libre|
|[Ehj7DrIeqezpbZiY.htm](blood-lords-bestiary-items/Ehj7DrIeqezpbZiY.htm)|Kabriri Lore|Connaissance de Kabriri|libre|
|[eiR0Q8HSYqEypfFc.htm](blood-lords-bestiary-items/eiR0Q8HSYqEypfFc.htm)|Negative Energy Plane Lore|Connaissance du plan de l'Énergie négative|libre|
|[ejEBYnL06N44uhyz.htm](blood-lords-bestiary-items/ejEBYnL06N44uhyz.htm)|Fast Healing 10|Guérison accélérée 10|libre|
|[eJvnm2F0rgg9b9ph.htm](blood-lords-bestiary-items/eJvnm2F0rgg9b9ph.htm)|Earthbind (At Will)|Cloué à terre (à volonté)|libre|
|[EKje0rFSGasep1s9.htm](blood-lords-bestiary-items/EKje0rFSGasep1s9.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[elanF5tvTBJ8YuuO.htm](blood-lords-bestiary-items/elanF5tvTBJ8YuuO.htm)|Mist Escape|Fuite brumeuse|libre|
|[eM6PixErExqpIpFl.htm](blood-lords-bestiary-items/eM6PixErExqpIpFl.htm)|Undead Virulence|Virulence de mort-vivant|libre|
|[EML87DjTDnoNsaRk.htm](blood-lords-bestiary-items/EML87DjTDnoNsaRk.htm)|Ectoplasm|Ectoplasme|libre|
|[ENc1M6PvSbRYUsZD.htm](blood-lords-bestiary-items/ENc1M6PvSbRYUsZD.htm)|Lifesense 120 feet|Perception de la vie 36 m|libre|
|[eNJU3pr48jiva5ob.htm](blood-lords-bestiary-items/eNJU3pr48jiva5ob.htm)|+1 Status to All Saves vs. Magic|Bonus de statut de +1 aux jets de sauvegarde contre la magie|libre|
|[ENqbfe06FpxgYdZv.htm](blood-lords-bestiary-items/ENqbfe06FpxgYdZv.htm)|Drain Wand|Baguette de drainage|libre|
|[eO2CyGHELcoHIq2K.htm](blood-lords-bestiary-items/eO2CyGHELcoHIq2K.htm)|Feinting Strike|Frappe feintée|libre|
|[EoknAO1gEQrmcFDR.htm](blood-lords-bestiary-items/EoknAO1gEQrmcFDR.htm)|Champion Devotion Spells|Sorts de dévotion de champion|libre|
|[eolQZDsZ53L6WHKx.htm](blood-lords-bestiary-items/eolQZDsZ53L6WHKx.htm)|Grab|Empoignade/agrippement|libre|
|[EpLcb6aeTkuZpYrL.htm](blood-lords-bestiary-items/EpLcb6aeTkuZpYrL.htm)|Grab|Empoignade/Agrippement|libre|
|[EqnX9EshvcCBB6MQ.htm](blood-lords-bestiary-items/EqnX9EshvcCBB6MQ.htm)|Glare|Regard rageur|libre|
|[ESPzwtkLtqfGf2od.htm](blood-lords-bestiary-items/ESPzwtkLtqfGf2od.htm)|Grab|Empoignade/Agrippement|libre|
|[ETaugj7PvAqfMfFY.htm](blood-lords-bestiary-items/ETaugj7PvAqfMfFY.htm)|Change Shape|Changement de forme|libre|
|[eTHthJpqHQYbbdao.htm](blood-lords-bestiary-items/eTHthJpqHQYbbdao.htm)|At-Will Spells|Sorts à volonté|libre|
|[etYGPlnD2S131ved.htm](blood-lords-bestiary-items/etYGPlnD2S131ved.htm)|Warhammer|+1|Marteau de guerre +1|libre|
|[eu0vfX0pff1WcwcD.htm](blood-lords-bestiary-items/eu0vfX0pff1WcwcD.htm)|Light Step|Pas léger|libre|
|[EUkVu0jdkqrrm4E4.htm](blood-lords-bestiary-items/EUkVu0jdkqrrm4E4.htm)|Drink Blood|Boire le sang|libre|
|[eUNG3ALTKnNJ07Tb.htm](blood-lords-bestiary-items/eUNG3ALTKnNJ07Tb.htm)|Shadow's Heart|+3,greaterStriking|Coeur d'ombres de frappe supérieure +3|libre|
|[eunUSJmcaSUA1Fi9.htm](blood-lords-bestiary-items/eunUSJmcaSUA1Fi9.htm)|Claw|Griffe|libre|
|[EuoiIOoLzyT2GQ20.htm](blood-lords-bestiary-items/EuoiIOoLzyT2GQ20.htm)|Nirvana Lore|Connaissance du Nirvana|libre|
|[EyC3aHhtPYiA446v.htm](blood-lords-bestiary-items/EyC3aHhtPYiA446v.htm)|Freedom of Movement (Constant)|Liberté de mouvement (constant)|libre|
|[EzfHoNLmCEpOLujy.htm](blood-lords-bestiary-items/EzfHoNLmCEpOLujy.htm)|Dominate (At Will)|Domination (à volonté)|libre|
|[F0STsqdvUNVNiK6o.htm](blood-lords-bestiary-items/F0STsqdvUNVNiK6o.htm)|Alcohol Lore|Connaissance des alcools|libre|
|[f3C9hHrMHRwvWIpK.htm](blood-lords-bestiary-items/f3C9hHrMHRwvWIpK.htm)|Negative Healing|Guérison négative|libre|
|[f3rhcLIKuURVP0jS.htm](blood-lords-bestiary-items/f3rhcLIKuURVP0jS.htm)|Breath Weapon|Arme de souffle|libre|
|[F4T3gKSI3Tbpq8Hs.htm](blood-lords-bestiary-items/F4T3gKSI3Tbpq8Hs.htm)|Rend|Éventration|libre|
|[F5UPgthKtsYfPXyw.htm](blood-lords-bestiary-items/F5UPgthKtsYfPXyw.htm)|Negative Healing|Guérison négative|libre|
|[F6OCpDKvvnmezthe.htm](blood-lords-bestiary-items/F6OCpDKvvnmezthe.htm)|Shadow Lash|Fouet ombreux|libre|
|[f7UIESKIiTEKvvLV.htm](blood-lords-bestiary-items/f7UIESKIiTEKvvLV.htm)|Read Omens (At Will)|Lire les présages (à volonté)|libre|
|[f88hSlyd00c75JbX.htm](blood-lords-bestiary-items/f88hSlyd00c75JbX.htm)|Bonded Theater|Théâtre lié|libre|
|[f9txeM87LU3BpOyq.htm](blood-lords-bestiary-items/f9txeM87LU3BpOyq.htm)|Leg|Jambe|libre|
|[fa962sidQFXoub56.htm](blood-lords-bestiary-items/fa962sidQFXoub56.htm)|Troop Movement|Mouvement de troupe|libre|
|[FcRP1VtS9omSUG8F.htm](blood-lords-bestiary-items/FcRP1VtS9omSUG8F.htm)|Claw|Griffe|libre|
|[fCYACagEutwMx2Qk.htm](blood-lords-bestiary-items/fCYACagEutwMx2Qk.htm)|Swift Leap|Bond rapide|libre|
|[FD08rlVq7IYngy5F.htm](blood-lords-bestiary-items/FD08rlVq7IYngy5F.htm)|Paralysis|Paralysie|libre|
|[fdDTUkfdrPLfymVP.htm](blood-lords-bestiary-items/fdDTUkfdrPLfymVP.htm)|Rejuvenation|Reconstruction|libre|
|[fdNN4565vAXMt6aY.htm](blood-lords-bestiary-items/fdNN4565vAXMt6aY.htm)|Long-Rage Lash|Longue lanière de rage|libre|
|[FDXV1kDiELNJwJBv.htm](blood-lords-bestiary-items/FDXV1kDiELNJwJBv.htm)|Ooze Globule|Bulle de vase|libre|
|[FeYS7jRMkAWOmoVW.htm](blood-lords-bestiary-items/FeYS7jRMkAWOmoVW.htm)|Negative Healing|Guérison négative|libre|
|[fG1uAi0MuizEIvYo.htm](blood-lords-bestiary-items/fG1uAi0MuizEIvYo.htm)|Boneyard Lore|Connaissance du Cimetière|libre|
|[FGmOZc3G3Z75Baxu.htm](blood-lords-bestiary-items/FGmOZc3G3Z75Baxu.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[FhIrUHvdH53ghrey.htm](blood-lords-bestiary-items/FhIrUHvdH53ghrey.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[FIikdbDDvf2VIrx0.htm](blood-lords-bestiary-items/FIikdbDDvf2VIrx0.htm)|Hoof|Sabot|libre|
|[fiQG42UQUM5WRobZ.htm](blood-lords-bestiary-items/fiQG42UQUM5WRobZ.htm)|Statue|Statue|libre|
|[FiqlQKCM0bw3f5Po.htm](blood-lords-bestiary-items/FiqlQKCM0bw3f5Po.htm)|Sacrifice Minion|Sacrifice de sbire|libre|
|[FjGNo0QftZVw5ktJ.htm](blood-lords-bestiary-items/FjGNo0QftZVw5ktJ.htm)|Weapon Master|Maître d'armes|libre|
|[fJJrh9W87tS9w2ac.htm](blood-lords-bestiary-items/fJJrh9W87tS9w2ac.htm)|Terminal Tug|Ultime traction|libre|
|[fKSG7vK6L1fcFNmE.htm](blood-lords-bestiary-items/fKSG7vK6L1fcFNmE.htm)|Cowering Fear|Recroquevillé de terreur|libre|
|[fkwaaTtYO92MMibk.htm](blood-lords-bestiary-items/fkwaaTtYO92MMibk.htm)|Slip|Glissade|libre|
|[FLYQS6BjTNq5AGEz.htm](blood-lords-bestiary-items/FLYQS6BjTNq5AGEz.htm)|Invisibility (At Will, Self Only)|Invisibilité (à volonté, soi uniquement)|libre|
|[FMC7hpLZV3PgVZHT.htm](blood-lords-bestiary-items/FMC7hpLZV3PgVZHT.htm)|Throw Rock|Projection de rocher|libre|
|[fPmkd6AGT73H496x.htm](blood-lords-bestiary-items/fPmkd6AGT73H496x.htm)|Offensive Line|Ligne offensive|libre|
|[Fq76u14PG7hXyQiU.htm](blood-lords-bestiary-items/Fq76u14PG7hXyQiU.htm)|Lifesense 100 feet|Perception de la vie 30 m|libre|
|[FqhqPAotqaXBL4tG.htm](blood-lords-bestiary-items/FqhqPAotqaXBL4tG.htm)|Grab|Empoignade/Agrippement|libre|
|[FRNJk2LTeLbJeq9c.htm](blood-lords-bestiary-items/FRNJk2LTeLbJeq9c.htm)|Ring of Energy Resistance (Cold)|Anneau de résistance aux énergies (froid)|libre|
|[frZzK6aHaPlfGXaG.htm](blood-lords-bestiary-items/frZzK6aHaPlfGXaG.htm)|Ashen Rise|Relevé des cendres|libre|
|[fSxgPygeMG5M2MXp.htm](blood-lords-bestiary-items/fSxgPygeMG5M2MXp.htm)|Keen Eyes|Yeux perçants|libre|
|[Ft46tzCQcolhliMe.htm](blood-lords-bestiary-items/Ft46tzCQcolhliMe.htm)|Implanted Stone|Pierre implantée|libre|
|[FtCRLG4tGhwO4MCV.htm](blood-lords-bestiary-items/FtCRLG4tGhwO4MCV.htm)|Constant Spells|Sorts constants|libre|
|[fTmadVnvCEsBEJ33.htm](blood-lords-bestiary-items/fTmadVnvCEsBEJ33.htm)|Gang Up|Attaque groupée|libre|
|[fUiV7MT5Xkn73xyb.htm](blood-lords-bestiary-items/fUiV7MT5Xkn73xyb.htm)|Life-Siphoning Screech|Cri d'aspiration de vie|libre|
|[FvCV25pS2ksjpkOt.htm](blood-lords-bestiary-items/FvCV25pS2ksjpkOt.htm)|Energy Charge|Charge d'énergie|libre|
|[fVHRATRLRiaVwvEi.htm](blood-lords-bestiary-items/fVHRATRLRiaVwvEi.htm)|Focus Gaze|Focaliser le regard|libre|
|[fvNa9q4DAn2iRbMf.htm](blood-lords-bestiary-items/fvNa9q4DAn2iRbMf.htm)|Paralysis|Paralysie|libre|
|[fVuKoA2aP7K3FVDK.htm](blood-lords-bestiary-items/fVuKoA2aP7K3FVDK.htm)|Shadow Plane Lore|Connaissance du plan de l'Ombre|libre|
|[FWQHcq8LK2U5FQ6Y.htm](blood-lords-bestiary-items/FWQHcq8LK2U5FQ6Y.htm)|Rapier|+1,striking|Rapière de frappe +1|libre|
|[fxgXMCISZUXwhsc0.htm](blood-lords-bestiary-items/fxgXMCISZUXwhsc0.htm)|Arcane Prepared Spells|Sorts arcaniques préparés|libre|
|[fy8jj3KRlrN9nHWV.htm](blood-lords-bestiary-items/fy8jj3KRlrN9nHWV.htm)|Divine Spontaneous Spells|Sorts divins spontanés|libre|
|[FZnb3ifrzOk33COG.htm](blood-lords-bestiary-items/FZnb3ifrzOk33COG.htm)|Dagger|+2,striking|Dague de frappe +2|libre|
|[FZwSFqaynmTAfiNW.htm](blood-lords-bestiary-items/FZwSFqaynmTAfiNW.htm)|Countless Digits|Doigts inombrables|libre|
|[g08HVYQyr4BtWYzK.htm](blood-lords-bestiary-items/g08HVYQyr4BtWYzK.htm)|Celestial Rage|Rage céleste|libre|
|[G0TgAor4CYC6eGVt.htm](blood-lords-bestiary-items/G0TgAor4CYC6eGVt.htm)|Staff of Power|+2,greaterStriking|Bâton de surpuissance de frappe supérieure +2|libre|
|[G13LX7IIeKwacGfa.htm](blood-lords-bestiary-items/G13LX7IIeKwacGfa.htm)|Sneak Attack|Attaque sournoise|libre|
|[g1wgI7zm2jqFuxCU.htm](blood-lords-bestiary-items/g1wgI7zm2jqFuxCU.htm)|Negative Healing|Guérison négative|libre|
|[g6feQOw8bX3TaBPZ.htm](blood-lords-bestiary-items/g6feQOw8bX3TaBPZ.htm)|Shield Block|Blocage au bouclier|libre|
|[G8T53rWLkY6m5M8c.htm](blood-lords-bestiary-items/G8T53rWLkY6m5M8c.htm)|Eager Shadows|Ombres avides|libre|
|[gaMdbLxllLq5eEi4.htm](blood-lords-bestiary-items/gaMdbLxllLq5eEi4.htm)|Negative Healing|Guérison négative|libre|
|[gBes2KdIMRvLfNPI.htm](blood-lords-bestiary-items/gBes2KdIMRvLfNPI.htm)|Turn to Mist|Se changer en brume|libre|
|[GBHZ3AC0edlgl0xg.htm](blood-lords-bestiary-items/GBHZ3AC0edlgl0xg.htm)|Regeneration 20 (Deactivated by Fire or Good)|Régénération 20 (désactivée par feu et bon)|libre|
|[gBRsOlbQWqETKArY.htm](blood-lords-bestiary-items/gBRsOlbQWqETKArY.htm)|Greatsword|+1,striking|Épée à deux mains de frappe +1|libre|
|[gchsjLTMYuclTZrQ.htm](blood-lords-bestiary-items/gchsjLTMYuclTZrQ.htm)|Light Mace|+1,striking|Masse d'arme légère de frappe +1|libre|
|[GdNAQ7JcgLPnVgKu.htm](blood-lords-bestiary-items/GdNAQ7JcgLPnVgKu.htm)|+1 Status to All Saves vs. Magic|Bonus de statut de +1 aux jets de sauvegarde contre la magie|libre|
|[GdtzYQ9k1YCRdkTT.htm](blood-lords-bestiary-items/GdtzYQ9k1YCRdkTT.htm)|Discern Lies (Constant)|Détection du mensonge (constant)|libre|
|[Ge7xk8scXD0EaAM8.htm](blood-lords-bestiary-items/Ge7xk8scXD0EaAM8.htm)|Trample|Piétinement|libre|
|[GeKZKWdbolc3bPct.htm](blood-lords-bestiary-items/GeKZKWdbolc3bPct.htm)|Hagshot|Tir de guenaude|libre|
|[gElZJHj913X1orQh.htm](blood-lords-bestiary-items/gElZJHj913X1orQh.htm)|Ruinous Weapons|Armes du désastre|libre|
|[gmdCWbWvrUPmvAQb.htm](blood-lords-bestiary-items/gmdCWbWvrUPmvAQb.htm)|Arcane Prepared Spells|Sorts arcaniques préparés|libre|
|[GMS9b8N0V1JhG5Ck.htm](blood-lords-bestiary-items/GMS9b8N0V1JhG5Ck.htm)|Tremorsense (Imprecise) 100 feet|Perception des vibrations 30 m (imprécis)|libre|
|[gO9cUtoJfSHg4ICC.htm](blood-lords-bestiary-items/gO9cUtoJfSHg4ICC.htm)|Susceptible to Mockery|Sensible à la moquerie|libre|
|[GOVDOv9MOM61figq.htm](blood-lords-bestiary-items/GOVDOv9MOM61figq.htm)|Axe Vulnerability|Vulnérabilité à la hache|libre|
|[gPlcC0VpGPZxl8qj.htm](blood-lords-bestiary-items/gPlcC0VpGPZxl8qj.htm)|Vampire Weaknesses|Faiblesse des vampires|libre|
|[GPNQRs1x6LsVRl63.htm](blood-lords-bestiary-items/GPNQRs1x6LsVRl63.htm)|Sneak Attack|Attaque sournoise|libre|
|[Gr2USt2VS9frmOS5.htm](blood-lords-bestiary-items/Gr2USt2VS9frmOS5.htm)|Comet|Comète|libre|
|[gRXIPyUgE8zXmbQ0.htm](blood-lords-bestiary-items/gRXIPyUgE8zXmbQ0.htm)|Swift Sneak|Furtivité rapide|libre|
|[GUeXopmtVLYBJod9.htm](blood-lords-bestiary-items/GUeXopmtVLYBJod9.htm)|Warfare Lore|Connaissance de la guerre|libre|
|[guvVys0isH3iJ6Bj.htm](blood-lords-bestiary-items/guvVys0isH3iJ6Bj.htm)|Repel|Répulsion|libre|
|[gWgxoSsAB6FROhDD.htm](blood-lords-bestiary-items/gWgxoSsAB6FROhDD.htm)|Night's Breath|Souffle de la nuit|libre|
|[gxa1J0DgJIrJbBVO.htm](blood-lords-bestiary-items/gxa1J0DgJIrJbBVO.htm)|Shield Block|Blocage au bouclier|libre|
|[GXCBdr3Hyxac62Kg.htm](blood-lords-bestiary-items/GXCBdr3Hyxac62Kg.htm)|Guisarme|+1|Guisarme +1|libre|
|[gxjS3APksgflCuua.htm](blood-lords-bestiary-items/gxjS3APksgflCuua.htm)|Burst Free|Rupture|libre|
|[gXPQ6ZHJXh0HH9Pe.htm](blood-lords-bestiary-items/gXPQ6ZHJXh0HH9Pe.htm)|Fangs|Crocs|libre|
|[gz9FXcaHrgMEOhfr.htm](blood-lords-bestiary-items/gz9FXcaHrgMEOhfr.htm)|Scrabble|Filer|libre|
|[gzDWlSIBxZKp2vso.htm](blood-lords-bestiary-items/gzDWlSIBxZKp2vso.htm)|Claw|Griffe|libre|
|[GzJUBKMhT6vGRr4H.htm](blood-lords-bestiary-items/GzJUBKMhT6vGRr4H.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[h09xjGZs6v0YCv7X.htm](blood-lords-bestiary-items/h09xjGZs6v0YCv7X.htm)|Illusory Disguise (At Will)|Déguisement illusoire (à volonté)|libre|
|[H5DHI9VmesJMobw8.htm](blood-lords-bestiary-items/H5DHI9VmesJMobw8.htm)|Tentacle|Tentacules|libre|
|[h6er5XScms89hVpB.htm](blood-lords-bestiary-items/h6er5XScms89hVpB.htm)|Gnashing Rage|Rage grinçante|libre|
|[H7FWPNStHF3mO4Cj.htm](blood-lords-bestiary-items/H7FWPNStHF3mO4Cj.htm)|Iron Claws|Griffe de fer|libre|
|[h9JB8ESEaaUKspWp.htm](blood-lords-bestiary-items/h9JB8ESEaaUKspWp.htm)|Blessed Armaments|Armes bénies|libre|
|[ha4G0jIzcuUNT5zS.htm](blood-lords-bestiary-items/ha4G0jIzcuUNT5zS.htm)|Knockdown|Renversement|libre|
|[hAQUQuH4rotLGV93.htm](blood-lords-bestiary-items/hAQUQuH4rotLGV93.htm)|Bardic Lore|Connaissance bardique|libre|
|[hbGonvqkjN6EsLXx.htm](blood-lords-bestiary-items/hbGonvqkjN6EsLXx.htm)|Soul Consumption|Dévorer l'âme|libre|
|[hcDv1KdfwFqB0ktp.htm](blood-lords-bestiary-items/hcDv1KdfwFqB0ktp.htm)|Stupefying Touch|Contact stupéfiant|libre|
|[HCexVSR35RdvQhYi.htm](blood-lords-bestiary-items/HCexVSR35RdvQhYi.htm)|Morningstar|+2,greaterStriking|Morgenstern de frappe supérieure +2|libre|
|[HdNJ99o6qKnR4kMC.htm](blood-lords-bestiary-items/HdNJ99o6qKnR4kMC.htm)|Dominate (At Will)|Domination (à volonté)|libre|
|[He2fdrt0rsXCnXBA.htm](blood-lords-bestiary-items/He2fdrt0rsXCnXBA.htm)|Discerning Aura|Aura de discernement|libre|
|[HE98Ku359W87UKZT.htm](blood-lords-bestiary-items/HE98Ku359W87UKZT.htm)|Shrink from the Light|Dérobade à la lumière|libre|
|[HEBKt4vHGuB7ul8L.htm](blood-lords-bestiary-items/HEBKt4vHGuB7ul8L.htm)|For You, Mistress|Pour toi, Maîtresse|libre|
|[HhC2cJM8arFVeb06.htm](blood-lords-bestiary-items/HhC2cJM8arFVeb06.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[HHdOAo37LMBn63w4.htm](blood-lords-bestiary-items/HHdOAo37LMBn63w4.htm)|Impart Fears|Transmission de terreur|libre|
|[hHpTOf0Sei8VSKd5.htm](blood-lords-bestiary-items/hHpTOf0Sei8VSKd5.htm)|Invasive Thoughts|Pensées envahissantes|libre|
|[HiqjK8otiyocru6B.htm](blood-lords-bestiary-items/HiqjK8otiyocru6B.htm)|Claw|Griffe|libre|
|[HKRUeJO7jgn4QHfR.htm](blood-lords-bestiary-items/HKRUeJO7jgn4QHfR.htm)|Boneyard Lore|Connaissance du Cimetière|libre|
|[HMiv3afmnwOfZYgH.htm](blood-lords-bestiary-items/HMiv3afmnwOfZYgH.htm)|Longsword|+2,greaterStriking|Épée longue de frappe supérieure +2|libre|
|[hmt1MoNSa9wHysAp.htm](blood-lords-bestiary-items/hmt1MoNSa9wHysAp.htm)|Claw|Griffe|libre|
|[HN1tRsEbpj1qQkX9.htm](blood-lords-bestiary-items/HN1tRsEbpj1qQkX9.htm)|Negative Healing|Guérison négative|libre|
|[Hn6X5wYdDhrMum8O.htm](blood-lords-bestiary-items/Hn6X5wYdDhrMum8O.htm)|Tunneling Strike|Frappe de tunnelier|libre|
|[hQDFn0Tk685TCycZ.htm](blood-lords-bestiary-items/hQDFn0Tk685TCycZ.htm)|Rend|Éventration|libre|
|[HR1m6Xt3k6CkTXy6.htm](blood-lords-bestiary-items/HR1m6Xt3k6CkTXy6.htm)|Negative Healing|Guérison négative|libre|
|[HrJ5JhtYoyiqDvRj.htm](blood-lords-bestiary-items/HrJ5JhtYoyiqDvRj.htm)|Solar Flare|Flambée solaire|libre|
|[HtSkU4atVYKDv2Zt.htm](blood-lords-bestiary-items/HtSkU4atVYKDv2Zt.htm)|Religious Symbol of Dagon|Symbole religieux de Dagon|libre|
|[hUrIdt4350dOmm6i.htm](blood-lords-bestiary-items/hUrIdt4350dOmm6i.htm)|+1 Status to All Saves vs. Positive|Bonus de statut de +1 aux JdS contre positif|libre|
|[HV69IwFN0szdBBsc.htm](blood-lords-bestiary-items/HV69IwFN0szdBBsc.htm)|Grave Ray|Rayon de tombe|libre|
|[hVYBYBpzcXRjIEi9.htm](blood-lords-bestiary-items/hVYBYBpzcXRjIEi9.htm)|Negative Healing|Guérison négative|libre|
|[hxoZazjfCKaYd04z.htm](blood-lords-bestiary-items/hxoZazjfCKaYd04z.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[HyEl2YtD1nZIdb4K.htm](blood-lords-bestiary-items/HyEl2YtD1nZIdb4K.htm)|Grab|Empoignade/Agrippement|libre|
|[HytM2L3Eblb1yHgg.htm](blood-lords-bestiary-items/HytM2L3Eblb1yHgg.htm)|Jaws|Mâchoire|libre|
|[HywOU1idS8Ial3X9.htm](blood-lords-bestiary-items/HywOU1idS8Ial3X9.htm)|Claw|Griffe|libre|
|[I3IBzx4155jNdX79.htm](blood-lords-bestiary-items/I3IBzx4155jNdX79.htm)|Thoughtsense 100 feet|Perception des pensées 30 m|libre|
|[i8WqztrmqL8eHOTi.htm](blood-lords-bestiary-items/i8WqztrmqL8eHOTi.htm)|Magaambya Scar|Cicatrice de Magaambya|libre|
|[I95mUhgE0TD5JcvS.htm](blood-lords-bestiary-items/I95mUhgE0TD5JcvS.htm)|Body|Corps|libre|
|[ia6gTy9AbC5ZJ1eA.htm](blood-lords-bestiary-items/ia6gTy9AbC5ZJ1eA.htm)|+1 Status to All Saves vs. Magic|Bonus de statut de +1 aux jets de sauvegarde contre la magie|libre|
|[ICF4eC8QzWpKC81e.htm](blood-lords-bestiary-items/ICF4eC8QzWpKC81e.htm)|Bonds of Iron|Liens de fer|libre|
|[id0Lj5zD5tQaYREF.htm](blood-lords-bestiary-items/id0Lj5zD5tQaYREF.htm)|Subtle Presence|Présence subtile|libre|
|[iHxeyxlgp1oPCLrS.htm](blood-lords-bestiary-items/iHxeyxlgp1oPCLrS.htm)|Negative Healing|Guérison négative|libre|
|[iicNRdaPUBDrRCDd.htm](blood-lords-bestiary-items/iicNRdaPUBDrRCDd.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[IiHNRCF5KzZHArm7.htm](blood-lords-bestiary-items/IiHNRCF5KzZHArm7.htm)|Legal Lore|Connaissance de la loi|libre|
|[ijnk9yOqEZC4rtWW.htm](blood-lords-bestiary-items/ijnk9yOqEZC4rtWW.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[iLDsrk31KjsbRR8b.htm](blood-lords-bestiary-items/iLDsrk31KjsbRR8b.htm)|Prayer-Blessed Blade|Lame baignée de prière|libre|
|[iLIeDuHrwNafePwL.htm](blood-lords-bestiary-items/iLIeDuHrwNafePwL.htm)|Throw Rock|Projection de rocher|libre|
|[ILn7NeyUxioWPWpN.htm](blood-lords-bestiary-items/ILn7NeyUxioWPWpN.htm)|Improved Grab|Empoignade/agrippement amélioré|libre|
|[iLV83KWLAubvTCHR.htm](blood-lords-bestiary-items/iLV83KWLAubvTCHR.htm)|Children of the Deep|Enfants des profondeurs|libre|
|[inmnuN3VcUuUDTaX.htm](blood-lords-bestiary-items/inmnuN3VcUuUDTaX.htm)|Reaping Scythe|Faux récolteuse|libre|
|[IO3T4Bekj7HVtkG3.htm](blood-lords-bestiary-items/IO3T4Bekj7HVtkG3.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[Iph95fu5IhW4Kq8b.htm](blood-lords-bestiary-items/Iph95fu5IhW4Kq8b.htm)|Constant Spells|Sorts constants|libre|
|[IpL3t43EPH59ZjEM.htm](blood-lords-bestiary-items/IpL3t43EPH59ZjEM.htm)|Launch Darts|Projection de fléchettes|libre|
|[iPOOFJx3TfTzCwx5.htm](blood-lords-bestiary-items/iPOOFJx3TfTzCwx5.htm)|Sallowshore Lore|Connaissance de Rivenoyé|libre|
|[IQO5rByWMQazmS0o.htm](blood-lords-bestiary-items/IQO5rByWMQazmS0o.htm)|Swallow Whole|Gober|libre|
|[iR8i2JeM3fL97FdW.htm](blood-lords-bestiary-items/iR8i2JeM3fL97FdW.htm)|Negative Healing|Guérison négative|libre|
|[isBAFFJUmEHE7wYA.htm](blood-lords-bestiary-items/isBAFFJUmEHE7wYA.htm)|Ghostly Images|Images fantomatiques|libre|
|[IsGLZSTgHv6OcM8A.htm](blood-lords-bestiary-items/IsGLZSTgHv6OcM8A.htm)|Stick Support|Support du bâton|libre|
|[IV7Z19dohR35I4Kg.htm](blood-lords-bestiary-items/IV7Z19dohR35I4Kg.htm)|Children of the Night|Enfants de la nuit|libre|
|[izKWeeQL9TTFKO7O.htm](blood-lords-bestiary-items/izKWeeQL9TTFKO7O.htm)|Tenebral Form|Forme ombrale|libre|
|[iZSMsCkLfjA3XKgs.htm](blood-lords-bestiary-items/iZSMsCkLfjA3XKgs.htm)|Earthbind (At Will)|Cloué à terre (à volonté)|libre|
|[j2PU4zgJeByDVbMK.htm](blood-lords-bestiary-items/j2PU4zgJeByDVbMK.htm)|Battering Blow|Impact déstabilisant|libre|
|[j2SKJWI2rOeIZs43.htm](blood-lords-bestiary-items/j2SKJWI2rOeIZs43.htm)|Surprise Attacker|Agresseur surprise|libre|
|[J3qS5qTu1HdzSECb.htm](blood-lords-bestiary-items/J3qS5qTu1HdzSECb.htm)|Bone Shard|Éclat d'os|libre|
|[J4NZUXqQJvKOlzXK.htm](blood-lords-bestiary-items/J4NZUXqQJvKOlzXK.htm)|Claw|Griffe|libre|
|[ja6ymHee8MuA7dWg.htm](blood-lords-bestiary-items/ja6ymHee8MuA7dWg.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[JauWWtIX00GMEnDF.htm](blood-lords-bestiary-items/JauWWtIX00GMEnDF.htm)|Claw|Griffe|libre|
|[jBZHpmjyIoQugMLT.htm](blood-lords-bestiary-items/jBZHpmjyIoQugMLT.htm)|Impromptu Props|Accessoires improvisés|libre|
|[jc8sf181cpfZ0al3.htm](blood-lords-bestiary-items/jc8sf181cpfZ0al3.htm)|Faith Bound|Lié à la foi|libre|
|[JCzTpefevVKXvSA1.htm](blood-lords-bestiary-items/JCzTpefevVKXvSA1.htm)|Tail|Queue|libre|
|[jEaaaGvLhtd91XlV.htm](blood-lords-bestiary-items/jEaaaGvLhtd91XlV.htm)|Negative Healing|Guérison négative|libre|
|[jeeUL7xXwHB3iIlv.htm](blood-lords-bestiary-items/jeeUL7xXwHB3iIlv.htm)|Crescendo of Claws|Griffes en créscendo|libre|
|[jeVTUAjLCW0Tbgfl.htm](blood-lords-bestiary-items/jeVTUAjLCW0Tbgfl.htm)|Coordinated Assault|Assaut coordonné|libre|
|[JFG2hh7HRU5iSUSF.htm](blood-lords-bestiary-items/JFG2hh7HRU5iSUSF.htm)|Academia Lore|Connaissance Académie|libre|
|[JfH9CSAknYYv3bDg.htm](blood-lords-bestiary-items/JfH9CSAknYYv3bDg.htm)|Kneel!|À genoux !|libre|
|[JgSmuAF0Y7WGZqxZ.htm](blood-lords-bestiary-items/JgSmuAF0Y7WGZqxZ.htm)|Paralysis|Paralysie|libre|
|[JH4hWbJhCLyGJ7mC.htm](blood-lords-bestiary-items/JH4hWbJhCLyGJ7mC.htm)|Putrid Blast|Coup sonique putride|libre|
|[JhNwGxbvu1EWosRG.htm](blood-lords-bestiary-items/JhNwGxbvu1EWosRG.htm)|Spirit Receptacle|Réceptacle de l'esprit|libre|
|[JjRPRgYR8ZyIATIY.htm](blood-lords-bestiary-items/JjRPRgYR8ZyIATIY.htm)|Mob Mentality|Mentalité de foule|libre|
|[JMxq3ADBC01HJnMz.htm](blood-lords-bestiary-items/JMxq3ADBC01HJnMz.htm)|Death Roll|Tournoiement mortel|libre|
|[JmyxIQQcJZ0SdqMM.htm](blood-lords-bestiary-items/JmyxIQQcJZ0SdqMM.htm)|Bone Key|Clé en os|libre|
|[JO12DoQwwF5Yxne2.htm](blood-lords-bestiary-items/JO12DoQwwF5Yxne2.htm)|Jaws|Mâchoire|libre|
|[jpiVScBxbZhkvpcT.htm](blood-lords-bestiary-items/jpiVScBxbZhkvpcT.htm)|Create Spawn|Création de rejeton|libre|
|[jprnheEoj1wr3ahE.htm](blood-lords-bestiary-items/jprnheEoj1wr3ahE.htm)|Draconic Momentum|Impulsion draconique|libre|
|[JPzFNUgmyYRfS2NV.htm](blood-lords-bestiary-items/JPzFNUgmyYRfS2NV.htm)|+1 Leather Armor|Armure en cuir +1|libre|
|[Jq2usNoNnsU6K6FR.htm](blood-lords-bestiary-items/Jq2usNoNnsU6K6FR.htm)|Sneak Attack|Attaque sournoise|libre|
|[JqOGyuof22phYfx8.htm](blood-lords-bestiary-items/JqOGyuof22phYfx8.htm)|Catch Rock|Interception de rocher|libre|
|[jrdRgU4eOOLxBMDy.htm](blood-lords-bestiary-items/jrdRgU4eOOLxBMDy.htm)|Flail|+2,greaterStriking,corrosive|Fléau corrosive de frappe supérieure +2|libre|
|[jRpiPUidZcaGDNG3.htm](blood-lords-bestiary-items/jRpiPUidZcaGDNG3.htm)|Greater Flaming Longsword|Épée longue Enflammée supérieure|libre|
|[JSLbN4VSOWuOMNel.htm](blood-lords-bestiary-items/JSLbN4VSOWuOMNel.htm)|Force Barrier|Barrière de force|libre|
|[jSYDiFX6EjzvYaVs.htm](blood-lords-bestiary-items/jSYDiFX6EjzvYaVs.htm)|Malevolent Mishaps|Imprévu malveillant|libre|
|[jtIWa6yULRgZnYiY.htm](blood-lords-bestiary-items/jtIWa6yULRgZnYiY.htm)|Enlightened Mind|Esprit éclairé|libre|
|[juHsHlmk5cqYFeIe.htm](blood-lords-bestiary-items/juHsHlmk5cqYFeIe.htm)|Swift Leap|Bond rapide|libre|
|[JUjA711m16vM33Hh.htm](blood-lords-bestiary-items/JUjA711m16vM33Hh.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[jUR7Ydda63BBMAlR.htm](blood-lords-bestiary-items/jUR7Ydda63BBMAlR.htm)|Implanted Stone|Pierre implantée|libre|
|[jVbSRflcaJqmTaim.htm](blood-lords-bestiary-items/jVbSRflcaJqmTaim.htm)|Coven|Cercle|libre|
|[jVdZlNSsJtQ7SNRD.htm](blood-lords-bestiary-items/jVdZlNSsJtQ7SNRD.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[JVMxk9KSZggsKduC.htm](blood-lords-bestiary-items/JVMxk9KSZggsKduC.htm)|Occult Prepared Spells|Sorts occultes préparés|libre|
|[jW0IBuqnVeRIa4tt.htm](blood-lords-bestiary-items/jW0IBuqnVeRIa4tt.htm)|Aquatic Assault|Assaut aquatique|libre|
|[jxrNMuWTnIHet9nN.htm](blood-lords-bestiary-items/jxrNMuWTnIHet9nN.htm)|Claw|Griffe|libre|
|[K1EIyYx89R3Zys21.htm](blood-lords-bestiary-items/K1EIyYx89R3Zys21.htm)|Shadowy Pop|Éclatement|libre|
|[K1Z8qdWYkQ3TlWN3.htm](blood-lords-bestiary-items/K1Z8qdWYkQ3TlWN3.htm)|Claw|Griffe|libre|
|[K2vHWqScxNJnWnUH.htm](blood-lords-bestiary-items/K2vHWqScxNJnWnUH.htm)|Great Cleave|Estafilade supérieure|libre|
|[k4X0vQBrs8WZ3tEV.htm](blood-lords-bestiary-items/k4X0vQBrs8WZ3tEV.htm)|Wicked Blow|Coup malfaisant|libre|
|[k8toE9c0iSEs0pLa.htm](blood-lords-bestiary-items/k8toE9c0iSEs0pLa.htm)|Lance|+1,striking|Lance de frappe +1|libre|
|[KaIZP5d1PdMuMefE.htm](blood-lords-bestiary-items/KaIZP5d1PdMuMefE.htm)|Shadow Plane Lore|Connaissance du plan de l'Ombre|libre|
|[kB4ioemAlFdlk3ao.htm](blood-lords-bestiary-items/kB4ioemAlFdlk3ao.htm)|Longspear|+3,greaterStriking,wounding|Pique sanglante de frappe supérieure +3|libre|
|[KBBl3TjXFlMDvrSH.htm](blood-lords-bestiary-items/KBBl3TjXFlMDvrSH.htm)|Fast Swallow|Engloutissement rapide|libre|
|[Kc81CLKWLtttWVvq.htm](blood-lords-bestiary-items/Kc81CLKWLtttWVvq.htm)|Gut Lash|Fouet d'intestin|libre|
|[kDogI9KtrI66DwNf.htm](blood-lords-bestiary-items/kDogI9KtrI66DwNf.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[KE51uvHdZPBUiOMr.htm](blood-lords-bestiary-items/KE51uvHdZPBUiOMr.htm)|War Razor|+2,greaterStriking|Rasoir de combat de frappe supérieure +2|libre|
|[KgQGSu7qKdX8dRCD.htm](blood-lords-bestiary-items/KgQGSu7qKdX8dRCD.htm)|Tearing Hooks|Crochets à déchirement|libre|
|[KilTEDOADZehIMO5.htm](blood-lords-bestiary-items/KilTEDOADZehIMO5.htm)|Sneak Attack|Attaque d'opportunité|libre|
|[kJlhjUzKPHduCf9G.htm](blood-lords-bestiary-items/kJlhjUzKPHduCf9G.htm)|Religious Symbol of Urgathoa|Symbole religieux d'Urgathoa|libre|
|[KjrOcolAfwTjjmgE.htm](blood-lords-bestiary-items/KjrOcolAfwTjjmgE.htm)|Jaws|Mâchoire|libre|
|[KJypjv7i8jsU0cW5.htm](blood-lords-bestiary-items/KJypjv7i8jsU0cW5.htm)|Corpsekiller|Tueur de cadavre|libre|
|[Kk9IagMOaeS2CfE0.htm](blood-lords-bestiary-items/Kk9IagMOaeS2CfE0.htm)|Slam|Claque|libre|
|[kkfXEqqrcMamJbPx.htm](blood-lords-bestiary-items/kkfXEqqrcMamJbPx.htm)|Foul Temptation|Vile tentation|libre|
|[knmBQ80Y2ZgY1jrN.htm](blood-lords-bestiary-items/knmBQ80Y2ZgY1jrN.htm)|Arcane Spontaneous Spells|Sorts arcaniques spontanés|libre|
|[Kp1tPvhmT2VzW9eU.htm](blood-lords-bestiary-items/Kp1tPvhmT2VzW9eU.htm)|Occult Prepared Spells|Sorts occultes préparés|libre|
|[KrFvNI4v5SKUZTxT.htm](blood-lords-bestiary-items/KrFvNI4v5SKUZTxT.htm)|Push|Bousculade|libre|
|[KRJrlJxtXYH93rOd.htm](blood-lords-bestiary-items/KRJrlJxtXYH93rOd.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[KsRMWCyt0jm30hK8.htm](blood-lords-bestiary-items/KsRMWCyt0jm30hK8.htm)|Lingering Ichor|Suppuration persistante|libre|
|[kUaxDs7UA7r6Hyc4.htm](blood-lords-bestiary-items/kUaxDs7UA7r6Hyc4.htm)|Icy Blast|Souffle glacial|libre|
|[KULBffXE7IRa4JWU.htm](blood-lords-bestiary-items/KULBffXE7IRa4JWU.htm)|Ghost Bane|Fléau spectral|libre|
|[kupACwSIdVRBzgNC.htm](blood-lords-bestiary-items/kupACwSIdVRBzgNC.htm)|Rock|Rocher|libre|
|[KuQ5KZzju8qEC4Is.htm](blood-lords-bestiary-items/KuQ5KZzju8qEC4Is.htm)|Spew Ectoplasm|Crachat d'ectoplasme|libre|
|[kVxaY1jO3IzoF77N.htm](blood-lords-bestiary-items/kVxaY1jO3IzoF77N.htm)|Change Shape|Changement de forme|libre|
|[KxUyLNXlkrlgS8fN.htm](blood-lords-bestiary-items/KxUyLNXlkrlgS8fN.htm)|Create Spawn|Création de rejeton|libre|
|[KYGkXasFEDSHzvXF.htm](blood-lords-bestiary-items/KYGkXasFEDSHzvXF.htm)|Focus Gaze|Focaliser le regard|libre|
|[KZigrcVlPz6Nmy4C.htm](blood-lords-bestiary-items/KZigrcVlPz6Nmy4C.htm)|Hooded Robe|Robe à capuchon|libre|
|[Kzn8mOyC21FguEFN.htm](blood-lords-bestiary-items/Kzn8mOyC21FguEFN.htm)|Timely Boast|Vantardise opportune|libre|
|[L0WhknLHZXfnNuhE.htm](blood-lords-bestiary-items/L0WhknLHZXfnNuhE.htm)|Divine Spontaneous Spells|Sorts divins spontanés|libre|
|[L4OuRUydE98iw6Uv.htm](blood-lords-bestiary-items/L4OuRUydE98iw6Uv.htm)|Bone Shard|Éclat d'os|libre|
|[L5mllfFFTWtYVmsl.htm](blood-lords-bestiary-items/L5mllfFFTWtYVmsl.htm)|Clairaudience (At Will)|Clairaudience (à volonté)|libre|
|[l6kYTqIsV3sGU5W9.htm](blood-lords-bestiary-items/l6kYTqIsV3sGU5W9.htm)|Shadow Plane Lore|Connaissance du plan de l'Ombre|libre|
|[l8kqKRoSgAHqu4qD.htm](blood-lords-bestiary-items/l8kqKRoSgAHqu4qD.htm)|Negative Healing|Guérison négative|libre|
|[L9n3mpJyFOVILSD8.htm](blood-lords-bestiary-items/L9n3mpJyFOVILSD8.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[LAA0XJePYKC5IgZv.htm](blood-lords-bestiary-items/LAA0XJePYKC5IgZv.htm)|Energy Ward|Protection d'énergie|libre|
|[Lc4tlNAUAYeMGoCb.htm](blood-lords-bestiary-items/Lc4tlNAUAYeMGoCb.htm)|Occult Prepared Spells|Sorts occulte préparés|libre|
|[LcatK42VYJ7RFHTo.htm](blood-lords-bestiary-items/LcatK42VYJ7RFHTo.htm)|Aquatic Ambush|Embuscade aquatique|libre|
|[LcF7XaYoS96nk8cl.htm](blood-lords-bestiary-items/LcF7XaYoS96nk8cl.htm)|Negative Healing|Guérison négative|libre|
|[lcIYzHiBK0GzYkw2.htm](blood-lords-bestiary-items/lcIYzHiBK0GzYkw2.htm)|At-Will Spells|Sorts à volonté|libre|
|[Le02eHQ1ZlJ9JV2Z.htm](blood-lords-bestiary-items/Le02eHQ1ZlJ9JV2Z.htm)|Constant Spells|Sorts constants|libre|
|[LEXVAisQOLgbK4DA.htm](blood-lords-bestiary-items/LEXVAisQOLgbK4DA.htm)|Disturbing Wail|Lamentation perturbante|libre|
|[lf93wWbmjIjfbzTh.htm](blood-lords-bestiary-items/lf93wWbmjIjfbzTh.htm)|Misdirection (At Will; Self Only)|Détection faussée (à volonté, soi uniquement)|libre|
|[lfixSUcyvIdrZqF1.htm](blood-lords-bestiary-items/lfixSUcyvIdrZqF1.htm)|Soul Ward|Armure d'âmes|libre|
|[lFpriGpGC4mH6iZD.htm](blood-lords-bestiary-items/lFpriGpGC4mH6iZD.htm)|Horn|Corne|libre|
|[lGLvct1KPSexucot.htm](blood-lords-bestiary-items/lGLvct1KPSexucot.htm)|Wing Buffet|Coup d'aile|libre|
|[lhih0FvCkJCECqjc.htm](blood-lords-bestiary-items/lhih0FvCkJCECqjc.htm)|At-Will Spells|Sorts à volonté|libre|
|[LIHCV87HXOK2X6n6.htm](blood-lords-bestiary-items/LIHCV87HXOK2X6n6.htm)|Root|Racine|libre|
|[LisRIk60NDO1Shr9.htm](blood-lords-bestiary-items/LisRIk60NDO1Shr9.htm)|Turn to Mist|Se changer en brume|libre|
|[LjKCJW6TRP0p5dkE.htm](blood-lords-bestiary-items/LjKCJW6TRP0p5dkE.htm)|Dagger|+2,greaterStriking|Dague de frappe supérieure +2|libre|
|[lkeLIkdbCDGg9ina.htm](blood-lords-bestiary-items/lkeLIkdbCDGg9ina.htm)|Fascinating Facets|Facettes fascinantes|libre|
|[lMBk39gtWXlJa2jm.htm](blood-lords-bestiary-items/lMBk39gtWXlJa2jm.htm)|Troop Defenses|Défense de troupe|libre|
|[lnaRP9rGL0FvWqft.htm](blood-lords-bestiary-items/lnaRP9rGL0FvWqft.htm)|Negative Healing|Guérison négative|libre|
|[LnZuT28n4FunhrWq.htm](blood-lords-bestiary-items/LnZuT28n4FunhrWq.htm)|Shadow Shift|Manipulation des ombres|libre|
|[LOud6TqmTefPKQna.htm](blood-lords-bestiary-items/LOud6TqmTefPKQna.htm)|Tongues (Constant)|Don des langues (constant)|libre|
|[LP83nqMdxD5uhnwW.htm](blood-lords-bestiary-items/LP83nqMdxD5uhnwW.htm)|Grave-Bound Guts|Ventre lié à la tombe|libre|
|[LpV8movSEB2EyosK.htm](blood-lords-bestiary-items/LpV8movSEB2EyosK.htm)|Settlement Bound|Lié au village|libre|
|[lqLxZLzDQ2kV4c78.htm](blood-lords-bestiary-items/lqLxZLzDQ2kV4c78.htm)|Slip|Glissade|libre|
|[lQqtLv22WEtMZ5Xn.htm](blood-lords-bestiary-items/lQqtLv22WEtMZ5Xn.htm)|Sewing Lore|Connaissance couture|libre|
|[lr6GDeCvyL32Cupe.htm](blood-lords-bestiary-items/lr6GDeCvyL32Cupe.htm)|Spotlight|Lumière de projecteur|libre|
|[LRWGmK7Fyfv4B3n4.htm](blood-lords-bestiary-items/LRWGmK7Fyfv4B3n4.htm)|Acid Flask (Moderate, Infused)|Fiole d'acide modérée imprégnée|libre|
|[LsouI25tE5V0xDXo.htm](blood-lords-bestiary-items/LsouI25tE5V0xDXo.htm)|Change Shape|Changement de forme|libre|
|[lvjyAr09tNp3BfPF.htm](blood-lords-bestiary-items/lvjyAr09tNp3BfPF.htm)|Bonds of Iron|Liens de fer|libre|
|[LvlP5RIyqPdNNiK6.htm](blood-lords-bestiary-items/LvlP5RIyqPdNNiK6.htm)|Turn to Mist|Se changer en brume|libre|
|[lvyX8fE28Dyfd4Qs.htm](blood-lords-bestiary-items/lvyX8fE28Dyfd4Qs.htm)|Battering Defense|Défense impactante|libre|
|[Ly1nfavSXV2D9LGa.htm](blood-lords-bestiary-items/Ly1nfavSXV2D9LGa.htm)|Greatsword|+1,striking,wounding|Épée longue sanglante de frappe +1|libre|
|[lY6bc6eOyJxPxYv4.htm](blood-lords-bestiary-items/lY6bc6eOyJxPxYv4.htm)|Beak|Bec|libre|
|[LZ7HHMG2bRktGVHP.htm](blood-lords-bestiary-items/LZ7HHMG2bRktGVHP.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[m5wLNDF9w0hkOQsc.htm](blood-lords-bestiary-items/m5wLNDF9w0hkOQsc.htm)|Assorted Jewlery|Assortiment de bijoux|libre|
|[M7QIVgVr31PK0otX.htm](blood-lords-bestiary-items/M7QIVgVr31PK0otX.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[M8mWSrNnMsNwZzlQ.htm](blood-lords-bestiary-items/M8mWSrNnMsNwZzlQ.htm)|Devastating Blast|Déflagration dévastatrice|libre|
|[May8pisi3prNRNuU.htm](blood-lords-bestiary-items/May8pisi3prNRNuU.htm)|Spirit Touch|Contact spirituel|libre|
|[MBD5CXHYozTcL56G.htm](blood-lords-bestiary-items/MBD5CXHYozTcL56G.htm)|Surprise Attack|Attaque surprise|libre|
|[mc7S9a4Bp8WEv5k8.htm](blood-lords-bestiary-items/mc7S9a4Bp8WEv5k8.htm)|Flesh Tearer|Déchireur de chair|libre|
|[McgflGFq2HTlL5Kz.htm](blood-lords-bestiary-items/McgflGFq2HTlL5Kz.htm)|Incessant Chattering|Claquement incessants|libre|
|[MdACG4DYX6EuKLG6.htm](blood-lords-bestiary-items/MdACG4DYX6EuKLG6.htm)|Claw|Griffe|libre|
|[mEncHvkwzXvMWRkR.htm](blood-lords-bestiary-items/mEncHvkwzXvMWRkR.htm)|Jutting Bones|Protubérance osseuse|libre|
|[mGCpHm2aFPjf7PRP.htm](blood-lords-bestiary-items/mGCpHm2aFPjf7PRP.htm)|Drain Corpse|Drain de cadavre|libre|
|[mgT4uqLXvMX58B8B.htm](blood-lords-bestiary-items/mgT4uqLXvMX58B8B.htm)|Tremorsense (Precise) 60 feet|Perception des vibrations 18 m précis|libre|
|[MhEQ2y3BFnzwJU1A.htm](blood-lords-bestiary-items/MhEQ2y3BFnzwJU1A.htm)|Bellow Soot|Soufflet de suie|libre|
|[MhguUjAfRdvejaez.htm](blood-lords-bestiary-items/MhguUjAfRdvejaez.htm)|Spiked Chain|+1,striking|Chaîne cloutée de frappe +1|libre|
|[Mip50CmNjfFNAO5Z.htm](blood-lords-bestiary-items/Mip50CmNjfFNAO5Z.htm)|Steady Spellcasting|Incantation fiable|libre|
|[MiQAl2p4nu6PpZbe.htm](blood-lords-bestiary-items/MiQAl2p4nu6PpZbe.htm)|Jaws|Mâchoire|libre|
|[mJOma9LjDpAHTEKX.htm](blood-lords-bestiary-items/mJOma9LjDpAHTEKX.htm)|Claw|Griffe|libre|
|[MlsNgSnDhpcgOdxw.htm](blood-lords-bestiary-items/MlsNgSnDhpcgOdxw.htm)|Discharge Glyph|Décharge de glyphe|libre|
|[mMhSDSQ8ATljWw0G.htm](blood-lords-bestiary-items/mMhSDSQ8ATljWw0G.htm)|Negative Healing|Guérison négative|libre|
|[MNHpEVRoJVeCUJqK.htm](blood-lords-bestiary-items/MNHpEVRoJVeCUJqK.htm)|Negative Healing|Guérison négative|libre|
|[mnz93nEddAvCEvbl.htm](blood-lords-bestiary-items/mnz93nEddAvCEvbl.htm)|Shield Block|Blocage au bouclier|libre|
|[mOf0N5cPqWpR53Kq.htm](blood-lords-bestiary-items/mOf0N5cPqWpR53Kq.htm)|Scattered Coven Spells|Sorts de Cercle éparpillé|libre|
|[MPmZOFjO9fqEYfCT.htm](blood-lords-bestiary-items/MPmZOFjO9fqEYfCT.htm)|+1 Leather Armor|Armure en cuir +1|libre|
|[MPXalnrzvj1m64TV.htm](blood-lords-bestiary-items/MPXalnrzvj1m64TV.htm)|Children of the Night|Enfants de la nuit|libre|
|[MQZC5UNddD43Rc4o.htm](blood-lords-bestiary-items/MQZC5UNddD43Rc4o.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[MrDa1bcvphzw2x9Z.htm](blood-lords-bestiary-items/MrDa1bcvphzw2x9Z.htm)|Cowl of Shadow|Ombres recouvrantes|libre|
|[mTd9vcCbXt7R4ayp.htm](blood-lords-bestiary-items/mTd9vcCbXt7R4ayp.htm)|Darkness (At Will)|Ténèbres (à volonté)|libre|
|[mTghTKnayuIycwJ0.htm](blood-lords-bestiary-items/mTghTKnayuIycwJ0.htm)|Sacrilegious Aura|Aura sacrilège|libre|
|[Mu4E1f0rQtPL4nrj.htm](blood-lords-bestiary-items/Mu4E1f0rQtPL4nrj.htm)|Skull Twist|Tortillon|libre|
|[MvfjNyi77WG96cFf.htm](blood-lords-bestiary-items/MvfjNyi77WG96cFf.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[MVJzWbiRIqCaCA3v.htm](blood-lords-bestiary-items/MVJzWbiRIqCaCA3v.htm)|Infuse Staff|Bâton imprégné|libre|
|[mVR4VRFVAEwUof36.htm](blood-lords-bestiary-items/mVR4VRFVAEwUof36.htm)|Divine Focus Spells|Sorts divins focalisés|libre|
|[MwKqf02y9DLE2s40.htm](blood-lords-bestiary-items/MwKqf02y9DLE2s40.htm)|Shield Spikes|+2,striking|Pointes de bouclier de frappe +2|libre|
|[mXXfXbsTHMYcdUNa.htm](blood-lords-bestiary-items/mXXfXbsTHMYcdUNa.htm)|At-Will Spells|Sorts à volonté|libre|
|[my4O8nsKYudMLUB1.htm](blood-lords-bestiary-items/my4O8nsKYudMLUB1.htm)|Cold Beyond Cold|Au-delà du froid|libre|
|[mYimqA1rv1XDY2mu.htm](blood-lords-bestiary-items/mYimqA1rv1XDY2mu.htm)|Secret Eater Lore|Connaissance du Mangeur secret|libre|
|[MZpcWVWMBx55HOTG.htm](blood-lords-bestiary-items/MZpcWVWMBx55HOTG.htm)|Spellstrike|Frappe de sort|libre|
|[N7b2siWj1vbirSgd.htm](blood-lords-bestiary-items/N7b2siWj1vbirSgd.htm)|Thoughtsense (Precise) 100 feet|Perception des pensées 30 m (précis)|libre|
|[n9M1lMGvS2fKZbvr.htm](blood-lords-bestiary-items/n9M1lMGvS2fKZbvr.htm)|Mandibles|Mandibules|libre|
|[NaBFsQf6B2AYHQGS.htm](blood-lords-bestiary-items/NaBFsQf6B2AYHQGS.htm)|Faithful Weapon|Arme de la foi|libre|
|[NadsS6KEYWE2sb2U.htm](blood-lords-bestiary-items/NadsS6KEYWE2sb2U.htm)|Small Bag of Ash Containing a Black Onyx Gem|Petite bourse de cendres contenant une gemme noire d'onyx|libre|
|[NaMWeG8lLqlu1nCn.htm](blood-lords-bestiary-items/NaMWeG8lLqlu1nCn.htm)|The Cottage Hungers|Faim de la Chaumière|libre|
|[nbnYt1tbPjDn1y06.htm](blood-lords-bestiary-items/nbnYt1tbPjDn1y06.htm)|Sunlight Powerlessness|Impuissance solaire|libre|
|[NC1LlhP87iNkiYlz.htm](blood-lords-bestiary-items/NC1LlhP87iNkiYlz.htm)|Sneak Attack|Attaque sournoise|libre|
|[nEEJ2f6zyBqQ3YYY.htm](blood-lords-bestiary-items/nEEJ2f6zyBqQ3YYY.htm)|Puncture|Ponction|libre|
|[NEKpZoOfoUVij6kk.htm](blood-lords-bestiary-items/NEKpZoOfoUVij6kk.htm)|Bard Composition Spells|Sorts de composition de barde|libre|
|[nF1EJh2Wj3EzO8jZ.htm](blood-lords-bestiary-items/nF1EJh2Wj3EzO8jZ.htm)|Meticulous|Méticuleux|libre|
|[nFJmmnsDz6R63kzE.htm](blood-lords-bestiary-items/nFJmmnsDz6R63kzE.htm)|Snake Fangs|Crochets de serpents|libre|
|[NgIzj5c3G3ZKA8C1.htm](blood-lords-bestiary-items/NgIzj5c3G3ZKA8C1.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[NIurbkvNlbq5lWX9.htm](blood-lords-bestiary-items/NIurbkvNlbq5lWX9.htm)|Gird in Prayer|Ceindre de prières|libre|
|[NJ7cTeN4x2t0WHaF.htm](blood-lords-bestiary-items/NJ7cTeN4x2t0WHaF.htm)|Construct Armor (Hardness 8)|Armure artificelle (Solidité 8)|libre|
|[Nj7VbXq1Tv38pEZE.htm](blood-lords-bestiary-items/Nj7VbXq1Tv38pEZE.htm)|Shadow Hook|Crochet ombreux|libre|
|[NKBNpdXyOfdb8ITv.htm](blood-lords-bestiary-items/NKBNpdXyOfdb8ITv.htm)|Bardic Lore|Connaissance bardique|libre|
|[NL4AmhDWTt41yuGx.htm](blood-lords-bestiary-items/NL4AmhDWTt41yuGx.htm)|Keystone|La base à clé|libre|
|[NlKYbxNfVBumLBOH.htm](blood-lords-bestiary-items/NlKYbxNfVBumLBOH.htm)|Disgorge Soul|Désengorgement d'âme|libre|
|[nLqayK7bqWLCa5cH.htm](blood-lords-bestiary-items/nLqayK7bqWLCa5cH.htm)|Claw|Griffe|libre|
|[NlZvVQnKLh5eH8zG.htm](blood-lords-bestiary-items/NlZvVQnKLh5eH8zG.htm)|Rock|Rocher|libre|
|[no414a0CNwtoky7W.htm](blood-lords-bestiary-items/no414a0CNwtoky7W.htm)|Claw|Griffe|libre|
|[Noeu26HdBIOYmgvw.htm](blood-lords-bestiary-items/Noeu26HdBIOYmgvw.htm)|Tail|Queue|libre|
|[NopSVt0MQX8mMKwP.htm](blood-lords-bestiary-items/NopSVt0MQX8mMKwP.htm)|Poison Lore|Connaissance Poisons|libre|
|[noZrrPatG0HlCOmr.htm](blood-lords-bestiary-items/noZrrPatG0HlCOmr.htm)|Ghoul Fever|Fièvre des goules|libre|
|[Nq1Qjxass8BueZBH.htm](blood-lords-bestiary-items/Nq1Qjxass8BueZBH.htm)|Pokmit's Discernment|Sagacité de Pokmit|libre|
|[nqI6MSL3fFcCZoYB.htm](blood-lords-bestiary-items/nqI6MSL3fFcCZoYB.htm)|Circling Assault|Assaut encerclant|libre|
|[nrUiJF6RfQWNailR.htm](blood-lords-bestiary-items/nrUiJF6RfQWNailR.htm)|Ghoul Fever|Fièvre des goules|libre|
|[nSWYHtEIfDX1f1iV.htm](blood-lords-bestiary-items/nSWYHtEIfDX1f1iV.htm)|Trident|+2,greaterStriking|Trident de frappe supérieure +2|libre|
|[nW1BVOQNHX2klHtj.htm](blood-lords-bestiary-items/nW1BVOQNHX2klHtj.htm)|Witch Patron|Protecteur sorcier|libre|
|[NwdUeTnAhtTWQgPj.htm](blood-lords-bestiary-items/NwdUeTnAhtTWQgPj.htm)|Change Shape|Changement de forme|libre|
|[NWJqhDE5HSOrwSvm.htm](blood-lords-bestiary-items/NWJqhDE5HSOrwSvm.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[NwtlLSbtnFIOV0jE.htm](blood-lords-bestiary-items/NwtlLSbtnFIOV0jE.htm)|Drain Soul Cage|Canaliser le phylactère|libre|
|[O03HMHG7y11WSzQJ.htm](blood-lords-bestiary-items/O03HMHG7y11WSzQJ.htm)|Tormenting Dreams|Rêves tourmenteurs|libre|
|[O07KaYpTbnaylyyM.htm](blood-lords-bestiary-items/O07KaYpTbnaylyyM.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|libre|
|[O1pXi5kvcVTK4FQW.htm](blood-lords-bestiary-items/O1pXi5kvcVTK4FQW.htm)|+1 Status to All Saves vs. Magic|Bonus de statut de +1 aux jets de sauvegarde contre la magie|libre|
|[o60vQWgmGvl3c3NC.htm](blood-lords-bestiary-items/o60vQWgmGvl3c3NC.htm)|Negative Healing|Guérison négative|libre|
|[o61qEO77xsq1zS2N.htm](blood-lords-bestiary-items/o61qEO77xsq1zS2N.htm)|Ghoul Fever|Fièvre des goules|libre|
|[o65v39cYe0TCpxWc.htm](blood-lords-bestiary-items/o65v39cYe0TCpxWc.htm)|Abyss Lore|Connaissance des abysses|libre|
|[O6GoC5yqrWcERUf8.htm](blood-lords-bestiary-items/O6GoC5yqrWcERUf8.htm)|Rock|Rocher|libre|
|[O8JRZ8ZAEwNxvyR1.htm](blood-lords-bestiary-items/O8JRZ8ZAEwNxvyR1.htm)|Font of Death|Source de mort|libre|
|[o9BXGPYKI3pz3OHb.htm](blood-lords-bestiary-items/o9BXGPYKI3pz3OHb.htm)|Agonizing Overload|Surcharge de douleur|libre|
|[Oaq4T5auxVlppuFQ.htm](blood-lords-bestiary-items/Oaq4T5auxVlppuFQ.htm)|Multiple Reactions|Réactions multiples|libre|
|[ObEGC7xLCQEp9EZX.htm](blood-lords-bestiary-items/ObEGC7xLCQEp9EZX.htm)|Improved Grab|Empoignade améliorée|officielle|
|[ODB0ADki8uJ1yfU4.htm](blood-lords-bestiary-items/ODB0ADki8uJ1yfU4.htm)|Whirlpool|Tourbillon|libre|
|[oDr6qqRzR4QVrczZ.htm](blood-lords-bestiary-items/oDr6qqRzR4QVrczZ.htm)|Plane Shift (Self And Locked Soul Only, To The Boneyard Only)|Changement de plan (soi et l'âme verrouillée seulement, vers le Cimetière uniquement)|libre|
|[OExj5bVPy0ABfKjW.htm](blood-lords-bestiary-items/OExj5bVPy0ABfKjW.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[oFBq6T55H7FpCRum.htm](blood-lords-bestiary-items/oFBq6T55H7FpCRum.htm)|Zombie Spawn|Rejeton zombie|libre|
|[ofSsXcVwvyShKkRg.htm](blood-lords-bestiary-items/ofSsXcVwvyShKkRg.htm)|Bone Shard|Éclat d'os|libre|
|[oFVPP6P4Xf4CQJrn.htm](blood-lords-bestiary-items/oFVPP6P4Xf4CQJrn.htm)|Paralyzing Claws|Griffes paralysantes|libre|
|[OgML8UQ6lul2u2L7.htm](blood-lords-bestiary-items/OgML8UQ6lul2u2L7.htm)|Shield Block|Blocage au bouclier|libre|
|[Oi10YnqeymdtqRHt.htm](blood-lords-bestiary-items/Oi10YnqeymdtqRHt.htm)|Paralysis|Paralysie|libre|
|[ojbPp8DLfA5Fl3EC.htm](blood-lords-bestiary-items/ojbPp8DLfA5Fl3EC.htm)|Consume Flesh|Dévorer la chair|libre|
|[OJuNHnHd0dRzaSgt.htm](blood-lords-bestiary-items/OJuNHnHd0dRzaSgt.htm)|Hasty Impediment|Obstacle rapide|libre|
|[OlYMKu8U3252vjmq.htm](blood-lords-bestiary-items/OlYMKu8U3252vjmq.htm)|Paralysis|Paralysie|libre|
|[omRU4fUfaCd6zdAI.htm](blood-lords-bestiary-items/omRU4fUfaCd6zdAI.htm)|Distracting Bitterness|Amertume gênante|libre|
|[OMtaIqvZMW0FPGA3.htm](blood-lords-bestiary-items/OMtaIqvZMW0FPGA3.htm)|Traitorous Betrayal|Trahison perfide|libre|
|[oNEcLYST1r5V2Ts9.htm](blood-lords-bestiary-items/oNEcLYST1r5V2Ts9.htm)|Paralyzing Claws|Griffes paralysantes|libre|
|[OuoCEgNWvWspliwB.htm](blood-lords-bestiary-items/OuoCEgNWvWspliwB.htm)|Rusting Touch|Contact de rouille|libre|
|[oUyQv0uxDRMDAcJQ.htm](blood-lords-bestiary-items/oUyQv0uxDRMDAcJQ.htm)|Slam|Claque|libre|
|[OvR4dcaFK5nBRvxg.htm](blood-lords-bestiary-items/OvR4dcaFK5nBRvxg.htm)|Thoughtsense 100 feet|Perception des pensées 30 m|libre|
|[OVWP4jGMqL4WNwzo.htm](blood-lords-bestiary-items/OVWP4jGMqL4WNwzo.htm)|Siphon Soul|Siphonnage d'âme|libre|
|[oW4GlZx8glb20tAK.htm](blood-lords-bestiary-items/oW4GlZx8glb20tAK.htm)|Piping Dirge|Tuyauterie funeste|libre|
|[oYi2p6D02npM0FnQ.htm](blood-lords-bestiary-items/oYi2p6D02npM0FnQ.htm)|Fangs|Crocs|libre|
|[oZOCqDZs86xNMW2p.htm](blood-lords-bestiary-items/oZOCqDZs86xNMW2p.htm)|Emit Agony|Émission d'agonie|libre|
|[P2XxJDFs2BRcS3gA.htm](blood-lords-bestiary-items/P2XxJDFs2BRcS3gA.htm)|Grab|Empoignade/Agrippement|libre|
|[p31UJwCpbW1PNbWA.htm](blood-lords-bestiary-items/p31UJwCpbW1PNbWA.htm)|Jaws|Mâchoire|libre|
|[P3DKNYk3hMABVGY0.htm](blood-lords-bestiary-items/P3DKNYk3hMABVGY0.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[p3SFrrXEHa045cK7.htm](blood-lords-bestiary-items/p3SFrrXEHa045cK7.htm)|Invisibility (At Will)|Invisibilité (à volonté)|libre|
|[Padh4azq1HtiVrHg.htm](blood-lords-bestiary-items/Padh4azq1HtiVrHg.htm)|Surprise Attack|Attaque surprise|libre|
|[pB0ijAomlGT1Hzvo.htm](blood-lords-bestiary-items/pB0ijAomlGT1Hzvo.htm)|Expand Aura|Aura étendue|libre|
|[PB6uzirZvVyNFLJQ.htm](blood-lords-bestiary-items/PB6uzirZvVyNFLJQ.htm)|Negative Healing|Guérison négative|libre|
|[pdcMo0Z3y78UAPle.htm](blood-lords-bestiary-items/pdcMo0Z3y78UAPle.htm)|Arcane Prepared Spells|Sorts arcaniques préparés|libre|
|[PE0YRDKP8ZZ0Zpht.htm](blood-lords-bestiary-items/PE0YRDKP8ZZ0Zpht.htm)|Light Mace|+1,striking|Masse légère de frappe +1|libre|
|[PeVbZxkiIPHXxbci.htm](blood-lords-bestiary-items/PeVbZxkiIPHXxbci.htm)|Divine Innate Spells|Sorts divin innés|libre|
|[pGfrMv2oB57a9HWH.htm](blood-lords-bestiary-items/pGfrMv2oB57a9HWH.htm)|Void Ray|Rayon du vide|libre|
|[PgteKOjUXoK7JyXE.htm](blood-lords-bestiary-items/PgteKOjUXoK7JyXE.htm)|Outcast's Curse (At Will)|Malédiction du paria (à volonté)|libre|
|[pH1iEWTjpUMabi5x.htm](blood-lords-bestiary-items/pH1iEWTjpUMabi5x.htm)|Jaws|Mâchoire|libre|
|[Ph3KVAcJpAjskSDf.htm](blood-lords-bestiary-items/Ph3KVAcJpAjskSDf.htm)|Walking Stick|Bâton de marche|libre|
|[pj2PzMSNP3aGSaFR.htm](blood-lords-bestiary-items/pj2PzMSNP3aGSaFR.htm)|Stench|Puanteur|libre|
|[PjFiNdRkQ5Se5koB.htm](blood-lords-bestiary-items/PjFiNdRkQ5Se5koB.htm)|Claw|Griffe|libre|
|[pJt9Jjt17jRLK4oP.htm](blood-lords-bestiary-items/pJt9Jjt17jRLK4oP.htm)|Consume Flesh|Dévorer la chair|libre|
|[PlAHuWkVyhVyEH3L.htm](blood-lords-bestiary-items/PlAHuWkVyhVyEH3L.htm)|Steady Spellcasting|Incantation fiable|libre|
|[pmFFz0OirohXhQgT.htm](blood-lords-bestiary-items/pmFFz0OirohXhQgT.htm)|At-Will Spells|Sorts à volonté|libre|
|[pmFVKYjk9jHtgOEZ.htm](blood-lords-bestiary-items/pmFVKYjk9jHtgOEZ.htm)|Death's Chill|Froid mortel|libre|
|[pmhZ1MBXvItpUSNr.htm](blood-lords-bestiary-items/pmhZ1MBXvItpUSNr.htm)|Occult Prepared Spells|Sorts occultes préparés|libre|
|[pNaTK2YQXqLsBvpF.htm](blood-lords-bestiary-items/pNaTK2YQXqLsBvpF.htm)|Countless Religious Symbols|Symboles religieux indénombrables|libre|
|[pOJJm2PFoBzwUqgK.htm](blood-lords-bestiary-items/pOJJm2PFoBzwUqgK.htm)|Pulse|Pulsation|libre|
|[PprlBKzBfXxYkclB.htm](blood-lords-bestiary-items/PprlBKzBfXxYkclB.htm)|Lifesense|Perception de la vie|libre|
|[PpvFkA9eo3Vb8yNr.htm](blood-lords-bestiary-items/PpvFkA9eo3Vb8yNr.htm)|Guisarme|+2,striking|Guisame de frappe +2|libre|
|[PqFglmTDV1B6OcFX.htm](blood-lords-bestiary-items/PqFglmTDV1B6OcFX.htm)|Negative Healing|Guérison négative|libre|
|[psEM5dqAQ71G01GV.htm](blood-lords-bestiary-items/psEM5dqAQ71G01GV.htm)|Slam Shut|Se refermer violemment|libre|
|[PsG1DC7CNm1Yx4Ao.htm](blood-lords-bestiary-items/PsG1DC7CNm1Yx4Ao.htm)|Knockdown|Renversement|libre|
|[pU2wsrbVRPL7i9Z5.htm](blood-lords-bestiary-items/pU2wsrbVRPL7i9Z5.htm)|Arcane Spontaneous Spells|Sorts arcaniques spontanés|libre|
|[pupjfqIi72kdSKTI.htm](blood-lords-bestiary-items/pupjfqIi72kdSKTI.htm)|Negative Healing|Guérison négative|libre|
|[PVgQC5UPhLXPKnjX.htm](blood-lords-bestiary-items/PVgQC5UPhLXPKnjX.htm)|Grave's Grip|Poigne de la tombe|libre|
|[pxjai5bMTbvqW3Qn.htm](blood-lords-bestiary-items/pxjai5bMTbvqW3Qn.htm)|Walking Stick|+2,striking|Bâton de marche de frappe +2|libre|
|[pxo58rpWswvZtQUs.htm](blood-lords-bestiary-items/pxo58rpWswvZtQUs.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[pyyhZ6BfNL9ATh0U.htm](blood-lords-bestiary-items/pyyhZ6BfNL9ATh0U.htm)|Hunger Pangs|Crampes de faim|libre|
|[Q0sMTnzMYC0wWnsK.htm](blood-lords-bestiary-items/Q0sMTnzMYC0wWnsK.htm)|Slippery Fighter|Combattant fuyant|libre|
|[Q2fouPG3hNPyOxHc.htm](blood-lords-bestiary-items/Q2fouPG3hNPyOxHc.htm)|Double-Barreled Musket|+1,striking|Mousquet à double barillet de frappe +1|libre|
|[q5DQX31tD4pldcTg.htm](blood-lords-bestiary-items/q5DQX31tD4pldcTg.htm)|Pressurized Pustules|Pustules pressurisées|libre|
|[q6hlPrPiT8JhtdjO.htm](blood-lords-bestiary-items/q6hlPrPiT8JhtdjO.htm)|Plane Shift (Self Only; to Shadow Plane or Material Plane Only)|Changement de plan (soi uniquement, plan de l'Ombre ou plan Matériel seulement)|libre|
|[Q9W0tmmKogEnfOJg.htm](blood-lords-bestiary-items/Q9W0tmmKogEnfOJg.htm)|Spirit Touch|Contact spirituel|libre|
|[qA0uCJOWECLVcNuN.htm](blood-lords-bestiary-items/qA0uCJOWECLVcNuN.htm)|Drink Thoughts|Boire les pensées|libre|
|[qB7gFx05p5TyaekK.htm](blood-lords-bestiary-items/qB7gFx05p5TyaekK.htm)|Tenebric Stride|Marche ténébreuse|libre|
|[qBc1qrTOGZIzCVLH.htm](blood-lords-bestiary-items/qBc1qrTOGZIzCVLH.htm)|Wizard School Spells|Sorts d'école de magicien|libre|
|[QblktZpnGuwZyRoE.htm](blood-lords-bestiary-items/QblktZpnGuwZyRoE.htm)|Bubonic Plague|Peste bubonique|libre|
|[QbwWSi5dZyCO8GzJ.htm](blood-lords-bestiary-items/QbwWSi5dZyCO8GzJ.htm)|Flagon|Choppe|libre|
|[qCZ93XZdw1VOY0cY.htm](blood-lords-bestiary-items/qCZ93XZdw1VOY0cY.htm)|Create Spawn|Création de rejetons|libre|
|[QczLfiCO8ZVRrzkW.htm](blood-lords-bestiary-items/QczLfiCO8ZVRrzkW.htm)|Cremate Undead|Crémation des morts-vivants|libre|
|[qddfkMcSKVLMn23e.htm](blood-lords-bestiary-items/qddfkMcSKVLMn23e.htm)|Battering Vortex|Vortex martelant|libre|
|[QHX8JgZjpxQ71msf.htm](blood-lords-bestiary-items/QHX8JgZjpxQ71msf.htm)|Devastating Blast|Déflagration dévastatrice|libre|
|[QjVDqHMHijBSDp2j.htm](blood-lords-bestiary-items/QjVDqHMHijBSDp2j.htm)|Negative Healing|Guérison négative|libre|
|[qJyRY2becImO8jLp.htm](blood-lords-bestiary-items/qJyRY2becImO8jLp.htm)|Negative Healing|Guérison négative|libre|
|[qlb3CxgMnQCywYHL.htm](blood-lords-bestiary-items/qlb3CxgMnQCywYHL.htm)|Remove Curse (At Will)|Délivrance des malédictions (à volonté)|libre|
|[QLMUMzHQksMagJOc.htm](blood-lords-bestiary-items/QLMUMzHQksMagJOc.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[qmMIJCw0r4JFDq1n.htm](blood-lords-bestiary-items/qmMIJCw0r4JFDq1n.htm)|Adamantine Edges|Tranchant en adamatium|libre|
|[QpRhJboLIrPLkeJI.htm](blood-lords-bestiary-items/QpRhJboLIrPLkeJI.htm)|Deny Advantage|Refus d'avantage|libre|
|[qqMPnhkqfp4uSfDm.htm](blood-lords-bestiary-items/qqMPnhkqfp4uSfDm.htm)|Tongues (Constant)|Don des langues (constant)|libre|
|[QRwz0rO8uK5KlPYy.htm](blood-lords-bestiary-items/QRwz0rO8uK5KlPYy.htm)|Improved Grab|Empoignade/agrippement amélioré|libre|
|[Qs8jgNRG7timjSVE.htm](blood-lords-bestiary-items/Qs8jgNRG7timjSVE.htm)|Corpse Scent (Imprecise)|Odorat des cadavres (imprécis)|libre|
|[qtO1WSrVjmSwb8Mb.htm](blood-lords-bestiary-items/qtO1WSrVjmSwb8Mb.htm)|Slip Between|Glissade|libre|
|[qUWtbc8MT7AreYzs.htm](blood-lords-bestiary-items/qUWtbc8MT7AreYzs.htm)|Rejuvenation|Reconstruction|libre|
|[QwanxsB4SIavve0x.htm](blood-lords-bestiary-items/QwanxsB4SIavve0x.htm)|Arcane Prepared Spells|Sorts arcaniques préparés|libre|
|[qxLcM4LwcXIOI4JT.htm](blood-lords-bestiary-items/qxLcM4LwcXIOI4JT.htm)|Paralysis|Paralysie|libre|
|[qy5d75mhIlKteqJ8.htm](blood-lords-bestiary-items/qy5d75mhIlKteqJ8.htm)|Ghastly Flames|Flammes épouvantables|libre|
|[qz6WlJm5c1QTYtA8.htm](blood-lords-bestiary-items/qz6WlJm5c1QTYtA8.htm)|Drink Blood|Boire le sang|libre|
|[qzGYfHE99qeNTsJS.htm](blood-lords-bestiary-items/qzGYfHE99qeNTsJS.htm)|Shield Warden|Gardien au bouclier|libre|
|[R5C9yPhddZNDULzM.htm](blood-lords-bestiary-items/R5C9yPhddZNDULzM.htm)|Acidic Assault|Agression d'acide|libre|
|[r5dYGunbr3uNPEDD.htm](blood-lords-bestiary-items/r5dYGunbr3uNPEDD.htm)|Field of Force|Champ de force|libre|
|[r5pz5mstgEyvX860.htm](blood-lords-bestiary-items/r5pz5mstgEyvX860.htm)|Negative Healing|Guérison négative|libre|
|[r6avuc82SNavBY0X.htm](blood-lords-bestiary-items/r6avuc82SNavBY0X.htm)|Speak with Animals (Constant)|Communication avec les animaux (constant)|libre|
|[r6ga1zxnQkt7n3NN.htm](blood-lords-bestiary-items/r6ga1zxnQkt7n3NN.htm)|Fast Healing 20|Guérison accélérée 20|libre|
|[R7m6UAfHaArx89Vz.htm](blood-lords-bestiary-items/R7m6UAfHaArx89Vz.htm)|Focus Spells|Sorts focalisés|libre|
|[r835KgCvkRk0ttPH.htm](blood-lords-bestiary-items/r835KgCvkRk0ttPH.htm)|+1 Status to All Saves vs. Magic|Bonus de statut de +1 aux jets de sauvegarde contre la magie|libre|
|[R9XRTrjfJ6f0YE1W.htm](blood-lords-bestiary-items/R9XRTrjfJ6f0YE1W.htm)|Key to all locks in Cerulean Glade|Clé de toutes les portes de la Clairière céruléenne.|libre|
|[rBFcY5MpSMdtvE5Y.htm](blood-lords-bestiary-items/rBFcY5MpSMdtvE5Y.htm)|Gaping Chomp|Rongement brutal|libre|
|[RBG5lCJDEdILIHU4.htm](blood-lords-bestiary-items/RBG5lCJDEdILIHU4.htm)|Part of the Attic|Particularité du Grenier|libre|
|[RbPJVy5Y5SRvCW3G.htm](blood-lords-bestiary-items/RbPJVy5Y5SRvCW3G.htm)|Tremorsense 100 feet|Perception des vibrations 30 m|libre|
|[RbyW1JCkLdDv7SxM.htm](blood-lords-bestiary-items/RbyW1JCkLdDv7SxM.htm)|Gaseous Form (At Will)|Forme gazeuse (à volonté)|libre|
|[RdcUjaxfQwMEGLbI.htm](blood-lords-bestiary-items/RdcUjaxfQwMEGLbI.htm)|Clairvoyance (At Will)|Clairvoyance (à volonté)|libre|
|[rdEr7ZuZTd1CBhnQ.htm](blood-lords-bestiary-items/rdEr7ZuZTd1CBhnQ.htm)|Boneyard Lore|Connaissance Cimetière|libre|
|[rdipU4PkTwrzcBIk.htm](blood-lords-bestiary-items/rdipU4PkTwrzcBIk.htm)|Vampire Weaknesses|Faiblesse des vampires|libre|
|[rEGSe0pHN3Hnc0bs.htm](blood-lords-bestiary-items/rEGSe0pHN3Hnc0bs.htm)|Battle|Bataille|libre|
|[ReJ20xAJKg7hXCyT.htm](blood-lords-bestiary-items/ReJ20xAJKg7hXCyT.htm)|Graydirge Lore|Connaissance de Graydirge|libre|
|[rFbZTPpRvxoWIHZG.htm](blood-lords-bestiary-items/rFbZTPpRvxoWIHZG.htm)|Divine Focus Spells|Sorts divins focalisés|libre|
|[RfZkJsVPKiboLSK8.htm](blood-lords-bestiary-items/RfZkJsVPKiboLSK8.htm)|Drink Blood|Boire le sang|libre|
|[rHL6SLi82P6JhiMs.htm](blood-lords-bestiary-items/rHL6SLi82P6JhiMs.htm)|Negative Healing|Guérison négative|libre|
|[Rid7x1Kule9WHCnO.htm](blood-lords-bestiary-items/Rid7x1Kule9WHCnO.htm)|Constant Spells|Sorts constants|libre|
|[rJ1opmsaOoIX1hei.htm](blood-lords-bestiary-items/rJ1opmsaOoIX1hei.htm)|Weapon Master|Maître d'armes|libre|
|[Rjj2wwaxgeF30eGg.htm](blood-lords-bestiary-items/Rjj2wwaxgeF30eGg.htm)|Fast Healing 10|Guérison accélérée 10|libre|
|[RkCHgu22C3w9ZGzs.htm](blood-lords-bestiary-items/RkCHgu22C3w9ZGzs.htm)|Immortal|Éternel|libre|
|[rkkyxr7CyLOptzkU.htm](blood-lords-bestiary-items/rkkyxr7CyLOptzkU.htm)|Rend|Éventration|libre|
|[rLEgd4MaJAlQya1j.htm](blood-lords-bestiary-items/rLEgd4MaJAlQya1j.htm)|Rapier|+2,greaterStriking|Rapière de frappe supérieure +2|libre|
|[RnLbi3AwbhWJQPnv.htm](blood-lords-bestiary-items/RnLbi3AwbhWJQPnv.htm)|Constrict|Constriction|libre|
|[RRh4AXrqShUdaQmV.htm](blood-lords-bestiary-items/RRh4AXrqShUdaQmV.htm)|Underworld Lore|Connaissance de la pègre|libre|
|[RRPzRLRS7BdAYtvS.htm](blood-lords-bestiary-items/RRPzRLRS7BdAYtvS.htm)|Jaws|Mâchoire|libre|
|[rRRIoy5QgOWOFzn2.htm](blood-lords-bestiary-items/rRRIoy5QgOWOFzn2.htm)|Quick Draw|Arme en main|libre|
|[RRtEbi7LCsYVcq0p.htm](blood-lords-bestiary-items/RRtEbi7LCsYVcq0p.htm)|Entangling Net|Filet entravant|libre|
|[rS4CtIjMBxJnIHVh.htm](blood-lords-bestiary-items/rS4CtIjMBxJnIHVh.htm)|Arcane Innate Spells|Sorts arcaniques innés|libre|
|[RsdJ1hobHnj5Huzn.htm](blood-lords-bestiary-items/RsdJ1hobHnj5Huzn.htm)|Composite Shortbow|+1,striking|Arc court composite de frappe +1|libre|
|[rso4lucgh0chsq23.htm](blood-lords-bestiary-items/rso4lucgh0chsq23.htm)|Shadow Pulse|Pulsation ombreuse|libre|
|[Rtm8o7JzZF50M8BC.htm](blood-lords-bestiary-items/Rtm8o7JzZF50M8BC.htm)|Rock Tunneler|Foreur de roche|libre|
|[rtzKA1GRO6XUXbNO.htm](blood-lords-bestiary-items/rtzKA1GRO6XUXbNO.htm)|Troop Spellcasting|Incantation de troupe|libre|
|[ru25VLTiiz3Xdeny.htm](blood-lords-bestiary-items/ru25VLTiiz3Xdeny.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|libre|
|[RUg3D9O5xbV2Ew9T.htm](blood-lords-bestiary-items/RUg3D9O5xbV2Ew9T.htm)|Horn|Corne|libre|
|[ruVQeW8yMOCnLrm0.htm](blood-lords-bestiary-items/ruVQeW8yMOCnLrm0.htm)|Summon Monster|Convocation de monstre|libre|
|[rwLMgxOYWCHlCD7R.htm](blood-lords-bestiary-items/rwLMgxOYWCHlCD7R.htm)|Troop Defenses|Défense de troupe|libre|
|[RxFOQd6evVSbKThM.htm](blood-lords-bestiary-items/RxFOQd6evVSbKThM.htm)|Spry Dodge|Esquive vivace|libre|
|[Rxy2LBFNu0ZMzW38.htm](blood-lords-bestiary-items/Rxy2LBFNu0ZMzW38.htm)|At-Will Spells|Sorts à volonté|libre|
|[ryu97a9Y75S5GuK0.htm](blood-lords-bestiary-items/ryu97a9Y75S5GuK0.htm)|Mohrg Spawn|Rejeton de mohrg|libre|
|[RyZH5DnpRyQvezrJ.htm](blood-lords-bestiary-items/RyZH5DnpRyQvezrJ.htm)|Discorporate|Dématérialisation|libre|
|[rZ16V3aUUZ0xskyr.htm](blood-lords-bestiary-items/rZ16V3aUUZ0xskyr.htm)|Spirit Body|Corps d'esprit|libre|
|[rZu3iZJmCOAdpNYm.htm](blood-lords-bestiary-items/rZu3iZJmCOAdpNYm.htm)|Hoof|Sabot|libre|
|[rzzDTTRv6sX73Yu2.htm](blood-lords-bestiary-items/rzzDTTRv6sX73Yu2.htm)|Counterattack|Contre-attaque|libre|
|[s1W6qbbXuGOf5KH9.htm](blood-lords-bestiary-items/s1W6qbbXuGOf5KH9.htm)|Curse of Futility|Malédiction de futilité|libre|
|[s2OsEgwfeUVBCyKQ.htm](blood-lords-bestiary-items/s2OsEgwfeUVBCyKQ.htm)|Shadow Breath|Souffle d'ombres|libre|
|[s33AQaOJJ5kWUXkw.htm](blood-lords-bestiary-items/s33AQaOJJ5kWUXkw.htm)|Skewer|Empalement|libre|
|[S4wsJs63cp0hjMDh.htm](blood-lords-bestiary-items/S4wsJs63cp0hjMDh.htm)|Jaws|Mâchoire|libre|
|[s9asrDvWzf12qCDG.htm](blood-lords-bestiary-items/s9asrDvWzf12qCDG.htm)|Sever Spell|Tranche sort|libre|
|[sA1AjCMr9UZaW7fp.htm](blood-lords-bestiary-items/sA1AjCMr9UZaW7fp.htm)|Slam|Claque|libre|
|[sB2hBiB5XcfKAEev.htm](blood-lords-bestiary-items/sB2hBiB5XcfKAEev.htm)|Grab|Empoignade/Agrippement|libre|
|[sb5m6sOre6tgHoeV.htm](blood-lords-bestiary-items/sb5m6sOre6tgHoeV.htm)|+1 Status to All Saves vs. Magic|Bonus de statut de +1 aux jets de sauvegarde contre la magie|libre|
|[sbJrYK7O0j91qRUJ.htm](blood-lords-bestiary-items/sbJrYK7O0j91qRUJ.htm)|Scattered Coven|Cercle éparpillé|libre|
|[sfBJSRamerWJM0KR.htm](blood-lords-bestiary-items/sfBJSRamerWJM0KR.htm)|Jaws|Mâchoire|libre|
|[sflDAvsA8PP3Jp11.htm](blood-lords-bestiary-items/sflDAvsA8PP3Jp11.htm)|Graveknight's Curse|Malédiction du chevalier sépulcre|libre|
|[SgK3k0iUZisYY83i.htm](blood-lords-bestiary-items/SgK3k0iUZisYY83i.htm)|Jaws|Mâchoire|libre|
|[SHGUwZ4U82CZ7QS6.htm](blood-lords-bestiary-items/SHGUwZ4U82CZ7QS6.htm)|Vetalarana Vulnerabilities|Vulnérabilités d'un vétalarana|libre|
|[si3cpuiipdaUs6pP.htm](blood-lords-bestiary-items/si3cpuiipdaUs6pP.htm)|Pseudopod|Pseudopode|libre|
|[Skieic55PgyEpTKa.htm](blood-lords-bestiary-items/Skieic55PgyEpTKa.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[sLbK87e5VN9NxmkM.htm](blood-lords-bestiary-items/sLbK87e5VN9NxmkM.htm)|Blood Roots|Racines de sang|libre|
|[SLif9LIriCHrFDuL.htm](blood-lords-bestiary-items/SLif9LIriCHrFDuL.htm)|Flagon|Choppe|libre|
|[sLXBqpRvWsK2VW3e.htm](blood-lords-bestiary-items/sLXBqpRvWsK2VW3e.htm)|Warfare Lore|Connaissance de la guerre|libre|
|[snTIGb2g7DF1L8wx.htm](blood-lords-bestiary-items/snTIGb2g7DF1L8wx.htm)|Phantasmal Pummeling|Tabassage fantasmagorique|libre|
|[sNvSxg1ui8jnr5Ky.htm](blood-lords-bestiary-items/sNvSxg1ui8jnr5Ky.htm)|Lockdown|Confinement|libre|
|[Sovn2AcegKyeoEwj.htm](blood-lords-bestiary-items/Sovn2AcegKyeoEwj.htm)|Darkness (At Will)|Ténèbres (à volonté)|libre|
|[soyHQJDMTqCELRZJ.htm](blood-lords-bestiary-items/soyHQJDMTqCELRZJ.htm)|Control Comatose|Contrôler les comateux|libre|
|[sPDFyxFqOtUlDF1P.htm](blood-lords-bestiary-items/sPDFyxFqOtUlDF1P.htm)|Negative Healing|Guérison négative|libre|
|[SsBTOGbXqC5fMugh.htm](blood-lords-bestiary-items/SsBTOGbXqC5fMugh.htm)|Bard Composition Spells|Sorts de composition de barde|libre|
|[SSJYmiOBRjHkg1Qt.htm](blood-lords-bestiary-items/SSJYmiOBRjHkg1Qt.htm)|Ravenous Breath Weapon|Souffle dévorant|libre|
|[stl7pAviHWEoCcxG.htm](blood-lords-bestiary-items/stl7pAviHWEoCcxG.htm)|Turn to Mist|Se changer en brume|libre|
|[SToxISh7fUFYil62.htm](blood-lords-bestiary-items/SToxISh7fUFYil62.htm)|Claw|Griffe|libre|
|[stXfg5Sba6tjjP5Z.htm](blood-lords-bestiary-items/stXfg5Sba6tjjP5Z.htm)|Paralysis|Paralysie|libre|
|[SULxmu88sWFOYyPP.htm](blood-lords-bestiary-items/SULxmu88sWFOYyPP.htm)|Branch|Branche|libre|
|[sV1VN6Fhrh22UXOU.htm](blood-lords-bestiary-items/sV1VN6Fhrh22UXOU.htm)|Constant Spells|Sorts constants|libre|
|[sy6ZKFJRQ0uLMPAR.htm](blood-lords-bestiary-items/sy6ZKFJRQ0uLMPAR.htm)|Mend Soul|Réparer l'âme|libre|
|[SzH0hxHyBlvwORAO.htm](blood-lords-bestiary-items/SzH0hxHyBlvwORAO.htm)|Draconic Frenzy|Frénésie draconique|libre|
|[t1Qloeszz4mTmonQ.htm](blood-lords-bestiary-items/t1Qloeszz4mTmonQ.htm)|At-Will Spells|Sorts à volonté|libre|
|[T3Mk7VMN6vwZ2N2N.htm](blood-lords-bestiary-items/T3Mk7VMN6vwZ2N2N.htm)|Religious Symbol of Pharasma|Symbole religieux de Pharasma|libre|
|[t5YGgXBCNeF3Qo2r.htm](blood-lords-bestiary-items/t5YGgXBCNeF3Qo2r.htm)|Rock|Rocher|libre|
|[T9NrWUqpdUWVCGhm.htm](blood-lords-bestiary-items/T9NrWUqpdUWVCGhm.htm)|Interposition|Interposition|libre|
|[Tc3W28SMOSvvjreh.htm](blood-lords-bestiary-items/Tc3W28SMOSvvjreh.htm)|Site Bound|Lié à un site|libre|
|[TEzQEJ8ewHlHI23c.htm](blood-lords-bestiary-items/TEzQEJ8ewHlHI23c.htm)|Divine Focus Spells|Sorts focalisés divins|libre|
|[tFdKOvLvAT9noqLE.htm](blood-lords-bestiary-items/tFdKOvLvAT9noqLE.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[THTZ70glpYi6EuWC.htm](blood-lords-bestiary-items/THTZ70glpYi6EuWC.htm)|Chug|Cul sec|libre|
|[tI0Nz7ZwoXgwzFnE.htm](blood-lords-bestiary-items/tI0Nz7ZwoXgwzFnE.htm)|+1 Circumstance Bonus to Will Saves Against Mental Effects|Bonus de circonstances de +1 aux jets de sauvegarde Volonté contre les effets mentaux|libre|
|[TI32SCeXh4AwiTF3.htm](blood-lords-bestiary-items/TI32SCeXh4AwiTF3.htm)|Draw the Line|Dessiner la frontière|libre|
|[Ti4M6Bxnmynx450J.htm](blood-lords-bestiary-items/Ti4M6Bxnmynx450J.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[tIc9vWOrxkGgq0FH.htm](blood-lords-bestiary-items/tIc9vWOrxkGgq0FH.htm)|Throw Rock|Projection de rocher|libre|
|[tlhZIlTIzio9uf0p.htm](blood-lords-bestiary-items/tlhZIlTIzio9uf0p.htm)|Hungerfang|+2,greaterStriking,returning,spellStoring|Croc-famine, lance boomerang, stockage de sort, de frappe supérieure +2|libre|
|[TMZAJntEAt03PB9g.htm](blood-lords-bestiary-items/TMZAJntEAt03PB9g.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[tMZFtFOnhFklTFgg.htm](blood-lords-bestiary-items/tMZFtFOnhFklTFgg.htm)|Coven|Cercle|libre|
|[TnY3ElSxrMRug4uY.htm](blood-lords-bestiary-items/TnY3ElSxrMRug4uY.htm)|Shoving Block|Blocage pousssé|libre|
|[TOYsFGjZBmCRAaeI.htm](blood-lords-bestiary-items/TOYsFGjZBmCRAaeI.htm)|Create Spawn|Création de rejeton|libre|
|[TpaEm4jr14ovJfci.htm](blood-lords-bestiary-items/TpaEm4jr14ovJfci.htm)|Claw|Griffe|libre|
|[tPZGgQq2byVjPKMQ.htm](blood-lords-bestiary-items/tPZGgQq2byVjPKMQ.htm)|Create Undead (Zombie)|Création de mort-vivant (zombie)|libre|
|[tsYuUYFpnyHaguLV.htm](blood-lords-bestiary-items/tsYuUYFpnyHaguLV.htm)|Claw|Griffe|libre|
|[tt9zjjDEJVENUiuV.htm](blood-lords-bestiary-items/tt9zjjDEJVENUiuV.htm)|Curse of the Grave|Malédiction de la tombe|libre|
|[tUur14bXK0CP1S6Y.htm](blood-lords-bestiary-items/tUur14bXK0CP1S6Y.htm)|Drain Thoughts|Drain de pensées|libre|
|[Tvf2O63lrcjNH7HL.htm](blood-lords-bestiary-items/Tvf2O63lrcjNH7HL.htm)|Plane Shift (Self Only; to Shadow Plane or Material Plane Only)|Changement de plan (soi uniquement; vers le plan de l'Ombre et le plan Matériel seulement)|libre|
|[TVw045eBSCQGLo63.htm](blood-lords-bestiary-items/TVw045eBSCQGLo63.htm)|At-Will Spells|Sorts à volonté|libre|
|[tw025qxniO3X63At.htm](blood-lords-bestiary-items/tw025qxniO3X63At.htm)|+2 Resilient Studded Leather Armor|Armure en cuir clouté de résilience +2|libre|
|[twaTXkQKZvHsXMbE.htm](blood-lords-bestiary-items/twaTXkQKZvHsXMbE.htm)|Shambler's Lament|Lamentation du titubeur|libre|
|[twj4EKcScsSt0E5V.htm](blood-lords-bestiary-items/twj4EKcScsSt0E5V.htm)|Teach You a Lesson|Donner une bonne leçon|libre|
|[tY3TZfRWPDy9qUYl.htm](blood-lords-bestiary-items/tY3TZfRWPDy9qUYl.htm)|Gotcha!|Je t'ai eu !|libre|
|[U5f43GO8dGHjcl08.htm](blood-lords-bestiary-items/U5f43GO8dGHjcl08.htm)|Key Conditioning|Soumis à la clé|libre|
|[U8KzgRL97wTZHRzW.htm](blood-lords-bestiary-items/U8KzgRL97wTZHRzW.htm)|Consume Flesh|Dévorer la chair|libre|
|[U9NiBmIUjYGYkzVA.htm](blood-lords-bestiary-items/U9NiBmIUjYGYkzVA.htm)|Visions of Death|Vision de mort|libre|
|[u9p0jtd2rO80l8Ty.htm](blood-lords-bestiary-items/u9p0jtd2rO80l8Ty.htm)|Suggestion (At Will)|Suggestion (à volonté)|libre|
|[uAi1OCB6zp1lUzhW.htm](blood-lords-bestiary-items/uAi1OCB6zp1lUzhW.htm)|Improved Grab|Empoignade/agrippement amélioré|libre|
|[uaypwbmMrRqxLWbM.htm](blood-lords-bestiary-items/uaypwbmMrRqxLWbM.htm)|Ghoul Fever|Fièvre des goules|libre|
|[UDgtFGTfCDsNWHoE.htm](blood-lords-bestiary-items/UDgtFGTfCDsNWHoE.htm)|Divine Aegis|Égide divine|libre|
|[uDGWvAQsEKDQrPLA.htm](blood-lords-bestiary-items/uDGWvAQsEKDQrPLA.htm)|Tremorsense (Imprecise) 30 feet|Perception des vibrations 9 m (imprécis)|libre|
|[UIRxk8ZQIc88MUzh.htm](blood-lords-bestiary-items/UIRxk8ZQIc88MUzh.htm)|Overwhelming Ectoplasm|Ectoplasme envahissant|libre|
|[UjNnBMOv8pcW7Efc.htm](blood-lords-bestiary-items/UjNnBMOv8pcW7Efc.htm)|Serpent Venom|Venin de serpent|libre|
|[UjS6H64upEIHfl5k.htm](blood-lords-bestiary-items/UjS6H64upEIHfl5k.htm)|Immortal|Éternel|libre|
|[uKVihdZh10f60zxY.htm](blood-lords-bestiary-items/uKVihdZh10f60zxY.htm)|Shard Shield|Bouclier d'éclats|libre|
|[UlBqkci6D8DO3QAc.htm](blood-lords-bestiary-items/UlBqkci6D8DO3QAc.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[uLe70a80LVMuUEbe.htm](blood-lords-bestiary-items/uLe70a80LVMuUEbe.htm)|Change Shape|Changement de forme|libre|
|[uLmmS0IQid7p3QX2.htm](blood-lords-bestiary-items/uLmmS0IQid7p3QX2.htm)|Tongue of Flame|Langue de flamme|libre|
|[UMSbBneVOO85C8uP.htm](blood-lords-bestiary-items/UMSbBneVOO85C8uP.htm)|Skittering Rush|Charge rapide|libre|
|[UndpFAP1pGzsvdqj.htm](blood-lords-bestiary-items/UndpFAP1pGzsvdqj.htm)|Deflect Attack|Parade d'attaque|libre|
|[UntetOTQ8X6cyAD2.htm](blood-lords-bestiary-items/UntetOTQ8X6cyAD2.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[UOKwdCh7MrZhenl7.htm](blood-lords-bestiary-items/UOKwdCh7MrZhenl7.htm)|Light Hammer|+2,greaterStriking|Marteau léger de frappe supérieure +2|libre|
|[UP609WltTX4fCkkj.htm](blood-lords-bestiary-items/UP609WltTX4fCkkj.htm)|Ghast Fever|Fièvre des blêmes|libre|
|[uPv4K0q7JLRdxw59.htm](blood-lords-bestiary-items/uPv4K0q7JLRdxw59.htm)|Femur Head|Tête de fémur|libre|
|[UrGxTIwT0tis9T6y.htm](blood-lords-bestiary-items/UrGxTIwT0tis9T6y.htm)|Arcane Innate Spells|Sorts arcaniques innés|libre|
|[uSaKHqyDykW3HaFh.htm](blood-lords-bestiary-items/uSaKHqyDykW3HaFh.htm)|Negative Healing|Guérison négative|libre|
|[UTkWE4ZUCzqoWE6Z.htm](blood-lords-bestiary-items/UTkWE4ZUCzqoWE6Z.htm)|Cottage-Drained|Drainée par la chaumière|libre|
|[UTn7JwqCP0HRLQqF.htm](blood-lords-bestiary-items/UTn7JwqCP0HRLQqF.htm)|Comet|Comète|libre|
|[uvbnu7OYbYNCYUJw.htm](blood-lords-bestiary-items/uvbnu7OYbYNCYUJw.htm)|Swallowing Staircase|Escaliers engloutissant|libre|
|[ux8vrBZVt7jnRi5F.htm](blood-lords-bestiary-items/ux8vrBZVt7jnRi5F.htm)|Axan Walk|Marche dans Axan|libre|
|[uY4yPjPebXpLKNaB.htm](blood-lords-bestiary-items/uY4yPjPebXpLKNaB.htm)|Twilight Castrum Lore|Connaissance du Castrum du Crépuscule|libre|
|[UYzK236nOykYLMUv.htm](blood-lords-bestiary-items/UYzK236nOykYLMUv.htm)|Jaws|Mâchoire|libre|
|[uzChuknCpsIvD8fW.htm](blood-lords-bestiary-items/uzChuknCpsIvD8fW.htm)|Twilight Aura|Aura de pénombre|libre|
|[V07kVm9iio4YPqH7.htm](blood-lords-bestiary-items/V07kVm9iio4YPqH7.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[V5DSj7cALmgEz4hI.htm](blood-lords-bestiary-items/V5DSj7cALmgEz4hI.htm)|Manifest|Manifestation|libre|
|[V6cSeDrnF0rTdEUU.htm](blood-lords-bestiary-items/V6cSeDrnF0rTdEUU.htm)|At-Will Spells|Sorts à volonté|libre|
|[v7k1fJYnFbaFmbEP.htm](blood-lords-bestiary-items/v7k1fJYnFbaFmbEP.htm)|Triceratops Trample|Piétinement de tricératops|libre|
|[vawZsydY2VJutDZg.htm](blood-lords-bestiary-items/vawZsydY2VJutDZg.htm)|Swift Steps|Foulées rapides|libre|
|[vaZ4wD8dkZrsFDkV.htm](blood-lords-bestiary-items/vaZ4wD8dkZrsFDkV.htm)|Holy Flame Burst|Explosion de flammes sacrées|libre|
|[Vc4GNlil160ibq9a.htm](blood-lords-bestiary-items/Vc4GNlil160ibq9a.htm)|Consume Flesh|Dévorer la chair|libre|
|[vcUhZ1Yjwbi3Iwrb.htm](blood-lords-bestiary-items/vcUhZ1Yjwbi3Iwrb.htm)|Coffin Restoration|Cercueil restaurateur|libre|
|[vCxY0e6UpHU8p9OU.htm](blood-lords-bestiary-items/vCxY0e6UpHU8p9OU.htm)|Constrict|Constriction|libre|
|[VDcS16SUMCLKDMtn.htm](blood-lords-bestiary-items/VDcS16SUMCLKDMtn.htm)|Bard Composition Spells|Sorts de composition de barde|libre|
|[VFhfoXbwgIId8hxa.htm](blood-lords-bestiary-items/VFhfoXbwgIId8hxa.htm)|Consume Flesh|Dévorer la chair|libre|
|[VfOImdNeKq468YwC.htm](blood-lords-bestiary-items/VfOImdNeKq468YwC.htm)|Grave Breath|Souffle de tombe|libre|
|[VFw56rLyCfCxIkB4.htm](blood-lords-bestiary-items/VFw56rLyCfCxIkB4.htm)|Agonizing Pulse|Pulsation agonisante|libre|
|[VGvxP7aM22lBfMEo.htm](blood-lords-bestiary-items/VGvxP7aM22lBfMEo.htm)|Rejuvenation|Reconstruction|libre|
|[VHJWL55aSdxBz5lg.htm](blood-lords-bestiary-items/VHJWL55aSdxBz5lg.htm)|Ring of Energy Resistance (Greater) (Acid)|Anneau de résistance aux énergies supérieur (acide)|libre|
|[VhlqeFWKIF1E1hja.htm](blood-lords-bestiary-items/VhlqeFWKIF1E1hja.htm)|Rancid Bloat|Boursouflure rance|libre|
|[Vj1idQE8Mt3MqpOG.htm](blood-lords-bestiary-items/Vj1idQE8Mt3MqpOG.htm)|Claw|Griffe|libre|
|[vJ6hS46dH5sh4qCl.htm](blood-lords-bestiary-items/vJ6hS46dH5sh4qCl.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[vjD67fWSDyoQbfDm.htm](blood-lords-bestiary-items/vjD67fWSDyoQbfDm.htm)|Controlled|Contrôlé|libre|
|[vjDdzGbeVCJKJYWR.htm](blood-lords-bestiary-items/vjDdzGbeVCJKJYWR.htm)|Telekinetic Maneuver (At Will)|Manoeuvre télékinésique (à volonté)|libre|
|[vJvUPyR0P5cMpQEU.htm](blood-lords-bestiary-items/vJvUPyR0P5cMpQEU.htm)|Shadow Tether|Frappe de pilier d'ombre|libre|
|[vjwSuDHynNJbLODR.htm](blood-lords-bestiary-items/vjwSuDHynNJbLODR.htm)|Claw|Griffe|libre|
|[VKK7OYMbX0tnmj0D.htm](blood-lords-bestiary-items/VKK7OYMbX0tnmj0D.htm)|+2 Resilient Leather Armor|Armure de cuir de résilience +2|libre|
|[vKVyY0o18BT6Mn9r.htm](blood-lords-bestiary-items/vKVyY0o18BT6Mn9r.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[VM9gLYTZKgbWBBKH.htm](blood-lords-bestiary-items/VM9gLYTZKgbWBBKH.htm)|Shield Block|Blocage au bouclier|libre|
|[vmaElwLfTTptfaGk.htm](blood-lords-bestiary-items/vmaElwLfTTptfaGk.htm)|Sacrilegious Aura|Aura sacrilège|libre|
|[VMY4BFNvsVHx85It.htm](blood-lords-bestiary-items/VMY4BFNvsVHx85It.htm)|Sneak Attack|Attaque sournoise|libre|
|[vnzCRDJ2FC51cfuS.htm](blood-lords-bestiary-items/vnzCRDJ2FC51cfuS.htm)|Stealth|Discrétion|libre|
|[vPOhEUDV01FYntbI.htm](blood-lords-bestiary-items/vPOhEUDV01FYntbI.htm)|Consume Flesh|Dévorer la chair|libre|
|[VqeP6XiXPIzqWqxq.htm](blood-lords-bestiary-items/VqeP6XiXPIzqWqxq.htm)|Horn|Cornes|libre|
|[VQi7KOUra5k87o9X.htm](blood-lords-bestiary-items/VQi7KOUra5k87o9X.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[VR1LdQMTDyoCvk0h.htm](blood-lords-bestiary-items/VR1LdQMTDyoCvk0h.htm)|Powerful Charge|Recharge puissante|libre|
|[VrGfMpbeUpjxlrpG.htm](blood-lords-bestiary-items/VrGfMpbeUpjxlrpG.htm)|Discipline the Righteous|Punition des Justes|libre|
|[vSJp06MlLkMsPbGi.htm](blood-lords-bestiary-items/vSJp06MlLkMsPbGi.htm)|Mechitar Lore|Connaissance de Méchitar|libre|
|[vsxjIPIKMzsQXXfM.htm](blood-lords-bestiary-items/vsxjIPIKMzsQXXfM.htm)|Death Throes|Agonie fatale|libre|
|[VTHflJPW4vKM8wRk.htm](blood-lords-bestiary-items/VTHflJPW4vKM8wRk.htm)|Negative Healing|Guérison négative|libre|
|[VThOAv0PEn1ai8J3.htm](blood-lords-bestiary-items/VThOAv0PEn1ai8J3.htm)|Consume Soul|Dévorer les âmes|libre|
|[VTkNAYlIdGho2d2c.htm](blood-lords-bestiary-items/VTkNAYlIdGho2d2c.htm)|Telekinetic Haul (At Will)|Transport télékinésique (à volonté)|libre|
|[VuCwAaZGR2Jk7tMb.htm](blood-lords-bestiary-items/VuCwAaZGR2Jk7tMb.htm)|At-Will Spells|Sorts à volonté|libre|
|[VV1cgiDjco87IMUF.htm](blood-lords-bestiary-items/VV1cgiDjco87IMUF.htm)|Memory Drain|Drain de mémoire|libre|
|[VVfnxAlzF6B4Pltg.htm](blood-lords-bestiary-items/VVfnxAlzF6B4Pltg.htm)|Inhabit Vessel|Possession du récipient|libre|
|[vvqYni9OtPA0VdCT.htm](blood-lords-bestiary-items/vvqYni9OtPA0VdCT.htm)|Defensive Shove|Hurlement défensif|libre|
|[VxxBjROFrw1LpH7F.htm](blood-lords-bestiary-items/VxxBjROFrw1LpH7F.htm)|Bloodshift|Transfusion|libre|
|[vzdMOcWihp5bnBFL.htm](blood-lords-bestiary-items/vzdMOcWihp5bnBFL.htm)|Keys to Area E13 and the Crooked Coffin Grates|Clés de la zone E13 et de la grille du cercueil biscornu|libre|
|[W4mxFTUeCKve9DG0.htm](blood-lords-bestiary-items/W4mxFTUeCKve9DG0.htm)|Death Grip|Saisie mortelle|libre|
|[w5Egx0UK1FBC7Hoi.htm](blood-lords-bestiary-items/w5Egx0UK1FBC7Hoi.htm)|Reflective Rip|Déchirement de reflet|libre|
|[w7a0ZGNADQBToYwh.htm](blood-lords-bestiary-items/w7a0ZGNADQBToYwh.htm)|At-Will Spells|Sorts à volonté|libre|
|[w9ebZfueLzHUnfqK.htm](blood-lords-bestiary-items/w9ebZfueLzHUnfqK.htm)|+2 Status to All Saves vs. Magic|Bonus de statut de +2 aux jets de sauvegarde contre la magie|libre|
|[w9M1J6k8oc7fsfRA.htm](blood-lords-bestiary-items/w9M1J6k8oc7fsfRA.htm)|Sneak Attack|Attaque sournoise|libre|
|[waJmXcmb48bshSkk.htm](blood-lords-bestiary-items/waJmXcmb48bshSkk.htm)|Alchemical Amplification|Amplification alchémique|libre|
|[wbd0G04A6nbUyWv2.htm](blood-lords-bestiary-items/wbd0G04A6nbUyWv2.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[wDbMi1lnQxhLpIxn.htm](blood-lords-bestiary-items/wDbMi1lnQxhLpIxn.htm)|Instrument of Faith|Instrument de la foi|libre|
|[WDeJyGlH0HAvF93d.htm](blood-lords-bestiary-items/WDeJyGlH0HAvF93d.htm)|Rejuvenation|Reconstruction|libre|
|[WEdJXbD22EVMBSl5.htm](blood-lords-bestiary-items/WEdJXbD22EVMBSl5.htm)|Negative Healing|Guérison négative|libre|
|[wEZIX4a9Ttjwu8uy.htm](blood-lords-bestiary-items/wEZIX4a9Ttjwu8uy.htm)|Consume Arcana|Dévorer l'arcane|libre|
|[Wgd4BX6oolkOPDW3.htm](blood-lords-bestiary-items/Wgd4BX6oolkOPDW3.htm)|Pause|Pause|libre|
|[WGMIZhZMyM5r1alA.htm](blood-lords-bestiary-items/WGMIZhZMyM5r1alA.htm)|Adopt Persona|Adopter un personnage|libre|
|[whcBFRIvmylFHQWn.htm](blood-lords-bestiary-items/whcBFRIvmylFHQWn.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[wLRyKa64zoa1vKn9.htm](blood-lords-bestiary-items/wLRyKa64zoa1vKn9.htm)|Scald|Ébouillanter|libre|
|[wOjnTOBFA1g6tffb.htm](blood-lords-bestiary-items/wOjnTOBFA1g6tffb.htm)|Wand of Cone of Cold (Level 5)|Baguette de Cône de froid (niveau 5)|libre|
|[wpdkohoMNWXtuIZQ.htm](blood-lords-bestiary-items/wpdkohoMNWXtuIZQ.htm)|Graveknight's Curse|Malédiction du chevalier sépulcre|libre|
|[wqgIo7YcCxxHLfzU.htm](blood-lords-bestiary-items/wqgIo7YcCxxHLfzU.htm)|Tentacle Storm|Tempête de tentacules|libre|
|[wRtvh1eOtU7mphF0.htm](blood-lords-bestiary-items/wRtvh1eOtU7mphF0.htm)|Negative Healing|Guérison négative|libre|
|[wsBpREIBpfr8oUXt.htm](blood-lords-bestiary-items/wsBpREIBpfr8oUXt.htm)|+2 Greater Resilient Leather Armor|Armure en cuir de résilience supérieure +2|libre|
|[wsLOmWDluK35E0WC.htm](blood-lords-bestiary-items/wsLOmWDluK35E0WC.htm)|Jagged Ribs|Côtes dentelées|libre|
|[wTtR8l24xLgPYEeP.htm](blood-lords-bestiary-items/wTtR8l24xLgPYEeP.htm)|Tongues (Constant)|Don des langues (constant)|libre|
|[WuUSyujKitUYRtuc.htm](blood-lords-bestiary-items/WuUSyujKitUYRtuc.htm)|Flamboyant Reposition|Repositionnement flamboyant|libre|
|[WVJdmD2UxJ7zu1NV.htm](blood-lords-bestiary-items/WVJdmD2UxJ7zu1NV.htm)|Shred Soul|Déchirer l'âme|libre|
|[WvNKMvCATOkPQZgw.htm](blood-lords-bestiary-items/WvNKMvCATOkPQZgw.htm)|Flamboyant Reposition|Repositionnement flamboyant|libre|
|[WWbqQZvhR7cFlPYO.htm](blood-lords-bestiary-items/WWbqQZvhR7cFlPYO.htm)|Walking Stick|Bâton de marche|libre|
|[wWgjKSC4mzFRMvch.htm](blood-lords-bestiary-items/wWgjKSC4mzFRMvch.htm)|Leather Bag Containing Finger Bones|Sac en cuir qui contient des Doigts squelettiques|libre|
|[wWlq8cB7VIQ0GuXZ.htm](blood-lords-bestiary-items/wWlq8cB7VIQ0GuXZ.htm)|Paralysis|Paralysie|libre|
|[wwLrJbTJqJCt1RuM.htm](blood-lords-bestiary-items/wwLrJbTJqJCt1RuM.htm)|Mist Escape|Fuite brumeuse|libre|
|[WwYb9AM5fSY9eyx0.htm](blood-lords-bestiary-items/WwYb9AM5fSY9eyx0.htm)|Holy Warhammer|Marteau de guerre saint|libre|
|[Wyy3twUkJiUhrIsN.htm](blood-lords-bestiary-items/Wyy3twUkJiUhrIsN.htm)|+1 Status to All Saves vs. Magic|Bonus de statut de +1 aux jets de sauvegarde contre la magie|libre|
|[wzcH3jjppJJsDudW.htm](blood-lords-bestiary-items/wzcH3jjppJJsDudW.htm)|Religious Symbol of Pharasma|Symbole religieux de Pharasma|libre|
|[X5IplGzcFbK7w3tp.htm](blood-lords-bestiary-items/X5IplGzcFbK7w3tp.htm)|Gnashing Portal|Portail broyant|libre|
|[X8OraH5XBlGbpqWb.htm](blood-lords-bestiary-items/X8OraH5XBlGbpqWb.htm)|Negative Healing|Guérison négative|libre|
|[XACRvtRDd4VXY6aS.htm](blood-lords-bestiary-items/XACRvtRDd4VXY6aS.htm)|Regeneration 5 (Deactivated by Acid, Fire, or Positive)|Régénération 5 (désactivée par acide, feu ou positif)|libre|
|[xAphpqL3UkAoHLcW.htm](blood-lords-bestiary-items/xAphpqL3UkAoHLcW.htm)|Dimension Door (At Will)|Porte dimensionnelle (à volonté)|libre|
|[xBQ9apbaJI1Z2nTH.htm](blood-lords-bestiary-items/xBQ9apbaJI1Z2nTH.htm)|Exposed Vault|Chambre forte exposée|libre|
|[xCwgmDYeng8vkMB4.htm](blood-lords-bestiary-items/xCwgmDYeng8vkMB4.htm)|Tongues (Constant)|Don des langues (constant)|libre|
|[xd3KrbrDOX8LkahX.htm](blood-lords-bestiary-items/xd3KrbrDOX8LkahX.htm)|Always Ready|Toujours prêt|libre|
|[XEjRf2KDZmpge6Er.htm](blood-lords-bestiary-items/XEjRf2KDZmpge6Er.htm)|Claw|Griffe|libre|
|[xGcUmFIwoqQu2UbT.htm](blood-lords-bestiary-items/xGcUmFIwoqQu2UbT.htm)|Cackling Coven|Cercle caquetant|libre|
|[xiiUBkwc3mYsWqlz.htm](blood-lords-bestiary-items/xiiUBkwc3mYsWqlz.htm)|Grab|Empoignade/agrippement|libre|
|[XiQifOVkG3qGEPwz.htm](blood-lords-bestiary-items/XiQifOVkG3qGEPwz.htm)|Grab|Empoignade/Agrippement|libre|
|[XJ9PSS8NeVa0xNTG.htm](blood-lords-bestiary-items/XJ9PSS8NeVa0xNTG.htm)|Claw|Griffe|libre|
|[XJj8TQj5hBV0uRCH.htm](blood-lords-bestiary-items/XJj8TQj5hBV0uRCH.htm)|Entropy's Shadow|Ombre d'entropie|libre|
|[xjklqk1B1Bg4zbAD.htm](blood-lords-bestiary-items/xjklqk1B1Bg4zbAD.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[XjrYXkqhue5UlQ94.htm](blood-lords-bestiary-items/XjrYXkqhue5UlQ94.htm)|Constrict|Constriction|libre|
|[xKNmdlpRseAGp91L.htm](blood-lords-bestiary-items/xKNmdlpRseAGp91L.htm)|Paralysis|Paralysie|libre|
|[XlOilQx1kVU4hWjH.htm](blood-lords-bestiary-items/XlOilQx1kVU4hWjH.htm)|Mental Rebirth|Renaissance mentale|libre|
|[xNjTIq85rIoWNhud.htm](blood-lords-bestiary-items/xNjTIq85rIoWNhud.htm)|Claw|Griffe|libre|
|[xNvRKk7Gf1OEuTjE.htm](blood-lords-bestiary-items/xNvRKk7Gf1OEuTjE.htm)|Scent (Imprecise 30) feet|Odorat 9 m (imprécis)|libre|
|[xPMIKJWGAIodN3DK.htm](blood-lords-bestiary-items/xPMIKJWGAIodN3DK.htm)|Composite Shortbow|+1,striking|Arc composite de frappe +1|libre|
|[xRxPAjswWOJyItbO.htm](blood-lords-bestiary-items/xRxPAjswWOJyItbO.htm)|Sudden Stitch|Couture soudaine|libre|
|[XSa2fjzbP3OVWgIy.htm](blood-lords-bestiary-items/XSa2fjzbP3OVWgIy.htm)|Inexorable|Inexorable|libre|
|[XTn4FwWPKK9n9MMq.htm](blood-lords-bestiary-items/XTn4FwWPKK9n9MMq.htm)|+2 Status to All Saves vs. Magic|Bonus de statut de +2 aux jets de sauvegarde contre la magie|libre|
|[XUFSbVXhiCWr2X8p.htm](blood-lords-bestiary-items/XUFSbVXhiCWr2X8p.htm)|Coven|Cercle|libre|
|[XUNSyrSkuWbDNTnf.htm](blood-lords-bestiary-items/XUNSyrSkuWbDNTnf.htm)|Spirit Needle|Aiguille esprit|libre|
|[xVaCNUsDfuwnWzmc.htm](blood-lords-bestiary-items/xVaCNUsDfuwnWzmc.htm)|Claw|Griffe|libre|
|[xvESPcpt1GkrOZBG.htm](blood-lords-bestiary-items/xvESPcpt1GkrOZBG.htm)|Negative Healing|Guérison négative|libre|
|[XVKwWXLGUpYzYyav.htm](blood-lords-bestiary-items/XVKwWXLGUpYzYyav.htm)|Spectral Claw|Griffe spectrale|libre|
|[xwHnoJPMGsM5oOIA.htm](blood-lords-bestiary-items/xwHnoJPMGsM5oOIA.htm)|Collapse|Effondrement|libre|
|[xy2azhCTZ0mmZqoS.htm](blood-lords-bestiary-items/xy2azhCTZ0mmZqoS.htm)|Stench|Puanteur|libre|
|[xyEue2d3lHHYREhF.htm](blood-lords-bestiary-items/xyEue2d3lHHYREhF.htm)|Smokestick (Lesser, Infused)|Bâton fumigène inférieur imprégné|libre|
|[y1bBayQkcWaeH3LE.htm](blood-lords-bestiary-items/y1bBayQkcWaeH3LE.htm)|Divine Focus Spells|Sorts divins focalisés|libre|
|[Y23JOg9qOtShESZg.htm](blood-lords-bestiary-items/Y23JOg9qOtShESZg.htm)|Change Shape|Changement de forme|libre|
|[y2AtKGIP51jiqnbi.htm](blood-lords-bestiary-items/y2AtKGIP51jiqnbi.htm)|Constant Spells|Sorts constants|libre|
|[Y2DI9Wc13k5UjDtg.htm](blood-lords-bestiary-items/Y2DI9Wc13k5UjDtg.htm)|Bardic Lore|Connaissance bardique|libre|
|[Y5cpbPonKPPXwWsy.htm](blood-lords-bestiary-items/Y5cpbPonKPPXwWsy.htm)|Smashing Blow|Coup écrasant|libre|
|[Y6NnCtJP7mXoLBrC.htm](blood-lords-bestiary-items/Y6NnCtJP7mXoLBrC.htm)|Negative Healing|Guérison négative|libre|
|[Y7hvS4ZYdpmVnEKM.htm](blood-lords-bestiary-items/Y7hvS4ZYdpmVnEKM.htm)|All-Knowing Eyes|Yeux omniscients|libre|
|[Y8HYNHds5L2GhKgk.htm](blood-lords-bestiary-items/Y8HYNHds5L2GhKgk.htm)|Frost Vial (Moderate, Infused)|Fiole de givre modérée, imprégnée|libre|
|[YBC11CkpCUaR8erJ.htm](blood-lords-bestiary-items/YBC11CkpCUaR8erJ.htm)|Speak with Animals (At Will)|Communication avec les animaux (à volonté)|libre|
|[YbIbr2pfYfDzv9RD.htm](blood-lords-bestiary-items/YbIbr2pfYfDzv9RD.htm)|Shield Ward|Gardien de pupille|libre|
|[ybRRLWDLtsP4WDa2.htm](blood-lords-bestiary-items/ybRRLWDLtsP4WDa2.htm)|+1 Status to All Saves vs. Magic|Bonus de statut de +1 aux jets de sauvegarde contre la magie|libre|
|[yByhPWWyLYMF0xPt.htm](blood-lords-bestiary-items/yByhPWWyLYMF0xPt.htm)|Ruinous Weapons|Armes du désastre|libre|
|[yCs2nE8K8PXJBAKB.htm](blood-lords-bestiary-items/yCs2nE8K8PXJBAKB.htm)|Mental Rebirth|Renaissance mentale|libre|
|[ydykhCatB0hphtVK.htm](blood-lords-bestiary-items/ydykhCatB0hphtVK.htm)|Claw|Griffe|libre|
|[YE6KsqzYHCVdO35R.htm](blood-lords-bestiary-items/YE6KsqzYHCVdO35R.htm)|+1 Resilient Full Plate|Harnois de résilience +1|libre|
|[yFTwKxJwOCyZ8E4l.htm](blood-lords-bestiary-items/yFTwKxJwOCyZ8E4l.htm)|Scream|Hurler|libre|
|[ygj8aubmhpmYUgE2.htm](blood-lords-bestiary-items/ygj8aubmhpmYUgE2.htm)|Intruder Alert|Alerte d'intrusion|libre|
|[yhDldk2WLsqfXkIk.htm](blood-lords-bestiary-items/yhDldk2WLsqfXkIk.htm)|Seize|Saisir|libre|
|[yhPRRdAVevZsgbVl.htm](blood-lords-bestiary-items/yhPRRdAVevZsgbVl.htm)|Grab|Empoignade/Agrippement|libre|
|[yjzcKHxxLILIkpm9.htm](blood-lords-bestiary-items/yjzcKHxxLILIkpm9.htm)|Familiar|familier|libre|
|[ylEXl7NSakJ1G8mo.htm](blood-lords-bestiary-items/ylEXl7NSakJ1G8mo.htm)|Swift Leap|Bond rapide|libre|
|[Ymu9fUPAY7am7msC.htm](blood-lords-bestiary-items/Ymu9fUPAY7am7msC.htm)|Fast Healing 10|Guérison accélérée 10|libre|
|[yo7zlMnCZOT8dMbE.htm](blood-lords-bestiary-items/yo7zlMnCZOT8dMbE.htm)|Sneak Attack|Attaque sournoise|libre|
|[YoC3A5KlKj0hBnrB.htm](blood-lords-bestiary-items/YoC3A5KlKj0hBnrB.htm)|Clinging Gloom|Obscurité collante|libre|
|[YOv13xbH1a25VfsQ.htm](blood-lords-bestiary-items/YOv13xbH1a25VfsQ.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[ypGQGTvYyK5bAhwW.htm](blood-lords-bestiary-items/ypGQGTvYyK5bAhwW.htm)|Seize Soul|Saisir l'âme|libre|
|[YqNb6MK5rPSJOvIZ.htm](blood-lords-bestiary-items/YqNb6MK5rPSJOvIZ.htm)|Counterspell|Contresort|libre|
|[yQZhpnGLdofg8nwo.htm](blood-lords-bestiary-items/yQZhpnGLdofg8nwo.htm)|Skeleton Key That is Also the Key to Area F10|Un passe-partout qui est aussi la clé de la zone F10|libre|
|[yrD2XqRweNn8fZbw.htm](blood-lords-bestiary-items/yrD2XqRweNn8fZbw.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|libre|
|[yThaeSJv15gPmoX4.htm](blood-lords-bestiary-items/yThaeSJv15gPmoX4.htm)|Grab|Empoignade/Agrippement|libre|
|[YtNXbGTLwW1Ju63J.htm](blood-lords-bestiary-items/YtNXbGTLwW1Ju63J.htm)|Holomog Lore|Connaissance d'Homolog|libre|
|[yuM5Q48OH1pbgS8U.htm](blood-lords-bestiary-items/yuM5Q48OH1pbgS8U.htm)|Flagon|Choppe|libre|
|[yuunf9T3hQvYTJmf.htm](blood-lords-bestiary-items/yuunf9T3hQvYTJmf.htm)|Theater Lore|Connaissance Théâtre|libre|
|[YvpiKlId2of8rjcZ.htm](blood-lords-bestiary-items/YvpiKlId2of8rjcZ.htm)|Spear Fisher|Lance de pêche|libre|
|[YvUhNZ1GT4PmqTz5.htm](blood-lords-bestiary-items/YvUhNZ1GT4PmqTz5.htm)|Venomous Gaze|Regard empoisonné|libre|
|[YW28JJ9FehN53tag.htm](blood-lords-bestiary-items/YW28JJ9FehN53tag.htm)|Grab|Empoignade/Agrippement|libre|
|[yw7bRJOAMVdTnVkG.htm](blood-lords-bestiary-items/yw7bRJOAMVdTnVkG.htm)|Negative Healing|Guérison négative|libre|
|[YXBezR9P1AqXwyGR.htm](blood-lords-bestiary-items/YXBezR9P1AqXwyGR.htm)|Negative Healing|Guérison négative|libre|
|[yyfxiplMvGMTqLKy.htm](blood-lords-bestiary-items/yyfxiplMvGMTqLKy.htm)|Ghostly Cookware|Ustensile de cuisine spectral|libre|
|[yZ2k03MCmmIOSIkx.htm](blood-lords-bestiary-items/yZ2k03MCmmIOSIkx.htm)|Rock|Rocher|libre|
|[Yz5J0hlFYdY6oarj.htm](blood-lords-bestiary-items/Yz5J0hlFYdY6oarj.htm)|Two-Weapon Flurry|Déluge à deux armes|libre|
|[YzCBJTb2mBGfoo0v.htm](blood-lords-bestiary-items/YzCBJTb2mBGfoo0v.htm)|Vicious Criticals|Critique vicieux|libre|
|[z3ZUZXI9r4nNBWhw.htm](blood-lords-bestiary-items/z3ZUZXI9r4nNBWhw.htm)|Sacrifice Ally|Allié sacrifié|libre|
|[z7kqAkB367vaH1Zy.htm](blood-lords-bestiary-items/z7kqAkB367vaH1Zy.htm)|Quickened Casting|Incantation accélérée|libre|
|[Z7lMB4d209mIsUU1.htm](blood-lords-bestiary-items/Z7lMB4d209mIsUU1.htm)|Dreadful Vision|Vision effroyable|libre|
|[Z7wj6l6y9SkUrAYv.htm](blood-lords-bestiary-items/Z7wj6l6y9SkUrAYv.htm)|Induction Word|Mot d'intégration|libre|
|[Z82AdmhDztEQ1Zz2.htm](blood-lords-bestiary-items/Z82AdmhDztEQ1Zz2.htm)|Khopesh|+2,striking|Khopesh de frappe +2|libre|
|[z9bxNbj1u1SM1aob.htm](blood-lords-bestiary-items/z9bxNbj1u1SM1aob.htm)|Claw|Griffe|libre|
|[ZADMtLtTzI1RQr8d.htm](blood-lords-bestiary-items/ZADMtLtTzI1RQr8d.htm)|Constant Spells|Sorts constants|libre|
|[ZCE0MMUew9kQ0v3t.htm](blood-lords-bestiary-items/ZCE0MMUew9kQ0v3t.htm)|Nightmare Essence|Essence de cauchemar|libre|
|[ZdM07jdOOFlqHVer.htm](blood-lords-bestiary-items/ZdM07jdOOFlqHVer.htm)|Lash|Lanière d'ombre|libre|
|[zGBrolmCtqnMeB9V.htm](blood-lords-bestiary-items/zGBrolmCtqnMeB9V.htm)|Hatred for Secret Doors|Haine des portes secrètes|libre|
|[zGemjvWNZRYrlrdm.htm](blood-lords-bestiary-items/zGemjvWNZRYrlrdm.htm)|Ghast Fever|Fièvre des blêmes|libre|
|[zgmptcANNsvVBD3Y.htm](blood-lords-bestiary-items/zgmptcANNsvVBD3Y.htm)|Claw|Griffe|libre|
|[zGPZCB9sz4MqsHYZ.htm](blood-lords-bestiary-items/zGPZCB9sz4MqsHYZ.htm)|Claw|Griffe|libre|
|[zGqC1f9nAbkhFc9Q.htm](blood-lords-bestiary-items/zGqC1f9nAbkhFc9Q.htm)|Iron Command|Ordre de fer|libre|
|[zGtSO1t7RINeWEir.htm](blood-lords-bestiary-items/zGtSO1t7RINeWEir.htm)|Form Up|Se reformer|libre|
|[ZJAuqeahi7qsfRUG.htm](blood-lords-bestiary-items/ZJAuqeahi7qsfRUG.htm)|Death Feast|Festin de cadavres|libre|
|[ZLcO9eT7RdF6M8kF.htm](blood-lords-bestiary-items/ZLcO9eT7RdF6M8kF.htm)|Paralysis|Paralysie|libre|
|[ZlOg2MVO2KWGm4wA.htm](blood-lords-bestiary-items/ZlOg2MVO2KWGm4wA.htm)|Center Self|Se recentrer|libre|
|[ZNaSx9Tg27cxalB9.htm](blood-lords-bestiary-items/ZNaSx9Tg27cxalB9.htm)|Harness Wellspring|Dompter la source|libre|
|[zo6cOXxj2pFK0kYO.htm](blood-lords-bestiary-items/zo6cOXxj2pFK0kYO.htm)|Regeneration 15 (Deactivated by Acid)|Régénération 15 (désactivée par l'acide)|libre|
|[ZOPyMr0me2g4Llk5.htm](blood-lords-bestiary-items/ZOPyMr0me2g4Llk5.htm)|Grab|Empoignade/Agrippement|libre|
|[zPX0qDvfC1YSPFjQ.htm](blood-lords-bestiary-items/zPX0qDvfC1YSPFjQ.htm)|Witch Patron|Protecteur sorcier|libre|
|[ZqZ3RpathLbDe0yK.htm](blood-lords-bestiary-items/ZqZ3RpathLbDe0yK.htm)|Vampire Weaknesses|Faiblesse des vampires|libre|
|[zSHK91gMB7zk1mWG.htm](blood-lords-bestiary-items/zSHK91gMB7zk1mWG.htm)|Eye Beam|Rayon oculaire|libre|
|[ZTdFKmaD7Imp3Vkr.htm](blood-lords-bestiary-items/ZTdFKmaD7Imp3Vkr.htm)|Magaambya Scar|Cicatrice de Magaambya|libre|
|[ZTu8zWX3EVqOMcZs.htm](blood-lords-bestiary-items/ZTu8zWX3EVqOMcZs.htm)|Emotional Consumption|Consommation émotionnelle|libre|
|[zvdJqSyO972cDcqm.htm](blood-lords-bestiary-items/zvdJqSyO972cDcqm.htm)|Divine Destruction|Destruction divine|libre|
|[ZvSjrujGGGepTjGL.htm](blood-lords-bestiary-items/ZvSjrujGGGepTjGL.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[Zw6ce2IdxVTrq3mO.htm](blood-lords-bestiary-items/Zw6ce2IdxVTrq3mO.htm)|Negative Healing|Guérison négative|libre|
|[ZWsduSo456POeA9g.htm](blood-lords-bestiary-items/ZWsduSo456POeA9g.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[ZxBQynV0P1lTn1JR.htm](blood-lords-bestiary-items/ZxBQynV0P1lTn1JR.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[ZZgCdwFX4QyXLmRV.htm](blood-lords-bestiary-items/ZZgCdwFX4QyXLmRV.htm)|Swift Leap|Bond rapide|libre|
