# État de la traduction (travel-guide-bestiary-items)

 * **libre**: 28
 * **officielle**: 1


Dernière mise à jour: 2023-07-12 20:28 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[28P5kFSjVh3w1R9o.htm](travel-guide-bestiary-items/28P5kFSjVh3w1R9o.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[5lPaA6GKal2YHAjj.htm](travel-guide-bestiary-items/5lPaA6GKal2YHAjj.htm)|Jaws|Mâchoire|libre|
|[7I0I09vYwkMcNZoL.htm](travel-guide-bestiary-items/7I0I09vYwkMcNZoL.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[cpSC9RcShIFK28EU.htm](travel-guide-bestiary-items/cpSC9RcShIFK28EU.htm)|Snout|Rostre|libre|
|[CyJqBVfPXX4Fp283.htm](travel-guide-bestiary-items/CyJqBVfPXX4Fp283.htm)|Gallop|Galop|libre|
|[dB4Pr3yiNKH32fE8.htm](travel-guide-bestiary-items/dB4Pr3yiNKH32fE8.htm)|Knockdown|Renversement|libre|
|[g7nl0QahmH5JaunK.htm](travel-guide-bestiary-items/g7nl0QahmH5JaunK.htm)|Low-Light Vision|Vision nocturne|libre|
|[HNM58d8JH28lGstw.htm](travel-guide-bestiary-items/HNM58d8JH28lGstw.htm)|Hoof|Sabot|libre|
|[HuxqXHbNPMVfb98R.htm](travel-guide-bestiary-items/HuxqXHbNPMVfb98R.htm)|Buck|Désarçonner|libre|
|[I1cXiDVfmHepyrXf.htm](travel-guide-bestiary-items/I1cXiDVfmHepyrXf.htm)|Low-Light Vision|Vision nocturne|libre|
|[jN9qLXiQfaOBlFfC.htm](travel-guide-bestiary-items/jN9qLXiQfaOBlFfC.htm)|Ramming Speed|Vitesse d'éperonnage|libre|
|[khgGPSxgTQcgev3o.htm](travel-guide-bestiary-items/khgGPSxgTQcgev3o.htm)|Deep Breath|Inspiration profonde|libre|
|[LIdmD7VqztundfTh.htm](travel-guide-bestiary-items/LIdmD7VqztundfTh.htm)|Beak|Bec|libre|
|[Mjlek9OZ25Tga7Cg.htm](travel-guide-bestiary-items/Mjlek9OZ25Tga7Cg.htm)|Filth Fever|Fièvre de la fange|libre|
|[NX0owCPUZbsVJ4az.htm](travel-guide-bestiary-items/NX0owCPUZbsVJ4az.htm)|Jaws|Mâchoire|libre|
|[O7BM6p23R9nIMLOP.htm](travel-guide-bestiary-items/O7BM6p23R9nIMLOP.htm)|Low-Light Vision|Vision nocturne|libre|
|[oikg7iKnE0OwX6CV.htm](travel-guide-bestiary-items/oikg7iKnE0OwX6CV.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[qCmKPruRObzVL1Dr.htm](travel-guide-bestiary-items/qCmKPruRObzVL1Dr.htm)|Screeching Advance|Progression hurlante|libre|
|[r5yruYZzaDx9MoHR.htm](travel-guide-bestiary-items/r5yruYZzaDx9MoHR.htm)|Low-Light Vision|Vision nocturne|libre|
|[sqceAJWlpdKGnHvx.htm](travel-guide-bestiary-items/sqceAJWlpdKGnHvx.htm)|Talon|Serres|libre|
|[TBp1tYUPtYoOEonO.htm](travel-guide-bestiary-items/TBp1tYUPtYoOEonO.htm)|Aquatic Echolocation|Écholocalisation aquatique|libre|
|[TCZQdv29obsz4Rie.htm](travel-guide-bestiary-items/TCZQdv29obsz4Rie.htm)|Low-Light Vision|Vision nocturne|libre|
|[TyHmjC5wvxo29J5L.htm](travel-guide-bestiary-items/TyHmjC5wvxo29J5L.htm)|Grab|Empoignade|libre|
|[vBFHfrDtlZ5vYw2q.htm](travel-guide-bestiary-items/vBFHfrDtlZ5vYw2q.htm)|Athletics|Athlétisme|libre|
|[VQW9KJrLdz5lDjJ9.htm](travel-guide-bestiary-items/VQW9KJrLdz5lDjJ9.htm)|Scent (Imprecise) 30 feet|Odorat (imprecis) 9 m|libre|
|[vTXaShzYmcNbRjzk.htm](travel-guide-bestiary-items/vTXaShzYmcNbRjzk.htm)|Bloodcurdling Screech|Hurlement à glacer le sang|officielle|
|[vudcfpUT9un2pDOP.htm](travel-guide-bestiary-items/vudcfpUT9un2pDOP.htm)|Gnaw|Ronger|libre|
|[x5GWeyJ3dSDa0npJ.htm](travel-guide-bestiary-items/x5GWeyJ3dSDa0npJ.htm)|Spit|Crachat|libre|
|[yLnwKltbJHYpn8qC.htm](travel-guide-bestiary-items/yLnwKltbJHYpn8qC.htm)|Jaws|Mâchoires|libre|
