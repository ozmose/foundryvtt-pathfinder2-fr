# État de la traduction (vehicles-items)

 * **libre**: 75


Dernière mise à jour: 2023-07-12 20:28 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0CcYSQlAiZcoMkF0.htm](vehicles-items/0CcYSQlAiZcoMkF0.htm)|Skewering Step|Pattes embrochantes|libre|
|[0pWllu1H0c2ZTJbM.htm](vehicles-items/0pWllu1H0c2ZTJbM.htm)|Hauler|Poids lourd|libre|
|[2UISHdYECuDVkedh.htm](vehicles-items/2UISHdYECuDVkedh.htm)|Sluggish|Lent|libre|
|[38Zgv0sAfLXH2dfB.htm](vehicles-items/38Zgv0sAfLXH2dfB.htm)|Wind-Up|Remonter un dispositif|libre|
|[3Vt9tFjjJmSsJ99w.htm](vehicles-items/3Vt9tFjjJmSsJ99w.htm)|Wind-Up|Remonter un dispositif|libre|
|[3WQ9vWhVa2yyJCcV.htm](vehicles-items/3WQ9vWhVa2yyJCcV.htm)|Luxurious Accommodations|Logements luxueux|libre|
|[79YskaB7D1bAmIDD.htm](vehicles-items/79YskaB7D1bAmIDD.htm)|Volatile Flamethrower|Lance-flammes anti volatiles|libre|
|[7LOtkjkF6S8p6CRA.htm](vehicles-items/7LOtkjkF6S8p6CRA.htm)|Tether Buoy|Relié à une bouée|libre|
|[7OqKezdHChnhLvmc.htm](vehicles-items/7OqKezdHChnhLvmc.htm)|Sluggish|Lent|libre|
|[9UcPAPPrzEhaoWgY.htm](vehicles-items/9UcPAPPrzEhaoWgY.htm)|Portaged|Portatif|libre|
|[b3OQJLYv4hEX7yhn.htm](vehicles-items/b3OQJLYv4hEX7yhn.htm)|Long Reach|Longue portée|libre|
|[Buxx8NC3JQzeLsjs.htm](vehicles-items/Buxx8NC3JQzeLsjs.htm)|Sluggish|Lent|libre|
|[cFwKXyV5W3o1v3KK.htm](vehicles-items/cFwKXyV5W3o1v3KK.htm)|Adhesive Secretions|Secrétion adhésives|libre|
|[ChjOaa11CkBJeVJ8.htm](vehicles-items/ChjOaa11CkBJeVJ8.htm)|Glow|Luire|libre|
|[dabct2Mb2XcbWKwb.htm](vehicles-items/dabct2Mb2XcbWKwb.htm)|Maneuverable|Manoeuvrable|libre|
|[DoCusD26UNQbmFNI.htm](vehicles-items/DoCusD26UNQbmFNI.htm)|Continous Track|Piste continue|libre|
|[eMf9xcynjFjF5ma8.htm](vehicles-items/eMf9xcynjFjF5ma8.htm)|Wind-Up|Remonter un dispositif|libre|
|[eXdF4IskaccqgTpS.htm](vehicles-items/eXdF4IskaccqgTpS.htm)|Survey|Enquêter|libre|
|[fG2IE4D8GEe8e98D.htm](vehicles-items/fG2IE4D8GEe8e98D.htm)|Hover|Planer|libre|
|[gafl9oZCmfSGtfxj.htm](vehicles-items/gafl9oZCmfSGtfxj.htm)|Hopper|Sautiller|libre|
|[Gy4AuNFAGBzfjRFq.htm](vehicles-items/Gy4AuNFAGBzfjRFq.htm)|Smog|Smog|libre|
|[hCwx6Q30DbNVZa88.htm](vehicles-items/hCwx6Q30DbNVZa88.htm)|Sluggish|Lent|libre|
|[iU2xj2sIMM3UdezU.htm](vehicles-items/iU2xj2sIMM3UdezU.htm)|Speed Boost|Poussée de vitesse|libre|
|[Ix4GHIqSUqHawiG8.htm](vehicles-items/Ix4GHIqSUqHawiG8.htm)|Starting Drop|Chute de départ|libre|
|[izZwMB8zgdsWje9o.htm](vehicles-items/izZwMB8zgdsWje9o.htm)|Prismatic Defense|Défense prismatique|libre|
|[jUfFzSzu0PIwCSgB.htm](vehicles-items/jUfFzSzu0PIwCSgB.htm)|Mountain Traverser|Franchisseur de montagnes|libre|
|[jv3XUW2upC7aFRtR.htm](vehicles-items/jv3XUW2upC7aFRtR.htm)|Open Eyes|Ouvrir les yeux|libre|
|[kbVUEpQsOKNyw0BS.htm](vehicles-items/kbVUEpQsOKNyw0BS.htm)|Steam Cloud|Nuage de vapeur|libre|
|[KFSpJiu6uoHoZl1b.htm](vehicles-items/KFSpJiu6uoHoZl1b.htm)|Manipulate Hands|Mains manipulatrices|libre|
|[kQbyDtF75cOAr2Lc.htm](vehicles-items/kQbyDtF75cOAr2Lc.htm)|Wind-Up|Remonter un dispositif|libre|
|[kr1leAaQlGXDTwn6.htm](vehicles-items/kr1leAaQlGXDTwn6.htm)|Wind-Up|Remonter un dispositif|libre|
|[kt9xSEJnhJNdCBEy.htm](vehicles-items/kt9xSEJnhJNdCBEy.htm)|Captivating Wealth|Richesse captivante|libre|
|[L0S2jaWemV8ndKIE.htm](vehicles-items/L0S2jaWemV8ndKIE.htm)|Sidecars|Nacelles|libre|
|[L5LOLKdKxPBT5LCB.htm](vehicles-items/L5LOLKdKxPBT5LCB.htm)|Iron Rim|Bordure de fer|libre|
|[lctMsuE0KxKUZgnH.htm](vehicles-items/lctMsuE0KxKUZgnH.htm)|Wind-Up|Remonter un dispositif|libre|
|[lTX8qPMdwaThuCnc.htm](vehicles-items/lTX8qPMdwaThuCnc.htm)|Quaking Step|Pas de tremblement|libre|
|[MBdI0c7xv3ufaJM8.htm](vehicles-items/MBdI0c7xv3ufaJM8.htm)|Environmental Protections|Protections environnementales|libre|
|[mTjyYbkFbVeKoGO8.htm](vehicles-items/mTjyYbkFbVeKoGO8.htm)|Submersible|Submersible|libre|
|[O5JvDSJWNWG2VtpR.htm](vehicles-items/O5JvDSJWNWG2VtpR.htm)|Electrify|Electrifier|libre|
|[oWlyE6XYOLX2gBB6.htm](vehicles-items/oWlyE6XYOLX2gBB6.htm)|Weapon Mount|Arme montée|libre|
|[PcXltLxBnG3FD8dH.htm](vehicles-items/PcXltLxBnG3FD8dH.htm)|Portable|Portable|libre|
|[PqsmKTcXOM0aNYo8.htm](vehicles-items/PqsmKTcXOM0aNYo8.htm)|Shard Trail|Piste d'éclats|libre|
|[QUHIpCfam8YMHy6U.htm](vehicles-items/QUHIpCfam8YMHy6U.htm)|Electrical Absorption|Absorption électrique|libre|
|[QwXzg5zGAZg4h78t.htm](vehicles-items/QwXzg5zGAZg4h78t.htm)|Fragile|Fragile|libre|
|[Qx3tJuimyRKT7l2W.htm](vehicles-items/Qx3tJuimyRKT7l2W.htm)|Cable|Cable|libre|
|[R2SaS8szVUWwiHQr.htm](vehicles-items/R2SaS8szVUWwiHQr.htm)|Streamlined|Profilé|libre|
|[R4Co971n6x5ZNJHd.htm](vehicles-items/R4Co971n6x5ZNJHd.htm)|Wind-Up|Remonter un dispositif|libre|
|[Rb4MyOLlodPUZdJX.htm](vehicles-items/Rb4MyOLlodPUZdJX.htm)|Flame Jet|Jet de flammes|libre|
|[rDwfQJPnK5YyRx7v.htm](vehicles-items/rDwfQJPnK5YyRx7v.htm)|Sluggish|Lent|libre|
|[RJf32Bczb9zQaFqs.htm](vehicles-items/RJf32Bczb9zQaFqs.htm)|Sluggish|Lent|libre|
|[rvLRBCsFQMdUKtar.htm](vehicles-items/rvLRBCsFQMdUKtar.htm)|Sand Skimmer|Écumeur des sables|libre|
|[scXXuno9YNm1GWXK.htm](vehicles-items/scXXuno9YNm1GWXK.htm)|Pivoting Seats|Sièges pivotants|libre|
|[tFb8nV3bxHVLeU8Z.htm](vehicles-items/tFb8nV3bxHVLeU8Z.htm)|Maneuverable|Manœuvrable|libre|
|[TMYxeMEQ1ilLoAlJ.htm](vehicles-items/TMYxeMEQ1ilLoAlJ.htm)|Wind Up|Remonter|libre|
|[TPj4AFDezXubgaJm.htm](vehicles-items/TPj4AFDezXubgaJm.htm)|Adamantine Drill|Vilebrequin en adamantium|libre|
|[tS9MnEsewMslgzhi.htm](vehicles-items/tS9MnEsewMslgzhi.htm)|Wind-Up|Remonter un dispositif|libre|
|[uoev6dRtG1YFWPrn.htm](vehicles-items/uoev6dRtG1YFWPrn.htm)|Open Portholes|Ouvrir les hublots|libre|
|[uP38cyn9BsX8ni0O.htm](vehicles-items/uP38cyn9BsX8ni0O.htm)|Massive Jump|Saut massif|libre|
|[upS8Tctv0gf8rQOC.htm](vehicles-items/upS8Tctv0gf8rQOC.htm)|Ballast Release|Vider les ballasts|libre|
|[UQr6y2RUCvhlcvZ3.htm](vehicles-items/UQr6y2RUCvhlcvZ3.htm)|Protective Barrier|Barrière protectrice|libre|
|[UZrxxtljdLoLewB9.htm](vehicles-items/UZrxxtljdLoLewB9.htm)|Deploy Wheels|Déployer les roues|libre|
|[V0HecEvj92RlltT5.htm](vehicles-items/V0HecEvj92RlltT5.htm)|Sluggish|Lent|libre|
|[V0Iw3ZOvbTqoxlvz.htm](vehicles-items/V0Iw3ZOvbTqoxlvz.htm)|Jump Jet|Propulsion de saut|libre|
|[V86H5yeH71jmelzY.htm](vehicles-items/V86H5yeH71jmelzY.htm)|Submersible|Submersible|libre|
|[vDMm6mf0S2i9JMzd.htm](vehicles-items/vDMm6mf0S2i9JMzd.htm)|Bolt Blast|Faisceau d'électricité|libre|
|[VqS08x8SzApqcmVy.htm](vehicles-items/VqS08x8SzApqcmVy.htm)|Weapon Mounts|Supports d'arme|libre|
|[W01aasqmTJL5TpkW.htm](vehicles-items/W01aasqmTJL5TpkW.htm)|Unstable Launch|Lancement instable|libre|
|[xB0A9RW1Zi6TC7g0.htm](vehicles-items/xB0A9RW1Zi6TC7g0.htm)|Starting Drop|Chute initiale|libre|
|[XLttadhtgYoOq6f1.htm](vehicles-items/XLttadhtgYoOq6f1.htm)|Flash|Flash|libre|
|[XvCWEJlIIDSfz2EA.htm](vehicles-items/XvCWEJlIIDSfz2EA.htm)|Ice Traverser|Franchisseur de glace|libre|
|[yKd1wGx6zmlBwk0u.htm](vehicles-items/yKd1wGx6zmlBwk0u.htm)|Fall Apart|Tombe en morceaux|libre|
|[Yr2rxvC8Hzmb70KZ.htm](vehicles-items/Yr2rxvC8Hzmb70KZ.htm)|Sluggish|Lent|libre|
|[zEf7XS6RlEjM0VSO.htm](vehicles-items/zEf7XS6RlEjM0VSO.htm)|Wind-Up|Remonter un dispositif|libre|
|[zGMODH6r1X3ZbwB1.htm](vehicles-items/zGMODH6r1X3ZbwB1.htm)|Sluggish|Lent|libre|
|[zni42kvDwzT6lzuW.htm](vehicles-items/zni42kvDwzT6lzuW.htm)|Wind-Up|Remonter un dispositif|libre|
