# État de la traduction (mwangi-expanse-bestiary-items)

 * **libre**: 167
 * **officielle**: 7


Dernière mise à jour: 2023-07-12 20:28 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0lzu6bfck0YarxR5.htm](mwangi-expanse-bestiary-items/0lzu6bfck0YarxR5.htm)|Claws|Griffes|libre|
|[1miPsRSIcintuJOE.htm](mwangi-expanse-bestiary-items/1miPsRSIcintuJOE.htm)|Crooning Cry|Plainte chantonnée|libre|
|[1ViB3OaOGYOfKB3P.htm](mwangi-expanse-bestiary-items/1ViB3OaOGYOfKB3P.htm)|Humanoid Form (At Will)|Forme humanoïde (À volonté)|libre|
|[1wSANkYbvhM6Z2R2.htm](mwangi-expanse-bestiary-items/1wSANkYbvhM6Z2R2.htm)|Thrown Weapon Mastery|Maîtrise des armes de jet|libre|
|[2IlYOWVCX2TfmEcN.htm](mwangi-expanse-bestiary-items/2IlYOWVCX2TfmEcN.htm)|Jaws|Mâchoire|libre|
|[2oevhXnu1QOT55BQ.htm](mwangi-expanse-bestiary-items/2oevhXnu1QOT55BQ.htm)|Face Eater|Mangeur de visage|libre|
|[2rc7rftQQ1MLECXG.htm](mwangi-expanse-bestiary-items/2rc7rftQQ1MLECXG.htm)|Swallow Whole|Gober|libre|
|[3LlKTmYQnV77vz49.htm](mwangi-expanse-bestiary-items/3LlKTmYQnV77vz49.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[460N6IY9mz7Q5PMC.htm](mwangi-expanse-bestiary-items/460N6IY9mz7Q5PMC.htm)|Shrieking Frenzy|Frénésie hurlante|libre|
|[4Wf1PZGckVeEph24.htm](mwangi-expanse-bestiary-items/4Wf1PZGckVeEph24.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[5ijBHOJkiKaEC6s2.htm](mwangi-expanse-bestiary-items/5ijBHOJkiKaEC6s2.htm)|Camouflage|Camouflage|libre|
|[5ptRIaj3SWZbrDDO.htm](mwangi-expanse-bestiary-items/5ptRIaj3SWZbrDDO.htm)|Weak Feet|Pied faible|libre|
|[6C2GqTT5fDsCyi7g.htm](mwangi-expanse-bestiary-items/6C2GqTT5fDsCyi7g.htm)|Jaws|Mâchoire|libre|
|[6mxWk9oM7RspRKLc.htm](mwangi-expanse-bestiary-items/6mxWk9oM7RspRKLc.htm)|Swipe|Frappe transversale|libre|
|[6SMASrmqJ3H77L12.htm](mwangi-expanse-bestiary-items/6SMASrmqJ3H77L12.htm)|Low-Light Vision|vision nocturne|libre|
|[8C3dwNbdW7Ck8njn.htm](mwangi-expanse-bestiary-items/8C3dwNbdW7Ck8njn.htm)|Greater Constrict|Constriction supérieure|libre|
|[8cKp6lKT5oL6dOn4.htm](mwangi-expanse-bestiary-items/8cKp6lKT5oL6dOn4.htm)|Ibis Dive|Plongeon de l'ibis|libre|
|[8COse1lqtTma8FJd.htm](mwangi-expanse-bestiary-items/8COse1lqtTma8FJd.htm)|Grab|Empoignade/Agrippement|libre|
|[8jzHYf5Au02zQV5j.htm](mwangi-expanse-bestiary-items/8jzHYf5Au02zQV5j.htm)|Improved Grab|Empoignade améliorée|officielle|
|[8wqRNOuFAhkTKXFf.htm](mwangi-expanse-bestiary-items/8wqRNOuFAhkTKXFf.htm)|Athletics|Athlétisme|libre|
|[9G5eprAsQmphgOR0.htm](mwangi-expanse-bestiary-items/9G5eprAsQmphgOR0.htm)|Jaws|Mâchoire|libre|
|[9QEsguV2PCRAPv5K.htm](mwangi-expanse-bestiary-items/9QEsguV2PCRAPv5K.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[A93SkvxMG74AEH1p.htm](mwangi-expanse-bestiary-items/A93SkvxMG74AEH1p.htm)|Aquatic Ambush|Embuscade aquatique|libre|
|[AAqmK3Yvg7ChGpRl.htm](mwangi-expanse-bestiary-items/AAqmK3Yvg7ChGpRl.htm)|Limited Vision|Vision limitée|libre|
|[ABJJKoc4oEcun3Z7.htm](mwangi-expanse-bestiary-items/ABJJKoc4oEcun3Z7.htm)|Scent|Odorat|libre|
|[aCFzrVFHI5hMNIww.htm](mwangi-expanse-bestiary-items/aCFzrVFHI5hMNIww.htm)|Sneak Attack|Attaque sournoise|libre|
|[Aiel3i6qUCQzsBGi.htm](mwangi-expanse-bestiary-items/Aiel3i6qUCQzsBGi.htm)|Thrown Debris|Projection de débris|officielle|
|[aj4svCLsqg6Kl49n.htm](mwangi-expanse-bestiary-items/aj4svCLsqg6Kl49n.htm)|Skulking Attack|Attaque à la dérobée|libre|
|[AlkaR0WqgJ5YLhLD.htm](mwangi-expanse-bestiary-items/AlkaR0WqgJ5YLhLD.htm)|Low-Light Vision|vision nocturne|libre|
|[aMonCZ2HbKmRMGa4.htm](mwangi-expanse-bestiary-items/aMonCZ2HbKmRMGa4.htm)|Cold Iron Jaws|Mâchoire en fer froid|libre|
|[B4L5rrixuMmIF0Mn.htm](mwangi-expanse-bestiary-items/B4L5rrixuMmIF0Mn.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[B5JZXq3ugCwVNY5I.htm](mwangi-expanse-bestiary-items/B5JZXq3ugCwVNY5I.htm)|Jaws|Mâchoire|libre|
|[BibTvmqNq8t4WsxM.htm](mwangi-expanse-bestiary-items/BibTvmqNq8t4WsxM.htm)|Zinba Restoration|Restauration zinba|libre|
|[bNWcouqx6SH8uAxF.htm](mwangi-expanse-bestiary-items/bNWcouqx6SH8uAxF.htm)|Woodland Stride|Déplacement facilité en forêt|libre|
|[boazYiEU4yxCfnQA.htm](mwangi-expanse-bestiary-items/boazYiEU4yxCfnQA.htm)|Grab|Empoignade/Agrippement|libre|
|[bXgRimCzt5v6iBye.htm](mwangi-expanse-bestiary-items/bXgRimCzt5v6iBye.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[C733S6U2bTn8YXmm.htm](mwangi-expanse-bestiary-items/C733S6U2bTn8YXmm.htm)|Mauler|Écharpeur|libre|
|[CdQIfzRILrt7WwdR.htm](mwangi-expanse-bestiary-items/CdQIfzRILrt7WwdR.htm)|Crafting|Artisanat|libre|
|[cey14Luo1DtXz0rN.htm](mwangi-expanse-bestiary-items/cey14Luo1DtXz0rN.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[CHSPbGGcFYWzrspR.htm](mwangi-expanse-bestiary-items/CHSPbGGcFYWzrspR.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[CJSHuwoLNcLj6iWJ.htm](mwangi-expanse-bestiary-items/CJSHuwoLNcLj6iWJ.htm)|Defensive Coil|Anneaux défensifs|libre|
|[cv6PTXyDI2kYqY3l.htm](mwangi-expanse-bestiary-items/cv6PTXyDI2kYqY3l.htm)|Attack of Opportunity (Tail Only)|Attaque d'opportunité (Queue seulement)|libre|
|[d4Is7XIdxwsvrjo1.htm](mwangi-expanse-bestiary-items/d4Is7XIdxwsvrjo1.htm)|Ancestral Response|Réponse ancestrale|libre|
|[daC4rWyDfi9sD0eo.htm](mwangi-expanse-bestiary-items/daC4rWyDfi9sD0eo.htm)|Jaws|Mâchoire|libre|
|[dO9YQ3ljXqPCGvzD.htm](mwangi-expanse-bestiary-items/dO9YQ3ljXqPCGvzD.htm)|Low-Light Vision|vision nocturne|libre|
|[dPhm6gvjQdG41ksM.htm](mwangi-expanse-bestiary-items/dPhm6gvjQdG41ksM.htm)|Pass Without Trace (Constant)|Passage sans trace (constant)|libre|
|[E7FKwcsNXjtCJHHE.htm](mwangi-expanse-bestiary-items/E7FKwcsNXjtCJHHE.htm)|Stealth|Discrétion|libre|
|[efUGr9NzEDDkHLgh.htm](mwangi-expanse-bestiary-items/efUGr9NzEDDkHLgh.htm)|Mocking Cry|Cri railleur|libre|
|[EqiCFPbBB1kojE3q.htm](mwangi-expanse-bestiary-items/EqiCFPbBB1kojE3q.htm)|Swallow Whole|Gober|libre|
|[eyepymQeLB62rP72.htm](mwangi-expanse-bestiary-items/eyepymQeLB62rP72.htm)|Arcane Innate Spells|Sorts arcaniques innés|libre|
|[EzPMWivQB9EDUszK.htm](mwangi-expanse-bestiary-items/EzPMWivQB9EDUszK.htm)|Ferocity|Férocité|libre|
|[f3pNQPzw0uoQMQm2.htm](mwangi-expanse-bestiary-items/f3pNQPzw0uoQMQm2.htm)|Tail|Queue|libre|
|[F7g6T6U8LEDRZe2J.htm](mwangi-expanse-bestiary-items/F7g6T6U8LEDRZe2J.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[fGO6kbxeB5SJHFpV.htm](mwangi-expanse-bestiary-items/fGO6kbxeB5SJHFpV.htm)|Blood Fury|Furie du sang|libre|
|[FH4V8Xw43HQPNctv.htm](mwangi-expanse-bestiary-items/FH4V8Xw43HQPNctv.htm)|Burrowing Concealment|Dissimulation par enfouissement|libre|
|[fkAJ3wwmp65qoaGo.htm](mwangi-expanse-bestiary-items/fkAJ3wwmp65qoaGo.htm)|River Form|Forme de rivière|libre|
|[fWUP4jCRrm7VotFD.htm](mwangi-expanse-bestiary-items/fWUP4jCRrm7VotFD.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[fxQVtzxLK32xSoAF.htm](mwangi-expanse-bestiary-items/fxQVtzxLK32xSoAF.htm)|Change Shape|Changement de forme|libre|
|[FY9hR3cHES1bWBcN.htm](mwangi-expanse-bestiary-items/FY9hR3cHES1bWBcN.htm)|Survival|Survie|libre|
|[gBLSXznhLW43fqlS.htm](mwangi-expanse-bestiary-items/gBLSXznhLW43fqlS.htm)|Spin Silk|Filer la soie|libre|
|[GCzfafuOop1VOwxg.htm](mwangi-expanse-bestiary-items/GCzfafuOop1VOwxg.htm)|Counterspelling Wall|Mur contresort|libre|
|[gETps6KlWUO9BOyL.htm](mwangi-expanse-bestiary-items/gETps6KlWUO9BOyL.htm)|Change Shape|Changement de forme|libre|
|[gKhylYG9Kf6mCVUE.htm](mwangi-expanse-bestiary-items/gKhylYG9Kf6mCVUE.htm)|Fangs|Crocs|libre|
|[GqepSGZGV3cXv8qs.htm](mwangi-expanse-bestiary-items/GqepSGZGV3cXv8qs.htm)|Scent (Imprecise) 60 feet|Odorat 18 m (imprécis)|libre|
|[GYJ6GHWDxvwvbLki.htm](mwangi-expanse-bestiary-items/GYJ6GHWDxvwvbLki.htm)|Sound Imitation|Imitation du son|libre|
|[H1PGojvxkIsvrHqR.htm](mwangi-expanse-bestiary-items/H1PGojvxkIsvrHqR.htm)|Grave Blight|Fléau des tombes|libre|
|[hcR44yY3gDKDQ0pt.htm](mwangi-expanse-bestiary-items/hcR44yY3gDKDQ0pt.htm)|Stealth|Discrétion|libre|
|[hhn6WCOoaWnJvopl.htm](mwangi-expanse-bestiary-items/hhn6WCOoaWnJvopl.htm)|Jaws|Mâchoire|libre|
|[Hqdc4UqMK8WH8lpj.htm](mwangi-expanse-bestiary-items/Hqdc4UqMK8WH8lpj.htm)|Obsidian Sickle|Faucille d'obsidienne|libre|
|[HRSNGQ9lKufLFrou.htm](mwangi-expanse-bestiary-items/HRSNGQ9lKufLFrou.htm)|At-Will Spells|Sorts à volonté|libre|
|[HS2K7FagxIukdgY0.htm](mwangi-expanse-bestiary-items/HS2K7FagxIukdgY0.htm)|Jaws|Mâchoire|libre|
|[Ias9P7r19TNysvO6.htm](mwangi-expanse-bestiary-items/Ias9P7r19TNysvO6.htm)|Swallow Whole|Gober|libre|
|[IFr3MHYmCrmlI0NY.htm](mwangi-expanse-bestiary-items/IFr3MHYmCrmlI0NY.htm)|Scent (Imprecise) 60 feet|Odorat 18 m (imprécis)|libre|
|[igxLTKJFu59021Eh.htm](mwangi-expanse-bestiary-items/igxLTKJFu59021Eh.htm)|Tail|Queue|libre|
|[IHVmUejGVzUcWtIG.htm](mwangi-expanse-bestiary-items/IHVmUejGVzUcWtIG.htm)|Claws|Griffes|libre|
|[INMmtWy9d8wwwQE9.htm](mwangi-expanse-bestiary-items/INMmtWy9d8wwwQE9.htm)|Divine Prepared Spells|Sorts divins préparés|libre|
|[IpvCxCkYJzPHJu0M.htm](mwangi-expanse-bestiary-items/IpvCxCkYJzPHJu0M.htm)|Luminous Lure|Leurre lumineux|libre|
|[ivuFT4BbZ4OnFdby.htm](mwangi-expanse-bestiary-items/ivuFT4BbZ4OnFdby.htm)|Constant Spells|Sorts constants|libre|
|[J32aRTXCa9FxIRIH.htm](mwangi-expanse-bestiary-items/J32aRTXCa9FxIRIH.htm)|Swallow Whole|Gober|libre|
|[jAQWygxVNJjiKzIK.htm](mwangi-expanse-bestiary-items/jAQWygxVNJjiKzIK.htm)|Deep Breath|Inspiration profonde|libre|
|[JbwMg2BBvURlzLdX.htm](mwangi-expanse-bestiary-items/JbwMg2BBvURlzLdX.htm)|Jaws|Mâchoire|libre|
|[jCsNuRVEQDzl0gcm.htm](mwangi-expanse-bestiary-items/jCsNuRVEQDzl0gcm.htm)|Dagger|+1|Dague +1|libre|
|[jK6rvCpQD7OADNTT.htm](mwangi-expanse-bestiary-items/jK6rvCpQD7OADNTT.htm)|Scent (Imprecise) 60 feet|Odorat 18 m (imprécis)|libre|
|[JmuzVgN41rKZWAEP.htm](mwangi-expanse-bestiary-items/JmuzVgN41rKZWAEP.htm)|Collar of Fire|Collier de feu|libre|
|[KPDIlbvK82mxxlkp.htm](mwangi-expanse-bestiary-items/KPDIlbvK82mxxlkp.htm)|Change Shape|Changement de forme|libre|
|[kPGbuie2boUJmwwM.htm](mwangi-expanse-bestiary-items/kPGbuie2boUJmwwM.htm)|Jaws|Mâchoire|libre|
|[kPYpreLipQjheZzj.htm](mwangi-expanse-bestiary-items/kPYpreLipQjheZzj.htm)|Deep Breath|Inspiration profonde|libre|
|[kS8WrIHbcUzgQpba.htm](mwangi-expanse-bestiary-items/kS8WrIHbcUzgQpba.htm)|Scent (Imprecise) 60 feet|Odorat 18 m (imprécis)|libre|
|[KSytifn6WnHjPWok.htm](mwangi-expanse-bestiary-items/KSytifn6WnHjPWok.htm)|Constant Spells|Sorts constants|libre|
|[kvJNya3qy945t3jp.htm](mwangi-expanse-bestiary-items/kvJNya3qy945t3jp.htm)|Change Shape|Changement de forme|libre|
|[LGocfA4bhbnXFwea.htm](mwangi-expanse-bestiary-items/LGocfA4bhbnXFwea.htm)|Drink Blood|Boire le sang|libre|
|[lNpwy4Rk5IKOxvvy.htm](mwangi-expanse-bestiary-items/lNpwy4Rk5IKOxvvy.htm)|Thrown Debris|Projection de débris|officielle|
|[LpZh0be8ST6pqnTF.htm](mwangi-expanse-bestiary-items/LpZh0be8ST6pqnTF.htm)|Horn|Corne|libre|
|[lzXJgjHA96IOH3zF.htm](mwangi-expanse-bestiary-items/lzXJgjHA96IOH3zF.htm)|Capsize|Chavirer|libre|
|[M8t48VvaJtVJOBDB.htm](mwangi-expanse-bestiary-items/M8t48VvaJtVJOBDB.htm)|Trident|+1,striking|Trident de frappe +1|libre|
|[mBWCH69hMveboorN.htm](mwangi-expanse-bestiary-items/mBWCH69hMveboorN.htm)|Burrowed Ambush|Embuscade enfouie|libre|
|[my79TeHUaRiixNX5.htm](mwangi-expanse-bestiary-items/my79TeHUaRiixNX5.htm)|Grab|Empoignade/Agrippement|libre|
|[n0nhkIEQ7tkTGxMA.htm](mwangi-expanse-bestiary-items/n0nhkIEQ7tkTGxMA.htm)|Grab|Empoignade/Agrippement|libre|
|[nGSh87HW3Cnx9gsR.htm](mwangi-expanse-bestiary-items/nGSh87HW3Cnx9gsR.htm)|Blowgun Darts (Poisoned)|Fléchettes de sarbacane empoisonnées|libre|
|[NhEfmIq6aHHuARfT.htm](mwangi-expanse-bestiary-items/NhEfmIq6aHHuARfT.htm)|Spin Silk|Filer la soie|libre|
|[NvzyHxVeGqojn5EK.htm](mwangi-expanse-bestiary-items/NvzyHxVeGqojn5EK.htm)|Foot|Patte|libre|
|[NWjkzN3dNcZGRrg7.htm](mwangi-expanse-bestiary-items/NWjkzN3dNcZGRrg7.htm)|Beak|Bec|libre|
|[NWWUgiN78Gu0F9Wh.htm](mwangi-expanse-bestiary-items/NWWUgiN78Gu0F9Wh.htm)|Grab|Empoignade/Agrippement|libre|
|[NYKLnAalhc45FLgf.htm](mwangi-expanse-bestiary-items/NYKLnAalhc45FLgf.htm)|Thunderous Slam|Plaquage tonitruant|libre|
|[ODrCaowNXEHvNgn5.htm](mwangi-expanse-bestiary-items/ODrCaowNXEHvNgn5.htm)|Rage|Rage|libre|
|[OdUVWVv4ABHnNbsY.htm](mwangi-expanse-bestiary-items/OdUVWVv4ABHnNbsY.htm)|Fangs|Crocs|libre|
|[ou1yl6f4qh1BkRPI.htm](mwangi-expanse-bestiary-items/ou1yl6f4qh1BkRPI.htm)|Anadi Venom|Venin anadi|libre|
|[Ow5XxjlhLQfaOHU4.htm](mwangi-expanse-bestiary-items/Ow5XxjlhLQfaOHU4.htm)|Attack from Above|Attaque du dessus|libre|
|[paoFQRpD7yc48lgn.htm](mwangi-expanse-bestiary-items/paoFQRpD7yc48lgn.htm)|Inspired Feast|Festin inspirant|libre|
|[PBICMyxMv3Mp6NsM.htm](mwangi-expanse-bestiary-items/PBICMyxMv3Mp6NsM.htm)|Evasion|Évasion|libre|
|[Pcwj9EhvjUW2YzwX.htm](mwangi-expanse-bestiary-items/Pcwj9EhvjUW2YzwX.htm)|Zinba Venom|Venin zinba|libre|
|[Pdcp2VD4aE0psFww.htm](mwangi-expanse-bestiary-items/Pdcp2VD4aE0psFww.htm)|Tighten Coils|Anneaux resserrés|libre|
|[pdnQW8Ct65O3JJGY.htm](mwangi-expanse-bestiary-items/pdnQW8Ct65O3JJGY.htm)|Anadi Venom|Venin anadi|libre|
|[PDSM3FTG7MzWZh0s.htm](mwangi-expanse-bestiary-items/PDSM3FTG7MzWZh0s.htm)|Low-Light Vision|vision nocturne|libre|
|[PUxSdqWVxgPtP44y.htm](mwangi-expanse-bestiary-items/PUxSdqWVxgPtP44y.htm)|Innate Primal Spells|Sorts primordiaux innés|libre|
|[PwKGsTUERVGvk8dr.htm](mwangi-expanse-bestiary-items/PwKGsTUERVGvk8dr.htm)|Light Shatter|Fracassement de lumière|libre|
|[q43Zr9WI9C73JqZq.htm](mwangi-expanse-bestiary-items/q43Zr9WI9C73JqZq.htm)|Arcane Innate Spells|Sorts arcaniques innés|libre|
|[q9tVR5E7zTPYlogb.htm](mwangi-expanse-bestiary-items/q9tVR5E7zTPYlogb.htm)|Crafting|Artisanat|libre|
|[qB2p1JMiuWWRGdTA.htm](mwangi-expanse-bestiary-items/qB2p1JMiuWWRGdTA.htm)|Obsidian Sickle|Faucille en obsidienne|libre|
|[qCRYzxalKMvt2eOZ.htm](mwangi-expanse-bestiary-items/qCRYzxalKMvt2eOZ.htm)|Claw|Griffe|libre|
|[qg6EYfhHr4O83rA9.htm](mwangi-expanse-bestiary-items/qg6EYfhHr4O83rA9.htm)|Blinding Halo|Halo aveuglant|libre|
|[QiiW6xq0JsWBKt2T.htm](mwangi-expanse-bestiary-items/QiiW6xq0JsWBKt2T.htm)|Crafting|Artisanat|libre|
|[Qm04XnmcECPmSr9k.htm](mwangi-expanse-bestiary-items/Qm04XnmcECPmSr9k.htm)|Size Alteration|Altération de taille|libre|
|[qqYEHc1PVdtmltg3.htm](mwangi-expanse-bestiary-items/qqYEHc1PVdtmltg3.htm)|Gilded Fascination|Fascination dorée|libre|
|[QSi2DdjTxOroNGWQ.htm](mwangi-expanse-bestiary-items/QSi2DdjTxOroNGWQ.htm)|Jaws|Mâchoire|libre|
|[R3AElnqkYLYhjTYs.htm](mwangi-expanse-bestiary-items/R3AElnqkYLYhjTYs.htm)|Low-Light Vision|vision nocturne|libre|
|[r4lGLHORRp0YpsFz.htm](mwangi-expanse-bestiary-items/r4lGLHORRp0YpsFz.htm)|Low-Light Vision|vision nocturne|libre|
|[RCJltuaZMaBLOV1s.htm](mwangi-expanse-bestiary-items/RCJltuaZMaBLOV1s.htm)|Fangs|Crocs|libre|
|[rDWiy412rhlSZyK8.htm](mwangi-expanse-bestiary-items/rDWiy412rhlSZyK8.htm)|Spirit Charge|Charge spirituelle|libre|
|[RI9tmh42fk5iU0FW.htm](mwangi-expanse-bestiary-items/RI9tmh42fk5iU0FW.htm)|Innate Primal Spells|Sorts primordiaux innés|libre|
|[sCqDHZfsQ6odPwEq.htm](mwangi-expanse-bestiary-items/sCqDHZfsQ6odPwEq.htm)|Aquatic Elusion|Esquive aquatique|libre|
|[sGDr2h4N12CW3LTU.htm](mwangi-expanse-bestiary-items/sGDr2h4N12CW3LTU.htm)|Thrown Weapon Mastery|Maîtrise des armes de jet|libre|
|[SlxxoCfG3DRyP0Ay.htm](mwangi-expanse-bestiary-items/SlxxoCfG3DRyP0Ay.htm)|Tail|Queue|libre|
|[SUcSXRslpjPkGsMT.htm](mwangi-expanse-bestiary-items/SUcSXRslpjPkGsMT.htm)|Grab|Empoignade/Agrippement|libre|
|[SW82ptRgUxB88231.htm](mwangi-expanse-bestiary-items/SW82ptRgUxB88231.htm)|Shrieking Frenzy|Frénésie hurlante|libre|
|[SwU7bw7xCeBBV1Qf.htm](mwangi-expanse-bestiary-items/SwU7bw7xCeBBV1Qf.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[t6HmHg3zon3O9J7I.htm](mwangi-expanse-bestiary-items/t6HmHg3zon3O9J7I.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[tFpAHyH73IXBqT5F.htm](mwangi-expanse-bestiary-items/tFpAHyH73IXBqT5F.htm)|Pounce|Bond|libre|
|[tJTwWiHQordmLTR4.htm](mwangi-expanse-bestiary-items/tJTwWiHQordmLTR4.htm)|Jaws|Mâchoire|libre|
|[tL03vFbxhAvCc4lU.htm](mwangi-expanse-bestiary-items/tL03vFbxhAvCc4lU.htm)|Rejuvenation|Reconstruction|libre|
|[tOyviMr7BziJqKBH.htm](mwangi-expanse-bestiary-items/tOyviMr7BziJqKBH.htm)|Talons|Serres|libre|
|[tYIw3txzXdYOMvO4.htm](mwangi-expanse-bestiary-items/tYIw3txzXdYOMvO4.htm)|Half Steps|Demi pas|libre|
|[tyMNSMyHuAJTj3oz.htm](mwangi-expanse-bestiary-items/tyMNSMyHuAJTj3oz.htm)|Foot|Patte|libre|
|[u74JAOvwMJtUTYIo.htm](mwangi-expanse-bestiary-items/u74JAOvwMJtUTYIo.htm)|Inspired Feast|Festin inspirant|libre|
|[uDVantmuNb4lHPSt.htm](mwangi-expanse-bestiary-items/uDVantmuNb4lHPSt.htm)|Beak|Bec|libre|
|[uFV9awWQUUTLzpt8.htm](mwangi-expanse-bestiary-items/uFV9awWQUUTLzpt8.htm)|Half-Speaker|Demi-parleur|libre|
|[uHRi8PEkDBnKHzkZ.htm](mwangi-expanse-bestiary-items/uHRi8PEkDBnKHzkZ.htm)|Athletics|Athlétisme|libre|
|[Uioct4jDb676vZCb.htm](mwangi-expanse-bestiary-items/Uioct4jDb676vZCb.htm)|Shrieking Frenzy|Frénésie hurlante|libre|
|[V1vRcDQ2IFRycIBj.htm](mwangi-expanse-bestiary-items/V1vRcDQ2IFRycIBj.htm)|Athletics|Athlétisme|libre|
|[vLKTy929m3lP4HBw.htm](mwangi-expanse-bestiary-items/vLKTy929m3lP4HBw.htm)|Tusk|Défense|libre|
|[VMcwUvR4wlGflpeU.htm](mwangi-expanse-bestiary-items/VMcwUvR4wlGflpeU.htm)|Greater Constrict|Constriction supérieure|libre|
|[vvXU4PalVWLPjnDg.htm](mwangi-expanse-bestiary-items/vvXU4PalVWLPjnDg.htm)|Spin Silk|Filer la soie|libre|
|[VxWzZiuP7fPGDfrw.htm](mwangi-expanse-bestiary-items/VxWzZiuP7fPGDfrw.htm)|Spirit Steps|Pas spirituel|libre|
|[Vxy96kPKk88gz9Qe.htm](mwangi-expanse-bestiary-items/Vxy96kPKk88gz9Qe.htm)|Slither|reptation|libre|
|[vZ5bxxJbi92Cs7s3.htm](mwangi-expanse-bestiary-items/vZ5bxxJbi92Cs7s3.htm)|Freedom of Movement (Constant)|Liberté de mouvement (constant)|libre|
|[Wape36Br9FSgTS2H.htm](mwangi-expanse-bestiary-items/Wape36Br9FSgTS2H.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[Wh9drGchO0qrVs4U.htm](mwangi-expanse-bestiary-items/Wh9drGchO0qrVs4U.htm)|Talon|Serre|libre|
|[wWsB5I4nHqsR3fBF.htm](mwangi-expanse-bestiary-items/wWsB5I4nHqsR3fBF.htm)|Improved Grab|Empoignade améliorée|officielle|
|[X52vV6cMtvVUfWAc.htm](mwangi-expanse-bestiary-items/X52vV6cMtvVUfWAc.htm)|Thrown Weapon Mastery|Maîtrise des armes de jet|libre|
|[xBis9ZRZVsgcF0Ns.htm](mwangi-expanse-bestiary-items/xBis9ZRZVsgcF0Ns.htm)|Rompo Chills|Frissons du rompo|libre|
|[XDCJ3PGz2z4zo3Fo.htm](mwangi-expanse-bestiary-items/XDCJ3PGz2z4zo3Fo.htm)|Inspired Feast|Festin inspirant|libre|
|[XhoGkdNs2eBgo5bM.htm](mwangi-expanse-bestiary-items/XhoGkdNs2eBgo5bM.htm)|Anadi Venom|Venin anadi|libre|
|[xi4Es6L6Nk3JiPqx.htm](mwangi-expanse-bestiary-items/xi4Es6L6Nk3JiPqx.htm)|Grab|Empoignade/Agrippement|libre|
|[xuwxzfEavsTyRQX2.htm](mwangi-expanse-bestiary-items/xuwxzfEavsTyRQX2.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[xwXKqYeS8wn01sOG.htm](mwangi-expanse-bestiary-items/xwXKqYeS8wn01sOG.htm)|Improved Grab|Empoignade améliorée|officielle|
|[Y6s9VOKh7c0ctUPT.htm](mwangi-expanse-bestiary-items/Y6s9VOKh7c0ctUPT.htm)|Push 10 feet|Pousser 3 m|libre|
|[Y8WPnqV6kzFd4ZsF.htm](mwangi-expanse-bestiary-items/Y8WPnqV6kzFd4ZsF.htm)|Thrown Debris|Projection de débris|officielle|
|[YA3ZwqBMAXgpNToc.htm](mwangi-expanse-bestiary-items/YA3ZwqBMAXgpNToc.htm)|Stealth|Discrétion|libre|
|[yYNlY1iySgLh2vUa.htm](mwangi-expanse-bestiary-items/yYNlY1iySgLh2vUa.htm)|Low-Light Vision|vision nocturne|libre|
|[zIIyPgt8WGCYilod.htm](mwangi-expanse-bestiary-items/zIIyPgt8WGCYilod.htm)|Jaws|Mâchoire|libre|
|[zK0CLdcqjHeldCOi.htm](mwangi-expanse-bestiary-items/zK0CLdcqjHeldCOi.htm)|Claw|Griffe|libre|
|[zltKri0RdONGiYzp.htm](mwangi-expanse-bestiary-items/zltKri0RdONGiYzp.htm)|Trample|Piétinement|libre|
|[ZS2Z05Guj9JmlrVd.htm](mwangi-expanse-bestiary-items/ZS2Z05Guj9JmlrVd.htm)|Shield Block|Blocage au bouclier|officielle|
|[zWEFdMq1oFwMEbBD.htm](mwangi-expanse-bestiary-items/zWEFdMq1oFwMEbBD.htm)|True Seeing (Constant)|Vision lucide (constant)|libre|
