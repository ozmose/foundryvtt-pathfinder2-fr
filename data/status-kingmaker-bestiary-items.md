# État de la traduction (kingmaker-bestiary-items)

 * **officielle**: 65
 * **libre**: 1110
 * **changé**: 5
 * **aucune**: 1


Dernière mise à jour: 2023-07-12 20:28 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[rYnxMGhSK5kn5X7J.htm](kingmaker-bestiary-items/rYnxMGhSK5kn5X7J.htm)|Spear|+1|

## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[2prGxYGRkpCNqmdI.htm](kingmaker-bestiary-items/2prGxYGRkpCNqmdI.htm)|Shelyn's Thanks|Remerciements de Shélyn|changé|
|[7SmLN0kqsTk5kyLK.htm](kingmaker-bestiary-items/7SmLN0kqsTk5kyLK.htm)|Jaws|Cimeterre en cristal|changé|
|[BKsKbanuSDh8osp9.htm](kingmaker-bestiary-items/BKsKbanuSDh8osp9.htm)|Hope or Despair|Espoir ou désespoir|changé|
|[fYhoTq6AnWKAwKBM.htm](kingmaker-bestiary-items/fYhoTq6AnWKAwKBM.htm)|Release the Inmost Worm|Relâcher le Ver Intérieur|changé|
|[R7WS5l7jScWRr53y.htm](kingmaker-bestiary-items/R7WS5l7jScWRr53y.htm)|+2 Resilient Hide Armor|Armure en peau résiliente +1|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0bBCzMxj06H1x2EI.htm](kingmaker-bestiary-items/0bBCzMxj06H1x2EI.htm)|Composite Longbow|+1,striking|Arc long composite de frappe +1|officielle|
|[0bbJhjr4XkyXm2cv.htm](kingmaker-bestiary-items/0bbJhjr4XkyXm2cv.htm)|Mistform Elixir (Greater) (Infused)|Élixir de brumeforme modéré (impregné)|libre|
|[0bQTICfElBoz3XPf.htm](kingmaker-bestiary-items/0bQTICfElBoz3XPf.htm)|+1 Breastplate|Cuirasse +1|libre|
|[0gCnA8mF12gNsipi.htm](kingmaker-bestiary-items/0gCnA8mF12gNsipi.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[0ivwHldip6F8T0gA.htm](kingmaker-bestiary-items/0ivwHldip6F8T0gA.htm)|Attack of Opportunity (Fangs Only)|Attaque d'opportunité (Crocs)|libre|
|[0mCmhGYAI47n4kdy.htm](kingmaker-bestiary-items/0mCmhGYAI47n4kdy.htm)|Divine Prepared Spells|Sorts divins préparés|libre|
|[0NDexqBHwunzi360.htm](kingmaker-bestiary-items/0NDexqBHwunzi360.htm)|Dagger|+2,greaterStriking|Dague de frappe supérieure +2|libre|
|[0pHw83Qkw39rMcIx.htm](kingmaker-bestiary-items/0pHw83Qkw39rMcIx.htm)|Contingency|Contingence|libre|
|[0qWcKzKBUEwvLvdH.htm](kingmaker-bestiary-items/0qWcKzKBUEwvLvdH.htm)|Pipes|Flûte|libre|
|[0Rjcnl6ynsH5LKjO.htm](kingmaker-bestiary-items/0Rjcnl6ynsH5LKjO.htm)|Warfare Lore|Connaissance de la guerre|libre|
|[0rmDD9ymGXaLDR7A.htm](kingmaker-bestiary-items/0rmDD9ymGXaLDR7A.htm)|Longsword|+1|Épée longue +1|libre|
|[0rYILxhM4TiZQaeA.htm](kingmaker-bestiary-items/0rYILxhM4TiZQaeA.htm)|Sudden Charge|Charge soudaine|libre|
|[0ShB5n7PzRHdtWVF.htm](kingmaker-bestiary-items/0ShB5n7PzRHdtWVF.htm)|Primal Call (Creatures with the Wild Hunt Trait Only)|Appel primordial (créature avec le trait de la Chasse sauvage uniquement)|libre|
|[0sXAwqL8Tb6XqJ0H.htm](kingmaker-bestiary-items/0sXAwqL8Tb6XqJ0H.htm)|Lantern King's Glow|Lueur du Roi Lanterne|libre|
|[0TyxIsxCmJlWFTk6.htm](kingmaker-bestiary-items/0TyxIsxCmJlWFTk6.htm)|Evasion|Évasion|libre|
|[0UqqnUjTvhlc7sGv.htm](kingmaker-bestiary-items/0UqqnUjTvhlc7sGv.htm)|Bardic Lore|Connaissance bardique|libre|
|[0VYJH6vuFSZm9NIX.htm](kingmaker-bestiary-items/0VYJH6vuFSZm9NIX.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[0WYsG1PdrVoJN2Ue.htm](kingmaker-bestiary-items/0WYsG1PdrVoJN2Ue.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[137OFGe9srUCXPpN.htm](kingmaker-bestiary-items/137OFGe9srUCXPpN.htm)|Closing Volley|Salve d'approche|libre|
|[1bKTp47s4lhPF0Ix.htm](kingmaker-bestiary-items/1bKTp47s4lhPF0Ix.htm)|Darkvision|Vision dans le noir|libre|
|[1bZpLXmtzfGcJX8S.htm](kingmaker-bestiary-items/1bZpLXmtzfGcJX8S.htm)|+2 Resilient Studded Leather Armor|Armure en cuir clouté de résilience +2|officielle|
|[1CVzuimA1fYyvCBq.htm](kingmaker-bestiary-items/1CVzuimA1fYyvCBq.htm)|Cruel Anatomist|Anatomiste cruel|libre|
|[1fxLVAyfm2PZ7Zxg.htm](kingmaker-bestiary-items/1fxLVAyfm2PZ7Zxg.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[1Ht8zfzmHL4tULd5.htm](kingmaker-bestiary-items/1Ht8zfzmHL4tULd5.htm)|Shield Rip and Chew|Arrache bouclier|libre|
|[1jsdM4qpD0smQsMV.htm](kingmaker-bestiary-items/1jsdM4qpD0smQsMV.htm)|Low-Light Vision|Vision nocturne|libre|
|[1kX9bpy0yIk83pGw.htm](kingmaker-bestiary-items/1kX9bpy0yIk83pGw.htm)|Rejuvenation|Reconstruction|libre|
|[1lrxLnSpTkBwcSli.htm](kingmaker-bestiary-items/1lrxLnSpTkBwcSli.htm)|Ghostly Battle Axe|Hache d'arme fantomatique|libre|
|[1Lvngo9gteYE4ep9.htm](kingmaker-bestiary-items/1Lvngo9gteYE4ep9.htm)|Dagger|Dague|libre|
|[1PdKOeeXtOPcFYCb.htm](kingmaker-bestiary-items/1PdKOeeXtOPcFYCb.htm)|Composite Longbow|Arc long composite|libre|
|[1V9pafG73kkZDmNM.htm](kingmaker-bestiary-items/1V9pafG73kkZDmNM.htm)|Negative Healing|Guérison négative|libre|
|[1VH2ArAHvIhMi57j.htm](kingmaker-bestiary-items/1VH2ArAHvIhMi57j.htm)|Fist|Poing|libre|
|[1Y7PAPhO3aVpgKoq.htm](kingmaker-bestiary-items/1Y7PAPhO3aVpgKoq.htm)|Ghostly Rapier|Rapière fantomatique|libre|
|[22UiypLQhIBbjWDe.htm](kingmaker-bestiary-items/22UiypLQhIBbjWDe.htm)|Religious Text of Saranrae|Texte religieux de Saranraé|libre|
|[28VxmNDvpa40sH4P.htm](kingmaker-bestiary-items/28VxmNDvpa40sH4P.htm)|Divine Prepared Spells|Sorts divins préparés|libre|
|[2A5WqL14A7eKaElj.htm](kingmaker-bestiary-items/2A5WqL14A7eKaElj.htm)|Claw|Griffe|libre|
|[2c38HSvjdnaJgeI2.htm](kingmaker-bestiary-items/2c38HSvjdnaJgeI2.htm)|Awesome Blow|Coup fabuleux|libre|
|[2cPfluVOMi5nDGgx.htm](kingmaker-bestiary-items/2cPfluVOMi5nDGgx.htm)|Trident|+1,striking|Trident de frappe +1|libre|
|[2I9ZCEqUKoC0AAqQ.htm](kingmaker-bestiary-items/2I9ZCEqUKoC0AAqQ.htm)|At-Will Spells|Sorts à volonté|libre|
|[2IqWrJj9edOVS3Gp.htm](kingmaker-bestiary-items/2IqWrJj9edOVS3Gp.htm)|Triple Opportunity|Triple opportunité|libre|
|[2jUfYxc6d6U4w3Z7.htm](kingmaker-bestiary-items/2jUfYxc6d6U4w3Z7.htm)|Dimension Door (At Will) (To any location in Armag's Tomb)|Porte dimensionnelle (à volonté, n'importe quel lieu dans la tombe d'Armarg)|libre|
|[2n6nRtgDNRWONgvp.htm](kingmaker-bestiary-items/2n6nRtgDNRWONgvp.htm)|Drain Luck|Drain de chance|libre|
|[2pEzXozrOXVdVg0A.htm](kingmaker-bestiary-items/2pEzXozrOXVdVg0A.htm)|Life Bloom|Éclosion de vie|libre|
|[2wByf3NmPbP0YRzy.htm](kingmaker-bestiary-items/2wByf3NmPbP0YRzy.htm)|Leng Ruby|Rubis de Leng|libre|
|[2xC1UooYAPVhcemO.htm](kingmaker-bestiary-items/2xC1UooYAPVhcemO.htm)|Religious Symbol of Gogunta (Silver)|Symbole religieux en argent de Gogunta|libre|
|[2YaktCYgfrnvuUbE.htm](kingmaker-bestiary-items/2YaktCYgfrnvuUbE.htm)|Slash Throat|Entailler la gorge|libre|
|[312Fug37MtFpvG0P.htm](kingmaker-bestiary-items/312Fug37MtFpvG0P.htm)|Tongue|Langue|libre|
|[38JeDzTzMbmPrydc.htm](kingmaker-bestiary-items/38JeDzTzMbmPrydc.htm)|Survival|Survie|libre|
|[3AidCm7XJT968Swi.htm](kingmaker-bestiary-items/3AidCm7XJT968Swi.htm)|Choke|Étouffer|libre|
|[3ANSkECTu3y2Lt3C.htm](kingmaker-bestiary-items/3ANSkECTu3y2Lt3C.htm)|Dogslicer|Tranchechien|libre|
|[3G3uDUo55k6PR6pz.htm](kingmaker-bestiary-items/3G3uDUo55k6PR6pz.htm)|Sheath|Fourreau|libre|
|[3gmJPQIKZq7dcelo.htm](kingmaker-bestiary-items/3gmJPQIKZq7dcelo.htm)|Composite Shortbow|Arc court composite|libre|
|[3hPEjn6DWOXtuQ2I.htm](kingmaker-bestiary-items/3hPEjn6DWOXtuQ2I.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[3Oyvp6SK64TaOXVq.htm](kingmaker-bestiary-items/3Oyvp6SK64TaOXVq.htm)|Bastard Sword|+2,striking,wounding|Épée bâtarde sanglante de frappe +1|libre|
|[3pzII2xLdduTjJrG.htm](kingmaker-bestiary-items/3pzII2xLdduTjJrG.htm)|Quickened Casting|Incantation accélérée|libre|
|[3VX47OPvg5PAjzsD.htm](kingmaker-bestiary-items/3VX47OPvg5PAjzsD.htm)|Primal Spontaneous Spells|Sorts primordiaux innés|libre|
|[3WcqrLbeRK1eZWeJ.htm](kingmaker-bestiary-items/3WcqrLbeRK1eZWeJ.htm)|+1 Leather Armor|Armure en cuir +1|officielle|
|[3Ygn2F1yeN3UTO2S.htm](kingmaker-bestiary-items/3Ygn2F1yeN3UTO2S.htm)|Pain|Douleur|libre|
|[3ZL7oyw6PKNMzEhf.htm](kingmaker-bestiary-items/3ZL7oyw6PKNMzEhf.htm)|Grant Desire|Accorder un voeu|libre|
|[432AQ0jbevEtWKdG.htm](kingmaker-bestiary-items/432AQ0jbevEtWKdG.htm)|Formula Book|Livre de formules|officielle|
|[45aANCYgNwQS8Ega.htm](kingmaker-bestiary-items/45aANCYgNwQS8Ega.htm)|Hooktongue Venom|Venin de langue-crochet|libre|
|[48zaDpaZFzUKLJHL.htm](kingmaker-bestiary-items/48zaDpaZFzUKLJHL.htm)|Sudden Charge|Charge soudaine|libre|
|[4bbZ6NaZSeW5sE1f.htm](kingmaker-bestiary-items/4bbZ6NaZSeW5sE1f.htm)|Trident|Trident|libre|
|[4C0eLAnhYCsQz9KO.htm](kingmaker-bestiary-items/4C0eLAnhYCsQz9KO.htm)|+4 Status to All Saves vs. Mental|bonus de statut de +4 aux JdS contre les effets mentaux|libre|
|[4EsbaXbLbmfGNLmn.htm](kingmaker-bestiary-items/4EsbaXbLbmfGNLmn.htm)|Constant Spells|Sorts constants|libre|
|[4hjXOaTYgTu9yoad.htm](kingmaker-bestiary-items/4hjXOaTYgTu9yoad.htm)|Claws|Griffes|libre|
|[4KoYrmcXEdMKXapg.htm](kingmaker-bestiary-items/4KoYrmcXEdMKXapg.htm)|Academia Lore|Connaissance universitaire|libre|
|[4mK32Hq1EHPgHPW4.htm](kingmaker-bestiary-items/4mK32Hq1EHPgHPW4.htm)|Dread Striker|Attaquant d'effroi|libre|
|[4S2A9APP31jlMK4Q.htm](kingmaker-bestiary-items/4S2A9APP31jlMK4Q.htm)|Arcane Spontaneous Spells|Sorts arcaniques spontanés|libre|
|[4SgwfAqj7PXlo4NI.htm](kingmaker-bestiary-items/4SgwfAqj7PXlo4NI.htm)|Kob's Ruinous Strike|Frappe désastre de Kob|libre|
|[4uhmqqCnN4EAqpVy.htm](kingmaker-bestiary-items/4uhmqqCnN4EAqpVy.htm)|Rock|Rocher|libre|
|[4WaSsGcstf79Y44R.htm](kingmaker-bestiary-items/4WaSsGcstf79Y44R.htm)|Jaws|Mâchoires|libre|
|[4WbCPkQrjR5xvIUs.htm](kingmaker-bestiary-items/4WbCPkQrjR5xvIUs.htm)|Temporal Strikes|Frappe de temporalité|libre|
|[4z1JD9bhf1R5wpmX.htm](kingmaker-bestiary-items/4z1JD9bhf1R5wpmX.htm)|Trident|Trident|libre|
|[4zR81aivcsDVQxAp.htm](kingmaker-bestiary-items/4zR81aivcsDVQxAp.htm)|Whip|+1,striking|Fouet de frappe +1|officielle|
|[57nrcKoJBLBDALst.htm](kingmaker-bestiary-items/57nrcKoJBLBDALst.htm)|Living Bow|Arc vivant|libre|
|[5AVIS7Cfz12DClSG.htm](kingmaker-bestiary-items/5AVIS7Cfz12DClSG.htm)|Darkvision|Vision dans le noir|libre|
|[5c6243RybWvondkY.htm](kingmaker-bestiary-items/5c6243RybWvondkY.htm)|Silver Stag Lord Amulet|Amulette du Seigneur Cerf en argent|libre|
|[5dKTN8eobwXG2iGV.htm](kingmaker-bestiary-items/5dKTN8eobwXG2iGV.htm)|Arcane Spontaneous Spells|Sorts arcaniques spontanés|libre|
|[5dvPmX8OUqLml3m6.htm](kingmaker-bestiary-items/5dvPmX8OUqLml3m6.htm)|Claw|Griffe|libre|
|[5EExyaDy8xArOFh0.htm](kingmaker-bestiary-items/5EExyaDy8xArOFh0.htm)|Fast Healing 10|Guérison accélérée 10|libre|
|[5G6Es9pOfgRiXtpy.htm](kingmaker-bestiary-items/5G6Es9pOfgRiXtpy.htm)|Power Attack|Attaque en puissance|libre|
|[5juLpMpruoLCJ9JZ.htm](kingmaker-bestiary-items/5juLpMpruoLCJ9JZ.htm)|Hatchet|+2,striking|Hachette de frappe +2|libre|
|[5LclMp6q589Sl0Mn.htm](kingmaker-bestiary-items/5LclMp6q589Sl0Mn.htm)|Claw|Griffes|libre|
|[5LgL3UMxFZRmIgXy.htm](kingmaker-bestiary-items/5LgL3UMxFZRmIgXy.htm)|Vulture Beak|Bec de vautour|libre|
|[5m1MoZTkyC5yL2jL.htm](kingmaker-bestiary-items/5m1MoZTkyC5yL2jL.htm)|Double Strike|Double frappe|libre|
|[5mJxaR7sQGjBWkRc.htm](kingmaker-bestiary-items/5mJxaR7sQGjBWkRc.htm)|+2 Resilient Leather Armor|Armure en cuir de résilience +2|officielle|
|[5MTbqNxF7xLIRqQY.htm](kingmaker-bestiary-items/5MTbqNxF7xLIRqQY.htm)|Abaddon Lore|Connaissance d'Abbadon|libre|
|[5nbfijONNe4T5HFM.htm](kingmaker-bestiary-items/5nbfijONNe4T5HFM.htm)|Draining Touch|Toucher affaiblissant|libre|
|[5O8YD9thn6myrfWy.htm](kingmaker-bestiary-items/5O8YD9thn6myrfWy.htm)|Pollen Burst|Explosion de pollen|libre|
|[5Tlw4BDzoIJ4STyR.htm](kingmaker-bestiary-items/5Tlw4BDzoIJ4STyR.htm)|Battered Breastplate|Cuirasse cabossée|libre|
|[5uF0UI8Ihj8qhfy5.htm](kingmaker-bestiary-items/5uF0UI8Ihj8qhfy5.htm)|Support Benefit|Avantage de soutien|libre|
|[5uI2L7wMgW34OuXg.htm](kingmaker-bestiary-items/5uI2L7wMgW34OuXg.htm)|Vengeful Rage|Rage vengeresse|libre|
|[5UTWzAXIsjMqThHw.htm](kingmaker-bestiary-items/5UTWzAXIsjMqThHw.htm)|Unfair Aim|Visée injuste|libre|
|[5w3fouC3WVuTBtWK.htm](kingmaker-bestiary-items/5w3fouC3WVuTBtWK.htm)|Trample|Piétinement|libre|
|[5xktm2PrBdKEX5oh.htm](kingmaker-bestiary-items/5xktm2PrBdKEX5oh.htm)|Dagger|Dague|libre|
|[5yj5ekSognH3S3RW.htm](kingmaker-bestiary-items/5yj5ekSognH3S3RW.htm)|Silver Stag Lord Amulet|Amulette du Seigneur Cerf en argent|libre|
|[5z29EkkGtyaSoB1a.htm](kingmaker-bestiary-items/5z29EkkGtyaSoB1a.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[61i4ipb2qXp6RshD.htm](kingmaker-bestiary-items/61i4ipb2qXp6RshD.htm)|Lumber Lore|Connaissance bûcheron|libre|
|[61pWFfMaO4glQNlI.htm](kingmaker-bestiary-items/61pWFfMaO4glQNlI.htm)|Fetch Weapon|Arme de fetch|libre|
|[64snLNQ9P2c8IffF.htm](kingmaker-bestiary-items/64snLNQ9P2c8IffF.htm)|Guild Lore|Connaissance des guildes|libre|
|[66WdJU8fGY3RHFZN.htm](kingmaker-bestiary-items/66WdJU8fGY3RHFZN.htm)|Blinded|Aveuglé|libre|
|[67Qch1f76P2Ze8KM.htm](kingmaker-bestiary-items/67Qch1f76P2Ze8KM.htm)|Religious Symbol of Gyronna (Silver)|Symbole religieux en argent de Gyronna|libre|
|[6aSDaFMEcz58NAqP.htm](kingmaker-bestiary-items/6aSDaFMEcz58NAqP.htm)|Despairing Breath|Souffle de désespoir|libre|
|[6cprEKOmbMfF47Lu.htm](kingmaker-bestiary-items/6cprEKOmbMfF47Lu.htm)|Shame|Honte|libre|
|[6e66VeO6FBdw8Fjn.htm](kingmaker-bestiary-items/6e66VeO6FBdw8Fjn.htm)|Hatchet|Hachette|libre|
|[6gDHZ6VPfZDrKCnp.htm](kingmaker-bestiary-items/6gDHZ6VPfZDrKCnp.htm)|Sneak Attack|Attaque sournoise|libre|
|[6ILB5ny6782Ujs7o.htm](kingmaker-bestiary-items/6ILB5ny6782Ujs7o.htm)|Change Shape|Changement de forme|libre|
|[6jXk3sASWaQvOH5n.htm](kingmaker-bestiary-items/6jXk3sASWaQvOH5n.htm)|Dagger|+1|Dague +1|officielle|
|[6k6LtJmrZAfTImSh.htm](kingmaker-bestiary-items/6k6LtJmrZAfTImSh.htm)|Staff|Bâton|libre|
|[6k7jBeAiFp27Wcv1.htm](kingmaker-bestiary-items/6k7jBeAiFp27Wcv1.htm)|Bard Composition Spells|Sorts de composition de barde|libre|
|[6kvbcAoDPilYf0fI.htm](kingmaker-bestiary-items/6kvbcAoDPilYf0fI.htm)|At-Will Spells|Sorts à volonté|libre|
|[6mO0FIwfW82Cszej.htm](kingmaker-bestiary-items/6mO0FIwfW82Cszej.htm)|Curse of the Weretiger|Malédiction du tigre-garou|libre|
|[6MwtMijY3R7DbxaJ.htm](kingmaker-bestiary-items/6MwtMijY3R7DbxaJ.htm)|Ghostly Attack|Attaque fantomatique|libre|
|[6ofr6YtODR30jRp0.htm](kingmaker-bestiary-items/6ofr6YtODR30jRp0.htm)|Lice-Infested Furs|Fourrure infestée de poux|libre|
|[6qAaC77CfOknbaMv.htm](kingmaker-bestiary-items/6qAaC77CfOknbaMv.htm)|Quick Recovery|Récupération rapide|libre|
|[6qhXb73HUuWsXVlw.htm](kingmaker-bestiary-items/6qhXb73HUuWsXVlw.htm)|Aldori Riposte|Riposte aldorie|libre|
|[6QtfXfEtO9Jk9Ceg.htm](kingmaker-bestiary-items/6QtfXfEtO9Jk9Ceg.htm)|Greensight|Vision dans la nature|libre|
|[6SNxPBIEy6G6pJ5S.htm](kingmaker-bestiary-items/6SNxPBIEy6G6pJ5S.htm)|Fist|Poing|libre|
|[6TCoYo76CHcPURpz.htm](kingmaker-bestiary-items/6TCoYo76CHcPURpz.htm)|Resilient Splint Mail|Clibanion de résilience|libre|
|[6XfWpghskVUmJSVG.htm](kingmaker-bestiary-items/6XfWpghskVUmJSVG.htm)|Aldori Dueling Sword|Épée de duel Aldori|libre|
|[6xM67gan9MlmS1dN.htm](kingmaker-bestiary-items/6xM67gan9MlmS1dN.htm)|Tilt and Roll|Baculer et tanguer|libre|
|[6YFC1LK4zBkiKg1N.htm](kingmaker-bestiary-items/6YFC1LK4zBkiKg1N.htm)|Beyond the Barrier|Au-delà de la barrière|libre|
|[6znbpXWxevbdvTl2.htm](kingmaker-bestiary-items/6znbpXWxevbdvTl2.htm)|Rip and Rend|Déchirer et éventrer|libre|
|[70wPP2Jajb7NN04d.htm](kingmaker-bestiary-items/70wPP2Jajb7NN04d.htm)|Change Shape|Changement de forme|libre|
|[73DVUYU49Oqo474k.htm](kingmaker-bestiary-items/73DVUYU49Oqo474k.htm)|Nymph's Tragedy|Tragédie de nymphe|libre|
|[73zfyyrBBuzM9Dh9.htm](kingmaker-bestiary-items/73zfyyrBBuzM9Dh9.htm)|Terrifying Croak|Croassement terrifiant|libre|
|[76TiztNcrVVUhZQl.htm](kingmaker-bestiary-items/76TiztNcrVVUhZQl.htm)|Speak with Animals (Constant)|Communication avec les animaux (constant)|officielle|
|[77XHZv9nEQwJTaiO.htm](kingmaker-bestiary-items/77XHZv9nEQwJTaiO.htm)|Drain Oculus|Drain de l'Oculus|libre|
|[79pJPNMXOyazfHy6.htm](kingmaker-bestiary-items/79pJPNMXOyazfHy6.htm)|Accelerated Existence|Existence accélérée|libre|
|[7bd1h5tOi35bvK7Q.htm](kingmaker-bestiary-items/7bd1h5tOi35bvK7Q.htm)|Wand of Shocking Grasp (Level 2)|Baguette de Décharge électrique (Niveau 2)|libre|
|[7ERyflHHinGfODO2.htm](kingmaker-bestiary-items/7ERyflHHinGfODO2.htm)|Primal Prepared Spells|Sorts primordiaux préparés|libre|
|[7ff6eVjIgZUezVKm.htm](kingmaker-bestiary-items/7ff6eVjIgZUezVKm.htm)|Jabberwock Bloodline|Lignage de jabberwock|libre|
|[7fPsSiHuHlM11fhF.htm](kingmaker-bestiary-items/7fPsSiHuHlM11fhF.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|libre|
|[7fVgYQK2ZSqvG5KV.htm](kingmaker-bestiary-items/7fVgYQK2ZSqvG5KV.htm)|Twin Feint|Feinte jumelée|libre|
|[7fwg0KKHJSx50lBW.htm](kingmaker-bestiary-items/7fwg0KKHJSx50lBW.htm)|Battlefield Command|Commandement de bataille|libre|
|[7iMYJiq8cs6BSSdN.htm](kingmaker-bestiary-items/7iMYJiq8cs6BSSdN.htm)|Nature's Edge|Avantage naturel|libre|
|[7j0otQgc8z5zzALv.htm](kingmaker-bestiary-items/7j0otQgc8z5zzALv.htm)|Site Bound|Lié à un site|libre|
|[7Lzy2YrsBlDo3O23.htm](kingmaker-bestiary-items/7Lzy2YrsBlDo3O23.htm)|Regeneration 25 (Deactivated by Acid or Fire)|Régénération 24 (désactivé par acide ou feu)|libre|
|[7MElwF26Lym91Wch.htm](kingmaker-bestiary-items/7MElwF26Lym91Wch.htm)|Longsword|Épée bâtarde|libre|
|[7N8Klhs5wpgJ6RvM.htm](kingmaker-bestiary-items/7N8Klhs5wpgJ6RvM.htm)|Hunting Horn|Cor de chasse|libre|
|[7N9zMdYcPW2J4Hzt.htm](kingmaker-bestiary-items/7N9zMdYcPW2J4Hzt.htm)|Nyrissa's Favor|Faveur de Nyrissa|libre|
|[7O2rhLBXoMyayaTV.htm](kingmaker-bestiary-items/7O2rhLBXoMyayaTV.htm)|Distracting Yipping|Jappements gênants|libre|
|[7omOmpvwPwKMmBfB.htm](kingmaker-bestiary-items/7omOmpvwPwKMmBfB.htm)|Intimidating Strike|Frappe intimidante|libre|
|[7pIVt1wT5IWY1dOP.htm](kingmaker-bestiary-items/7pIVt1wT5IWY1dOP.htm)|Flurry of Blows|Déluge de coups|libre|
|[7PNhfO9yF8jlxc97.htm](kingmaker-bestiary-items/7PNhfO9yF8jlxc97.htm)|Rapier|+1|Rapière +1|libre|
|[7QpzxU7FsF8lN6FP.htm](kingmaker-bestiary-items/7QpzxU7FsF8lN6FP.htm)|Shameful Memories|Souvenirs honteux|libre|
|[7s2ezouHGaqFCuhW.htm](kingmaker-bestiary-items/7s2ezouHGaqFCuhW.htm)|Corrupting Gaze|Regard corrupteur|libre|
|[7SDmYIdMAER2dLGK.htm](kingmaker-bestiary-items/7SDmYIdMAER2dLGK.htm)|Invoke Stisshak|Invoquer Stisshak|libre|
|[7TT0BqubK5ErNyN3.htm](kingmaker-bestiary-items/7TT0BqubK5ErNyN3.htm)|Ranseur|Corsèque|libre|
|[7tW8esj9IVdwLqwS.htm](kingmaker-bestiary-items/7tW8esj9IVdwLqwS.htm)|Primal Weapon|Arme primordiale|libre|
|[7Y8YE3i8NiZAOPkt.htm](kingmaker-bestiary-items/7Y8YE3i8NiZAOPkt.htm)|Collapse|Effondrement du plancher|libre|
|[7ZhJ9zRnsiGY2LMq.htm](kingmaker-bestiary-items/7ZhJ9zRnsiGY2LMq.htm)|Change Shape|Changement de forme|libre|
|[7zyckBxX0Qo3fPo5.htm](kingmaker-bestiary-items/7zyckBxX0Qo3fPo5.htm)|Chain of Lovelies|Chaîne de choses jolies|libre|
|[83BVCoUzow3GIr5W.htm](kingmaker-bestiary-items/83BVCoUzow3GIr5W.htm)|Claw|Griffe|libre|
|[873UBJDHNOwZqAn0.htm](kingmaker-bestiary-items/873UBJDHNOwZqAn0.htm)|Longsword|Épée longue|libre|
|[88hwkEhv8cx338VM.htm](kingmaker-bestiary-items/88hwkEhv8cx338VM.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[88uOslPj6NpTUQNF.htm](kingmaker-bestiary-items/88uOslPj6NpTUQNF.htm)|Shortsword|+1,striking|Épée courte de frappe +1|officielle|
|[89ANJyvglhWZiKZv.htm](kingmaker-bestiary-items/89ANJyvglhWZiKZv.htm)|Deploy Flytraps|Déploiement d'attrape-mouches|libre|
|[8FamBadz3fY1Ktf1.htm](kingmaker-bestiary-items/8FamBadz3fY1Ktf1.htm)|Javelin|Javeline|libre|
|[8fXr0ygvqmKMJQ7y.htm](kingmaker-bestiary-items/8fXr0ygvqmKMJQ7y.htm)|Ambush Strike|Frappe d'embuscade|libre|
|[8GzTttsR8tPUpy3a.htm](kingmaker-bestiary-items/8GzTttsR8tPUpy3a.htm)|Manifest Fetch Weapon|Manifestation d'arme fetch|libre|
|[8I1oL3XnWQjb1WeF.htm](kingmaker-bestiary-items/8I1oL3XnWQjb1WeF.htm)|Torture Lore|Connaissance de la torture|libre|
|[8Ib2hxgIftGdnnC4.htm](kingmaker-bestiary-items/8Ib2hxgIftGdnnC4.htm)|Occult Focus Spells|Sorts occultes focalisés|libre|
|[8k6w6YpDwJuyU7Lj.htm](kingmaker-bestiary-items/8k6w6YpDwJuyU7Lj.htm)|Twin Feint|Feinte jumelée|libre|
|[8lbGHlEEQstmS0rd.htm](kingmaker-bestiary-items/8lbGHlEEQstmS0rd.htm)|Club|Gourdin|libre|
|[8s4nFCFF2c0eNDnN.htm](kingmaker-bestiary-items/8s4nFCFF2c0eNDnN.htm)|Divine Prepared Spells|Sorts divins préparés|libre|
|[8Ss1d21OHToUfFNl.htm](kingmaker-bestiary-items/8Ss1d21OHToUfFNl.htm)|Yog-Sothoth Lore|Connaissance de Yog-Sothoth|libre|
|[8T1NpFqpuMXKzB51.htm](kingmaker-bestiary-items/8T1NpFqpuMXKzB51.htm)|Jaws|Mâchoire|libre|
|[8wo9GPXQVySZtBb5.htm](kingmaker-bestiary-items/8wo9GPXQVySZtBb5.htm)|Arcane Prepared Spells|Sorts arcaniques préparés|libre|
|[8x5ve1XWJQ7yIA7F.htm](kingmaker-bestiary-items/8x5ve1XWJQ7yIA7F.htm)|Attack of Opportunity|Attaque d'opporunité|libre|
|[8zIAec8NlrwPEbab.htm](kingmaker-bestiary-items/8zIAec8NlrwPEbab.htm)|Trident|Trident|libre|
|[907AYCZJGYIzNC2a.htm](kingmaker-bestiary-items/907AYCZJGYIzNC2a.htm)|Quickened Casting|Incantation accélérée|libre|
|[95EZclhH2zhKgoxm.htm](kingmaker-bestiary-items/95EZclhH2zhKgoxm.htm)|Dagger|Dague|libre|
|[97i0qo04YJEOlBSn.htm](kingmaker-bestiary-items/97i0qo04YJEOlBSn.htm)|Rare Candles and Incense|Bougies et encens rare|libre|
|[9a3n2YfibJlQxctv.htm](kingmaker-bestiary-items/9a3n2YfibJlQxctv.htm)|Unseen Sight|Vision invisible|libre|
|[9AVnb90miimzc7rS.htm](kingmaker-bestiary-items/9AVnb90miimzc7rS.htm)|Sneak Attack|Attaque sournoise|libre|
|[9F78hYb3XMI7z4ai.htm](kingmaker-bestiary-items/9F78hYb3XMI7z4ai.htm)|Rapier|Rapière|libre|
|[9h5jUpLhqi52Tedi.htm](kingmaker-bestiary-items/9h5jUpLhqi52Tedi.htm)|Brevoy Lore|Connaissance du Brévoy|libre|
|[9jMNfcA0u8pECJhS.htm](kingmaker-bestiary-items/9jMNfcA0u8pECJhS.htm)|Dagger|Dague|libre|
|[9JQwWcFOpgrzmEIy.htm](kingmaker-bestiary-items/9JQwWcFOpgrzmEIy.htm)|Dagger|Dague|libre|
|[9khUkSjPZRFIilUZ.htm](kingmaker-bestiary-items/9khUkSjPZRFIilUZ.htm)|Rend|Éventration|libre|
|[9kVpK6J5EVHZcp0K.htm](kingmaker-bestiary-items/9kVpK6J5EVHZcp0K.htm)|Major Staff of Transmutation|Bâton de transmutation majeur|libre|
|[9ODQkquyZvS7PFNK.htm](kingmaker-bestiary-items/9ODQkquyZvS7PFNK.htm)|Knockdown|Renversement|libre|
|[9PUgsYyJWDJ9J0sE.htm](kingmaker-bestiary-items/9PUgsYyJWDJ9J0sE.htm)|+1 Status vs. Posion|bonus de statut de +1 contre le poison|libre|
|[9qzHAK95CCa74dtM.htm](kingmaker-bestiary-items/9qzHAK95CCa74dtM.htm)|Absorb the Bloom|Absorber la Fleur|libre|
|[9rDjTECEOqQb11SQ.htm](kingmaker-bestiary-items/9rDjTECEOqQb11SQ.htm)|Dagger|Dague|libre|
|[9RNGJOLNS21hY10V.htm](kingmaker-bestiary-items/9RNGJOLNS21hY10V.htm)|Falchion|+1,striking|Cimeterre à deux mains de frappe +1|libre|
|[9ST6BDxKH8vXvpEq.htm](kingmaker-bestiary-items/9ST6BDxKH8vXvpEq.htm)|Attack of Opportunity (Worm Jaws Only)|Attaque d'opportunité (mâchoire de ver uniquement)|libre|
|[9TeqIJRQyjU0d2eg.htm](kingmaker-bestiary-items/9TeqIJRQyjU0d2eg.htm)|+2 Greater Fire Resistant Greater Resilient Leather Armor|Armure en cuir de résistance supérieure au feu et de résilience supérieure +2|libre|
|[9uNCXDfw6yzI9yL5.htm](kingmaker-bestiary-items/9uNCXDfw6yzI9yL5.htm)|Release Spell|Décharge du sort|libre|
|[9VA9Qjcx5YJxs4P1.htm](kingmaker-bestiary-items/9VA9Qjcx5YJxs4P1.htm)|Jangle the Chain|Claquement de chaîne|libre|
|[9VgJpOQ6xJ9gTgCU.htm](kingmaker-bestiary-items/9VgJpOQ6xJ9gTgCU.htm)|Bard Spontaneous Spells|Sorts de barde spontanés|libre|
|[9xLtvFQAoJh8tSOd.htm](kingmaker-bestiary-items/9xLtvFQAoJh8tSOd.htm)|Staff of Power|+2,greaterStriking|Bâton de surpuissance de frappe supérieure +2|libre|
|[9XpL2lVavNQF91Zx.htm](kingmaker-bestiary-items/9XpL2lVavNQF91Zx.htm)|True Seeing (Constant)|Vision lucide (constant)|officielle|
|[a4xj4dNHkUjHRH4O.htm](kingmaker-bestiary-items/a4xj4dNHkUjHRH4O.htm)|Silver Ring|Anneau d'argent|libre|
|[a7WtcnRg6JlsVIDg.htm](kingmaker-bestiary-items/a7WtcnRg6JlsVIDg.htm)|Maul|Maillet|libre|
|[A8rxcOUACAmsVmTH.htm](kingmaker-bestiary-items/A8rxcOUACAmsVmTH.htm)|Command the Undead|Commander les mort-vivants|libre|
|[A9Hfkb1IhZNWZW7U.htm](kingmaker-bestiary-items/A9Hfkb1IhZNWZW7U.htm)|Divine Prepared Spells|Sorts divins préparés|libre|
|[AaMjBE4xQc5ZqroJ.htm](kingmaker-bestiary-items/AaMjBE4xQc5ZqroJ.htm)|Sneak Attack|Attaque sournoise|libre|
|[aan6tOAkYHhETUdL.htm](kingmaker-bestiary-items/aan6tOAkYHhETUdL.htm)|Deadly Aim|Visée mortelle|libre|
|[AB7mMjajOUtvqlxD.htm](kingmaker-bestiary-items/AB7mMjajOUtvqlxD.htm)|Evasion|Évasion|libre|
|[abr9bhWIMzRT7FHN.htm](kingmaker-bestiary-items/abr9bhWIMzRT7FHN.htm)|Grisly Scythe|+1,striking,wounding|Épouvantable faux sanglante de frappe +1|libre|
|[aDli69WFeRXUP2U3.htm](kingmaker-bestiary-items/aDli69WFeRXUP2U3.htm)|Ceremonial Robes|Robes de cérémonie|libre|
|[Aeu3hLK3x9HeXzhD.htm](kingmaker-bestiary-items/Aeu3hLK3x9HeXzhD.htm)|Darkness (At Will)|Ténèbres (À volonté)|officielle|
|[AfvMfBPmJB8eecwr.htm](kingmaker-bestiary-items/AfvMfBPmJB8eecwr.htm)|Sneak Attack|Attaque sournoise|libre|
|[afWpHJ6qqzsRORUX.htm](kingmaker-bestiary-items/afWpHJ6qqzsRORUX.htm)|Steady Spellcasting|Incantation fiable|libre|
|[AFXLJNAW0dhc0PkH.htm](kingmaker-bestiary-items/AFXLJNAW0dhc0PkH.htm)|Wineskin|Outre de vin|libre|
|[AFz5oCbdGYZQVrDD.htm](kingmaker-bestiary-items/AFz5oCbdGYZQVrDD.htm)|Agonized Wail|Hurlement d'agonie|libre|
|[AfZgCzOcPYkL5eV2.htm](kingmaker-bestiary-items/AfZgCzOcPYkL5eV2.htm)|Kukri|+1|Kukri +1|libre|
|[Ag07kBthiVumPkcV.htm](kingmaker-bestiary-items/Ag07kBthiVumPkcV.htm)|Focus Hatred|Haine focalisée|libre|
|[aGPW7GAY6qDJQHh8.htm](kingmaker-bestiary-items/aGPW7GAY6qDJQHh8.htm)|Ring of Energy Resistance (Major) (Fire)|Anneau de Résistance à l'energie majeur (Feu)|libre|
|[aHuQg1Wi1gb8KAbH.htm](kingmaker-bestiary-items/aHuQg1Wi1gb8KAbH.htm)|Time Siphon|Siphon du temps|libre|
|[ai3N8T91JXo3Hx5e.htm](kingmaker-bestiary-items/ai3N8T91JXo3Hx5e.htm)|Negative Healing|Guérison négative|libre|
|[AIde6VUDBdxDlQQS.htm](kingmaker-bestiary-items/AIde6VUDBdxDlQQS.htm)|Dagger|Dague|libre|
|[AjjeSlBUiJKE8SOB.htm](kingmaker-bestiary-items/AjjeSlBUiJKE8SOB.htm)|Divine Prepared Spells|Sorts divins préparés|libre|
|[AJYRmsDmV1LVU4Rv.htm](kingmaker-bestiary-items/AJYRmsDmV1LVU4Rv.htm)|Silver Stag Lord Amulet|Amulette du Seigneur Cerf en argent|libre|
|[AkE2QdLe1wysN32X.htm](kingmaker-bestiary-items/AkE2QdLe1wysN32X.htm)|Blindness|Aveuglement|libre|
|[AksHJ5xCuwQbdOmq.htm](kingmaker-bestiary-items/AksHJ5xCuwQbdOmq.htm)|Claw|Griffe|libre|
|[aLcLFpXMpgdiDrzO.htm](kingmaker-bestiary-items/aLcLFpXMpgdiDrzO.htm)|Symbiotic Swarm|Nuée symbiotique|libre|
|[aM4rXRFUMH2Ufswh.htm](kingmaker-bestiary-items/aM4rXRFUMH2Ufswh.htm)|Improved Grab|Empoignade/Agrippement améliorés|libre|
|[AMuN6VT4RTloaWQk.htm](kingmaker-bestiary-items/AMuN6VT4RTloaWQk.htm)|Bomber|Artificier|libre|
|[ao5De1dHxQjNJ8za.htm](kingmaker-bestiary-items/ao5De1dHxQjNJ8za.htm)|Souleater|Mangeur d'âme|libre|
|[AoTJ2NwBcO5rItKq.htm](kingmaker-bestiary-items/AoTJ2NwBcO5rItKq.htm)|Darkvision|Vision dans le noir|libre|
|[ap4RPRdJx6CtkxmL.htm](kingmaker-bestiary-items/ap4RPRdJx6CtkxmL.htm)|Ovinrbaane|+2,striking,wounding|Ovinrbaane sanglante de frappe +2|libre|
|[ap7UmDIuWShLoSbJ.htm](kingmaker-bestiary-items/ap7UmDIuWShLoSbJ.htm)|Forest Lore|Connaissance de la forêt|libre|
|[ApHre7Dw1aJJlSiv.htm](kingmaker-bestiary-items/ApHre7Dw1aJJlSiv.htm)|Hatchet|Hachette|libre|
|[apQdVdQWSEbQzCw2.htm](kingmaker-bestiary-items/apQdVdQWSEbQzCw2.htm)|Focus Gaze|Focaliser le regard|libre|
|[Aq3vYXMGDf6pFo4D.htm](kingmaker-bestiary-items/Aq3vYXMGDf6pFo4D.htm)|Plane Shift (to or from the First World only)|Chngement de plan (de ou vers le Premier monde uniquement)|libre|
|[aQsTI9u9o9zMbaHa.htm](kingmaker-bestiary-items/aQsTI9u9o9zMbaHa.htm)|Resolve|Résolution|libre|
|[Asj3JNQgw7B1ZXXC.htm](kingmaker-bestiary-items/Asj3JNQgw7B1ZXXC.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[atdIIgSxVkVrP1Ti.htm](kingmaker-bestiary-items/atdIIgSxVkVrP1Ti.htm)|Druid Order Spells|Sorts de l'Ordre des druides|libre|
|[AtiJbhL8pY43Rvv3.htm](kingmaker-bestiary-items/AtiJbhL8pY43Rvv3.htm)|Smash Kneecaps|Destruction de genoux|libre|
|[auHvPa2qajS0vG6w.htm](kingmaker-bestiary-items/auHvPa2qajS0vG6w.htm)|Primal Spontaneous Spells|Sorts primordiaux spontanés|libre|
|[auvnM9kZNqXdqAIR.htm](kingmaker-bestiary-items/auvnM9kZNqXdqAIR.htm)|Outside of Time|Hors du temps|libre|
|[AvThDzMVhWejEVsL.htm](kingmaker-bestiary-items/AvThDzMVhWejEVsL.htm)|Low-Light Vision|Vision nocturne|libre|
|[aW63u7ufl1uEOBQh.htm](kingmaker-bestiary-items/aW63u7ufl1uEOBQh.htm)|Furious Finish|Final furieux|libre|
|[AwAeIIlDkmCoVoca.htm](kingmaker-bestiary-items/AwAeIIlDkmCoVoca.htm)|Regeneration 20 (Deactivated by Acid or Fire)|Régénération 20 (désactivée par acide ou feu)|libre|
|[AwKlC3yGdtDzjPWM.htm](kingmaker-bestiary-items/AwKlC3yGdtDzjPWM.htm)|Masterful Hunter|Chasseur magistral|libre|
|[AX9lSAPNk2PJCat1.htm](kingmaker-bestiary-items/AX9lSAPNk2PJCat1.htm)|Outside of Time|Hors du temps|libre|
|[axy7Bj9PMjBKlFdd.htm](kingmaker-bestiary-items/axy7Bj9PMjBKlFdd.htm)|Club|Gourdin|libre|
|[ay85G9ub5DX6nf12.htm](kingmaker-bestiary-items/ay85G9ub5DX6nf12.htm)|No Escape|Pas d'échappatoire|libre|
|[AYlNJO8ziqGDNG6F.htm](kingmaker-bestiary-items/AYlNJO8ziqGDNG6F.htm)|Forest Lore|Connaissance de la forêt|libre|
|[Az48BJ0aNRNzQotH.htm](kingmaker-bestiary-items/Az48BJ0aNRNzQotH.htm)|Wounded Arm|Blessé au bras|libre|
|[azD5OELMU31IrNwz.htm](kingmaker-bestiary-items/azD5OELMU31IrNwz.htm)|Dagger|Dague|libre|
|[aZke0WE5mklrrBK7.htm](kingmaker-bestiary-items/aZke0WE5mklrrBK7.htm)|Draining Inhalation|Inhalation drainante|libre|
|[b07B1akTo84FKZUe.htm](kingmaker-bestiary-items/b07B1akTo84FKZUe.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[b2b4en2ylHZagF2u.htm](kingmaker-bestiary-items/b2b4en2ylHZagF2u.htm)|Wand of Burning Hands (Level 1)|Baguette de Mains brûlantes (Niveau 1)|libre|
|[B2sQYrMGR7VibDvV.htm](kingmaker-bestiary-items/B2sQYrMGR7VibDvV.htm)|Dagger|+1,striking,silver|Dague en argent de frappe +1|libre|
|[b5j9aIeq7ERjg1cn.htm](kingmaker-bestiary-items/b5j9aIeq7ERjg1cn.htm)|Religious Symbol of Gozreh (Wooden and Bloodstained)|Symbole religieux en bois et taché de sang de Gozreh|libre|
|[BA3fqrqPyWZ59YSs.htm](kingmaker-bestiary-items/BA3fqrqPyWZ59YSs.htm)|Deadeye's Shame|Honte du vieux borgne|libre|
|[BbhEZOpQbLUnfahO.htm](kingmaker-bestiary-items/BbhEZOpQbLUnfahO.htm)|Ruby Ring|Anneau de rubis|libre|
|[BDyM0mqNVmkzGKe2.htm](kingmaker-bestiary-items/BDyM0mqNVmkzGKe2.htm)|Carpenter's Tools|Outils de menuiserie|libre|
|[bekh1R8LAluGLNaA.htm](kingmaker-bestiary-items/bekh1R8LAluGLNaA.htm)|Low-Light Vision|Vision nocturne|libre|
|[BFpgenVI0pBLyV0S.htm](kingmaker-bestiary-items/BFpgenVI0pBLyV0S.htm)|Infused Items|Objets imprégnés|libre|
|[BGZlipUdtmQIe6By.htm](kingmaker-bestiary-items/BGZlipUdtmQIe6By.htm)|Divine Prepared Spells|Sorts divins préparés|libre|
|[BltNCa3ZEsbxzhf7.htm](kingmaker-bestiary-items/BltNCa3ZEsbxzhf7.htm)|Buck|Désarçonner|libre|
|[bMbUyfSqVR9lWDB7.htm](kingmaker-bestiary-items/bMbUyfSqVR9lWDB7.htm)|Rejuvenation|Reconstruction|libre|
|[BMGyGT2KQinKwWfe.htm](kingmaker-bestiary-items/BMGyGT2KQinKwWfe.htm)|Shortsword|Épée courte|libre|
|[BMzs4OQEu3Mt6iZQ.htm](kingmaker-bestiary-items/BMzs4OQEu3Mt6iZQ.htm)|Gambling Lore|Connaissance des jeux d'argent|libre|
|[BO3hN1F0bkrNu60q.htm](kingmaker-bestiary-items/BO3hN1F0bkrNu60q.htm)|Fast Healing 15|Guérison accélérée 15|libre|
|[bOpAJRLpemL4HwNd.htm](kingmaker-bestiary-items/bOpAJRLpemL4HwNd.htm)|Swamp Stride|Déplacement facilité dans les marais|libre|
|[BOya1lhwnf2RNvGy.htm](kingmaker-bestiary-items/BOya1lhwnf2RNvGy.htm)|Juggernaut|Juggernaut|libre|
|[BqhAFrC5aDQ1BKoT.htm](kingmaker-bestiary-items/BqhAFrC5aDQ1BKoT.htm)|Blowgun|Sarbacane|libre|
|[BtJvNGAGxO2VOuXB.htm](kingmaker-bestiary-items/BtJvNGAGxO2VOuXB.htm)|Mind-Numbing Breath|Souffle brume-esprit|libre|
|[BuBkGYAPCPnJ03go.htm](kingmaker-bestiary-items/BuBkGYAPCPnJ03go.htm)|Claw|Griffe|libre|
|[bvBZPt5ocFCNJflm.htm](kingmaker-bestiary-items/bvBZPt5ocFCNJflm.htm)|Ready and Load|Prêt et chargé|libre|
|[BVk9QB1imJ9KD0Vq.htm](kingmaker-bestiary-items/BVk9QB1imJ9KD0Vq.htm)|Silver Stag Lord Amulet|Amulette du Seigneur Cerf en argent|libre|
|[BvsSj4ZTgepDO6JN.htm](kingmaker-bestiary-items/BvsSj4ZTgepDO6JN.htm)|Battle Axe|+1,striking|Hache d'armes de frappe +1|libre|
|[BXDlaMPWYXlB5Few.htm](kingmaker-bestiary-items/BXDlaMPWYXlB5Few.htm)|+1 Full Plate|Harnois +1|officielle|
|[BxuCFIo6yswNsArJ.htm](kingmaker-bestiary-items/BxuCFIo6yswNsArJ.htm)|Hunting Cry|Hurlement de chasse|libre|
|[bZEeE1MlmmqIJiiK.htm](kingmaker-bestiary-items/bZEeE1MlmmqIJiiK.htm)|Cleric Domain Spells|Sorts de domaine de prêtre|libre|
|[c1sQmMq46Aeg6en6.htm](kingmaker-bestiary-items/c1sQmMq46Aeg6en6.htm)|Axe Specialization|Spécialisation de la hache|libre|
|[c2r3T8G5zXq6UOBS.htm](kingmaker-bestiary-items/c2r3T8G5zXq6UOBS.htm)|Mobility|Mobilité|libre|
|[C31XU8XsrO1R112R.htm](kingmaker-bestiary-items/C31XU8XsrO1R112R.htm)|Focused Target|Cible focalisée|libre|
|[C3EpruyM244f0JVf.htm](kingmaker-bestiary-items/C3EpruyM244f0JVf.htm)|Greatsword|+2,striking|Épée à deux mains de frappe +2|libre|
|[c3NPgUPsH0WkQZXG.htm](kingmaker-bestiary-items/c3NPgUPsH0WkQZXG.htm)|Composite Shortbow|+1,striking|Arc court composite de Frappe +1|libre|
|[c3vIbIBnVpy9fjsE.htm](kingmaker-bestiary-items/c3vIbIBnVpy9fjsE.htm)|Rage|Rage|libre|
|[C4fZA5gB6JeZf08P.htm](kingmaker-bestiary-items/C4fZA5gB6JeZf08P.htm)|Club|Gourdin|libre|
|[C9Ds23BokMiIUMi5.htm](kingmaker-bestiary-items/C9Ds23BokMiIUMi5.htm)|Release Jewel|Libérer Bijou|libre|
|[C9ESEAYQaJUciIba.htm](kingmaker-bestiary-items/C9ESEAYQaJUciIba.htm)|Sneak Attack|Attaque sournoise|libre|
|[cA2cBXkewzB0Fwtj.htm](kingmaker-bestiary-items/cA2cBXkewzB0Fwtj.htm)|Change Shape|Changement de forme|libre|
|[CBKkrJnJ9DcFA6p0.htm](kingmaker-bestiary-items/CBKkrJnJ9DcFA6p0.htm)|Hunting Lore|Connaissance de la chasse|libre|
|[CBuSOJ26ZXTgXAze.htm](kingmaker-bestiary-items/CBuSOJ26ZXTgXAze.htm)|Constant Spells|Sorts constants|libre|
|[CC2IGMZw3wpLt6Av.htm](kingmaker-bestiary-items/CC2IGMZw3wpLt6Av.htm)|Composite Longbow|+1|Arc long composite +1|officielle|
|[cc2QQL18PozIgxVb.htm](kingmaker-bestiary-items/cc2QQL18PozIgxVb.htm)|Wizard School Spells|Sorts de l'école de magicien|libre|
|[CcrqR1iP71tzTA5V.htm](kingmaker-bestiary-items/CcrqR1iP71tzTA5V.htm)|Collapse|Effondrement|libre|
|[cD3KZDeLep4rM0aB.htm](kingmaker-bestiary-items/cD3KZDeLep4rM0aB.htm)|Darkvision|Vision dans le noir|libre|
|[CDUrtwX9v7aZEVjj.htm](kingmaker-bestiary-items/CDUrtwX9v7aZEVjj.htm)|Change Shape|Changement de forme|libre|
|[CeBgXo66gTiufqL5.htm](kingmaker-bestiary-items/CeBgXo66gTiufqL5.htm)|Nature's Edge|Avantage naturel|libre|
|[cFKmkUCecFtQweau.htm](kingmaker-bestiary-items/cFKmkUCecFtQweau.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|libre|
|[cG10UmeLEy9eANOc.htm](kingmaker-bestiary-items/cG10UmeLEy9eANOc.htm)|Acid Flask (Greater) (Infused)|Fiole d'acide supérieure (imprégnée)|officielle|
|[Ch2vwQBo7w7hd8ou.htm](kingmaker-bestiary-items/Ch2vwQBo7w7hd8ou.htm)|Harrying Swordfighter|Épéiste harrassant|libre|
|[cIFXkQ4Qhht73I7F.htm](kingmaker-bestiary-items/cIFXkQ4Qhht73I7F.htm)|Longsword|+1,striking|Épée longue de frappe +1|libre|
|[cjGRYigrrey1TIiS.htm](kingmaker-bestiary-items/cjGRYigrrey1TIiS.htm)|Underworld Lore|Connaissance de la pègre|libre|
|[cJIYg8gvnBbvoiuI.htm](kingmaker-bestiary-items/cJIYg8gvnBbvoiuI.htm)|Mobility|Mobilité|libre|
|[CJravyxL4AFAla1i.htm](kingmaker-bestiary-items/CJravyxL4AFAla1i.htm)|Curse of the Wererat|Malédiction du rat-garou|libre|
|[ckg1JdT2pmJlw8zC.htm](kingmaker-bestiary-items/ckg1JdT2pmJlw8zC.htm)|Dagger|Dague|libre|
|[ckkFk6fhxAXrQHzv.htm](kingmaker-bestiary-items/ckkFk6fhxAXrQHzv.htm)|Composite Shortbow|Arc court composite|libre|
|[CLhR4qg5HhAxRnYA.htm](kingmaker-bestiary-items/CLhR4qg5HhAxRnYA.htm)|Suggestion (At Will)|Suggestion (À volonté)|officielle|
|[cOFKoOU0dk3bhJeX.htm](kingmaker-bestiary-items/cOFKoOU0dk3bhJeX.htm)|Religious Symbol of Calistra (Silver)|Symbole religieux en argent de Calistria|libre|
|[cpFEKCq7CWkJ3qrw.htm](kingmaker-bestiary-items/cpFEKCq7CWkJ3qrw.htm)|Trident|Trident|libre|
|[cQluYUjYjTeHDhTp.htm](kingmaker-bestiary-items/cQluYUjYjTeHDhTp.htm)|Spinning Sweep|Balayage tournoyant|libre|
|[CquGBdxcC31YxWhu.htm](kingmaker-bestiary-items/CquGBdxcC31YxWhu.htm)|Rotting Stench|Puanteur de cadavre|libre|
|[crdKpfEf0xqRNZES.htm](kingmaker-bestiary-items/crdKpfEf0xqRNZES.htm)|Greataxe|+2,striking|Grande hache de frappe +2|libre|
|[CtzLyJ8UMGgt86Xw.htm](kingmaker-bestiary-items/CtzLyJ8UMGgt86Xw.htm)|+2 Greater Resilient Mithral Chain Mail|Chemise de mailles en mithral de résilience supérieure +2|libre|
|[cu5gzpbsORryK2Zs.htm](kingmaker-bestiary-items/cu5gzpbsORryK2Zs.htm)|Athletics|Athlétisme|libre|
|[CX6zUhlF06g9iSjB.htm](kingmaker-bestiary-items/CX6zUhlF06g9iSjB.htm)|Wand of Invisibility (Level 2)|Baguette d'Invisibilité (niveau 2)|libre|
|[d2pAqCh02beVW16z.htm](kingmaker-bestiary-items/d2pAqCh02beVW16z.htm)|Jaws|Mâchoire|libre|
|[D2qG6bBAwV0Xu4iO.htm](kingmaker-bestiary-items/D2qG6bBAwV0Xu4iO.htm)|+1 Full Plate|Harnois +1|officielle|
|[D4f8OzwwMvBLI86y.htm](kingmaker-bestiary-items/D4f8OzwwMvBLI86y.htm)|Cold Iron Shortsword|Épée courte en fer froid|libre|
|[D6qVDUiJjJhA5CIX.htm](kingmaker-bestiary-items/D6qVDUiJjJhA5CIX.htm)|Wild Reincarnation|Réincarnation sauvage|libre|
|[d7qSq3HVLR2AAayU.htm](kingmaker-bestiary-items/d7qSq3HVLR2AAayU.htm)|Low-Light Vision|Vision nocturne|libre|
|[D8DPsCfECCJBxsBs.htm](kingmaker-bestiary-items/D8DPsCfECCJBxsBs.htm)|Speak with Plants (Constant)|Communication avec les plantes (Constant)|libre|
|[d8jlUApm6erhQTpM.htm](kingmaker-bestiary-items/d8jlUApm6erhQTpM.htm)|Whip|+1|Fouet +1|libre|
|[d8wBT2ExQFfDG2Mt.htm](kingmaker-bestiary-items/d8wBT2ExQFfDG2Mt.htm)|Wild Hunt Link|Lien de Chasse sauvage|libre|
|[d9FqMXrNIAm6rnHs.htm](kingmaker-bestiary-items/d9FqMXrNIAm6rnHs.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[dahtG8lHhjQBQemo.htm](kingmaker-bestiary-items/dahtG8lHhjQBQemo.htm)|Hurled Thorn|Épine projetée|libre|
|[dBd9trOqG8wBEYZ6.htm](kingmaker-bestiary-items/dBd9trOqG8wBEYZ6.htm)|+1 Resilient Hide Armor|Armure en peau résiliente +1|libre|
|[DbU5apk8FlJos4xz.htm](kingmaker-bestiary-items/DbU5apk8FlJos4xz.htm)|Critical Debilitating Strike|Frappe incapacitante critique|libre|
|[dCRhF1JVyPGvXOfW.htm](kingmaker-bestiary-items/dCRhF1JVyPGvXOfW.htm)|First World Lore|Connaissance du Premier Monde|libre|
|[DdEls9nTJvA7pEi0.htm](kingmaker-bestiary-items/DdEls9nTJvA7pEi0.htm)|Blowgun|Sarbacane|libre|
|[DeAD5IPwOAtQ1Zjr.htm](kingmaker-bestiary-items/DeAD5IPwOAtQ1Zjr.htm)|Kukri|Kukri|libre|
|[dEj5HxpnzeRCKekU.htm](kingmaker-bestiary-items/dEj5HxpnzeRCKekU.htm)|Wand of Glitterdust (Level 2)|Baguette de Poussière scintillante (Niveau 2)|libre|
|[DFgNddcsAbZVlWSH.htm](kingmaker-bestiary-items/DFgNddcsAbZVlWSH.htm)|Pounce|Bond|libre|
|[dGa7fiQkgtKN8PDA.htm](kingmaker-bestiary-items/dGa7fiQkgtKN8PDA.htm)|Crafting|Artisanat|libre|
|[DgApO6iOQpwx3KfR.htm](kingmaker-bestiary-items/DgApO6iOQpwx3KfR.htm)|Resolve|Résolution|libre|
|[dGglOjtXAyHqoW5X.htm](kingmaker-bestiary-items/dGglOjtXAyHqoW5X.htm)|Steady Spellcasting|Incantation fiable|libre|
|[DGNIDiy16GZYbh8b.htm](kingmaker-bestiary-items/DGNIDiy16GZYbh8b.htm)|Trapdoor Lunge|Fente de trappe|libre|
|[dhHfUP1VQHRPtvdU.htm](kingmaker-bestiary-items/dhHfUP1VQHRPtvdU.htm)|Stealth|Discrétion|libre|
|[dhTLYiCmWDkONUN5.htm](kingmaker-bestiary-items/dhTLYiCmWDkONUN5.htm)|Jaws|Machoires|libre|
|[DiARocJoBMTbmCN9.htm](kingmaker-bestiary-items/DiARocJoBMTbmCN9.htm)|Key to Area E4|Clé à la Zone E4|libre|
|[DJVCW8WVRR0Prvrs.htm](kingmaker-bestiary-items/DJVCW8WVRR0Prvrs.htm)|Prismatic Burst|Explosion prismatique|libre|
|[dJzydVxO1TtPnaho.htm](kingmaker-bestiary-items/dJzydVxO1TtPnaho.htm)|Deny Advantage|Refus d'avantage|libre|
|[DksWsSv1BvpLeNhJ.htm](kingmaker-bestiary-items/DksWsSv1BvpLeNhJ.htm)|Jaws|Mâchoires|libre|
|[DLI2axhKyDzeg5Ub.htm](kingmaker-bestiary-items/DLI2axhKyDzeg5Ub.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[DN5RQpUoYCp3qWou.htm](kingmaker-bestiary-items/DN5RQpUoYCp3qWou.htm)|Dagger|+1|Dague +1|officielle|
|[DNaScDDIg4t6u0Hw.htm](kingmaker-bestiary-items/DNaScDDIg4t6u0Hw.htm)|Ballista Bolt|Projectile de baliste|libre|
|[dnLiUmNpLT4ZnUQ6.htm](kingmaker-bestiary-items/dnLiUmNpLT4ZnUQ6.htm)|Darkvision|Vision dans le noir|libre|
|[dNOthX618UT1hZN6.htm](kingmaker-bestiary-items/dNOthX618UT1hZN6.htm)|Distracting Shot|Tir déroutant|libre|
|[DoCmnJxIOLLTb9Xm.htm](kingmaker-bestiary-items/DoCmnJxIOLLTb9Xm.htm)|Greater Acid Flask|Fiole d'acide supérieure|libre|
|[DOviMcCQeyu6rmjx.htm](kingmaker-bestiary-items/DOviMcCQeyu6rmjx.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[DrW6hlIWaGCF1pU4.htm](kingmaker-bestiary-items/DrW6hlIWaGCF1pU4.htm)|Juggernaut|Juggernaut|libre|
|[dsVVZHTsFi9iFwgD.htm](kingmaker-bestiary-items/dsVVZHTsFi9iFwgD.htm)|Hill Lore|Connaissance des collines|libre|
|[dtTve1TvHU0al6Es.htm](kingmaker-bestiary-items/dtTve1TvHU0al6Es.htm)|Darkvision|Vision dans le noir|libre|
|[du4VcQ2P0kOpRVrS.htm](kingmaker-bestiary-items/du4VcQ2P0kOpRVrS.htm)|Buck|Désarçonner|libre|
|[DuDuSE2EKfZt9r6m.htm](kingmaker-bestiary-items/DuDuSE2EKfZt9r6m.htm)|Quickened Casting|Incantation accélérée|libre|
|[DUWbE68XkGU2jM9J.htm](kingmaker-bestiary-items/DUWbE68XkGU2jM9J.htm)|Fist|Poing|libre|
|[DVsGmUvb4tqHJrCw.htm](kingmaker-bestiary-items/DVsGmUvb4tqHJrCw.htm)|Rejuvenation|Reconstruction|libre|
|[dWpKdYaRxbAJiHSi.htm](kingmaker-bestiary-items/dWpKdYaRxbAJiHSi.htm)|Bloodbird|Oiseau sanguinaire|libre|
|[dX22wsxoPsGLzOQ0.htm](kingmaker-bestiary-items/dX22wsxoPsGLzOQ0.htm)|Dagger|Dague|libre|
|[DxTspLZpQI4zVPoa.htm](kingmaker-bestiary-items/DxTspLZpQI4zVPoa.htm)|Sorcerer Bloodline Spells|Sorts de lignage d'ensorceleur|libre|
|[e1txRyfONnCuYPny.htm](kingmaker-bestiary-items/e1txRyfONnCuYPny.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|libre|
|[E2BfR5UhdImAFitO.htm](kingmaker-bestiary-items/E2BfR5UhdImAFitO.htm)|Shyka's Judgement|Arbitrage de Shyka|libre|
|[e2cSvAso2wePkLAK.htm](kingmaker-bestiary-items/e2cSvAso2wePkLAK.htm)|Korog's Command|Autorité de Korog|libre|
|[E3C47mJQhyySioGh.htm](kingmaker-bestiary-items/E3C47mJQhyySioGh.htm)|Hatchet|Hachette|libre|
|[E3r5U0LkfWOXhgLt.htm](kingmaker-bestiary-items/E3r5U0LkfWOXhgLt.htm)|Shortbow|Arc court|libre|
|[EaWA2WkRjbpm2GiI.htm](kingmaker-bestiary-items/EaWA2WkRjbpm2GiI.htm)|Duplicate Victim|Dupliquer la victime|libre|
|[ECXG4lnlaSBXuqcP.htm](kingmaker-bestiary-items/ECXG4lnlaSBXuqcP.htm)|Defensive Slice|Attaque sur la défensive|libre|
|[EdoAyjfImWlnNF3Z.htm](kingmaker-bestiary-items/EdoAyjfImWlnNF3Z.htm)|End the Charade|Fin de la mascarade|libre|
|[Eeclk1UrcRUe4od5.htm](kingmaker-bestiary-items/Eeclk1UrcRUe4od5.htm)|Lock and Shriek|Verrouiller et hurler|libre|
|[EGY18Yebn9ZVwEbP.htm](kingmaker-bestiary-items/EGY18Yebn9ZVwEbP.htm)|Rat Empathy|Empathie avec les rats|libre|
|[EgymgxzMTQGUvdBD.htm](kingmaker-bestiary-items/EgymgxzMTQGUvdBD.htm)|Telekinetic Assault|Assaut télékinésique|libre|
|[EH6c2qLG9oWj3bcW.htm](kingmaker-bestiary-items/EH6c2qLG9oWj3bcW.htm)|Greataxe|+2,striking|Grande hache de frappe +2|libre|
|[eHtfWWTVUSw5Zu4F.htm](kingmaker-bestiary-items/eHtfWWTVUSw5Zu4F.htm)|Forceful Focus|Focus renforcé|libre|
|[EihtJU42yTV6LfVg.htm](kingmaker-bestiary-items/EihtJU42yTV6LfVg.htm)|Fetch Weapon|Arme de fetch|libre|
|[ek9L2OPkQLBaP0Ml.htm](kingmaker-bestiary-items/ek9L2OPkQLBaP0Ml.htm)|Bound to the Mortal Night|Lié à la nuit des mortels|libre|
|[Ekik19hqrAXIezul.htm](kingmaker-bestiary-items/Ekik19hqrAXIezul.htm)|Lock of green hair bound with red twine|Une mèche de cheveux verts reliés par une ficelle rouge|libre|
|[ELQWppSD9MT72Ci1.htm](kingmaker-bestiary-items/ELQWppSD9MT72Ci1.htm)|Staff of Fire (Major)|Bâton de Feu majeur|libre|
|[emb5f7dGdRTiUTnN.htm](kingmaker-bestiary-items/emb5f7dGdRTiUTnN.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[eMFelq83Tb6uIwqb.htm](kingmaker-bestiary-items/eMFelq83Tb6uIwqb.htm)|Moon Frenzy|Frénésie lunaire|libre|
|[Enk2lR0Vry7rabGe.htm](kingmaker-bestiary-items/Enk2lR0Vry7rabGe.htm)|Site Bound|Lié à un site|libre|
|[Epu5XvonDsZQClpu.htm](kingmaker-bestiary-items/Epu5XvonDsZQClpu.htm)|Flechette|Flèchettes|libre|
|[eQQryZfmKwV4meJL.htm](kingmaker-bestiary-items/eQQryZfmKwV4meJL.htm)|Quickened Casting|Incantation accélérée|libre|
|[ESPLHQJWpOyak6Wa.htm](kingmaker-bestiary-items/ESPLHQJWpOyak6Wa.htm)|Battle Axe|Hache d'arme|libre|
|[eSXITfBnqJ3s71nu.htm](kingmaker-bestiary-items/eSXITfBnqJ3s71nu.htm)|Divine Focus Spells|Sorts divins focalisés|libre|
|[EtUgChhYGZ37rp9z.htm](kingmaker-bestiary-items/EtUgChhYGZ37rp9z.htm)|Sure Possession|Possession certaine|libre|
|[eUDWR9NZHLWYc7Dj.htm](kingmaker-bestiary-items/eUDWR9NZHLWYc7Dj.htm)|Darkvision|Vision dans le noir|libre|
|[EVaLrmDugpw1gfO1.htm](kingmaker-bestiary-items/EVaLrmDugpw1gfO1.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[EwwHk83vJUlZ0cbV.htm](kingmaker-bestiary-items/EwwHk83vJUlZ0cbV.htm)|Improved Grab|Empoignade/Agrippement amélioré|libre|
|[exrZ9BjFfwPvWYPf.htm](kingmaker-bestiary-items/exrZ9BjFfwPvWYPf.htm)|Ripping Grab|Agrippement lacérant|libre|
|[Ez68XkpqwOnuPfr3.htm](kingmaker-bestiary-items/Ez68XkpqwOnuPfr3.htm)|Shield Block|Blocage au bouclier|libre|
|[eznh9ydPPecUq1K1.htm](kingmaker-bestiary-items/eznh9ydPPecUq1K1.htm)|Steady Spellcasting|Incantation fiable|libre|
|[f0ReT77CQpNnH0wr.htm](kingmaker-bestiary-items/f0ReT77CQpNnH0wr.htm)|+2 Resilient Full Plate|Harnois de résilience +2|officielle|
|[F0yeghQAPNgRWKCf.htm](kingmaker-bestiary-items/F0yeghQAPNgRWKCf.htm)|Deafening Cry|Hennissement assourdissant|libre|
|[f1KaRpZo7GoKPttu.htm](kingmaker-bestiary-items/f1KaRpZo7GoKPttu.htm)|Wand of Magic Missile (Level 1)|Baguette de Missiles magiques (Niveau 1)|libre|
|[f42wFnhs1khcDf8G.htm](kingmaker-bestiary-items/f42wFnhs1khcDf8G.htm)|Fast Healing 20|Guérison accélérée 20|libre|
|[F4qi1llZbd9QZy4d.htm](kingmaker-bestiary-items/F4qi1llZbd9QZy4d.htm)|Raise a Shield|Lever un bouclier|libre|
|[F6NTNYwrbXvVsHwE.htm](kingmaker-bestiary-items/F6NTNYwrbXvVsHwE.htm)|Robes|Robes|libre|
|[f7AUQc1tVLR8FHEA.htm](kingmaker-bestiary-items/f7AUQc1tVLR8FHEA.htm)|Fog Vision|Vision malgré le brouillard|libre|
|[fbNFZNeEcQgHVKW4.htm](kingmaker-bestiary-items/fbNFZNeEcQgHVKW4.htm)|Darkvision|Vision dans le noir|libre|
|[fckcc8VGCaiue1sX.htm](kingmaker-bestiary-items/fckcc8VGCaiue1sX.htm)|Divine Prepared Spells|Sorts divins préparés|libre|
|[FCU5JyYgiHDot5rm.htm](kingmaker-bestiary-items/FCU5JyYgiHDot5rm.htm)|Swarming Bites|Nuée de morsures|libre|
|[FEC49j9OjVBivyUX.htm](kingmaker-bestiary-items/FEC49j9OjVBivyUX.htm)|Planar Acclimation|Acclimatation planaire|libre|
|[FEvGGdluzakeGdBF.htm](kingmaker-bestiary-items/FEvGGdluzakeGdBF.htm)|+2 Greater Resilient Clothing (Explorer's)|Vêtement d'explorateur de résilience supérieure +2|libre|
|[FjxVn3oLSCrfIIIz.htm](kingmaker-bestiary-items/FjxVn3oLSCrfIIIz.htm)|Religious Symbol of Yog-Sothoth|Symbole religieux de Yog-Sothoth|libre|
|[FKNzpxoNtzzC5ytc.htm](kingmaker-bestiary-items/FKNzpxoNtzzC5ytc.htm)|Dagger|Dague|libre|
|[FktubLHfxuouP7QK.htm](kingmaker-bestiary-items/FktubLHfxuouP7QK.htm)|Stubborn Conviction|Conviction entêtée|libre|
|[Fl8QSyuys5Y8YCrK.htm](kingmaker-bestiary-items/Fl8QSyuys5Y8YCrK.htm)|Ghostly Hand|Main spectrale|libre|
|[FMwhWB2KeMuU3kYQ.htm](kingmaker-bestiary-items/FMwhWB2KeMuU3kYQ.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[FMyBd24spHonNXKf.htm](kingmaker-bestiary-items/FMyBd24spHonNXKf.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|libre|
|[FoPN2th3v5NxtKds.htm](kingmaker-bestiary-items/FoPN2th3v5NxtKds.htm)|Maul|+1|Maillet +1|libre|
|[fqcdNor1vTHZe5Zl.htm](kingmaker-bestiary-items/fqcdNor1vTHZe5Zl.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|libre|
|[fqO0yIYBu1FrLvB9.htm](kingmaker-bestiary-items/fqO0yIYBu1FrLvB9.htm)|Scythe|Faux|libre|
|[FQyPQNxKCzuNCQHu.htm](kingmaker-bestiary-items/FQyPQNxKCzuNCQHu.htm)|Wolf Empathy|Empathie avec les loups|libre|
|[fRaI1Z66oWzSAz2Q.htm](kingmaker-bestiary-items/fRaI1Z66oWzSAz2Q.htm)|Hit 'Em Hard|Taper fort|libre|
|[FrmCI6DzaoSpGyxI.htm](kingmaker-bestiary-items/FrmCI6DzaoSpGyxI.htm)|Twin Parry|Parade jumelée|libre|
|[fryglhBup2tRHWFS.htm](kingmaker-bestiary-items/fryglhBup2tRHWFS.htm)|Telepathy 300 feet|Télépathie 90 m|libre|
|[fS0rXdbVRA1IMM8E.htm](kingmaker-bestiary-items/fS0rXdbVRA1IMM8E.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[FS1iX2ShhtCPk1Wr.htm](kingmaker-bestiary-items/FS1iX2ShhtCPk1Wr.htm)|Shortsword|+1|Épée courte +1|officielle|
|[ftnV0syBwJfqGk6Z.htm](kingmaker-bestiary-items/ftnV0syBwJfqGk6Z.htm)|Thorn|Épines|libre|
|[fTVvztwtM7PZgob6.htm](kingmaker-bestiary-items/fTVvztwtM7PZgob6.htm)|Grab|Empoigner/agripper|libre|
|[FuvkjEpm8ZZnjZ46.htm](kingmaker-bestiary-items/FuvkjEpm8ZZnjZ46.htm)|Sneak Attack|Attaque sournoise|libre|
|[Fv9HoAW6MyuvLWBs.htm](kingmaker-bestiary-items/Fv9HoAW6MyuvLWBs.htm)|At-Will Spells|Sorts à volonté|libre|
|[Fw6GAT5RmaN6VMII.htm](kingmaker-bestiary-items/Fw6GAT5RmaN6VMII.htm)|Giant's Lunge|Fente de géant|libre|
|[FwhFzYJLEkQ3J68c.htm](kingmaker-bestiary-items/FwhFzYJLEkQ3J68c.htm)|Low-Light Vision|Vision nocturne|libre|
|[FwN998JxnTCCkJw3.htm](kingmaker-bestiary-items/FwN998JxnTCCkJw3.htm)|Reveal the Enemy|Révéler l'ennemi|libre|
|[fwojd1PspBIG8CEY.htm](kingmaker-bestiary-items/fwojd1PspBIG8CEY.htm)|Shortsword|Épée courte|libre|
|[fxgXQNNgTLwJ2JoU.htm](kingmaker-bestiary-items/fxgXQNNgTLwJ2JoU.htm)|Arcane Prepared Spells|Sorts arcaniques préparés|libre|
|[fXlB9FRj3C3aY3Vk.htm](kingmaker-bestiary-items/fXlB9FRj3C3aY3Vk.htm)|Form Control|Contrôle de forme|libre|
|[fYdweFlxcKABfwpC.htm](kingmaker-bestiary-items/fYdweFlxcKABfwpC.htm)|Rock|Rocher|libre|
|[FZAkeuRb3tyRnlmH.htm](kingmaker-bestiary-items/FZAkeuRb3tyRnlmH.htm)|Hurried Retreat|Retraite précipitée|libre|
|[FzjT88a1Bn4XsEYM.htm](kingmaker-bestiary-items/FzjT88a1Bn4XsEYM.htm)|Battle Axe|Hache d'armes|libre|
|[FZZwIPdH86woV8uz.htm](kingmaker-bestiary-items/FZZwIPdH86woV8uz.htm)|+1 Chain Mail|Cotte de mailles +1|officielle|
|[G0x7fr9ZPMuqw2RG.htm](kingmaker-bestiary-items/G0x7fr9ZPMuqw2RG.htm)|Weeping Aura|Aura de larmes|libre|
|[g6XydGLF6RtVUp5L.htm](kingmaker-bestiary-items/g6XydGLF6RtVUp5L.htm)|Stealth|Discrétion|libre|
|[g9hAlndNzXTVzNDu.htm](kingmaker-bestiary-items/g9hAlndNzXTVzNDu.htm)|Basic Woodworker's Book|Manuel de base du menuisier|libre|
|[GAwPcYxItK5M24Ve.htm](kingmaker-bestiary-items/GAwPcYxItK5M24Ve.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[gbg7dpE5qccG0uh1.htm](kingmaker-bestiary-items/gbg7dpE5qccG0uh1.htm)|Greater Shock Maul|Maillet de foudre supérieur|libre|
|[GcSM5ZkG8OONvY0G.htm](kingmaker-bestiary-items/GcSM5ZkG8OONvY0G.htm)|Fey Conduit|Intermédiaire Fée|libre|
|[gDENaXLY2aYA7jYG.htm](kingmaker-bestiary-items/gDENaXLY2aYA7jYG.htm)|Greataxe|Grande hache|libre|
|[gEF9138FUjhpAL9X.htm](kingmaker-bestiary-items/gEF9138FUjhpAL9X.htm)|True Seeing (Constant)|Vision lucide (constant)|officielle|
|[gfH6Km7ijznOKHzF.htm](kingmaker-bestiary-items/gfH6Km7ijznOKHzF.htm)|Negative Healing|Guérison négative|libre|
|[ghqAZgEVPVGBLtZW.htm](kingmaker-bestiary-items/ghqAZgEVPVGBLtZW.htm)|Tongue Grab|Agrippement avec la langue|libre|
|[gj9hDQvaXekxyv1Q.htm](kingmaker-bestiary-items/gj9hDQvaXekxyv1Q.htm)|Longsword|Épée longue|libre|
|[GJxtjkowvzk3cvaE.htm](kingmaker-bestiary-items/GJxtjkowvzk3cvaE.htm)|Constant Spells|Sorts constants|libre|
|[Gk2AECC4rYPqsJbg.htm](kingmaker-bestiary-items/Gk2AECC4rYPqsJbg.htm)|Restore Senses (At Will)|Restaurer les sens (À volonté)|libre|
|[GLcjMCpmzoaId5jx.htm](kingmaker-bestiary-items/GLcjMCpmzoaId5jx.htm)|Imprisonment (Cannot Currently Cast)|Emprisonnement (Ne peut pas le lancer pour l'instant)|libre|
|[GMKTkSGoth8sggbH.htm](kingmaker-bestiary-items/GMKTkSGoth8sggbH.htm)|Call Glaive|Appel du glaive|libre|
|[gS87nm7lmCmY8pzz.htm](kingmaker-bestiary-items/gS87nm7lmCmY8pzz.htm)|First World Lore|Connaissance du Premier monde|libre|
|[gsd3hcxt7GSe5WzZ.htm](kingmaker-bestiary-items/gsd3hcxt7GSe5WzZ.htm)|Aldori Dueling Sword|+1|Épée de duel Aldori +1|libre|
|[gsoYzQxRygC654Xf.htm](kingmaker-bestiary-items/gsoYzQxRygC654Xf.htm)|Pounce|Bond|libre|
|[gVhcOspe8h7FkmF1.htm](kingmaker-bestiary-items/gVhcOspe8h7FkmF1.htm)|Robes|Robes|libre|
|[gvKFoNQurhUZnlWh.htm](kingmaker-bestiary-items/gvKFoNQurhUZnlWh.htm)|Ring of Energy Resistance (Fire)|Anneau de résistance aux énergies (Feu)|libre|
|[GxlQBNruDU25fJkQ.htm](kingmaker-bestiary-items/GxlQBNruDU25fJkQ.htm)|Open Doors|Ouverture des portes|libre|
|[GxSpJFY9UJiwry54.htm](kingmaker-bestiary-items/GxSpJFY9UJiwry54.htm)|Jaws|Mâchoires|libre|
|[gymUtf0OxE6gzjqN.htm](kingmaker-bestiary-items/gymUtf0OxE6gzjqN.htm)|+1 Glamered Chain Shirt|Chemise de maille de mimétisme +1|libre|
|[GZ3aIv1wg9M7gGFU.htm](kingmaker-bestiary-items/GZ3aIv1wg9M7gGFU.htm)|Giant Lore|Connaissance des géants|libre|
|[GZ9qL58QMmBtlHul.htm](kingmaker-bestiary-items/GZ9qL58QMmBtlHul.htm)|Electric Healing|Guérison électrique|libre|
|[gzziZdYQCjJ1pCM1.htm](kingmaker-bestiary-items/gzziZdYQCjJ1pCM1.htm)|Reminder of Doom|Rappel du châtiment|libre|
|[h0MxDLIAI1CLU61G.htm](kingmaker-bestiary-items/h0MxDLIAI1CLU61G.htm)|Thorny Lash|Fouet épineux|libre|
|[h2wSXBnK6CA96ypS.htm](kingmaker-bestiary-items/h2wSXBnK6CA96ypS.htm)|Reach Spell|Sort éloigné|libre|
|[h4yqrCg7sP5DCU4F.htm](kingmaker-bestiary-items/h4yqrCg7sP5DCU4F.htm)|Darkvision|Vision dans le noir|libre|
|[H5KqV1tEsBEfhfvU.htm](kingmaker-bestiary-items/H5KqV1tEsBEfhfvU.htm)|Hunt Prey|Chasser une proie|libre|
|[H8mIfS4BQ0XpEAtY.htm](kingmaker-bestiary-items/H8mIfS4BQ0XpEAtY.htm)|Launch Marbles|Lance billes|libre|
|[H8UomLORlnQErtR4.htm](kingmaker-bestiary-items/H8UomLORlnQErtR4.htm)|Gnawing Bite|Morsure rongeante|libre|
|[hbOLSP9RZmA3hyU8.htm](kingmaker-bestiary-items/hbOLSP9RZmA3hyU8.htm)|+1 Status to All Saves vs. Poison|bonus de statut de +1 aux JdS contre le poison|libre|
|[hCCNqP3969tyxk96.htm](kingmaker-bestiary-items/hCCNqP3969tyxk96.htm)|+2 Greater Striking Spiked Greatclub|+2,greaterStriking|Massue cloutée de frappe supérieure +2|libre|
|[hdEvwgLsMiLlJbMm.htm](kingmaker-bestiary-items/hdEvwgLsMiLlJbMm.htm)|+2 Greater Resilient Hide Armor|Armure en peau de résilience supérieure +2|officielle|
|[hE1MVc0Pxfi3nERv.htm](kingmaker-bestiary-items/hE1MVc0Pxfi3nERv.htm)|Greatsword|Épée longue|libre|
|[HfIRzOBnqZx60iSO.htm](kingmaker-bestiary-items/HfIRzOBnqZx60iSO.htm)|Club|Gourdin|libre|
|[hhOHcOSPkhLdvric.htm](kingmaker-bestiary-items/hhOHcOSPkhLdvric.htm)|Confusing Gaze|Regard déconcertant|libre|
|[hID13CF39qYYQ2rX.htm](kingmaker-bestiary-items/hID13CF39qYYQ2rX.htm)|Collapse|Effondrement|libre|
|[HiL2TCMI2Rzmy2SW.htm](kingmaker-bestiary-items/HiL2TCMI2Rzmy2SW.htm)|Juggernaut|Juggernaut|libre|
|[HiZTEnboQEoek6kL.htm](kingmaker-bestiary-items/HiZTEnboQEoek6kL.htm)|Three-Headed Strike|Frappe à trois têtes|libre|
|[hjfvtpOl2IljcOOH.htm](kingmaker-bestiary-items/hjfvtpOl2IljcOOH.htm)|Outcast's Curse (At Will)|Malédiction du paria (à volonté)|libre|
|[HjxDZjwRHwjQx54V.htm](kingmaker-bestiary-items/HjxDZjwRHwjQx54V.htm)|Spear|Lance|libre|
|[HKEKbaIpMla2LGS8.htm](kingmaker-bestiary-items/HKEKbaIpMla2LGS8.htm)|Sneak Attack|Attaque sournoise|libre|
|[hO1dPgyMTWEGJRxH.htm](kingmaker-bestiary-items/hO1dPgyMTWEGJRxH.htm)|Rejuvenation|Reconstruction|libre|
|[HplaolUyjv0ldmah.htm](kingmaker-bestiary-items/HplaolUyjv0ldmah.htm)|Eagle Eye Elixir (Major) (Infused)|Élixir d'oeil de faucon majeur (imprégné)|libre|
|[HQAGq6saSYHzJC0I.htm](kingmaker-bestiary-items/HQAGq6saSYHzJC0I.htm)|Three Headed|Tricéphale|libre|
|[hqd0OwbFVtZ6VGi3.htm](kingmaker-bestiary-items/hqd0OwbFVtZ6VGi3.htm)|Sapphire Earrings|Boucles d'oreilles en saphir|libre|
|[hqm0z6fZAXCCTDOs.htm](kingmaker-bestiary-items/hqm0z6fZAXCCTDOs.htm)|Wild Stride|Déplacement facilité en milieu naturel|libre|
|[HrP24kDK1jVss45v.htm](kingmaker-bestiary-items/HrP24kDK1jVss45v.htm)|Fast Healing 10|Guérison accélérée 10|libre|
|[hsn4E9lEPjjDCMAN.htm](kingmaker-bestiary-items/hsn4E9lEPjjDCMAN.htm)|Occult Focus Spells|Sorts occultes focalisés|libre|
|[HsvS5P4EDcjC5Y5A.htm](kingmaker-bestiary-items/HsvS5P4EDcjC5Y5A.htm)|Outcast's Curse (At Will)|Malédiction du paria (à volonté)|libre|
|[htpcG9CC3rJNay4j.htm](kingmaker-bestiary-items/htpcG9CC3rJNay4j.htm)|Fist|Poing|libre|
|[HtScT1S9mxNqMylg.htm](kingmaker-bestiary-items/HtScT1S9mxNqMylg.htm)|Fast Healing 15|Guérison accélérée 15|libre|
|[hvCc4reISEOqsFIa.htm](kingmaker-bestiary-items/hvCc4reISEOqsFIa.htm)|Darkness (At Will)|Ténèbres (À volonté)|officielle|
|[HvSxWkSSm2CCuJHu.htm](kingmaker-bestiary-items/HvSxWkSSm2CCuJHu.htm)|+1 Cold-Resistant Leather Armor|Armure en cuir de résistance au froid +1|libre|
|[HWNQMuf5GV8rRSMu.htm](kingmaker-bestiary-items/HWNQMuf5GV8rRSMu.htm)|Frightful Presence|Présence terrifiante|libre|
|[hXe1xaYFIHJ8f7tw.htm](kingmaker-bestiary-items/hXe1xaYFIHJ8f7tw.htm)|Trident|+1,striking|Trident de frappe +1|libre|
|[hxeZesdJLgHPkEJ5.htm](kingmaker-bestiary-items/hxeZesdJLgHPkEJ5.htm)|Spectral Hand|Main spectrale|libre|
|[HYAfgOkdktmrrKGf.htm](kingmaker-bestiary-items/HYAfgOkdktmrrKGf.htm)|Stygian Fire|Feu stygien|libre|
|[HYnUw0h1bJQMkzBc.htm](kingmaker-bestiary-items/HYnUw0h1bJQMkzBc.htm)|Planar Acclimation|Acclimatation planaire|libre|
|[Hyozh14mnCpe6YVy.htm](kingmaker-bestiary-items/Hyozh14mnCpe6YVy.htm)|Shortsword|+1,striking|Épée courte de frappe +1|officielle|
|[hznpxgtC8SziycgN.htm](kingmaker-bestiary-items/hznpxgtC8SziycgN.htm)|Hatchet Onslaught|Assaut à la hachette|libre|
|[HZvbzYmbccVoGV1B.htm](kingmaker-bestiary-items/HZvbzYmbccVoGV1B.htm)|Maul Mastery|Maîtrise du maillet|libre|
|[HZwLWbZdPHnymsUr.htm](kingmaker-bestiary-items/HZwLWbZdPHnymsUr.htm)|Darkvision|Vision dans le noir|libre|
|[i42oMxLUkfr8rkMc.htm](kingmaker-bestiary-items/i42oMxLUkfr8rkMc.htm)|Open Doors|Ouverture des portes|libre|
|[i5DVxzz2vN9zcxsC.htm](kingmaker-bestiary-items/i5DVxzz2vN9zcxsC.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[i8ykQ7pFedbfWxeV.htm](kingmaker-bestiary-items/i8ykQ7pFedbfWxeV.htm)|Staff|Bâton|libre|
|[i98RWuQkObalnsWO.htm](kingmaker-bestiary-items/i98RWuQkObalnsWO.htm)|Shortbow|Arc court|libre|
|[I9tYnS5FyCSmLBYY.htm](kingmaker-bestiary-items/I9tYnS5FyCSmLBYY.htm)|Tree Shape (At Will)|Morphologie d'arbre (À volonté)|libre|
|[IDIM2wkSDDgMI06V.htm](kingmaker-bestiary-items/IDIM2wkSDDgMI06V.htm)|Grasping Roots|Racines agrippantes|libre|
|[iE3cMVqsLzS8aqvH.htm](kingmaker-bestiary-items/iE3cMVqsLzS8aqvH.htm)|Rejuvenation|Reconstruction|libre|
|[ifhXNlwgfGtUNABs.htm](kingmaker-bestiary-items/ifhXNlwgfGtUNABs.htm)|Reactive Charge|Charge réactive|libre|
|[iGGr29rdFCwRoCHG.htm](kingmaker-bestiary-items/iGGr29rdFCwRoCHG.htm)|Turquise Earrings|Boucles d'oreilles de tourquoise|libre|
|[iGnRoOsUlKx1zO7Q.htm](kingmaker-bestiary-items/iGnRoOsUlKx1zO7Q.htm)|Death Throes|Agonie fatale|libre|
|[ihGm6kH3jaNxYb3p.htm](kingmaker-bestiary-items/ihGm6kH3jaNxYb3p.htm)|Tremorsense (Precise) 30 feet|Perception des vibrations 9 m (précis)|libre|
|[iHz93nPS055VlpTQ.htm](kingmaker-bestiary-items/iHz93nPS055VlpTQ.htm)|+1 Crossbow|+1,striking|Arbalète de frappe +1|libre|
|[IJyUjXFsO9M8LpUs.htm](kingmaker-bestiary-items/IJyUjXFsO9M8LpUs.htm)|Elegant Robes|Robes élégantes|libre|
|[ik0FmQbY7s9wzZh2.htm](kingmaker-bestiary-items/ik0FmQbY7s9wzZh2.htm)|Wild Shape|Morphologie sauvage|libre|
|[Ik3rJsTnSTRAi671.htm](kingmaker-bestiary-items/Ik3rJsTnSTRAi671.htm)|Invisibility (Self Only)|Invisibilité (soi uniquement)|officielle|
|[IkPoWCmWZcyW5osI.htm](kingmaker-bestiary-items/IkPoWCmWZcyW5osI.htm)|Staff|Bâton|libre|
|[ILS10PAagocQkyqT.htm](kingmaker-bestiary-items/ILS10PAagocQkyqT.htm)|Hunt Prey|Chasser une proie|libre|
|[ImYiua3KKxG5XU61.htm](kingmaker-bestiary-items/ImYiua3KKxG5XU61.htm)|Play the Pipes|Jouer la flûte|libre|
|[ImzpAPq1SgMcf7t3.htm](kingmaker-bestiary-items/ImzpAPq1SgMcf7t3.htm)|Hunt Prey|Chasser une proie|libre|
|[In6OZtLo0Avr0mV1.htm](kingmaker-bestiary-items/In6OZtLo0Avr0mV1.htm)|Trident|Trident|libre|
|[inAbFfjyYxWyFk2e.htm](kingmaker-bestiary-items/inAbFfjyYxWyFk2e.htm)|Prostrate|Prosternation|libre|
|[Io2JD6hMiVboalmM.htm](kingmaker-bestiary-items/Io2JD6hMiVboalmM.htm)|Cunning|Astucieux|libre|
|[iOA6wNMnIzBStwhB.htm](kingmaker-bestiary-items/iOA6wNMnIzBStwhB.htm)|Return to Pavetta|Revenir vers Pavetta|libre|
|[IOZpqdcbetYaemZ3.htm](kingmaker-bestiary-items/IOZpqdcbetYaemZ3.htm)|Monstrous Bloom|Floraison monstrueuse|libre|
|[IP2DHIvoqbsWaRVU.htm](kingmaker-bestiary-items/IP2DHIvoqbsWaRVU.htm)|Cleaver|Fendoir|libre|
|[ipARSA7VFfyApjl1.htm](kingmaker-bestiary-items/ipARSA7VFfyApjl1.htm)|Throw Rock|Lancé de rocher|libre|
|[iqKVeVAvsVEgW29e.htm](kingmaker-bestiary-items/iqKVeVAvsVEgW29e.htm)|Rejuvenation|Reconstruction|libre|
|[ITgYWyiUVLD2SpLl.htm](kingmaker-bestiary-items/ITgYWyiUVLD2SpLl.htm)|Composite Shortbow|Arc court composite|libre|
|[ItuwvVtjFsk5P6Rk.htm](kingmaker-bestiary-items/ItuwvVtjFsk5P6Rk.htm)|Knight and Dragon Toys|Jouets figurines de chevalier et de dragon|libre|
|[iVaCyzg2MMKJ1SIu.htm](kingmaker-bestiary-items/iVaCyzg2MMKJ1SIu.htm)|Darkvision|Vision dans le noir|libre|
|[IW0bpDD32dNAHnum.htm](kingmaker-bestiary-items/IW0bpDD32dNAHnum.htm)|Frumious Charge|Charge perfide|libre|
|[IYbU9IiWVPm4CGyc.htm](kingmaker-bestiary-items/IYbU9IiWVPm4CGyc.htm)|Fist|Poing|libre|
|[Iyw7GZBX5oMU1kR0.htm](kingmaker-bestiary-items/Iyw7GZBX5oMU1kR0.htm)|Darkvision|Vision dans le noir|libre|
|[IzVf6cehi9NajoXy.htm](kingmaker-bestiary-items/IzVf6cehi9NajoXy.htm)|Trident Twist|Torsade de trident|libre|
|[j1GC3KLLJa3BPe0o.htm](kingmaker-bestiary-items/j1GC3KLLJa3BPe0o.htm)|Battle Axe|Hache de bataille|libre|
|[J3lzyo3f2n6RNBG4.htm](kingmaker-bestiary-items/J3lzyo3f2n6RNBG4.htm)|Rushing Water|Précipitation d'eau|libre|
|[j4kcdayowXPW4Avm.htm](kingmaker-bestiary-items/j4kcdayowXPW4Avm.htm)|Horns|Cornes|libre|
|[j8nRwfwxCyxXRet5.htm](kingmaker-bestiary-items/j8nRwfwxCyxXRet5.htm)|Dagger|Dague|libre|
|[j8PYlRTNy6uxScHw.htm](kingmaker-bestiary-items/j8PYlRTNy6uxScHw.htm)|Wand of Dimension Door (Level 4)|Baguette de Porte dimensionnelle (Niveau 4)|libre|
|[jAX00kRCiI1ahLJA.htm](kingmaker-bestiary-items/jAX00kRCiI1ahLJA.htm)|Bow Specialist|Spécialisé en Arc|libre|
|[jBAYmOeepk3w4EgS.htm](kingmaker-bestiary-items/jBAYmOeepk3w4EgS.htm)|Rend|Éventration|libre|
|[jEkasU1qDlN76A9i.htm](kingmaker-bestiary-items/jEkasU1qDlN76A9i.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[jga4jzrMzJRkNouq.htm](kingmaker-bestiary-items/jga4jzrMzJRkNouq.htm)|Formation Fighters|Formation de guerriers|libre|
|[JHBWAs0dwbsbDu7J.htm](kingmaker-bestiary-items/JHBWAs0dwbsbDu7J.htm)|+2 Greater Resilient Full Plate|Harnois de résilience supérieure +2|officielle|
|[JjphR7198vaXiAJo.htm](kingmaker-bestiary-items/JjphR7198vaXiAJo.htm)|Defensive Roll|Roulade défensive|libre|
|[jkgZ1UCUT3j7cOf2.htm](kingmaker-bestiary-items/jkgZ1UCUT3j7cOf2.htm)|Restov Lore|Connaissance de Restov|libre|
|[jkJ5X9sX7YU7UA2F.htm](kingmaker-bestiary-items/jkJ5X9sX7YU7UA2F.htm)|+1 Resilient Breastplate|Cuirasse de résilience +1|officielle|
|[JlGJE2TMv5okfuks.htm](kingmaker-bestiary-items/JlGJE2TMv5okfuks.htm)|Dagger|+1,greaterStriking,greaterFrost|Dague de froid supérieure de frappe supérieure +1|libre|
|[jlJLCgnS5JMrSDgN.htm](kingmaker-bestiary-items/jlJLCgnS5JMrSDgN.htm)|Tear Free|S'arracher|libre|
|[JloIn4LDx9M1fiyw.htm](kingmaker-bestiary-items/JloIn4LDx9M1fiyw.htm)|Eclipsed Incantations|Incantations Éclipsées|libre|
|[jmgh011n8MXGN953.htm](kingmaker-bestiary-items/jmgh011n8MXGN953.htm)|Dagger|Dague|libre|
|[jmL1vjBAG3jha7pe.htm](kingmaker-bestiary-items/jmL1vjBAG3jha7pe.htm)|Stabbing Fit|Crise au poignard|libre|
|[Jn2HBbRFuiRxKRbB.htm](kingmaker-bestiary-items/Jn2HBbRFuiRxKRbB.htm)|Goblin Scuttle|Précipitation gobeline|libre|
|[JoOzphwUQC4ZFm7J.htm](kingmaker-bestiary-items/JoOzphwUQC4ZFm7J.htm)|Saving Slash|Parade salvatrice|libre|
|[jp5lLzCnJEJ3qCUl.htm](kingmaker-bestiary-items/jp5lLzCnJEJ3qCUl.htm)|Arcane Spells Prepared|Sorts arcaniques préparés|libre|
|[JP9GiA6aaQlXRKAE.htm](kingmaker-bestiary-items/JP9GiA6aaQlXRKAE.htm)|Primal Prepared Spells|Sorts primordiaux préparés|libre|
|[JPmOqF1x4Au8drWv.htm](kingmaker-bestiary-items/JPmOqF1x4Au8drWv.htm)|Spectral Uprising|Insurrection spectrale|libre|
|[JRCxllIuf9pbk0QY.htm](kingmaker-bestiary-items/JRCxllIuf9pbk0QY.htm)|Kukri|+2,greaterStriking,keen|Kukri acéré de frappe supérieure +1|libre|
|[jU73I507kdECl13G.htm](kingmaker-bestiary-items/jU73I507kdECl13G.htm)|Contingency|Contingence|libre|
|[jUFQA2dWOvLpv6zg.htm](kingmaker-bestiary-items/jUFQA2dWOvLpv6zg.htm)|Fist|Poing|libre|
|[jvDyUbBwbo3R1H4x.htm](kingmaker-bestiary-items/jvDyUbBwbo3R1H4x.htm)|Divine Prepared Spells|Sorts divins préparés|libre|
|[JWMAFJOdy4Odx7RY.htm](kingmaker-bestiary-items/JWMAFJOdy4Odx7RY.htm)|Playful Switch|Échange enjoué|libre|
|[JXtD0GzHXfGW7Aan.htm](kingmaker-bestiary-items/JXtD0GzHXfGW7Aan.htm)|Deny Advantage|Refus d'avantage|libre|
|[jXwGpQmNRAqEcUQN.htm](kingmaker-bestiary-items/jXwGpQmNRAqEcUQN.htm)|Quickened Casting|Incantation accélérée|libre|
|[jyTZDCNVazHG1tSk.htm](kingmaker-bestiary-items/jyTZDCNVazHG1tSk.htm)|Burst|Explosion|libre|
|[jZfqY5quDvHq9tmh.htm](kingmaker-bestiary-items/jZfqY5quDvHq9tmh.htm)|Wild Empathy|Empathie sauvage|libre|
|[k1LT9AcDMUPeGPOy.htm](kingmaker-bestiary-items/k1LT9AcDMUPeGPOy.htm)|Righteous Certainty|Certitude du juste|libre|
|[k2R72XvX3USofjXB.htm](kingmaker-bestiary-items/k2R72XvX3USofjXB.htm)|Lash Out|Se déchaîner|libre|
|[K3AfPmsD7hzdbhv0.htm](kingmaker-bestiary-items/K3AfPmsD7hzdbhv0.htm)|Sneak Attack|Attaque sournoise|libre|
|[K470oCZYTiRoTfqf.htm](kingmaker-bestiary-items/K470oCZYTiRoTfqf.htm)|Jaws|Mâchoire|libre|
|[K5qMhCPST2MiSV7J.htm](kingmaker-bestiary-items/K5qMhCPST2MiSV7J.htm)|Inveigled|Hypnotisé|libre|
|[k6dnEhWODfyRtg4b.htm](kingmaker-bestiary-items/k6dnEhWODfyRtg4b.htm)|Composite Shortbow|Arc court composite|libre|
|[K7J1kJsdpeoiTUqZ.htm](kingmaker-bestiary-items/K7J1kJsdpeoiTUqZ.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[k8GjK7psOKSAVUhr.htm](kingmaker-bestiary-items/k8GjK7psOKSAVUhr.htm)|Prismatic Spray (At Will)|Rayon prismatique (À volonté)|libre|
|[k9kbOoeLWV3Bx8l5.htm](kingmaker-bestiary-items/k9kbOoeLWV3Bx8l5.htm)|Moment of Solitude|Moment de solitude|libre|
|[kaPLdey2Dv3F1VUN.htm](kingmaker-bestiary-items/kaPLdey2Dv3F1VUN.htm)|Oathbow|Arc du serment|libre|
|[KbYGZddUNQGhrhEf.htm](kingmaker-bestiary-items/KbYGZddUNQGhrhEf.htm)|Composite Longbow|Arc long composite|libre|
|[KCOXqXVayJMKPZVz.htm](kingmaker-bestiary-items/KCOXqXVayJMKPZVz.htm)|Composite Longbow|Arc long composite|libre|
|[kCwijuNrHVlRHQr0.htm](kingmaker-bestiary-items/kCwijuNrHVlRHQr0.htm)|Flask of Wine|Outre de vin|libre|
|[kdl1FTleQZfZf6eP.htm](kingmaker-bestiary-items/kdl1FTleQZfZf6eP.htm)|Primal Spontaneous Spells|Sorts primordiaux spontanés|libre|
|[kEk8UpJ0Ow9EnnTr.htm](kingmaker-bestiary-items/kEk8UpJ0Ow9EnnTr.htm)|Warhammer|+2,greaterFrost|Marteau de guerre de froid supérieur +2|libre|
|[KEm1y8hEl4sjQ6Cv.htm](kingmaker-bestiary-items/KEm1y8hEl4sjQ6Cv.htm)|Talon|Serre|libre|
|[kfdzBPs02IvVMabf.htm](kingmaker-bestiary-items/kfdzBPs02IvVMabf.htm)|Focus Gaze|Focaliser le regard|libre|
|[kgHoUAo23IIBJKPK.htm](kingmaker-bestiary-items/kgHoUAo23IIBJKPK.htm)|+1 Striking Staff of Fire (Major)|+1,striking|Bâton de Feu majeur de frappe +1|libre|
|[kh9EPJwZWvajnZBV.htm](kingmaker-bestiary-items/kh9EPJwZWvajnZBV.htm)|At-Will Spells|Sorts à volonté|libre|
|[khNk0fYgnDMF7kKq.htm](kingmaker-bestiary-items/khNk0fYgnDMF7kKq.htm)|First World Lore|Connaissance du Premier monde|libre|
|[kJ9e1DXfbX04Qfoe.htm](kingmaker-bestiary-items/kJ9e1DXfbX04Qfoe.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|libre|
|[kJR445IOTwrpVfxm.htm](kingmaker-bestiary-items/kJR445IOTwrpVfxm.htm)|Emerald Ring|Bague d'émeraude|libre|
|[klmOYsNgGYaAMROh.htm](kingmaker-bestiary-items/klmOYsNgGYaAMROh.htm)|Knockback|Repousser|libre|
|[kMVtB4pZd0LYoYOm.htm](kingmaker-bestiary-items/kMVtB4pZd0LYoYOm.htm)|Slam Doors|Claquer les portes|libre|
|[KngnsUrW0RbmKrce.htm](kingmaker-bestiary-items/KngnsUrW0RbmKrce.htm)|Focus Beauty|Focaliser la beauté|libre|
|[KNywleLC7uqltyig.htm](kingmaker-bestiary-items/KNywleLC7uqltyig.htm)|Focus Gaze|Focaliser le regard|libre|
|[Kok7cEEC0PeallmB.htm](kingmaker-bestiary-items/Kok7cEEC0PeallmB.htm)|Horns|Cornes|libre|
|[kooXn2rvr04D9fM6.htm](kingmaker-bestiary-items/kooXn2rvr04D9fM6.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[koqEAUHXo9t7wqVS.htm](kingmaker-bestiary-items/koqEAUHXo9t7wqVS.htm)|Mace|Masse d'armes|libre|
|[kPaYMeS20gIcKi3E.htm](kingmaker-bestiary-items/kPaYMeS20gIcKi3E.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[KpHIq12RL6K9ukG2.htm](kingmaker-bestiary-items/KpHIq12RL6K9ukG2.htm)|First World Lore|Connaissance du Premier monde|libre|
|[krTcoq8OKd5gFAL0.htm](kingmaker-bestiary-items/krTcoq8OKd5gFAL0.htm)|Sneak Attack|Attaque sournoise|libre|
|[KtA9MV7RA5nWRmko.htm](kingmaker-bestiary-items/KtA9MV7RA5nWRmko.htm)|Blinding Sickness|Maladie aveuglante|libre|
|[kTN86guVl5qzlzIK.htm](kingmaker-bestiary-items/kTN86guVl5qzlzIK.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[kTPMsO2Nauv9vM6Y.htm](kingmaker-bestiary-items/kTPMsO2Nauv9vM6Y.htm)|Speak with Plants (Constant)|Communication avec les plantes (Constant)|libre|
|[kTXlEGWWz4WnsrwF.htm](kingmaker-bestiary-items/kTXlEGWWz4WnsrwF.htm)|Warfare Lore|Connaissance de la guerre|libre|
|[kU6LiydL3jldv5KQ.htm](kingmaker-bestiary-items/kU6LiydL3jldv5KQ.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[KUcCUd82J1JqXWE5.htm](kingmaker-bestiary-items/KUcCUd82J1JqXWE5.htm)|Breath Weapon|Arme de souffle|libre|
|[KV65DhNy4Axid8kc.htm](kingmaker-bestiary-items/KV65DhNy4Axid8kc.htm)|Cleaver|Fendoir|libre|
|[kv7A4qqpl2mG1nHt.htm](kingmaker-bestiary-items/kv7A4qqpl2mG1nHt.htm)|Quickened Casting|Incantation accélérée|libre|
|[kVgdUKwhGxbwACnL.htm](kingmaker-bestiary-items/kVgdUKwhGxbwACnL.htm)|Hunting Spider Venom (On Darts)|Venin d'araignée chasseresse (sur les fléchettes)|libre|
|[kX089KrOjeO3aDf3.htm](kingmaker-bestiary-items/kX089KrOjeO3aDf3.htm)|Composite Longbow|Arc long composite|libre|
|[kXmoXkc5myLgiFoy.htm](kingmaker-bestiary-items/kXmoXkc5myLgiFoy.htm)|Mind Probe (At Will)|Sonde mentale (À volonté)|libre|
|[KXuuj04cishnGNpk.htm](kingmaker-bestiary-items/KXuuj04cishnGNpk.htm)|Trapdoor Lunge|Fente de trappe|libre|
|[kZiYWe3uWt1JBdbS.htm](kingmaker-bestiary-items/kZiYWe3uWt1JBdbS.htm)|Familiar|Familier|libre|
|[l47LUxjBXwp06JZ8.htm](kingmaker-bestiary-items/l47LUxjBXwp06JZ8.htm)|Glaive|+1|Coutille +1|libre|
|[L9sxOR3SnpDYfZHq.htm](kingmaker-bestiary-items/L9sxOR3SnpDYfZHq.htm)|Tree Stride (Self plus Willing Rider)|Voyage par les arbres (soi plus Chevaucheur consentant)|libre|
|[LbdwyQgOrSBqDgv9.htm](kingmaker-bestiary-items/LbdwyQgOrSBqDgv9.htm)|Dagger|Dague|libre|
|[lbWYtWXNDRTmYH9O.htm](kingmaker-bestiary-items/lbWYtWXNDRTmYH9O.htm)|Gold Signet Ring|Chevalière en or|libre|
|[LC8WDqxmulYb1L4q.htm](kingmaker-bestiary-items/LC8WDqxmulYb1L4q.htm)|Dagger|Dague|libre|
|[lCn44iSRdDgu2GdC.htm](kingmaker-bestiary-items/lCn44iSRdDgu2GdC.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[LCVw81WewWAm0wOB.htm](kingmaker-bestiary-items/LCVw81WewWAm0wOB.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[LEDnvueR2NzH8zkD.htm](kingmaker-bestiary-items/LEDnvueR2NzH8zkD.htm)|Dagger|Dague|libre|
|[leYjDFOKDNCIZOBf.htm](kingmaker-bestiary-items/leYjDFOKDNCIZOBf.htm)|Sneak Attack|Attaque sournoise|libre|
|[lffL96H4Twel1YRH.htm](kingmaker-bestiary-items/lffL96H4Twel1YRH.htm)|Darkvision|Vision dans le noir|libre|
|[lGRlzXnOSNGW7djU.htm](kingmaker-bestiary-items/lGRlzXnOSNGW7djU.htm)|Shame|Honte|libre|
|[lH0kmQP6fJWkOqZ5.htm](kingmaker-bestiary-items/lH0kmQP6fJWkOqZ5.htm)|+2 Resilient White Dragonhide Breastplate|Cuirasse en peau de dragon blanc de résilience +2|libre|
|[LHmME2otANk4a79s.htm](kingmaker-bestiary-items/LHmME2otANk4a79s.htm)|Worm Jaws|Mâchoires de ver|libre|
|[LHPXbhgsSYCjB4uG.htm](kingmaker-bestiary-items/LHPXbhgsSYCjB4uG.htm)|Club|Gourdin|libre|
|[lim8vhAnHsss3MkL.htm](kingmaker-bestiary-items/lim8vhAnHsss3MkL.htm)|Light Hammer|Marteau de guerre léger|libre|
|[LK0wAtZtoKTLOgnz.htm](kingmaker-bestiary-items/LK0wAtZtoKTLOgnz.htm)|Claw|Griffe|libre|
|[lLQgAigQauz0Z3sx.htm](kingmaker-bestiary-items/lLQgAigQauz0Z3sx.htm)|Designate Blood Foe|Ennemi de sang désigné|libre|
|[lo9zjzoOZSRs5svZ.htm](kingmaker-bestiary-items/lo9zjzoOZSRs5svZ.htm)|Darkness (At Will)|Ténèbres (À volonté)|officielle|
|[Lq9tywTCezzL2UYy.htm](kingmaker-bestiary-items/Lq9tywTCezzL2UYy.htm)|Lamashtu's Bloom|Floraison de Lamashtu|libre|
|[lqfoMvtMondVCDSV.htm](kingmaker-bestiary-items/lqfoMvtMondVCDSV.htm)|Steady Spellcasting|Incantation fiable|libre|
|[Lri10NA5TEU7fZeX.htm](kingmaker-bestiary-items/Lri10NA5TEU7fZeX.htm)|Call to the Hunt|Appel de la chasse|libre|
|[LsUtyTnBZPF6Hvnp.htm](kingmaker-bestiary-items/LsUtyTnBZPF6Hvnp.htm)|Frightful Presence|Présence terrifiante|libre|
|[lta8LCkZJo5xDrQ5.htm](kingmaker-bestiary-items/lta8LCkZJo5xDrQ5.htm)|+1 Resilient Full Plate|Harnois de résilience +1|officielle|
|[LTaIE8s2797qElxg.htm](kingmaker-bestiary-items/LTaIE8s2797qElxg.htm)|Dagger|Dague|libre|
|[LTLa6hS15bgHkOBi.htm](kingmaker-bestiary-items/LTLa6hS15bgHkOBi.htm)|Protection from Decapitation|Protection de la décapitation|libre|
|[luiDDR5D3oiCAEZh.htm](kingmaker-bestiary-items/luiDDR5D3oiCAEZh.htm)|Hatchet|Hachette|libre|
|[lUMpyYokJ5dTTdu9.htm](kingmaker-bestiary-items/lUMpyYokJ5dTTdu9.htm)|Blood Drain|Drain de sang|libre|
|[LXvqvVhorWXVewtI.htm](kingmaker-bestiary-items/LXvqvVhorWXVewtI.htm)|Twist Time|Remuer le temps|libre|
|[lYCCv7NRnl6ZnNjT.htm](kingmaker-bestiary-items/lYCCv7NRnl6ZnNjT.htm)|Constant Spells|Sorts constant|libre|
|[lYx7vXEHaPIDLbWp.htm](kingmaker-bestiary-items/lYx7vXEHaPIDLbWp.htm)|Sickle|Serpe|libre|
|[Lz7LqoEOMbBrTosy.htm](kingmaker-bestiary-items/Lz7LqoEOMbBrTosy.htm)|Repulsing Blow|Coup répulsif|libre|
|[LZhDKd6gpJJ5o2RO.htm](kingmaker-bestiary-items/LZhDKd6gpJJ5o2RO.htm)|Aura of Disquietude|Aura d'angoisse|libre|
|[M0O2aUtdSXT0HpSQ.htm](kingmaker-bestiary-items/M0O2aUtdSXT0HpSQ.htm)|Chieftain's Necklace|Collier du Chef|libre|
|[m4LUwAovVkqwRsiG.htm](kingmaker-bestiary-items/m4LUwAovVkqwRsiG.htm)|Vengeful Rage|Rage vengeresse|libre|
|[M5azwdU2fCtU2eR5.htm](kingmaker-bestiary-items/M5azwdU2fCtU2eR5.htm)|Gorum Lore|Connaissance de Gorum|libre|
|[m5He7mR6hyUEterZ.htm](kingmaker-bestiary-items/m5He7mR6hyUEterZ.htm)|Regeneration 30 (Deactivated by Fire or Good)|Régénération 30 (désactive par Feu ou Bon)|libre|
|[M9wyjXrmWfCD7nBk.htm](kingmaker-bestiary-items/M9wyjXrmWfCD7nBk.htm)|Blinding Beauty|Beauté aveuglante|libre|
|[MBYJQAMX2qMwwp1o.htm](kingmaker-bestiary-items/MBYJQAMX2qMwwp1o.htm)|Divine Prepared Spells|Sorts divins préparés|libre|
|[mc4qvML9Y9Vngpwo.htm](kingmaker-bestiary-items/mc4qvML9Y9Vngpwo.htm)|Thorny Vine|Liane épineuse|libre|
|[mcMNYM92nivzmq61.htm](kingmaker-bestiary-items/mcMNYM92nivzmq61.htm)|+2 Status to All Saves vs. Mental|bonus de statut de +2 aux JdS contre les effets mentaux|libre|
|[mCooSIipdGY8hP2o.htm](kingmaker-bestiary-items/mCooSIipdGY8hP2o.htm)|Living Bow|Arc vivant|libre|
|[mcvnk6mFrYyK92Mb.htm](kingmaker-bestiary-items/mcvnk6mFrYyK92Mb.htm)|Morningstar|+1,darkwood,thundering|Morgenstern en ébénite de tonnerre de frappe +1|libre|
|[MDHk8qTYvCueXiiR.htm](kingmaker-bestiary-items/MDHk8qTYvCueXiiR.htm)|Dwelling Lore|Connaissance de l'habitat|libre|
|[mDmFiG2gsRPiENrP.htm](kingmaker-bestiary-items/mDmFiG2gsRPiENrP.htm)|Warp Teleportation|Téléportation déformant|libre|
|[MeD83DvPWfOBUylm.htm](kingmaker-bestiary-items/MeD83DvPWfOBUylm.htm)|Dagger|+2,greaterStriking,spellStoring|Dague de stockage de sort de frappe supérieure +2|libre|
|[MfVHl3j1UyN4VFks.htm](kingmaker-bestiary-items/MfVHl3j1UyN4VFks.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[MgV9p8g40O0uwDjs.htm](kingmaker-bestiary-items/MgV9p8g40O0uwDjs.htm)|Wild Gaze|Regard sauvage|libre|
|[mGvIyNKk0KBZu3BK.htm](kingmaker-bestiary-items/mGvIyNKk0KBZu3BK.htm)|Aldori Dueling Sword|+2,striking|Épée de duel aldorie de frappe +2|libre|
|[MgVuD5shNkwpUCdM.htm](kingmaker-bestiary-items/MgVuD5shNkwpUCdM.htm)|Frightful Moan|Lamentation d'épouvante|libre|
|[mGzWQjNPCtlz97Cn.htm](kingmaker-bestiary-items/mGzWQjNPCtlz97Cn.htm)|Sorcerer Bloodline Spells|Sorts de lignage d'ensorceleur|libre|
|[MHLJGTn6eQAmWIUX.htm](kingmaker-bestiary-items/MHLJGTn6eQAmWIUX.htm)|True Seeing (Constant)|Vision lucide (constant)|officielle|
|[MHRiahLowofy8ek9.htm](kingmaker-bestiary-items/MHRiahLowofy8ek9.htm)|Invisibility (At Will)|Invisibilité (À volonté)|officielle|
|[mkO8uUk9bttHYIRn.htm](kingmaker-bestiary-items/mkO8uUk9bttHYIRn.htm)|Snake Fangs|Crocs de serpent|libre|
|[Mkzdftre3Tzz3aVM.htm](kingmaker-bestiary-items/Mkzdftre3Tzz3aVM.htm)|Drop From Ceiling|Tomber du plafond|libre|
|[MMamQCPSgaxFWQd4.htm](kingmaker-bestiary-items/MMamQCPSgaxFWQd4.htm)|Whip|Fouet|libre|
|[mmgcrA8q8AjJiPoF.htm](kingmaker-bestiary-items/mmgcrA8q8AjJiPoF.htm)|Shuriken|Shuriken|libre|
|[mMPWudSAzEWfl1eN.htm](kingmaker-bestiary-items/mMPWudSAzEWfl1eN.htm)|Composite Longbow|Arc long composite|libre|
|[mo5giist2lOlgVh5.htm](kingmaker-bestiary-items/mo5giist2lOlgVh5.htm)|Longsword|+1,striking,grievous|Épée longue douloureuse de frappe +1|libre|
|[moA510VYoGxOfach.htm](kingmaker-bestiary-items/moA510VYoGxOfach.htm)|Recall Lintwerth|Rappel de Lintwerth|libre|
|[MP8qtlyLZ6pRMVZl.htm](kingmaker-bestiary-items/MP8qtlyLZ6pRMVZl.htm)|Dagger|Dague|libre|
|[MQbQ4WqtcjmvDsJS.htm](kingmaker-bestiary-items/MQbQ4WqtcjmvDsJS.htm)|Seeking Shots|Tirs chercheurs|libre|
|[Mqryx0fWVqgV0RiU.htm](kingmaker-bestiary-items/Mqryx0fWVqgV0RiU.htm)|Mauler|Écharpeur|libre|
|[MqUTUiuHEWGtwSkQ.htm](kingmaker-bestiary-items/MqUTUiuHEWGtwSkQ.htm)|Instinctive Cooperation|Coopération instinctive|libre|
|[MrbeKdSngiqGczPC.htm](kingmaker-bestiary-items/MrbeKdSngiqGczPC.htm)|Glaive|Coutille|libre|
|[mSjLHeaPtyB5Sjcm.htm](kingmaker-bestiary-items/mSjLHeaPtyB5Sjcm.htm)|Fist|Poing|libre|
|[mTecTRC1ab5f2qOh.htm](kingmaker-bestiary-items/mTecTRC1ab5f2qOh.htm)|Negative Healing|Guérison négative|libre|
|[MUZm9jerelh5o9lK.htm](kingmaker-bestiary-items/MUZm9jerelh5o9lK.htm)|Royal Command|Décret royal|libre|
|[mVgSuJseTKH5wKIO.htm](kingmaker-bestiary-items/mVgSuJseTKH5wKIO.htm)|Twin Takedown|Agression jumelée|libre|
|[mwIVxTyDPXVZaEcf.htm](kingmaker-bestiary-items/mwIVxTyDPXVZaEcf.htm)|Fearsome Gaze|Regard effrayant|libre|
|[MwxI4D0saKJpksvK.htm](kingmaker-bestiary-items/MwxI4D0saKJpksvK.htm)|Shoulder Slam|Frappe d'épaule|libre|
|[MxGd3lTVgUVyx8lw.htm](kingmaker-bestiary-items/MxGd3lTVgUVyx8lw.htm)|Handwraps of Mighty Blows|+2,striking|Bandelettes de coups puissants de frappe +2|libre|
|[MxkYw0L8AzlUayt9.htm](kingmaker-bestiary-items/MxkYw0L8AzlUayt9.htm)|Rush|Ruée|libre|
|[MxQXSU7ZH7FLyYf5.htm](kingmaker-bestiary-items/MxQXSU7ZH7FLyYf5.htm)|Scythe|Serpe|libre|
|[mxTIF80CDuT8kywe.htm](kingmaker-bestiary-items/mxTIF80CDuT8kywe.htm)|Ring of Energy Resistance (Fire)|Anneau de Résistance aux énergies (feu)|libre|
|[myUDdwt1FjfMMl9p.htm](kingmaker-bestiary-items/myUDdwt1FjfMMl9p.htm)|Staff|+1|Bâton +1|libre|
|[MZGshvRJ0ZlhdQ5k.htm](kingmaker-bestiary-items/MZGshvRJ0ZlhdQ5k.htm)|Rhetorical Spell|Sort rhétorique|libre|
|[n1fjlb5osV2yl7as.htm](kingmaker-bestiary-items/n1fjlb5osV2yl7as.htm)|+4 Status to All Saves vs. Mental|bonus de statut de +4 aux JdS contre les effets mentaux|libre|
|[n1p0bJc7I91avM4S.htm](kingmaker-bestiary-items/n1p0bJc7I91avM4S.htm)|Wild Empathy|Empathie sauvage|libre|
|[N4dy1EmZsnQD8XR3.htm](kingmaker-bestiary-items/N4dy1EmZsnQD8XR3.htm)|Chill Breath|Souffle glacial|libre|
|[N4EVKfsc2JGoy6hL.htm](kingmaker-bestiary-items/N4EVKfsc2JGoy6hL.htm)|Rider Synergy|Synérgie du cavalier|libre|
|[N4FRJPLjGNaWpvxp.htm](kingmaker-bestiary-items/N4FRJPLjGNaWpvxp.htm)|Religious Symbol of Yog-Sothoth|Symbole religieux de Yog-Sothoth|libre|
|[n4uBILCtoQcDwVBY.htm](kingmaker-bestiary-items/n4uBILCtoQcDwVBY.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|libre|
|[n5KuZKOcFvPmlkze.htm](kingmaker-bestiary-items/n5KuZKOcFvPmlkze.htm)|Cleric Domain Spells|Sorts de domaine de prêtre|libre|
|[n5PTMIe2knU6k8WD.htm](kingmaker-bestiary-items/n5PTMIe2knU6k8WD.htm)|Spiked Pitfall|Fosse garnie d'épieux|libre|
|[N7BomXiXSkV8IDmE.htm](kingmaker-bestiary-items/N7BomXiXSkV8IDmE.htm)|Perpetual Hangover|Gueule-de-bois perpétuel|libre|
|[N8EKVbUAmiDxtOlF.htm](kingmaker-bestiary-items/N8EKVbUAmiDxtOlF.htm)|Sneak Attack|Attaque sournoise|libre|
|[n8TGGjTIP2jpXLiC.htm](kingmaker-bestiary-items/n8TGGjTIP2jpXLiC.htm)|Monstrous Bloom|Fleur monstrueuse|libre|
|[nC28cnIAmXUrJhdf.htm](kingmaker-bestiary-items/nC28cnIAmXUrJhdf.htm)|Rage|Rage|libre|
|[NcDY8Koc4BA35pJ3.htm](kingmaker-bestiary-items/NcDY8Koc4BA35pJ3.htm)|The Inward Flowing Source (Spellbook)|La Source fluide intérieure (Grimoire)|libre|
|[nCPtTG7EkbC5KQpL.htm](kingmaker-bestiary-items/nCPtTG7EkbC5KQpL.htm)|Furious Power Attack|Attaque en puissance furieuse|libre|
|[nGVNV8Ln1yOs1ff4.htm](kingmaker-bestiary-items/nGVNV8Ln1yOs1ff4.htm)|Scimitar|+1,striking,disrupting|Cimeterre perturbatrice de frappe +1|libre|
|[nHs2Wp0Ifqpenmxk.htm](kingmaker-bestiary-items/nHs2Wp0Ifqpenmxk.htm)|Warhammer|Marteau de guerre|libre|
|[nHt7MnXhByHOX78E.htm](kingmaker-bestiary-items/nHt7MnXhByHOX78E.htm)|Tandem Chop|Coupe en tandem|libre|
|[nkAaDbVaSoGjlqCp.htm](kingmaker-bestiary-items/nkAaDbVaSoGjlqCp.htm)|Poison Spray|Vaporisation de poison|libre|
|[NKFCPYANKZVEHAVK.htm](kingmaker-bestiary-items/NKFCPYANKZVEHAVK.htm)|No Escape|Pas d'échappatoire|libre|
|[nkFUtIxpiWKJ11ko.htm](kingmaker-bestiary-items/nkFUtIxpiWKJ11ko.htm)|Hand|Main|libre|
|[nNdV2vrrzVUlwrhp.htm](kingmaker-bestiary-items/nNdV2vrrzVUlwrhp.htm)|Fast Healing 15|Guérison accélérée 15|libre|
|[nnU6e1a4DLTuRnUV.htm](kingmaker-bestiary-items/nnU6e1a4DLTuRnUV.htm)|Academia Lore|Connaissance universitaire|libre|
|[NO9VvuwakH04tpcx.htm](kingmaker-bestiary-items/NO9VvuwakH04tpcx.htm)|Hatchet|Hachette|libre|
|[nOFWQRAuYiD4lmUi.htm](kingmaker-bestiary-items/nOFWQRAuYiD4lmUi.htm)|+1 Resilient Clothing (Explorer's)|Vêtements d'explorateur de résilience +1|libre|
|[nP9Bi95YRl4E9QMt.htm](kingmaker-bestiary-items/nP9Bi95YRl4E9QMt.htm)|Greataxe|Grande hache|libre|
|[nrmfu7IJT3ecM1M8.htm](kingmaker-bestiary-items/nrmfu7IJT3ecM1M8.htm)|Beak|Bec|libre|
|[NsbWotgycMOTGuGT.htm](kingmaker-bestiary-items/NsbWotgycMOTGuGT.htm)|Suggestion (At Will)|Suggestion (À volonté)|officielle|
|[nsDXE0HKZhrqgV54.htm](kingmaker-bestiary-items/nsDXE0HKZhrqgV54.htm)|Moon Frenzy|Frénésie lunaire|libre|
|[ntcF2Dw0m1DqvDnw.htm](kingmaker-bestiary-items/ntcF2Dw0m1DqvDnw.htm)|Dream of Rulership|Rêve de royauté|libre|
|[NTFiXa55jt87djrs.htm](kingmaker-bestiary-items/NTFiXa55jt87djrs.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[NTO1vgINd90By1LE.htm](kingmaker-bestiary-items/NTO1vgINd90By1LE.htm)|Journal|Journal|libre|
|[NTRKdsufuPkE5ERS.htm](kingmaker-bestiary-items/NTRKdsufuPkE5ERS.htm)|Aldori Parry|Parade aldorie|libre|
|[Nu5hdhS5ABZ0CZjq.htm](kingmaker-bestiary-items/Nu5hdhS5ABZ0CZjq.htm)|Twin Butchery|Double boucherie|libre|
|[nUhHPT76LVNXNYRg.htm](kingmaker-bestiary-items/nUhHPT76LVNXNYRg.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|libre|
|[NWflOL2JDt8KPMH6.htm](kingmaker-bestiary-items/NWflOL2JDt8KPMH6.htm)|Rejuvenation|Reconstruction|libre|
|[NzVLRMB9AI3b1pBg.htm](kingmaker-bestiary-items/NzVLRMB9AI3b1pBg.htm)|Tattered Robes|Robes en lambeaux|libre|
|[Nzx1vI7LkdpuIwph.htm](kingmaker-bestiary-items/Nzx1vI7LkdpuIwph.htm)|Unseen Sight|Vision invisible|libre|
|[O2EsiBYHC57XSjTz.htm](kingmaker-bestiary-items/O2EsiBYHC57XSjTz.htm)|Jaws|Mâchoires|libre|
|[o2schqbpr94UUldL.htm](kingmaker-bestiary-items/o2schqbpr94UUldL.htm)|Hatchet|Hachette|libre|
|[O3DozXqnlvka4VIK.htm](kingmaker-bestiary-items/O3DozXqnlvka4VIK.htm)|Choker of Elocution (Sylvan)|Collier d'élocution (Sylvestre)|libre|
|[o3sdERN8Ehqn7jo0.htm](kingmaker-bestiary-items/o3sdERN8Ehqn7jo0.htm)|Low-Light Vision|Vision nocturne|libre|
|[o5RWcZEKhSBVRSCd.htm](kingmaker-bestiary-items/o5RWcZEKhSBVRSCd.htm)|See Invisibility (Constant)|Détection de l'invisibilité (constant)|officielle|
|[O6kjx7rDPPICVkym.htm](kingmaker-bestiary-items/O6kjx7rDPPICVkym.htm)|Rapier|Rapière|libre|
|[o82vOevA36JTvcnU.htm](kingmaker-bestiary-items/o82vOevA36JTvcnU.htm)|Forest Lore|Connaissance de la forêt|libre|
|[oBUF4qu6Shoi0A37.htm](kingmaker-bestiary-items/oBUF4qu6Shoi0A37.htm)|Musical Instrument (Lute)|Instrument de musique (luth)|libre|
|[ocYHO8rNnbCQGplD.htm](kingmaker-bestiary-items/ocYHO8rNnbCQGplD.htm)|Bewildering Hoofbeats|Bruits de sabots déroutants|libre|
|[OenA92WNHdH1I94w.htm](kingmaker-bestiary-items/OenA92WNHdH1I94w.htm)|Conjure Daemon|Conjurer un daémon|libre|
|[OFUhWZezAxCDljSA.htm](kingmaker-bestiary-items/OFUhWZezAxCDljSA.htm)|Dogslicer|+1,striking|Tranchechien de frappe +1|libre|
|[OGDCzsyhNsf5h3j3.htm](kingmaker-bestiary-items/OGDCzsyhNsf5h3j3.htm)|Artisan's Tools (Woodworker's)|Outils d'artisan (menuiserie)|libre|
|[ogXJyvypuEP8U1hd.htm](kingmaker-bestiary-items/ogXJyvypuEP8U1hd.htm)|Bloom Venom|Venin de la Fleur|libre|
|[ohbFrIxS5WF1abVb.htm](kingmaker-bestiary-items/ohbFrIxS5WF1abVb.htm)|Dagger|Dague|libre|
|[oHxfnh9KIO2dQcTN.htm](kingmaker-bestiary-items/oHxfnh9KIO2dQcTN.htm)|Darkvision|Vision dans le noir|libre|
|[OiCu7wtLBEPD8y4D.htm](kingmaker-bestiary-items/OiCu7wtLBEPD8y4D.htm)|Spear|Lance|libre|
|[oK1SNnCrHiQzSXJH.htm](kingmaker-bestiary-items/oK1SNnCrHiQzSXJH.htm)|True Seeing (Constant)|Vision lucide (constant)|officielle|
|[oka4i9kQ0yTjUsVF.htm](kingmaker-bestiary-items/oka4i9kQ0yTjUsVF.htm)|Trident|Trident|libre|
|[okVVkEEDYOrrlhfS.htm](kingmaker-bestiary-items/okVVkEEDYOrrlhfS.htm)|Cleric Domain Spells|Sorts de domaine de prêtre|libre|
|[OLgE1eKYfChrP7ip.htm](kingmaker-bestiary-items/OLgE1eKYfChrP7ip.htm)|Ring of Energy Resistance (Fire)|Anneau de résistance à l'énergie (feu)|libre|
|[oM6MOLJCVEE8WTTc.htm](kingmaker-bestiary-items/oM6MOLJCVEE8WTTc.htm)|Speak with Animals (Constant)|Communication avec les animaux (constant)|officielle|
|[oMIhKijss8NvtV5O.htm](kingmaker-bestiary-items/oMIhKijss8NvtV5O.htm)|Sickle|cold-iron|Serpe en fer froid|libre|
|[on7BQSeRUgbsFSSE.htm](kingmaker-bestiary-items/on7BQSeRUgbsFSSE.htm)|Knockdown|Renversement|libre|
|[onRMNTdGzo2Qq4rE.htm](kingmaker-bestiary-items/onRMNTdGzo2Qq4rE.htm)|Sneak Attack|Attaque sournoise|libre|
|[oORyGKlMVkIFWGHJ.htm](kingmaker-bestiary-items/oORyGKlMVkIFWGHJ.htm)|Greatsword|Épée à deux mains|libre|
|[OQrd4dnZRVS4hQEO.htm](kingmaker-bestiary-items/OQrd4dnZRVS4hQEO.htm)|Maestro's Instrument (Lesser) (Lute)|Instrument de maestro inférieur (luth)|libre|
|[OQv2VZ49tvgCKDhW.htm](kingmaker-bestiary-items/OQv2VZ49tvgCKDhW.htm)|First World Lore|Connaissance du Premier monde|libre|
|[OqVIaOEX3mbFTW9O.htm](kingmaker-bestiary-items/OqVIaOEX3mbFTW9O.htm)|Arcane Spontaneous Spells|Sorts arcaniques spontanés|libre|
|[orJLwvKxUPCxgkWs.htm](kingmaker-bestiary-items/orJLwvKxUPCxgkWs.htm)|Monarch's Horn|Cor du Monarque de la chasse sauvage|libre|
|[OSCP7JLtSYLDVYrO.htm](kingmaker-bestiary-items/OSCP7JLtSYLDVYrO.htm)|Jaws|Mâchoires|libre|
|[OtqpSI4VXEPSxQox.htm](kingmaker-bestiary-items/OtqpSI4VXEPSxQox.htm)|Intimidating Prowess|Prouesse intimidante|libre|
|[otVXldOceOZtEV2x.htm](kingmaker-bestiary-items/otVXldOceOZtEV2x.htm)|Frightening Rattle|Cascabelle effrayante|libre|
|[oVDdlUuHM54d6gAP.htm](kingmaker-bestiary-items/oVDdlUuHM54d6gAP.htm)|At-Will Spells|Sorts à volonté|libre|
|[OvgHtZCLvGtWHyg9.htm](kingmaker-bestiary-items/OvgHtZCLvGtWHyg9.htm)|Raging Resistance|Résistance enragée|libre|
|[OvqcWkhFk95fFEEX.htm](kingmaker-bestiary-items/OvqcWkhFk95fFEEX.htm)|Shoulder Spikes|Pointes d'épaule|libre|
|[owZSVQs8IkCQjNLE.htm](kingmaker-bestiary-items/owZSVQs8IkCQjNLE.htm)|Composite Longbow|Arc long composite|libre|
|[OyvZeoS5KOjNU1P7.htm](kingmaker-bestiary-items/OyvZeoS5KOjNU1P7.htm)|Heavy Wooden Shield|Bouclier en bois lourd|libre|
|[OZRGnwXPUQq6AsKe.htm](kingmaker-bestiary-items/OZRGnwXPUQq6AsKe.htm)|Force Bolt|Projectile de force|libre|
|[P143e4Kl2GktAt4b.htm](kingmaker-bestiary-items/P143e4Kl2GktAt4b.htm)|Greatsword Critical Specialization|Effet critique spécialisé d'Épée longue|libre|
|[p3LP4p7cOQkgP2pl.htm](kingmaker-bestiary-items/p3LP4p7cOQkgP2pl.htm)|Befuddle|Embrouiller|libre|
|[p4u7kb7SoMyLXv5z.htm](kingmaker-bestiary-items/p4u7kb7SoMyLXv5z.htm)|Wand of Paralyze (Level 3)|Baguette de Paralysie (Niveau 3)|libre|
|[P5jo7Pf9aXvKHvel.htm](kingmaker-bestiary-items/P5jo7Pf9aXvKHvel.htm)|Unseen Sight|Vision invisible|libre|
|[P65Jvc5wODFPZ00e.htm](kingmaker-bestiary-items/P65Jvc5wODFPZ00e.htm)|Dagger|+1,striking,wounding|Dague sanglante de frappe +1|libre|
|[pahKe1LJWdfkKj6M.htm](kingmaker-bestiary-items/pahKe1LJWdfkKj6M.htm)|Mind Reading (At Will)|Lecture des pensées (À volonté)|officielle|
|[paPaJBwymUtXLyYF.htm](kingmaker-bestiary-items/paPaJBwymUtXLyYF.htm)|Whip|Fouet|libre|
|[PBGOOwAe8HedCuGy.htm](kingmaker-bestiary-items/PBGOOwAe8HedCuGy.htm)|Longsword|Épée longue|libre|
|[pc3Nk8nioNXX1wuM.htm](kingmaker-bestiary-items/pc3Nk8nioNXX1wuM.htm)|Flash of Insight|Intuition fulgurante|libre|
|[pF8Ngkhp4Zxkwqhd.htm](kingmaker-bestiary-items/pF8Ngkhp4Zxkwqhd.htm)|+2 Glamered Fortification Greater Resilient Leather Armor|Armure en cuir de mimétisme de fortification supérieure +2|libre|
|[pfic0QiF1jUkDVwR.htm](kingmaker-bestiary-items/pfic0QiF1jUkDVwR.htm)|River Lore|Connaissance des rivières|libre|
|[Pfn9Ff6lgtXDae3e.htm](kingmaker-bestiary-items/Pfn9Ff6lgtXDae3e.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|libre|
|[pg54945zKnAqECAM.htm](kingmaker-bestiary-items/pg54945zKnAqECAM.htm)|Darkvision|Vision dans le noir|libre|
|[PG62mZgFRnBReYcw.htm](kingmaker-bestiary-items/PG62mZgFRnBReYcw.htm)|Word of Recall|Mot de rappel|libre|
|[PhsmxJZ4aqEowjOn.htm](kingmaker-bestiary-items/PhsmxJZ4aqEowjOn.htm)|Claws|Griffes|libre|
|[pIQHLI7rmcBAcJql.htm](kingmaker-bestiary-items/pIQHLI7rmcBAcJql.htm)|Wand of Heal (Level 1)|Baguette de guérison (Niveau 1)|libre|
|[PLl0lrfdaaqgezrn.htm](kingmaker-bestiary-items/PLl0lrfdaaqgezrn.htm)|Clothing and Crown made of thorny vines and weeds|Vêtement et couronne fait de lianes épineuses et de mauvaises herbes|libre|
|[pllCGrR7E0eAP35u.htm](kingmaker-bestiary-items/pllCGrR7E0eAP35u.htm)|Dagger|Dague|libre|
|[PmFUHtH32XT97IlJ.htm](kingmaker-bestiary-items/PmFUHtH32XT97IlJ.htm)|Warfare Lore|Connaissance de la guerre|libre|
|[pMKjMXZobeZ6zFPm.htm](kingmaker-bestiary-items/pMKjMXZobeZ6zFPm.htm)|Primal Prepared Spells|Sorts primordiaux préparés|libre|
|[pmVLSDQuvnEqYH5y.htm](kingmaker-bestiary-items/pmVLSDQuvnEqYH5y.htm)|Fey Lore|Connaissance des feys|libre|
|[PmwxtRI969adfqM7.htm](kingmaker-bestiary-items/PmwxtRI969adfqM7.htm)|Key Ring|Porte clés|libre|
|[pNlVk6Eq16L9OBYt.htm](kingmaker-bestiary-items/pNlVk6Eq16L9OBYt.htm)|Tongues (Constant)|Don des langues (constant)|officielle|
|[pO1coGfLkTGz4KtP.htm](kingmaker-bestiary-items/pO1coGfLkTGz4KtP.htm)|Speak with Plants (Constant)|Communication avec les plantes (Constant)|libre|
|[po8aw9EkLZ78IlVy.htm](kingmaker-bestiary-items/po8aw9EkLZ78IlVy.htm)|Hatchet|Hachette|libre|
|[pPppryZClUfw47fK.htm](kingmaker-bestiary-items/pPppryZClUfw47fK.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|libre|
|[pQowCb4EVsOhUgK2.htm](kingmaker-bestiary-items/pQowCb4EVsOhUgK2.htm)|Breath Weapon|Arme de souffle|libre|
|[PqqO1TraA4ak7FIy.htm](kingmaker-bestiary-items/PqqO1TraA4ak7FIy.htm)|Divine Focus Spells|Sorts divins focalisés|libre|
|[pQr1ecKdOpSlfSnw.htm](kingmaker-bestiary-items/pQr1ecKdOpSlfSnw.htm)|Spiteful Command|Ordre malveillant|libre|
|[pQrymwjXMI3vqcOk.htm](kingmaker-bestiary-items/pQrymwjXMI3vqcOk.htm)|Negative Healing|Guérison négative|libre|
|[pu63PF0pspvou1kE.htm](kingmaker-bestiary-items/pu63PF0pspvou1kE.htm)|Whiplashing Tail|Queue fouet|libre|
|[pu7iTks4TUgGpglG.htm](kingmaker-bestiary-items/pu7iTks4TUgGpglG.htm)|First World Lore|Connaissance du Premier monde|libre|
|[PueAKB1MJBXmbhHV.htm](kingmaker-bestiary-items/PueAKB1MJBXmbhHV.htm)|Formation Fighter|Formation de guerrier|libre|
|[pugi4AZBmL8aH1oF.htm](kingmaker-bestiary-items/pugi4AZBmL8aH1oF.htm)|Vengeful Sting|Piqûre vindificative|libre|
|[pWFsPT5b6xUAwvpD.htm](kingmaker-bestiary-items/pWFsPT5b6xUAwvpD.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[PxCKKRzmpOzQRHlN.htm](kingmaker-bestiary-items/PxCKKRzmpOzQRHlN.htm)|Tiger Empathy|Empathie avec les tigres|libre|
|[pyyskEw6HoHX0DBp.htm](kingmaker-bestiary-items/pyyskEw6HoHX0DBp.htm)|Site Bound|Lié à un site|libre|
|[PzkoaPhLG2JLClMM.htm](kingmaker-bestiary-items/PzkoaPhLG2JLClMM.htm)|Longsword|Épée longue|libre|
|[q0qbUmAXaCjmrWfl.htm](kingmaker-bestiary-items/q0qbUmAXaCjmrWfl.htm)|Water Walk (Constant) (Self Only)|Marche sur l'eau (constant) (soi-même)|libre|
|[Q0WHpEJkqbeT0KZr.htm](kingmaker-bestiary-items/Q0WHpEJkqbeT0KZr.htm)|Waves of Fear|Vagues de terreur|libre|
|[q1z16jklLBVTY0A8.htm](kingmaker-bestiary-items/q1z16jklLBVTY0A8.htm)|Glaive|Coutille|libre|
|[Q2aaq7yspaPD9kAk.htm](kingmaker-bestiary-items/Q2aaq7yspaPD9kAk.htm)|Nobility Lore|Connaissance noblesse|libre|
|[q2Yeh8EgyWJTHR2E.htm](kingmaker-bestiary-items/q2Yeh8EgyWJTHR2E.htm)|Greataxe|+1|Grande hache +1|libre|
|[Q3rzVNhMmKJUv8Hd.htm](kingmaker-bestiary-items/Q3rzVNhMmKJUv8Hd.htm)|Rusted Iron Holy Symbol of Erastil|Symbole sacré d'Érastil en fer rouillé|libre|
|[q5MeF25mWxrHIqg3.htm](kingmaker-bestiary-items/q5MeF25mWxrHIqg3.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[Q6aECCBC8XJs3Eke.htm](kingmaker-bestiary-items/Q6aECCBC8XJs3Eke.htm)|Water Breathing (Constant)|Respiration aquatique (Constant)|libre|
|[q6QpDgIvY1kfKSWc.htm](kingmaker-bestiary-items/q6QpDgIvY1kfKSWc.htm)|Dagger|Dague|libre|
|[q6ZsHR2exUj0eCp7.htm](kingmaker-bestiary-items/q6ZsHR2exUj0eCp7.htm)|Aldori Dueling Sword|Épée de duel aldorie|libre|
|[Q78tMeRUfO7vehcO.htm](kingmaker-bestiary-items/Q78tMeRUfO7vehcO.htm)|Regeneration 45 (Deactivated by Fire or Negative)|Régénération 45 (désactivé par feu et négatif)|libre|
|[Q7g8pfqUSNczyqNo.htm](kingmaker-bestiary-items/Q7g8pfqUSNczyqNo.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[q7jtffxL9IJqb4Pq.htm](kingmaker-bestiary-items/q7jtffxL9IJqb4Pq.htm)|Change Shape|Changement de forme|libre|
|[Q8CDVoVPdXpwkoEA.htm](kingmaker-bestiary-items/Q8CDVoVPdXpwkoEA.htm)|Constant Spells|Sorts constants|libre|
|[qAjzrkRNotMQvgjw.htm](kingmaker-bestiary-items/qAjzrkRNotMQvgjw.htm)|Infuse Arrow|Imprégner la flèche|libre|
|[QB62e4FfQvymENul.htm](kingmaker-bestiary-items/QB62e4FfQvymENul.htm)|Tail|Queue|libre|
|[QDw6f81ZeMuIueQP.htm](kingmaker-bestiary-items/QDw6f81ZeMuIueQP.htm)|Tail|Queue|libre|
|[QebQalR7fIq41wKr.htm](kingmaker-bestiary-items/QebQalR7fIq41wKr.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[qEenLpsXQfSrqeJh.htm](kingmaker-bestiary-items/qEenLpsXQfSrqeJh.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[qesdwQLA0OfebLki.htm](kingmaker-bestiary-items/qesdwQLA0OfebLki.htm)|Intimidation|Intimidation|libre|
|[Qg6K3SNkS9IdmxUm.htm](kingmaker-bestiary-items/Qg6K3SNkS9IdmxUm.htm)|Blessed Life|Vie bénie|libre|
|[qiU4iKX6FIHuopg7.htm](kingmaker-bestiary-items/qiU4iKX6FIHuopg7.htm)|Sting|Orties|libre|
|[QLpCft5YSm98kbx4.htm](kingmaker-bestiary-items/QLpCft5YSm98kbx4.htm)|Speak with Animals (At Will) (Arthropods Only)|Communication avec les animaux (à volonté, arthropodes uniquement)|officielle|
|[QLqI8GHyMhlnlL3h.htm](kingmaker-bestiary-items/QLqI8GHyMhlnlL3h.htm)|Rock|Rocher|libre|
|[qn7XE1djceSKfkuH.htm](kingmaker-bestiary-items/qn7XE1djceSKfkuH.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[qnW0tYvY2eKqxqcI.htm](kingmaker-bestiary-items/qnW0tYvY2eKqxqcI.htm)|+1 Glamered Resilient reastplate|Cuirasse de mimétisme de résilience +1|libre|
|[QQaSJEmqMeqq6kd9.htm](kingmaker-bestiary-items/QQaSJEmqMeqq6kd9.htm)|Lurker Claw|Griffe du rôdeur|libre|
|[qQD7sRQC5iCPloAd.htm](kingmaker-bestiary-items/qQD7sRQC5iCPloAd.htm)|Ectoplasmic Maneuver|Manoeuvre ectoplasmique|libre|
|[qQY3snkHHYYEQDxW.htm](kingmaker-bestiary-items/qQY3snkHHYYEQDxW.htm)|Primal Spontaneous Spells|Sorts primordiaux spontanés|libre|
|[QrCROiN8OwsCLDUj.htm](kingmaker-bestiary-items/QrCROiN8OwsCLDUj.htm)|Skirmish Strike|Frappe d'escarmouche|libre|
|[QryAkNRvJCn0uaSN.htm](kingmaker-bestiary-items/QryAkNRvJCn0uaSN.htm)|Dagger|Dague|libre|
|[QsQBz0EKCXO9IzXT.htm](kingmaker-bestiary-items/QsQBz0EKCXO9IzXT.htm)|Rapier|Rapière|libre|
|[qTKs06NZl2BkZiYP.htm](kingmaker-bestiary-items/qTKs06NZl2BkZiYP.htm)|+1 Status to All Saves vs. Positive|bonus de statut de +1 aux JdS contre les effets positifs|libre|
|[qVOjbIdihxei6lTm.htm](kingmaker-bestiary-items/qVOjbIdihxei6lTm.htm)|Composite Longbow|Arc long composite|libre|
|[qvps9GmZSlGuhdrK.htm](kingmaker-bestiary-items/qvps9GmZSlGuhdrK.htm)|Royal Outfit|Tenue royale|libre|
|[QW0dGg59W0r9tJtU.htm](kingmaker-bestiary-items/QW0dGg59W0r9tJtU.htm)|Stealth|Discrétion|libre|
|[QXYemYqhU6zdoBsz.htm](kingmaker-bestiary-items/QXYemYqhU6zdoBsz.htm)|Reach Spell|Sort éloigné|libre|
|[QXyRODDmF4tozV5P.htm](kingmaker-bestiary-items/QXyRODDmF4tozV5P.htm)|Longsword|Épée longue|libre|
|[qYTvq8RiCpkT6wG7.htm](kingmaker-bestiary-items/qYTvq8RiCpkT6wG7.htm)|Dagger|Dague|libre|
|[qztKctN0HIhpmOSy.htm](kingmaker-bestiary-items/qztKctN0HIhpmOSy.htm)|Low-Light Vision|Vision nocturne|libre|
|[r0B1Lotd2T85M2u2.htm](kingmaker-bestiary-items/r0B1Lotd2T85M2u2.htm)|Low-Light Vision|Vision nocturne|libre|
|[R2ZrKwkuQb3XIG4z.htm](kingmaker-bestiary-items/R2ZrKwkuQb3XIG4z.htm)|Dagger|Dague|libre|
|[r4ZnY6A2rOJTCSSI.htm](kingmaker-bestiary-items/r4ZnY6A2rOJTCSSI.htm)|Battle Axe|+1,striking|Hache d'armes de frappe +1|libre|
|[R6qqK2TgPEbgEcH0.htm](kingmaker-bestiary-items/R6qqK2TgPEbgEcH0.htm)|Defensive Burst|Explosion défensive|libre|
|[r9vIYF8LhOmQFT2b.htm](kingmaker-bestiary-items/r9vIYF8LhOmQFT2b.htm)|Dagger|+1|Dague +1|officielle|
|[rA1w7Wi3HDq5IpD3.htm](kingmaker-bestiary-items/rA1w7Wi3HDq5IpD3.htm)|Longsword|+1,striking,thundering|Épée longue de tonnerre de frappe +1|libre|
|[Ral7Zi8ooe0f3emu.htm](kingmaker-bestiary-items/Ral7Zi8ooe0f3emu.htm)|Moon Frenzy|Frénésie lunaire|libre|
|[rbZIdY30oD5udoGi.htm](kingmaker-bestiary-items/rbZIdY30oD5udoGi.htm)|Occult Focus Spells|Sorts occultes focalisés|libre|
|[rcNrrQtcdNppDhtd.htm](kingmaker-bestiary-items/rcNrrQtcdNppDhtd.htm)|Feasting Bite|Morsure vorace|libre|
|[rdf4aOsP7jywGEIC.htm](kingmaker-bestiary-items/rdf4aOsP7jywGEIC.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[rf2VoAlQi6dlp1Yq.htm](kingmaker-bestiary-items/rf2VoAlQi6dlp1Yq.htm)|Silver Stag Lord Amulet|Amulette du Seigneur Cerf en argent|libre|
|[RflctXAVkddCvSeq.htm](kingmaker-bestiary-items/RflctXAVkddCvSeq.htm)|Jaws|Mâchoires|libre|
|[rkdBPs8QgQ9Tfvpj.htm](kingmaker-bestiary-items/rkdBPs8QgQ9Tfvpj.htm)|Ovinrbaane|Ovinrbaane|libre|
|[rl3BJy5yXHIijmZz.htm](kingmaker-bestiary-items/rl3BJy5yXHIijmZz.htm)|Darkvision|Vision dans le noir|libre|
|[RLutvx6GldUXGhjl.htm](kingmaker-bestiary-items/RLutvx6GldUXGhjl.htm)|Twin Parry|Parade jumelée|libre|
|[rlwsvNyOSTobwhA9.htm](kingmaker-bestiary-items/rlwsvNyOSTobwhA9.htm)|Centipede Swarm Venom|Venin de nuée de mille-pattes|libre|
|[rNPNkpT1CEzgR8Ry.htm](kingmaker-bestiary-items/rNPNkpT1CEzgR8Ry.htm)|Unnerving Prowess|Prouesse désarçonnante|libre|
|[rnQSJr976QIURT9w.htm](kingmaker-bestiary-items/rnQSJr976QIURT9w.htm)|Songstrike|Frappe de chant|libre|
|[RNxWSJIolrbdRbNy.htm](kingmaker-bestiary-items/RNxWSJIolrbdRbNy.htm)|Bardic Lore|Connaissance bardique|libre|
|[roURyHTV2DxU7WTQ.htm](kingmaker-bestiary-items/roURyHTV2DxU7WTQ.htm)|Darkvision|Vision dans le noir|libre|
|[rTLmI9nr5Sji2tZD.htm](kingmaker-bestiary-items/rTLmI9nr5Sji2tZD.htm)|Axe Critical Specialization|Critique spécialisé de la Hache|libre|
|[rtPLskLwq9ldQzDC.htm](kingmaker-bestiary-items/rtPLskLwq9ldQzDC.htm)|Scalding Burst|Éruption bouillante|libre|
|[rwJegRqmKknX4v9P.htm](kingmaker-bestiary-items/rwJegRqmKknX4v9P.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[rwoTbwYfMVqkiolf.htm](kingmaker-bestiary-items/rwoTbwYfMVqkiolf.htm)|Tongues (Tongues)|Don des langues|libre|
|[rWVM81zPBmywUXvF.htm](kingmaker-bestiary-items/rWVM81zPBmywUXvF.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|libre|
|[RXMeyIF1blCFYaFi.htm](kingmaker-bestiary-items/RXMeyIF1blCFYaFi.htm)|Darkvision|Vision dans le noir|libre|
|[RyEkbNlf2v3XdUOC.htm](kingmaker-bestiary-items/RyEkbNlf2v3XdUOC.htm)|Filthy Rags|Oripeaux dégoûtant|libre|
|[rZftbooG5W57BA0g.htm](kingmaker-bestiary-items/rZftbooG5W57BA0g.htm)|Divine Prepared Spells|Sorts divins préparés|libre|
|[S01l0m7wdPHytTG5.htm](kingmaker-bestiary-items/S01l0m7wdPHytTG5.htm)|+1 Resilient Breastplate|Cuirasse de résilience +1|officielle|
|[S0kF01v0eYuEyyQj.htm](kingmaker-bestiary-items/S0kF01v0eYuEyyQj.htm)|Charm (At Will)|Charme (À volonté)|officielle|
|[S2bjckIBFgZGuliH.htm](kingmaker-bestiary-items/S2bjckIBFgZGuliH.htm)|Ogre Hook|Crochet d'ogre|libre|
|[s2robi6KwZM7XeVw.htm](kingmaker-bestiary-items/s2robi6KwZM7XeVw.htm)|Falling Portcullis|Herse tombante|libre|
|[S3gLjkwIbPv8hrwC.htm](kingmaker-bestiary-items/S3gLjkwIbPv8hrwC.htm)|True Seeing (Constant)|Vision lucide (constant)|officielle|
|[S6sguxBDLkRhBNAS.htm](kingmaker-bestiary-items/S6sguxBDLkRhBNAS.htm)|Divine Prepared Spells|Sorts divins préparés|libre|
|[S9OYItI2DPSsb4Za.htm](kingmaker-bestiary-items/S9OYItI2DPSsb4Za.htm)|Switch Fables|Changer de Fable|libre|
|[sAPqpw1AmafiS0qn.htm](kingmaker-bestiary-items/sAPqpw1AmafiS0qn.htm)|Palace Key|Clé du Palais|libre|
|[sCOLNwAp0vM1QRy9.htm](kingmaker-bestiary-items/sCOLNwAp0vM1QRy9.htm)|Baleful Gaze|Regard sinistre|libre|
|[Sd3o6E29DT50HqQ5.htm](kingmaker-bestiary-items/Sd3o6E29DT50HqQ5.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[sED3umw26WGHjqa6.htm](kingmaker-bestiary-items/sED3umw26WGHjqa6.htm)|Consume Memories|Consommation de souvenirs|libre|
|[sHdlA1TMLwvXBRX0.htm](kingmaker-bestiary-items/sHdlA1TMLwvXBRX0.htm)|Composite Shortbow|Arc court composite|libre|
|[sIdAMHJe8g1PQUF7.htm](kingmaker-bestiary-items/sIdAMHJe8g1PQUF7.htm)|Low-Light Vision|Vision nocturne|libre|
|[SIvuOyYTHG1nqVDM.htm](kingmaker-bestiary-items/SIvuOyYTHG1nqVDM.htm)|Telekinetic Haul (At Will)|Transport télékinésique (À volonté)|officielle|
|[SKOscQwReKF5ZqAY.htm](kingmaker-bestiary-items/SKOscQwReKF5ZqAY.htm)|Maming Chop|Hachette mutilante|libre|
|[SMqPkvP3sngKVR0q.htm](kingmaker-bestiary-items/SMqPkvP3sngKVR0q.htm)|Giant Wasp Venom (On Darts)|Venin de guêpes géantes (sur les fléchettes)|libre|
|[smt8XqXd55pl6Gwi.htm](kingmaker-bestiary-items/smt8XqXd55pl6Gwi.htm)|Clamp Shut|Fermeture|libre|
|[Sn0vYKbSWgQmv5g7.htm](kingmaker-bestiary-items/Sn0vYKbSWgQmv5g7.htm)|Cleric Domain Spells|Sorts de domaine de prêtre|libre|
|[SnhDyvqRiwCT2xFp.htm](kingmaker-bestiary-items/SnhDyvqRiwCT2xFp.htm)|Greatsword|+1,striking|Épée à deux mains de frappe +1|libre|
|[snWh3FVKpgmQAHQz.htm](kingmaker-bestiary-items/snWh3FVKpgmQAHQz.htm)|Disfigure|Défigurer|libre|
|[spF1Kzai1HQbME5t.htm](kingmaker-bestiary-items/spF1Kzai1HQbME5t.htm)|Bloom Regeneration|Régénération de la Fleur|libre|
|[SPiKrNADm6u2qbhl.htm](kingmaker-bestiary-items/SPiKrNADm6u2qbhl.htm)|Triumphant Roar|Rugissement triomphant|libre|
|[SpVGiNHO8YKLWtpn.htm](kingmaker-bestiary-items/SpVGiNHO8YKLWtpn.htm)|Quick Alchemy|Alchimie rapide|libre|
|[SQ4DUNap1xbzs5Sq.htm](kingmaker-bestiary-items/SQ4DUNap1xbzs5Sq.htm)|Jaws|Mâchoires|libre|
|[Sq7PsPkZ8LUfigSz.htm](kingmaker-bestiary-items/Sq7PsPkZ8LUfigSz.htm)|+1 Resilient Studded Leather Armor|Armure en cuir clouté de résilience +1|officielle|
|[SqomM8yPUdX1yPY8.htm](kingmaker-bestiary-items/SqomM8yPUdX1yPY8.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[ssgy6SBt7L8uYlUi.htm](kingmaker-bestiary-items/ssgy6SBt7L8uYlUi.htm)|Arcane Innate Spells|Sorts arcaniques innés|libre|
|[sT3ZX2Zurexp4hLH.htm](kingmaker-bestiary-items/sT3ZX2Zurexp4hLH.htm)|Steady Spellcasting|Incantation fiable|libre|
|[sTjHob8UlAoUPu20.htm](kingmaker-bestiary-items/sTjHob8UlAoUPu20.htm)|Yog-Sothoth Lore|Connaissance de Yog-Sothoth|libre|
|[sU4JRJE89RWZ5Z6a.htm](kingmaker-bestiary-items/sU4JRJE89RWZ5Z6a.htm)|Scent (Precise) 120 feet|Odorat 36 m (précis)|libre|
|[sUGKCp2vAoGiIFn9.htm](kingmaker-bestiary-items/sUGKCp2vAoGiIFn9.htm)|Oathbow|+2,striking|Arc du serment de frappe +2|libre|
|[SviievXlIEecohMT.htm](kingmaker-bestiary-items/SviievXlIEecohMT.htm)|Glaive|+2,greaterStriking|Coutille de frappe supérieure +2|libre|
|[sw2mWVFaQb93J9el.htm](kingmaker-bestiary-items/sw2mWVFaQb93J9el.htm)|Forager|Glaneur|libre|
|[sWBDDbGvVypQmjhH.htm](kingmaker-bestiary-items/sWBDDbGvVypQmjhH.htm)|Robes|Robes|libre|
|[swHSxsAoATAgMMX2.htm](kingmaker-bestiary-items/swHSxsAoATAgMMX2.htm)|Ride Tick|Chevaucher Grattedos|libre|
|[Swn94Gh9zgVIcHmO.htm](kingmaker-bestiary-items/Swn94Gh9zgVIcHmO.htm)|Ankle Bite|Morsure de cheville|libre|
|[SYMyLr9FwZW5kVcr.htm](kingmaker-bestiary-items/SYMyLr9FwZW5kVcr.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[SzjP0fTFs7v3U74Q.htm](kingmaker-bestiary-items/SzjP0fTFs7v3U74Q.htm)|Composite Shortbow|+1,striking|Arc court composite de Frappe +1|libre|
|[SZOur85Nka45xVe7.htm](kingmaker-bestiary-items/SZOur85Nka45xVe7.htm)|Frightful Moan|Lamentation d'épouvante|libre|
|[T0IRXqqR5Zr2FXsp.htm](kingmaker-bestiary-items/T0IRXqqR5Zr2FXsp.htm)|Negative Healing|Guérison négative|libre|
|[T1dMCJU34dJYVgZw.htm](kingmaker-bestiary-items/T1dMCJU34dJYVgZw.htm)|Club|Gourdin|libre|
|[T1JhJ4gf00b500cI.htm](kingmaker-bestiary-items/T1JhJ4gf00b500cI.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[T2CxMAShv0ikuq5z.htm](kingmaker-bestiary-items/T2CxMAShv0ikuq5z.htm)|Ranseur|+1|Corsèque +1|libre|
|[T3OviChVRRywo6sB.htm](kingmaker-bestiary-items/T3OviChVRRywo6sB.htm)|Mocking Laughter|Rire moqueur|libre|
|[t49UcMd23mTLwrj5.htm](kingmaker-bestiary-items/t49UcMd23mTLwrj5.htm)|Paralyzing Touch|Contact paralysant|libre|
|[t4BxYpWMm3lEqjxG.htm](kingmaker-bestiary-items/t4BxYpWMm3lEqjxG.htm)|Occult Spontaneous Spells|Sorts occulte spontannés|libre|
|[T5dyuJhTrUcC9fqv.htm](kingmaker-bestiary-items/T5dyuJhTrUcC9fqv.htm)|Claw|Griffe|libre|
|[T9wwE2uJLrI8NxyM.htm](kingmaker-bestiary-items/T9wwE2uJLrI8NxyM.htm)|Bear Trap|Piège à ours|libre|
|[taxWERmxNkRViJLR.htm](kingmaker-bestiary-items/taxWERmxNkRViJLR.htm)|Greataxe|Grande hache|libre|
|[TCbRZkzWxt6vXhCx.htm](kingmaker-bestiary-items/TCbRZkzWxt6vXhCx.htm)|+2 Status to All Saves vs. Fear Effects|bonus de statut de +2 aux JdS contre les effets de peur|libre|
|[tDlyF5IZNsYAYYhP.htm](kingmaker-bestiary-items/tDlyF5IZNsYAYYhP.htm)|Thrown Object|Objet lancé|libre|
|[TGbnXBgCIKg1wFwT.htm](kingmaker-bestiary-items/TGbnXBgCIKg1wFwT.htm)|Targeting Shot|Tir de ciblage|libre|
|[TgxTGQ9ZPvKuOKzP.htm](kingmaker-bestiary-items/TgxTGQ9ZPvKuOKzP.htm)|Elixir of Life (Major) (Infused)|Élixir de vie majeur (imprégné)|libre|
|[thieXYs44gA9P3Uo.htm](kingmaker-bestiary-items/thieXYs44gA9P3Uo.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[TILteUNBjDkjtfg6.htm](kingmaker-bestiary-items/TILteUNBjDkjtfg6.htm)|Inspiration|Inspiration|libre|
|[TIWU41UBtl2ksfmx.htm](kingmaker-bestiary-items/TIWU41UBtl2ksfmx.htm)|Twin Riposte|Riposte jumelée|libre|
|[tJSXokhRWx6GoMJe.htm](kingmaker-bestiary-items/tJSXokhRWx6GoMJe.htm)|Bleed for Lamashtu|Saignée pour Lamashtu|libre|
|[tKaxQW2SwxusRLDJ.htm](kingmaker-bestiary-items/tKaxQW2SwxusRLDJ.htm)|Mace|+1,striking,frost|Masse d'armes de froid de frappe +1|libre|
|[tKFw0mI1NIfpLYBn.htm](kingmaker-bestiary-items/tKFw0mI1NIfpLYBn.htm)|Unbalancing Blow|Coup déséquilibrant|libre|
|[tkP4RVEwAR4aHYbk.htm](kingmaker-bestiary-items/tkP4RVEwAR4aHYbk.htm)|Darkvision|Vision dans la noir|libre|
|[tqOuI2ayN5VhcNyW.htm](kingmaker-bestiary-items/tqOuI2ayN5VhcNyW.htm)|Hand Crossbow|Arbalète de poing|libre|
|[TqvPH7FdjdwawFbW.htm](kingmaker-bestiary-items/TqvPH7FdjdwawFbW.htm)|Shield Block|Blocage au bouclier|libre|
|[TR472pgOTxUJrN9B.htm](kingmaker-bestiary-items/TR472pgOTxUJrN9B.htm)|Club|Gourdin|libre|
|[TSDKiQYnEhZxGO3K.htm](kingmaker-bestiary-items/TSDKiQYnEhZxGO3K.htm)|Warfare Lore|Connaissance de la guerre|libre|
|[tsZqAzdWTSJww5o9.htm](kingmaker-bestiary-items/tsZqAzdWTSJww5o9.htm)|First World Lore|Connaissance du Premier monde|libre|
|[tt77yIY913w4s6Qb.htm](kingmaker-bestiary-items/tt77yIY913w4s6Qb.htm)|Rod of Razors|+2,greaterStriking,adamantine|Bâton de rasoirs en adamantine de frappe supérieure +2|libre|
|[ttXiy5Zys3PzCa8f.htm](kingmaker-bestiary-items/ttXiy5Zys3PzCa8f.htm)|Tortuous Touch|Caresse de torture|libre|
|[TUJZ5ff9G3KgBsCN.htm](kingmaker-bestiary-items/TUJZ5ff9G3KgBsCN.htm)|Darkvision|Vision dans le noir|libre|
|[TvcbacSr4PLH15CQ.htm](kingmaker-bestiary-items/TvcbacSr4PLH15CQ.htm)|Curse of the Werewolf|Malédiction du loup-garou|libre|
|[tvr5xWTm58t3Dgy3.htm](kingmaker-bestiary-items/tvr5xWTm58t3Dgy3.htm)|Crumbling Edge|Désagrégation des bords|libre|
|[TWA2MXZuX91plGrL.htm](kingmaker-bestiary-items/TWA2MXZuX91plGrL.htm)|Art Lore|Connaissance des arts|libre|
|[TwMRnjbKEUPZwBKx.htm](kingmaker-bestiary-items/TwMRnjbKEUPZwBKx.htm)|Divine Font|Source divine|libre|
|[TwVHeJqYT1io5LxX.htm](kingmaker-bestiary-items/TwVHeJqYT1io5LxX.htm)|Summon Pack|Rassembler la meute|libre|
|[TxCWNmfsEkdTzNvw.htm](kingmaker-bestiary-items/TxCWNmfsEkdTzNvw.htm)|Outside of Time|Hors du temps|libre|
|[TxKSVVgJ5OZw3cy0.htm](kingmaker-bestiary-items/TxKSVVgJ5OZw3cy0.htm)|Outside of Time|Hors du temps|libre|
|[TXyxtLOPRcUESvfj.htm](kingmaker-bestiary-items/TXyxtLOPRcUESvfj.htm)|Tongues (Constant)|Don des langues (constant)|officielle|
|[tz6uqrNAY2ySZ2TY.htm](kingmaker-bestiary-items/tz6uqrNAY2ySZ2TY.htm)|Of Three Minds|Volonté tricéphale|libre|
|[Tzi4lR0Oigo4U8Wu.htm](kingmaker-bestiary-items/Tzi4lR0Oigo4U8Wu.htm)|Emerald Beam|Rayon d'émeraude|libre|
|[TZKyD7ZHyYEDKtws.htm](kingmaker-bestiary-items/TZKyD7ZHyYEDKtws.htm)|Tongues (Constant)|Don des langues (constant)|officielle|
|[u0gmeeDEvRzdbvFs.htm](kingmaker-bestiary-items/u0gmeeDEvRzdbvFs.htm)|Negative Healing|Guérison négative|libre|
|[U22LSFuhWJzxq7TO.htm](kingmaker-bestiary-items/U22LSFuhWJzxq7TO.htm)|Damsel Act|Performance de la damoiselle|libre|
|[U69KdW5yZ21jJT4a.htm](kingmaker-bestiary-items/U69KdW5yZ21jJT4a.htm)|Yog-Sothoth Lore|Connaissance Yog-Sothoth|libre|
|[u8YE1BntdMHM1ks0.htm](kingmaker-bestiary-items/u8YE1BntdMHM1ks0.htm)|At-Will Spells|Sorts à volonté|libre|
|[Ubytn72fPKxfwdab.htm](kingmaker-bestiary-items/Ubytn72fPKxfwdab.htm)|Dagger|Dague|libre|
|[ucBBQrbesTH1rIdv.htm](kingmaker-bestiary-items/ucBBQrbesTH1rIdv.htm)|Light Hammer|Marteau de guerre léger|libre|
|[uCIO2gyqCCw5HMiZ.htm](kingmaker-bestiary-items/uCIO2gyqCCw5HMiZ.htm)|Quick Bomber|Artificier rapide|libre|
|[UCNMMptie1HfmWdu.htm](kingmaker-bestiary-items/UCNMMptie1HfmWdu.htm)|Rapier|+3,majorStriking,greaterShock,wounding|Rapière de choc supérieur sanglante de frappe majeure +3|libre|
|[UdmjBD1i3DxTYEep.htm](kingmaker-bestiary-items/UdmjBD1i3DxTYEep.htm)|Telekinetic Assault|Assaut télékinésique|libre|
|[UeSFgZM3o03iSxuq.htm](kingmaker-bestiary-items/UeSFgZM3o03iSxuq.htm)|+2 Resilient Full Plate|Harnois de résilience +2|officielle|
|[UGAcwTdmLcqn2A5C.htm](kingmaker-bestiary-items/UGAcwTdmLcqn2A5C.htm)|River Lore|Connaissance de la rivière|libre|
|[UGbMxXUr68ITj7ND.htm](kingmaker-bestiary-items/UGbMxXUr68ITj7ND.htm)|Drain Lintwerth|Drain de Lintwerth|libre|
|[UiApvNrZOpUiRTze.htm](kingmaker-bestiary-items/UiApvNrZOpUiRTze.htm)|Centipede Mandibles|Mandibules de mille-pattes|libre|
|[UIGEr5LUogZmy8ln.htm](kingmaker-bestiary-items/UIGEr5LUogZmy8ln.htm)|Phantasmagoric Fog|Brouillard fantasmagorique|libre|
|[UiU7w5ovykgz63Vf.htm](kingmaker-bestiary-items/UiU7w5ovykgz63Vf.htm)|First World Lore|Connaissance du Premier monde|libre|
|[uiZk5k4gZbLlAo44.htm](kingmaker-bestiary-items/uiZk5k4gZbLlAo44.htm)|Rejuvenation|Reconstruction|libre|
|[UJnXwBbuFjAcC01q.htm](kingmaker-bestiary-items/UJnXwBbuFjAcC01q.htm)|Unsettling Attention|Attention perturbante|libre|
|[UKYROFSj3c464waM.htm](kingmaker-bestiary-items/UKYROFSj3c464waM.htm)|Battle Axe|+2,striking|Hache d'armes de frappe +2|libre|
|[ulr3xQ9vjVzN0JcV.htm](kingmaker-bestiary-items/ulr3xQ9vjVzN0JcV.htm)|Constant Spells|Sorts constants|libre|
|[UlSgJk73FttvDVKx.htm](kingmaker-bestiary-items/UlSgJk73FttvDVKx.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[Um8WRHtbayEPwLGn.htm](kingmaker-bestiary-items/Um8WRHtbayEPwLGn.htm)|Shortsword|cold-iron|Epée coure en fer froid|libre|
|[uMg5j8kbgsMRkJNO.htm](kingmaker-bestiary-items/uMg5j8kbgsMRkJNO.htm)|Invoke Old Sharptooth|Invoquer Vieux Dentranchante|libre|
|[uox8BdqT7SHwPAXF.htm](kingmaker-bestiary-items/uox8BdqT7SHwPAXF.htm)|Backlash|Retour de bâton|libre|
|[Up8YexckH0qYu7Mg.htm](kingmaker-bestiary-items/Up8YexckH0qYu7Mg.htm)|Hatchet|Hachette|libre|
|[uq6NvrLNEXMkTsQf.htm](kingmaker-bestiary-items/uq6NvrLNEXMkTsQf.htm)|Pass Without Trace (Constant)|Passage sans trace (constant)|officielle|
|[UqivmeGpdoq5TEFp.htm](kingmaker-bestiary-items/UqivmeGpdoq5TEFp.htm)|Manifest Fetch Weapon|Manifestation d'arme fetch|libre|
|[UqyHj2l5UsVe9uNp.htm](kingmaker-bestiary-items/UqyHj2l5UsVe9uNp.htm)|+1 Leather Armor|Armure en cuir +1|officielle|
|[Us5NJn2RFIpZuqkH.htm](kingmaker-bestiary-items/Us5NJn2RFIpZuqkH.htm)|Crushing Despair (At Will)|Désespoir foudroyant (À volonté)|libre|
|[usScZt5t8JLT5Ipy.htm](kingmaker-bestiary-items/usScZt5t8JLT5Ipy.htm)|Illusory Disguise (At Will)|Déguisement illusoire (À volonté)|officielle|
|[UsxGNp0GLCHolNw9.htm](kingmaker-bestiary-items/UsxGNp0GLCHolNw9.htm)|Warhammer|+1|Marteau de guerre +1|libre|
|[utrBYZarwPMS7ACv.htm](kingmaker-bestiary-items/utrBYZarwPMS7ACv.htm)|Resolve|Résolution|libre|
|[UtXatkaqCOacbbnj.htm](kingmaker-bestiary-items/UtXatkaqCOacbbnj.htm)|Jaws|Mâchoires|libre|
|[UV0GwxK9jPNBGPkx.htm](kingmaker-bestiary-items/UV0GwxK9jPNBGPkx.htm)|Composite Longbow|+1|Arc long composite +1|officielle|
|[uv3BAfwMvrlyZDI6.htm](kingmaker-bestiary-items/uv3BAfwMvrlyZDI6.htm)|Fist|Poing|libre|
|[uwaEfaQp2MZCrkyx.htm](kingmaker-bestiary-items/uwaEfaQp2MZCrkyx.htm)|Greataxe|cold-iron|Grande hache en fer froid|libre|
|[UZ4VlSGBzBXCEdfz.htm](kingmaker-bestiary-items/UZ4VlSGBzBXCEdfz.htm)|Religious Symbol of Lamashtu (Gold)|Symbole religieux en or de Lamashtu|libre|
|[UZkmL2WnxA4MOAUK.htm](kingmaker-bestiary-items/UZkmL2WnxA4MOAUK.htm)|Composite Shortbow|+2,greaterStriking,darkwood,grievous,thundering|Arc court composite en ébénite douloureuse de tonnerre de Frappe supérieure +2|libre|
|[v0ppjSET59hXq9Z5.htm](kingmaker-bestiary-items/v0ppjSET59hXq9Z5.htm)|Dagger|Dague|libre|
|[v4dtDXWYnbH5R47w.htm](kingmaker-bestiary-items/v4dtDXWYnbH5R47w.htm)|Fearsome Blizzard|Tornade de terreur|libre|
|[V6fjZPcQEMD98LxA.htm](kingmaker-bestiary-items/V6fjZPcQEMD98LxA.htm)|Dramatic Disarm|Désarmement dramatique|libre|
|[v76tHhlGlywyinPF.htm](kingmaker-bestiary-items/v76tHhlGlywyinPF.htm)|Shortbow|Arc court|libre|
|[V7siOMUXReEpRR0u.htm](kingmaker-bestiary-items/V7siOMUXReEpRR0u.htm)|Dagger|Dague|libre|
|[v8fWAG80JbMt4nOy.htm](kingmaker-bestiary-items/v8fWAG80JbMt4nOy.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[VaiIAm2nnOW5bySh.htm](kingmaker-bestiary-items/VaiIAm2nnOW5bySh.htm)|Silver Stag Lord Amulet|Amulette du Seigneur Cerf en argent|libre|
|[vb6tOxigCXGBNO8n.htm](kingmaker-bestiary-items/vb6tOxigCXGBNO8n.htm)|+2 Resilient Leather Armor|Armure en cuir de résilience +2|officielle|
|[vbjtlEObLLpIhfeu.htm](kingmaker-bestiary-items/vbjtlEObLLpIhfeu.htm)|Rapier|+1|Rapière +1|libre|
|[vCJLJPrm6qXxNRzd.htm](kingmaker-bestiary-items/vCJLJPrm6qXxNRzd.htm)|Divine Font (Harm)|Source divine (Mise à mal)|libre|
|[VCxhoISThjwcy55r.htm](kingmaker-bestiary-items/VCxhoISThjwcy55r.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[Vd2tGIDTvBddHJ36.htm](kingmaker-bestiary-items/Vd2tGIDTvBddHJ36.htm)|Arcane Focus Spells|Sorts arcaniques focalisés|libre|
|[VdKXa5UE8pjAkhRN.htm](kingmaker-bestiary-items/VdKXa5UE8pjAkhRN.htm)|Staff|Bâton|libre|
|[vFhm5NJpp6V7lfc8.htm](kingmaker-bestiary-items/vFhm5NJpp6V7lfc8.htm)|Composite Shortbow|+1,striking|Arc court composite de Frappe +1|libre|
|[VgQyT5O8X59xTXWs.htm](kingmaker-bestiary-items/VgQyT5O8X59xTXWs.htm)|Summon Elemental (Water Elementals only)|Convocation d'élémentaire (Eau uniquement)|libre|
|[VGrJpWKaRTR27Qyu.htm](kingmaker-bestiary-items/VGrJpWKaRTR27Qyu.htm)|Arcane Prepared Spells|Sorts arcaniques préparés|libre|
|[viDna1Oudpf22Ieg.htm](kingmaker-bestiary-items/viDna1Oudpf22Ieg.htm)|Blowgun|Sarbacane|libre|
|[viyrIe0gXqX4x1ZG.htm](kingmaker-bestiary-items/viyrIe0gXqX4x1ZG.htm)|Mithral Necklace|Collier en mithral|libre|
|[vJiFJxTfxGmRSWe0.htm](kingmaker-bestiary-items/vJiFJxTfxGmRSWe0.htm)|Composite Shortbow|Arc court composite|libre|
|[vLKZN5X8t0pUwZJd.htm](kingmaker-bestiary-items/vLKZN5X8t0pUwZJd.htm)|Improved Knockdown|Renversement amélioré|libre|
|[vllPNnBmoSwFEtl9.htm](kingmaker-bestiary-items/vllPNnBmoSwFEtl9.htm)|Guarded Movement|Déplacement circonspect|libre|
|[VmmlUbjFsbcN9dM8.htm](kingmaker-bestiary-items/VmmlUbjFsbcN9dM8.htm)|Sneak Attack|Attaque sournoise|libre|
|[vO3Jnytxei8sWTZC.htm](kingmaker-bestiary-items/vO3Jnytxei8sWTZC.htm)|Kukri|+1|Kukri +1|libre|
|[VPg7zeVby8h8yTMX.htm](kingmaker-bestiary-items/VPg7zeVby8h8yTMX.htm)|Bark Orders|Aboyer des ordres|libre|
|[VPpeBvTnuLzMsNon.htm](kingmaker-bestiary-items/VPpeBvTnuLzMsNon.htm)|Primal Prepared Spells|Sorts primordiaux préparés|libre|
|[VQzmSFhPlPCnWRYb.htm](kingmaker-bestiary-items/VQzmSFhPlPCnWRYb.htm)|Arcane Innate Spells|Sorts arcaniques innés|libre|
|[vRSrxRGekX69z1xM.htm](kingmaker-bestiary-items/vRSrxRGekX69z1xM.htm)|Greatsword|Épée à deux mains|libre|
|[vrZuMJAZcSYNFRp1.htm](kingmaker-bestiary-items/vrZuMJAZcSYNFRp1.htm)|Woodland Stride|Déplacement facilité en forêt|libre|
|[VTjvgSGDhiMzsKdY.htm](kingmaker-bestiary-items/VTjvgSGDhiMzsKdY.htm)|Frenzied Fangs|Crocs frénétiques|libre|
|[VUyMebZxthD1vUj3.htm](kingmaker-bestiary-items/VUyMebZxthD1vUj3.htm)|Composite Longbow|Arc long composite|libre|
|[vvB4kICd5X8HaFue.htm](kingmaker-bestiary-items/vvB4kICd5X8HaFue.htm)|Rage|Rage|libre|
|[VVezMNwhjbCrp0yX.htm](kingmaker-bestiary-items/VVezMNwhjbCrp0yX.htm)|Warfare Lore|Connaissance de la guerre|libre|
|[VViv6cOIpMtNUZzW.htm](kingmaker-bestiary-items/VViv6cOIpMtNUZzW.htm)|Brutal Gore|Carnage brutal|libre|
|[vVOwyQHVQvmtGjkx.htm](kingmaker-bestiary-items/vVOwyQHVQvmtGjkx.htm)|+1 Greater Invisibility Resilient Leather Armor|Armure en cuir d'invisibilité supérieure de résilience +1|libre|
|[VwMYfCPLMmuJQ2GH.htm](kingmaker-bestiary-items/VwMYfCPLMmuJQ2GH.htm)|+1 Resilient Breastplate|Cuirasse de résilience +1|officielle|
|[vwnAqdmQtoc2jUsW.htm](kingmaker-bestiary-items/vwnAqdmQtoc2jUsW.htm)|Broken +1 Cold Iron Flaming Bastard Sword|+1,cold-iron,flaming|Épée bâtarde en fer froid Enflammée +1 brisée|libre|
|[vXW5poBVgyqzdM9r.htm](kingmaker-bestiary-items/vXW5poBVgyqzdM9r.htm)|Pass Without Trace (Constant)|Passage sans trace (constant)|officielle|
|[vYdUPrjuB8HrzGBA.htm](kingmaker-bestiary-items/vYdUPrjuB8HrzGBA.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[vYngXSj2DDVpfWFe.htm](kingmaker-bestiary-items/vYngXSj2DDVpfWFe.htm)|Meddling Tail|Queue d'ingérence|libre|
|[vyta0B6y63mLUnmx.htm](kingmaker-bestiary-items/vyta0B6y63mLUnmx.htm)|Maestro's Lute (Lesser)|Luth du maestro inférieur|libre|
|[VzPueq3VgRjoXiJE.htm](kingmaker-bestiary-items/VzPueq3VgRjoXiJE.htm)|+1 Resilient Hide Armor|Armure en peau de résilience +1|libre|
|[VzrK9L3fTzGmyHto.htm](kingmaker-bestiary-items/VzrK9L3fTzGmyHto.htm)|Druid Order Spells|Sorts de l'Ordre des druides|libre|
|[w5ySqTQIaFATIjDM.htm](kingmaker-bestiary-items/w5ySqTQIaFATIjDM.htm)|Arcane Innate Spells|Sorts arcaniques innés|libre|
|[W8NvzJ9eGCBEL7cH.htm](kingmaker-bestiary-items/W8NvzJ9eGCBEL7cH.htm)|Robes|Robes|libre|
|[wAjwXDiTkvisDxZR.htm](kingmaker-bestiary-items/wAjwXDiTkvisDxZR.htm)|Survival of the Fittest|Survie du plus fort|libre|
|[wb6prflG67LiVXh1.htm](kingmaker-bestiary-items/wb6prflG67LiVXh1.htm)|Trackless Step|Absence de traces|libre|
|[wbtt2m0nhK330JwC.htm](kingmaker-bestiary-items/wbtt2m0nhK330JwC.htm)|Hatchet|Hachette|libre|
|[WD2RKxV7k6NLl0Bn.htm](kingmaker-bestiary-items/WD2RKxV7k6NLl0Bn.htm)|Ragged Robes|Guenilles|libre|
|[wd4WShZlAcAc3dLe.htm](kingmaker-bestiary-items/wd4WShZlAcAc3dLe.htm)|Religious Symbol of Yog-Sothoth|Symbole religieux de Yog-Sothoth|libre|
|[WDpA7ff1gJiokx9N.htm](kingmaker-bestiary-items/WDpA7ff1gJiokx9N.htm)|Hunted Shot|Tir du chasseur|libre|
|[we4aKsadr3FYXQCP.htm](kingmaker-bestiary-items/we4aKsadr3FYXQCP.htm)|Primal Prepared Spells|Sorts primordiaux préparés|libre|
|[wEUwtWWTglvCsbrr.htm](kingmaker-bestiary-items/wEUwtWWTglvCsbrr.htm)|Ring of Energy Resistance (Fire)|Anneau de Résistance à l'énergie (feu)|libre|
|[WF3kud5WAVewvKL0.htm](kingmaker-bestiary-items/WF3kud5WAVewvKL0.htm)|Low-Light Vision|Vision dans le noir|libre|
|[wFE3Lth938xGYdIL.htm](kingmaker-bestiary-items/wFE3Lth938xGYdIL.htm)|Warfare Lore|Connaissance de la guerre|libre|
|[wFieL2rsS0tFWUAR.htm](kingmaker-bestiary-items/wFieL2rsS0tFWUAR.htm)|Regeneration 20 (Deactivated by Cold Iron or Lawful)|Régénération 20 (désactivé par fer froid ou loyal)|libre|
|[WhCqHDOg0A8FK3IF.htm](kingmaker-bestiary-items/WhCqHDOg0A8FK3IF.htm)|Relentless Tracker|Pisteur infatigable|libre|
|[WIPJ7Xl3dOhNuLlq.htm](kingmaker-bestiary-items/WIPJ7Xl3dOhNuLlq.htm)|Counterspell|Contresort|libre|
|[Wj4PwJFZfcVXdLml.htm](kingmaker-bestiary-items/Wj4PwJFZfcVXdLml.htm)|Mind Reading (At Will)|Lecture des pensées (À volonté)|officielle|
|[WJaww98OCHbzRIIP.htm](kingmaker-bestiary-items/WJaww98OCHbzRIIP.htm)|Jaws|Mâchoires|libre|
|[Wjfmdyuddn5fCyVZ.htm](kingmaker-bestiary-items/Wjfmdyuddn5fCyVZ.htm)|Insult Challenges|Défis d'insultes|libre|
|[wkahGwEkL5eYhtZy.htm](kingmaker-bestiary-items/wkahGwEkL5eYhtZy.htm)|Tail|Queue|libre|
|[wll4JHYsNNNsx4lF.htm](kingmaker-bestiary-items/wll4JHYsNNNsx4lF.htm)|Academia Lore|Connaissance universitaire|libre|
|[WlQZsDquZL824cDy.htm](kingmaker-bestiary-items/WlQZsDquZL824cDy.htm)|+1 Hide Armor|Armure en peau +1|officielle|
|[WNANaLjOqPHdTPjN.htm](kingmaker-bestiary-items/WNANaLjOqPHdTPjN.htm)|Dagger|Dague|libre|
|[wO3hSmCvgcCzSah3.htm](kingmaker-bestiary-items/wO3hSmCvgcCzSah3.htm)|Bloom Curse|Malédiction de la fleur|libre|
|[WpEtlM3mpICnsXPm.htm](kingmaker-bestiary-items/WpEtlM3mpICnsXPm.htm)|Steady Spellcasting|Incantation fiable|libre|
|[wrFzv3AXMax8ZXTt.htm](kingmaker-bestiary-items/wrFzv3AXMax8ZXTt.htm)|Illusory Disguise (At Will)|Déguisement illusoire (À volonté)|officielle|
|[WRLKbO1wNweUuW4n.htm](kingmaker-bestiary-items/WRLKbO1wNweUuW4n.htm)|Longspear|Pique|libre|
|[WrmnFHWCqCQMpQlB.htm](kingmaker-bestiary-items/WrmnFHWCqCQMpQlB.htm)|Pin to the Sky|Épingler en l'air|libre|
|[wSYY3PhHktTGw8ua.htm](kingmaker-bestiary-items/wSYY3PhHktTGw8ua.htm)|Baroness's Scream|Cri de baronne|libre|
|[Wt9KDemV1jGIVjE5.htm](kingmaker-bestiary-items/Wt9KDemV1jGIVjE5.htm)|Catch Rock|Interception de rocher|libre|
|[WTjCVJwNHH8vUKzE.htm](kingmaker-bestiary-items/WTjCVJwNHH8vUKzE.htm)|Sorcerer Bloodline Spells|Sorts de lignage d'ensorceleur|libre|
|[wuin1zchdZrKVQEY.htm](kingmaker-bestiary-items/wuin1zchdZrKVQEY.htm)|Dagger|Dague|libre|
|[WvKgLpntjekJJHp7.htm](kingmaker-bestiary-items/WvKgLpntjekJJHp7.htm)|Sneak Attack|Attaque sournoise|libre|
|[wXPOAgEeixFbagOW.htm](kingmaker-bestiary-items/wXPOAgEeixFbagOW.htm)|Swift Summon|Convocation rapide|libre|
|[wyAVtZ4X5ErNqi3C.htm](kingmaker-bestiary-items/wyAVtZ4X5ErNqi3C.htm)|Sneak Attack|Attaque sournoise|libre|
|[wYKABRKZFADU4eFV.htm](kingmaker-bestiary-items/wYKABRKZFADU4eFV.htm)|Fortune's Friend|Ami de la Chance|libre|
|[wzltAJwbP73KlolU.htm](kingmaker-bestiary-items/wzltAJwbP73KlolU.htm)|Itchy|Démangeaisons|libre|
|[x1BxQoUjfPHH06yh.htm](kingmaker-bestiary-items/x1BxQoUjfPHH06yh.htm)|Dagger|Dague|libre|
|[X1qtaRb6bkYt1N3s.htm](kingmaker-bestiary-items/X1qtaRb6bkYt1N3s.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[X3C10c6qy59DlNdh.htm](kingmaker-bestiary-items/X3C10c6qy59DlNdh.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[X3HpUq80mQWyiHZ3.htm](kingmaker-bestiary-items/X3HpUq80mQWyiHZ3.htm)|Hunt Prey|Chasser une proie (Niveau 6)|libre|
|[X3z6nTBllD8cR58y.htm](kingmaker-bestiary-items/X3z6nTBllD8cR58y.htm)|Ring of Energy Resistance (Cold)|Anneau de résistance à l'énergie (Froid)|libre|
|[X41cWt7idOHqasPx.htm](kingmaker-bestiary-items/X41cWt7idOHqasPx.htm)|Greatsword|Épée à deux mains|libre|
|[x46bcv7xjs0irbqk.htm](kingmaker-bestiary-items/x46bcv7xjs0irbqk.htm)|Morningstar|Morgenstern|libre|
|[x46WnJD9YKQXpRkE.htm](kingmaker-bestiary-items/x46WnJD9YKQXpRkE.htm)|Raging Storm|Violente tempête|libre|
|[X7v48sCoD3y3bAyO.htm](kingmaker-bestiary-items/X7v48sCoD3y3bAyO.htm)|Cultist Robes|Robes de cultiste|libre|
|[x8mIj7POmkmnt5WS.htm](kingmaker-bestiary-items/x8mIj7POmkmnt5WS.htm)|Divine Recovery|Récupération divine|libre|
|[x9smkTQ1YyyY2Zcx.htm](kingmaker-bestiary-items/x9smkTQ1YyyY2Zcx.htm)|Invisibility (At Will)|Invisibilité (À volonté)|officielle|
|[xBvjgSrMNlbRs2vt.htm](kingmaker-bestiary-items/xBvjgSrMNlbRs2vt.htm)|Crossbow|Arbalète|libre|
|[XCuiusaM9Pgov7Hy.htm](kingmaker-bestiary-items/XCuiusaM9Pgov7Hy.htm)|Reactive|Réactif|libre|
|[XCvqYacJqKAyKyBy.htm](kingmaker-bestiary-items/XCvqYacJqKAyKyBy.htm)|Change Shape|Changement de forme|libre|
|[xdpY6vYKdagXO6bk.htm](kingmaker-bestiary-items/xdpY6vYKdagXO6bk.htm)|Bard Composition Spells|Sorts de composition de barde|libre|
|[XdUyVqH2LISYTKP4.htm](kingmaker-bestiary-items/XdUyVqH2LISYTKP4.htm)|Sickle|Serpe|libre|
|[XEczLnuwuxklAds8.htm](kingmaker-bestiary-items/XEczLnuwuxklAds8.htm)|Adjust Temperature|Régler la température|libre|
|[xeEI4dqDE4PmxOeA.htm](kingmaker-bestiary-items/xeEI4dqDE4PmxOeA.htm)|Forest Walker|Arpenteur des forêts|libre|
|[XEeQ6n37g3yEASJB.htm](kingmaker-bestiary-items/XEeQ6n37g3yEASJB.htm)|Dagger|Dague|libre|
|[XFTxhLhYbwJ3pxdt.htm](kingmaker-bestiary-items/XFTxhLhYbwJ3pxdt.htm)|Unseen Sight|Vision invisible|libre|
|[XfUDUv0iccZ8hNjg.htm](kingmaker-bestiary-items/XfUDUv0iccZ8hNjg.htm)|Negative Healing|Guérison négative|libre|
|[XFWwEVmfkf4Naohf.htm](kingmaker-bestiary-items/XFWwEVmfkf4Naohf.htm)|Jaws|Mâchoire|libre|
|[XgD0k33XDvhefMEQ.htm](kingmaker-bestiary-items/XgD0k33XDvhefMEQ.htm)|Tongues (Constant)|Don des langues (constant)|officielle|
|[XHQxmGEm71C14Nci.htm](kingmaker-bestiary-items/XHQxmGEm71C14Nci.htm)|Grab|Empoigner/agripper|libre|
|[XhrxL6SNJhTojhys.htm](kingmaker-bestiary-items/XhrxL6SNJhTojhys.htm)|Manifest Crystals|Manifestation de cristaux|libre|
|[XI80UWdBffkVI3qW.htm](kingmaker-bestiary-items/XI80UWdBffkVI3qW.htm)|Maul|+2,greaterShock|Maillet +2 de foudre supérieure|libre|
|[xI9jqkNAsWJ8GrxC.htm](kingmaker-bestiary-items/xI9jqkNAsWJ8GrxC.htm)|Collapse|Effondrement|libre|
|[xIXTT704xBEHdGIn.htm](kingmaker-bestiary-items/xIXTT704xBEHdGIn.htm)|Dagger|Dague|libre|
|[xj83ymsPlY7u87j1.htm](kingmaker-bestiary-items/xj83ymsPlY7u87j1.htm)|Athletics|Athlétisme|libre|
|[XJFqRYGx02Um1W4Y.htm](kingmaker-bestiary-items/XJFqRYGx02Um1W4Y.htm)|Spear|+2,striking,anarchic|Lance anarchique de frappe +2|libre|
|[XJXxOQSoroFvm4m9.htm](kingmaker-bestiary-items/XJXxOQSoroFvm4m9.htm)|Constant Spells|Sorts constant|libre|
|[xkPOrd9a16OIHTyl.htm](kingmaker-bestiary-items/xkPOrd9a16OIHTyl.htm)|Second Wind|Second souffle|libre|
|[XKVb3v9QlngaKRQV.htm](kingmaker-bestiary-items/XKVb3v9QlngaKRQV.htm)|Sneak Attack|Attaque sournoise|libre|
|[XlxlWOl8QpPb2Inn.htm](kingmaker-bestiary-items/XlxlWOl8QpPb2Inn.htm)|Sack of Flytrap Pods|Sac de gousses d'attrape-mouches|libre|
|[xMM6S0SqKD29GPxJ.htm](kingmaker-bestiary-items/xMM6S0SqKD29GPxJ.htm)|Site Bound|Lié à un site|libre|
|[XnVq5NAwuLiDa4dD.htm](kingmaker-bestiary-items/XnVq5NAwuLiDa4dD.htm)|Sudden Stormburst|Explosion tempêtueuse soudaine|libre|
|[XR8SUq7u8lPNwfsz.htm](kingmaker-bestiary-items/XR8SUq7u8lPNwfsz.htm)|Overhand Smash|Coup vertical|libre|
|[xrKfhlv0heNZjJQl.htm](kingmaker-bestiary-items/xrKfhlv0heNZjJQl.htm)|+1 Morningstar|+1|Morgenstern +1|officielle|
|[xT9pOWbS31HQus3Y.htm](kingmaker-bestiary-items/xT9pOWbS31HQus3Y.htm)|Hatchet|+1,striking|Hachette de frappe +1|libre|
|[xtGHTPAuSTsudXMG.htm](kingmaker-bestiary-items/xtGHTPAuSTsudXMG.htm)|Enhanced Familiar|Familier compagnon amélioré|libre|
|[xtitjNKcLXuYUcnS.htm](kingmaker-bestiary-items/xtitjNKcLXuYUcnS.htm)|Reel In|Enroulement|libre|
|[XU7E8CJIc6dwyRml.htm](kingmaker-bestiary-items/XU7E8CJIc6dwyRml.htm)|Knockback|Repousser|libre|
|[Xva9qQPkH1v1bsG1.htm](kingmaker-bestiary-items/Xva9qQPkH1v1bsG1.htm)|Floor Collapse|Effondrement du sol|libre|
|[XwujWRcPvBs2BAHX.htm](kingmaker-bestiary-items/XwujWRcPvBs2BAHX.htm)|Sack for Holding Rocks|Sac contenant des rochers|libre|
|[xy0RcI3dflBZepLE.htm](kingmaker-bestiary-items/xy0RcI3dflBZepLE.htm)|Shortsword|+1|Épée courte +1|officielle|
|[xYt0Yl9LAnVB1BZ6.htm](kingmaker-bestiary-items/xYt0Yl9LAnVB1BZ6.htm)|Warfare Lore|Connaissance de la guerre|libre|
|[xYyZLfzVCUdoVnQn.htm](kingmaker-bestiary-items/xYyZLfzVCUdoVnQn.htm)|Reach Spell|Sort éloigné|libre|
|[xZwLtaE6SEA50Rqf.htm](kingmaker-bestiary-items/xZwLtaE6SEA50Rqf.htm)|Twin Chop|Hache jumelée|libre|
|[Y0nwjk1NkdGHxBxi.htm](kingmaker-bestiary-items/Y0nwjk1NkdGHxBxi.htm)|Change Shape|Changement de forme|libre|
|[y1uZ9I98YpDpmpQx.htm](kingmaker-bestiary-items/y1uZ9I98YpDpmpQx.htm)|At-Will Spells|Sorts à volonté|libre|
|[y57fpMJDVfTQ3moG.htm](kingmaker-bestiary-items/y57fpMJDVfTQ3moG.htm)|Brevoy Lore|Connaissance du Brévoy|libre|
|[y6KKrN8Aiff22yLS.htm](kingmaker-bestiary-items/y6KKrN8Aiff22yLS.htm)|Bloodline Magic|Magie de lignage|libre|
|[y8OK5g64U5E8qC0q.htm](kingmaker-bestiary-items/y8OK5g64U5E8qC0q.htm)|Vicious Ranseur|Coutille vicieuse|libre|
|[Y9hSg33JjPCL2a3D.htm](kingmaker-bestiary-items/Y9hSg33JjPCL2a3D.htm)|+2 Resilient Leather Armor|Armure en cuir de résilience +2|officielle|
|[Y9nkyrHVb6qQd6hs.htm](kingmaker-bestiary-items/Y9nkyrHVb6qQd6hs.htm)|Shame|Honte|libre|
|[YaqUxn6Ybbvxv3gB.htm](kingmaker-bestiary-items/YaqUxn6Ybbvxv3gB.htm)|Quill|Piquants|libre|
|[yaWanh7jgvzb4bNA.htm](kingmaker-bestiary-items/yaWanh7jgvzb4bNA.htm)|Jaws|Mâchoires|libre|
|[YCWMeM1zCWZqP48p.htm](kingmaker-bestiary-items/YCWMeM1zCWZqP48p.htm)|Rod of Razors|Bâton de rasoirs|libre|
|[YdimsTpSLxkBXaBh.htm](kingmaker-bestiary-items/YdimsTpSLxkBXaBh.htm)|Arcane Spontaneous Spells|Sorts arcaniques spontanés|libre|
|[yEmvQZDwtr985C2Y.htm](kingmaker-bestiary-items/yEmvQZDwtr985C2Y.htm)|Hatchet Flurry|Déluge de hachette|libre|
|[YeVYuXNHpkcHdBo2.htm](kingmaker-bestiary-items/YeVYuXNHpkcHdBo2.htm)|Attack of Opportunity (Tail Only)|Attaque d'opportunité (Queue uniquement)|libre|
|[yGkx5YIsqb7P1b34.htm](kingmaker-bestiary-items/yGkx5YIsqb7P1b34.htm)|Effortless Concentration|Concentration aisée|libre|
|[yGxF0q0Wjk3mF2Mo.htm](kingmaker-bestiary-items/yGxF0q0Wjk3mF2Mo.htm)|Air Walk (Constant)|Marche dans les airs (constant)|officielle|
|[Yh6PceHFS60uaThW.htm](kingmaker-bestiary-items/Yh6PceHFS60uaThW.htm)|Release Boulders|Libérer les rochers|libre|
|[yI8AXFiHc0FyemjU.htm](kingmaker-bestiary-items/yI8AXFiHc0FyemjU.htm)|Ominous Mien|Air menaçant|libre|
|[yIiN4CzNAnrIGY5R.htm](kingmaker-bestiary-items/yIiN4CzNAnrIGY5R.htm)|Low-Light Vision|Vision nocturne|libre|
|[yJ37AcaU1dl5m7KT.htm](kingmaker-bestiary-items/yJ37AcaU1dl5m7KT.htm)|Dagger|Dague|libre|
|[YJbZ9CTWY2HNVBXB.htm](kingmaker-bestiary-items/YJbZ9CTWY2HNVBXB.htm)|Silver Stag Lord Amulet|Amulette du Seigneur Cerf en argent|libre|
|[YJesTEWYLdHelRVi.htm](kingmaker-bestiary-items/YJesTEWYLdHelRVi.htm)|At-Will Spells|Sorts à volonté|libre|
|[YJR2VSzinZXYbwkd.htm](kingmaker-bestiary-items/YJR2VSzinZXYbwkd.htm)|Greatclub|+1,striking|Massue de frappe +1|libre|
|[YjsZVkEOo90ZoPjJ.htm](kingmaker-bestiary-items/YjsZVkEOo90ZoPjJ.htm)|Wand of Heal (Level 3)|Baguette de guérison (Niveau 3)|libre|
|[ykE4J2hEBa7gWmVx.htm](kingmaker-bestiary-items/ykE4J2hEBa7gWmVx.htm)|Bloom Step|Marche dans le Bourgeon|libre|
|[yKwcZmq0W6TpvY7z.htm](kingmaker-bestiary-items/yKwcZmq0W6TpvY7z.htm)|Lance|Lance d'arçon|libre|
|[yLgcfIF0Rbavu6Tr.htm](kingmaker-bestiary-items/yLgcfIF0Rbavu6Tr.htm)|Glaive|Coutille|libre|
|[YmkfgSgetEPoWRKA.htm](kingmaker-bestiary-items/YmkfgSgetEPoWRKA.htm)|Religious Symbol of Gyronna|Symbole religieux de Gyronna|libre|
|[YMNKladez96kdBX8.htm](kingmaker-bestiary-items/YMNKladez96kdBX8.htm)|Nimble Dodge|Esquive agile|libre|
|[ynDcPwSchoDdTX0R.htm](kingmaker-bestiary-items/ynDcPwSchoDdTX0R.htm)|Dimension Door (At Will)|Porte dimensionnelle (À volonté)|officielle|
|[ypEVZiKiaV4PXVPe.htm](kingmaker-bestiary-items/ypEVZiKiaV4PXVPe.htm)|Yog-Sothoth Lore|Connaissance de Yog-Sothoth|libre|
|[yPpDjeNgrvnyJ092.htm](kingmaker-bestiary-items/yPpDjeNgrvnyJ092.htm)|Distort Senses|Dénaturer les sens|libre|
|[Yq84rD7Ts0Wko9Pf.htm](kingmaker-bestiary-items/Yq84rD7Ts0Wko9Pf.htm)|Rip and Drag|Déchirer et traîner|libre|
|[yR0motfxtDNCXbQv.htm](kingmaker-bestiary-items/yR0motfxtDNCXbQv.htm)|Felling Blow|Coup renversant|libre|
|[yS5bYDhKr2Koa3mm.htm](kingmaker-bestiary-items/yS5bYDhKr2Koa3mm.htm)|No Time To Die|Pas le temps de mourir|libre|
|[YtRTdI3UAqH6vZVL.htm](kingmaker-bestiary-items/YtRTdI3UAqH6vZVL.htm)|Elemental Form (Water only)|Forme élémentaire (eau uniquement)|libre|
|[yTVotZYCI6abxOvt.htm](kingmaker-bestiary-items/yTVotZYCI6abxOvt.htm)|Bastard Sword|Épée bâtarde|libre|
|[yTWeFCTfaEECHhCs.htm](kingmaker-bestiary-items/yTWeFCTfaEECHhCs.htm)|Falchion|Cimeterre à deux mains|libre|
|[Yutc2Syx2q0EKb1P.htm](kingmaker-bestiary-items/Yutc2Syx2q0EKb1P.htm)|Heraldry Lore|Connaissance de l'héraldique|libre|
|[yvOTSeGrhPgBSOBZ.htm](kingmaker-bestiary-items/yvOTSeGrhPgBSOBZ.htm)|Capsize|Chavirer|libre|
|[yX5Z8bj03S67Thb5.htm](kingmaker-bestiary-items/yX5Z8bj03S67Thb5.htm)|Primal Spontaneous Spells|Sorts primordiaux spontanés|libre|
|[YXGsH5DgEDFiRIu7.htm](kingmaker-bestiary-items/YXGsH5DgEDFiRIu7.htm)|Lumber Lore|Connaissance du bûcheron|libre|
|[YxX6oMlTOXzTGheb.htm](kingmaker-bestiary-items/YxX6oMlTOXzTGheb.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[YyYWWpan1IChNfUA.htm](kingmaker-bestiary-items/YyYWWpan1IChNfUA.htm)|Jade and Pearl Necklace|Collier de jade et de perles|libre|
|[z3giiwwqKX9RBgD8.htm](kingmaker-bestiary-items/z3giiwwqKX9RBgD8.htm)|Spew Seed Pod|Projection de gousses|libre|
|[Z5F5Xx9nQAfj1TtM.htm](kingmaker-bestiary-items/Z5F5Xx9nQAfj1TtM.htm)|Fireball|Boule de feu|libre|
|[Z65zRWuULtfR4enK.htm](kingmaker-bestiary-items/Z65zRWuULtfR4enK.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[z97KUUoWcBLn9aga.htm](kingmaker-bestiary-items/z97KUUoWcBLn9aga.htm)|+1 Leather Armor|Armure en cuir +1|officielle|
|[Z9mCi4uEoEQi6uMv.htm](kingmaker-bestiary-items/Z9mCi4uEoEQi6uMv.htm)|Quick Block|Blocage rapide|libre|
|[ZaZTsIwbHhL3ZcQy.htm](kingmaker-bestiary-items/ZaZTsIwbHhL3ZcQy.htm)|Battle Axe|Hache d'armes|libre|
|[Zb5uVpmuHCmlnVkv.htm](kingmaker-bestiary-items/Zb5uVpmuHCmlnVkv.htm)|+2 Resilient Mithral Breastplate|Cuirasse en mithral résiliente +2|libre|
|[ZBxbAEeCdREK43Yu.htm](kingmaker-bestiary-items/ZBxbAEeCdREK43Yu.htm)|Bard Composition Spells|Sorts de composition de barde|libre|
|[zdtGqzcFir4opahs.htm](kingmaker-bestiary-items/zdtGqzcFir4opahs.htm)|Refocus Curse|Refocaliser la malédiction|libre|
|[zfVISHBP6rc5o8Rv.htm](kingmaker-bestiary-items/zfVISHBP6rc5o8Rv.htm)|Exile's Curse|Malédiction de l'exilé|libre|
|[ZG61qjm93pm36Iw5.htm](kingmaker-bestiary-items/ZG61qjm93pm36Iw5.htm)|Darkvision|Vision dans le noir|libre|
|[zgdHnMuj8S2a5m8L.htm](kingmaker-bestiary-items/zgdHnMuj8S2a5m8L.htm)|Ghostly Hand|Main spectrale|libre|
|[zGifR2PUMCuF3jab.htm](kingmaker-bestiary-items/zGifR2PUMCuF3jab.htm)|Religious Symbol of Yog-Sothoth|Symbole religieux de Yog-Sothoth|libre|
|[ZH9w82rcAvicGUV6.htm](kingmaker-bestiary-items/ZH9w82rcAvicGUV6.htm)|Menace Prey|Proie menacée|libre|
|[ziktYNU02MOBNRm7.htm](kingmaker-bestiary-items/ziktYNU02MOBNRm7.htm)|Infectious Wrath|Vengeance infectieuse|libre|
|[ZiobchoAZw8EGQAl.htm](kingmaker-bestiary-items/ZiobchoAZw8EGQAl.htm)|Recovery|Récupération|libre|
|[ZixFLnOGrgPgMXgK.htm](kingmaker-bestiary-items/ZixFLnOGrgPgMXgK.htm)|Shortbow|+1,striking|Arc court de frappe +1|libre|
|[ZlCrr2J1Yth6IZEj.htm](kingmaker-bestiary-items/ZlCrr2J1Yth6IZEj.htm)|Lamashtu's Bloom|Floraison de Lamashtu|libre|
|[zm4JTE61At7s9UrX.htm](kingmaker-bestiary-items/zm4JTE61At7s9UrX.htm)|Blow Mists|Souffle brouillard|libre|
|[zNJKK7fb7gXv78OV.htm](kingmaker-bestiary-items/zNJKK7fb7gXv78OV.htm)|Warhammer|Marteau de guerre|libre|
|[ZNWj00PO55PVPFmc.htm](kingmaker-bestiary-items/ZNWj00PO55PVPFmc.htm)|Shortsword|Épée courte|libre|
|[zOBaEVlReAlrAIKE.htm](kingmaker-bestiary-items/zOBaEVlReAlrAIKE.htm)|Lonely Dirge|Mélopée funèbre solitaire|libre|
|[ZOJd2q3zsGLrA8Xw.htm](kingmaker-bestiary-items/ZOJd2q3zsGLrA8Xw.htm)|Divine Prepared Spells|Sorts divins préparés|libre|
|[ZozpoIHVu6M6hsy6.htm](kingmaker-bestiary-items/ZozpoIHVu6M6hsy6.htm)|+2 Resilient Hide Armor|Armure en peau de résilience +2|libre|
|[ZPVlL1UH75oMFTCB.htm](kingmaker-bestiary-items/ZPVlL1UH75oMFTCB.htm)|Armag's Rage|Rage d'Armag|libre|
|[zquNaeOnacgZG38j.htm](kingmaker-bestiary-items/zquNaeOnacgZG38j.htm)|Darkvision|Vision dans le noir|libre|
|[ZQWoQnlsIHN6EyFc.htm](kingmaker-bestiary-items/ZQWoQnlsIHN6EyFc.htm)|Wolf Jaws|Mâchoires de loup|libre|
|[ZR6JE7Gh0fgDobEm.htm](kingmaker-bestiary-items/ZR6JE7Gh0fgDobEm.htm)|Lurker Stance|Posture du rôdeur|libre|
|[ZrwMBFIWJnpx6Utu.htm](kingmaker-bestiary-items/ZrwMBFIWJnpx6Utu.htm)|Dread Striker|Attaquant d'effroi|libre|
|[ZSab4ZN954h6Ckqb.htm](kingmaker-bestiary-items/ZSab4ZN954h6Ckqb.htm)|Atrophied Lich|Liche atrophiée|libre|
|[ztl64mYGjNx9Qab6.htm](kingmaker-bestiary-items/ztl64mYGjNx9Qab6.htm)|Sorcerer Bloodline Spells|Sorts de lignage d'ensorceleur|libre|
|[zWFmuptIJBa1t8FT.htm](kingmaker-bestiary-items/zWFmuptIJBa1t8FT.htm)|Warfare Lore|Connaissance de la guerre|libre|
|[zWVvmrN2Kq0dZWsu.htm](kingmaker-bestiary-items/zWVvmrN2Kq0dZWsu.htm)|Freezing Wind|Vent glacial|libre|
|[ZYFSO8d8sBeN7WO7.htm](kingmaker-bestiary-items/ZYFSO8d8sBeN7WO7.htm)|Unsettling Revelation|Révélation perturbante|libre|
