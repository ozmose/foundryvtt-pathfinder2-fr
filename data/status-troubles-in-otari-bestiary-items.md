# État de la traduction (troubles-in-otari-bestiary-items)

 * **officielle**: 87
 * **libre**: 5


Dernière mise à jour: 2023-07-12 20:28 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[088hXYj7GIHJtlm7.htm](troubles-in-otari-bestiary-items/088hXYj7GIHJtlm7.htm)|Hand Crossbow|Arbalète de poing|officielle|
|[0gQuuCA2OyaQRNwR.htm](troubles-in-otari-bestiary-items/0gQuuCA2OyaQRNwR.htm)|Stench|Puanteur|officielle|
|[0SzWuQBdiVOEj4WC.htm](troubles-in-otari-bestiary-items/0SzWuQBdiVOEj4WC.htm)|Shattering Urn|Urne brisée|officielle|
|[19bgEIET0naweDE4.htm](troubles-in-otari-bestiary-items/19bgEIET0naweDE4.htm)|Darkvision|Vision dans le noir|officielle|
|[1on1nKLods5yCirm.htm](troubles-in-otari-bestiary-items/1on1nKLods5yCirm.htm)|Sneak Attack|Attaque sournoise|officielle|
|[1uUxlDMn7nj3yjAy.htm](troubles-in-otari-bestiary-items/1uUxlDMn7nj3yjAy.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[2uDTTLnPGGNUafjb.htm](troubles-in-otari-bestiary-items/2uDTTLnPGGNUafjb.htm)|Fiend Lore|Connaissance des fiélons|officielle|
|[5519SZd2Gx7R0pso.htm](troubles-in-otari-bestiary-items/5519SZd2Gx7R0pso.htm)|Spellbook (Advanced Arcanomnemonics)|Grimoire (Arcanumnémonique avancée)|officielle|
|[56wLde8SVEfys89N.htm](troubles-in-otari-bestiary-items/56wLde8SVEfys89N.htm)|Vine|Liane|officielle|
|[5v6fepRCSyg6ZABC.htm](troubles-in-otari-bestiary-items/5v6fepRCSyg6ZABC.htm)|Darkvision|Vision dans le noir|officielle|
|[71j8rIykcKntxLJQ.htm](troubles-in-otari-bestiary-items/71j8rIykcKntxLJQ.htm)|Collapse|Effondrement|officielle|
|[73fsCGNUERkE3I1S.htm](troubles-in-otari-bestiary-items/73fsCGNUERkE3I1S.htm)|Hypnotic Stare|Regard hypnotisant|officielle|
|[742oUZV30i6otpgL.htm](troubles-in-otari-bestiary-items/742oUZV30i6otpgL.htm)|Stinky Leaves|Feuilles puantes|officielle|
|[7TD5aX6vNauxTw5T.htm](troubles-in-otari-bestiary-items/7TD5aX6vNauxTw5T.htm)|Arcane Bond|Lien arcanique|officielle|
|[8hva3s7KDSqeUTKh.htm](troubles-in-otari-bestiary-items/8hva3s7KDSqeUTKh.htm)|Dagger|Dague|officielle|
|[8kScozUBrCo4DJdt.htm](troubles-in-otari-bestiary-items/8kScozUBrCo4DJdt.htm)|Athletics|Athlétisme|officielle|
|[8mPErkHbf2ju6RgN.htm](troubles-in-otari-bestiary-items/8mPErkHbf2ju6RgN.htm)|Ferocity|Férocité|officielle|
|[9d61GjN8ciAoj4Z2.htm](troubles-in-otari-bestiary-items/9d61GjN8ciAoj4Z2.htm)|Low-Light Vision|Vision nocturne|officielle|
|[9JzCRTSPmLBMHyll.htm](troubles-in-otari-bestiary-items/9JzCRTSPmLBMHyll.htm)|Darkvision|Vision dans le noir|officielle|
|[9Nve1oYElRWcJhIh.htm](troubles-in-otari-bestiary-items/9Nve1oYElRWcJhIh.htm)|Unburdened Iron|Fer allégé|officielle|
|[aADQJA0W9vAaOmXT.htm](troubles-in-otari-bestiary-items/aADQJA0W9vAaOmXT.htm)|Maul|Maillet|officielle|
|[ae0LOykLqNxqAXLN.htm](troubles-in-otari-bestiary-items/ae0LOykLqNxqAXLN.htm)|Darkvision|Vision dans le noir|officielle|
|[ahXVih7AtAIrRV01.htm](troubles-in-otari-bestiary-items/ahXVih7AtAIrRV01.htm)|Demon Lore|Connaissance des démons|officielle|
|[b200ENWSAzXNlSMf.htm](troubles-in-otari-bestiary-items/b200ENWSAzXNlSMf.htm)|Battle Axe|Hache d'armes|officielle|
|[BH97LqHtuqNlighQ.htm](troubles-in-otari-bestiary-items/BH97LqHtuqNlighQ.htm)|Hurried Retreat|Retraite précipitée|officielle|
|[c6BnmJR85HoUuFbA.htm](troubles-in-otari-bestiary-items/c6BnmJR85HoUuFbA.htm)|Inflict Pain|Infliger la souffrance|officielle|
|[ccosYJxMalM6rfN8.htm](troubles-in-otari-bestiary-items/ccosYJxMalM6rfN8.htm)|Spike Trap|Piège à pics|officielle|
|[d5T3nJCRgqqqQvzD.htm](troubles-in-otari-bestiary-items/d5T3nJCRgqqqQvzD.htm)|Battle Cry|Cri de guerre|officielle|
|[di2qBg3DVoOSKzDk.htm](troubles-in-otari-bestiary-items/di2qBg3DVoOSKzDk.htm)|Unburdened Iron|Fer allégé|officielle|
|[dwvQyKJ9gUWC4S3f.htm](troubles-in-otari-bestiary-items/dwvQyKJ9gUWC4S3f.htm)|Starknife Attack|Attaque de lamétoile|officielle|
|[eIrvs73CKJq05yZP.htm](troubles-in-otari-bestiary-items/eIrvs73CKJq05yZP.htm)|Scent (Imprecise) 60 feet|Odorat 18 m (imprécis)|officielle|
|[Fges35T0kVykR3rl.htm](troubles-in-otari-bestiary-items/Fges35T0kVykR3rl.htm)|Sneak Attack|Attaque sournoise|officielle|
|[Fk4R8F6Vr2Lr7mZZ.htm](troubles-in-otari-bestiary-items/Fk4R8F6Vr2Lr7mZZ.htm)|Step into Nightmares|Entrer dans le champ des cauchemars|officielle|
|[fq5jsjB5HkWo1JYN.htm](troubles-in-otari-bestiary-items/fq5jsjB5HkWo1JYN.htm)|Scroll of Magic Weapon (Level 1)|Parchemin d'Arme Magique (Niveau 1)|officielle|
|[gqjrPshyFW4Y4vrS.htm](troubles-in-otari-bestiary-items/gqjrPshyFW4Y4vrS.htm)|Javelin|Javeline|officielle|
|[H8wipabcGbcu7thF.htm](troubles-in-otari-bestiary-items/H8wipabcGbcu7thF.htm)|Waving Weed|Mauvaise herbe|officielle|
|[HWwf9OfDEQH53F11.htm](troubles-in-otari-bestiary-items/HWwf9OfDEQH53F11.htm)|Religious Symbol of Asmodeus (Wooden)|Symbole religieux en bois d'Asmodéus|officielle|
|[hyIUDH9G8UlF3pne.htm](troubles-in-otari-bestiary-items/hyIUDH9G8UlF3pne.htm)|Knockdown|Renversement|officielle|
|[I0AZeY12ooiFLuuW.htm](troubles-in-otari-bestiary-items/I0AZeY12ooiFLuuW.htm)|Powerful Shove|Poussée en puissance|officielle|
|[I6Vb3uZRlv6pkiNj.htm](troubles-in-otari-bestiary-items/I6Vb3uZRlv6pkiNj.htm)|Unbalancing Blow|Coup déséquilibrant|officielle|
|[i74S7XGGuuWnr88J.htm](troubles-in-otari-bestiary-items/i74S7XGGuuWnr88J.htm)|Mauler|Écharpeur|officielle|
|[Ihvi8o6Gm4pwbhph.htm](troubles-in-otari-bestiary-items/Ihvi8o6Gm4pwbhph.htm)|Fangs|Crocs|officielle|
|[iIaK3retIHJkXgrh.htm](troubles-in-otari-bestiary-items/iIaK3retIHJkXgrh.htm)|Bushwhack|Embûche|officielle|
|[Iq79xetoAoYDXkjs.htm](troubles-in-otari-bestiary-items/Iq79xetoAoYDXkjs.htm)|Darkvision|Vision dans le noir|officielle|
|[J08BqvSNQN7R2E9v.htm](troubles-in-otari-bestiary-items/J08BqvSNQN7R2E9v.htm)|Ferocity|Férocité|officielle|
|[jgcCZHnrNbS5iREn.htm](troubles-in-otari-bestiary-items/jgcCZHnrNbS5iREn.htm)|Area Map|Carte de la région|officielle|
|[k5kr2ILvlJNg5oKb.htm](troubles-in-otari-bestiary-items/k5kr2ILvlJNg5oKb.htm)|Darkvision|Vision dans le noir|officielle|
|[l8f0B8czvLKLsc06.htm](troubles-in-otari-bestiary-items/l8f0B8czvLKLsc06.htm)|Web Noose|Noeud coulant de toile|officielle|
|[ll1kYssjNZ4S5vnd.htm](troubles-in-otari-bestiary-items/ll1kYssjNZ4S5vnd.htm)|Brutish Shove|Poussée brutale|officielle|
|[lLR6EoZxZTWifZUj.htm](troubles-in-otari-bestiary-items/lLR6EoZxZTWifZUj.htm)|Maul|Maillet|officielle|
|[lykdG2IwAxNTGqQo.htm](troubles-in-otari-bestiary-items/lykdG2IwAxNTGqQo.htm)|Darkvision|Vision dans le noir|officielle|
|[MKiOmYkknzLyP0Am.htm](troubles-in-otari-bestiary-items/MKiOmYkknzLyP0Am.htm)|Javelin|Javeline|officielle|
|[Nc23oEL8srJURx0P.htm](troubles-in-otari-bestiary-items/Nc23oEL8srJURx0P.htm)|Remove Memory|Effacer la mémoire|officielle|
|[nDnP6GpXjTbXqbXB.htm](troubles-in-otari-bestiary-items/nDnP6GpXjTbXqbXB.htm)|Spike Trap|Piège à pointes|officielle|
|[NoYQle3HJatFQcgH.htm](troubles-in-otari-bestiary-items/NoYQle3HJatFQcgH.htm)|Noose|Noeud coulant|officielle|
|[nUWlISx8ev0qZpTG.htm](troubles-in-otari-bestiary-items/nUWlISx8ev0qZpTG.htm)|Low-Light Vision|Vision nocturne|officielle|
|[O9himtaJvrfCALhE.htm](troubles-in-otari-bestiary-items/O9himtaJvrfCALhE.htm)|Magical Starknife|Lamétoile magique|officielle|
|[OOCu4hNQg4NvyXLU.htm](troubles-in-otari-bestiary-items/OOCu4hNQg4NvyXLU.htm)|Arcane Prepared Spells|Sorts arcaniques préparés|libre|
|[OoNcWTdHVJ69lAop.htm](troubles-in-otari-bestiary-items/OoNcWTdHVJ69lAop.htm)|Diplomacy|Diplomatie|officielle|
|[OW9XOD4ssqRZAZk2.htm](troubles-in-otari-bestiary-items/OW9XOD4ssqRZAZk2.htm)|Diplomacy|Diplomatie|officielle|
|[pgQRipY9qrXPcNlY.htm](troubles-in-otari-bestiary-items/pgQRipY9qrXPcNlY.htm)|Darkvision|Vision dans le noir|officielle|
|[qEbFimPOeE5aFvE7.htm](troubles-in-otari-bestiary-items/qEbFimPOeE5aFvE7.htm)|Dagger|Dague|officielle|
|[QfnsBk0XQ6x9wJSe.htm](troubles-in-otari-bestiary-items/QfnsBk0XQ6x9wJSe.htm)|Quick Trap|Piège éclair|officielle|
|[QfWg8mYxqbJly84b.htm](troubles-in-otari-bestiary-items/QfWg8mYxqbJly84b.htm)|Shortsword|Épée courte|officielle|
|[qunm5Cfit6Q1T4dI.htm](troubles-in-otari-bestiary-items/qunm5Cfit6Q1T4dI.htm)|Wing Flash|Battement d'aile|officielle|
|[QVjozo4fqxCzzccc.htm](troubles-in-otari-bestiary-items/QVjozo4fqxCzzccc.htm)|Rapier|Rapière|officielle|
|[QZb6NNceGC3aMTMO.htm](troubles-in-otari-bestiary-items/QZb6NNceGC3aMTMO.htm)|Staff|Bâton|officielle|
|[Rocjsria9JUsNrFA.htm](troubles-in-otari-bestiary-items/Rocjsria9JUsNrFA.htm)|Athletics|Athlétisme|officielle|
|[STheFAvLWD42KLo9.htm](troubles-in-otari-bestiary-items/STheFAvLWD42KLo9.htm)|Nimble Dodge|Esquive agile|officielle|
|[symt5PZrNoJWeIqe.htm](troubles-in-otari-bestiary-items/symt5PZrNoJWeIqe.htm)|Fist|Poing|officielle|
|[u4infcHTr2ku9Uxi.htm](troubles-in-otari-bestiary-items/u4infcHTr2ku9Uxi.htm)|Spear|Lance|officielle|
|[uGR730eqkGcdJEfo.htm](troubles-in-otari-bestiary-items/uGR730eqkGcdJEfo.htm)|Javelin|Javeline|officielle|
|[UkurqY6wQGrS67eI.htm](troubles-in-otari-bestiary-items/UkurqY6wQGrS67eI.htm)|Divine Prepared Spells|Sorts divins préparés|libre|
|[UMd6rwUlzKFS3lvJ.htm](troubles-in-otari-bestiary-items/UMd6rwUlzKFS3lvJ.htm)|Javelin|Javeline|officielle|
|[VePco7JeJmUM7ijK.htm](troubles-in-otari-bestiary-items/VePco7JeJmUM7ijK.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[Vkudm63q6gEmBgGp.htm](troubles-in-otari-bestiary-items/Vkudm63q6gEmBgGp.htm)|Low-Light Vision|Vision nocturne|officielle|
|[vlVKxFijTHI7cMPh.htm](troubles-in-otari-bestiary-items/vlVKxFijTHI7cMPh.htm)|Necromancer|Nécromancien|officielle|
|[VmpGQOac5GRmdaDj.htm](troubles-in-otari-bestiary-items/VmpGQOac5GRmdaDj.htm)|Dagger|+1|Dague +1|officielle|
|[W6XkoyVR7QH9Z8lg.htm](troubles-in-otari-bestiary-items/W6XkoyVR7QH9Z8lg.htm)|Necromantic Defense|Défense nécromantique|officielle|
|[wBuoAb6EXYtmwPmo.htm](troubles-in-otari-bestiary-items/wBuoAb6EXYtmwPmo.htm)|Longsword|Épée longue|officielle|
|[Wigs6zeZoSmogj8P.htm](troubles-in-otari-bestiary-items/Wigs6zeZoSmogj8P.htm)|Seedpod|Gousse|officielle|
|[wlSTHbfmM751AGNy.htm](troubles-in-otari-bestiary-items/wlSTHbfmM751AGNy.htm)|Invisibility (Self Only)|Invisibilité (soi uniquement)|officielle|
|[WqgfgkwqUVMJK44b.htm](troubles-in-otari-bestiary-items/WqgfgkwqUVMJK44b.htm)|Jaws|Mâchoires|officielle|
|[wyzsbdPo28UWUYTD.htm](troubles-in-otari-bestiary-items/wyzsbdPo28UWUYTD.htm)|Unburdened Iron|Fer allégé|officielle|
|[xQTqtFFkEcJJWCDW.htm](troubles-in-otari-bestiary-items/xQTqtFFkEcJJWCDW.htm)|Wand of Fear (Level 1)|Baguette de Terreur (Niveau 1)|officielle|
|[YB4lJDTFGnMJZxPX.htm](troubles-in-otari-bestiary-items/YB4lJDTFGnMJZxPX.htm)|Jaws|Mâchoires|officielle|
|[YhlP3I6uuAc1wEik.htm](troubles-in-otari-bestiary-items/YhlP3I6uuAc1wEik.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[z7UOnDRw58SSvHLY.htm](troubles-in-otari-bestiary-items/z7UOnDRw58SSvHLY.htm)|Fist|Poing|officielle|
|[ZhDg0uKQhEzdCReZ.htm](troubles-in-otari-bestiary-items/ZhDg0uKQhEzdCReZ.htm)|Fist|Poing|officielle|
|[zhR9Ys8AyYNjAz29.htm](troubles-in-otari-bestiary-items/zhR9Ys8AyYNjAz29.htm)|Arcane Focus Spells|Sorts arcaniques focalisés|libre|
|[zNOKH6cGU4FaspAX.htm](troubles-in-otari-bestiary-items/zNOKH6cGU4FaspAX.htm)|Woodland Stride|Déplacement facilité en forêt|officielle|
|[ZxSGALYRVWtMBLTi.htm](troubles-in-otari-bestiary-items/ZxSGALYRVWtMBLTi.htm)|Darkvision|Vision dans le noir|officielle|
