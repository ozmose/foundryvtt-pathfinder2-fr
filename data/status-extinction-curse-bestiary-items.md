# État de la traduction (extinction-curse-bestiary-items)

 * **officielle**: 1399
 * **libre**: 134
 * **changé**: 4


Dernière mise à jour: 2023-07-12 20:28 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0oyBktgze70q4n6D.htm](extinction-curse-bestiary-items/0oyBktgze70q4n6D.htm)|Raking Claws|Griffes ratissantes|changé|
|[pHZZDAT6KIGrViZE.htm](extinction-curse-bestiary-items/pHZZDAT6KIGrViZE.htm)|Assimilate Lava|Assimiler la lave|changé|
|[QHfqtuROQMmVf7uq.htm](extinction-curse-bestiary-items/QHfqtuROQMmVf7uq.htm)|Unkillable|Intuable|changé|
|[wcALGN4qxbG1E6A4.htm](extinction-curse-bestiary-items/wcALGN4qxbG1E6A4.htm)|Unmasked Statues|Statues démasquées|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[03fiR3RK6Jw9Wv9p.htm](extinction-curse-bestiary-items/03fiR3RK6Jw9Wv9p.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|officielle|
|[05Ch36nkyu7Nm0e7.htm](extinction-curse-bestiary-items/05Ch36nkyu7Nm0e7.htm)|Spiked Gauntlet|Gantelet à pointes|officielle|
|[0ADnKFiKMszSKZaF.htm](extinction-curse-bestiary-items/0ADnKFiKMszSKZaF.htm)|Sneak Attack|Attaque sournoise|officielle|
|[0CIDFYX7xU5OaPcw.htm](extinction-curse-bestiary-items/0CIDFYX7xU5OaPcw.htm)|Convergent Tactics|Stratégie convergente|officielle|
|[0CogrGS0ASUBnsef.htm](extinction-curse-bestiary-items/0CogrGS0ASUBnsef.htm)|Sacrilegious Aura|Aura sacrilège|officielle|
|[0eCHhLgu7JC3do8q.htm](extinction-curse-bestiary-items/0eCHhLgu7JC3do8q.htm)|Abandon Body|Abandonner un corps|officielle|
|[0hTWvwnoEFyZWBzR.htm](extinction-curse-bestiary-items/0hTWvwnoEFyZWBzR.htm)|Telepathy 100 feet|Télépathie 30 m|officielle|
|[0inCgv8zE1zM339E.htm](extinction-curse-bestiary-items/0inCgv8zE1zM339E.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[0MA7GYvcLU7sgYtn.htm](extinction-curse-bestiary-items/0MA7GYvcLU7sgYtn.htm)|Whip|Fouet|officielle|
|[0OUF0y5EmFm2s0oD.htm](extinction-curse-bestiary-items/0OUF0y5EmFm2s0oD.htm)|Wizard School Spells|Sorts de l'école de magicien|libre|
|[0PmWWWaGPmE3r4bq.htm](extinction-curse-bestiary-items/0PmWWWaGPmE3r4bq.htm)|Longbow|Arc long|officielle|
|[0QAulbffblLQuA21.htm](extinction-curse-bestiary-items/0QAulbffblLQuA21.htm)|Constant Spells|Sorts constants|officielle|
|[0UbSYN1Y6Sk2mfjQ.htm](extinction-curse-bestiary-items/0UbSYN1Y6Sk2mfjQ.htm)|Beak Crunch|Bec mordeur|officielle|
|[0uWgmxpffv5a1h3J.htm](extinction-curse-bestiary-items/0uWgmxpffv5a1h3J.htm)|Detect Alignment (Constant) (Good Only)|Détection de l'alignement (constant, bon uniquement)|officielle|
|[0XWnW8LKuBLAUWDn.htm](extinction-curse-bestiary-items/0XWnW8LKuBLAUWDn.htm)|Face Mask|Masque facial|officielle|
|[1329nZKSKvV6X654.htm](extinction-curse-bestiary-items/1329nZKSKvV6X654.htm)|Consume Knowledge|Consommation de savoir|officielle|
|[13WOSIjbbyXFLN47.htm](extinction-curse-bestiary-items/13WOSIjbbyXFLN47.htm)|Uncanny Bombs|Bombes stupéfiantes|officielle|
|[14NkcqddU66Ux2ro.htm](extinction-curse-bestiary-items/14NkcqddU66Ux2ro.htm)|Jaws|Mâchoires|officielle|
|[14Udi3de3xejTJ37.htm](extinction-curse-bestiary-items/14Udi3de3xejTJ37.htm)|Calm Emotions (At Will)|Apaisement des émotions (À volonté)|officielle|
|[15cOjThlAU6xLAzc.htm](extinction-curse-bestiary-items/15cOjThlAU6xLAzc.htm)|Convergent Calm|Apaisement convergent|officielle|
|[16HnvP6u858vpixj.htm](extinction-curse-bestiary-items/16HnvP6u858vpixj.htm)|Tentacle|Tentacule|officielle|
|[1Aoz2hUdk0inH018.htm](extinction-curse-bestiary-items/1Aoz2hUdk0inH018.htm)|Familiar|Familier|officielle|
|[1cplu9lvCddcGPOx.htm](extinction-curse-bestiary-items/1cplu9lvCddcGPOx.htm)|Demonic Condemnation|Condamnation démoniaque|officielle|
|[1CtLUNvKIjzCyJIK.htm](extinction-curse-bestiary-items/1CtLUNvKIjzCyJIK.htm)|Religious Symbol of Zevgavizeb (Bone)|Symbol religieux en os de Zevgavizeb|officielle|
|[1d1wn0NQpxxTHN1s.htm](extinction-curse-bestiary-items/1d1wn0NQpxxTHN1s.htm)|Incredible Reload|Rechargement extraordinaire|officielle|
|[1FdVVIvFCyUYO42c.htm](extinction-curse-bestiary-items/1FdVVIvFCyUYO42c.htm)|Athletics|Athlétisme|officielle|
|[1fJFJOb1u8PBT1qs.htm](extinction-curse-bestiary-items/1fJFJOb1u8PBT1qs.htm)|Arcane Innate Spells|Sorts arcaniques innés|libre|
|[1FQhQGiVuDUtLiNn.htm](extinction-curse-bestiary-items/1FQhQGiVuDUtLiNn.htm)|Javelin|Javeline|officielle|
|[1GNw5dkroNexAH5F.htm](extinction-curse-bestiary-items/1GNw5dkroNexAH5F.htm)|Nimble Grazer|Brouteur agile|officielle|
|[1IjRwDjNaN8Y7Fm0.htm](extinction-curse-bestiary-items/1IjRwDjNaN8Y7Fm0.htm)|Religious Symbol of Zevgavizeb (Silver)|Symbole religieux en argent de Zevgavizeb|officielle|
|[1K62WMcAnmRMi30L.htm](extinction-curse-bestiary-items/1K62WMcAnmRMi30L.htm)|Hammer Mastery|Maîtrise du marteau|officielle|
|[1LKBXuy7BK2mG9vh.htm](extinction-curse-bestiary-items/1LKBXuy7BK2mG9vh.htm)|Negative Healing|Guérison négative|officielle|
|[1PpMA6kgFuFqrS88.htm](extinction-curse-bestiary-items/1PpMA6kgFuFqrS88.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|officielle|
|[1Rnw0tm3ktKN04Sv.htm](extinction-curse-bestiary-items/1Rnw0tm3ktKN04Sv.htm)|Primordial Balance|Équilibre primordial|officielle|
|[1sDrhVs1HdgK5SIw.htm](extinction-curse-bestiary-items/1sDrhVs1HdgK5SIw.htm)|Jaws|Mâchoires|officielle|
|[1SlSXQvnIyNY6mQu.htm](extinction-curse-bestiary-items/1SlSXQvnIyNY6mQu.htm)|Grab|Empoignade|officielle|
|[1uReN3TQSYYikfWz.htm](extinction-curse-bestiary-items/1uReN3TQSYYikfWz.htm)|Surprise Attack|Attaque surprise|officielle|
|[1W3SqzQpvwlpRf5W.htm](extinction-curse-bestiary-items/1W3SqzQpvwlpRf5W.htm)|Double Shot|Double tir|officielle|
|[1WyeUsZoxRGbP2Ad.htm](extinction-curse-bestiary-items/1WyeUsZoxRGbP2Ad.htm)|Gozreh Lore|Connaissance de Gozreh|officielle|
|[1x1JjlN10VDIIVb3.htm](extinction-curse-bestiary-items/1x1JjlN10VDIIVb3.htm)|Darkvision|Vision dans le noir|officielle|
|[20pfR59zCrylpO9C.htm](extinction-curse-bestiary-items/20pfR59zCrylpO9C.htm)|Mobility|Mobilté|officielle|
|[22saQWACDGfXp2ry.htm](extinction-curse-bestiary-items/22saQWACDGfXp2ry.htm)|Arcane Spontaneous Spells|Sorts arcaniques spontanés|libre|
|[2AI929eLb9JZQxCU.htm](extinction-curse-bestiary-items/2AI929eLb9JZQxCU.htm)|Swift Sneak|Furtivité rapide|officielle|
|[2aiIUhx9Z3yMKGyS.htm](extinction-curse-bestiary-items/2aiIUhx9Z3yMKGyS.htm)|Rage|Rage|officielle|
|[2arHY3THmPR4sHUA.htm](extinction-curse-bestiary-items/2arHY3THmPR4sHUA.htm)|Talons|Serres|officielle|
|[2bDfYtubnMMlCagj.htm](extinction-curse-bestiary-items/2bDfYtubnMMlCagj.htm)|Turn to Mist|Se changer en brume|officielle|
|[2CJo5tauCDhGyp1O.htm](extinction-curse-bestiary-items/2CJo5tauCDhGyp1O.htm)|Telepathy 100 feet|Télépathie 30 m|officielle|
|[2CTbAYtWJgwlBDPL.htm](extinction-curse-bestiary-items/2CTbAYtWJgwlBDPL.htm)|Grab|Empoignade|officielle|
|[2dOTpPLMKccRRnjc.htm](extinction-curse-bestiary-items/2dOTpPLMKccRRnjc.htm)|Devourer's Dictum|Précepte du Dévoreur|officielle|
|[2dsQhpuTqRGQXQLq.htm](extinction-curse-bestiary-items/2dsQhpuTqRGQXQLq.htm)|Quick Draw|Arme en main|officielle|
|[2f1qGs2WLtiERtrf.htm](extinction-curse-bestiary-items/2f1qGs2WLtiERtrf.htm)|Shadow Essence|Essence d'ombre|officielle|
|[2GPHZdvZjNrQX4S7.htm](extinction-curse-bestiary-items/2GPHZdvZjNrQX4S7.htm)|Vanished Alignment|Alignement volatilisé|officielle|
|[2GPOucqeB9jZndUB.htm](extinction-curse-bestiary-items/2GPOucqeB9jZndUB.htm)|Jaws|Mâchoires|officielle|
|[2HXsP310zFFnwcpi.htm](extinction-curse-bestiary-items/2HXsP310zFFnwcpi.htm)|Claw|Griffe|officielle|
|[2La0q0KxbfIUFD3k.htm](extinction-curse-bestiary-items/2La0q0KxbfIUFD3k.htm)|Faerie Fire (At Will)|Lueurs féeriques (À volonté)|officielle|
|[2LY21nrCeQ74PTkR.htm](extinction-curse-bestiary-items/2LY21nrCeQ74PTkR.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[2mOCKRrDP2pm8VSK.htm](extinction-curse-bestiary-items/2mOCKRrDP2pm8VSK.htm)|Necrotic Decay|Décomposition nécrotique|officielle|
|[2oeiSTDdWeqdi1Ew.htm](extinction-curse-bestiary-items/2oeiSTDdWeqdi1Ew.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[2oftzl4JLLJIvRRz.htm](extinction-curse-bestiary-items/2oftzl4JLLJIvRRz.htm)|Elaborate Feint|Feinte élaborée|officielle|
|[2Om4RqsiR8arN4hn.htm](extinction-curse-bestiary-items/2Om4RqsiR8arN4hn.htm)|Graveknight's Curse|Malédiction du chevalier sépulcre|officielle|
|[2SeK0haJzlBzrLGe.htm](extinction-curse-bestiary-items/2SeK0haJzlBzrLGe.htm)|Concentrated Xulgath Bile (Infused)|Bile xulgathe concentrée (imprégnée)|officielle|
|[2sSVjsfJJNodprfg.htm](extinction-curse-bestiary-items/2sSVjsfJJNodprfg.htm)|Change of Luck|La chance tourne|officielle|
|[2tVmuiOOVxmIhIT4.htm](extinction-curse-bestiary-items/2tVmuiOOVxmIhIT4.htm)|Furious Swing|Balayage furieux|officielle|
|[2UrfKw5PocQe3AE6.htm](extinction-curse-bestiary-items/2UrfKw5PocQe3AE6.htm)|Constant Spells|Sorts constants|officielle|
|[2WRsdtZcLSWodZG3.htm](extinction-curse-bestiary-items/2WRsdtZcLSWodZG3.htm)|Low-Light Vision|Vision nocturne|officielle|
|[2y98GhAzbFbtTv88.htm](extinction-curse-bestiary-items/2y98GhAzbFbtTv88.htm)|Bastard Sword|+2,greaterStriking|Épée bâtarde de frappe supérieure +2|libre|
|[2yAxIhu0gToaXv6n.htm](extinction-curse-bestiary-items/2yAxIhu0gToaXv6n.htm)|Evasion|Évasion|officielle|
|[2zQIifa5JMldE13S.htm](extinction-curse-bestiary-items/2zQIifa5JMldE13S.htm)|+26 when using Perception for Initiative|+26 lorsqu'il utilise la Perception pour l'Initiative|officielle|
|[30u18PLPEhoWfUTp.htm](extinction-curse-bestiary-items/30u18PLPEhoWfUTp.htm)|Claw|Griffe|officielle|
|[33EELhSSsT1qGqIx.htm](extinction-curse-bestiary-items/33EELhSSsT1qGqIx.htm)|Darkvision|Vision dans le noir|libre|
|[33Vq76XNGvlE9MHH.htm](extinction-curse-bestiary-items/33Vq76XNGvlE9MHH.htm)|Powerful Arm|Bras puissant|officielle|
|[34564bR2UkVUdEBz.htm](extinction-curse-bestiary-items/34564bR2UkVUdEBz.htm)|Maul|+1,striking|Maillet de Frappe +1|libre|
|[34SnogKLGJ3qYmNi.htm](extinction-curse-bestiary-items/34SnogKLGJ3qYmNi.htm)|Echoes of Aeons|Échos d'éons|officielle|
|[38UzCSLYydGU7NfZ.htm](extinction-curse-bestiary-items/38UzCSLYydGU7NfZ.htm)|Recoil from Wasted Opportunities|Contrecoup des opportunités ratées|officielle|
|[39iXJ3VDAsZ4Ij0P.htm](extinction-curse-bestiary-items/39iXJ3VDAsZ4Ij0P.htm)|Utter Despair|Grand désespoir|officielle|
|[3AW2a8B425pkI8WL.htm](extinction-curse-bestiary-items/3AW2a8B425pkI8WL.htm)|Flea Fever|Fièvre des puces|officielle|
|[3BzPIWSalYh96dFx.htm](extinction-curse-bestiary-items/3BzPIWSalYh96dFx.htm)|Wand of Magic Mouth (Level 2)|Baguette de Bouche magique (niveau 2)|officielle|
|[3cmVTpBQzoejUsbQ.htm](extinction-curse-bestiary-items/3cmVTpBQzoejUsbQ.htm)|Rock|Rocher|officielle|
|[3eMwI2ZGla9CVsIz.htm](extinction-curse-bestiary-items/3eMwI2ZGla9CVsIz.htm)|Constant Spells|Sorts constants|officielle|
|[3FngXmc5sXcStz5D.htm](extinction-curse-bestiary-items/3FngXmc5sXcStz5D.htm)|+1 Status Bonus to Saves vs. Magical Effects|bonus de statut de +1 aux JdS contre les effets magiques|officielle|
|[3fuKucSTNEuRZh4b.htm](extinction-curse-bestiary-items/3fuKucSTNEuRZh4b.htm)|Blood Squirt|Giclée de sang|officielle|
|[3gJM7nNe6xASowZz.htm](extinction-curse-bestiary-items/3gJM7nNe6xASowZz.htm)|Swallow Whole|Gober|officielle|
|[3h4TV4uROy8yVnxx.htm](extinction-curse-bestiary-items/3h4TV4uROy8yVnxx.htm)|Constant Spells|Sorts constants|officielle|
|[3IYBBjbLG10Rsmme.htm](extinction-curse-bestiary-items/3IYBBjbLG10Rsmme.htm)|Terrifying Sneer|Ricanement effrayant|officielle|
|[3khAQSOvCGGFmARp.htm](extinction-curse-bestiary-items/3khAQSOvCGGFmARp.htm)|Blightburn Radiation|Radiation de brûlure ardente|officielle|
|[3kILoZ5MVAOnGohC.htm](extinction-curse-bestiary-items/3kILoZ5MVAOnGohC.htm)|Scimitar|Cimeterre|officielle|
|[3LzPeJJxCoxD2Roq.htm](extinction-curse-bestiary-items/3LzPeJJxCoxD2Roq.htm)|Necrotic Decay|Décomposition nécrotique|officielle|
|[3O0SbByZ5mishfy0.htm](extinction-curse-bestiary-items/3O0SbByZ5mishfy0.htm)|Quick Bomber|Artificier rapide|officielle|
|[3pFc3GsLQcSTVkLk.htm](extinction-curse-bestiary-items/3pFc3GsLQcSTVkLk.htm)|Summon Animal (Dinosaurs Only)|Convocation d'animal (dinosaures uniquement)|officielle|
|[3pL0yHt4nvMJI7eK.htm](extinction-curse-bestiary-items/3pL0yHt4nvMJI7eK.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[3QVgbicaKeJdgy6l.htm](extinction-curse-bestiary-items/3QVgbicaKeJdgy6l.htm)|Tremorsense (imprecise) 30 feet|Perception des vibrations 9 m (imprécis)|officielle|
|[3rsC0fxsq7KltGj5.htm](extinction-curse-bestiary-items/3rsC0fxsq7KltGj5.htm)|+2 Resilient Leather Armor|Armure en cuir de résilience +2|officielle|
|[3sXjjaMPyL0UFfVY.htm](extinction-curse-bestiary-items/3sXjjaMPyL0UFfVY.htm)|Expeditious Evolution|Évolution expéditive|officielle|
|[3VYFcfVRP5S7MswK.htm](extinction-curse-bestiary-items/3VYFcfVRP5S7MswK.htm)|Jaws|Mâchoires|officielle|
|[3yrncDBmozmswIwg.htm](extinction-curse-bestiary-items/3yrncDBmozmswIwg.htm)|True Seeing (Constant)|Vision lucide (constant)|officielle|
|[3zIULjjGCvaxoCpI.htm](extinction-curse-bestiary-items/3zIULjjGCvaxoCpI.htm)|Staff|Bâton|officielle|
|[415YyiRgxhBj4QVH.htm](extinction-curse-bestiary-items/415YyiRgxhBj4QVH.htm)|Sulfurous Plume|Volute sulfureuse|officielle|
|[41uD7oGTc3XEcgP8.htm](extinction-curse-bestiary-items/41uD7oGTc3XEcgP8.htm)|Greater Frost Rapier|Rapière de froid supérieur|officielle|
|[46lzlqxwhzjHJYhD.htm](extinction-curse-bestiary-items/46lzlqxwhzjHJYhD.htm)|Disrupted Link|Lien interrompu|officielle|
|[47vQ5EzH5mCWzmvL.htm](extinction-curse-bestiary-items/47vQ5EzH5mCWzmvL.htm)|Baton Doused in Flammable Oil|Baguettes trempées dans l'huile inflammable|officielle|
|[49kZdXcigtGfmc19.htm](extinction-curse-bestiary-items/49kZdXcigtGfmc19.htm)|Constant Spells|Sorts constants|officielle|
|[4BgQ2Qd9NVWa3Hw1.htm](extinction-curse-bestiary-items/4BgQ2Qd9NVWa3Hw1.htm)|Gnashing Bite|Mastiquer|officielle|
|[4cj7pDegwTLGP1gx.htm](extinction-curse-bestiary-items/4cj7pDegwTLGP1gx.htm)|Invisibility (Self Only)|Invisibilité (soi uniquement)|officielle|
|[4Ct2SmjptIVcbVSy.htm](extinction-curse-bestiary-items/4Ct2SmjptIVcbVSy.htm)|Darkness (At Will)|Ténèbres (À volonté)|officielle|
|[4hmSKla48vJAAJG6.htm](extinction-curse-bestiary-items/4hmSKla48vJAAJG6.htm)|Tainted Guts|Tripes avariées|officielle|
|[4hQQHFYYXkIr2wtS.htm](extinction-curse-bestiary-items/4hQQHFYYXkIr2wtS.htm)|Pseudopod|Pseudopode|officielle|
|[4mA8PLTN8ZFWmcr6.htm](extinction-curse-bestiary-items/4mA8PLTN8ZFWmcr6.htm)|Furious Claws|Furie de griffes|officielle|
|[4nBcbBTCqRjCvM7x.htm](extinction-curse-bestiary-items/4nBcbBTCqRjCvM7x.htm)|Invisibility (At-Will) (Self Only)|Invisibilité (À volonté) (soi uniquement)|officielle|
|[4ooKJCagcx7gbeiK.htm](extinction-curse-bestiary-items/4ooKJCagcx7gbeiK.htm)|Compact|Compact|officielle|
|[4qK7Ch0rYRLpn8rm.htm](extinction-curse-bestiary-items/4qK7Ch0rYRLpn8rm.htm)|All-Around Vision|Vision panoramique|officielle|
|[4QuTQ1CVNw72E020.htm](extinction-curse-bestiary-items/4QuTQ1CVNw72E020.htm)|Jaws|Mâchoires|officielle|
|[4TqbrL1nWjV7igKG.htm](extinction-curse-bestiary-items/4TqbrL1nWjV7igKG.htm)|Black Desert Lore|Connaissance du Désert noir|officielle|
|[4v1QwNqequfwzIu9.htm](extinction-curse-bestiary-items/4v1QwNqequfwzIu9.htm)|Unholy Spiked Gauntlet|Gantelet à pointes impie|officielle|
|[4xdwD1vWSCPYxjVQ.htm](extinction-curse-bestiary-items/4xdwD1vWSCPYxjVQ.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[55Ue9KoElZXuS8Dc.htm](extinction-curse-bestiary-items/55Ue9KoElZXuS8Dc.htm)|Quick Bomber|Artificier rapide|officielle|
|[57yZt0sTPCTIwvyc.htm](extinction-curse-bestiary-items/57yZt0sTPCTIwvyc.htm)|Claw|Griffe|officielle|
|[5AIvyudQYSy8JIW9.htm](extinction-curse-bestiary-items/5AIvyudQYSy8JIW9.htm)|Claw|Griffe|officielle|
|[5ARBJjRyKKr0s71H.htm](extinction-curse-bestiary-items/5ARBJjRyKKr0s71H.htm)|At-Will Spells|Sorts à volonté|officielle|
|[5dfhS831WAAuyoAD.htm](extinction-curse-bestiary-items/5dfhS831WAAuyoAD.htm)|Composite Longbow|+1,striking|Arc long composite de frappe +1|officielle|
|[5dzqMDEkCOAmRjCF.htm](extinction-curse-bestiary-items/5dzqMDEkCOAmRjCF.htm)|Pack Attack|Attaque en meute|officielle|
|[5fE5X1TnaedkAQkG.htm](extinction-curse-bestiary-items/5fE5X1TnaedkAQkG.htm)|Fear (At-Will)|Terreur (À volonté)|officielle|
|[5gMfuCDBI4h3IyAu.htm](extinction-curse-bestiary-items/5gMfuCDBI4h3IyAu.htm)|Darkvision|Vision dans le noir|officielle|
|[5h3L7HCWGZnNodvy.htm](extinction-curse-bestiary-items/5h3L7HCWGZnNodvy.htm)|Bounce|Rebondir|officielle|
|[5hIqoXM1Pti9OonO.htm](extinction-curse-bestiary-items/5hIqoXM1Pti9OonO.htm)|Ill Omen|Mauvais augure|officielle|
|[5hkkuahwgVNIk8o8.htm](extinction-curse-bestiary-items/5hkkuahwgVNIk8o8.htm)|Wand of Heal (Level 2)|Baguette de Guérison (niveau 2)|officielle|
|[5IFayxoGlhzAgcEL.htm](extinction-curse-bestiary-items/5IFayxoGlhzAgcEL.htm)|Divine Focus Spells|Sorts divins focalisés|libre|
|[5JNkXeCqgTEkUhoN.htm](extinction-curse-bestiary-items/5JNkXeCqgTEkUhoN.htm)|Deny Advantage|Refus d'avantage|officielle|
|[5JvYtzfHEmAqqWPv.htm](extinction-curse-bestiary-items/5JvYtzfHEmAqqWPv.htm)|Illusory Scene (At Will)|Scène illusoire (À volonté)|officielle|
|[5NVqmIO25meno1io.htm](extinction-curse-bestiary-items/5NVqmIO25meno1io.htm)|Hand Crossbow|+2,greaterStriking|Arbalète de poing de frappe supérieure +2|libre|
|[5OiYfhHPstcj412O.htm](extinction-curse-bestiary-items/5OiYfhHPstcj412O.htm)|Attack of Opportunity (Special)|Attaque d'opportunité (Spéciale)|officielle|
|[5pSRrtmYXSwMkm8e.htm](extinction-curse-bestiary-items/5pSRrtmYXSwMkm8e.htm)|Low-Light Vision|Vision nocturne|officielle|
|[5pTxW5pvTdGLWj2C.htm](extinction-curse-bestiary-items/5pTxW5pvTdGLWj2C.htm)|Longspear|Pique|officielle|
|[5QcLOLT0zFzjb2Fg.htm](extinction-curse-bestiary-items/5QcLOLT0zFzjb2Fg.htm)|Hand Crossbow|Arbalète de poing|officielle|
|[5ShmRpRiadEdwNuC.htm](extinction-curse-bestiary-items/5ShmRpRiadEdwNuC.htm)|+2 Status to All Saves vs. Magical Effects|+2 de statut aux JdS contre les effets magiques|officielle|
|[5TKcGIGIKGyjkpnm.htm](extinction-curse-bestiary-items/5TKcGIGIKGyjkpnm.htm)|Drink Blood|Boire le sang|officielle|
|[5uceub19IdfqJ0Ze.htm](extinction-curse-bestiary-items/5uceub19IdfqJ0Ze.htm)|Warfare Lore|Connaissance de la guerre|officielle|
|[5Uoof6HFaymWjuTE.htm](extinction-curse-bestiary-items/5Uoof6HFaymWjuTE.htm)|Sneak Attack|Attaque sournoise|officielle|
|[5utWutqAL3Pfpee3.htm](extinction-curse-bestiary-items/5utWutqAL3Pfpee3.htm)|Stunning Blow|Coup étourdissant|officielle|
|[5VgxQkr436I8Ty46.htm](extinction-curse-bestiary-items/5VgxQkr436I8Ty46.htm)|At-Will Spells|Sorts à volonté|officielle|
|[5WvF0LR7Xr4lkeWV.htm](extinction-curse-bestiary-items/5WvF0LR7Xr4lkeWV.htm)|Composite Longbow|+2,greaterStriking|Arc long composite de frappe supérieure +2|libre|
|[5WzAbTjNTM4YElhM.htm](extinction-curse-bestiary-items/5WzAbTjNTM4YElhM.htm)|Channel Smite|Châtiment canalisé|officielle|
|[5XALtpIuUMlUiRTS.htm](extinction-curse-bestiary-items/5XALtpIuUMlUiRTS.htm)|Jaws|Mâchoires|officielle|
|[5yJ7XmHWutKmWc2L.htm](extinction-curse-bestiary-items/5yJ7XmHWutKmWc2L.htm)|Swallow Whole|Gober|officielle|
|[612zPFqgcul7ohBP.htm](extinction-curse-bestiary-items/612zPFqgcul7ohBP.htm)|Scalescribe|Runécailler|officielle|
|[62XRuG8BxfIDEFOw.htm](extinction-curse-bestiary-items/62XRuG8BxfIDEFOw.htm)|Tremorsense (Imprecise) 30 feet|Perception des vibrations 9 m (imprécis)|officielle|
|[65H1lnX2GYJaLpHs.htm](extinction-curse-bestiary-items/65H1lnX2GYJaLpHs.htm)|Darkvision|Vision dans le noir|officielle|
|[68ILx13H6RgkuEJO.htm](extinction-curse-bestiary-items/68ILx13H6RgkuEJO.htm)|Improved Knockdown|Renversement amélioré|officielle|
|[69tw4cowol64x682.htm](extinction-curse-bestiary-items/69tw4cowol64x682.htm)|Darkvision|Vision dans le noir|officielle|
|[6coOtGICrhXGQ5MW.htm](extinction-curse-bestiary-items/6coOtGICrhXGQ5MW.htm)|Steady Spellcasting|Incantation fiable|officielle|
|[6CRzO3g1vhWEqvny.htm](extinction-curse-bestiary-items/6CRzO3g1vhWEqvny.htm)|Sickle|Serpe|officielle|
|[6CvKZvykGC9DRN7Z.htm](extinction-curse-bestiary-items/6CvKZvykGC9DRN7Z.htm)|Deny Advantage|Refus d'avantage|officielle|
|[6DipIrHqdCiJN01W.htm](extinction-curse-bestiary-items/6DipIrHqdCiJN01W.htm)|Dagger|Dague|officielle|
|[6fCsQA7Mbw0rYh0L.htm](extinction-curse-bestiary-items/6fCsQA7Mbw0rYh0L.htm)|Tanglefoot Bag (Greater) (Infused)|Sacoche immobilisante supérieure [imprégné]|officielle|
|[6H5o1EKmMK9GLW0X.htm](extinction-curse-bestiary-items/6H5o1EKmMK9GLW0X.htm)|Scroll of Unrelenting Observation (Level 8)|Parchemin d'Observation implacable (Niveau 8)|officielle|
|[6Iz60sgKIQyvGdXg.htm](extinction-curse-bestiary-items/6Iz60sgKIQyvGdXg.htm)|Cowardly|Lâche|officielle|
|[6j7TXX4foQ6J6jT6.htm](extinction-curse-bestiary-items/6j7TXX4foQ6J6jT6.htm)|+1 Striking Mancatcher|+1,striking|Attrape-coquin de frappe +1|officielle|
|[6JEzupbVB1uGFoq4.htm](extinction-curse-bestiary-items/6JEzupbVB1uGFoq4.htm)|Disrupted Link|Lien interrompu|officielle|
|[6jthkAwvS3DHh7jF.htm](extinction-curse-bestiary-items/6jthkAwvS3DHh7jF.htm)|Summon Monster|Convoquer un monstre|officielle|
|[6lzGDph9WZ9msyJq.htm](extinction-curse-bestiary-items/6lzGDph9WZ9msyJq.htm)|Arrow Vulnerability|Vulnérabilité aux flèches|officielle|
|[6nV4lSGXCgZTjigM.htm](extinction-curse-bestiary-items/6nV4lSGXCgZTjigM.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[6pnFCJxQ1BPSdZfN.htm](extinction-curse-bestiary-items/6pnFCJxQ1BPSdZfN.htm)|Darkvision|Vision dans le noir|officielle|
|[6Q3lgvUWzPKUKxeH.htm](extinction-curse-bestiary-items/6Q3lgvUWzPKUKxeH.htm)|Expert Alchemist's Tools|Outils d'alchimiste experts|officielle|
|[6qV8HNnpHA2TjjBA.htm](extinction-curse-bestiary-items/6qV8HNnpHA2TjjBA.htm)|Acid Flask (Greater) (Infused)|Fiole d'acide supérieure (imprégnée)|officielle|
|[6SbGSgbpCYxPYfsS.htm](extinction-curse-bestiary-items/6SbGSgbpCYxPYfsS.htm)|Lifesense (Imprecise) 60 feet|Perception de la vie 18 m (imprécis)|officielle|
|[6swZIa9vyXdVDpwQ.htm](extinction-curse-bestiary-items/6swZIa9vyXdVDpwQ.htm)|Obsidian Sliver|Éclat d'obsidienne|officielle|
|[6T41uBvYSdsd3etb.htm](extinction-curse-bestiary-items/6T41uBvYSdsd3etb.htm)|Congealed|Solidification|officielle|
|[6tlcY6vJ6xRLoy23.htm](extinction-curse-bestiary-items/6tlcY6vJ6xRLoy23.htm)|Longsword|+3,majorStriking|Épée longue de frappe majeure +3|libre|
|[6Ue9mTJcWViQYqOR.htm](extinction-curse-bestiary-items/6Ue9mTJcWViQYqOR.htm)|Jaws|Mâchoires|officielle|
|[6ufwCMehbagfms2J.htm](extinction-curse-bestiary-items/6ufwCMehbagfms2J.htm)|Set on Fire|Mettre le feu|officielle|
|[6uHq8MgU7xzJ6vGW.htm](extinction-curse-bestiary-items/6uHq8MgU7xzJ6vGW.htm)|Sprinkle Pixie Dust|Saupoudrer de la poussière de pixie|officielle|
|[6ULxQITrncDRTf0g.htm](extinction-curse-bestiary-items/6ULxQITrncDRTf0g.htm)|Clown Costume|Costume de clown|officielle|
|[6Ww4Qkldv49LbeZh.htm](extinction-curse-bestiary-items/6Ww4Qkldv49LbeZh.htm)|Crystalline Construction|Construction cristalline|officielle|
|[6x5ndW51ukQRdEkd.htm](extinction-curse-bestiary-items/6x5ndW51ukQRdEkd.htm)|Darkvision|Vision dans le noir|officielle|
|[6yZT0Oua0CQQmx9r.htm](extinction-curse-bestiary-items/6yZT0Oua0CQQmx9r.htm)|Alchemist's Fire (Greater) (Infused)|Feu grégeois supérieur (imprégné)|officielle|
|[6ZhIbuBKgvn1KAfr.htm](extinction-curse-bestiary-items/6ZhIbuBKgvn1KAfr.htm)|Constrict|Constriction|officielle|
|[70GgLhQiyd0TPrVN.htm](extinction-curse-bestiary-items/70GgLhQiyd0TPrVN.htm)|Hoof|Sabot|officielle|
|[75B144VmeAAs88N3.htm](extinction-curse-bestiary-items/75B144VmeAAs88N3.htm)|Grab|Empoignade|officielle|
|[7601xuNSze8YXgHA.htm](extinction-curse-bestiary-items/7601xuNSze8YXgHA.htm)|Shortsword|Épée courte|officielle|
|[76GZfG5yXvb8EAyU.htm](extinction-curse-bestiary-items/76GZfG5yXvb8EAyU.htm)|Jaws|Mâchoires|officielle|
|[7b3GNADyuxWoQu4e.htm](extinction-curse-bestiary-items/7b3GNADyuxWoQu4e.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[7C6yRhAx3CPR1NFc.htm](extinction-curse-bestiary-items/7C6yRhAx3CPR1NFc.htm)|Whip Vulnerability|Vulnérabilité au fouet|officielle|
|[7D7K3q9IS7T2K11z.htm](extinction-curse-bestiary-items/7D7K3q9IS7T2K11z.htm)|Sneak Attack|Attaque sournoise|officielle|
|[7e908l7OkfXJHtHN.htm](extinction-curse-bestiary-items/7e908l7OkfXJHtHN.htm)|Animal Vision (At Will) (Dinosaurs Only)|Vision animale (À volonté) (dinosaures uniquement)|officielle|
|[7HIDyV0Dl364Jch8.htm](extinction-curse-bestiary-items/7HIDyV0Dl364Jch8.htm)|Foot|Patte|officielle|
|[7HpYZ4qlfqXrnAeX.htm](extinction-curse-bestiary-items/7HpYZ4qlfqXrnAeX.htm)|Bastard Sword|Épée bâtarde|officielle|
|[7I2FYqAcQ3SORicy.htm](extinction-curse-bestiary-items/7I2FYqAcQ3SORicy.htm)|Grab|Empoignade|officielle|
|[7jb5XLgHQFBJzvra.htm](extinction-curse-bestiary-items/7jb5XLgHQFBJzvra.htm)|Constant Spells|Sorts constants|officielle|
|[7jGgiwswo0feOhqx.htm](extinction-curse-bestiary-items/7jGgiwswo0feOhqx.htm)|Claw|Griffe|officielle|
|[7JQkP6acnn4UB5Dt.htm](extinction-curse-bestiary-items/7JQkP6acnn4UB5Dt.htm)|Malevolent Possession|Possession malveillante|officielle|
|[7PZLjnzWIYsyU8k9.htm](extinction-curse-bestiary-items/7PZLjnzWIYsyU8k9.htm)|Greatclub|+1,striking|Massue de frappe +1|libre|
|[7rfez4zvg21jamKH.htm](extinction-curse-bestiary-items/7rfez4zvg21jamKH.htm)|Ankylostar|Ankylostern|officielle|
|[7wtKLLAt30PdWUEX.htm](extinction-curse-bestiary-items/7wtKLLAt30PdWUEX.htm)|Ghostly Bite|Morsure spectrale|officielle|
|[7XUkm64tCN25G6VB.htm](extinction-curse-bestiary-items/7XUkm64tCN25G6VB.htm)|Candle Fingers|Doigts-bougies|officielle|
|[7XZ5d79YD3f0pWUk.htm](extinction-curse-bestiary-items/7XZ5d79YD3f0pWUk.htm)|Reactive Slime|Mucus réactif|officielle|
|[7yOLigTAGYofVRkH.htm](extinction-curse-bestiary-items/7yOLigTAGYofVRkH.htm)|Channel Rot|Canalisation de la putréfaction|officielle|
|[7YxbvFT8BdjHaBTM.htm](extinction-curse-bestiary-items/7YxbvFT8BdjHaBTM.htm)|Arcane Prepared Spells|Sorts arcaniques préparés|libre|
|[7YxHt2qByZ37NDj5.htm](extinction-curse-bestiary-items/7YxHt2qByZ37NDj5.htm)|Unfathomable Aspect|Apparence insondable|officielle|
|[7Z7WDMHFDrNJcL7D.htm](extinction-curse-bestiary-items/7Z7WDMHFDrNJcL7D.htm)|At-Will Spells|Sorts à volonté|officielle|
|[7zPJ2oI0YtpNMQiH.htm](extinction-curse-bestiary-items/7zPJ2oI0YtpNMQiH.htm)|Thorny Branch|Branche épineuse|officielle|
|[85ZFVNdum6WrNAnT.htm](extinction-curse-bestiary-items/85ZFVNdum6WrNAnT.htm)|Telepathy 100 feet|Télépathie 30 m|officielle|
|[88vgxSTx2c1kRrzY.htm](extinction-curse-bestiary-items/88vgxSTx2c1kRrzY.htm)|Claw|Griffe|officielle|
|[89iNsonBI863Pvyb.htm](extinction-curse-bestiary-items/89iNsonBI863Pvyb.htm)|Sack of Spiders|Sac d'araignées|officielle|
|[8aMnXRh8CULYzKV8.htm](extinction-curse-bestiary-items/8aMnXRh8CULYzKV8.htm)|Ignite Baton|Enflammer une baguette|officielle|
|[8arXWMYazzSvR5im.htm](extinction-curse-bestiary-items/8arXWMYazzSvR5im.htm)|Constant Spells|Sorts constants|officielle|
|[8aV587HlWQ9O8qCx.htm](extinction-curse-bestiary-items/8aV587HlWQ9O8qCx.htm)|Necklace of Bone and Gems|Collier d’os et de pierres précieuses|officielle|
|[8GwhfcNZYW7M1YqR.htm](extinction-curse-bestiary-items/8GwhfcNZYW7M1YqR.htm)|Anatomy Lore|Connaissance de l'anatomie|officielle|
|[8JSEG6z9Sf6PnMGY.htm](extinction-curse-bestiary-items/8JSEG6z9Sf6PnMGY.htm)|Frightful Presence|Présence terrifiante|officielle|
|[8kQ7IgMQlvRAyicP.htm](extinction-curse-bestiary-items/8kQ7IgMQlvRAyicP.htm)|Earthen Torrent|Torrent de terre|officielle|
|[8LWkgJLzQH5BuUe3.htm](extinction-curse-bestiary-items/8LWkgJLzQH5BuUe3.htm)|Attack of Opportunity (Special)|Attaque d'opportunité (Spéciale)|officielle|
|[8lXF29Na3oJh3PrL.htm](extinction-curse-bestiary-items/8lXF29Na3oJh3PrL.htm)|Forest Lore|Connaissance des forêts|officielle|
|[8NbpXctXpdSLejVt.htm](extinction-curse-bestiary-items/8NbpXctXpdSLejVt.htm)|Power Attack|Attaque en puissance|officielle|
|[8pEUtQIaEuhNMeNq.htm](extinction-curse-bestiary-items/8pEUtQIaEuhNMeNq.htm)|Falling Stone Unarmed Attack|Attaque à mains nues Chute de pierre|officielle|
|[8rZVMVE5e5IobbkP.htm](extinction-curse-bestiary-items/8rZVMVE5e5IobbkP.htm)|Javelin|Javeline|officielle|
|[8T1lphW5agh8iPDk.htm](extinction-curse-bestiary-items/8T1lphW5agh8iPDk.htm)|Sudden Charge|Charge soudaine|officielle|
|[8uTem9WAYzYRXxjq.htm](extinction-curse-bestiary-items/8uTem9WAYzYRXxjq.htm)|Tongues (Constant)|Don des langues (constant)|officielle|
|[8UWmcpWRaJbBvrgi.htm](extinction-curse-bestiary-items/8UWmcpWRaJbBvrgi.htm)|Worry Prey|Malmener sa proie|officielle|
|[8vEzM2MigepekMyc.htm](extinction-curse-bestiary-items/8vEzM2MigepekMyc.htm)|Alchemist's Fire (Moderate)|Feu grégeois (Moyen)|officielle|
|[8z8uIcobom68tmyS.htm](extinction-curse-bestiary-items/8z8uIcobom68tmyS.htm)|Deepest Fear|Plus grande peur|officielle|
|[92xnULMmOwNxGCpN.htm](extinction-curse-bestiary-items/92xnULMmOwNxGCpN.htm)|Death Stench|Puanteur de la mort|officielle|
|[938PC6CKpMMlv4b7.htm](extinction-curse-bestiary-items/938PC6CKpMMlv4b7.htm)|Earthen Blow|Souffle de terre|officielle|
|[954oCjihi0rQxaMN.htm](extinction-curse-bestiary-items/954oCjihi0rQxaMN.htm)|Paranoia (Animals Only)|Paranoïa (animaux uniquement)|officielle|
|[98XSigNoV6MVwImS.htm](extinction-curse-bestiary-items/98XSigNoV6MVwImS.htm)|Acid Flask (Greater) [Infused]|Fiole d'acide supérieure (imprégnée)|officielle|
|[9ClztbAmAcGhe6zm.htm](extinction-curse-bestiary-items/9ClztbAmAcGhe6zm.htm)|Constant Spells|Sorts constants|officielle|
|[9GD9cU7HYaHGStvx.htm](extinction-curse-bestiary-items/9GD9cU7HYaHGStvx.htm)|Darkvision|Vision dans le noir|officielle|
|[9gQhAfAUHKZwPfXQ.htm](extinction-curse-bestiary-items/9gQhAfAUHKZwPfXQ.htm)|Jaws|Mâchoires|officielle|
|[9JJx0yI602CpXiOj.htm](extinction-curse-bestiary-items/9JJx0yI602CpXiOj.htm)|Darkvision|Vision dans le noir|officielle|
|[9KjQBibdPnAX1HTL.htm](extinction-curse-bestiary-items/9KjQBibdPnAX1HTL.htm)|Stone Throes|Affres de la pierre|officielle|
|[9LeI99p7iFnhng1g.htm](extinction-curse-bestiary-items/9LeI99p7iFnhng1g.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[9MkG2PZhLavYiFIf.htm](extinction-curse-bestiary-items/9MkG2PZhLavYiFIf.htm)|Telepathy 100 feet|Télépathie 30 m|officielle|
|[9mMqegSFLDuJ14KD.htm](extinction-curse-bestiary-items/9mMqegSFLDuJ14KD.htm)|Claw|Griffe|officielle|
|[9OdQ3Zp6ZN6b2hx5.htm](extinction-curse-bestiary-items/9OdQ3Zp6ZN6b2hx5.htm)|Constant Spells|Sorts constants|officielle|
|[9P7DE9pnYbvjzjog.htm](extinction-curse-bestiary-items/9P7DE9pnYbvjzjog.htm)|Ranseur|Corsèque|officielle|
|[9r5OSSnhL1XGFAvn.htm](extinction-curse-bestiary-items/9r5OSSnhL1XGFAvn.htm)|Gray Robes|Robe grise|officielle|
|[9rFk9LIKScKcgEaC.htm](extinction-curse-bestiary-items/9rFk9LIKScKcgEaC.htm)|Air Walk (Constant)|Marche dans les airs (constant)|officielle|
|[9siPff9BhSHUe2TK.htm](extinction-curse-bestiary-items/9siPff9BhSHUe2TK.htm)|Scroll of False Life (Level 2)|Parchemin de Simulacre de vie (Niveau 2)|officielle|
|[9TRBVjnPJbPXtMlt.htm](extinction-curse-bestiary-items/9TRBVjnPJbPXtMlt.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|officielle|
|[9tVCotEnUfgqibXT.htm](extinction-curse-bestiary-items/9tVCotEnUfgqibXT.htm)|Stone Throes|Affres de la pierre|officielle|
|[9UJyODZuM14ccZBZ.htm](extinction-curse-bestiary-items/9UJyODZuM14ccZBZ.htm)|Fist|Poing|officielle|
|[9vk0rdEFsd8p86wX.htm](extinction-curse-bestiary-items/9vk0rdEFsd8p86wX.htm)|Major Mentalist's Staff|Bâton du mentaliste majeur|libre|
|[9vz4EyPOnZrxMkmn.htm](extinction-curse-bestiary-items/9vz4EyPOnZrxMkmn.htm)|Jaws|Mâchoires|officielle|
|[9WtQUWj0NxeFpth4.htm](extinction-curse-bestiary-items/9WtQUWj0NxeFpth4.htm)|Quick Catch|Rattrapé au vol|officielle|
|[9ysQBDvKbsfYCZAe.htm](extinction-curse-bestiary-items/9ysQBDvKbsfYCZAe.htm)|Rapier|+1|Rapière +1|libre|
|[9yZ9mazhUqzLcQR7.htm](extinction-curse-bestiary-items/9yZ9mazhUqzLcQR7.htm)|Mobility|Mobilité|officielle|
|[a06YRZqcMZjOHFeJ.htm](extinction-curse-bestiary-items/a06YRZqcMZjOHFeJ.htm)|Spellbook|Grimoire|officielle|
|[a0KvieVyT5Xohz5m.htm](extinction-curse-bestiary-items/a0KvieVyT5Xohz5m.htm)|Shield Warden|Gardien au bouclier|officielle|
|[a0TSS06q0DBjSQ3n.htm](extinction-curse-bestiary-items/a0TSS06q0DBjSQ3n.htm)|At-Will Spells|Sorts à volonté|officielle|
|[a4SSMVjAQgcwes1C.htm](extinction-curse-bestiary-items/a4SSMVjAQgcwes1C.htm)|Muscle Striker|Muscle endommagé|officielle|
|[A4XBLOuiaS91sKg0.htm](extinction-curse-bestiary-items/A4XBLOuiaS91sKg0.htm)|Digging Bar|Tige de forage|officielle|
|[a5hbLlCGgZWkEv4t.htm](extinction-curse-bestiary-items/a5hbLlCGgZWkEv4t.htm)|Smoke Exhalation|Souffle de fumée|officielle|
|[aamOVZjUNF7bmH68.htm](extinction-curse-bestiary-items/aamOVZjUNF7bmH68.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[ab7DMqaYPHB6wIhC.htm](extinction-curse-bestiary-items/ab7DMqaYPHB6wIhC.htm)|Foot|Patte|officielle|
|[AbffdpOVV9q9QwtQ.htm](extinction-curse-bestiary-items/AbffdpOVV9q9QwtQ.htm)|Aggressive Rush|Ruée agressive|officielle|
|[aBlnDa69uYoI9jdA.htm](extinction-curse-bestiary-items/aBlnDa69uYoI9jdA.htm)|Sleep (At Will)|Sommeil (À volonté)|officielle|
|[aBnc125GEiD84WGM.htm](extinction-curse-bestiary-items/aBnc125GEiD84WGM.htm)|Ruinous Weapons|Armes du désastre|officielle|
|[aBNPRaWuVV6WJ41a.htm](extinction-curse-bestiary-items/aBNPRaWuVV6WJ41a.htm)|Spiked Gauntlet|Gantelet à pointes|officielle|
|[AcW2qid0dvfeGVR0.htm](extinction-curse-bestiary-items/AcW2qid0dvfeGVR0.htm)|Fist|Poing|officielle|
|[aCZG1swHw2Dy22DT.htm](extinction-curse-bestiary-items/aCZG1swHw2Dy22DT.htm)|Blunt Snout|Truffe épatée|officielle|
|[af6LBcRSbi6EeWEW.htm](extinction-curse-bestiary-items/af6LBcRSbi6EeWEW.htm)|Falling Rocks|Chute de roches|officielle|
|[afriJ8KUeqfph4Ra.htm](extinction-curse-bestiary-items/afriJ8KUeqfph4Ra.htm)|Rhoka Sword|Épée rhoka|officielle|
|[aGULlUDAVR1BMKC7.htm](extinction-curse-bestiary-items/aGULlUDAVR1BMKC7.htm)|Smoke Vision|Vision malgré la fumée|officielle|
|[aGUovHznkIyYG8m6.htm](extinction-curse-bestiary-items/aGUovHznkIyYG8m6.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[AHRPf1UGPJOGMM9l.htm](extinction-curse-bestiary-items/AHRPf1UGPJOGMM9l.htm)|Dispel Magic (At Will)|Dissipation de la magie (À volonté)|officielle|
|[aJky1rLHdCQPoA58.htm](extinction-curse-bestiary-items/aJky1rLHdCQPoA58.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|officielle|
|[AJPHV58xJ71hC3xz.htm](extinction-curse-bestiary-items/AJPHV58xJ71hC3xz.htm)|Keys to All the Cages in the Glen of Uncommon Wonders|Clés de toutes les cages du vallon des Merveilles hors du commun|officielle|
|[AKIZnRoLY7lsf9VG.htm](extinction-curse-bestiary-items/AKIZnRoLY7lsf9VG.htm)|Wicked Bite|Méchante morsure|officielle|
|[aKtsu17liUh7DC0t.htm](extinction-curse-bestiary-items/aKtsu17liUh7DC0t.htm)|Grab|Empoignade|officielle|
|[An5Bh0snG4EiSy6u.htm](extinction-curse-bestiary-items/An5Bh0snG4EiSy6u.htm)|Trunk Beam|Trompe lumineuse|officielle|
|[AnbtNKsICChPCBHe.htm](extinction-curse-bestiary-items/AnbtNKsICChPCBHe.htm)|Fling Wax|Lancer de cire|officielle|
|[aNQI6UJBZemWiBCa.htm](extinction-curse-bestiary-items/aNQI6UJBZemWiBCa.htm)|Telepathy 100 feet|Télépathie 30 m|officielle|
|[ANQsTNA3uzOJTguT.htm](extinction-curse-bestiary-items/ANQsTNA3uzOJTguT.htm)|Gray Robes|Robe grise|officielle|
|[aoirwF9wgYMRSHdg.htm](extinction-curse-bestiary-items/aoirwF9wgYMRSHdg.htm)|Divine Prepared Spells|Sorts divins préparés|libre|
|[aoZIcdrhcrNGcXgG.htm](extinction-curse-bestiary-items/aoZIcdrhcrNGcXgG.htm)|Composite Longbow|Arc long composite|officielle|
|[apbGXdheHVHXOiQZ.htm](extinction-curse-bestiary-items/apbGXdheHVHXOiQZ.htm)|Darkvision|Vision dans le noir|libre|
|[AR8S1f2bVJPU2t0I.htm](extinction-curse-bestiary-items/AR8S1f2bVJPU2t0I.htm)|Coffin Restoration|Cercueil restaurateur|officielle|
|[asg1peHnq8Kfjber.htm](extinction-curse-bestiary-items/asg1peHnq8Kfjber.htm)|Eagle Garrison Badge|Insigne de la Garnison de l’aigle|officielle|
|[aT0ulfinBIPpvzuM.htm](extinction-curse-bestiary-items/aT0ulfinBIPpvzuM.htm)|Hampering Blow|Coup entravant|officielle|
|[AUdWGOamBXZXat3B.htm](extinction-curse-bestiary-items/AUdWGOamBXZXat3B.htm)|Consume Flesh|Dévorer la chair|officielle|
|[AvIzHasAT7nWYhzG.htm](extinction-curse-bestiary-items/AvIzHasAT7nWYhzG.htm)|Athletics|Athlétisme|officielle|
|[aVumT4gAl6v4gbG0.htm](extinction-curse-bestiary-items/aVumT4gAl6v4gbG0.htm)|Sorcerer Bloodline Spells|Sorts de lignage d'ensorceleur|libre|
|[aWpXug8seWiT6d81.htm](extinction-curse-bestiary-items/aWpXug8seWiT6d81.htm)|Dagger|Dague|officielle|
|[AwQS25ihKMRo7aXf.htm](extinction-curse-bestiary-items/AwQS25ihKMRo7aXf.htm)|Tail|Queue|officielle|
|[aWZBvHQ5pBO8L1iS.htm](extinction-curse-bestiary-items/aWZBvHQ5pBO8L1iS.htm)|Composite Shortbow|Arc court composite|officielle|
|[ax83ccfFLMazkzxs.htm](extinction-curse-bestiary-items/ax83ccfFLMazkzxs.htm)|Powerful Stench|Puissante puanteur|officielle|
|[Axi77dW9fWlKe5ct.htm](extinction-curse-bestiary-items/Axi77dW9fWlKe5ct.htm)|Pot of Grease Paint|Pot de fard|officielle|
|[AYJVxqIbMbM6Hom4.htm](extinction-curse-bestiary-items/AYJVxqIbMbM6Hom4.htm)|Rhoka Sword|+2,striking|Épée rhoka de frappe +2|libre|
|[AYL0s7HCCgpgbmtO.htm](extinction-curse-bestiary-items/AYL0s7HCCgpgbmtO.htm)|Alluring Aspect|Apparence hypnotisante|officielle|
|[AYucJ5sk37mn8r87.htm](extinction-curse-bestiary-items/AYucJ5sk37mn8r87.htm)|Low-Light Vision|Vision nocturne|officielle|
|[AYvQsWHdPY8qJXTz.htm](extinction-curse-bestiary-items/AYvQsWHdPY8qJXTz.htm)|Wasting Wound|Blessure débilitante|officielle|
|[B0RufxivwZqPbB6R.htm](extinction-curse-bestiary-items/B0RufxivwZqPbB6R.htm)|Trample|Piétinement|officielle|
|[b0yCrKSgbslsUe5W.htm](extinction-curse-bestiary-items/b0yCrKSgbslsUe5W.htm)|Dagger|Dague|officielle|
|[b1DiEoXUNFfYIgRG.htm](extinction-curse-bestiary-items/b1DiEoXUNFfYIgRG.htm)|Dagger|Dague|officielle|
|[B1S8agx9nAGw4Bxt.htm](extinction-curse-bestiary-items/B1S8agx9nAGw4Bxt.htm)|Negative Healing|Guérison négative|officielle|
|[Ba3OUInVSq7YCdrO.htm](extinction-curse-bestiary-items/Ba3OUInVSq7YCdrO.htm)|Mounted Bow Expert|Cavalier expert du tir à l'arc|officielle|
|[bAw4bSovV5qrCsAx.htm](extinction-curse-bestiary-items/bAw4bSovV5qrCsAx.htm)|Low-Light Vision|Vision nocturne|officielle|
|[bB3qQlo5VdUbfeQh.htm](extinction-curse-bestiary-items/bB3qQlo5VdUbfeQh.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[bc9ZTQWLulMjdnJ5.htm](extinction-curse-bestiary-items/bc9ZTQWLulMjdnJ5.htm)|Haywire|Se détraquer|officielle|
|[bClPOSeIXlOZbjEV.htm](extinction-curse-bestiary-items/bClPOSeIXlOZbjEV.htm)|Iron Mind|Esprit de fer|officielle|
|[bcmrCRt6L0KXk4iE.htm](extinction-curse-bestiary-items/bcmrCRt6L0KXk4iE.htm)|Claw|Griffe|officielle|
|[bcrG00wkCK5L7uZE.htm](extinction-curse-bestiary-items/bcrG00wkCK5L7uZE.htm)|Negative Healing|Guérison négative|officielle|
|[BcrVzHc6oLVuEmXs.htm](extinction-curse-bestiary-items/BcrVzHc6oLVuEmXs.htm)|Grab|Empoignade|officielle|
|[BD0WtU1mZFhFSCn4.htm](extinction-curse-bestiary-items/BD0WtU1mZFhFSCn4.htm)|Hardscale Shield Stance|Posture de défense de dure-écaille|officielle|
|[bd8O12QXeS6Usuz1.htm](extinction-curse-bestiary-items/bd8O12QXeS6Usuz1.htm)|Telepathy 100 feet|Télépathie 30 m|officielle|
|[beKDaFwTUAjBlXuY.htm](extinction-curse-bestiary-items/beKDaFwTUAjBlXuY.htm)|Phantom Pain (At Will)|Douleur fantôme (À volonté)|officielle|
|[BEMhZJteQTj5sUGM.htm](extinction-curse-bestiary-items/BEMhZJteQTj5sUGM.htm)|Engulf|Engloutir|officielle|
|[BeWl7Ot39HAvDQoK.htm](extinction-curse-bestiary-items/BeWl7Ot39HAvDQoK.htm)|Status Bonus to Saves|bonus de statut aux jets de sauvegarde|officielle|
|[BFPXxBgFh6cenPgG.htm](extinction-curse-bestiary-items/BFPXxBgFh6cenPgG.htm)|Vulnerable to Rust|Vulnérabilité à la rouille|officielle|
|[bgLBaYc65Aq8OxQ2.htm](extinction-curse-bestiary-items/bgLBaYc65Aq8OxQ2.htm)|At-Will Spells|Sorts à volonté|officielle|
|[bGV9QKdEo62zbwAD.htm](extinction-curse-bestiary-items/bGV9QKdEo62zbwAD.htm)|+1 Resilient Bone Armor|Armure d'os de résilience +1|officielle|
|[bHjyGlr68JjtV7RP.htm](extinction-curse-bestiary-items/bHjyGlr68JjtV7RP.htm)|Shred Armor|Broyer l'armure|officielle|
|[bHxBiwRHApJXOQTs.htm](extinction-curse-bestiary-items/bHxBiwRHApJXOQTs.htm)|Darkvision|Vision dans le noir|officielle|
|[Bi8RZTKlzNcrlZXd.htm](extinction-curse-bestiary-items/Bi8RZTKlzNcrlZXd.htm)|Maul|Maillet|officielle|
|[bipIvdk7SAVNAKJV.htm](extinction-curse-bestiary-items/bipIvdk7SAVNAKJV.htm)|Quick Draw|Arme en main|officielle|
|[bj60CVwHmHYJF9pA.htm](extinction-curse-bestiary-items/bj60CVwHmHYJF9pA.htm)|Fangs|Crocs|officielle|
|[BJhx0ORRonO7nFzG.htm](extinction-curse-bestiary-items/BJhx0ORRonO7nFzG.htm)|Darkvision|Vision dans le noir|officielle|
|[Bjuv7ra9W5xu1wJi.htm](extinction-curse-bestiary-items/Bjuv7ra9W5xu1wJi.htm)|Shrewd Eye|Regard perçant|officielle|
|[bKUoSbiAMRYkv1ol.htm](extinction-curse-bestiary-items/bKUoSbiAMRYkv1ol.htm)|Composite Longbow|Arc long composite|officielle|
|[BKVl8zZlq09HHet1.htm](extinction-curse-bestiary-items/BKVl8zZlq09HHet1.htm)|Darkvision|Vision dans le noir|officielle|
|[bKziwa1hNAYNhHtz.htm](extinction-curse-bestiary-items/bKziwa1hNAYNhHtz.htm)|Charged Earth|Terre électrifiée|officielle|
|[BLfd48IDsZFHeeCm.htm](extinction-curse-bestiary-items/BLfd48IDsZFHeeCm.htm)|At-Will Spells|Sorts à volonté|officielle|
|[bn0Z3k2gK4VsKjQ4.htm](extinction-curse-bestiary-items/bn0Z3k2gK4VsKjQ4.htm)|Hatchet|Hachette|officielle|
|[bNJHQZLJcksGVWsc.htm](extinction-curse-bestiary-items/bNJHQZLJcksGVWsc.htm)|Boneshaking Roar|Rugissement foudroyant|officielle|
|[BoGD0m0tUXjkgQax.htm](extinction-curse-bestiary-items/BoGD0m0tUXjkgQax.htm)|Detect Alignment (Constant)(Evil Only)|Détection de l'alignement (constant, mauvais uniquement)|officielle|
|[bOKG9bGFFIf6vSxZ.htm](extinction-curse-bestiary-items/bOKG9bGFFIf6vSxZ.htm)|Cleaver|Fendoir|officielle|
|[boMBR55PMuMTWR4T.htm](extinction-curse-bestiary-items/boMBR55PMuMTWR4T.htm)|Touch of Zevgavizeb|Toucher de Zevgavizeb|officielle|
|[BPDL9YkRvinFnlEi.htm](extinction-curse-bestiary-items/BPDL9YkRvinFnlEi.htm)|Selective Scent|Odorat sélectif|officielle|
|[BPZQ1dC6ouU0B3Ud.htm](extinction-curse-bestiary-items/BPZQ1dC6ouU0B3Ud.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|libre|
|[bQB1uLPL0KXt6C4L.htm](extinction-curse-bestiary-items/bQB1uLPL0KXt6C4L.htm)|Longsword|Épée longue|officielle|
|[Bqkj3XCvzqqQZur2.htm](extinction-curse-bestiary-items/Bqkj3XCvzqqQZur2.htm)|Darkvision|Vision dans le noir|officielle|
|[Br7RNde4i4eoRkTL.htm](extinction-curse-bestiary-items/Br7RNde4i4eoRkTL.htm)|Spear|Lance|officielle|
|[Brr6EgY2XOH5SCbH.htm](extinction-curse-bestiary-items/Brr6EgY2XOH5SCbH.htm)|Cat Sith's Mark|Signe du chat sith|officielle|
|[BsPkvlLcpyGM76pg.htm](extinction-curse-bestiary-items/BsPkvlLcpyGM76pg.htm)|Low-Light Vision|Vision nocturne|officielle|
|[BSqgVF2Zcf7Vv8im.htm](extinction-curse-bestiary-items/BSqgVF2Zcf7Vv8im.htm)|Low-Light Vision|Vision nocturne|officielle|
|[btfD7bWkD0rzi4JA.htm](extinction-curse-bestiary-items/btfD7bWkD0rzi4JA.htm)|Shortsword|+1,striking|Épée courte de frappe +1|officielle|
|[Btfq47RDcKImj5q3.htm](extinction-curse-bestiary-items/Btfq47RDcKImj5q3.htm)|Earth Glide|Glissement de terrain|libre|
|[BTkdHTwWzCskxz7S.htm](extinction-curse-bestiary-items/BTkdHTwWzCskxz7S.htm)|Spectral Gore|Coup de corne spectral|officielle|
|[buuATh1VmZvUaYAM.htm](extinction-curse-bestiary-items/buuATh1VmZvUaYAM.htm)|Trunk|Trompe|officielle|
|[Bwk1au1bwoRPov0x.htm](extinction-curse-bestiary-items/Bwk1au1bwoRPov0x.htm)|Jaws|Mâchoires|officielle|
|[bXlG2k7ws1b8Wffl.htm](extinction-curse-bestiary-items/bXlG2k7ws1b8Wffl.htm)|Darkvision|Vision dans le noir|libre|
|[c1nV69h5eKoUvmDM.htm](extinction-curse-bestiary-items/c1nV69h5eKoUvmDM.htm)|Mounted Superiority|Supériorité du cavalier|officielle|
|[C1rEWsXR5VEckDEo.htm](extinction-curse-bestiary-items/C1rEWsXR5VEckDEo.htm)|Criminal Lore|Connaissance des crimes|officielle|
|[c2lrhpLmasLXBnIC.htm](extinction-curse-bestiary-items/c2lrhpLmasLXBnIC.htm)|Constant Spells|Sorts constants|officielle|
|[c3A2YAnkRX3JPc0B.htm](extinction-curse-bestiary-items/c3A2YAnkRX3JPc0B.htm)|Darkvision|Vision dans le noir|officielle|
|[C4dcn6EdPVp0Ec5y.htm](extinction-curse-bestiary-items/C4dcn6EdPVp0Ec5y.htm)|Vulnerability to Extinguishing|Vulnérabilité à l'extinction|officielle|
|[C5CbW2c2gPmnN8il.htm](extinction-curse-bestiary-items/C5CbW2c2gPmnN8il.htm)|Low-Light Vision|Vision nocturne|officielle|
|[C5gcMBexKui4KaYg.htm](extinction-curse-bestiary-items/C5gcMBexKui4KaYg.htm)|Paralytic Saliva|Salive paralysante|officielle|
|[C5gIfV30l6HcWkwS.htm](extinction-curse-bestiary-items/C5gIfV30l6HcWkwS.htm)|Aroden Lore|Connaissance d'Aroden|officielle|
|[c716Uv2dXVDhuZaX.htm](extinction-curse-bestiary-items/c716Uv2dXVDhuZaX.htm)|Treacherous Veil|Voile trompeur|officielle|
|[C8fFcklKWgsWS7pR.htm](extinction-curse-bestiary-items/C8fFcklKWgsWS7pR.htm)|Forced Transfusion|Transfusion forcée|officielle|
|[c9o8wtXKr3BgyUJa.htm](extinction-curse-bestiary-items/c9o8wtXKr3BgyUJa.htm)|Spear|+2,striking|Lance de frappe +2|officielle|
|[c9zfJMOOSFj5Gqcb.htm](extinction-curse-bestiary-items/c9zfJMOOSFj5Gqcb.htm)|Mounted Superiority|Supériorité du cavalier|officielle|
|[cA7JHHD6mDUi7NQE.htm](extinction-curse-bestiary-items/cA7JHHD6mDUi7NQE.htm)|Infused Reagents|Réactifs imprégnés|officielle|
|[CARAuWZoSe8i4SSF.htm](extinction-curse-bestiary-items/CARAuWZoSe8i4SSF.htm)|Jaws|Mâchoires|officielle|
|[Cc90LRPBjtIS0WLk.htm](extinction-curse-bestiary-items/Cc90LRPBjtIS0WLk.htm)|Invisibility (At Will) (Self Only)|Invisibilité (À volonté) (soi uniquement)|officielle|
|[cca2HxwcLz1pklgq.htm](extinction-curse-bestiary-items/cca2HxwcLz1pklgq.htm)|Greater Darkvision|Vision dans le noir supérieure|officielle|
|[cceloNgYjNLlTIG8.htm](extinction-curse-bestiary-items/cceloNgYjNLlTIG8.htm)|Darkvision|Vision dans le noir|officielle|
|[CCoxtcbk850mBaMi.htm](extinction-curse-bestiary-items/CCoxtcbk850mBaMi.htm)|Jaws|Mâchoires|officielle|
|[cDjgzvQmTMWhvvaO.htm](extinction-curse-bestiary-items/cDjgzvQmTMWhvvaO.htm)|Lifesense (Imprecise) 60 feet|Perception de la vie 18 m (imprécis)|officielle|
|[cdxLqOHRZzka3F9n.htm](extinction-curse-bestiary-items/cdxLqOHRZzka3F9n.htm)|Tentacular Burst|Irruption de tentacules|officielle|
|[ce0ME84C2cbPH0JA.htm](extinction-curse-bestiary-items/ce0ME84C2cbPH0JA.htm)|Paralysis|Paralysie|officielle|
|[Ce2SsOT9AnFtxFb9.htm](extinction-curse-bestiary-items/Ce2SsOT9AnFtxFb9.htm)|Ranseur|Corsèque|officielle|
|[CeucQqSmssZqnYDf.htm](extinction-curse-bestiary-items/CeucQqSmssZqnYDf.htm)|Flippable|Renversable|officielle|
|[CfGyCIUgQSgrIVyE.htm](extinction-curse-bestiary-items/CfGyCIUgQSgrIVyE.htm)|Powerful Stench|Puissante puanteur|officielle|
|[Cgn2V1u3dIaaSjCc.htm](extinction-curse-bestiary-items/Cgn2V1u3dIaaSjCc.htm)|Successful Fortitude saves are critical successes instead|Les jets de Vigueur réussis deviennent des succès critiques|officielle|
|[CHFfq14eNMMH7UGa.htm](extinction-curse-bestiary-items/CHFfq14eNMMH7UGa.htm)|Demonic Strength|Force démoniaque|officielle|
|[CHkvWxLYAe1SOzaW.htm](extinction-curse-bestiary-items/CHkvWxLYAe1SOzaW.htm)|Greatpick|Grand pic de guerre|officielle|
|[cI3F9pBXykpzpGpP.htm](extinction-curse-bestiary-items/cI3F9pBXykpzpGpP.htm)|Tail|Queue|officielle|
|[Ci7biTFOwXd5SyQV.htm](extinction-curse-bestiary-items/Ci7biTFOwXd5SyQV.htm)|+23 when using Perception for Initiative|+23 en utilisant Perception pour l'Initiative|officielle|
|[cI80h0WLWTix6fdX.htm](extinction-curse-bestiary-items/cI80h0WLWTix6fdX.htm)|Dagger|Dague|officielle|
|[cJAwzP7ugkINDPvs.htm](extinction-curse-bestiary-items/cJAwzP7ugkINDPvs.htm)|Trample|Piétinement|officielle|
|[cKPrl4Em8PlcrlED.htm](extinction-curse-bestiary-items/cKPrl4Em8PlcrlED.htm)|Bowling Pin|Quille|officielle|
|[cKUzL3t8iW7J5EWm.htm](extinction-curse-bestiary-items/cKUzL3t8iW7J5EWm.htm)|At-Will Spells|Sorts à volonté|officielle|
|[CLbLXBJNSCtMb0bH.htm](extinction-curse-bestiary-items/CLbLXBJNSCtMb0bH.htm)|Jaws|Mâchoires|officielle|
|[ClFUTI0Jfb1az69J.htm](extinction-curse-bestiary-items/ClFUTI0Jfb1az69J.htm)|Invisibility (At Will)|Invisibilité (À volonté)|officielle|
|[cM3DMiodWWRFm8ve.htm](extinction-curse-bestiary-items/cM3DMiodWWRFm8ve.htm)|+2 Greater Resilient Chain Mail|Cotte de mailles de résilience supérieure +2|officielle|
|[CmEN8uWFDeuYp1rP.htm](extinction-curse-bestiary-items/CmEN8uWFDeuYp1rP.htm)|Improvised Surprise|Surprise improvisée|officielle|
|[cmJ4It6KgqjEgmOi.htm](extinction-curse-bestiary-items/cmJ4It6KgqjEgmOi.htm)|Powerful Stench|Puissante puanteur|officielle|
|[CNoZswC3ObHsm2Jd.htm](extinction-curse-bestiary-items/CNoZswC3ObHsm2Jd.htm)|Knock (At-Will)|Déblocage (À volonté)|officielle|
|[coep2DirH6k49uz3.htm](extinction-curse-bestiary-items/coep2DirH6k49uz3.htm)|Flea Fever|Fièvre des puces|officielle|
|[COxiLfpFDrDT8lEG.htm](extinction-curse-bestiary-items/COxiLfpFDrDT8lEG.htm)|Bowling Pin|Quille|officielle|
|[Cp4os9TY3OTwnTf2.htm](extinction-curse-bestiary-items/Cp4os9TY3OTwnTf2.htm)|Composite Shortbow|+2,greaterStriking|Arc court composite de Frappe supérieure +2|libre|
|[cpcIX2KKJCMLAkwJ.htm](extinction-curse-bestiary-items/cpcIX2KKJCMLAkwJ.htm)|Quick Draw|Arme en main|officielle|
|[cQcOaBFYo9hz8JUB.htm](extinction-curse-bestiary-items/cQcOaBFYo9hz8JUB.htm)|Fast Healing 30|Guérison accélérée 30|officielle|
|[cr8qKlbGGGNeDAHl.htm](extinction-curse-bestiary-items/cr8qKlbGGGNeDAHl.htm)|Darkvision|Vision dans le noir|officielle|
|[cs9D6zSPlntBJcWq.htm](extinction-curse-bestiary-items/cs9D6zSPlntBJcWq.htm)|Detect Magic (Constant)|Détection de la magie (constant)|officielle|
|[Csc3FpAiT4LS7zI7.htm](extinction-curse-bestiary-items/Csc3FpAiT4LS7zI7.htm)|Jaws|Mâchoires|officielle|
|[cSPBpYXKmUBKXwQ3.htm](extinction-curse-bestiary-items/cSPBpYXKmUBKXwQ3.htm)|Horns|Cornes|officielle|
|[Csqes18z0CZnmC2a.htm](extinction-curse-bestiary-items/Csqes18z0CZnmC2a.htm)|Begin Convergence|Commencer la convergence|officielle|
|[cSvshId5YCLIYZCG.htm](extinction-curse-bestiary-items/cSvshId5YCLIYZCG.htm)|Eye Stalk|Tentacule oculaire|officielle|
|[CucvL4Psm96kCofz.htm](extinction-curse-bestiary-items/CucvL4Psm96kCofz.htm)|Fear (At Will)|Terreur (À volonté)|officielle|
|[CVT6zaghpuo7F5Qs.htm](extinction-curse-bestiary-items/CVT6zaghpuo7F5Qs.htm)|Cleaver|Fendoir|officielle|
|[cWgbf0nThpGkUwma.htm](extinction-curse-bestiary-items/cWgbf0nThpGkUwma.htm)|Tongues (Constant)|Don des langues (constant)|officielle|
|[CwMPxG8qELCyPvc1.htm](extinction-curse-bestiary-items/CwMPxG8qELCyPvc1.htm)|Darkvision|Vision dans le noir|libre|
|[CwrLtsP8ondPDdfF.htm](extinction-curse-bestiary-items/CwrLtsP8ondPDdfF.htm)|Telekinetic Haul (At Will)|Transport télékinésique (À volonté)|officielle|
|[cXwerHf1acuccvb8.htm](extinction-curse-bestiary-items/cXwerHf1acuccvb8.htm)|Arcane Spontaneous Spells|Sorts arcaniques spontanés|libre|
|[cYcpQzRaSNxmtRwk.htm](extinction-curse-bestiary-items/cYcpQzRaSNxmtRwk.htm)|Low-Light Vision|Vision nocturne|officielle|
|[CyWELRcAGeuO8Xdv.htm](extinction-curse-bestiary-items/CyWELRcAGeuO8Xdv.htm)|Claw|Griffe|officielle|
|[cZEvbEpDTZmmwmdY.htm](extinction-curse-bestiary-items/cZEvbEpDTZmmwmdY.htm)|Low-Light Vision|Vision nocturne|officielle|
|[D1U3wK3uyoOs5sL1.htm](extinction-curse-bestiary-items/D1U3wK3uyoOs5sL1.htm)|Tainted Rage|Rage corrompue|officielle|
|[D2bX7v0RmrFFnVye.htm](extinction-curse-bestiary-items/D2bX7v0RmrFFnVye.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[D6Xun7rk5Brir0IF.htm](extinction-curse-bestiary-items/D6Xun7rk5Brir0IF.htm)|Speak with Animals (Constant)|Communication avec les animaux (constant)|officielle|
|[D8wFcvl1kfrS8ylG.htm](extinction-curse-bestiary-items/D8wFcvl1kfrS8ylG.htm)|Reckless Abandon|Dangereux abandon|libre|
|[daRZgdF46eHobOw0.htm](extinction-curse-bestiary-items/daRZgdF46eHobOw0.htm)|Stench|Puanteur|officielle|
|[DBng7QU65qzXSOrS.htm](extinction-curse-bestiary-items/DBng7QU65qzXSOrS.htm)|Resin Spray|Jet de résine|officielle|
|[dCh3xhDcX6ef0gtU.htm](extinction-curse-bestiary-items/dCh3xhDcX6ef0gtU.htm)|Greasy Seepage|Suintement graisseux|officielle|
|[dcsCqElp022elQ9N.htm](extinction-curse-bestiary-items/dcsCqElp022elQ9N.htm)|Jaws|Mâchoires|officielle|
|[dFflNs9ymwMB3ky8.htm](extinction-curse-bestiary-items/dFflNs9ymwMB3ky8.htm)|Stench|Puanteur|officielle|
|[dFgEVCoqXZx2LxVQ.htm](extinction-curse-bestiary-items/dFgEVCoqXZx2LxVQ.htm)|No Escape|Pas d'échappatoire|officielle|
|[DfLYFCPstsIKpqse.htm](extinction-curse-bestiary-items/DfLYFCPstsIKpqse.htm)|Knockdown|Renversement|officielle|
|[dFwMJyxs9OvKd6Hk.htm](extinction-curse-bestiary-items/dFwMJyxs9OvKd6Hk.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[dfYmwPtqFedXkfE7.htm](extinction-curse-bestiary-items/dfYmwPtqFedXkfE7.htm)|True Seeing (Constant)|Vision lucide (constant)|officielle|
|[DgPlSSqql6BVhbUO.htm](extinction-curse-bestiary-items/DgPlSSqql6BVhbUO.htm)|Quick Draw|Arme en main|officielle|
|[dIqtE84EVpEREjSZ.htm](extinction-curse-bestiary-items/dIqtE84EVpEREjSZ.htm)|Quick Bomber|Artificier rapide|officielle|
|[diUbIYXAIUT4w27K.htm](extinction-curse-bestiary-items/diUbIYXAIUT4w27K.htm)|Pain Touch|Contact désespérant|officielle|
|[DJwAey2Keob2rJ2y.htm](extinction-curse-bestiary-items/DJwAey2Keob2rJ2y.htm)|Dangerous Sorcery|Sorcellerie dangereuse|officielle|
|[DLLKE5ky8d8zWKa4.htm](extinction-curse-bestiary-items/DLLKE5ky8d8zWKa4.htm)|Vulnerable to Shatter|Vulnérabilité à Fracassement|officielle|
|[Dm321ezqoyu7Pz2L.htm](extinction-curse-bestiary-items/Dm321ezqoyu7Pz2L.htm)|Truth Vulnerability|Vulnérabilité à la vérité|officielle|
|[dM9C8lwWHWEBUxDR.htm](extinction-curse-bestiary-items/dM9C8lwWHWEBUxDR.htm)|Wild Empathy|Empathie sauvage|officielle|
|[DmFD5HCxaT3wtIMk.htm](extinction-curse-bestiary-items/DmFD5HCxaT3wtIMk.htm)|Devour Soul|Engloutissement d'âmes|officielle|
|[dnBXS9fUYPg74ls2.htm](extinction-curse-bestiary-items/dnBXS9fUYPg74ls2.htm)|True Seeing (Constant)|Vision lucide (constant)|officielle|
|[DNVtXPFDKGXBnkvT.htm](extinction-curse-bestiary-items/DNVtXPFDKGXBnkvT.htm)|Claw|Griffe|officielle|
|[doOqdxPdlJYa0GsX.htm](extinction-curse-bestiary-items/doOqdxPdlJYa0GsX.htm)|Mobile Shot Stance|Posture de tir mobile|officielle|
|[DPKdvyRQD79RjlTV.htm](extinction-curse-bestiary-items/DPKdvyRQD79RjlTV.htm)|Summon Fiend (Daemons Only)|Convocation de fiélon (daémons uniquement)|officielle|
|[dPn7Q8LR4nsQ4Aar.htm](extinction-curse-bestiary-items/dPn7Q8LR4nsQ4Aar.htm)|Jaws|Mâchoires|officielle|
|[dq6M4Jej5LRa8pai.htm](extinction-curse-bestiary-items/dq6M4Jej5LRa8pai.htm)|Restoration Vulnerability|Vulnérabilité à la restauration|officielle|
|[DQLALym9BRxjL6Rc.htm](extinction-curse-bestiary-items/DQLALym9BRxjL6Rc.htm)|Fist|Poing|officielle|
|[DqSmbwh963Ftosau.htm](extinction-curse-bestiary-items/DqSmbwh963Ftosau.htm)|Improvised Weapon|Arme improvisée|officielle|
|[dQxzoa0Vlfr0DKua.htm](extinction-curse-bestiary-items/dQxzoa0Vlfr0DKua.htm)|Halberd|Hallebarde|officielle|
|[DqZJwi3AmpInsFhs.htm](extinction-curse-bestiary-items/DqZJwi3AmpInsFhs.htm)|Darkvision|Vision dans le noir|officielle|
|[DrJdeATQTR9zK1Rn.htm](extinction-curse-bestiary-items/DrJdeATQTR9zK1Rn.htm)|Pummel the Fallen|Tabasser les créatures à terre|officielle|
|[dSGJjwStpSRNtqOu.htm](extinction-curse-bestiary-items/dSGJjwStpSRNtqOu.htm)|Gambling Lore|Connaissance des jeux d'argent|officielle|
|[DsnYPhbnCJOnQiIV.htm](extinction-curse-bestiary-items/DsnYPhbnCJOnQiIV.htm)|Negative Healing|Guérison négative|officielle|
|[DtEZmNobgsVWzikz.htm](extinction-curse-bestiary-items/DtEZmNobgsVWzikz.htm)|Dooming Bark|Aboiement de condamnation|officielle|
|[DtYm8jdItEs6dlmW.htm](extinction-curse-bestiary-items/DtYm8jdItEs6dlmW.htm)|At-Will Spells|Sorts à volonté|officielle|
|[DVb8rll2Xx3wj0Zx.htm](extinction-curse-bestiary-items/DVb8rll2Xx3wj0Zx.htm)|Claw|Griffe|officielle|
|[DveD5UtW57U98ZZr.htm](extinction-curse-bestiary-items/DveD5UtW57U98ZZr.htm)|Weapon Master|Maître d'armes|officielle|
|[dvKGUrS6UQXX4qts.htm](extinction-curse-bestiary-items/dvKGUrS6UQXX4qts.htm)|Tentacle|Tentacule|officielle|
|[DvUIErNvZwDWm1xw.htm](extinction-curse-bestiary-items/DvUIErNvZwDWm1xw.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[Dw6gmSPBaL248eKB.htm](extinction-curse-bestiary-items/Dw6gmSPBaL248eKB.htm)|Shraen Lore|Connaissance de Shraen|officielle|
|[dweCpMS4m3DyGNS6.htm](extinction-curse-bestiary-items/dweCpMS4m3DyGNS6.htm)|Tanglefoot Bag (Greater) (Infused)|Sacoche d'immobilisation supérieure (imprégnée)|officielle|
|[dWpvSluY2OjLNlab.htm](extinction-curse-bestiary-items/dWpvSluY2OjLNlab.htm)|Detect Magic (Constant)|Détection de la magie (constant)|officielle|
|[DwsRNc9qk55FLo8f.htm](extinction-curse-bestiary-items/DwsRNc9qk55FLo8f.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[dWxzw3v6njHLRsgV.htm](extinction-curse-bestiary-items/dWxzw3v6njHLRsgV.htm)|Nimble Dodge|Esquive agile|officielle|
|[dxePtwfdF42mXjHb.htm](extinction-curse-bestiary-items/dxePtwfdF42mXjHb.htm)|Religious Symbol (Silver) of Aroden|Symbole religieux en argent d'Aroden|officielle|
|[DXu0yLebZZ8ZPgay.htm](extinction-curse-bestiary-items/DXu0yLebZZ8ZPgay.htm)|Convergent Link|Lien convergent|officielle|
|[DzcmUlbG0ynUzWSX.htm](extinction-curse-bestiary-items/DzcmUlbG0ynUzWSX.htm)|Alchemist's Fire|Feu grégeois|officielle|
|[e09QNAXtgAev98xw.htm](extinction-curse-bestiary-items/e09QNAXtgAev98xw.htm)|Shortsword|Épée courte|officielle|
|[e1DGkcB2QL0mRcLw.htm](extinction-curse-bestiary-items/e1DGkcB2QL0mRcLw.htm)|Surprise Attack|Attaque surprise|officielle|
|[E2nQYNcliQTmFXuG.htm](extinction-curse-bestiary-items/E2nQYNcliQTmFXuG.htm)|Darkness (At Will)|Ténèbres (À volonté)|officielle|
|[E2UQQSW7N3ZLX51N.htm](extinction-curse-bestiary-items/E2UQQSW7N3ZLX51N.htm)|Sneak Attack|Attaque sournoise|officielle|
|[E4kWYfMtWsLrq662.htm](extinction-curse-bestiary-items/E4kWYfMtWsLrq662.htm)|Claw|Griffe|officielle|
|[e4w04mrVyi9Okgu8.htm](extinction-curse-bestiary-items/e4w04mrVyi9Okgu8.htm)|Twisted Desires|Désirs pervertis|officielle|
|[e6hdWb90a4uxuhor.htm](extinction-curse-bestiary-items/e6hdWb90a4uxuhor.htm)|Bomber|Artificier|officielle|
|[E7ey0z1cm7nKTIH7.htm](extinction-curse-bestiary-items/E7ey0z1cm7nKTIH7.htm)|Vampire Drider Venom|Venin de drider vampire|officielle|
|[EAbi0YGQcDdrK1rK.htm](extinction-curse-bestiary-items/EAbi0YGQcDdrK1rK.htm)|+2 Resilient Leather Armor|Armure en cuir de résilience +2|officielle|
|[ead7pwbvr3oideQg.htm](extinction-curse-bestiary-items/ead7pwbvr3oideQg.htm)|Pollen Burst|Pulvérisation de pollen|officielle|
|[eb5QTbp37dxuOa2K.htm](extinction-curse-bestiary-items/eb5QTbp37dxuOa2K.htm)|Constant Spells|Sorts constants|officielle|
|[EbJV7OigGb2BRRbY.htm](extinction-curse-bestiary-items/EbJV7OigGb2BRRbY.htm)|Rabies|Rage|officielle|
|[EbKHyd2DVj0sOh0r.htm](extinction-curse-bestiary-items/EbKHyd2DVj0sOh0r.htm)|Dagger|Dague|officielle|
|[EC6gyIBni8OO9swx.htm](extinction-curse-bestiary-items/EC6gyIBni8OO9swx.htm)|Ethereal Jaunt (At Will) (From Heartstone)|Forme éthérée (à volonté, cardioline)|officielle|
|[ee9wYp6ZD1nOsbJs.htm](extinction-curse-bestiary-items/ee9wYp6ZD1nOsbJs.htm)|Quick Alchemy|Alchimie rapide|officielle|
|[EEbVQQPyiltRr8dm.htm](extinction-curse-bestiary-items/EEbVQQPyiltRr8dm.htm)|Counterspell|Contresort|officielle|
|[eF1NPrH42zK6nWtF.htm](extinction-curse-bestiary-items/eF1NPrH42zK6nWtF.htm)|Bloodline Magic|Magie de lignage|officielle|
|[ef5iLBGiYGg2sKRf.htm](extinction-curse-bestiary-items/ef5iLBGiYGg2sKRf.htm)|Chief Constable's Badge|Insigne de capitaine des gardes|officielle|
|[EHoVL9UXHlhPng6e.htm](extinction-curse-bestiary-items/EHoVL9UXHlhPng6e.htm)|Fist|Poing|officielle|
|[eJ1kLbGFLHyRoqnW.htm](extinction-curse-bestiary-items/eJ1kLbGFLHyRoqnW.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|officielle|
|[EK9FzoYNZUXr3Mk4.htm](extinction-curse-bestiary-items/EK9FzoYNZUXr3Mk4.htm)|Dimension Door (At Will)|Porte dimensionnelle (À volonté)|officielle|
|[Ekh1nBGghfmd4C6b.htm](extinction-curse-bestiary-items/Ekh1nBGghfmd4C6b.htm)|Blunt Snout|Truffe épatée|officielle|
|[eKTLgw1q7VJ64kFf.htm](extinction-curse-bestiary-items/eKTLgw1q7VJ64kFf.htm)|Claw|Griffe|officielle|
|[ElO9xLxjB4wblX7d.htm](extinction-curse-bestiary-items/ElO9xLxjB4wblX7d.htm)|Flail|Fléau d'armes|officielle|
|[En5XyqbwPFuM1ya6.htm](extinction-curse-bestiary-items/En5XyqbwPFuM1ya6.htm)|Ranseur|+1,striking|Corsèque de frappe +1|libre|
|[eNCqlXhdNVfLn6zx.htm](extinction-curse-bestiary-items/eNCqlXhdNVfLn6zx.htm)|Powerful Stench|Puissante puanteur|officielle|
|[EOAJdvYpfMEyRoEY.htm](extinction-curse-bestiary-items/EOAJdvYpfMEyRoEY.htm)|Claw|Griffe|officielle|
|[EoEZjFDg31YP4hUr.htm](extinction-curse-bestiary-items/EoEZjFDg31YP4hUr.htm)|Hatchet|Hachette|officielle|
|[EoheVjsQC39ZT9Ya.htm](extinction-curse-bestiary-items/EoheVjsQC39ZT9Ya.htm)|Quick Draw|Arme en main|officielle|
|[EOlqIML78FxmOKnN.htm](extinction-curse-bestiary-items/EOlqIML78FxmOKnN.htm)|Grab|Empoignade|officielle|
|[Eoo8nFMnqyYVA8S9.htm](extinction-curse-bestiary-items/Eoo8nFMnqyYVA8S9.htm)|Unsprung Wires|Câbles déclencheurs|officielle|
|[Epd15gSJ08XAuMS2.htm](extinction-curse-bestiary-items/Epd15gSJ08XAuMS2.htm)|Low-Light Vision|Vision nocturne|officielle|
|[EPOz45AVHD1zOj5z.htm](extinction-curse-bestiary-items/EPOz45AVHD1zOj5z.htm)|Golem Antimagic|Antimagie du golem|officielle|
|[epTn8izUucNDtBXJ.htm](extinction-curse-bestiary-items/epTn8izUucNDtBXJ.htm)|Jaws|Mâchoires|officielle|
|[eQesIwS05im7it7H.htm](extinction-curse-bestiary-items/eQesIwS05im7it7H.htm)|Rhoka Sword|+2,greaterStriking|Épée rhoka de frappe supérieure +2|libre|
|[EQPvoy0awap74MNj.htm](extinction-curse-bestiary-items/EQPvoy0awap74MNj.htm)|Shortsword|Épée courte|officielle|
|[eQwOg2taLdr6yqIb.htm](extinction-curse-bestiary-items/eQwOg2taLdr6yqIb.htm)|Thoughtsense (Imprecise) 60 feet|Perception des pensées 18 m (imprécis)|officielle|
|[erJjvQ0PRlmb1VLz.htm](extinction-curse-bestiary-items/erJjvQ0PRlmb1VLz.htm)|Existential Agony|Tourment existentiel|officielle|
|[EroB5CcwaWtvURqP.htm](extinction-curse-bestiary-items/EroB5CcwaWtvURqP.htm)|Fear (At Will)|Terreur (À volonté)|officielle|
|[ESJs8ZFuYfcIo7WE.htm](extinction-curse-bestiary-items/ESJs8ZFuYfcIo7WE.htm)|Muse Possession|Possession de la muse|officielle|
|[eTns0wXwB8nrvDuF.htm](extinction-curse-bestiary-items/eTns0wXwB8nrvDuF.htm)|Push|Bousculade|officielle|
|[etXQCdL5Q2AdyjxP.htm](extinction-curse-bestiary-items/etXQCdL5Q2AdyjxP.htm)|Force Communication|Communication de force|officielle|
|[EVi2gsT6wLToy6mR.htm](extinction-curse-bestiary-items/EVi2gsT6wLToy6mR.htm)|Light in the Darkness|Lumière dans l'obscurité|officielle|
|[eWqNIy4ejpF685wS.htm](extinction-curse-bestiary-items/eWqNIy4ejpF685wS.htm)|Lurching Charge|Charge soudaine|officielle|
|[eWuV67fIWoPIYMfp.htm](extinction-curse-bestiary-items/eWuV67fIWoPIYMfp.htm)|Grab|Empoignade|officielle|
|[eX9GmQ5kPOaJk2yM.htm](extinction-curse-bestiary-items/eX9GmQ5kPOaJk2yM.htm)|Darkvision|Vision dans le noir|officielle|
|[exnEDT15hRasjXZk.htm](extinction-curse-bestiary-items/exnEDT15hRasjXZk.htm)|Powerful Stench|Puissante puanteur|officielle|
|[eYBDQwviDeBD3lXK.htm](extinction-curse-bestiary-items/eYBDQwviDeBD3lXK.htm)|Darkvision|Vision dans le noir|libre|
|[EyUD2ROErzsB3uXQ.htm](extinction-curse-bestiary-items/EyUD2ROErzsB3uXQ.htm)|Jaws|Mâchoires|officielle|
|[eZpEJwLSEEfDXS1R.htm](extinction-curse-bestiary-items/eZpEJwLSEEfDXS1R.htm)|Bastard Sword|+1,striking|Épée bâtarde de frappe +1|libre|
|[ezZxuEc2ENbqDDnV.htm](extinction-curse-bestiary-items/ezZxuEc2ENbqDDnV.htm)|Hand Crossbow|+1,striking|Arbalète de poing de frappe +1|libre|
|[F4djhkzZDXpAotWi.htm](extinction-curse-bestiary-items/F4djhkzZDXpAotWi.htm)|Rend|Éventration|officielle|
|[f6pIZeCMzq997PVq.htm](extinction-curse-bestiary-items/f6pIZeCMzq997PVq.htm)|Maul|+2,greaterStriking|Maillet +2 de Frappe supérieure|libre|
|[f6rRwvnYIpGpqCcN.htm](extinction-curse-bestiary-items/f6rRwvnYIpGpqCcN.htm)|Negative Healing|Guérison négative|officielle|
|[F9xnDBHxVUqqbUBb.htm](extinction-curse-bestiary-items/F9xnDBHxVUqqbUBb.htm)|Low-Light Vision|Vision nocturne|officielle|
|[FADVo5mG2frt3d8x.htm](extinction-curse-bestiary-items/FADVo5mG2frt3d8x.htm)|Soulfire Inhalation|Inhalation de l'âme incendiaire|officielle|
|[faLRVKzt3bOyIaTB.htm](extinction-curse-bestiary-items/faLRVKzt3bOyIaTB.htm)|At-Will Spells|Sorts à volonté|officielle|
|[FaneRZfLNKwW9Fj3.htm](extinction-curse-bestiary-items/FaneRZfLNKwW9Fj3.htm)|Alchemist's Fire (Greater) [Infused]|Feu grégeois supérieur (imprégné)|officielle|
|[FaPP84ihUiXrLLm8.htm](extinction-curse-bestiary-items/FaPP84ihUiXrLLm8.htm)|True Seeing (Constant)|Vision lucide (constant)|officielle|
|[FbbJSkeiW59zMcZe.htm](extinction-curse-bestiary-items/FbbJSkeiW59zMcZe.htm)|Grab|Empoignade|officielle|
|[FBUfRtHdX6pDQqmM.htm](extinction-curse-bestiary-items/FBUfRtHdX6pDQqmM.htm)|Attack of Opportunity (Special)|Attaque d'opportunité (Spéciale)|officielle|
|[FbV73v3RSYlWpc56.htm](extinction-curse-bestiary-items/FbV73v3RSYlWpc56.htm)|Darkvision|Vision dans le noir|officielle|
|[FD1j6iyu3TIbvvW9.htm](extinction-curse-bestiary-items/FD1j6iyu3TIbvvW9.htm)|Web|Toile|officielle|
|[fD1wLMV2uktFhB2U.htm](extinction-curse-bestiary-items/fD1wLMV2uktFhB2U.htm)|Devour Soul|Engloutissement d'âmes|officielle|
|[Fdjdb2OGv08d4sL7.htm](extinction-curse-bestiary-items/Fdjdb2OGv08d4sL7.htm)|Jaws|Mâchoires|officielle|
|[fekB8XfrAypEycMA.htm](extinction-curse-bestiary-items/fekB8XfrAypEycMA.htm)|Whip Of Compliance|Fouet de soumission|officielle|
|[FEN9wcbD8tt6FXsm.htm](extinction-curse-bestiary-items/FEN9wcbD8tt6FXsm.htm)|Gaseous Form (Self Only)|Forme gazeuse (soi uniquement)|officielle|
|[FFhpizMPIxIyhvzO.htm](extinction-curse-bestiary-items/FFhpizMPIxIyhvzO.htm)|Harrow Deck (Metal)|Jeu du Tourment en métal|officielle|
|[fgb3vhQw8zBEaCrc.htm](extinction-curse-bestiary-items/fgb3vhQw8zBEaCrc.htm)|Spear|Lance|officielle|
|[fhBXlEiQsR5ijwfv.htm](extinction-curse-bestiary-items/fhBXlEiQsR5ijwfv.htm)|Grab|Empoignade|officielle|
|[FHc5gHaZ8ngFzhdy.htm](extinction-curse-bestiary-items/FHc5gHaZ8ngFzhdy.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[fHtpNhpRCcTQHzVO.htm](extinction-curse-bestiary-items/fHtpNhpRCcTQHzVO.htm)|Greater Darkvision|Vision dans le noir supérieure|officielle|
|[fIVKEuRBev6cm8EW.htm](extinction-curse-bestiary-items/fIVKEuRBev6cm8EW.htm)|Status (At Will)|Rapport (À volonté)|officielle|
|[FJ5ZIeD2msGVDFXU.htm](extinction-curse-bestiary-items/FJ5ZIeD2msGVDFXU.htm)|Shortsword|+2,greaterStriking|Épée courte de frappe supérieure +2|officielle|
|[FJAtShazGIV67CZM.htm](extinction-curse-bestiary-items/FJAtShazGIV67CZM.htm)|Longbow|Arc long|officielle|
|[Fkch2WMm3nRHukfN.htm](extinction-curse-bestiary-items/Fkch2WMm3nRHukfN.htm)|Cleric Focus Spells|Sorts focalisés de prêtre|libre|
|[FKJSbQwQyoWxOJAj.htm](extinction-curse-bestiary-items/FKJSbQwQyoWxOJAj.htm)|Fling Shards|Lancer des éclats|officielle|
|[fLgTnGYjeLODWrZ5.htm](extinction-curse-bestiary-items/fLgTnGYjeLODWrZ5.htm)|Illusory Disguise (At Will)|Déguisement illusoire (À volonté)|officielle|
|[FMub7YCrl8phAqPa.htm](extinction-curse-bestiary-items/FMub7YCrl8phAqPa.htm)|Change Shape|Changement de forme|officielle|
|[fNT0RdVIPBpQZgpm.htm](extinction-curse-bestiary-items/fNT0RdVIPBpQZgpm.htm)|Whip of Compliance|+1,striking|Fouet de soumission|officielle|
|[FnXVIj505rfZjlbt.htm](extinction-curse-bestiary-items/FnXVIj505rfZjlbt.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|officielle|
|[FOFEvFiF3HyHeD3C.htm](extinction-curse-bestiary-items/FOFEvFiF3HyHeD3C.htm)|Hand Crossbow|Arbalète de poing|officielle|
|[fofjczD57mSL1xA2.htm](extinction-curse-bestiary-items/fofjczD57mSL1xA2.htm)|Motion Sense|Perception du mouvement|officielle|
|[fQBkHuE4KEzg86Bk.htm](extinction-curse-bestiary-items/fQBkHuE4KEzg86Bk.htm)|Wicked Bite|Méchante morsure|officielle|
|[FQIgX2HCNkiPsSUB.htm](extinction-curse-bestiary-items/FQIgX2HCNkiPsSUB.htm)|Death Drider Venom|Venin de drider de la mort|officielle|
|[FrMmR6TGEZYNt4Fc.htm](extinction-curse-bestiary-items/FrMmR6TGEZYNt4Fc.htm)|Greater Darkvision|Vision dans le noir supérieure|officielle|
|[FS7b7xFxtknejiPh.htm](extinction-curse-bestiary-items/FS7b7xFxtknejiPh.htm)|Psychic Howl|Hurlement psychique|officielle|
|[ftecIzpBFfnsQX7L.htm](extinction-curse-bestiary-items/ftecIzpBFfnsQX7L.htm)|Blind Ambition (At Will) (Emotional Focus)|Ambition aveugle (à volonté, Focalisation émotionnelle)|officielle|
|[ftRL4ZW49NsWC8tu.htm](extinction-curse-bestiary-items/ftRL4ZW49NsWC8tu.htm)|Spear|+2,greaterStriking|Lance de frappe supérieure +2|officielle|
|[Fu8tkGZao9c5IYgV.htm](extinction-curse-bestiary-items/Fu8tkGZao9c5IYgV.htm)|Telekinetic Maneuver (At Will)|Manoeuvre télékinésique (À volonté)|officielle|
|[Fu9Yec7x12lmehJE.htm](extinction-curse-bestiary-items/Fu9Yec7x12lmehJE.htm)|Snatch|Saisir|officielle|
|[fUeuCzXg5rcHjjeb.htm](extinction-curse-bestiary-items/fUeuCzXg5rcHjjeb.htm)|Rage|Rage|officielle|
|[fVDzActy99X50n5n.htm](extinction-curse-bestiary-items/fVDzActy99X50n5n.htm)|Surprise Attack|Attaque surprise|officielle|
|[FWCaUjBmQQkZbFfs.htm](extinction-curse-bestiary-items/FWCaUjBmQQkZbFfs.htm)|Grievous Strike|Frappe douloureuse|officielle|
|[fWI5g5k9p5EtqWoH.htm](extinction-curse-bestiary-items/fWI5g5k9p5EtqWoH.htm)|At-Will Spells|Sorts à volonté|libre|
|[FwQ6hfFQbFbTSEWf.htm](extinction-curse-bestiary-items/FwQ6hfFQbFbTSEWf.htm)|Juggernaut|Juggernaut|officielle|
|[fwSryrzGPdQfxaBv.htm](extinction-curse-bestiary-items/fwSryrzGPdQfxaBv.htm)|Animal Vision (At Will) (Target Must be a Rodent, Serpent, or Similar Animal Considered Troublesome by Humans)|Vision animale (À volonté) (la cible doit être un rongeur, un serpent ou un animal similaire considérée comme nuisible par les humains)|officielle|
|[Fza6vai8Wr7r8P6I.htm](extinction-curse-bestiary-items/Fza6vai8Wr7r8P6I.htm)|Buck|Désarçonner|officielle|
|[FzdAA0olz3ivmV27.htm](extinction-curse-bestiary-items/FzdAA0olz3ivmV27.htm)|Bravo's Brew (Moderate) (Infused)|Breuvage de bravoure modéré (imprégné)|officielle|
|[fzEhis5mum8aBN0a.htm](extinction-curse-bestiary-items/fzEhis5mum8aBN0a.htm)|Greater Darkvision|Vision dans le noir supérieure|officielle|
|[FZSw4o3rZCgCEFcW.htm](extinction-curse-bestiary-items/FZSw4o3rZCgCEFcW.htm)|Low-Light Vision|Vision nocturne|officielle|
|[g1Oa6Im9D6DICdqU.htm](extinction-curse-bestiary-items/g1Oa6Im9D6DICdqU.htm)|At-Will Spells|Sorts à volonté|officielle|
|[g2NeO9TpE9vzveoP.htm](extinction-curse-bestiary-items/g2NeO9TpE9vzveoP.htm)|Swallow Whole|Gober|officielle|
|[G2WFzkTuSXgoD6cE.htm](extinction-curse-bestiary-items/G2WFzkTuSXgoD6cE.htm)|Darkvision|Vision dans le noir|officielle|
|[g4ebpNlSIRHftDFy.htm](extinction-curse-bestiary-items/g4ebpNlSIRHftDFy.htm)|Spring-loaded Fist|Poing à ressort|officielle|
|[G4RxtZW5Dd8XP74g.htm](extinction-curse-bestiary-items/G4RxtZW5Dd8XP74g.htm)|Wand of Ray of Enfeeblement (Level 2)|Baguette de Rayon d'affaiblissement (niveau 2)|officielle|
|[g65rOk12pYhMLjoe.htm](extinction-curse-bestiary-items/g65rOk12pYhMLjoe.htm)|Bob and Weave|Feinter et esquiver|officielle|
|[g6JXzCEcGkpvkTOb.htm](extinction-curse-bestiary-items/g6JXzCEcGkpvkTOb.htm)|Stalker|Harceleur|officielle|
|[gb1r1Z5H5PiFdqyV.htm](extinction-curse-bestiary-items/gb1r1Z5H5PiFdqyV.htm)|Sudden Slices|Découpes imprévisibles|officielle|
|[GBZBg2dACMgs5Uz2.htm](extinction-curse-bestiary-items/GBZBg2dACMgs5Uz2.htm)|Disappearance (At Will) (Self Only)|Disparition (À volonté) (soi uniquement)|officielle|
|[gCcUGrKxC8wq622r.htm](extinction-curse-bestiary-items/gCcUGrKxC8wq622r.htm)|+3 Major Resilient Breastplate|Cuirasse de résilience majeure +3|officielle|
|[GCpCG2QeVwHXzzqM.htm](extinction-curse-bestiary-items/GCpCG2QeVwHXzzqM.htm)|Ghast Fever|Fièvre des blêmes|officielle|
|[GDrdlzlbCnTetn1d.htm](extinction-curse-bestiary-items/GDrdlzlbCnTetn1d.htm)|Confusion (At Will)|Confusion (À volonté)|officielle|
|[gen8uxMPnuBegIbr.htm](extinction-curse-bestiary-items/gen8uxMPnuBegIbr.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[gfMG9GR7q2Nmo3c2.htm](extinction-curse-bestiary-items/gfMG9GR7q2Nmo3c2.htm)|Dagger|+1,striking|Dague de frappe +1|libre|
|[gFpOboFky2k5eBIf.htm](extinction-curse-bestiary-items/gFpOboFky2k5eBIf.htm)|Sudden Betrayal|Trahison soudaine|officielle|
|[Gg4EGIEKIMWM6Lln.htm](extinction-curse-bestiary-items/Gg4EGIEKIMWM6Lln.htm)|Color Splash|Éclaboussure colorée|officielle|
|[GHc8hcw19z08DhNU.htm](extinction-curse-bestiary-items/GHc8hcw19z08DhNU.htm)|Soulfire Breath|Souffle de l'âme incendiaire|officielle|
|[gHELFZYck8aJljEX.htm](extinction-curse-bestiary-items/gHELFZYck8aJljEX.htm)|Negative Healing|Guérison négative|officielle|
|[gHi2vKCOAok2Xdj9.htm](extinction-curse-bestiary-items/gHi2vKCOAok2Xdj9.htm)|Frost Susceptibility|Sensibilité au froid|officielle|
|[ghqho4OQkcRArLP9.htm](extinction-curse-bestiary-items/ghqho4OQkcRArLP9.htm)|Hoe|Houe|officielle|
|[gi1EHeu40BAEjrvS.htm](extinction-curse-bestiary-items/gi1EHeu40BAEjrvS.htm)|Negative Healing|Guérison négative|officielle|
|[GiqRd4nRXj6C8YKM.htm](extinction-curse-bestiary-items/GiqRd4nRXj6C8YKM.htm)|Opportunistic Brawler|Bagarreur opportuniste|officielle|
|[GJ3bbiF7rgm9jQY8.htm](extinction-curse-bestiary-items/GJ3bbiF7rgm9jQY8.htm)|Claw|Griffe|officielle|
|[GJq1pfArAB262GZd.htm](extinction-curse-bestiary-items/GJq1pfArAB262GZd.htm)|Raptor Jaw Disarm|Désarmement par mâchoires de vélociraptor|officielle|
|[GkBHOD9XctIvvbNf.htm](extinction-curse-bestiary-items/GkBHOD9XctIvvbNf.htm)|Cleaver|Fendoir|officielle|
|[GkznkrnInAnqtWGs.htm](extinction-curse-bestiary-items/GkznkrnInAnqtWGs.htm)|Abberton Lore|Connaissance d'Abberville|officielle|
|[gkZZB6VEmnazsd8w.htm](extinction-curse-bestiary-items/gkZZB6VEmnazsd8w.htm)|+2 Greater Resilient Studded Leather Armor|Armure en cuir clouté de résilience supérieure +2|officielle|
|[glENAMfpx6hiXDjN.htm](extinction-curse-bestiary-items/glENAMfpx6hiXDjN.htm)|Tongue|Langue|officielle|
|[glkLv9FBF94aocjz.htm](extinction-curse-bestiary-items/glkLv9FBF94aocjz.htm)|Flame Spit|Crachat de feu|officielle|
|[Gm2RUsy31ZQjt84W.htm](extinction-curse-bestiary-items/Gm2RUsy31ZQjt84W.htm)|Jaws|Mâchoires|officielle|
|[gmbDVefxioirNHej.htm](extinction-curse-bestiary-items/gmbDVefxioirNHej.htm)|Feather Fall (At Will) (Self Only)|Feuille morte (À volonté) (soi uniquement)|officielle|
|[gnbyS4J4UvV5J0pq.htm](extinction-curse-bestiary-items/gnbyS4J4UvV5J0pq.htm)|Deadly Aim|Visée mortelle|officielle|
|[GoczydT7BBRPFRMa.htm](extinction-curse-bestiary-items/GoczydT7BBRPFRMa.htm)|Trample|Piétinement|officielle|
|[GoIeG0DnvMbYVA6T.htm](extinction-curse-bestiary-items/GoIeG0DnvMbYVA6T.htm)|Scent (Imprecise) 60 feet|Odorat 18 m (imprécis)|officielle|
|[GP3KcxrUOzub9EVd.htm](extinction-curse-bestiary-items/GP3KcxrUOzub9EVd.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[GrkAR3xwWPZIyTAt.htm](extinction-curse-bestiary-items/GrkAR3xwWPZIyTAt.htm)|Fangs|Crocs|officielle|
|[GrscH4cYbnoD8K5i.htm](extinction-curse-bestiary-items/GrscH4cYbnoD8K5i.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|officielle|
|[gRXkTbTggZI3GBGZ.htm](extinction-curse-bestiary-items/gRXkTbTggZI3GBGZ.htm)|Longspear|+1|Pique +1|libre|
|[gSUoDPZCnPwyLrzQ.htm](extinction-curse-bestiary-items/gSUoDPZCnPwyLrzQ.htm)|Horn|Corne|officielle|
|[GTru3mYViGeiuDV3.htm](extinction-curse-bestiary-items/GTru3mYViGeiuDV3.htm)|Fist|Poing|officielle|
|[guwEcneKYJeKnJ6x.htm](extinction-curse-bestiary-items/guwEcneKYJeKnJ6x.htm)|Jaws|Mâchoires|officielle|
|[gWGzUCdK9CnnBc97.htm](extinction-curse-bestiary-items/gWGzUCdK9CnnBc97.htm)|Spiked Gauntlet|Gantelet à pointes|officielle|
|[GWziNffDvPJKsyJE.htm](extinction-curse-bestiary-items/GWziNffDvPJKsyJE.htm)|Wicked Bite|Méchante morsure|officielle|
|[GXC9PnE9TL5hNcx5.htm](extinction-curse-bestiary-items/GXC9PnE9TL5hNcx5.htm)|Falling Stones|Chute de pierres|officielle|
|[gYLcvHg47lmk8WVF.htm](extinction-curse-bestiary-items/gYLcvHg47lmk8WVF.htm)|Negative Healing|Guérison négative|officielle|
|[h0hJAD32OkrXK3lx.htm](extinction-curse-bestiary-items/h0hJAD32OkrXK3lx.htm)|Wolf Empathy|Empathie avec les loups|officielle|
|[H1uqJEQyZBDutvmh.htm](extinction-curse-bestiary-items/H1uqJEQyZBDutvmh.htm)|Tremorsense (Imprecise) 60 feet|Perception des vibrations 18 m (imprécis)|officielle|
|[H3lSdbz8S3BWprZ6.htm](extinction-curse-bestiary-items/H3lSdbz8S3BWprZ6.htm)|Striking Fear|Terreur saisissante|officielle|
|[H5lFEWzr0REUektR.htm](extinction-curse-bestiary-items/H5lFEWzr0REUektR.htm)|Constant Spells|Sorts constants|officielle|
|[H8wjoy6dgcpleGxb.htm](extinction-curse-bestiary-items/H8wjoy6dgcpleGxb.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[H925aaHFfAyXeLkl.htm](extinction-curse-bestiary-items/H925aaHFfAyXeLkl.htm)|Darkvision|Vision dans le noir|officielle|
|[H9E215npIwweVezi.htm](extinction-curse-bestiary-items/H9E215npIwweVezi.htm)|Darkvision|Vision dans le noir|libre|
|[h9vBtsjNnNHixcDh.htm](extinction-curse-bestiary-items/h9vBtsjNnNHixcDh.htm)|Constant Spells|Sorts constants|officielle|
|[h9wGVkU8tCqqBBXU.htm](extinction-curse-bestiary-items/h9wGVkU8tCqqBBXU.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[HAq5bDyta4z3yz98.htm](extinction-curse-bestiary-items/HAq5bDyta4z3yz98.htm)|Perverse Prayer|Prière inique|officielle|
|[hcpegSxpNx41ckc9.htm](extinction-curse-bestiary-items/hcpegSxpNx41ckc9.htm)|Brutal Blow|Coup brutal|officielle|
|[hcqt8oyd2ReY1Sbz.htm](extinction-curse-bestiary-items/hcqt8oyd2ReY1Sbz.htm)|Cleaver|Fendoir|officielle|
|[hCTilN2T9gjd8N5c.htm](extinction-curse-bestiary-items/hCTilN2T9gjd8N5c.htm)|Willowside Lore|Connaissance de Rive-des-Saules|officielle|
|[hd8pBhwAE36GIvt7.htm](extinction-curse-bestiary-items/hd8pBhwAE36GIvt7.htm)|Darkvision|Vision dans le noir|officielle|
|[heTem3QEjhzZKzbj.htm](extinction-curse-bestiary-items/heTem3QEjhzZKzbj.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la Magie|officielle|
|[Heyof9Emy1vFTnqq.htm](extinction-curse-bestiary-items/Heyof9Emy1vFTnqq.htm)|Frightful Presence|Présence terrifiante|officielle|
|[HGadzUCJdCezQNEJ.htm](extinction-curse-bestiary-items/HGadzUCJdCezQNEJ.htm)|Mauler|Écharpeur|officielle|
|[hgAe4LDV9pOetFnG.htm](extinction-curse-bestiary-items/hgAe4LDV9pOetFnG.htm)|Eerie Flexibility|Souplesse surnaturelle|officielle|
|[hhgKxBXjAKv8GaOe.htm](extinction-curse-bestiary-items/hhgKxBXjAKv8GaOe.htm)|Generate Bomb|Création de bombe|officielle|
|[hHMgZMry8AKd125Z.htm](extinction-curse-bestiary-items/hHMgZMry8AKd125Z.htm)|Elixir of Life (Greater) [Infused]|Élixir de vie supérieur (imprégné)|officielle|
|[hKCyP0kuF8Zoh3ey.htm](extinction-curse-bestiary-items/hKCyP0kuF8Zoh3ey.htm)|Bedazzling|Éblouissante|officielle|
|[hmpuTKDBrivG8uJz.htm](extinction-curse-bestiary-items/hmpuTKDBrivG8uJz.htm)|Sylvan Wine|Vin sylvestre|officielle|
|[HnvK1KfAAcXoF3Gq.htm](extinction-curse-bestiary-items/HnvK1KfAAcXoF3Gq.htm)|Composite Shortbow|Arc court composite|officielle|
|[hoeJkPRGT9A1gfra.htm](extinction-curse-bestiary-items/hoeJkPRGT9A1gfra.htm)|Claw|Griffe|officielle|
|[hoHQP1kFuvAU3pGi.htm](extinction-curse-bestiary-items/hoHQP1kFuvAU3pGi.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[hOlYawxI9XqrexmA.htm](extinction-curse-bestiary-items/hOlYawxI9XqrexmA.htm)|Fleet Performer|Artiste leste|officielle|
|[HorpW2NucpQNU3sV.htm](extinction-curse-bestiary-items/HorpW2NucpQNU3sV.htm)|Darkvision|Vision dans le noir|officielle|
|[HRNkLkrR4YcOavPi.htm](extinction-curse-bestiary-items/HRNkLkrR4YcOavPi.htm)|At-Will Spells|Sorts à volonté|officielle|
|[hrq73xXhTyT182d2.htm](extinction-curse-bestiary-items/hrq73xXhTyT182d2.htm)|Suppressed Alignment|Alignement supprimé|officielle|
|[hRyOqsE8Eje4tw06.htm](extinction-curse-bestiary-items/hRyOqsE8Eje4tw06.htm)|Survival|Survie|officielle|
|[HSdzmGpD9SX9gMti.htm](extinction-curse-bestiary-items/HSdzmGpD9SX9gMti.htm)|Charm (Animals Only)|Charme (animaux uniquement)|officielle|
|[HtbPnLHYwn4NLjKm.htm](extinction-curse-bestiary-items/HtbPnLHYwn4NLjKm.htm)|Purple Worm Venom|Venin de ver pourpre|officielle|
|[HthRagLsoKlSBX0L.htm](extinction-curse-bestiary-items/HthRagLsoKlSBX0L.htm)|Sand Spear|Lance de sable|officielle|
|[HTSkrC7WJF2LvhAY.htm](extinction-curse-bestiary-items/HTSkrC7WJF2LvhAY.htm)|Quicksilver Mutagen (Greater) (Infused)|Mutagène de vif-argent supérieur (imprégné)|officielle|
|[HuhJDJ9P5pLj5Asb.htm](extinction-curse-bestiary-items/HuhJDJ9P5pLj5Asb.htm)|Claw|Griffe|officielle|
|[hXMssBGUnnIgsqkV.htm](extinction-curse-bestiary-items/hXMssBGUnnIgsqkV.htm)|Darkvision|Vision dans le noir|officielle|
|[hz1IPPAcQC4UoSnA.htm](extinction-curse-bestiary-items/hz1IPPAcQC4UoSnA.htm)|Grab|Empoignade|officielle|
|[HZ3H7Z76Xxjqqoqw.htm](extinction-curse-bestiary-items/HZ3H7Z76Xxjqqoqw.htm)|Rapid Strikes|Frappes rapides|officielle|
|[I07i3Rw3iXrxS2A9.htm](extinction-curse-bestiary-items/I07i3Rw3iXrxS2A9.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[i0jem1LORXY0csEi.htm](extinction-curse-bestiary-items/i0jem1LORXY0csEi.htm)|Change Shape|Changement de forme|officielle|
|[I0l5wzkOqX0rEsjo.htm](extinction-curse-bestiary-items/I0l5wzkOqX0rEsjo.htm)|Fear (At Will)|Terreur (À volonté)|officielle|
|[I3SJEMtzxGoUPoc7.htm](extinction-curse-bestiary-items/I3SJEMtzxGoUPoc7.htm)|Tremorsense (Imprecise) 60 feet|Perception des vibrations 18 m (imprécis)|officielle|
|[i621hyfk7W2Zq4X5.htm](extinction-curse-bestiary-items/i621hyfk7W2Zq4X5.htm)|Demonic Strength|Force démoniaque|officielle|
|[I7TcllhLdsgw39xY.htm](extinction-curse-bestiary-items/I7TcllhLdsgw39xY.htm)|Mancatcher|Attrape-coquin|officielle|
|[I8280Iq9dO7AtDc4.htm](extinction-curse-bestiary-items/I8280Iq9dO7AtDc4.htm)|Abyssal Plague|Peste abyssale|officielle|
|[I8blzGPi6zi9IEQ5.htm](extinction-curse-bestiary-items/I8blzGPi6zi9IEQ5.htm)|At-Will Spells|Sorts à volonté|libre|
|[i9Y89elHLIhcIh43.htm](extinction-curse-bestiary-items/i9Y89elHLIhcIh43.htm)|Telepathy 100 feet|Télépathie 30 m|officielle|
|[iADRxNCeaT5m51vW.htm](extinction-curse-bestiary-items/iADRxNCeaT5m51vW.htm)|Children of the Night|Enfants de la nuit|officielle|
|[iBalLMYNzfQfwcHn.htm](extinction-curse-bestiary-items/iBalLMYNzfQfwcHn.htm)|Herecite Deity|Divinité hérécite|officielle|
|[IbgOSKVasX4WGvhx.htm](extinction-curse-bestiary-items/IbgOSKVasX4WGvhx.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[iCJLRUtX82AUP977.htm](extinction-curse-bestiary-items/iCJLRUtX82AUP977.htm)|Roll Up|S'enrouler|officielle|
|[iCMgd2Z2HKq0Ewg3.htm](extinction-curse-bestiary-items/iCMgd2Z2HKq0Ewg3.htm)|Claw|Griffe|officielle|
|[ICQ6LdZTOXIh61jG.htm](extinction-curse-bestiary-items/ICQ6LdZTOXIh61jG.htm)|Negative Healing|Guérison négative|officielle|
|[ICsz8ij2XAYIvcoc.htm](extinction-curse-bestiary-items/ICsz8ij2XAYIvcoc.htm)|Warhammer|Marteau de guerre|officielle|
|[iCvFkYwUNj1fDYM9.htm](extinction-curse-bestiary-items/iCvFkYwUNj1fDYM9.htm)|Convergent Link|Lien convergent|officielle|
|[iCxGTcQkrWZdAtUe.htm](extinction-curse-bestiary-items/iCxGTcQkrWZdAtUe.htm)|Ravenous Tracker|Pisteur vorace|officielle|
|[idbSmKz24v98oQhN.htm](extinction-curse-bestiary-items/idbSmKz24v98oQhN.htm)|Sheriff Banyan's Map of the Sea Caves|Carte des cavernes marines ayant appartenu à la shérif Banian|officielle|
|[IdhAmzNMMDraoy7q.htm](extinction-curse-bestiary-items/IdhAmzNMMDraoy7q.htm)|Nimble Dodge|Esquive agile|officielle|
|[IeoEKCfC28dShDCr.htm](extinction-curse-bestiary-items/IeoEKCfC28dShDCr.htm)|Choke Slam|Choc empoigné|officielle|
|[ieZcxj746XMjV8LQ.htm](extinction-curse-bestiary-items/ieZcxj746XMjV8LQ.htm)|Jaws|Mâchoires|officielle|
|[iFh8asouVDS4zj2y.htm](extinction-curse-bestiary-items/iFh8asouVDS4zj2y.htm)|Bard Composition Spells|Sorts de composition de barde|libre|
|[ifTXKPVR6xsgBX4o.htm](extinction-curse-bestiary-items/ifTXKPVR6xsgBX4o.htm)|Antidote (Major) [Infused]|Antidore majeur (imprégné)|officielle|
|[iH7yVlvaFJF7bjNQ.htm](extinction-curse-bestiary-items/iH7yVlvaFJF7bjNQ.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[Ii6Er4G5VD71YDMV.htm](extinction-curse-bestiary-items/Ii6Er4G5VD71YDMV.htm)|Create Spawn|Création de rejetons|officielle|
|[IiFWeofaMoWPJb8k.htm](extinction-curse-bestiary-items/IiFWeofaMoWPJb8k.htm)|Opening Threat|Menace préalable|officielle|
|[iKOLPcoDilY7HLdL.htm](extinction-curse-bestiary-items/iKOLPcoDilY7HLdL.htm)|Mercy Vulnerability|Vulnérabilité à la miséricorde|officielle|
|[iKTJM59aw3XI1kNW.htm](extinction-curse-bestiary-items/iKTJM59aw3XI1kNW.htm)|Fist|Poing|officielle|
|[IKv1F31tfUJko4Kl.htm](extinction-curse-bestiary-items/IKv1F31tfUJko4Kl.htm)|Hoof|Sabot|officielle|
|[iMfJOLJk4i8ywGZb.htm](extinction-curse-bestiary-items/iMfJOLJk4i8ywGZb.htm)|Frightful Presence|Présence terrifiante|officielle|
|[imLjlss8L8M0heAz.htm](extinction-curse-bestiary-items/imLjlss8L8M0heAz.htm)|Circus Lore|Connaissance du cirque|officielle|
|[iO7f9ASpYCNIBid3.htm](extinction-curse-bestiary-items/iO7f9ASpYCNIBid3.htm)|Sanguine Rain|Pluie de sang|officielle|
|[IOV4sVgze4V4qLcw.htm](extinction-curse-bestiary-items/IOV4sVgze4V4qLcw.htm)|Fist|Poing|officielle|
|[IQ3sd03wmGJr3aIw.htm](extinction-curse-bestiary-items/IQ3sd03wmGJr3aIw.htm)|Fist|Poing|officielle|
|[IryGUwPWgCMPmKR1.htm](extinction-curse-bestiary-items/IryGUwPWgCMPmKR1.htm)|Sneak Attack|Attaque sournoise|officielle|
|[issxBVRAEt4rjlKI.htm](extinction-curse-bestiary-items/issxBVRAEt4rjlKI.htm)|Flaming Baton|Baguette enflammée|officielle|
|[iur53JebaExSunmH.htm](extinction-curse-bestiary-items/iur53JebaExSunmH.htm)|Iron Sword|Epée de fer|officielle|
|[IuSKKdURFPmkHFi8.htm](extinction-curse-bestiary-items/IuSKKdURFPmkHFi8.htm)|Grab|Empoignade|officielle|
|[IUZN9K8piSmzzwJE.htm](extinction-curse-bestiary-items/IUZN9K8piSmzzwJE.htm)|Skirmishing Movement|Déplacement du tirailleur|officielle|
|[IvDxsg9WiiyQoCtb.htm](extinction-curse-bestiary-items/IvDxsg9WiiyQoCtb.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[iWDeWv63BnPfzCqn.htm](extinction-curse-bestiary-items/iWDeWv63BnPfzCqn.htm)|Surprise Attack|Attaque surprise|officielle|
|[ix9mbv1z3onh9Lsi.htm](extinction-curse-bestiary-items/ix9mbv1z3onh9Lsi.htm)|Longsword|+2,greaterStriking|Épée longue de frappe supérieure +2|libre|
|[IXhK0WImaHn472zp.htm](extinction-curse-bestiary-items/IXhK0WImaHn472zp.htm)|Ram|Charge de bélier|officielle|
|[iYbZ0U36eZpGPrvb.htm](extinction-curse-bestiary-items/iYbZ0U36eZpGPrvb.htm)|Rhoka Sword|+1,striking|Épée rhoka de frappe +1|libre|
|[iyWevLBiGLmYMOP5.htm](extinction-curse-bestiary-items/iyWevLBiGLmYMOP5.htm)|Frightful Presence|Présence terrifiante|officielle|
|[IZ8Omnth78RPYDTU.htm](extinction-curse-bestiary-items/IZ8Omnth78RPYDTU.htm)|Sneak Attack|Attaque sournoise|officielle|
|[izc4ztOBcRsH7EKB.htm](extinction-curse-bestiary-items/izc4ztOBcRsH7EKB.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|libre|
|[iZhufeHVk6VtD2Mw.htm](extinction-curse-bestiary-items/iZhufeHVk6VtD2Mw.htm)|Jaws|Mâchoires|officielle|
|[j0wzmEVge78zbfkb.htm](extinction-curse-bestiary-items/j0wzmEVge78zbfkb.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|officielle|
|[j1ofFhuO1mqyLeZV.htm](extinction-curse-bestiary-items/j1ofFhuO1mqyLeZV.htm)|Darkvision|Vision dans le noir|officielle|
|[j2hIRiY8Zn7Egfco.htm](extinction-curse-bestiary-items/j2hIRiY8Zn7Egfco.htm)|Golem Antimagic|Antimagie du golem|officielle|
|[j7QVtuN08hfCGEUq.htm](extinction-curse-bestiary-items/j7QVtuN08hfCGEUq.htm)|Keen Eyes|Yeux perçants|officielle|
|[J7YUxP9ma7sbY5rl.htm](extinction-curse-bestiary-items/J7YUxP9ma7sbY5rl.htm)|Claw|Griffe|officielle|
|[J9r3EGdiW0V79wgo.htm](extinction-curse-bestiary-items/J9r3EGdiW0V79wgo.htm)|Dimension Door (At Will)|Porte dimensionnelle (À volonté)|officielle|
|[JAdbfBOcQMFxEEUU.htm](extinction-curse-bestiary-items/JAdbfBOcQMFxEEUU.htm)|Staff of the Magi|+3,majorStriking|Bâton de thaumaturge|officielle|
|[jajMdZuaov0LtkBl.htm](extinction-curse-bestiary-items/jajMdZuaov0LtkBl.htm)|Call to Halt|Halte-là|officielle|
|[jAotzs7DanRzRCVW.htm](extinction-curse-bestiary-items/jAotzs7DanRzRCVW.htm)|Claw|Griffe|officielle|
|[jaYwPmKt8kxpFg9N.htm](extinction-curse-bestiary-items/jaYwPmKt8kxpFg9N.htm)|Feral Directive|Ordre féroce|officielle|
|[jBd2Z17zgzpxyvDF.htm](extinction-curse-bestiary-items/jBd2Z17zgzpxyvDF.htm)|Sneak Attack|Attaque sournoise|officielle|
|[JbKAKCipLhCSyGfm.htm](extinction-curse-bestiary-items/JbKAKCipLhCSyGfm.htm)|Swallow Whole|Gober|officielle|
|[jBPbNNVD1lCQ5GZC.htm](extinction-curse-bestiary-items/jBPbNNVD1lCQ5GZC.htm)|Spittle|Postillons|officielle|
|[jBwBgMthqpbCxl50.htm](extinction-curse-bestiary-items/jBwBgMthqpbCxl50.htm)|Circus Lore|Connaissance du cirque|officielle|
|[JBZGDHmgcSKsrjtc.htm](extinction-curse-bestiary-items/JBZGDHmgcSKsrjtc.htm)|Stand Still|Ne bouge plus|officielle|
|[jcxnj2Ps2GYGCILS.htm](extinction-curse-bestiary-items/jcxnj2Ps2GYGCILS.htm)|Sand Fist|Poing de sable|officielle|
|[jD5e8vnSLE6rJyBA.htm](extinction-curse-bestiary-items/jD5e8vnSLE6rJyBA.htm)|At-Will Spells|Sorts à volonté|officielle|
|[jG3Y1PfZ9WG6fVyv.htm](extinction-curse-bestiary-items/jG3Y1PfZ9WG6fVyv.htm)|Fly (Constant)|Vol (constant)|officielle|
|[jgEA3ptMljnGxy1j.htm](extinction-curse-bestiary-items/jgEA3ptMljnGxy1j.htm)|Forest Jaunt|Balade en forêt|officielle|
|[jGq0rfEKsdYFGYmW.htm](extinction-curse-bestiary-items/jGq0rfEKsdYFGYmW.htm)|Bounce-Bounce|Bond-Bond|officielle|
|[jiZ3qpQbCw5jBgSz.htm](extinction-curse-bestiary-items/jiZ3qpQbCw5jBgSz.htm)|At-Will Spells|Sorts à volonté|officielle|
|[jk7KLiC4TT506huA.htm](extinction-curse-bestiary-items/jk7KLiC4TT506huA.htm)|Fist|Poing|officielle|
|[JlbjzKNAD6l6yzqs.htm](extinction-curse-bestiary-items/JlbjzKNAD6l6yzqs.htm)|Eat Anything|Mange-tout|officielle|
|[JLMrybXzf35bFGZ5.htm](extinction-curse-bestiary-items/JLMrybXzf35bFGZ5.htm)|Bloodsense (Precise) 30 feet|Perception du sang|officielle|
|[JlpJ89IL7ZyHUWgp.htm](extinction-curse-bestiary-items/JlpJ89IL7ZyHUWgp.htm)|Athletics|Athlétisme|officielle|
|[jlvhbKSEAZkBHBIm.htm](extinction-curse-bestiary-items/jlvhbKSEAZkBHBIm.htm)|Longbow|+1,striking|Arc long de frappe +1|libre|
|[jMeQhmA9WidXyyaB.htm](extinction-curse-bestiary-items/jMeQhmA9WidXyyaB.htm)|Churning Frenzy|Frénésie bouillonnante|officielle|
|[JNfFOPN58ILPiAjU.htm](extinction-curse-bestiary-items/JNfFOPN58ILPiAjU.htm)|Harm (At-Will)|Mise à mal (À volonté)|officielle|
|[joVEQgf7vPuTLuTt.htm](extinction-curse-bestiary-items/joVEQgf7vPuTLuTt.htm)|+37 Will Save vs. Emotion Effects|Volonté +37 contre les effets d'émotion|officielle|
|[jovXFUyi4oe3ZNFR.htm](extinction-curse-bestiary-items/jovXFUyi4oe3ZNFR.htm)|Alchemist's Fire|Feu grégeois|officielle|
|[JOyY3xr5i8Bxot4t.htm](extinction-curse-bestiary-items/JOyY3xr5i8Bxot4t.htm)|Feather Fall (Self Only)|Feuille morte (soi uniquement)|officielle|
|[jp2sVLnIKEVPq8o6.htm](extinction-curse-bestiary-items/jp2sVLnIKEVPq8o6.htm)|Staff|Bâton|officielle|
|[jpECNxPropkVu94M.htm](extinction-curse-bestiary-items/jpECNxPropkVu94M.htm)|Mobility|Mobilité|officielle|
|[JPP8jkjRT4UlELJV.htm](extinction-curse-bestiary-items/JPP8jkjRT4UlELJV.htm)|Claw|Griffe|officielle|
|[jQn2KwqwhEwFFaZA.htm](extinction-curse-bestiary-items/jQn2KwqwhEwFFaZA.htm)|Fist|Poing|officielle|
|[jrcPXASGsebozgzh.htm](extinction-curse-bestiary-items/jrcPXASGsebozgzh.htm)|Jaws|Mâchoires|officielle|
|[JrRQ3eTuGR50kL6B.htm](extinction-curse-bestiary-items/JrRQ3eTuGR50kL6B.htm)|Roll the Bones|S'en remettre aux dés|officielle|
|[JsETPxZAqYOAGagW.htm](extinction-curse-bestiary-items/JsETPxZAqYOAGagW.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|officielle|
|[jSG9ZWWLJseQGg4X.htm](extinction-curse-bestiary-items/jSG9ZWWLJseQGg4X.htm)|Fist|Poing|officielle|
|[jsL5jhGaz2pctkeU.htm](extinction-curse-bestiary-items/jsL5jhGaz2pctkeU.htm)|Restrain|Entrave|officielle|
|[JTK8CW0TwOok1X9Z.htm](extinction-curse-bestiary-items/JTK8CW0TwOok1X9Z.htm)|Thunderstone Explosion|Explosion de pierres à tonnerre|officielle|
|[jtlZEA7cK11MXoOX.htm](extinction-curse-bestiary-items/jtlZEA7cK11MXoOX.htm)|Criminal Lore|Connaissance des crimes|officielle|
|[JtY4GmPSaE1qxfgU.htm](extinction-curse-bestiary-items/JtY4GmPSaE1qxfgU.htm)|No Vision|Dénuée de vision|officielle|
|[jU8gscCDgYLL4jRa.htm](extinction-curse-bestiary-items/jU8gscCDgYLL4jRa.htm)|Acrobatics|Acrobaties|officielle|
|[JUAv4A165EW9vjcX.htm](extinction-curse-bestiary-items/JUAv4A165EW9vjcX.htm)|Constant Spells|Sorts constants|officielle|
|[jUDRlSsIxqf6G6h3.htm](extinction-curse-bestiary-items/jUDRlSsIxqf6G6h3.htm)|Drain Life|Drain de vie|officielle|
|[JW7pj6pjhMG4D95l.htm](extinction-curse-bestiary-items/JW7pj6pjhMG4D95l.htm)|Residual Grease|Résidus graisseux|officielle|
|[jwBiXSOEkYsL5V2p.htm](extinction-curse-bestiary-items/jwBiXSOEkYsL5V2p.htm)|Sidestep|Pas de côté|officielle|
|[jwtvc92eNGbKi3wr.htm](extinction-curse-bestiary-items/jwtvc92eNGbKi3wr.htm)|Rhoka Sword|Épée rhoka|officielle|
|[Jxth7CtaG4IyFCWC.htm](extinction-curse-bestiary-items/Jxth7CtaG4IyFCWC.htm)|Composite Shortbow|Arc court composite|officielle|
|[JXwGkvpYAsrmdfj9.htm](extinction-curse-bestiary-items/JXwGkvpYAsrmdfj9.htm)|Beguile the Addled|Séduire les esprits confus|officielle|
|[JyWVziAy6MCtn3Nb.htm](extinction-curse-bestiary-items/JyWVziAy6MCtn3Nb.htm)|Formula Book|Livre de formules|officielle|
|[Jz2nWIs5hfh5nWak.htm](extinction-curse-bestiary-items/Jz2nWIs5hfh5nWak.htm)|Powerful Stench|Puissante puanteur|officielle|
|[k0JyHOPZZzOu7DCM.htm](extinction-curse-bestiary-items/k0JyHOPZZzOu7DCM.htm)|Fanatical Juggernaut|Juggernaut fanatique|officielle|
|[k0Pbu3xoSfQc9iEi.htm](extinction-curse-bestiary-items/k0Pbu3xoSfQc9iEi.htm)|Darkvision|Vision dans le noir|officielle|
|[k1bxsxdo1I2iTOLW.htm](extinction-curse-bestiary-items/k1bxsxdo1I2iTOLW.htm)|Innate Divine Spells|Sorts divins innés|libre|
|[k1Ehx0dobB1JSYjA.htm](extinction-curse-bestiary-items/k1Ehx0dobB1JSYjA.htm)|Swift Leap|Bond rapide|officielle|
|[k1jWhjh5eXKkavLa.htm](extinction-curse-bestiary-items/k1jWhjh5eXKkavLa.htm)|Frightful Presence|Présence terrifiante|officielle|
|[k3I5F189xsmhpHsC.htm](extinction-curse-bestiary-items/k3I5F189xsmhpHsC.htm)|Composite Longbow|+1,striking|Arc long composite de frappe +1|officielle|
|[k5PXZtu7pMNd0Hx4.htm](extinction-curse-bestiary-items/k5PXZtu7pMNd0Hx4.htm)|Criminal Lore|Connaissance des crimes|officielle|
|[K6KEcVCjAgpkCZuv.htm](extinction-curse-bestiary-items/K6KEcVCjAgpkCZuv.htm)|At-Will Spells|Sorts à volonté|officielle|
|[K7VOxwpZ7Na4TBFK.htm](extinction-curse-bestiary-items/K7VOxwpZ7Na4TBFK.htm)|+1 Resilient Breastplate|Cuirasse de résilience +1|officielle|
|[K8HQpzjGee43PFeB.htm](extinction-curse-bestiary-items/K8HQpzjGee43PFeB.htm)|Light Mace|+1|Masse légère +1|libre|
|[k9lNa3pfAPN99oJd.htm](extinction-curse-bestiary-items/k9lNa3pfAPN99oJd.htm)|Convergent Tactics|Stratégie convergente|officielle|
|[kANFuSENY1afffH8.htm](extinction-curse-bestiary-items/kANFuSENY1afffH8.htm)|Spiked Gauntlet|+1,striking|Gantelet clouté de frappe +1|libre|
|[kBizs5Rm1STeq41l.htm](extinction-curse-bestiary-items/kBizs5Rm1STeq41l.htm)|Aura of Smoke|Aura de fumée|officielle|
|[kCl7ix3UfVgsJxQ8.htm](extinction-curse-bestiary-items/kCl7ix3UfVgsJxQ8.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[KczcQ2zt8BqH8GXB.htm](extinction-curse-bestiary-items/KczcQ2zt8BqH8GXB.htm)|Enraging Stench|Puanteur enrageante|officielle|
|[KDeAmqNZ8NGjjpMZ.htm](extinction-curse-bestiary-items/KDeAmqNZ8NGjjpMZ.htm)|Levitate (At Will)|Lévitation (À volonté)|officielle|
|[KdGsDBgPGKlQjwFc.htm](extinction-curse-bestiary-items/KdGsDBgPGKlQjwFc.htm)|Trample|Piétinement|officielle|
|[kdH6teRXPVQOdmdp.htm](extinction-curse-bestiary-items/kdH6teRXPVQOdmdp.htm)|Shoony Lore|Connaissance des Shounis|officielle|
|[kdKmscxdhMu94gTS.htm](extinction-curse-bestiary-items/kdKmscxdhMu94gTS.htm)|Frost Vial (Greater) (Infused)|Fiole de givre supérieure (imprégnée)|officielle|
|[KeuZBLboNz2Rx0ou.htm](extinction-curse-bestiary-items/KeuZBLboNz2Rx0ou.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[kGDDuORhStpjLtdf.htm](extinction-curse-bestiary-items/kGDDuORhStpjLtdf.htm)|Jaws|Mâchoires|officielle|
|[kgOx9c22i9ZSZt40.htm](extinction-curse-bestiary-items/kgOx9c22i9ZSZt40.htm)|Negative Healing|Guérison négative|officielle|
|[KGXwqGgOZpDp0Ck3.htm](extinction-curse-bestiary-items/KGXwqGgOZpDp0Ck3.htm)|+1 Hide Armor|Armure de peau +1|officielle|
|[kHgE08pNOtxoY0UO.htm](extinction-curse-bestiary-items/kHgE08pNOtxoY0UO.htm)|Low-Light Vision|Vision nocturne|officielle|
|[kHpIOS55WOLGDdil.htm](extinction-curse-bestiary-items/kHpIOS55WOLGDdil.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|libre|
|[kiapf7DQYgbxC5NC.htm](extinction-curse-bestiary-items/kiapf7DQYgbxC5NC.htm)|Frightful Presence|Présence terrifiante|officielle|
|[kifvfBeEd0qFnfEN.htm](extinction-curse-bestiary-items/kifvfBeEd0qFnfEN.htm)|Constant Spells|Sorts constants|libre|
|[Kke4izO3OxCZuJm6.htm](extinction-curse-bestiary-items/Kke4izO3OxCZuJm6.htm)|Grab|Empoignade|officielle|
|[KkjtCnHpVjDcnDIb.htm](extinction-curse-bestiary-items/KkjtCnHpVjDcnDIb.htm)|+2 Greater Resilient Full Plate|Harnois de résilience supérieure +2|officielle|
|[KkvACSDVhU76AEdA.htm](extinction-curse-bestiary-items/KkvACSDVhU76AEdA.htm)|Negative Healing|Guérison négative|officielle|
|[KLJhKMXJejeXccOV.htm](extinction-curse-bestiary-items/KLJhKMXJejeXccOV.htm)|Jaws|Mâchoires|officielle|
|[km1pCQZC65kAx4WY.htm](extinction-curse-bestiary-items/km1pCQZC65kAx4WY.htm)|Bastard Sword|Épée bâtarde|officielle|
|[kmD7smfQheBtKJZb.htm](extinction-curse-bestiary-items/kmD7smfQheBtKJZb.htm)|Fist|Poing|officielle|
|[kNi8uFBuD1YHDFE8.htm](extinction-curse-bestiary-items/kNi8uFBuD1YHDFE8.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[kNNjjTC0b0ZPTnII.htm](extinction-curse-bestiary-items/kNNjjTC0b0ZPTnII.htm)|Speak with Animals (Constant)|Communication avec les animaux (constant)|officielle|
|[KnZpH9lLGq7oF6GH.htm](extinction-curse-bestiary-items/KnZpH9lLGq7oF6GH.htm)|Acrobatics|Acrobaties|officielle|
|[KOREkPHjaugDT5fn.htm](extinction-curse-bestiary-items/KOREkPHjaugDT5fn.htm)|Religious Symbol of Urgathoa (Silver)|Symbole religieux en argent d'Urgathoa|officielle|
|[Kp2VQoJVTR9vUiFM.htm](extinction-curse-bestiary-items/Kp2VQoJVTR9vUiFM.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|officielle|
|[kqKZCfaYLS8DSSjM.htm](extinction-curse-bestiary-items/kqKZCfaYLS8DSSjM.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[KQUnhrJmsbAJPZik.htm](extinction-curse-bestiary-items/KQUnhrJmsbAJPZik.htm)|Scroll of Maze (Level 8)|Parchemin de Dédale (niveau 8)|officielle|
|[KqUSudmO5lFvURLO.htm](extinction-curse-bestiary-items/KqUSudmO5lFvURLO.htm)|Sting of the Lash|Cinglement du fouet|officielle|
|[Kqva1BHv5UXhAeIW.htm](extinction-curse-bestiary-items/Kqva1BHv5UXhAeIW.htm)|Telekinetic Maneuver (At Will)|Manoeuvre télékinésique (À volonté)|officielle|
|[kQzJhTzViDZxfegS.htm](extinction-curse-bestiary-items/kQzJhTzViDZxfegS.htm)|Bottle|Bouteille|officielle|
|[krSzk0G6TRACGHQK.htm](extinction-curse-bestiary-items/krSzk0G6TRACGHQK.htm)|Stench|Puanteur|officielle|
|[KsOqiokf6euChMn3.htm](extinction-curse-bestiary-items/KsOqiokf6euChMn3.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|officielle|
|[KTuygUzynAd3MJFg.htm](extinction-curse-bestiary-items/KTuygUzynAd3MJFg.htm)|Jaws|Mâchoires|officielle|
|[KUODkAXkx2zLQ2SC.htm](extinction-curse-bestiary-items/KUODkAXkx2zLQ2SC.htm)|Jaws|Mâchoires|officielle|
|[kV6UuNK9ctIBBWvX.htm](extinction-curse-bestiary-items/kV6UuNK9ctIBBWvX.htm)|Farming Lore|Connaissance agricole|officielle|
|[kV8gViNdEEYh26gD.htm](extinction-curse-bestiary-items/kV8gViNdEEYh26gD.htm)|+2 Greater Resilient Mithral Breastplate (Standard-Grade)|Cuirasse en mithral de qualité standard de résilience supérieure +2|officielle|
|[KvKBQk0h1QCw5ZC8.htm](extinction-curse-bestiary-items/KvKBQk0h1QCw5ZC8.htm)|Telekinetic Haul (At Will)|Transport télékinésique (À volonté)|officielle|
|[KVrMIzW0i6AHJQvS.htm](extinction-curse-bestiary-items/KVrMIzW0i6AHJQvS.htm)|The Maze Awakens|Le labyrinthe se réveille|officielle|
|[Kw8WsCyXZ9AnshFU.htm](extinction-curse-bestiary-items/Kw8WsCyXZ9AnshFU.htm)|Coward Sense (Imprecise) 60 feet|Perception des lâches|officielle|
|[KwKdT0U7saZ9u8qc.htm](extinction-curse-bestiary-items/KwKdT0U7saZ9u8qc.htm)|Roiling Mind|Esprit agité|officielle|
|[kwNOg7TY9zGl3ju9.htm](extinction-curse-bestiary-items/kwNOg7TY9zGl3ju9.htm)|Constant Spells|Sorts constants|officielle|
|[kWxHjxdCg01V9cxk.htm](extinction-curse-bestiary-items/kWxHjxdCg01V9cxk.htm)|At-Will Spells|Sorts à volonté|officielle|
|[kXtQUn28pNXI8ZAF.htm](extinction-curse-bestiary-items/kXtQUn28pNXI8ZAF.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|officielle|
|[KYxBpk7xiZXkkbFH.htm](extinction-curse-bestiary-items/KYxBpk7xiZXkkbFH.htm)|Spear|Lance|officielle|
|[L0Lzy1bN4NABxIgx.htm](extinction-curse-bestiary-items/L0Lzy1bN4NABxIgx.htm)|Evasion|Évasion|officielle|
|[l3yA4WK8vdnQnJJb.htm](extinction-curse-bestiary-items/l3yA4WK8vdnQnJJb.htm)|+1 Studded Leather Armor|Armure en cuir clouté +1|officielle|
|[L7PvaYLhErs0eOxg.htm](extinction-curse-bestiary-items/L7PvaYLhErs0eOxg.htm)|Wicked Bite|Méchante morsure|officielle|
|[L8YJHX8v6mfDBrMC.htm](extinction-curse-bestiary-items/L8YJHX8v6mfDBrMC.htm)|Spear|Lance|officielle|
|[l94suSMqUEuJilRY.htm](extinction-curse-bestiary-items/l94suSMqUEuJilRY.htm)|Vengeful Presence|Présence vengeresse|officielle|
|[L9otISXKarqdQVqY.htm](extinction-curse-bestiary-items/L9otISXKarqdQVqY.htm)|Darkvision|Vision dans le noir|officielle|
|[lajGLW4hoWGw1JCP.htm](extinction-curse-bestiary-items/lajGLW4hoWGw1JCP.htm)|Grab|Empoignade|officielle|
|[lB6Yf4JEFM6O42nb.htm](extinction-curse-bestiary-items/lB6Yf4JEFM6O42nb.htm)|Awakened|Éveillé|officielle|
|[lcfERNye3ZrCAlsu.htm](extinction-curse-bestiary-items/lcfERNye3ZrCAlsu.htm)|Shape Stone (At Will)|Façonnage de la pierre (À volonté)|officielle|
|[Ld3LrV6FM6WRbeYH.htm](extinction-curse-bestiary-items/Ld3LrV6FM6WRbeYH.htm)|Jaws|Mâchoires|officielle|
|[lDjbS8x4P6y41QbW.htm](extinction-curse-bestiary-items/lDjbS8x4P6y41QbW.htm)|Gout of Acid|Éclaboussure acide|officielle|
|[LDnG8zvyqote8tTp.htm](extinction-curse-bestiary-items/LDnG8zvyqote8tTp.htm)|Telepathy 100 feet|Télépathie 30 m|officielle|
|[LE0JjeyWLVVvqtCV.htm](extinction-curse-bestiary-items/LE0JjeyWLVVvqtCV.htm)|Naginata|Naginata|officielle|
|[lFbZsm0qVAkyKv1O.htm](extinction-curse-bestiary-items/lFbZsm0qVAkyKv1O.htm)|Vulnerable to Sunlight|Vulnérabilité à la lumière du soleil|officielle|
|[lFWZDfozTLjw9c8x.htm](extinction-curse-bestiary-items/lFWZDfozTLjw9c8x.htm)|Heart-Seeking Strike|Frappe perce-coeur|officielle|
|[lGFh9Y08etrUTHz5.htm](extinction-curse-bestiary-items/lGFh9Y08etrUTHz5.htm)|Fly (Constant)|Vol (constant)|officielle|
|[lGlw9snRuf39t1H3.htm](extinction-curse-bestiary-items/lGlw9snRuf39t1H3.htm)|Grab|Empoignade|officielle|
|[lhfpnOLNlbRjtgBB.htm](extinction-curse-bestiary-items/lhfpnOLNlbRjtgBB.htm)|Claw|Griffe|officielle|
|[lHtukJ46NfLrTp0w.htm](extinction-curse-bestiary-items/lHtukJ46NfLrTp0w.htm)|Grab|Empoignade|officielle|
|[LHyFqz22wgaucwGA.htm](extinction-curse-bestiary-items/LHyFqz22wgaucwGA.htm)|Jaws|Mâchoires|officielle|
|[LI5f4Jm5rjlsXN34.htm](extinction-curse-bestiary-items/LI5f4Jm5rjlsXN34.htm)|Petrification Overcharge|Surtension pétrifiante|officielle|
|[ljula66WZiHTsMFe.htm](extinction-curse-bestiary-items/ljula66WZiHTsMFe.htm)|See Invisibility (Constant)|Détection de l'invisibilité (constant)|officielle|
|[Ll1mko5i4mBq60YH.htm](extinction-curse-bestiary-items/Ll1mko5i4mBq60YH.htm)|Embrace of Death|Étreinte mortelle|officielle|
|[lLSRWP7Om7Huzd19.htm](extinction-curse-bestiary-items/lLSRWP7Om7Huzd19.htm)|Veil (Self Only)|Voile (soi uniquement)|officielle|
|[lMFrsrHGqi1hl36L.htm](extinction-curse-bestiary-items/lMFrsrHGqi1hl36L.htm)|Rattling Blow|Coup déstabilisant|officielle|
|[ln65krKkJthepPkH.htm](extinction-curse-bestiary-items/ln65krKkJthepPkH.htm)|Tail|Queue|officielle|
|[lnChTLzpk1PEBJ0E.htm](extinction-curse-bestiary-items/lnChTLzpk1PEBJ0E.htm)|Caustic Dart|Fléchette corrosive|officielle|
|[LnqJSEHKMH6xbkRx.htm](extinction-curse-bestiary-items/LnqJSEHKMH6xbkRx.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[lO6FqcK8sAscEaQN.htm](extinction-curse-bestiary-items/lO6FqcK8sAscEaQN.htm)|Darkvision|Vision dans le noir|libre|
|[lp862sClvRojxe2C.htm](extinction-curse-bestiary-items/lp862sClvRojxe2C.htm)|Phantom Sermon|Sermon fantomatique|officielle|
|[LqrQ3Kv7BGLa9Qyo.htm](extinction-curse-bestiary-items/LqrQ3Kv7BGLa9Qyo.htm)|Negative Healing|Guérison négative|officielle|
|[LRKpdpQoppWvEcLr.htm](extinction-curse-bestiary-items/LRKpdpQoppWvEcLr.htm)|Site Bound|Lié à un site|officielle|
|[ltmniMbuhHmyNIeh.htm](extinction-curse-bestiary-items/ltmniMbuhHmyNIeh.htm)|Spear|Lance|officielle|
|[ltyVtPkBGAreDacY.htm](extinction-curse-bestiary-items/ltyVtPkBGAreDacY.htm)|Suck Blood|Sucer le sang|officielle|
|[lu7LiJ9o5N69xKMd.htm](extinction-curse-bestiary-items/lu7LiJ9o5N69xKMd.htm)|Spear|Lance|officielle|
|[LURnplPbJOwNAozj.htm](extinction-curse-bestiary-items/LURnplPbJOwNAozj.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[luzb0P0GAIgFEHxS.htm](extinction-curse-bestiary-items/luzb0P0GAIgFEHxS.htm)|Curse of Defiled Idols|Malédiction des idoles profanées|officielle|
|[LVECyCtAv2DesAxX.htm](extinction-curse-bestiary-items/LVECyCtAv2DesAxX.htm)|Headbutt|Coup de tête|officielle|
|[LvMFhf8LPHSHd6Jd.htm](extinction-curse-bestiary-items/LvMFhf8LPHSHd6Jd.htm)|Claw|Griffe|officielle|
|[lwnO6Iuybc8JncAu.htm](extinction-curse-bestiary-items/lwnO6Iuybc8JncAu.htm)|Negative Healing|Guérison négative|officielle|
|[lWRG2NAbjrUze1M2.htm](extinction-curse-bestiary-items/lWRG2NAbjrUze1M2.htm)|Darkvision|Vision dans le noir|libre|
|[LxPQprD4lkZ2Wjc9.htm](extinction-curse-bestiary-items/LxPQprD4lkZ2Wjc9.htm)|Grand Finale|Clou du spectacle|officielle|
|[Ly6nitvAQ2kCd7K0.htm](extinction-curse-bestiary-items/Ly6nitvAQ2kCd7K0.htm)|Sickle|+1|Serpe +1|libre|
|[LyJY1n9lgwAKOFk7.htm](extinction-curse-bestiary-items/LyJY1n9lgwAKOFk7.htm)|Xulgath Bile|Bile xulgathe|officielle|
|[lyKcDYNprlsFkk9e.htm](extinction-curse-bestiary-items/lyKcDYNprlsFkk9e.htm)|Divine Prepared Spells|Sorts divins préparés|libre|
|[lYkQae8WS94MiKJn.htm](extinction-curse-bestiary-items/lYkQae8WS94MiKJn.htm)|Wight Spawn|Rejeton de nécrophage|officielle|
|[LzStFZ3TQx9YYYe4.htm](extinction-curse-bestiary-items/LzStFZ3TQx9YYYe4.htm)|Spear|Lance|officielle|
|[M07XMDmE5qHjyRuU.htm](extinction-curse-bestiary-items/M07XMDmE5qHjyRuU.htm)|Minion|Sbire|officielle|
|[m0wrjjZLBjDz3Ona.htm](extinction-curse-bestiary-items/m0wrjjZLBjDz3Ona.htm)|At-Will Spells|Sorts à volonté|libre|
|[m1KBPeMqrNwEMxby.htm](extinction-curse-bestiary-items/m1KBPeMqrNwEMxby.htm)|Aggressive Block|Blocage agressif|officielle|
|[m716szn1fHz5unyk.htm](extinction-curse-bestiary-items/m716szn1fHz5unyk.htm)|Tentacle Assault|Assaut tentaculaire|officielle|
|[mAwxYKlNfq1Lgofj.htm](extinction-curse-bestiary-items/mAwxYKlNfq1Lgofj.htm)|Spear|+1,striking|Lance de frappe +1|officielle|
|[mB66uu8XN9drrr9S.htm](extinction-curse-bestiary-items/mB66uu8XN9drrr9S.htm)|Cleaver|Couperet|officielle|
|[MBzAQ6mXHGE5nAla.htm](extinction-curse-bestiary-items/MBzAQ6mXHGE5nAla.htm)|Key to the Ringmaster's Wagon|Clé de la roulotte du directeur du cirque|officielle|
|[mC8ZXIDyGQCCGvee.htm](extinction-curse-bestiary-items/mC8ZXIDyGQCCGvee.htm)|Jaws|Mâchoires|officielle|
|[MCrT1usrhDxwMWCc.htm](extinction-curse-bestiary-items/MCrT1usrhDxwMWCc.htm)|Engulf|Engloutir|officielle|
|[mDgoSp5xBF4354YD.htm](extinction-curse-bestiary-items/mDgoSp5xBF4354YD.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[mdOM5OxBjVA6iPqa.htm](extinction-curse-bestiary-items/mdOM5OxBjVA6iPqa.htm)|Golem Antimagic|Antimagie du golem|officielle|
|[mDxntBa1Fvx5511p.htm](extinction-curse-bestiary-items/mDxntBa1Fvx5511p.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[MEvSBSRMvbzou7sj.htm](extinction-curse-bestiary-items/MEvSBSRMvbzou7sj.htm)|Dart Barrage|Déluge de fléchettes|officielle|
|[mEzBPI4zxWLerwvt.htm](extinction-curse-bestiary-items/mEzBPI4zxWLerwvt.htm)|Tail|Queue|officielle|
|[mezWuI2AxifmZTwP.htm](extinction-curse-bestiary-items/mezWuI2AxifmZTwP.htm)|Sneak Attack|Attaque sournoise|officielle|
|[MFsAqLtYbroqBTeA.htm](extinction-curse-bestiary-items/MFsAqLtYbroqBTeA.htm)|Arcane Innate Spells|Sorts arcaniques innés|libre|
|[MGk6Sbt5QtkGL8k3.htm](extinction-curse-bestiary-items/MGk6Sbt5QtkGL8k3.htm)|Claw|Griffe|officielle|
|[mH9WDo5EI4ZZJuTb.htm](extinction-curse-bestiary-items/mH9WDo5EI4ZZJuTb.htm)|Teleport (Self Only)|Téléportation (soi uniquement)|officielle|
|[MHRF7WqhFfgOn6ID.htm](extinction-curse-bestiary-items/MHRF7WqhFfgOn6ID.htm)|Beak|Bec|officielle|
|[miu3o0Zxz1ZBSRci.htm](extinction-curse-bestiary-items/miu3o0Zxz1ZBSRci.htm)|Sneak Savant|Science de la furtivité|officielle|
|[mJ6Xt01ZdOkIrWw2.htm](extinction-curse-bestiary-items/mJ6Xt01ZdOkIrWw2.htm)|Negative Healing|Guérison négative|officielle|
|[MJeOGWYLpgDMSTo4.htm](extinction-curse-bestiary-items/MJeOGWYLpgDMSTo4.htm)|Frost Vial (Greater) (Infused)|Fiole de givre supérieure [Imprégné]|officielle|
|[mK3VIhZzqlLlHNtN.htm](extinction-curse-bestiary-items/mK3VIhZzqlLlHNtN.htm)|Writhe Independently|Grouillement indépendant|officielle|
|[mnxX8zV7Y41pDoeS.htm](extinction-curse-bestiary-items/mnxX8zV7Y41pDoeS.htm)|Hasty Sacrifice|Sacrifice irréfléchi|officielle|
|[mOgh0bLWwjU5JzRo.htm](extinction-curse-bestiary-items/mOgh0bLWwjU5JzRo.htm)|Greater Darkvision|Vision dans le noir supérieure|officielle|
|[MoGzK6NqeB1NQ13G.htm](extinction-curse-bestiary-items/MoGzK6NqeB1NQ13G.htm)|Hungering Web|Toile affamée|officielle|
|[Mq5ICRhJZYQxQMyX.htm](extinction-curse-bestiary-items/Mq5ICRhJZYQxQMyX.htm)|At-Will Spells|Sorts à volonté|libre|
|[mQ8k4znou6fvkiXP.htm](extinction-curse-bestiary-items/mQ8k4znou6fvkiXP.htm)|Guiding Finish|Final directif|officielle|
|[MQLisfH68V3Wk7Bg.htm](extinction-curse-bestiary-items/MQLisfH68V3Wk7Bg.htm)|Fast Healing 5|Guérison accélérée 5|officielle|
|[MSrCpe3iS4MlI9Or.htm](extinction-curse-bestiary-items/MSrCpe3iS4MlI9Or.htm)|Enervating Tug|Souplesse surnaturelle|officielle|
|[MtbB1z53GXFLggFe.htm](extinction-curse-bestiary-items/MtbB1z53GXFLggFe.htm)|Alchemist's Fire (Moderate) (Infused)|Feu grégeois modéré (imprégné)|officielle|
|[mtxgvU7yVCk7qxpV.htm](extinction-curse-bestiary-items/mtxgvU7yVCk7qxpV.htm)|Command an Animal|Diriger un animal|officielle|
|[MvMDJ7fMPk4NO6bP.htm](extinction-curse-bestiary-items/MvMDJ7fMPk4NO6bP.htm)|Pounce|Bond|officielle|
|[MWMoiiaCQn1JlcdB.htm](extinction-curse-bestiary-items/MWMoiiaCQn1JlcdB.htm)|Change Shape|Changement de forme|officielle|
|[mWnwn5pBpT6VjGMB.htm](extinction-curse-bestiary-items/mWnwn5pBpT6VjGMB.htm)|Dimension Door (At Will)|Porte dimensionnelle (À volonté)|officielle|
|[mXs0X782H3FV7qJD.htm](extinction-curse-bestiary-items/mXs0X782H3FV7qJD.htm)|Flurry of Blows|Déluge de coups|officielle|
|[mxYaxKXMJuFXTuBN.htm](extinction-curse-bestiary-items/mxYaxKXMJuFXTuBN.htm)|Claw|Griffe|officielle|
|[mY4dvHb9fAotqjkc.htm](extinction-curse-bestiary-items/mY4dvHb9fAotqjkc.htm)|Surprise Attack|Attaque surprise|officielle|
|[MyPNYt7AI7qWY6wW.htm](extinction-curse-bestiary-items/MyPNYt7AI7qWY6wW.htm)|Alchemical Chambers|Compartiments alchimiques|officielle|
|[MzlHAoedA1ZLzlQi.htm](extinction-curse-bestiary-items/MzlHAoedA1ZLzlQi.htm)|Tail|Queue|officielle|
|[n08h7X8lJXldUrxY.htm](extinction-curse-bestiary-items/n08h7X8lJXldUrxY.htm)|Gozreh Lore|Connaissance de Gozreh|officielle|
|[n0pHuo2H6DjU4MBo.htm](extinction-curse-bestiary-items/n0pHuo2H6DjU4MBo.htm)|Cleric Domain Spells|Sorts de domaine de prêtre|libre|
|[n0wujzukuAjn0imB.htm](extinction-curse-bestiary-items/n0wujzukuAjn0imB.htm)|Dig Quickly|Creuser rapidement|officielle|
|[n178y9YC0VdwlUrF.htm](extinction-curse-bestiary-items/n178y9YC0VdwlUrF.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[N1mvyhPLJjZvHzqX.htm](extinction-curse-bestiary-items/N1mvyhPLJjZvHzqX.htm)|Rumbling Rocks|Roches grondantes|officielle|
|[n86sZNE8O0jn1kkF.htm](extinction-curse-bestiary-items/n86sZNE8O0jn1kkF.htm)|Humanoid Form (At Will)|Forme humanoïde (À volonté)|officielle|
|[NAIEy57GDQ8NKq2y.htm](extinction-curse-bestiary-items/NAIEy57GDQ8NKq2y.htm)|Low-Light Vision|Vision nocturne|officielle|
|[NBPD4xc1GPzZK1eR.htm](extinction-curse-bestiary-items/NBPD4xc1GPzZK1eR.htm)|Rhoka Sword|Épée rhoka|officielle|
|[nCMY5ROefFFtABDw.htm](extinction-curse-bestiary-items/nCMY5ROefFFtABDw.htm)|Darkvision|Vision dans le noir|officielle|
|[nDFUQPOK3ZLFR63i.htm](extinction-curse-bestiary-items/nDFUQPOK3ZLFR63i.htm)|Steady Spellcasting|Incantation fiable|officielle|
|[nDQsWnkhOjMNedYF.htm](extinction-curse-bestiary-items/nDQsWnkhOjMNedYF.htm)|Change Shape|Changement de forme|officielle|
|[neAhXUkNh8UEeJcf.htm](extinction-curse-bestiary-items/neAhXUkNh8UEeJcf.htm)|Claw|Griffe|officielle|
|[Neq70NbSJ4xaMcQZ.htm](extinction-curse-bestiary-items/Neq70NbSJ4xaMcQZ.htm)|Axiomatic Polymorph|Métamorphose axiomatique|officielle|
|[NHf4kQKVaYzotrz9.htm](extinction-curse-bestiary-items/NHf4kQKVaYzotrz9.htm)|Ghostly Hand|Main spectrale|officielle|
|[nhKkjCmqXzikihGp.htm](extinction-curse-bestiary-items/nhKkjCmqXzikihGp.htm)|Grab|Empoignade|officielle|
|[nIf5o2VCB8hURo6S.htm](extinction-curse-bestiary-items/nIf5o2VCB8hURo6S.htm)|Trident|Trident|officielle|
|[NjJyrUWQep7Xf1ap.htm](extinction-curse-bestiary-items/NjJyrUWQep7Xf1ap.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[nKJryW0RpvxmB4hH.htm](extinction-curse-bestiary-items/nKJryW0RpvxmB4hH.htm)|Striking Fear|Terreur saisissante|officielle|
|[nkY4eQPWGxaslKMa.htm](extinction-curse-bestiary-items/nkY4eQPWGxaslKMa.htm)|Augury (At Will)|Augure (À volonté)|officielle|
|[nlC81yf92sWZGQNO.htm](extinction-curse-bestiary-items/nlC81yf92sWZGQNO.htm)|Sharp Eyes|Regard perçant|officielle|
|[nlKm3K7VC3GevqjC.htm](extinction-curse-bestiary-items/nlKm3K7VC3GevqjC.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[NlljUPjO3NTY37sa.htm](extinction-curse-bestiary-items/NlljUPjO3NTY37sa.htm)|Stealth|Discrétion|officielle|
|[NlM6fsKFpKz5nVMX.htm](extinction-curse-bestiary-items/NlM6fsKFpKz5nVMX.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[nLZYrihD0JFqZ1pJ.htm](extinction-curse-bestiary-items/nLZYrihD0JFqZ1pJ.htm)|Circus Lore|Connaissance du cirque|officielle|
|[nN899egloBXvGHGV.htm](extinction-curse-bestiary-items/nN899egloBXvGHGV.htm)|Assault the Soul|Assaillir l'âme|officielle|
|[Nnkb3sPqsfGFOhtv.htm](extinction-curse-bestiary-items/Nnkb3sPqsfGFOhtv.htm)|Falchion|+1,striking|Cimeterre à deux mains de frappe +1|libre|
|[nNzZIeP9wu8KlZFp.htm](extinction-curse-bestiary-items/nNzZIeP9wu8KlZFp.htm)|Seawater Retch|Renvoi d'eau de mer|officielle|
|[NoasXhnYfse9eg51.htm](extinction-curse-bestiary-items/NoasXhnYfse9eg51.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[NqAs4SvCETnYSYOK.htm](extinction-curse-bestiary-items/NqAs4SvCETnYSYOK.htm)|Bolts coated with shadow essence|Carreaux recouverts d'Essence d'ombre|officielle|
|[NrFxf3psvDqHyWQv.htm](extinction-curse-bestiary-items/NrFxf3psvDqHyWQv.htm)|Ghost Crystal Cloud|Nuage de cristaux fantômes|officielle|
|[nrhu2A276xievQyN.htm](extinction-curse-bestiary-items/nrhu2A276xievQyN.htm)|At-Will Spells|Sorts à volonté|officielle|
|[NSj8GP5Uyy1BW0JE.htm](extinction-curse-bestiary-items/NSj8GP5Uyy1BW0JE.htm)|Air Walk (Constant)|Marche dans les airs (constant)|officielle|
|[nSWM6vrnzV4iaYnK.htm](extinction-curse-bestiary-items/nSWM6vrnzV4iaYnK.htm)|Tusk|Défense|officielle|
|[nTRfMlXAqXK1i2VK.htm](extinction-curse-bestiary-items/nTRfMlXAqXK1i2VK.htm)|At-Will Spells|Sorts à volonté|officielle|
|[NTyx5yZkxCxGwJBJ.htm](extinction-curse-bestiary-items/NTyx5yZkxCxGwJBJ.htm)|Gambling Lore|Connaissance des jeux d'argent|officielle|
|[nWG8oxYnQkgZ85MI.htm](extinction-curse-bestiary-items/nWG8oxYnQkgZ85MI.htm)|Emotional Frenzy|Frénésie émotionnelle|officielle|
|[nxmZVcQPHk28sOnW.htm](extinction-curse-bestiary-items/nxmZVcQPHk28sOnW.htm)|Composite Shortbow|Arc court composite|officielle|
|[NXWA3YnWW3T52ycS.htm](extinction-curse-bestiary-items/NXWA3YnWW3T52ycS.htm)|At-Will Spells|Sorts à volonté|officielle|
|[nycgOFC3jC4CfnmG.htm](extinction-curse-bestiary-items/nycgOFC3jC4CfnmG.htm)|Feather Fall (At Will) (Self Only)|Feuille morte (À volonté) (soi uniquement)|officielle|
|[nyPjPPwEThfAJoow.htm](extinction-curse-bestiary-items/nyPjPPwEThfAJoow.htm)|At-Will Spells|Sorts à volonté|officielle|
|[nYZMYEzR32X1P92B.htm](extinction-curse-bestiary-items/nYZMYEzR32X1P92B.htm)|Composite Shortbow|+1,striking|Arc court composite de Frappe +1|libre|
|[NZ4q9cv0qu2zI3NC.htm](extinction-curse-bestiary-items/NZ4q9cv0qu2zI3NC.htm)|Dueling Riposte|Riposte en duel|officielle|
|[o1Dwspcc5Yryf7By.htm](extinction-curse-bestiary-items/o1Dwspcc5Yryf7By.htm)|Devour Soul|Engloutissement d'âmes|officielle|
|[o204NA9EFmplkmbF.htm](extinction-curse-bestiary-items/o204NA9EFmplkmbF.htm)|Motion Sense (60 feet)|Perception du mouvement (18 m)|officielle|
|[O2vFMtCMKiP3cEqw.htm](extinction-curse-bestiary-items/O2vFMtCMKiP3cEqw.htm)|Claw|Griffe|officielle|
|[O3GuY1OWhHhmq9FW.htm](extinction-curse-bestiary-items/O3GuY1OWhHhmq9FW.htm)|Suck Blood|Sucer le sang|officielle|
|[o59thQeI8g01OAvC.htm](extinction-curse-bestiary-items/o59thQeI8g01OAvC.htm)|Hungry Spear|Lance affamée|officielle|
|[O5By0tAUVvL66vcN.htm](extinction-curse-bestiary-items/O5By0tAUVvL66vcN.htm)|Evasion|Évasion|officielle|
|[o5oVycGSBlCtwNU9.htm](extinction-curse-bestiary-items/o5oVycGSBlCtwNU9.htm)|+2 Circumstance to Fortitude and Reflex vs. Shove or Trip|+2 de circonstances à Vigueur et Réflexes contre Pousser et Croc-en-jambe|officielle|
|[O6DDDWmQMfgrvOUU.htm](extinction-curse-bestiary-items/O6DDDWmQMfgrvOUU.htm)|Black Desert Lore|Connaissance du Désert noir|officielle|
|[O6ibunfTGnutfemd.htm](extinction-curse-bestiary-items/O6ibunfTGnutfemd.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[o7B3d9PhWctaj0P8.htm](extinction-curse-bestiary-items/o7B3d9PhWctaj0P8.htm)|Scimitar|+2,greaterStriking|Cimeterre de frappe supérieure +2|libre|
|[o7xu8uge6T0IT23h.htm](extinction-curse-bestiary-items/o7xu8uge6T0IT23h.htm)|Hypnotic Stench|Puanteur hypnotisante|officielle|
|[O8uuBNiuKfPW8eyF.htm](extinction-curse-bestiary-items/O8uuBNiuKfPW8eyF.htm)|Whip|Fouet|officielle|
|[oAogJcqTK6rECpGp.htm](extinction-curse-bestiary-items/oAogJcqTK6rECpGp.htm)|Dire Warning|Avertissement funeste|officielle|
|[oByC6W1h6qcY4eXg.htm](extinction-curse-bestiary-items/oByC6W1h6qcY4eXg.htm)|Deny Advantage|Refus d'avantage|officielle|
|[Od9QuV4TEmA9vlH5.htm](extinction-curse-bestiary-items/Od9QuV4TEmA9vlH5.htm)|Black Seed Cloud|Nuage de semence noire|officielle|
|[odG6wvcX6jYyHa7t.htm](extinction-curse-bestiary-items/odG6wvcX6jYyHa7t.htm)|Punishing Tail|Queue percutante|officielle|
|[oDVz4ipascXAYmri.htm](extinction-curse-bestiary-items/oDVz4ipascXAYmri.htm)|Telepathy 100 feet|Télépathie 30 m|officielle|
|[oe15nn62zh9y60Wn.htm](extinction-curse-bestiary-items/oe15nn62zh9y60Wn.htm)|Smoking Wound|Blessure fumante|officielle|
|[OE99dZLqJ6NlpBqw.htm](extinction-curse-bestiary-items/OE99dZLqJ6NlpBqw.htm)|Darkvision|Vision dans le noir|officielle|
|[oeenNOAvMw2BAQWV.htm](extinction-curse-bestiary-items/oeenNOAvMw2BAQWV.htm)|Low-Light Vision|Vision nocturne|officielle|
|[OeYHa0iu7IPcFWFH.htm](extinction-curse-bestiary-items/OeYHa0iu7IPcFWFH.htm)|Darkvision|Vision dans le noir|officielle|
|[OfDoBC89FG7NjSPu.htm](extinction-curse-bestiary-items/OfDoBC89FG7NjSPu.htm)|Poisoned Thorns|Épines empoisonnées|officielle|
|[oFEYbMQiLvzwPAtG.htm](extinction-curse-bestiary-items/oFEYbMQiLvzwPAtG.htm)|Religious Symbol of Bokrug (Platinum)|Symbole religieux en platine de Bokrug|officielle|
|[OFM7aCfqlQVWMVNo.htm](extinction-curse-bestiary-items/OFM7aCfqlQVWMVNo.htm)|Claw|Griffe|officielle|
|[OGUXwEjvyda2Jgfs.htm](extinction-curse-bestiary-items/OGUXwEjvyda2Jgfs.htm)|Low-Light Vision|Vision nocturne|officielle|
|[ohdTtVsfQICponGx.htm](extinction-curse-bestiary-items/ohdTtVsfQICponGx.htm)|Darkvision|Vision dans le noir|officielle|
|[OhLq6xthHdhj2mzP.htm](extinction-curse-bestiary-items/OhLq6xthHdhj2mzP.htm)|Bag of Faces|Sac de visages|officielle|
|[OjAuvyGPfAG9j4Dv.htm](extinction-curse-bestiary-items/OjAuvyGPfAG9j4Dv.htm)|Dimension Door (At Will)|Porte dimensionnelle (À volonté)|officielle|
|[oKMZi1P2n9vQ9SvK.htm](extinction-curse-bestiary-items/oKMZi1P2n9vQ9SvK.htm)|Psychic Sip|Dégustation psychique|officielle|
|[OkSGq1Wy2ceTrxFS.htm](extinction-curse-bestiary-items/OkSGq1Wy2ceTrxFS.htm)|Pseudopod|Pseudopode|officielle|
|[oKvWxPQ1VdadXlOL.htm](extinction-curse-bestiary-items/oKvWxPQ1VdadXlOL.htm)|Cabal|Cabale|officielle|
|[OLaJWLiujGgz8HCl.htm](extinction-curse-bestiary-items/OLaJWLiujGgz8HCl.htm)|Stealth|Discrétion|officielle|
|[oLLRMvLu25gNhBdu.htm](extinction-curse-bestiary-items/oLLRMvLu25gNhBdu.htm)|Dagger|Dague|officielle|
|[oLMdVdJKbDXXxlbp.htm](extinction-curse-bestiary-items/oLMdVdJKbDXXxlbp.htm)|Darkvision|Vision dans le noir|officielle|
|[OLmfUY8tRGHs6zpa.htm](extinction-curse-bestiary-items/OLmfUY8tRGHs6zpa.htm)|Divert Thoughts|Dévier les pensées|officielle|
|[oM6jqTPLCdRYP1WX.htm](extinction-curse-bestiary-items/oM6jqTPLCdRYP1WX.htm)|At-Will Spells|Sorts à volonté|officielle|
|[om85FhVgReT8plPu.htm](extinction-curse-bestiary-items/om85FhVgReT8plPu.htm)|Feral Directive|Ordre féroce|officielle|
|[omGN70P6v81nFafa.htm](extinction-curse-bestiary-items/omGN70P6v81nFafa.htm)|Emotional Focus|Focalisation émotionnelle|officielle|
|[ONJaqLmNDjXmPpES.htm](extinction-curse-bestiary-items/ONJaqLmNDjXmPpES.htm)|Sickle|Serpe|officielle|
|[OoEHiY3SG02Ny7kr.htm](extinction-curse-bestiary-items/OoEHiY3SG02Ny7kr.htm)|Antler|Bois|officielle|
|[OpIksuLcYybF2H6o.htm](extinction-curse-bestiary-items/OpIksuLcYybF2H6o.htm)|Putrid Blast|Éruption nauséabonde|officielle|
|[OpULqY48Og5zS0iV.htm](extinction-curse-bestiary-items/OpULqY48Og5zS0iV.htm)|Mounted Defense|Défense de cavalier|officielle|
|[Oq8qCN1Ot7PQUhWE.htm](extinction-curse-bestiary-items/Oq8qCN1Ot7PQUhWE.htm)|Paralyze (At Will)|Paralysie (À volonté)|officielle|
|[oQVUQYLZSbOzdbRx.htm](extinction-curse-bestiary-items/oQVUQYLZSbOzdbRx.htm)|Primal Prepared Spells|Sorts primordiaux préparés|libre|
|[OQX9XEhfUYIiPSNz.htm](extinction-curse-bestiary-items/OQX9XEhfUYIiPSNz.htm)|Hex of the Bloody Thief|Malédiction du voleur sanguinaire|officielle|
|[orAEjgUO2zLIc2Di.htm](extinction-curse-bestiary-items/orAEjgUO2zLIc2Di.htm)|+1 Resilient Leather Armor|Armure en cuir de résilience +1|officielle|
|[oRBm7hBPk5gfvefV.htm](extinction-curse-bestiary-items/oRBm7hBPk5gfvefV.htm)|Enhance Victuals (At Will)|Amélioration des victuailles (À volonté)|officielle|
|[ordT6deii6Tl80qq.htm](extinction-curse-bestiary-items/ordT6deii6Tl80qq.htm)|Darkvision|Vision dans le noir|officielle|
|[OrumAw48Pwq9fDTZ.htm](extinction-curse-bestiary-items/OrumAw48Pwq9fDTZ.htm)|Ankylostar|+2,greaterStriking|Ankylostern|officielle|
|[ORzFTDNAhTrNzkq6.htm](extinction-curse-bestiary-items/ORzFTDNAhTrNzkq6.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[OS3Ccca6DLGDr67x.htm](extinction-curse-bestiary-items/OS3Ccca6DLGDr67x.htm)|Greater Darkvision|Vision dans le noir supérieure|officielle|
|[oSaKLB4trA23IqwP.htm](extinction-curse-bestiary-items/oSaKLB4trA23IqwP.htm)|Shield Block|Blocage au bouclier|officielle|
|[oSoePQdNadEUnATM.htm](extinction-curse-bestiary-items/oSoePQdNadEUnATM.htm)|Faerie Fire (At Will)|Lueurs féeriques (À volonté)|officielle|
|[oTDnAl5JS81qlDYh.htm](extinction-curse-bestiary-items/oTDnAl5JS81qlDYh.htm)|Shortsword|Épée courte|officielle|
|[OthdN7VZZ2OhjTnY.htm](extinction-curse-bestiary-items/OthdN7VZZ2OhjTnY.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[OtJ0BMN8Z5o0eMdr.htm](extinction-curse-bestiary-items/OtJ0BMN8Z5o0eMdr.htm)|Rapier|Rapière|officielle|
|[oTqwsUwuBzfzn5t4.htm](extinction-curse-bestiary-items/oTqwsUwuBzfzn5t4.htm)|At-Will Spells|Sorts à volonté|libre|
|[Ou7j9iRSTfEmOPJq.htm](extinction-curse-bestiary-items/Ou7j9iRSTfEmOPJq.htm)|Golem Antimagic|Antimagie du golem|officielle|
|[OUAUqe08Aktnirvq.htm](extinction-curse-bestiary-items/OUAUqe08Aktnirvq.htm)|Deception|Duperie|officielle|
|[oUCSylGhNS9ioPWO.htm](extinction-curse-bestiary-items/oUCSylGhNS9ioPWO.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|libre|
|[OukSsGFoINYIbHD9.htm](extinction-curse-bestiary-items/OukSsGFoINYIbHD9.htm)|Trample|Piétinement|officielle|
|[ouztmCgjgV65kMJ4.htm](extinction-curse-bestiary-items/ouztmCgjgV65kMJ4.htm)|Antler Toss|Projection de bois|officielle|
|[oV6jmBptcFGgbNXA.htm](extinction-curse-bestiary-items/oV6jmBptcFGgbNXA.htm)|Reach Spell|Sort éloigné|officielle|
|[OwAhtu9K9pfUPHun.htm](extinction-curse-bestiary-items/OwAhtu9K9pfUPHun.htm)|Light Blindness|Aveuglé par la lumière|officielle|
|[OWuc4hzQaHKQ4cO0.htm](extinction-curse-bestiary-items/OWuc4hzQaHKQ4cO0.htm)|Constant Spells|Sorts constants|libre|
|[oxKLVBMpW1qrYgZH.htm](extinction-curse-bestiary-items/oxKLVBMpW1qrYgZH.htm)|Hallucinogenic Pollen|Pollen hallucinogène|officielle|
|[OYivzyUv9rlanxFD.htm](extinction-curse-bestiary-items/OYivzyUv9rlanxFD.htm)|Blade|Lame|officielle|
|[oZtIJY1LCleBd0XM.htm](extinction-curse-bestiary-items/oZtIJY1LCleBd0XM.htm)|Heavy Crossbow|Arbalète lourde|officielle|
|[oZzczpHFxrH5IUJq.htm](extinction-curse-bestiary-items/oZzczpHFxrH5IUJq.htm)|Roar|Rugissement|officielle|
|[P1prRlTlxSzsnhNi.htm](extinction-curse-bestiary-items/P1prRlTlxSzsnhNi.htm)|Stoneraiser Javelin|+2,striking,returning|Javeline dresse-pierre|officielle|
|[P55ThVkZipK0fIVo.htm](extinction-curse-bestiary-items/P55ThVkZipK0fIVo.htm)|Uncanny Divination|Divination troublante|officielle|
|[P6IbJGGscBwb0Kuy.htm](extinction-curse-bestiary-items/P6IbJGGscBwb0Kuy.htm)|Divine Prepared Spells|Sorts divins préparés|libre|
|[p7TxyyKhv9w7s5Xp.htm](extinction-curse-bestiary-items/p7TxyyKhv9w7s5Xp.htm)|Beguiling Gaze|Regard envoûtant|officielle|
|[P85ToDPnYlaZWUq4.htm](extinction-curse-bestiary-items/P85ToDPnYlaZWUq4.htm)|Darkvision|Vision dans le noir|officielle|
|[PA1pNOAVXRh7cuhC.htm](extinction-curse-bestiary-items/PA1pNOAVXRh7cuhC.htm)|Negative Healing|Guérison négative|officielle|
|[pAMOEkL6TdnNLhqQ.htm](extinction-curse-bestiary-items/pAMOEkL6TdnNLhqQ.htm)|Rend|Éventration|officielle|
|[pBAPuoNaSyHoRG4V.htm](extinction-curse-bestiary-items/pBAPuoNaSyHoRG4V.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[pCcRbDIkKmjmDyfT.htm](extinction-curse-bestiary-items/pCcRbDIkKmjmDyfT.htm)|Constant Spells|Sorts constants|officielle|
|[PCdyZXJEAyvmCnYy.htm](extinction-curse-bestiary-items/PCdyZXJEAyvmCnYy.htm)|Constant Spells|Sorts constants|officielle|
|[PED3IBsGf0atbCwI.htm](extinction-curse-bestiary-items/PED3IBsGf0atbCwI.htm)|Darkvision|Vision dans le noir|officielle|
|[peIPD9LBmPiab3Co.htm](extinction-curse-bestiary-items/peIPD9LBmPiab3Co.htm)|Zevgavizeb Lore|Connaissance de Zevgavizeb|officielle|
|[PGIZYkUT2mwkytuL.htm](extinction-curse-bestiary-items/PGIZYkUT2mwkytuL.htm)|Scent (Precise) 100 feet|Odorat (Précis) 30 m|officielle|
|[pkJg8aphLGRyaSdk.htm](extinction-curse-bestiary-items/pkJg8aphLGRyaSdk.htm)|Rat Hood|Capuche de rat|officielle|
|[PmoZHqEM39IJVPZt.htm](extinction-curse-bestiary-items/PmoZHqEM39IJVPZt.htm)|Sneak Attack|Attaque sournoise|officielle|
|[PMvKwqjv8zSTg6HR.htm](extinction-curse-bestiary-items/PMvKwqjv8zSTg6HR.htm)|Burrowing Agony|Douleur agonisante|officielle|
|[pn15gaicH5zNmSAV.htm](extinction-curse-bestiary-items/pn15gaicH5zNmSAV.htm)|Maul|Maillet|officielle|
|[PnbdGiGnDuyflxJa.htm](extinction-curse-bestiary-items/PnbdGiGnDuyflxJa.htm)|Crafting|Artisanat|officielle|
|[pnI5OGCrwAz8IPJ1.htm](extinction-curse-bestiary-items/pnI5OGCrwAz8IPJ1.htm)|Adhesive Body|Corps adhésif|officielle|
|[PO0gF2xQ8vL9kRKY.htm](extinction-curse-bestiary-items/PO0gF2xQ8vL9kRKY.htm)|Firedamp Winds|Vents inflammables|officielle|
|[po14dNEO7G6AIdqA.htm](extinction-curse-bestiary-items/po14dNEO7G6AIdqA.htm)|Darkvision|Vision dans le noir|officielle|
|[PoE00pbKu92pJcmI.htm](extinction-curse-bestiary-items/PoE00pbKu92pJcmI.htm)|At-Will Spells|Sorts à volonté|officielle|
|[Pp1p9fNkgDG02dcL.htm](extinction-curse-bestiary-items/Pp1p9fNkgDG02dcL.htm)|Inhabit Object|Occuper un objet|officielle|
|[PP6cWEeflOzf6dln.htm](extinction-curse-bestiary-items/PP6cWEeflOzf6dln.htm)|Counteflora Toxin|Toxine contreflore|officielle|
|[PP7sGT67f4IAwfaU.htm](extinction-curse-bestiary-items/PP7sGT67f4IAwfaU.htm)|Faerie Fire (At Will)|Lueurs féeriques (À volonté)|officielle|
|[pPAfuiRx3hQmyGuJ.htm](extinction-curse-bestiary-items/pPAfuiRx3hQmyGuJ.htm)|Boneshaking Roar|Rugissement foudroyant|officielle|
|[pPirfuGfRrPRUV8Y.htm](extinction-curse-bestiary-items/pPirfuGfRrPRUV8Y.htm)|Nimble Dodge|Esquive agile|officielle|
|[PpLXQRLE9C2ygCvU.htm](extinction-curse-bestiary-items/PpLXQRLE9C2ygCvU.htm)|Tail|Queue|officielle|
|[PQ4Thk0ntFSWU7Gz.htm](extinction-curse-bestiary-items/PQ4Thk0ntFSWU7Gz.htm)|Cabal Communion|Communication de la cabale|officielle|
|[PQCJPgjYoVwBLXyY.htm](extinction-curse-bestiary-items/PQCJPgjYoVwBLXyY.htm)|Harrowing Misfortune|Infortune du Tourment|officielle|
|[PQprcJ7E0RDiHXfR.htm](extinction-curse-bestiary-items/PQprcJ7E0RDiHXfR.htm)|Grab|Empoignade|officielle|
|[pquHeaKZPdfFQK6c.htm](extinction-curse-bestiary-items/pquHeaKZPdfFQK6c.htm)|Dagger|Dague|officielle|
|[PqvaZ3DvfcX3DqQD.htm](extinction-curse-bestiary-items/PqvaZ3DvfcX3DqQD.htm)|Blink (At Will)|Clignotement (À volonté)|officielle|
|[PqYLOfqbS9HKpXwR.htm](extinction-curse-bestiary-items/PqYLOfqbS9HKpXwR.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[pr00tZKsLorUCDcD.htm](extinction-curse-bestiary-items/pr00tZKsLorUCDcD.htm)|Hunt Prey|Chasser une proie|officielle|
|[PTFpKWM9gdLACL3N.htm](extinction-curse-bestiary-items/PTFpKWM9gdLACL3N.htm)|Coven|Cercle|officielle|
|[ptMSulmBrypLe9hN.htm](extinction-curse-bestiary-items/ptMSulmBrypLe9hN.htm)|Stench|Puanteur|officielle|
|[pTrcnnDT8kWdlqJU.htm](extinction-curse-bestiary-items/pTrcnnDT8kWdlqJU.htm)|Fast Swallow|Gober rapidement|officielle|
|[pulNjOGaDMXFhrC4.htm](extinction-curse-bestiary-items/pulNjOGaDMXFhrC4.htm)|Lifesense (Imprecise) 60 feet|Perception de la vie 18 m (imprécis)|officielle|
|[PVPqGw7nPCIvqbHf.htm](extinction-curse-bestiary-items/PVPqGw7nPCIvqbHf.htm)|Captivating Adoration (At Will) (Emotional Focus)|Adoration captivante (à volonté, Focalisation émotionnelle)|officielle|
|[PVSV062yywdBQCMh.htm](extinction-curse-bestiary-items/PVSV062yywdBQCMh.htm)|Corrosive Kiss|Baiser corrosif|officielle|
|[pvYJQt0PVzYMhbJY.htm](extinction-curse-bestiary-items/pvYJQt0PVzYMhbJY.htm)|Jaws|Mâchoires|officielle|
|[pwOWBEPVPsrfwS5D.htm](extinction-curse-bestiary-items/pwOWBEPVPsrfwS5D.htm)|Telekinetic Maneuver (At Will)|Manoeuvre télékinésique (À volonté)|officielle|
|[pwQ9ySONmvrBDAn7.htm](extinction-curse-bestiary-items/pwQ9ySONmvrBDAn7.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|officielle|
|[pxM83ECTkuL1t1yb.htm](extinction-curse-bestiary-items/pxM83ECTkuL1t1yb.htm)|Possessed|Possédé|officielle|
|[q0Go6gYxsqhxoIiT.htm](extinction-curse-bestiary-items/q0Go6gYxsqhxoIiT.htm)|Mountain Stronghold|Bastion de la montagne|officielle|
|[q2hMXszV7WfZt7lw.htm](extinction-curse-bestiary-items/q2hMXszV7WfZt7lw.htm)|Charm (At-Will)|Charme (À volonté)|officielle|
|[q2ksBBTuj9IItWjI.htm](extinction-curse-bestiary-items/q2ksBBTuj9IItWjI.htm)|Invisibility (At Will)|Invisibilité (À volonté)|officielle|
|[q34JoxlJlf1D0nZT.htm](extinction-curse-bestiary-items/q34JoxlJlf1D0nZT.htm)|Claw|Griffe|officielle|
|[q4nI7tSE0zcxhH3I.htm](extinction-curse-bestiary-items/q4nI7tSE0zcxhH3I.htm)|Pierced Tendon|Tendon perforé|officielle|
|[q4Nwd5vpZrD2cK4B.htm](extinction-curse-bestiary-items/q4Nwd5vpZrD2cK4B.htm)|Haymaker|Coup de poing fracassant|officielle|
|[Q7lByDcjQdmv3be5.htm](extinction-curse-bestiary-items/Q7lByDcjQdmv3be5.htm)|Acid Flask|Fiole d'acide|officielle|
|[qAcFJ6ENZ6hIxnoG.htm](extinction-curse-bestiary-items/qAcFJ6ENZ6hIxnoG.htm)|Show-Off|M’as-tu vu|officielle|
|[qCyjY8GOlu7YozEX.htm](extinction-curse-bestiary-items/qCyjY8GOlu7YozEX.htm)|Web|Toile|officielle|
|[qddyx06c0lhO2bqA.htm](extinction-curse-bestiary-items/qddyx06c0lhO2bqA.htm)|Enlarge (Self Only)|Agrandissement (soi uniquement)|officielle|
|[QDO52xyGxgu4Sa4l.htm](extinction-curse-bestiary-items/QDO52xyGxgu4Sa4l.htm)|Negative Healing|Guérison négative|officielle|
|[qEzLJPCCAZLBYVV4.htm](extinction-curse-bestiary-items/qEzLJPCCAZLBYVV4.htm)|Surprise Attack|Attaque surprise|officielle|
|[QF1sbXL0ly14wIOr.htm](extinction-curse-bestiary-items/QF1sbXL0ly14wIOr.htm)|Invisibility (At-Will) (Self-Only)|Invisibilité (À volonté) (soi uniquement)|officielle|
|[QFL7zXFbbEXPg5V9.htm](extinction-curse-bestiary-items/QFL7zXFbbEXPg5V9.htm)|Claw|Griffe|officielle|
|[Qfpb2l5lZtzBzwYX.htm](extinction-curse-bestiary-items/Qfpb2l5lZtzBzwYX.htm)|Burning Hands (At Will)|Mains brûlantes (À volonté)|officielle|
|[qFVgtcLTXV4LIYQu.htm](extinction-curse-bestiary-items/qFVgtcLTXV4LIYQu.htm)|Glowing Bones|Os lumineux|officielle|
|[qgRGLReGps2La4FL.htm](extinction-curse-bestiary-items/qgRGLReGps2La4FL.htm)|Shraen Lore|Connaissance de Shraen|officielle|
|[QhKOuFQANuodhk8x.htm](extinction-curse-bestiary-items/QhKOuFQANuodhk8x.htm)|Mountain Stance|Posture de la montagne|officielle|
|[QKMTY0vHcMBDExoq.htm](extinction-curse-bestiary-items/QKMTY0vHcMBDExoq.htm)|Lion Tamer's Outfit|Costume de dompteuse de lion|officielle|
|[QkXcthXHJxRMKAFf.htm](extinction-curse-bestiary-items/QkXcthXHJxRMKAFf.htm)|Darkvision|Vision dans le noir|officielle|
|[qLAXvZ1JTs2cxRUR.htm](extinction-curse-bestiary-items/qLAXvZ1JTs2cxRUR.htm)|Expanded Splash|Éclaboussure élargie|officielle|
|[QlMKYLdaF5xd0vap.htm](extinction-curse-bestiary-items/QlMKYLdaF5xd0vap.htm)|Divine Restoration|Restauration divine|officielle|
|[QNih5VevNZaGEYIs.htm](extinction-curse-bestiary-items/QNih5VevNZaGEYIs.htm)|Hand Crossbow|Arbalète de poing|officielle|
|[QnkaPblW1eS7DJQR.htm](extinction-curse-bestiary-items/QnkaPblW1eS7DJQR.htm)|Spear|Lance|officielle|
|[qnz5lB0E15Y5lYDX.htm](extinction-curse-bestiary-items/qnz5lB0E15Y5lYDX.htm)|Terror Master|Maître de la terreur|officielle|
|[qPFlP0EUcnxETgQw.htm](extinction-curse-bestiary-items/qPFlP0EUcnxETgQw.htm)|+2 Greater Resilient Elven Chain (Standard-Grade)|Maille elfique de qualité standard et de résilience supérieure +2|officielle|
|[qpmRSL3kdhharvvt.htm](extinction-curse-bestiary-items/qpmRSL3kdhharvvt.htm)|Darkvision|Vision dans le noir|officielle|
|[qpXDUheFeUsWxKho.htm](extinction-curse-bestiary-items/qpXDUheFeUsWxKho.htm)|Darkvision|Vision dans le noir|officielle|
|[QqLiKxNQA0ywz1hJ.htm](extinction-curse-bestiary-items/QqLiKxNQA0ywz1hJ.htm)|Darkvision|Vision dans le noir|officielle|
|[qQOkGagQPycQw5YC.htm](extinction-curse-bestiary-items/qQOkGagQPycQw5YC.htm)|+1 Status to All Saves vs. Magical Effects and Positive Effects|bonus de statut de +1 aux JdS contre les effets magiques et positifs|officielle|
|[QR5bODSKMiojeESn.htm](extinction-curse-bestiary-items/QR5bODSKMiojeESn.htm)|Orc Ferocity|Férocité orque|officielle|
|[Qrev8Nd4NZoiDnJ7.htm](extinction-curse-bestiary-items/Qrev8Nd4NZoiDnJ7.htm)|Ghostly Hand|Main spectrale|officielle|
|[qRTwH6KdmvOMv6pr.htm](extinction-curse-bestiary-items/qRTwH6KdmvOMv6pr.htm)|At-Will Spells|Sorts à volonté|libre|
|[qs4zOgvQbmRifUez.htm](extinction-curse-bestiary-items/qs4zOgvQbmRifUez.htm)|Raving Diatribe|Diatribe enragée|officielle|
|[qsR9Zyfw0HbieQM9.htm](extinction-curse-bestiary-items/qsR9Zyfw0HbieQM9.htm)|Earthen Torrent|Torrent de terre|officielle|
|[qsz7qYjE6S3Fc1ut.htm](extinction-curse-bestiary-items/qsz7qYjE6S3Fc1ut.htm)|Bowling Pin|Quille|officielle|
|[QT4UShVoztc5IRSM.htm](extinction-curse-bestiary-items/QT4UShVoztc5IRSM.htm)|Eyes of the Enthralled|Les Yeux de l'asservi|officielle|
|[QT8BrycRuuUhs2V7.htm](extinction-curse-bestiary-items/QT8BrycRuuUhs2V7.htm)|Fetid Winds|Vents fétides|officielle|
|[QwnhlBEgmmLWoeSk.htm](extinction-curse-bestiary-items/QwnhlBEgmmLWoeSk.htm)|Bind Soul (At Will) (From Heartstone)|Âme prisonnière (à volonté, cardioline)|officielle|
|[QXq9Nn7zGrJqJnuV.htm](extinction-curse-bestiary-items/QXq9Nn7zGrJqJnuV.htm)|Experienced Ambusher|Expert en embuscade|officielle|
|[qYCOhM3CGVIsOcqt.htm](extinction-curse-bestiary-items/qYCOhM3CGVIsOcqt.htm)|Fast Healing 10|Guérison accélérée 10|officielle|
|[QyNDhFzjNVZdcfcL.htm](extinction-curse-bestiary-items/QyNDhFzjNVZdcfcL.htm)|Handwraps of Mighty Blows|+3,majorStriking|Bandelettes de coups puissants de frappe majeure +3|officielle|
|[QYVQuOt7vXM39lkd.htm](extinction-curse-bestiary-items/QYVQuOt7vXM39lkd.htm)|Harrow Card|Carte du jeu du Tourment|officielle|
|[qZ6MdZpGfCcoPSt7.htm](extinction-curse-bestiary-items/qZ6MdZpGfCcoPSt7.htm)|Longbow|Arc long|officielle|
|[r0SfPNZ2ODvH5Jn3.htm](extinction-curse-bestiary-items/r0SfPNZ2ODvH5Jn3.htm)|Tentacle|Tentacule|officielle|
|[R1VM8NI2d9gcyPk0.htm](extinction-curse-bestiary-items/R1VM8NI2d9gcyPk0.htm)|Telekinetic Maneuver (At Will)|Manoeuvre télékinésique (À volonté)|officielle|
|[r3gz4OYV1y7jbwTj.htm](extinction-curse-bestiary-items/r3gz4OYV1y7jbwTj.htm)|Constant Spells|Sorts constants|officielle|
|[r4uxKTSX8eoYMesb.htm](extinction-curse-bestiary-items/r4uxKTSX8eoYMesb.htm)|Dagger|Dague|officielle|
|[r8JQHbOW0puwaFWz.htm](extinction-curse-bestiary-items/r8JQHbOW0puwaFWz.htm)|Stool Bash|Coup de tabouret|officielle|
|[R8Kt7N2ubdHBIdow.htm](extinction-curse-bestiary-items/R8Kt7N2ubdHBIdow.htm)|Disgorge Bile|Vomir de la bile|officielle|
|[R9WycjP9krAdeKQw.htm](extinction-curse-bestiary-items/R9WycjP9krAdeKQw.htm)|Cleric Domain Spell|Sort de domaine de prêtre|libre|
|[R9xjpMLRExdAfpdw.htm](extinction-curse-bestiary-items/R9xjpMLRExdAfpdw.htm)|Duck Away|Changer de place|officielle|
|[Ra5CZtxKiCGaTCKR.htm](extinction-curse-bestiary-items/Ra5CZtxKiCGaTCKR.htm)|At-Will Spells|Sorts à volonté|officielle|
|[RaAxUg8NsJWovO3S.htm](extinction-curse-bestiary-items/RaAxUg8NsJWovO3S.htm)|Drain Life|Drain de vie|officielle|
|[RAzDul5aLDAcZI57.htm](extinction-curse-bestiary-items/RAzDul5aLDAcZI57.htm)|Vulnerable to Shatter|Vulnérabilité à Fracassement|officielle|
|[RBTICSCn9OcPSsfb.htm](extinction-curse-bestiary-items/RBTICSCn9OcPSsfb.htm)|Darkvision|Vision dans le noir|officielle|
|[rCVpQ7gsbhPzhRsi.htm](extinction-curse-bestiary-items/rCVpQ7gsbhPzhRsi.htm)|Thrown Bottle|Jet de bouteille|officielle|
|[rdFZCjqKcogZyMg7.htm](extinction-curse-bestiary-items/rdFZCjqKcogZyMg7.htm)|Dreadful Spite|Malveillance redoutable|officielle|
|[RdSrem9D6XcqzAuu.htm](extinction-curse-bestiary-items/RdSrem9D6XcqzAuu.htm)|Boar Charge|Charge du sanglier|officielle|
|[RDYHiUOFIIaDKt8y.htm](extinction-curse-bestiary-items/RDYHiUOFIIaDKt8y.htm)|Infused Reagents|Réactifs imprégnés|officielle|
|[RFVHeM61eFQI1Y9j.htm](extinction-curse-bestiary-items/RFVHeM61eFQI1Y9j.htm)|Halberd|+1,striking|Hallebarde de frappe +1|libre|
|[rH50o3UwWDkWRLLK.htm](extinction-curse-bestiary-items/rH50o3UwWDkWRLLK.htm)|Wight Spawn|Rejeton de nécrophage|officielle|
|[rhC1IVvlV0IPjPcB.htm](extinction-curse-bestiary-items/rhC1IVvlV0IPjPcB.htm)|Rend|Éventration|officielle|
|[rHEKhmV0XO0CxR0y.htm](extinction-curse-bestiary-items/rHEKhmV0XO0CxR0y.htm)|Maul|Maillet|officielle|
|[rHEZ6pZ9PZWW3kwC.htm](extinction-curse-bestiary-items/rHEZ6pZ9PZWW3kwC.htm)|Claw|Griffe|officielle|
|[RhulgNsATpCM6nip.htm](extinction-curse-bestiary-items/RhulgNsATpCM6nip.htm)|Claw|Griffe|officielle|
|[RHWWGr2v6M4d2qBz.htm](extinction-curse-bestiary-items/RHWWGr2v6M4d2qBz.htm)|Darkvision|Vision dans le noir|officielle|
|[RJ26mcn4UnryXIsN.htm](extinction-curse-bestiary-items/RJ26mcn4UnryXIsN.htm)|Haunting Wail|Gémissement lancinant|officielle|
|[RjbNYYv9HvyrTFlA.htm](extinction-curse-bestiary-items/RjbNYYv9HvyrTFlA.htm)|Dream Message (At Will)|Message onirique (À volonté)|officielle|
|[rjSNiLNccg17t8pc.htm](extinction-curse-bestiary-items/rjSNiLNccg17t8pc.htm)|Key to Strongbox in A10|Clé du coffre-fort de la zone A10|officielle|
|[rKjDLURibGU1uFiB.htm](extinction-curse-bestiary-items/rKjDLURibGU1uFiB.htm)|Jaws|Mâchoires|officielle|
|[RmnzUOOeZZH9SVi3.htm](extinction-curse-bestiary-items/RmnzUOOeZZH9SVi3.htm)|Alchemical Injection|Injection alchimique|officielle|
|[RMwLgobPTr1kBImK.htm](extinction-curse-bestiary-items/RMwLgobPTr1kBImK.htm)|Resonating Note|Note dissonante|officielle|
|[rNe7hr19TVEKyp1e.htm](extinction-curse-bestiary-items/rNe7hr19TVEKyp1e.htm)|Starvation Aura|Aura de famine|officielle|
|[ROfK1SKRZK5bHSNe.htm](extinction-curse-bestiary-items/ROfK1SKRZK5bHSNe.htm)|Religious Symbol of Gozreh (Wooden, Defaced with Demonic Runes)|Symbole religieux en bois gravé de runes démoniaques de Gozreh|officielle|
|[rPxLM6aWMkXPfN46.htm](extinction-curse-bestiary-items/rPxLM6aWMkXPfN46.htm)|At-Will Spells|Sorts à volonté|officielle|
|[RqGgTNak1Cjjsdm0.htm](extinction-curse-bestiary-items/RqGgTNak1Cjjsdm0.htm)|Greater Frost Main-Gauche|Main-gauche de froid supérieur|officielle|
|[RqsjlbJfjDXjttuD.htm](extinction-curse-bestiary-items/RqsjlbJfjDXjttuD.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[RSvyyBPox2VzEhkh.htm](extinction-curse-bestiary-items/RSvyyBPox2VzEhkh.htm)|Ray of Enfeeblement (At Will)|Rayon affaiblissant (À volonté)|officielle|
|[RtG3Dfe9kEKJnPYX.htm](extinction-curse-bestiary-items/RtG3Dfe9kEKJnPYX.htm)|Aura of Annihilation|Aura d'annihilation|officielle|
|[RThixt2DZUbyrFWM.htm](extinction-curse-bestiary-items/RThixt2DZUbyrFWM.htm)|Pincer|Pince|officielle|
|[RUD5WUI6tdR6jtHc.htm](extinction-curse-bestiary-items/RUD5WUI6tdR6jtHc.htm)|Mobility|Mobilité|officielle|
|[rulKmak2Pk0vfAnF.htm](extinction-curse-bestiary-items/rulKmak2Pk0vfAnF.htm)|Greater Darkvision|Vision dans le noir supérieure|officielle|
|[rvBCgpjjGfH5PcRh.htm](extinction-curse-bestiary-items/rvBCgpjjGfH5PcRh.htm)|Negative Healing|Guérison négative|officielle|
|[RvNmrpkeCr0FxRBy.htm](extinction-curse-bestiary-items/RvNmrpkeCr0FxRBy.htm)|Gate Collapse|Effondrement de la porte|officielle|
|[RvPP5jeliNA9rwoy.htm](extinction-curse-bestiary-items/RvPP5jeliNA9rwoy.htm)|Steady Spellcasting|Incantation fiable|officielle|
|[RVQyJE6Qk4ngMwfH.htm](extinction-curse-bestiary-items/RVQyJE6Qk4ngMwfH.htm)|Primal Spontaneous Spells|Sorts primordiaux spontanés|libre|
|[rvtMyyj7UBsQraKA.htm](extinction-curse-bestiary-items/rvtMyyj7UBsQraKA.htm)|Negative Healing|Guérison négative|officielle|
|[rVwshWQjLtgO0x1Z.htm](extinction-curse-bestiary-items/rVwshWQjLtgO0x1Z.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[rWtKnUa62qWW2Knk.htm](extinction-curse-bestiary-items/rWtKnUa62qWW2Knk.htm)|Pass Without Trace (Constant)|Passage sans trace (constant)|officielle|
|[ryVPqtMIfWvzGo2o.htm](extinction-curse-bestiary-items/ryVPqtMIfWvzGo2o.htm)|Darkvision|Vision dans le noir|officielle|
|[s1YAaHM6yGhPRyhY.htm](extinction-curse-bestiary-items/s1YAaHM6yGhPRyhY.htm)|Darkvision|Vision dans le noir|officielle|
|[s2ZCoyccvRlVnT8V.htm](extinction-curse-bestiary-items/s2ZCoyccvRlVnT8V.htm)|Darkvision|Vision dans le noir|libre|
|[S3xTVbBZjdKXi28O.htm](extinction-curse-bestiary-items/S3xTVbBZjdKXi28O.htm)|Heavy Crossbow|+2,striking|Arbalète lourde de frappe +2|libre|
|[S9kIEucfakAniYkE.htm](extinction-curse-bestiary-items/S9kIEucfakAniYkE.htm)|Claw|Griffe|officielle|
|[S9rsAqZ6i0J68L8D.htm](extinction-curse-bestiary-items/S9rsAqZ6i0J68L8D.htm)|Alchemical Formulas|Formules alchimiques|officielle|
|[sa6rqRzXU6Y37Y0P.htm](extinction-curse-bestiary-items/sa6rqRzXU6Y37Y0P.htm)|Divine Prepared Spells|Sorts divins préparés|libre|
|[sAcBKkpmfacpuDgO.htm](extinction-curse-bestiary-items/sAcBKkpmfacpuDgO.htm)|Necrotic Decay|Décomposition nécrotique|officielle|
|[saDsnNdD6KrLH2zA.htm](extinction-curse-bestiary-items/saDsnNdD6KrLH2zA.htm)|Captive Rake|Balayage du captif|officielle|
|[saeYOvVw7Zbc6J1y.htm](extinction-curse-bestiary-items/saeYOvVw7Zbc6J1y.htm)|At-Will Spells|Sorts à volonté|officielle|
|[SAILABWdBSyuuEzf.htm](extinction-curse-bestiary-items/SAILABWdBSyuuEzf.htm)|Fist|Poing|officielle|
|[SaNPZ048iYwMkyBR.htm](extinction-curse-bestiary-items/SaNPZ048iYwMkyBR.htm)|Remove Face|Ablation de visage|officielle|
|[SBbVvzgTVrVDQLcd.htm](extinction-curse-bestiary-items/SBbVvzgTVrVDQLcd.htm)|Religious Symbol of Bokrug|Symbole religieux de Bokrug|officielle|
|[SBj6G1VOAt4Bk46Z.htm](extinction-curse-bestiary-items/SBj6G1VOAt4Bk46Z.htm)|Darkvision|Vision dans le noir|officielle|
|[SbnljAkVkA39hCW0.htm](extinction-curse-bestiary-items/SbnljAkVkA39hCW0.htm)|Javelin|Javeline|officielle|
|[sbOuf0oUW8o7LInv.htm](extinction-curse-bestiary-items/sbOuf0oUW8o7LInv.htm)|Devastating Strikes|Frappes dévastatrices|officielle|
|[SDcOSLxZFCLOVEvk.htm](extinction-curse-bestiary-items/SDcOSLxZFCLOVEvk.htm)|Etheric Tug|Attraction éthérique|officielle|
|[sdS6ZYyIEWr1joRs.htm](extinction-curse-bestiary-items/sdS6ZYyIEWr1joRs.htm)|Intimidating Strike|Frappe démoralisante|officielle|
|[sDwP6nXN8KQIiRuR.htm](extinction-curse-bestiary-items/sDwP6nXN8KQIiRuR.htm)|Spike Jab|Frappe coupante|officielle|
|[SeJz5BH1ioJC1OVG.htm](extinction-curse-bestiary-items/SeJz5BH1ioJC1OVG.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|libre|
|[SFb9j3FbIppSf34W.htm](extinction-curse-bestiary-items/SFb9j3FbIppSf34W.htm)|Jaws|Mâchoires|officielle|
|[SFKdrfAhk0O6ItXS.htm](extinction-curse-bestiary-items/SFKdrfAhk0O6ItXS.htm)|Talon|Serre|officielle|
|[sgfHGUowMefpHneQ.htm](extinction-curse-bestiary-items/sgfHGUowMefpHneQ.htm)|Xulgath Lore|Connaissance des Xulgath|officielle|
|[sgoOmgJ1Ff1lrKfa.htm](extinction-curse-bestiary-items/sgoOmgJ1Ff1lrKfa.htm)|Constant Spells|Sorts constants|officielle|
|[SgXreqFoQH1zwIKM.htm](extinction-curse-bestiary-items/SgXreqFoQH1zwIKM.htm)|Grab|Empoignade|officielle|
|[sHOiqltQCZRdyxTa.htm](extinction-curse-bestiary-items/sHOiqltQCZRdyxTa.htm)|Claw|Griffe|officielle|
|[SHp8QZBD4sEwfuHc.htm](extinction-curse-bestiary-items/SHp8QZBD4sEwfuHc.htm)|Psychogenic Secretions|Sécrétions psychogènes|officielle|
|[ShRk5Gxx1Zm4oN09.htm](extinction-curse-bestiary-items/ShRk5Gxx1Zm4oN09.htm)|Grab|Empoignade|officielle|
|[siGtm9FgzQmusNTV.htm](extinction-curse-bestiary-items/siGtm9FgzQmusNTV.htm)|Rapier|+1|Rapière +1|libre|
|[SIOm7a8pyY2r7KU4.htm](extinction-curse-bestiary-items/SIOm7a8pyY2r7KU4.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[SizhdwVCwNc2BO62.htm](extinction-curse-bestiary-items/SizhdwVCwNc2BO62.htm)|Claw|Griffe|officielle|
|[SjV0H9VKBoqzK0Bc.htm](extinction-curse-bestiary-items/SjV0H9VKBoqzK0Bc.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[skC80CG18plDdch1.htm](extinction-curse-bestiary-items/skC80CG18plDdch1.htm)|Vein|Veine|officielle|
|[sKd0ZJXJ5gco5WqC.htm](extinction-curse-bestiary-items/sKd0ZJXJ5gco5WqC.htm)|Underworld Lore|Connaissance de la pègre|officielle|
|[SLW8aLZrbrMqAniN.htm](extinction-curse-bestiary-items/SLW8aLZrbrMqAniN.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[sLxIE1kiKD1TAX9B.htm](extinction-curse-bestiary-items/sLxIE1kiKD1TAX9B.htm)|Telekinetic Maneuver (At Will)|Manoeuvre télékinésique (À volonté)|officielle|
|[sM4ejYbqYSJxrVSd.htm](extinction-curse-bestiary-items/sM4ejYbqYSJxrVSd.htm)|Inexorable March|Marche inexorable|officielle|
|[sMH395MKnCnUaAGg.htm](extinction-curse-bestiary-items/sMH395MKnCnUaAGg.htm)|Fortune's Favor|Faveur de la Fortune|officielle|
|[sn7vd9r7ywSeeqkm.htm](extinction-curse-bestiary-items/sn7vd9r7ywSeeqkm.htm)|Spirit Blast (From Heartstone)|Coup spirituel (cardioline)|officielle|
|[SnC7t8ZXBipTfwbi.htm](extinction-curse-bestiary-items/SnC7t8ZXBipTfwbi.htm)|War Flail|+2,greaterStriking,greaterFlaming|Fléau de guerre enflammé supérieur de frappe supérieure +2|libre|
|[SNhhLpkB6xBZW7tt.htm](extinction-curse-bestiary-items/SNhhLpkB6xBZW7tt.htm)|Low-Light Vision|Vision nocturne|officielle|
|[sNTGYTNdoPw797fO.htm](extinction-curse-bestiary-items/sNTGYTNdoPw797fO.htm)|Claw|Griffe|officielle|
|[SOJYFj8N8muE40NO.htm](extinction-curse-bestiary-items/SOJYFj8N8muE40NO.htm)|Ferocity|Férocité|officielle|
|[SOZB5DP0hEjlVrnz.htm](extinction-curse-bestiary-items/SOZB5DP0hEjlVrnz.htm)|Dagger|Dague|officielle|
|[sPi3ZAYG2vpg9HAv.htm](extinction-curse-bestiary-items/sPi3ZAYG2vpg9HAv.htm)|Naginata|+2,greaterStriking|Naginata de frappe supérieure +2|libre|
|[sPp6K8MLjVeA5Dok.htm](extinction-curse-bestiary-items/sPp6K8MLjVeA5Dok.htm)|Teleport (At Will, Self Only)|Téléportatin (À volonté) (soi uniquement)|officielle|
|[srgnKyGLbnQtqfOC.htm](extinction-curse-bestiary-items/srgnKyGLbnQtqfOC.htm)|Low-Light Vision|Vision nocturne|officielle|
|[ssPBbznwUNXKYBrK.htm](extinction-curse-bestiary-items/ssPBbznwUNXKYBrK.htm)|Warhammer|Marteau de guerre|officielle|
|[st4i4dmwri16Jaw4.htm](extinction-curse-bestiary-items/st4i4dmwri16Jaw4.htm)|Claw|Griffe|officielle|
|[STPKBwIDUxnzyqnQ.htm](extinction-curse-bestiary-items/STPKBwIDUxnzyqnQ.htm)|Claw|Griffe|officielle|
|[Stv3Oe7QU577Gke1.htm](extinction-curse-bestiary-items/Stv3Oe7QU577Gke1.htm)|Wheel Spin|Roue de la chance|officielle|
|[suHARe69G02OdMHO.htm](extinction-curse-bestiary-items/suHARe69G02OdMHO.htm)|At-Will Spells|Sorts à volonté|officielle|
|[SWKWArGZdBHMKGGt.htm](extinction-curse-bestiary-items/SWKWArGZdBHMKGGt.htm)|Protect|Protéger|officielle|
|[SWl7AjDMWH11G4ER.htm](extinction-curse-bestiary-items/SWl7AjDMWH11G4ER.htm)|At-Will Spells|Sorts à volonté|officielle|
|[swSzLoXHSeUzg4jT.htm](extinction-curse-bestiary-items/swSzLoXHSeUzg4jT.htm)|Dagger|Dague|officielle|
|[SX1FS88WJhELdBeJ.htm](extinction-curse-bestiary-items/SX1FS88WJhELdBeJ.htm)|Jaws|Mâchoires|officielle|
|[sxbPWmr9UrqQlMcl.htm](extinction-curse-bestiary-items/sxbPWmr9UrqQlMcl.htm)|Jaws|Mâchoires|officielle|
|[SZ8U6jGsKMqKBXxd.htm](extinction-curse-bestiary-items/SZ8U6jGsKMqKBXxd.htm)|Tighten Bracts|Bractées rétractées|officielle|
|[SZJLgj6RbPu5Q3a7.htm](extinction-curse-bestiary-items/SZJLgj6RbPu5Q3a7.htm)|Alchemist's Fire (Greater) (Infused)|Feu grégeois supérieur [Imprégné]|officielle|
|[T1x41KttaKGcSc4U.htm](extinction-curse-bestiary-items/T1x41KttaKGcSc4U.htm)|Vines|Lianes|officielle|
|[T2LmG0xgs6TOZdMJ.htm](extinction-curse-bestiary-items/T2LmG0xgs6TOZdMJ.htm)|Divine Prepared Spells|Sorts divins préparés|libre|
|[t3bOnN3uPrJ4Ornk.htm](extinction-curse-bestiary-items/t3bOnN3uPrJ4Ornk.htm)|Stunning Tail|Queue étourdissante|officielle|
|[t4LSexvOzvp0ZgSM.htm](extinction-curse-bestiary-items/t4LSexvOzvp0ZgSM.htm)|Darkvision|Vision dans le noir|officielle|
|[t5VxrfWqKM7zBVtb.htm](extinction-curse-bestiary-items/t5VxrfWqKM7zBVtb.htm)|Golem Antimagic|Antimagie du golem|officielle|
|[t8wpGhneIB81fXLO.htm](extinction-curse-bestiary-items/t8wpGhneIB81fXLO.htm)|Magic Missile (At Will)|Projectile magique (À volonté)|officielle|
|[t8XNlOoaUrFDisdv.htm](extinction-curse-bestiary-items/t8XNlOoaUrFDisdv.htm)|Freedom of Movement (Constant)|Liberté de mouvement (constant)|officielle|
|[tAJk9Rlb2IK6RZDK.htm](extinction-curse-bestiary-items/tAJk9Rlb2IK6RZDK.htm)|Claw|Griffe|officielle|
|[taymsycnWc4CSJIZ.htm](extinction-curse-bestiary-items/taymsycnWc4CSJIZ.htm)|Darkvision|Vision dans le noir|officielle|
|[tBRAlDxZSt1mK0F8.htm](extinction-curse-bestiary-items/tBRAlDxZSt1mK0F8.htm)|Jaws|Mâchoires|officielle|
|[TCAdoYNBMwayH1bw.htm](extinction-curse-bestiary-items/TCAdoYNBMwayH1bw.htm)|+1 Resilient Chain Shirt|Chemise de mailles de résilience +1|officielle|
|[tdl0fojnWCseyME0.htm](extinction-curse-bestiary-items/tdl0fojnWCseyME0.htm)|Rejuvenation|Reconstruction|officielle|
|[TDTA1aGACXzFFKJ1.htm](extinction-curse-bestiary-items/TDTA1aGACXzFFKJ1.htm)|Low-Light Vision|Vision nocturne|officielle|
|[TdYMUhqe9meOFVEe.htm](extinction-curse-bestiary-items/TdYMUhqe9meOFVEe.htm)|At-Will Spells|Sorts à volonté|officielle|
|[teoYBsXeF4ePoNS9.htm](extinction-curse-bestiary-items/teoYBsXeF4ePoNS9.htm)|Spiked Gauntlet|+1,striking|Gantelet clouté de frappe +1|libre|
|[texToCbGMNj42gJm.htm](extinction-curse-bestiary-items/texToCbGMNj42gJm.htm)|Jaws|Mâchoires|officielle|
|[tF6fTR0l1CiErgQt.htm](extinction-curse-bestiary-items/tF6fTR0l1CiErgQt.htm)|Darkvision|Vision dans le noir|officielle|
|[tfSYqUkDGCRtI9KG.htm](extinction-curse-bestiary-items/tfSYqUkDGCRtI9KG.htm)|+2 Status to All Saves vs. Mental Effects|+2 de statut contre les effets mentaux|officielle|
|[TFv6rcyouMcUWGYM.htm](extinction-curse-bestiary-items/TFv6rcyouMcUWGYM.htm)|Jaws|Mâchoires|officielle|
|[TFVafGVljhShJjij.htm](extinction-curse-bestiary-items/TFVafGVljhShJjij.htm)|Intimidation|Intimidation|officielle|
|[TG7StlfXO7qcbaFy.htm](extinction-curse-bestiary-items/TG7StlfXO7qcbaFy.htm)|Cave Lore|Connaissance des cavernes|officielle|
|[tgKfIAeJLnZoj24L.htm](extinction-curse-bestiary-items/tgKfIAeJLnZoj24L.htm)|Signal Arrows|Flèche d'alerte|officielle|
|[th7bXgkU6h5f3edz.htm](extinction-curse-bestiary-items/th7bXgkU6h5f3edz.htm)|Sticky Poison|Poison poisseux|officielle|
|[ThFBVTrezGFEC7SU.htm](extinction-curse-bestiary-items/ThFBVTrezGFEC7SU.htm)|Vulnerable to Neutralize Poison|Vulnérable à Neutralisation du poison|officielle|
|[thIGTmJJq0pQ2UGS.htm](extinction-curse-bestiary-items/thIGTmJJq0pQ2UGS.htm)|Ranseur|Corsèque|officielle|
|[TiC9nLfhB47bWwSQ.htm](extinction-curse-bestiary-items/TiC9nLfhB47bWwSQ.htm)|Low-Light Vision|Vision nocturne|officielle|
|[TJ2DjlYVnOpP8O0a.htm](extinction-curse-bestiary-items/TJ2DjlYVnOpP8O0a.htm)|Fist|Poing|officielle|
|[tjSb9xUg6kUCt5Jc.htm](extinction-curse-bestiary-items/tjSb9xUg6kUCt5Jc.htm)|Improved Grab|Empoignade améliorée|officielle|
|[tk1qMQuv7snMkpnc.htm](extinction-curse-bestiary-items/tk1qMQuv7snMkpnc.htm)|Breath Weapon|Souffle|officielle|
|[TKCUudDUNrXv1Z8q.htm](extinction-curse-bestiary-items/TKCUudDUNrXv1Z8q.htm)|Acid Flask (Moderate) (Infused)|Fiole d'acide modérée (imprégnée)|officielle|
|[tkLePiTdXXB3VbWz.htm](extinction-curse-bestiary-items/tkLePiTdXXB3VbWz.htm)|Scorching Maul|Écharper et brûler|officielle|
|[Tlt7eeLvAwfloiuI.htm](extinction-curse-bestiary-items/Tlt7eeLvAwfloiuI.htm)|Sneak Attack|Attaque sournoise|officielle|
|[to3YPHwxklrUbd6F.htm](extinction-curse-bestiary-items/to3YPHwxklrUbd6F.htm)|Darkvision|Vision dans le noir|officielle|
|[togeLXdDUkCenYQ2.htm](extinction-curse-bestiary-items/togeLXdDUkCenYQ2.htm)|Darkvision|Vision dans le noir|officielle|
|[tOUoA3MBZL0bbGQn.htm](extinction-curse-bestiary-items/tOUoA3MBZL0bbGQn.htm)|Delusional Pride (At Will) (Emotional Focus)|Fierté illusoire (à volonté, Focalisation émotionnelle)|officielle|
|[tp9ORpYsDj2Yd3XE.htm](extinction-curse-bestiary-items/tp9ORpYsDj2Yd3XE.htm)|Zevgavizeb Lore|Connaissance de Zevgavizeb|officielle|
|[tQg1mD39bXYAzcyF.htm](extinction-curse-bestiary-items/tQg1mD39bXYAzcyF.htm)|Darkvision|Vision dans le noir|libre|
|[TQoNfhkr54rzdEGA.htm](extinction-curse-bestiary-items/TQoNfhkr54rzdEGA.htm)|Etheric Fibers|Fibres éthériques|officielle|
|[TqypIhZnwhIuh2eR.htm](extinction-curse-bestiary-items/TqypIhZnwhIuh2eR.htm)|Thoughtsense (Imprecise) 60 feet|Perception des pensées 18 m (imprécis)|officielle|
|[TRACG7yjgPCzShSH.htm](extinction-curse-bestiary-items/TRACG7yjgPCzShSH.htm)|Furious Sprint|Course furieuse|officielle|
|[trpqjYarZmAIE6JR.htm](extinction-curse-bestiary-items/trpqjYarZmAIE6JR.htm)|Detect Magic (Constant)|Détection de la magie (constant)|officielle|
|[tRUwzQREvBkvCsOz.htm](extinction-curse-bestiary-items/tRUwzQREvBkvCsOz.htm)|Surprise Attack|Attaque surprise|officielle|
|[tsSj7T94dDPZAsP0.htm](extinction-curse-bestiary-items/tsSj7T94dDPZAsP0.htm)|Wildwood Halfling|Halfelin bois-sauvage|officielle|
|[tuA9DbCZRvsPkJ25.htm](extinction-curse-bestiary-items/tuA9DbCZRvsPkJ25.htm)|Tremorsense (Imprecise) 60 feet|Perception des vibrations 18 m (imprécis)|officielle|
|[tW1DUMQeCYYiJFWi.htm](extinction-curse-bestiary-items/tW1DUMQeCYYiJFWi.htm)|Dream Haunting|Hanter les rêves|officielle|
|[tWCEPnK5ZMOw1rIS.htm](extinction-curse-bestiary-items/tWCEPnK5ZMOw1rIS.htm)|Spear|+2,striking|Lance de frappe +2|officielle|
|[TXbhRlWu7SAx8Ctw.htm](extinction-curse-bestiary-items/TXbhRlWu7SAx8Ctw.htm)|Powerful Stench|Puissante puanteur|officielle|
|[TXT2Ytirxdb5hIhI.htm](extinction-curse-bestiary-items/TXT2Ytirxdb5hIhI.htm)|Blinding Flare|Lumière aveuglante|officielle|
|[tzGttrmK2YxnEDXH.htm](extinction-curse-bestiary-items/tzGttrmK2YxnEDXH.htm)|Stealth|Discrétion|officielle|
|[TzHZRpvvWRrSuk59.htm](extinction-curse-bestiary-items/TzHZRpvvWRrSuk59.htm)|Darkness (At Will)|Ténèbres (À volonté)|officielle|
|[tZRyEtuS7OQFZRDu.htm](extinction-curse-bestiary-items/tZRyEtuS7OQFZRDu.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[tZyhBnoDUGVEDL7x.htm](extinction-curse-bestiary-items/tZyhBnoDUGVEDL7x.htm)|Lightning Catcher|Capteur de lumière|officielle|
|[U2pZ9Ib09IyARwgZ.htm](extinction-curse-bestiary-items/U2pZ9Ib09IyARwgZ.htm)|Nature|Nature|officielle|
|[U3GaqjscOV9MmG6W.htm](extinction-curse-bestiary-items/U3GaqjscOV9MmG6W.htm)|Arcane Innate Spells|Sorts arcaniques innés|libre|
|[u3ZNtTIdDIwBtNav.htm](extinction-curse-bestiary-items/u3ZNtTIdDIwBtNav.htm)|Concentrated Xulgath Bile|Bile xulgathe concentrée|officielle|
|[U4mx6nAal2UDRgr5.htm](extinction-curse-bestiary-items/U4mx6nAal2UDRgr5.htm)|Shortbow|Arc court|officielle|
|[u6AGWAaIUZZF6DmM.htm](extinction-curse-bestiary-items/u6AGWAaIUZZF6DmM.htm)|Psionic Scent (Imprecise) 120 feet|Odorat psionique|officielle|
|[u6kenk4mksE3KEYJ.htm](extinction-curse-bestiary-items/u6kenk4mksE3KEYJ.htm)|Religious Symbol (Wooden) of Zevgavizeb|Symbole religieux en bois de Zevgavizeb|officielle|
|[u7gy4Uzdf1tEa3xv.htm](extinction-curse-bestiary-items/u7gy4Uzdf1tEa3xv.htm)|Spike|Pics|officielle|
|[U8nRgKXL8LdnfuMo.htm](extinction-curse-bestiary-items/U8nRgKXL8LdnfuMo.htm)|Lava Ball|Boule de lave|officielle|
|[uaFGybzZAhGxxd3L.htm](extinction-curse-bestiary-items/uaFGybzZAhGxxd3L.htm)|Psychogenic Secretions|Sécrétions psychogènes|officielle|
|[UBibmfZaH20tWub7.htm](extinction-curse-bestiary-items/UBibmfZaH20tWub7.htm)|Detect Alignment (Constant) (All Alignments Simultaneously)|Détection de l'alignement (constant, tous simultanément)|officielle|
|[ubs94wAy7XupF4V3.htm](extinction-curse-bestiary-items/ubs94wAy7XupF4V3.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|libre|
|[UBZHa183gehiCiti.htm](extinction-curse-bestiary-items/UBZHa183gehiCiti.htm)|Darkvision|Vision dans le noir|officielle|
|[ucPPtZXjJlD0iDBT.htm](extinction-curse-bestiary-items/ucPPtZXjJlD0iDBT.htm)|Hoe|Houe|officielle|
|[udZhqwVQP0tOUeZZ.htm](extinction-curse-bestiary-items/udZhqwVQP0tOUeZZ.htm)|True Seeing (Constant)|Vision lucide (constant)|officielle|
|[UE6Gaj3IfXSpVJue.htm](extinction-curse-bestiary-items/UE6Gaj3IfXSpVJue.htm)|Signal Arrow|Flèche d'alerte|officielle|
|[UeNHmJELEMJ1Bs7N.htm](extinction-curse-bestiary-items/UeNHmJELEMJ1Bs7N.htm)|Scroll of Heroism (Level 9)|Parchemin d'Héroïsme (niveau 9)|officielle|
|[UEuaQyxEGdYTyPJh.htm](extinction-curse-bestiary-items/UEuaQyxEGdYTyPJh.htm)|Darkvision|Vision dans le noir|officielle|
|[Ug2gQJqmRhm1KiCo.htm](extinction-curse-bestiary-items/Ug2gQJqmRhm1KiCo.htm)|Jaws|Mâchoires|officielle|
|[uH9ary8OcexwY8Hf.htm](extinction-curse-bestiary-items/uH9ary8OcexwY8Hf.htm)|Cat's Luck|Chance du félin|officielle|
|[UhrlbhvCVWik38KZ.htm](extinction-curse-bestiary-items/UhrlbhvCVWik38KZ.htm)|Mirror Vulnerability|Vulnérabilité aux miroirs|officielle|
|[Ui0JxPyP262iI9kF.htm](extinction-curse-bestiary-items/Ui0JxPyP262iI9kF.htm)|Xulgath Bile|Bile Xulgath|officielle|
|[uI0lVkJrLHDNJ0a3.htm](extinction-curse-bestiary-items/uI0lVkJrLHDNJ0a3.htm)|Grab|Empoignade|officielle|
|[uIFV7L1y3JpQbP7J.htm](extinction-curse-bestiary-items/uIFV7L1y3JpQbP7J.htm)|Profane Gift|Don blasphématoire|officielle|
|[Uj6U5QEXhGHuGrJ0.htm](extinction-curse-bestiary-items/Uj6U5QEXhGHuGrJ0.htm)|Proboscis|Trompe|officielle|
|[ujQNaJiDY5ja5QlI.htm](extinction-curse-bestiary-items/ujQNaJiDY5ja5QlI.htm)|Magical Tongue|Langue magique|officielle|
|[UK1kJIuJwvm59qRG.htm](extinction-curse-bestiary-items/UK1kJIuJwvm59qRG.htm)|Jaws|Mâchoires|officielle|
|[Ulxj8gjRkhtCZWc1.htm](extinction-curse-bestiary-items/Ulxj8gjRkhtCZWc1.htm)|Punishing Tail|Queue percutante|officielle|
|[un5jhCkWDwKr2WAC.htm](extinction-curse-bestiary-items/un5jhCkWDwKr2WAC.htm)|Stoneraiser Javelin|Javeline dresse-pierre|officielle|
|[Unihf1X2OVrBjHLa.htm](extinction-curse-bestiary-items/Unihf1X2OVrBjHLa.htm)|Tremorsense (Imprecise) 30 feet|Perception des vibrations 9 m (imprécis)|officielle|
|[UNNVhJIyikJIMTEt.htm](extinction-curse-bestiary-items/UNNVhJIyikJIMTEt.htm)|Constrict|Constriction|officielle|
|[UnpSG1NEamzBnZEZ.htm](extinction-curse-bestiary-items/UnpSG1NEamzBnZEZ.htm)|True Seeing (Constant)|Vision lucide (constant)|officielle|
|[UOtcq8C7cvf4ZBlr.htm](extinction-curse-bestiary-items/UOtcq8C7cvf4ZBlr.htm)|Animal Vision (At Will) (Dinosaurs Only)|Vision animale (À volonté) (dinosaures uniquement)|officielle|
|[UOVIQWXoyaOc2IBj.htm](extinction-curse-bestiary-items/UOVIQWXoyaOc2IBj.htm)|Nightmare Rider|Cavalière de destrier noir|officielle|
|[UpFgpADvEZmNKChA.htm](extinction-curse-bestiary-items/UpFgpADvEZmNKChA.htm)|Negative Healing|Guérison négative|officielle|
|[upwihrPQGTpiBwCk.htm](extinction-curse-bestiary-items/upwihrPQGTpiBwCk.htm)|Blightburn Sickness|Mal de la brûlure ardente|officielle|
|[uqAGOAWqfLDXtxZj.htm](extinction-curse-bestiary-items/uqAGOAWqfLDXtxZj.htm)|Darkvision|Vision dans le noir|officielle|
|[UquLgAOYtyCaklg8.htm](extinction-curse-bestiary-items/UquLgAOYtyCaklg8.htm)|Bastard Sword|Épée bâtarde|officielle|
|[URItNOOpBctKvf7C.htm](extinction-curse-bestiary-items/URItNOOpBctKvf7C.htm)|Pounce|Bond|officielle|
|[URt0ZSSRegSabF7O.htm](extinction-curse-bestiary-items/URt0ZSSRegSabF7O.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la Magie|officielle|
|[Us6h9h1kYfyx8LCO.htm](extinction-curse-bestiary-items/Us6h9h1kYfyx8LCO.htm)|Powerful Stench|Puissante puanteur|officielle|
|[usMIDvxtqfXusKnf.htm](extinction-curse-bestiary-items/usMIDvxtqfXusKnf.htm)|Phantom Hand|Main du fantôme|officielle|
|[ut1GcOdnxUjt4DaV.htm](extinction-curse-bestiary-items/ut1GcOdnxUjt4DaV.htm)|Jaws|Mâchoires|officielle|
|[uTDwnPruhh463nll.htm](extinction-curse-bestiary-items/uTDwnPruhh463nll.htm)|Geology Lore|Connaissance de la géologie|officielle|
|[UuJaCgIvEAS1EyoP.htm](extinction-curse-bestiary-items/UuJaCgIvEAS1EyoP.htm)|Longsword|Épée longue|officielle|
|[UwmfxnwjIDn1MR7C.htm](extinction-curse-bestiary-items/UwmfxnwjIDn1MR7C.htm)|+2 status to all saves vs. magic|+2 de statut contre la magie|officielle|
|[UX2gj24gyBJCM4LU.htm](extinction-curse-bestiary-items/UX2gj24gyBJCM4LU.htm)|Fly (Constant)|Vol (constant)|officielle|
|[UxbAHJiUJAsFm4cq.htm](extinction-curse-bestiary-items/UxbAHJiUJAsFm4cq.htm)|Breath Weapon|Arme de souffle|officielle|
|[UxkpitCYml2lDYR5.htm](extinction-curse-bestiary-items/UxkpitCYml2lDYR5.htm)|Bard Composition Spells|Sorts de composition de barde|libre|
|[uy0V4RLpmPZUTFQ9.htm](extinction-curse-bestiary-items/uy0V4RLpmPZUTFQ9.htm)|Pollen Spray|Pulvérisation de pollen|officielle|
|[uyZlbHUmh7S7hZjt.htm](extinction-curse-bestiary-items/uyZlbHUmh7S7hZjt.htm)|Change Shape|Changement de forme|officielle|
|[uZNc6ZOmT97s4O7v.htm](extinction-curse-bestiary-items/uZNc6ZOmT97s4O7v.htm)|Steal Soul|Vol d'âme|officielle|
|[V1W3sYkzk0bs9ztJ.htm](extinction-curse-bestiary-items/V1W3sYkzk0bs9ztJ.htm)|Darkness (At Will)|Ténèbres (À volonté)|officielle|
|[v3xcacl25td5gbaY.htm](extinction-curse-bestiary-items/v3xcacl25td5gbaY.htm)|Convergent Link|Lien convergent|officielle|
|[V4kdJTgqzGT8pdXd.htm](extinction-curse-bestiary-items/V4kdJTgqzGT8pdXd.htm)|Spiral of Despair|Spirale du désespoir|officielle|
|[V6jm45qr9Nbcz49c.htm](extinction-curse-bestiary-items/V6jm45qr9Nbcz49c.htm)|Jaws|Mâchoires|officielle|
|[vbLxcEZSddIEfLOW.htm](extinction-curse-bestiary-items/vbLxcEZSddIEfLOW.htm)|Web Trap|Piège de toile|officielle|
|[vbtSXntRG1Pu1oO6.htm](extinction-curse-bestiary-items/vbtSXntRG1Pu1oO6.htm)|Fly (Constant)|Vol (constant)|officielle|
|[VCPQniFfaKaaW5dO.htm](extinction-curse-bestiary-items/VCPQniFfaKaaW5dO.htm)|Dogged Persistence|Ténacité acharnée|officielle|
|[vCZP7eeVSKKUB4y7.htm](extinction-curse-bestiary-items/vCZP7eeVSKKUB4y7.htm)|Caustic Fog|Brouillard corrosif|officielle|
|[vd0T6m7E9RIQTVJH.htm](extinction-curse-bestiary-items/vd0T6m7E9RIQTVJH.htm)|Resin Crust|Croûte résineuse|officielle|
|[vdGhEdioWGsSOE8A.htm](extinction-curse-bestiary-items/vdGhEdioWGsSOE8A.htm)|Grab|Empoignade|officielle|
|[vdR9EBKzeEkLlmzv.htm](extinction-curse-bestiary-items/vdR9EBKzeEkLlmzv.htm)|Infused Items|Objets imprégnés|officielle|
|[Ve7Oe6nX2LPhgzr3.htm](extinction-curse-bestiary-items/Ve7Oe6nX2LPhgzr3.htm)|Constant Spells|Sorts constants|officielle|
|[vebtRV0UMG9x1p84.htm](extinction-curse-bestiary-items/vebtRV0UMG9x1p84.htm)|Divine Wrath (Lawful Only)|Colère divine (loyal uniquement)|officielle|
|[ves4VuZndUbyx170.htm](extinction-curse-bestiary-items/ves4VuZndUbyx170.htm)|Bile Jet|Jet de bile|officielle|
|[VEujF17wRSyBWQXO.htm](extinction-curse-bestiary-items/VEujF17wRSyBWQXO.htm)|Speak with Animals (Constant)|Communication avec les animaux (constant)|officielle|
|[vFlEsdlagsFZzRgT.htm](extinction-curse-bestiary-items/vFlEsdlagsFZzRgT.htm)|Warhammer|+1,striking|Marteau de guerre de frappe +1|officielle|
|[vGPRZAcP9sO1oSWb.htm](extinction-curse-bestiary-items/vGPRZAcP9sO1oSWb.htm)|Explosion|Déflagration|officielle|
|[vhSzb4rXivmMx69j.htm](extinction-curse-bestiary-items/vhSzb4rXivmMx69j.htm)|Corpse Consumption|Absorption de cadavres|officielle|
|[vHvfuUwHIpuo9u3k.htm](extinction-curse-bestiary-items/vHvfuUwHIpuo9u3k.htm)|Darkvision|Vision dans le noir|officielle|
|[VIc5tlcbWy6TfS7W.htm](extinction-curse-bestiary-items/VIc5tlcbWy6TfS7W.htm)|Vampire Weaknesses|Faiblesse des vampires|officielle|
|[VKEoaAVU8jhX7DV0.htm](extinction-curse-bestiary-items/VKEoaAVU8jhX7DV0.htm)|Claw|Griffes|officielle|
|[VKl3rUBu2nLA2szl.htm](extinction-curse-bestiary-items/VKl3rUBu2nLA2szl.htm)|Sylvan Wineskin|Outre de vin sylvestre|officielle|
|[vkLS4esGKnpycmG3.htm](extinction-curse-bestiary-items/vkLS4esGKnpycmG3.htm)|Grabbing Trunk|Empoignade à la trompe|officielle|
|[vLi3Zdc0TOzo5Ns5.htm](extinction-curse-bestiary-items/vLi3Zdc0TOzo5Ns5.htm)|Stench|Puanteur|officielle|
|[vmm346ihGo1JwNqk.htm](extinction-curse-bestiary-items/vmm346ihGo1JwNqk.htm)|Grab|Empoignade|officielle|
|[VMTjcCiLutAHY3LX.htm](extinction-curse-bestiary-items/VMTjcCiLutAHY3LX.htm)|Sudden Charge|Charge soudaine|officielle|
|[vMw7KfTKAEzlocp3.htm](extinction-curse-bestiary-items/vMw7KfTKAEzlocp3.htm)|Mirror Image (At Will)|Image miroir (À volonté)|officielle|
|[Vn8Ud2h9UUkYBpqn.htm](extinction-curse-bestiary-items/Vn8Ud2h9UUkYBpqn.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[vNO1fTV51lJ1XgEx.htm](extinction-curse-bestiary-items/vNO1fTV51lJ1XgEx.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[voHVxry1TMmVuJ05.htm](extinction-curse-bestiary-items/voHVxry1TMmVuJ05.htm)|Dirt Clod|Motte de terre|officielle|
|[VojkR50T97lhGAno.htm](extinction-curse-bestiary-items/VojkR50T97lhGAno.htm)|Blunt Snout|Truffe épatée|officielle|
|[VoNCwqZ2SF42txoX.htm](extinction-curse-bestiary-items/VoNCwqZ2SF42txoX.htm)|True Seeing (Constant)|Vision lucide (constant)|officielle|
|[vOSiesS62Grm2PL7.htm](extinction-curse-bestiary-items/vOSiesS62Grm2PL7.htm)|Composite Longbow|+1,striking|Arc long composite de frappe +1|officielle|
|[VPLYApahsyX5VFMb.htm](extinction-curse-bestiary-items/VPLYApahsyX5VFMb.htm)|Claw|Griffe|officielle|
|[VQ95h7HFFMZ0YpGA.htm](extinction-curse-bestiary-items/VQ95h7HFFMZ0YpGA.htm)|Jaws|Mâchoires|officielle|
|[VsbGRflYaQb24YM7.htm](extinction-curse-bestiary-items/VsbGRflYaQb24YM7.htm)|Darkvision|Vision dans le noir|officielle|
|[VsSnUcSJag9gY3y8.htm](extinction-curse-bestiary-items/VsSnUcSJag9gY3y8.htm)|Mobility|Mobilité|officielle|
|[Vt51sZhJfLZaVw6i.htm](extinction-curse-bestiary-items/Vt51sZhJfLZaVw6i.htm)|Ghostly Grip|Prise spectrale|officielle|
|[VTbk56rOHlsO8Mcf.htm](extinction-curse-bestiary-items/VTbk56rOHlsO8Mcf.htm)|Circus Lore|Connaissance du cirque|officielle|
|[VTneIxzjqMPtSAZp.htm](extinction-curse-bestiary-items/VTneIxzjqMPtSAZp.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[VTpDnvKqgWyKG8Ow.htm](extinction-curse-bestiary-items/VTpDnvKqgWyKG8Ow.htm)|Claw|Griffe|officielle|
|[vtYn8bg0Liql8rWa.htm](extinction-curse-bestiary-items/vtYn8bg0Liql8rWa.htm)|Improved Grab|Empoignade améliorée|officielle|
|[vU5wizpU0tuQR4N7.htm](extinction-curse-bestiary-items/vU5wizpU0tuQR4N7.htm)|Whip Vulnerability|Vulnérabilité au fouet|officielle|
|[vUEEF3wzoLnBwUKQ.htm](extinction-curse-bestiary-items/vUEEF3wzoLnBwUKQ.htm)|Telepathy 100 feet|Télépathie 30 m|officielle|
|[vW9bna7OUkxyfZxx.htm](extinction-curse-bestiary-items/vW9bna7OUkxyfZxx.htm)|Darkvision|Vision dans le noir|officielle|
|[vwi28Ix3z8efoGcw.htm](extinction-curse-bestiary-items/vwi28Ix3z8efoGcw.htm)|Grab|Empoignade|officielle|
|[vWOXvMUv1MGUsjKM.htm](extinction-curse-bestiary-items/vWOXvMUv1MGUsjKM.htm)|Jaws|Mâchoires|officielle|
|[vwr9j20rI5uA7Vs7.htm](extinction-curse-bestiary-items/vwr9j20rI5uA7Vs7.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[vYBcrx4xIUPetI0S.htm](extinction-curse-bestiary-items/vYBcrx4xIUPetI0S.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[VyBZbrON0TldOYvl.htm](extinction-curse-bestiary-items/VyBZbrON0TldOYvl.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|officielle|
|[vYd3rbKnbc32kox2.htm](extinction-curse-bestiary-items/vYd3rbKnbc32kox2.htm)|Motion Sense 60 feet|Perception du mouvement|officielle|
|[VyjU0grk7ThTTa4U.htm](extinction-curse-bestiary-items/VyjU0grk7ThTTa4U.htm)|Claw|Griffe|officielle|
|[vZ2fFYnZKoSgkkhI.htm](extinction-curse-bestiary-items/vZ2fFYnZKoSgkkhI.htm)|Breath Weapon|Souffle|officielle|
|[Vz8k5bYltczBVQhv.htm](extinction-curse-bestiary-items/Vz8k5bYltczBVQhv.htm)|+2 Resilient Leather Armor|Armure en cuir de résilience +2|officielle|
|[VZsSOXRGbGwz6dqc.htm](extinction-curse-bestiary-items/VZsSOXRGbGwz6dqc.htm)|Animated Attack|Attaque animée|officielle|
|[W0iJibS7qX5Av9oY.htm](extinction-curse-bestiary-items/W0iJibS7qX5Av9oY.htm)|Knockdown|Renversement|officielle|
|[W1NWJyvyZIHarWZi.htm](extinction-curse-bestiary-items/W1NWJyvyZIHarWZi.htm)|Jaws|Mâchoires|officielle|
|[w285iz3GQ53ORMER.htm](extinction-curse-bestiary-items/w285iz3GQ53ORMER.htm)|Blood Eruption|Éruption de sang|officielle|
|[W4dTvjsUpOMjS9ip.htm](extinction-curse-bestiary-items/W4dTvjsUpOMjS9ip.htm)|Tremorsense (Imprecise) 60 feet|Perception des vibrations 18 m (imprécis)|officielle|
|[W6tZElfSPD000KNK.htm](extinction-curse-bestiary-items/W6tZElfSPD000KNK.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[w8UngXIC47fcMU5n.htm](extinction-curse-bestiary-items/w8UngXIC47fcMU5n.htm)|+2 Circumstance to All Saves vs. Disarm|+2 de circonstances aux JdS contre Désarmer|officielle|
|[wAyWp8e9RHnS2chv.htm](extinction-curse-bestiary-items/wAyWp8e9RHnS2chv.htm)|Javelin|Javeline|officielle|
|[WC2Jedgc1MI3i61x.htm](extinction-curse-bestiary-items/WC2Jedgc1MI3i61x.htm)|Paddler Shoony|Pagayeur|officielle|
|[WcSu6LxPKmis0S40.htm](extinction-curse-bestiary-items/WcSu6LxPKmis0S40.htm)|Disrupted Link|Lien interrompu|officielle|
|[WdgZS5lTwdxmxcS7.htm](extinction-curse-bestiary-items/WdgZS5lTwdxmxcS7.htm)|Spiked Gauntlet|+3,majorStriking,unholy|Gantelet clouté impie de frappe majeure +3|libre|
|[wFol3Gz0VjCNM4zQ.htm](extinction-curse-bestiary-items/wFol3Gz0VjCNM4zQ.htm)|Bloodline Spells|Sorts de lignage|libre|
|[wg9Dbifr3nxpoe9D.htm](extinction-curse-bestiary-items/wg9Dbifr3nxpoe9D.htm)|True Seeing (Constant)|Vision lucide (constant)|officielle|
|[wgAgCBHThMqRAxPI.htm](extinction-curse-bestiary-items/wgAgCBHThMqRAxPI.htm)|A Brush and Pot of Red Paint|Un pinceau et un pot de peinture rouge|officielle|
|[WggmVdmCE4CvyNXL.htm](extinction-curse-bestiary-items/WggmVdmCE4CvyNXL.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[WgQCSrHux9sfsvj3.htm](extinction-curse-bestiary-items/WgQCSrHux9sfsvj3.htm)|Trident|Trident|officielle|
|[wKH8WPFicTjEMDpB.htm](extinction-curse-bestiary-items/wKH8WPFicTjEMDpB.htm)|Draining Touch|Toucher affaiblissant|officielle|
|[wKHSYNpLLDbCM74h.htm](extinction-curse-bestiary-items/wKHSYNpLLDbCM74h.htm)|Religious Symbol (Silver) of Zevgavizeb|Symbole religieux en argent de Zevgavizeb|officielle|
|[WL6FySYpcxtf9ULD.htm](extinction-curse-bestiary-items/WL6FySYpcxtf9ULD.htm)|Wing|Aile|officielle|
|[WLQETe40LtIHyPGa.htm](extinction-curse-bestiary-items/WLQETe40LtIHyPGa.htm)|Sneak Attack|Attaque sournoise|officielle|
|[wM1opIZyPoYqYrGh.htm](extinction-curse-bestiary-items/wM1opIZyPoYqYrGh.htm)|Low-Light Vision|Vision nocturne|officielle|
|[wmFjSXvEH8SqQPNG.htm](extinction-curse-bestiary-items/wmFjSXvEH8SqQPNG.htm)|Darkvision|Vision dans le noir|officielle|
|[wMph0ESDRk2zz1Ff.htm](extinction-curse-bestiary-items/wMph0ESDRk2zz1Ff.htm)|Juggernaut|Juggernaut|officielle|
|[WmQ1U0HMX7ayTqMC.htm](extinction-curse-bestiary-items/WmQ1U0HMX7ayTqMC.htm)|Unsettling Movement|Mouvement terrifiant|officielle|
|[Wn8lDlqkYOulrZju.htm](extinction-curse-bestiary-items/Wn8lDlqkYOulrZju.htm)|Negative Healing|Guérison négative|officielle|
|[wnbjJLnvmvIEGia9.htm](extinction-curse-bestiary-items/wnbjJLnvmvIEGia9.htm)|Choke Slam|Choc empoigné|officielle|
|[wPnZZSRGb9t9kP6p.htm](extinction-curse-bestiary-items/wPnZZSRGb9t9kP6p.htm)|Dagger|Dague|officielle|
|[wPt51hV4BCFg08eR.htm](extinction-curse-bestiary-items/wPt51hV4BCFg08eR.htm)|Short Stool|Petit tabouret|officielle|
|[Wq1u7bRZlmf8iWH5.htm](extinction-curse-bestiary-items/Wq1u7bRZlmf8iWH5.htm)|Powerful Stench|Puissante puanteur|officielle|
|[WqAoB01jXaEfzA7R.htm](extinction-curse-bestiary-items/WqAoB01jXaEfzA7R.htm)|Athletics|Athlétisme|officielle|
|[wQkNuWQMiqVySwgb.htm](extinction-curse-bestiary-items/wQkNuWQMiqVySwgb.htm)|Ranseur|+2,greaterStriking|Corsèque de frappe supérieure +2|libre|
|[WqZw6sQ6UuR8XHlz.htm](extinction-curse-bestiary-items/WqZw6sQ6UuR8XHlz.htm)|Lore|Connaissance|officielle|
|[wrAEK8zz0CszI6AJ.htm](extinction-curse-bestiary-items/wrAEK8zz0CszI6AJ.htm)|Darkvision|Vision dans le noir|officielle|
|[ws10H9jL2IqUsARn.htm](extinction-curse-bestiary-items/ws10H9jL2IqUsARn.htm)|Sudden Slices|Découpes imprévisibles|officielle|
|[wSygxDqpllEezbzw.htm](extinction-curse-bestiary-items/wSygxDqpllEezbzw.htm)|Staff|Bâton|officielle|
|[WtzTULcqCIDli7Sh.htm](extinction-curse-bestiary-items/WtzTULcqCIDli7Sh.htm)|Kukri|Kukri|officielle|
|[wUOW3RgVxG2HQmH0.htm](extinction-curse-bestiary-items/wUOW3RgVxG2HQmH0.htm)|Pseudopod|Pseudopode|officielle|
|[wv4csg9YxPFOP1Pm.htm](extinction-curse-bestiary-items/wv4csg9YxPFOP1Pm.htm)|Infused Items|Objets imprégnés|officielle|
|[WV6I29RGQdzGq17J.htm](extinction-curse-bestiary-items/WV6I29RGQdzGq17J.htm)|Claw|Griffe|officielle|
|[Ww4pKd6IZaOBRAbN.htm](extinction-curse-bestiary-items/Ww4pKd6IZaOBRAbN.htm)|Innate Divine Spells|Sorts divins innés|libre|
|[wwBDrFSCf5XFZioZ.htm](extinction-curse-bestiary-items/wwBDrFSCf5XFZioZ.htm)|Divine Wrath (At Will)|Colère divine (À volonté)|officielle|
|[WWVER4ZYSmZhye2k.htm](extinction-curse-bestiary-items/WWVER4ZYSmZhye2k.htm)|Theatre Lore|Connaissance théâtrale|officielle|
|[WXL6oZ1rorjYNmxa.htm](extinction-curse-bestiary-items/WXL6oZ1rorjYNmxa.htm)|Spiked Gauntlet|+1,striking|Gantelet clouté de frappe +1|libre|
|[wYMjLcGkMx9LTbKi.htm](extinction-curse-bestiary-items/wYMjLcGkMx9LTbKi.htm)|+1 Striking Falchion|Cimeterre à deux mains de frappe +1|officielle|
|[X4jmRoph9IEyOVDv.htm](extinction-curse-bestiary-items/X4jmRoph9IEyOVDv.htm)|Light Step|Pas léger|officielle|
|[X5jrkgJhRyd0x78g.htm](extinction-curse-bestiary-items/X5jrkgJhRyd0x78g.htm)|Earthen Blow|Souffle de terre|officielle|
|[x8bFDzkhGGqdhVb2.htm](extinction-curse-bestiary-items/x8bFDzkhGGqdhVb2.htm)|Inexorable March|Marche inexorable|officielle|
|[X8zCQwR4m0Kqzz0U.htm](extinction-curse-bestiary-items/X8zCQwR4m0Kqzz0U.htm)|Coven Spells|Sorts de cercle|libre|
|[x8zvyv0LEKpl4zb1.htm](extinction-curse-bestiary-items/x8zvyv0LEKpl4zb1.htm)|Pack Attack|Attaque en meute|officielle|
|[xAhm4AbqiD9yy2Or.htm](extinction-curse-bestiary-items/xAhm4AbqiD9yy2Or.htm)|Cleaver|Fendoir|officielle|
|[XB84vRtRawFpT6yg.htm](extinction-curse-bestiary-items/XB84vRtRawFpT6yg.htm)|Insidious Mummy Rot|Putréfaction insidieuse de momie|officielle|
|[xc31WWNLp2WBfKhJ.htm](extinction-curse-bestiary-items/xc31WWNLp2WBfKhJ.htm)|Greatclub|Massue|officielle|
|[xcCjk9YBXJF1CPRT.htm](extinction-curse-bestiary-items/xcCjk9YBXJF1CPRT.htm)|Fast Healing 20|Guérison accélérée 20|officielle|
|[Xck3GduL0mgbVURO.htm](extinction-curse-bestiary-items/Xck3GduL0mgbVURO.htm)|Early Collapse|Écroulement précoce|officielle|
|[XDQPyhPgdA1Luya1.htm](extinction-curse-bestiary-items/XDQPyhPgdA1Luya1.htm)|Telekinetic Haul (At Will)|Transport télékinésique (À volonté)|officielle|
|[xDvNX3A7fi4Fr7Rj.htm](extinction-curse-bestiary-items/xDvNX3A7fi4Fr7Rj.htm)|Fast Healing 20|Guérison accélérée 20|officielle|
|[xdZGT2FQbkaAnnVv.htm](extinction-curse-bestiary-items/xdZGT2FQbkaAnnVv.htm)|Stench|Puanteur|officielle|
|[Xe68wniGERn685DL.htm](extinction-curse-bestiary-items/Xe68wniGERn685DL.htm)|Longsword|Épée longue|officielle|
|[Xg6Mx6eG3Oigbdmv.htm](extinction-curse-bestiary-items/Xg6Mx6eG3Oigbdmv.htm)|Necrotic Decay|Décomposition nécrotique|officielle|
|[XGl6e6PBFVB4qmjB.htm](extinction-curse-bestiary-items/XGl6e6PBFVB4qmjB.htm)|Play the Pipes|Jouer de la flûte|officielle|
|[Xh9l2gZgUXuevWZW.htm](extinction-curse-bestiary-items/Xh9l2gZgUXuevWZW.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|officielle|
|[xhdtj1UGrpdoFfnG.htm](extinction-curse-bestiary-items/xhdtj1UGrpdoFfnG.htm)|Rapier|Rapière|officielle|
|[XhQyqq54ULAcj0tz.htm](extinction-curse-bestiary-items/XhQyqq54ULAcj0tz.htm)|Dagger|Dague|officielle|
|[xINgpEoG6H55cFZu.htm](extinction-curse-bestiary-items/xINgpEoG6H55cFZu.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|officielle|
|[XiYeDNfpnbFWC8bp.htm](extinction-curse-bestiary-items/XiYeDNfpnbFWC8bp.htm)|Invisibility (Self Only)|Invisibilité (soi uniquement)|officielle|
|[XKYMPRy9Hv28Tbkk.htm](extinction-curse-bestiary-items/XKYMPRy9Hv28Tbkk.htm)|Three-Limbed Lunge|Triple fente|officielle|
|[Xmcr4PwRCrFQgjVC.htm](extinction-curse-bestiary-items/Xmcr4PwRCrFQgjVC.htm)|Rejuvenation|Reconstruction|officielle|
|[XMV37fJggFZoHRuj.htm](extinction-curse-bestiary-items/XMV37fJggFZoHRuj.htm)|Daemon Lore|Connaissance des daémons|officielle|
|[XMwEKtn4GT15Vkbz.htm](extinction-curse-bestiary-items/XMwEKtn4GT15Vkbz.htm)|Grab|Empoignade|officielle|
|[xnDIzYzOSJgR2VHr.htm](extinction-curse-bestiary-items/xnDIzYzOSJgR2VHr.htm)|Tusk|Défense|officielle|
|[xNeFCRc6vVzyQDT0.htm](extinction-curse-bestiary-items/xNeFCRc6vVzyQDT0.htm)|Bokrug's Lashing Tail|+2,greaterStriking,keen|Queue de flagellation de Bokrug|officielle|
|[xNolyNRgNNCS7SrY.htm](extinction-curse-bestiary-items/xNolyNRgNNCS7SrY.htm)|Feasting Tentacles|Tentacules voraces|officielle|
|[XNTq0RqLFZcAzxUn.htm](extinction-curse-bestiary-items/XNTq0RqLFZcAzxUn.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[xnyGzApt0aAZnZYJ.htm](extinction-curse-bestiary-items/xnyGzApt0aAZnZYJ.htm)|Mist Escape|Fuite brumeuse|officielle|
|[Xo4wSqGnQ754izAW.htm](extinction-curse-bestiary-items/Xo4wSqGnQ754izAW.htm)|Devour Soul|Dévorer une âme|officielle|
|[xP1DZ7qExdlGxTkQ.htm](extinction-curse-bestiary-items/xP1DZ7qExdlGxTkQ.htm)|Acidic Effluence|Effluence acide|officielle|
|[xQhi5H6icTLenWSG.htm](extinction-curse-bestiary-items/xQhi5H6icTLenWSG.htm)|Needles|Aiguilles|officielle|
|[XrGLOuWPI7PbY3JT.htm](extinction-curse-bestiary-items/XrGLOuWPI7PbY3JT.htm)|Stench|Puanteur|officielle|
|[xrgMIiBqzQXebseK.htm](extinction-curse-bestiary-items/xrgMIiBqzQXebseK.htm)|Bloom|Floraison|officielle|
|[xs380ivZqjl5WCwP.htm](extinction-curse-bestiary-items/xs380ivZqjl5WCwP.htm)|Charm (At Will)|Charme (À volonté)|officielle|
|[xSiITNruineLQZJU.htm](extinction-curse-bestiary-items/xSiITNruineLQZJU.htm)|Constant Spells|Sorts constants|officielle|
|[XSNIitnbXnIcTzSZ.htm](extinction-curse-bestiary-items/XSNIitnbXnIcTzSZ.htm)|Light Mace|Masse d'armes légère|officielle|
|[XtcOTRGK7GJm59i6.htm](extinction-curse-bestiary-items/XtcOTRGK7GJm59i6.htm)|Shield Block|Blocage au bouclier|officielle|
|[xUBmTzpgqhcLUJDd.htm](extinction-curse-bestiary-items/xUBmTzpgqhcLUJDd.htm)|Javelin|Javeline|officielle|
|[XUhIXiVMEs1bgrP7.htm](extinction-curse-bestiary-items/XUhIXiVMEs1bgrP7.htm)|Bite|Morsure|officielle|
|[XUO5Lf0Nls7cYGwN.htm](extinction-curse-bestiary-items/XUO5Lf0Nls7cYGwN.htm)|Eagle Dive|Piqué de l'aigle|officielle|
|[xVc28OEoqZGKHa2g.htm](extinction-curse-bestiary-items/xVc28OEoqZGKHa2g.htm)|Convergent Tactics|Stratégie convergente|officielle|
|[xX5Ycq93HKRAsmvI.htm](extinction-curse-bestiary-items/xX5Ycq93HKRAsmvI.htm)|Light Up|Fulgurer|officielle|
|[xyO3M6BDrtYA6yRv.htm](extinction-curse-bestiary-items/xyO3M6BDrtYA6yRv.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|officielle|
|[XYsfQIAGwEG7CEHy.htm](extinction-curse-bestiary-items/XYsfQIAGwEG7CEHy.htm)|Cleaver|Fendoir|officielle|
|[y0FMfElX3eNgSQWh.htm](extinction-curse-bestiary-items/y0FMfElX3eNgSQWh.htm)|Dagger|Dague|officielle|
|[y0mBs7a3QdDfJbUQ.htm](extinction-curse-bestiary-items/y0mBs7a3QdDfJbUQ.htm)|Predator's Leap|Bond de prédateur|officielle|
|[Y0ySCcnmHvN9oQCd.htm](extinction-curse-bestiary-items/Y0ySCcnmHvN9oQCd.htm)|Iron Golem Poison|Poison du golem de fer|officielle|
|[Y16FL2sBfoT0zbNY.htm](extinction-curse-bestiary-items/Y16FL2sBfoT0zbNY.htm)|Attack of Opportunity (Tentacle Only)|Attaque d'opportunité (Tentacule uniquement)|officielle|
|[Y4Ox9DseRSG2Epun.htm](extinction-curse-bestiary-items/Y4Ox9DseRSG2Epun.htm)|Wendigo Remnants|Vestiges de wendigo|officielle|
|[Y7eL4NGth0AzOHCF.htm](extinction-curse-bestiary-items/Y7eL4NGth0AzOHCF.htm)|Change Shape|Changement de forme|officielle|
|[Y7lD8hRHSgTJuVoF.htm](extinction-curse-bestiary-items/Y7lD8hRHSgTJuVoF.htm)|Grab|Empoignade|officielle|
|[Y8Ki9xMtjEbm4nIF.htm](extinction-curse-bestiary-items/Y8Ki9xMtjEbm4nIF.htm)|Horrid Visage|Visage épouvantable|officielle|
|[yaj6h10oMEPUzkTo.htm](extinction-curse-bestiary-items/yaj6h10oMEPUzkTo.htm)|Jaws|Mâchoires|officielle|
|[yAYC82wKWwT3Yr5O.htm](extinction-curse-bestiary-items/yAYC82wKWwT3Yr5O.htm)|Fist|Poing|officielle|
|[yaYZxfRFuqY5ZKUv.htm](extinction-curse-bestiary-items/yaYZxfRFuqY5ZKUv.htm)|Trapped in the Maze|Piégés dans le labyrinthe|officielle|
|[YC6l2GTlJST6HmzF.htm](extinction-curse-bestiary-items/YC6l2GTlJST6HmzF.htm)|Stoke the Fervent|Raviver le fervent|officielle|
|[ycvmtIzMkmfLy5hY.htm](extinction-curse-bestiary-items/ycvmtIzMkmfLy5hY.htm)|Dinosaur Lore|Connaissance des dinosaures|officielle|
|[YDEvyjWRnqtD024w.htm](extinction-curse-bestiary-items/YDEvyjWRnqtD024w.htm)|Jaws|Mâchoires|officielle|
|[YFBnzaEl7UBKGHLo.htm](extinction-curse-bestiary-items/YFBnzaEl7UBKGHLo.htm)|Composite Shortbow|Arc court composite|officielle|
|[yfi5yLVQJBHQQfgr.htm](extinction-curse-bestiary-items/yfi5yLVQJBHQQfgr.htm)|Whirlwind Strike|Frappe tourbillonnante|officielle|
|[YHBBkkagd6BLfiQo.htm](extinction-curse-bestiary-items/YHBBkkagd6BLfiQo.htm)|Deceitful Feast|Festin illusoire|officielle|
|[yhk9Uo7Z0sSIDvpj.htm](extinction-curse-bestiary-items/yhk9Uo7Z0sSIDvpj.htm)|Bag of Faces|Sac de visages|officielle|
|[yhUioD8pgXFVQiro.htm](extinction-curse-bestiary-items/yhUioD8pgXFVQiro.htm)|Feed on Emotion|Se nourrir des émotions|officielle|
|[YIb66Svrt8CZOKjj.htm](extinction-curse-bestiary-items/YIb66Svrt8CZOKjj.htm)|Renewed Vigor|Regain de vigueur|officielle|
|[yihwJb9tcVGBGiIv.htm](extinction-curse-bestiary-items/yihwJb9tcVGBGiIv.htm)|Thespian Aura|Aura de comédien|officielle|
|[yItwou9rDi09XB5s.htm](extinction-curse-bestiary-items/yItwou9rDi09XB5s.htm)|Reflection of Evil|Reflet maléfique|officielle|
|[Yj0Sq5ehsOc1b6S1.htm](extinction-curse-bestiary-items/Yj0Sq5ehsOc1b6S1.htm)|Darkvision|Vision dans le noir|officielle|
|[yjqHfRdCFN6ZTk5o.htm](extinction-curse-bestiary-items/yjqHfRdCFN6ZTk5o.htm)|Constable's Badge|Badge de garde de la ville|officielle|
|[yJXuk1WCt5HI5tjm.htm](extinction-curse-bestiary-items/yJXuk1WCt5HI5tjm.htm)|Darkvision|Vision dans le noir|libre|
|[Yk8dZyRSg1tLKIy8.htm](extinction-curse-bestiary-items/Yk8dZyRSg1tLKIy8.htm)|Swallow Whole|Gober|officielle|
|[yKLg0f7z9z5rM6iA.htm](extinction-curse-bestiary-items/yKLg0f7z9z5rM6iA.htm)|Self-Detonate|Implosion volontaire|officielle|
|[yM2uUCjL6GUDVlF0.htm](extinction-curse-bestiary-items/yM2uUCjL6GUDVlF0.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[YM5j7CwSD2jKdyee.htm](extinction-curse-bestiary-items/YM5j7CwSD2jKdyee.htm)|Light Step|Pas léger|officielle|
|[Ym5lHTSIM5818EUZ.htm](extinction-curse-bestiary-items/Ym5lHTSIM5818EUZ.htm)|+1 Striking Warhammer|+1,striking|Marteau de guerre de frappe +1|officielle|
|[yMe7KsfLbsSFSdYK.htm](extinction-curse-bestiary-items/yMe7KsfLbsSFSdYK.htm)|Remove Face|Ablation de visage|officielle|
|[YMzo8b4ZWBJo3wFO.htm](extinction-curse-bestiary-items/YMzo8b4ZWBJo3wFO.htm)|Low-Light Vision|Vision nocturne|officielle|
|[YOtRTnyprnEC769l.htm](extinction-curse-bestiary-items/YOtRTnyprnEC769l.htm)|Spell Ambush|Embûche magique|officielle|
|[YPaGpuTkNn4M3jlK.htm](extinction-curse-bestiary-items/YPaGpuTkNn4M3jlK.htm)|Shortbow|Arc court|officielle|
|[YphbnRiR9XKU2vJ0.htm](extinction-curse-bestiary-items/YphbnRiR9XKU2vJ0.htm)|Javelin|Javeline|officielle|
|[YPo8ttJxdRwBB28Q.htm](extinction-curse-bestiary-items/YPo8ttJxdRwBB28Q.htm)|Greater Frost Hand Crossbow|Arbalète de poing de froid supérieur|officielle|
|[YPpyu3mkV5IecFy4.htm](extinction-curse-bestiary-items/YPpyu3mkV5IecFy4.htm)|Change Shape|Changement de forme|officielle|
|[YRcfAf2JSSaoxUCv.htm](extinction-curse-bestiary-items/YRcfAf2JSSaoxUCv.htm)|Furious Claws|Furie de griffes|officielle|
|[Yrpk6vP1DrVPPPhK.htm](extinction-curse-bestiary-items/Yrpk6vP1DrVPPPhK.htm)|Foil the Hunt|Saboter la chasse|officielle|
|[YRWhIzukE4zO9fUH.htm](extinction-curse-bestiary-items/YRWhIzukE4zO9fUH.htm)|-2 to All Saves (If Hearstone is Lost)|-2 à tous les JdS (si la cardioline est perdue)|officielle|
|[yTuD5OYd94nyf8YO.htm](extinction-curse-bestiary-items/yTuD5OYd94nyf8YO.htm)|Consume Knowledge|Consommation de savoir|officielle|
|[yU4tKXoO17Gqu0i5.htm](extinction-curse-bestiary-items/yU4tKXoO17Gqu0i5.htm)|Low-Light Vision|Vision nocturne|officielle|
|[yVBUPO8bNkZEb4gC.htm](extinction-curse-bestiary-items/yVBUPO8bNkZEb4gC.htm)|Eat Away|Digestion|officielle|
|[yVbYX4gSvL0pm4Py.htm](extinction-curse-bestiary-items/yVbYX4gSvL0pm4Py.htm)|Inhabit Body|Occuper un corps|officielle|
|[Yvw6yKyrv3WnvdAA.htm](extinction-curse-bestiary-items/Yvw6yKyrv3WnvdAA.htm)|Musical Instrument (Panpipes)|Flûte de pan|officielle|
|[YWC5X9RpwpHTDgGR.htm](extinction-curse-bestiary-items/YWC5X9RpwpHTDgGR.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[YXTmEPsQrFyF3kDl.htm](extinction-curse-bestiary-items/YXTmEPsQrFyF3kDl.htm)|Juggler|Jongleur|officielle|
|[YYHOVKuankVTjlpC.htm](extinction-curse-bestiary-items/YYHOVKuankVTjlpC.htm)|Dimension Door (At Will)|Porte dimensionnelle (À volonté)|officielle|
|[yyIFnrfdqZb4Oqeh.htm](extinction-curse-bestiary-items/yyIFnrfdqZb4Oqeh.htm)|Devastating Blast|Déflagration dévastatrice|officielle|
|[YzXOUwdHiS1qUgkO.htm](extinction-curse-bestiary-items/YzXOUwdHiS1qUgkO.htm)|Constrict|Constriction|officielle|
|[Z0JKKHnRuGwdq3gP.htm](extinction-curse-bestiary-items/Z0JKKHnRuGwdq3gP.htm)|Minion|Sbire|officielle|
|[z0kRnyPJ1GIDPXB0.htm](extinction-curse-bestiary-items/z0kRnyPJ1GIDPXB0.htm)|Detect Magic (Constant)|Détection de la magie (constant)|officielle|
|[Z1FtloAoiSz9VnEc.htm](extinction-curse-bestiary-items/Z1FtloAoiSz9VnEc.htm)|Athletics|Athlétisme|officielle|
|[z346znqdZJAlbClQ.htm](extinction-curse-bestiary-items/z346znqdZJAlbClQ.htm)|Jolting Buzz|Électrochoc bourdonnant|officielle|
|[z3tK3Qo5wCvyDf6a.htm](extinction-curse-bestiary-items/z3tK3Qo5wCvyDf6a.htm)|Darkvision|Vision dans le noir|officielle|
|[z49XhuhTdNery690.htm](extinction-curse-bestiary-items/z49XhuhTdNery690.htm)|Trample|Piétinement|officielle|
|[z7LlEkjfkvJpEt7l.htm](extinction-curse-bestiary-items/z7LlEkjfkvJpEt7l.htm)|At-Will Spells|Sorts à volonté|officielle|
|[z7ONYn1k9pLAkFgo.htm](extinction-curse-bestiary-items/z7ONYn1k9pLAkFgo.htm)|War Flail|Fléau d'armes|officielle|
|[z8WlMl1wNjG6WMAp.htm](extinction-curse-bestiary-items/z8WlMl1wNjG6WMAp.htm)|Jaws|Mâchoires|officielle|
|[ZA4AaC9hSaCxwUee.htm](extinction-curse-bestiary-items/ZA4AaC9hSaCxwUee.htm)|Mirror Rejuventation|Reconstruction dans le miroir|officielle|
|[ZAxGEizKqPqUvP2N.htm](extinction-curse-bestiary-items/ZAxGEizKqPqUvP2N.htm)|Composite Longbow|Arc long composite|officielle|
|[zBa9WIu0MHIhcZTg.htm](extinction-curse-bestiary-items/zBa9WIu0MHIhcZTg.htm)|Summon Monster|Convoquer un monstre|officielle|
|[zbf9bW9X1MyCYPvF.htm](extinction-curse-bestiary-items/zbf9bW9X1MyCYPvF.htm)|Club|Gourdin|officielle|
|[zbFv2GQKidMFTpld.htm](extinction-curse-bestiary-items/zbFv2GQKidMFTpld.htm)|Bastard Sword|+1,striking|Épée bâtarde de frappe +1|libre|
|[ZBkyhFcereMKMx5a.htm](extinction-curse-bestiary-items/ZBkyhFcereMKMx5a.htm)|Stench|Puanteur|officielle|
|[ZBRB00Un9P2Q4aVv.htm](extinction-curse-bestiary-items/ZBRB00Un9P2Q4aVv.htm)|Nimble Dodge|Esquive agile|officielle|
|[ZCYqySTC44S2U1ig.htm](extinction-curse-bestiary-items/ZCYqySTC44S2U1ig.htm)|Composite Shortbow|+1,striking|Arc court composite de Frappe +1|libre|
|[zdB58TAmkBBPd6q1.htm](extinction-curse-bestiary-items/zdB58TAmkBBPd6q1.htm)|Scimitar|Cimeterre|officielle|
|[zDG10Ne7cluh9cw0.htm](extinction-curse-bestiary-items/zDG10Ne7cluh9cw0.htm)|Black Cat Curse|Malédiction du chat noir|officielle|
|[zDJDO6RiYek1vtDZ.htm](extinction-curse-bestiary-items/zDJDO6RiYek1vtDZ.htm)|Ectoplasmic Explosion|Déflagration ectoplasmique|officielle|
|[zDrqxe9jDHILx6v0.htm](extinction-curse-bestiary-items/zDrqxe9jDHILx6v0.htm)|Slow|Lent|officielle|
|[ZEVtz1rOhoQ04e8r.htm](extinction-curse-bestiary-items/ZEVtz1rOhoQ04e8r.htm)|Swipe|Frappe transversale|officielle|
|[ZGfedCFrmAmnidZZ.htm](extinction-curse-bestiary-items/ZGfedCFrmAmnidZZ.htm)|Jaws|Mâchoires|officielle|
|[ZgYetoNRvvpabxnK.htm](extinction-curse-bestiary-items/ZgYetoNRvvpabxnK.htm)|Foot|Patte|officielle|
|[ZhPo31BzeCCR8NiY.htm](extinction-curse-bestiary-items/ZhPo31BzeCCR8NiY.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[ZiFwoApOZ8V32uzv.htm](extinction-curse-bestiary-items/ZiFwoApOZ8V32uzv.htm)|Gray Robes|Robe grise|officielle|
|[zIOafMbE3v7YoQZc.htm](extinction-curse-bestiary-items/zIOafMbE3v7YoQZc.htm)|Claw|Griffe|officielle|
|[ZJ9xQpaRJlhEZ2NV.htm](extinction-curse-bestiary-items/ZJ9xQpaRJlhEZ2NV.htm)|Breathe Fire|Souffler du feu|officielle|
|[zJLCfFihvadAJAdk.htm](extinction-curse-bestiary-items/zJLCfFihvadAJAdk.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[zjQ2H1SrpbuuW3al.htm](extinction-curse-bestiary-items/zjQ2H1SrpbuuW3al.htm)|Negative Healing|Guérison négative|officielle|
|[zkM8zASAbYmKxFT4.htm](extinction-curse-bestiary-items/zkM8zASAbYmKxFT4.htm)|Darkvision|Vision dans le noir|officielle|
|[Zlt4tx3zlotDVMMd.htm](extinction-curse-bestiary-items/Zlt4tx3zlotDVMMd.htm)|Dinosaur Lore|Connaissance des dinosaures|officielle|
|[zltQfojhYPzk7OpP.htm](extinction-curse-bestiary-items/zltQfojhYPzk7OpP.htm)|Feather Fall (Self Only)|Feuille morte (soi uniquement)|officielle|
|[ZmHokNmS9yqQ5FIy.htm](extinction-curse-bestiary-items/ZmHokNmS9yqQ5FIy.htm)|Horn|Corne|officielle|
|[ZmpXD5Zs0PQBXPU3.htm](extinction-curse-bestiary-items/ZmpXD5Zs0PQBXPU3.htm)|Swarming Bites|Nuée de morsures|officielle|
|[zNdfAHhn7D5xSrzh.htm](extinction-curse-bestiary-items/zNdfAHhn7D5xSrzh.htm)|Acid Flask|Fiole d'acide|officielle|
|[ZnvHJDLrVZdsku1Y.htm](extinction-curse-bestiary-items/ZnvHJDLrVZdsku1Y.htm)|Thoughtsense (Imprecise) 60 feet|Perception des pensées 18 m (imprécis)|officielle|
|[zP8eq2K9H89vQwFw.htm](extinction-curse-bestiary-items/zP8eq2K9H89vQwFw.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[ZQrY1mv55wut2A9o.htm](extinction-curse-bestiary-items/ZQrY1mv55wut2A9o.htm)|Spring|Lancement|officielle|
|[Zr3FwVvEurlu8nhb.htm](extinction-curse-bestiary-items/Zr3FwVvEurlu8nhb.htm)|Acid Flask (Greater) (Infused)|Fiole d'acide supérieure [Imprégné]|officielle|
|[zrv4X9CqDhIpkMkJ.htm](extinction-curse-bestiary-items/zrv4X9CqDhIpkMkJ.htm)|Shadow Blast (From Heartstone)|Explosion d'ombre (cardioline)|officielle|
|[ztjUvBPpUPyRPb5P.htm](extinction-curse-bestiary-items/ztjUvBPpUPyRPb5P.htm)|Greater Darkvision|Vision dans le noir supérieure|officielle|
|[ztlZ2YqDidf7D2DN.htm](extinction-curse-bestiary-items/ztlZ2YqDidf7D2DN.htm)|Darkvision|Vision dans le noir|officielle|
|[ZUghtcO2JEOGQOAq.htm](extinction-curse-bestiary-items/ZUghtcO2JEOGQOAq.htm)|Tremorsense (Imprecise) 60 feet|Perception des vibrations 18 m (imprécis)|officielle|
|[zUL7K0vd1msnHEUE.htm](extinction-curse-bestiary-items/zUL7K0vd1msnHEUE.htm)|Fist|Poing|officielle|
|[ZV6wTo1kAYTI9DlU.htm](extinction-curse-bestiary-items/ZV6wTo1kAYTI9DlU.htm)|Drunken Rage|Rage d'ivrogne|officielle|
|[zw5dzRRxnfMr1IBO.htm](extinction-curse-bestiary-items/zw5dzRRxnfMr1IBO.htm)|Darkvision|Vision dans le noir|officielle|
|[ZwdP3qwfYd4W0BZ7.htm](extinction-curse-bestiary-items/ZwdP3qwfYd4W0BZ7.htm)|Low-Light Vision|Vision nocturne|officielle|
|[zwkgV9sWG9MJ5GIJ.htm](extinction-curse-bestiary-items/zwkgV9sWG9MJ5GIJ.htm)|Foot|Patte|officielle|
|[ZwS0gU5ghEmq2cOB.htm](extinction-curse-bestiary-items/ZwS0gU5ghEmq2cOB.htm)|Composite Longbow|Arc long composite|officielle|
|[ZwZxW282cbsJVc4E.htm](extinction-curse-bestiary-items/ZwZxW282cbsJVc4E.htm)|Dueling Parry|Parade en duel|officielle|
|[zXdU4CPFolugmDo2.htm](extinction-curse-bestiary-items/zXdU4CPFolugmDo2.htm)|Infused Items|Objets imprégnés|officielle|
|[ZXW62ilDNKxEYwX1.htm](extinction-curse-bestiary-items/ZXW62ilDNKxEYwX1.htm)|Athletics|Athlétisme|officielle|
|[zY6dnugGXHyrRplD.htm](extinction-curse-bestiary-items/zY6dnugGXHyrRplD.htm)|Nimble Opportunist|Opportuniste vif|officielle|
|[ZyHfjNED06IY0URq.htm](extinction-curse-bestiary-items/ZyHfjNED06IY0URq.htm)|Greater Darkvision|Vision dans le noir supérieure|officielle|
|[ZZdTJTrqMUiQj1UM.htm](extinction-curse-bestiary-items/ZZdTJTrqMUiQj1UM.htm)|Staff|Bâton|officielle|
