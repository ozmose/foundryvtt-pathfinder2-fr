# État de la traduction (campaign-effects)

 * **libre**: 21
 * **aucune**: 3


Dernière mise à jour: 2023-07-12 20:28 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[DOxl0FDd21VMDOMP.htm](campaign-effects/DOxl0FDd21VMDOMP.htm)|Effect: Reflection of Life (Fast Healing)|
|[lZ4DA81o4kbL8nve.htm](campaign-effects/lZ4DA81o4kbL8nve.htm)|Effect: Burned Tongue (Linguistic)|
|[XOE1gp3phFyxL4Dq.htm](campaign-effects/XOE1gp3phFyxL4Dq.htm)|Effect: Burned Tongue (No Singing or Yelling)|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[4vnF6BFM4Xg4Eg0k.htm](campaign-effects/4vnF6BFM4Xg4Eg0k.htm)|Effect: Improved reflexes|Effet : Réflexes améliorés|libre|
|[5DBYunGL2HV8aekO.htm](campaign-effects/5DBYunGL2HV8aekO.htm)|Resonant Reflection: Reflection of Stone|Reflet retentissant : Reflet de pierre|libre|
|[8eMGYNilWV3dFAUI.htm](campaign-effects/8eMGYNilWV3dFAUI.htm)|Resonant Reflection: Reflection of Life|Reflet retentissant : Reflet de vie|libre|
|[cESIlNrgjo1uW71D.htm](campaign-effects/cESIlNrgjo1uW71D.htm)|Mixed Drink: Guidance|Cocktail : Assistance divine|libre|
|[CS1XbH7GYfP6Ve61.htm](campaign-effects/CS1XbH7GYfP6Ve61.htm)|Effect: Burned Mouth and Throat (No Singing or Yelling)|Effet : Gorge et langue brulées (Chanter ou Crier)|libre|
|[e8K0YTKxzf0bhayh.htm](campaign-effects/e8K0YTKxzf0bhayh.htm)|Mixed Drink: Luck|Cocktail : Chance|libre|
|[j8hA7CsmSY90tBqw.htm](campaign-effects/j8hA7CsmSY90tBqw.htm)|Mixed Drink: Prestidigitation|Cocktail : Prestidigitation|libre|
|[JnQi8whNqY7sPW7x.htm](campaign-effects/JnQi8whNqY7sPW7x.htm)|Mixed Drink: Dancing Lights|Cocktail : Lumières dansantes|libre|
|[Luti1qjFOKYwZio0.htm](campaign-effects/Luti1qjFOKYwZio0.htm)|Effect: Extreme stomach cramps|Effet : crampes d'estomac intenses|libre|
|[meTIFa2VsIzRVywE.htm](campaign-effects/meTIFa2VsIzRVywE.htm)|Effect: Hope or Despair (Critical Success)|Effet : Espoir ou Désespoir (Succès critique)|libre|
|[NwF6m7SyaqwjdQxT.htm](campaign-effects/NwF6m7SyaqwjdQxT.htm)|Effect: Manipulate Luck - Drusilla (Good)|Effet : Manipulation de la chance - Drusilla (Bonne)|libre|
|[nwFhhgZRggDoDgdk.htm](campaign-effects/nwFhhgZRggDoDgdk.htm)|Effect: Burned Mouth and Throat (Linguistic)|Effet : Langue et gorge brûlées (linguistique)|libre|
|[Px7sSipQxHdOMSjk.htm](campaign-effects/Px7sSipQxHdOMSjk.htm)|Effect: Hope or Despair (Failure or Critical Failure)|Effet : Espoir ou désespoir (Échec ou Échec critique)|libre|
|[QWW4hNCtLkOK1k71.htm](campaign-effects/QWW4hNCtLkOK1k71.htm)|Effect: Immediate and Intense Headache|Effet: Mal de tête immédiat et intense|libre|
|[Rz35d02dUtAjKtMB.htm](campaign-effects/Rz35d02dUtAjKtMB.htm)|Effect: Heightened awareness|Effet : Perception augmentée|libre|
|[scgEAXAqHEr3Egeb.htm](campaign-effects/scgEAXAqHEr3Egeb.htm)|Resonant Reflection: Reflection of Water|Reflet retentissant : reflet d'eau|libre|
|[SvR7Ez1lfnN4You5.htm](campaign-effects/SvR7Ez1lfnN4You5.htm)|Geb's Blessing|Bénédiction de Geb|libre|
|[uDQw7YPMiKPXCbaV.htm](campaign-effects/uDQw7YPMiKPXCbaV.htm)|Resonant Reflection: Reflection of Light|Reflet retentissant : Reflet de lumière|libre|
|[UXBOvlJbnI76UoGp.htm](campaign-effects/UXBOvlJbnI76UoGp.htm)|Resonant Reflection: Reflection of Storm|Reflet retentissant : Reflet de tempête|libre|
|[W8MD8WCjVC4pLlqn.htm](campaign-effects/W8MD8WCjVC4pLlqn.htm)|Effect: Heart Break|Effet : Coeur brisé|libre|
|[yJWWTfZkAF4raa4R.htm](campaign-effects/yJWWTfZkAF4raa4R.htm)|Effect: Keen insight|Effet : Perception affûtée|libre|
