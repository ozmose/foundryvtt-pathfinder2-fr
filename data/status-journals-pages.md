# État de la traduction (journals-pages)

 * **libre**: 171
 * **changé**: 107
 * **aucune**: 62
 * **officielle**: 5


Dernière mise à jour: 2023-07-12 20:28 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[3H168yyaP6ze75kp.htm](journals-pages/3H168yyaP6ze75kp.htm)|Inspiring Relationships|
|[6xnJaS3qRZGdDHtr.htm](journals-pages/6xnJaS3qRZGdDHtr.htm)|Roll Back|
|[7uERHpfJplGtYf8w.htm](journals-pages/7uERHpfJplGtYf8w.htm)|Aura of Protection|
|[8NHTJ8kK9qCzOVxp.htm](journals-pages/8NHTJ8kK9qCzOVxp.htm)|Battle Cry|
|[8w220Bvj6W2Ui8Ae.htm](journals-pages/8w220Bvj6W2Ui8Ae.htm)|Surge of Speed|
|[a68Phr1qsqVRPMVp.htm](journals-pages/a68Phr1qsqVRPMVp.htm)|Class Might|
|[AeGcoQNaZ1BUmWvu.htm](journals-pages/AeGcoQNaZ1BUmWvu.htm)|Last Second Sidestep|
|[bAIAWQ7q3ycIAnoj.htm](journals-pages/bAIAWQ7q3ycIAnoj.htm)|Relationships|
|[bb4nv44qEdpt0pLq.htm](journals-pages/bb4nv44qEdpt0pLq.htm)|Stoke the Magical Flame|
|[BHn7nSm4BMPh9QoZ.htm](journals-pages/BHn7nSm4BMPh9QoZ.htm)|Using Deep Backgrounds|
|[c8psqIuH4YFi6msK.htm](journals-pages/c8psqIuH4YFi6msK.htm)|Family Background|
|[cQH9Syl1SQpbAi5A.htm](journals-pages/cQH9Syl1SQpbAi5A.htm)|Rage and Fury|
|[d15Zjdqplmj7ZTKK.htm](journals-pages/d15Zjdqplmj7ZTKK.htm)|Desperate Swing|
|[D6hqfmZUksRvqNFR.htm](journals-pages/D6hqfmZUksRvqNFR.htm)|Fluid Motion|
|[dCd0YXLVx0DQwitm.htm](journals-pages/dCd0YXLVx0DQwitm.htm)|Tuck and Roll|
|[dpL9RyApn4y7jJ3y.htm](journals-pages/dpL9RyApn4y7jJ3y.htm)|Homeland|
|[E4aI0BTV9ATxfjGd.htm](journals-pages/E4aI0BTV9ATxfjGd.htm)|Reckless Charge|
|[EGa7oNFMSLkvkQjL.htm](journals-pages/EGa7oNFMSLkvkQjL.htm)|Flash of Insight|
|[eZUSi5q5NnEvCDEk.htm](journals-pages/eZUSi5q5NnEvCDEk.htm)|Cut Through the Fog|
|[EZZxz9jeEB0N3FPZ.htm](journals-pages/EZZxz9jeEB0N3FPZ.htm)|Spark of Courage|
|[FgA086C40VR9mzNr.htm](journals-pages/FgA086C40VR9mzNr.htm)|Hold the Line|
|[Fq1KEUyv0zBp5nMV.htm](journals-pages/Fq1KEUyv0zBp5nMV.htm)|Last Ounce of Strength|
|[ftdrUGjW8A4TPkMa.htm](journals-pages/ftdrUGjW8A4TPkMa.htm)|Protect the Innocent|
|[g5G4unm9tbEDu3pZ.htm](journals-pages/g5G4unm9tbEDu3pZ.htm)|Hasty Block|
|[gmwyfTlk0BGZLdCt.htm](journals-pages/gmwyfTlk0BGZLdCt.htm)|Magical Reverberation|
|[GqZLaGQDNe41saZ7.htm](journals-pages/GqZLaGQDNe41saZ7.htm)|Misdirected Attack|
|[Gu6CHAPMigpV9awj.htm](journals-pages/Gu6CHAPMigpV9awj.htm)|Tumble Through|
|[IQ56rAleMRc53Lcv.htm](journals-pages/IQ56rAleMRc53Lcv.htm)|Major Childhood Event|
|[j6hsF2TWFtGMyClW.htm](journals-pages/j6hsF2TWFtGMyClW.htm)|Shoot Through|
|[jHn1m1r95YgMQSvM.htm](journals-pages/jHn1m1r95YgMQSvM.htm)|Daring Attempt|
|[jIs5mtRaqG0aGQ8u.htm](journals-pages/jIs5mtRaqG0aGQ8u.htm)|Make Way!|
|[jMhD1Z6aagYAYCnb.htm](journals-pages/jMhD1Z6aagYAYCnb.htm)|Last Stand|
|[k0ue81dFKH4OKxed.htm](journals-pages/k0ue81dFKH4OKxed.htm)|Impossible Shot|
|[ksg8D5ssP6WGwGqi.htm](journals-pages/ksg8D5ssP6WGwGqi.htm)|Opportune Distraction|
|[LafJ1PXfnR4Ogyzh.htm](journals-pages/LafJ1PXfnR4Ogyzh.htm)|Catch your Breath|
|[LaNM3BfoZUG2B39v.htm](journals-pages/LaNM3BfoZUG2B39v.htm)|Warding Sign|
|[lJe2uRM1wNJw4Mgr.htm](journals-pages/lJe2uRM1wNJw4Mgr.htm)|Push Through the Pain|
|[lLdAvaOo1rLLg5b4.htm](journals-pages/lLdAvaOo1rLLg5b4.htm)|Pierce Resistance|
|[MSdKfjoNBj7PjEoo.htm](journals-pages/MSdKfjoNBj7PjEoo.htm)|Grazing Blow|
|[O2hME2ilgN9BcEA4.htm](journals-pages/O2hME2ilgN9BcEA4.htm)|Dive Out of Danger|
|[oa0wVDfT3dbWwNDf.htm](journals-pages/oa0wVDfT3dbWwNDf.htm)|Surge of Magic|
|[oAfVg9t7GGTW7R1H.htm](journals-pages/oAfVg9t7GGTW7R1H.htm)|Challenging Relationships|
|[odrMigI38k7lgurE.htm](journals-pages/odrMigI38k7lgurE.htm)|Channel Life Force|
|[OnB94Y7wYE9j0ict.htm](journals-pages/OnB94Y7wYE9j0ict.htm)|I Hope This Works|
|[ox9mFGY50NEzRR12.htm](journals-pages/ox9mFGY50NEzRR12.htm)|Reverse Strike|
|[P3mSUWRvCCwEouB0.htm](journals-pages/P3mSUWRvCCwEouB0.htm)|Critical Moment|
|[pS87Zh4QJnu0p8ES.htm](journals-pages/pS87Zh4QJnu0p8ES.htm)|Strike True|
|[qjnUXickBOBDBu2N.htm](journals-pages/qjnUXickBOBDBu2N.htm)|Introspection Domain|
|[QUhXVsUDujUTYz7F.htm](journals-pages/QUhXVsUDujUTYz7F.htm)|Stay in the Fight|
|[quxPxuMub8k6abzN.htm](journals-pages/quxPxuMub8k6abzN.htm)|Ancestral Might|
|[QWw0D2YPKdz6HJPu.htm](journals-pages/QWw0D2YPKdz6HJPu.htm)|Influential Associate|
|[spzRl5CS4vnvcrm5.htm](journals-pages/spzRl5CS4vnvcrm5.htm)|Called Foe|
|[Ttn2H0JHJVksChGi.htm](journals-pages/Ttn2H0JHJVksChGi.htm)|Healing Prayer|
|[TU21DvdpQG7U2Q9Y.htm](journals-pages/TU21DvdpQG7U2Q9Y.htm)|Endure the Onslaught|
|[tUu3x2iHtOUqqWUc.htm](journals-pages/tUu3x2iHtOUqqWUc.htm)|Distract Foe|
|[ukLpSqTkmRLvZj0z.htm](journals-pages/ukLpSqTkmRLvZj0z.htm)|Stalwart Defender|
|[VSZGUtcr5NGNPtlB.htm](journals-pages/VSZGUtcr5NGNPtlB.htm)|Rampage|
|[XHSXMcRaMOsbjoO1.htm](journals-pages/XHSXMcRaMOsbjoO1.htm)|Rending Swipe|
|[xsdJIpd9NRLpRZT2.htm](journals-pages/xsdJIpd9NRLpRZT2.htm)|Drain Power|
|[Y20GMjqVbTvGZ87k.htm](journals-pages/Y20GMjqVbTvGZ87k.htm)|Shake it Off|
|[zcBCxlvPCpmjt5IS.htm](journals-pages/zcBCxlvPCpmjt5IS.htm)|Press On|
|[zX28AGU1k5sO2Tpd.htm](journals-pages/zX28AGU1k5sO2Tpd.htm)|Run and Shoot|

## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0e2o62hyCxbIkVoC.htm](journals-pages/0e2o62hyCxbIkVoC.htm)|Assassin|Assassin|changé|
|[0RnXjFday2Lp4ECG.htm](journals-pages/0RnXjFday2Lp4ECG.htm)|Sample Chases|Exemples d'obstacles|changé|
|[0ZXiLwvBG20MzkO6.htm](journals-pages/0ZXiLwvBG20MzkO6.htm)|Scrollmaster|Maître des parchemins|changé|
|[2CbiV0VtDKZfQNte.htm](journals-pages/2CbiV0VtDKZfQNte.htm)|Viking|Viking|changé|
|[3a9kFb9x9UXeLA1s.htm](journals-pages/3a9kFb9x9UXeLA1s.htm)|Quick Environmental Details|Détails environnementaux|changé|
|[4VMzG36Pm1oivS6Y.htm](journals-pages/4VMzG36Pm1oivS6Y.htm)|Beastmaster|Maître des bêtes|changé|
|[4YtHynO8i1a33zo9.htm](journals-pages/4YtHynO8i1a33zo9.htm)|Flexible Spellcaster (Class Archetype)|Incantateur flexible (Archétype de classe)|changé|
|[6A9zBmTyt8cn6ioa.htm](journals-pages/6A9zBmTyt8cn6ioa.htm)|Eldritch Archer|Archer mystique|changé|
|[6bDpXy7pQdGrd2og.htm](journals-pages/6bDpXy7pQdGrd2og.htm)|Star Domain|Domaine des Étoiles|changé|
|[6BXJUVvwvzljaCFT.htm](journals-pages/6BXJUVvwvzljaCFT.htm)|Armor Traits|Traits d'armure|changé|
|[6DgCLFEGEHvqmy7Y.htm](journals-pages/6DgCLFEGEHvqmy7Y.htm)|Clockwork Reanimator|Réanimateur nécromécanicien|changé|
|[6KciIDJV6ZdJcAVa.htm](journals-pages/6KciIDJV6ZdJcAVa.htm)|Sterling Dynamo|Dynamo sterling|changé|
|[8RYKz1WDPMJBmMNt.htm](journals-pages/8RYKz1WDPMJBmMNt.htm)|Game Hunter|Chasseur de gros gibier|changé|
|[9hfEG4kJmEbrjGS1.htm](journals-pages/9hfEG4kJmEbrjGS1.htm)|Encounter Building & XP Awards|Bâtir une rencontre & Récompenses en PX|changé|
|[aBALUjLyOqXKlhrP.htm](journals-pages/aBALUjLyOqXKlhrP.htm)|Earn Income|Gagner de l'argent|changé|
|[ackdHdIAmSb0AiVL.htm](journals-pages/ackdHdIAmSb0AiVL.htm)|Loremaster|Maître savant|changé|
|[act12TfHpnKx6ZTu.htm](journals-pages/act12TfHpnKx6ZTu.htm)|Different Size Items|Objets de différente taille|changé|
|[b7oePIMEMdFFS5TH.htm](journals-pages/b7oePIMEMdFFS5TH.htm)|Captivator|Enjôleur|changé|
|[BsaTOw6p74DW5d8t.htm](journals-pages/BsaTOw6p74DW5d8t.htm)|Wand Prices|Prix des baguettes|changé|
|[BwPUY2X7TSmlJj5c.htm](journals-pages/BwPUY2X7TSmlJj5c.htm)|Duelist|Duelliste|changé|
|[C3P0TycdFCN02p9u.htm](journals-pages/C3P0TycdFCN02p9u.htm)|Bastion|Bastion|changé|
|[C4b2AvMpVpe5BcVv.htm](journals-pages/C4b2AvMpVpe5BcVv.htm)|Material Hardness, Hit Points and Broken Threshold|Solidité du matériau, Points de vie et Seuil de rupture|changé|
|[cjD6YVPQpmYiOPdC.htm](journals-pages/cjD6YVPQpmYiOPdC.htm)|Terrain in Encounters|Terrain dans les rencontres|changé|
|[DAVBjDSysgXgtVQu.htm](journals-pages/DAVBjDSysgXgtVQu.htm)|Gray Gardener|Jardinier gris|changé|
|[DBxoE3TNDPtcKHxH.htm](journals-pages/DBxoE3TNDPtcKHxH.htm)|Animals & Barding|Animaux & bardes|changé|
|[DJiYP5tFBrBMD0We.htm](journals-pages/DJiYP5tFBrBMD0We.htm)|Sorcerer|Ensorceleur|changé|
|[dnbUao9yk8Bs99eT.htm](journals-pages/dnbUao9yk8Bs99eT.htm)|Weapon Traits|Traits d'arme|changé|
|[drgCQcXZbIJU0Zhw.htm](journals-pages/drgCQcXZbIJU0Zhw.htm)|Wrestler|Lutteur|changé|
|[dRu66xNqJ9Ihe1or.htm](journals-pages/dRu66xNqJ9Ihe1or.htm)|Difficulty Classes|Degrés de difficulté|changé|
|[DUDsA3sIlOlibvIu.htm](journals-pages/DUDsA3sIlOlibvIu.htm)|High-Quality Items|Objets de haute qualité|changé|
|[DXtjeVaWNB8zSjpA.htm](journals-pages/DXtjeVaWNB8zSjpA.htm)|Champion|Champion|changé|
|[DztQF2FexWvnzcaE.htm](journals-pages/DztQF2FexWvnzcaE.htm)|Spell Trickster|Mystificateur de sort|changé|
|[Edn6qQNVcEVpkhPl.htm](journals-pages/Edn6qQNVcEVpkhPl.htm)|Detecting Creatures|Détecter des créatures|changé|
|[EWiUv3UiR3RHSGlA.htm](journals-pages/EWiUv3UiR3RHSGlA.htm)|Cathartic Mage|Mage cathartique|changé|
|[EwlYs1OzaMj9BB5I.htm](journals-pages/EwlYs1OzaMj9BB5I.htm)|Student of Perfection|Étudiant de la perfection|changé|
|[F5gBiVGbS8YSgPSI.htm](journals-pages/F5gBiVGbS8YSgPSI.htm)|Sixth Pillar|Sixième pilier|changé|
|[Fea8ZereQhNolDoP.htm](journals-pages/Fea8ZereQhNolDoP.htm)|Bounty Hunter|Chasseur de primes|changé|
|[FJGPBhYmD7xTFsrW.htm](journals-pages/FJGPBhYmD7xTFsrW.htm)|Spellshot (Class Archetype)|Sortiléro (Archétype de classe)|changé|
|[FOI43M8DJe2lkMwl.htm](journals-pages/FOI43M8DJe2lkMwl.htm)|Structures|Structures|changé|
|[fTxZjmNLckRwZ4Po.htm](journals-pages/fTxZjmNLckRwZ4Po.htm)|Pathfinder Agent|Agent des Éclaireurs|changé|
|[FzzMt7ybWy03D9qZ.htm](journals-pages/FzzMt7ybWy03D9qZ.htm)|Basic Services|Services et consommables basiques|changé|
|[G38usCLuTDmFYw7V.htm](journals-pages/G38usCLuTDmFYw7V.htm)|Zombie|Zombie|changé|
|[G3AtnAkIKNHrahmd.htm](journals-pages/G3AtnAkIKNHrahmd.htm)|Vampire|Vampire|changé|
|[G9Fzy5ZK4KtAmcFb.htm](journals-pages/G9Fzy5ZK4KtAmcFb.htm)|Snarecrafter|Fabricant de pièges artisanaux|changé|
|[gbBf6x89m4SEFpsL.htm](journals-pages/gbBf6x89m4SEFpsL.htm)|Druid|Druide|changé|
|[gE3x4USaU4VWdwKD.htm](journals-pages/gE3x4USaU4VWdwKD.htm)|Gamemastery|Maîtrise du jeu|changé|
|[hOsFg2VpnOshaSOi.htm](journals-pages/hOsFg2VpnOshaSOi.htm)|Environmental Damage|Dégâts environnementaux|changé|
|[HvbDEgCsLbzuMRiR.htm](journals-pages/HvbDEgCsLbzuMRiR.htm)|Poisoner|Empoisonneur|changé|
|[HZHzpATOyhTjUDrR.htm](journals-pages/HZHzpATOyhTjUDrR.htm)|Alter Ego|Imposteur|changé|
|[iH3Vrt35DyUbhxZj.htm](journals-pages/iH3Vrt35DyUbhxZj.htm)|Mammoth Lord|Seigneur des mammouths|changé|
|[INYPqM7rs9SNFRba.htm](journals-pages/INYPqM7rs9SNFRba.htm)|Downtime Events|Évènements d'intermèdes|changé|
|[iVY32tNvfS1tPYU2.htm](journals-pages/iVY32tNvfS1tPYU2.htm)|Formula Price|Prix des formules|changé|
|[j5uZbCwoHOOEO1bC.htm](journals-pages/j5uZbCwoHOOEO1bC.htm)|Runescarred|Scarifié de runes|changé|
|[JzUDumJ3Dlyame4z.htm](journals-pages/JzUDumJ3Dlyame4z.htm)|Drow Shootist|Tireur drow|changé|
|[kAMseeY0TewNmnLQ.htm](journals-pages/kAMseeY0TewNmnLQ.htm)|Deep Backgrounds|Historiques approfondis|changé|
|[KRUWPDzuwG0LC26d.htm](journals-pages/KRUWPDzuwG0LC26d.htm)|Curse Maelstrom|Maelström maudit|changé|
|[kVC4kgYKbhqPsaDt.htm](journals-pages/kVC4kgYKbhqPsaDt.htm)|Rogue|Roublard|changé|
|[KXAxZfdMpnHQZQoc.htm](journals-pages/KXAxZfdMpnHQZQoc.htm)|Golarion Calendar|Calendrier de Golarion|changé|
|[kxDloJH5ImKnIV9P.htm](journals-pages/kxDloJH5ImKnIV9P.htm)|Temperature Effects|Effets de la température|changé|
|[lcCPztCqmXpwdmWx.htm](journals-pages/lcCPztCqmXpwdmWx.htm)|Elite/Weak Adjustments|Ajustements de créatures Élite/Faible|changé|
|[LgkfmZnFP3TWtkuy.htm](journals-pages/LgkfmZnFP3TWtkuy.htm)|Living Vessel|Réceptacle vivant|changé|
|[lQ9MmX3zg4Bd9hjb.htm](journals-pages/lQ9MmX3zg4Bd9hjb.htm)|GM Screen|Écran de base MJ|changé|
|[Ly8l2GT6dbvuY3A2.htm](journals-pages/Ly8l2GT6dbvuY3A2.htm)|Treasure|Trésors (Niveau)|changé|
|[M5QOocGyP4zkMo9m.htm](journals-pages/M5QOocGyP4zkMo9m.htm)|Horizon Walker|Arpenteur d'horizon|changé|
|[MKU4d2hIpLuXGN2J.htm](journals-pages/MKU4d2hIpLuXGN2J.htm)|Shadowdancer|Danseur de l'ombre|changé|
|[mmB3EkkdCpLke7Lk.htm](journals-pages/mmB3EkkdCpLke7Lk.htm)|Investigator|Enquêteur|changé|
|[mmfWcV3Iyql5nzTo.htm](journals-pages/mmfWcV3Iyql5nzTo.htm)|Counteract|Contrer|changé|
|[N01RLMQPaSeoHEuL.htm](journals-pages/N01RLMQPaSeoHEuL.htm)|Martial Artist|Artiste martial|changé|
|[ngVnNmi1Qke3lTy0.htm](journals-pages/ngVnNmi1Qke3lTy0.htm)|Oracle|Oracle|changé|
|[o71hqcfzhCKXcSml.htm](journals-pages/o71hqcfzhCKXcSml.htm)|Archer|Archer|changé|
|[O79hOcsaQyj3aQC5.htm](journals-pages/O79hOcsaQyj3aQC5.htm)|Archaeologist|Archéologue|changé|
|[oBOw6E6V7ncrB1rN.htm](journals-pages/oBOw6E6V7ncrB1rN.htm)|Treasure Per Encounter|Trésor par niveau de rencontre|changé|
|[oM0DobZe9uEkz9kb.htm](journals-pages/oM0DobZe9uEkz9kb.htm)|Force Open|Ouvrir de force|changé|
|[oMIA3aFKvpuV9f2H.htm](journals-pages/oMIA3aFKvpuV9f2H.htm)|Elementalist (Class Archetype)|Élémentaliste (Archétype de classe)|changé|
|[OtBPlgtudM4PlTWD.htm](journals-pages/OtBPlgtudM4PlTWD.htm)|Harrower|Liseur de Tourment|changé|
|[PVrMSF93KIejBTMm.htm](journals-pages/PVrMSF93KIejBTMm.htm)|Critical Specialization Effects|Effets critique spécialisé|changé|
|[qMNBD91gc5kIKPs2.htm](journals-pages/qMNBD91gc5kIKPs2.htm)|Spellcasting Services|Services d'incantation|changé|
|[QSk78hQR3zskMlq2.htm](journals-pages/QSk78hQR3zskMlq2.htm)|Cities Domain|Domaine des Villes|changé|
|[rhDvoOHAhAlABiae.htm](journals-pages/rhDvoOHAhAlABiae.htm)|Acrobat|Acrobate|changé|
|[rtAFOZOjWHC2zRNO.htm](journals-pages/rtAFOZOjWHC2zRNO.htm)|Vigilante|Justicier|changé|
|[sAFWD8D0RKH4m25n.htm](journals-pages/sAFWD8D0RKH4m25n.htm)|Dual-Weapon Warrior|Combattant à deux armes|changé|
|[SAnmegCTIqGW9S7S.htm](journals-pages/SAnmegCTIqGW9S7S.htm)|Family Domain|Domaine de la Famille|changé|
|[SiMXtLf4YQeigzIE.htm](journals-pages/SiMXtLf4YQeigzIE.htm)|Senses|Sens|changé|
|[SnSGU7DPiogZ9JOr.htm](journals-pages/SnSGU7DPiogZ9JOr.htm)|Automatic Bonus Progression|Progression automatique des bonus|changé|
|[SpWLU1ixQRT6rkUP.htm](journals-pages/SpWLU1ixQRT6rkUP.htm)|Downtime Tasks|Tâches d'Intermède|changé|
|[ts7gtyAhDEI4V7Ve.htm](journals-pages/ts7gtyAhDEI4V7Ve.htm)|Character Wealth|Richesse du personnage|changé|
|[ttkG4geKXTao5pku.htm](journals-pages/ttkG4geKXTao5pku.htm)|Alkenstar Agent|Agent d'Alkenastre|changé|
|[uWJfFysCcoU1o2Hs.htm](journals-pages/uWJfFysCcoU1o2Hs.htm)|Skill Actions|Actions de compétence|changé|
|[uxdPLsxY8fNrenDR.htm](journals-pages/uxdPLsxY8fNrenDR.htm)|Death and Dying|Mort et l'état Mourant|changé|
|[uYCTmos3yZtbD9qs.htm](journals-pages/uYCTmos3yZtbD9qs.htm)|Barbarian|Barbare|changé|
|[VnR8gBBq1oCG7YQh.htm](journals-pages/VnR8gBBq1oCG7YQh.htm)|Reputation|Réputation|changé|
|[VR9UdfhZphZGZdsx.htm](journals-pages/VR9UdfhZphZGZdsx.htm)|Bulk Conversions for Different Sizes|Conversions d'encombrement pour des tailles différentes|changé|
|[wMUAm1PJ1HjJ8fFU.htm](journals-pages/wMUAm1PJ1HjJ8fFU.htm)|Pistol Phenom|Phénomène du pistolet|changé|
|[WvleaXPfigawBvtN.htm](journals-pages/WvleaXPfigawBvtN.htm)|Exorcist|Exorciste|changé|
|[XCxK6OGFY0KvJPo3.htm](journals-pages/XCxK6OGFY0KvJPo3.htm)|Learn a Spell|Apprendre un sort|changé|
|[XH3xabCB01FiCRal.htm](journals-pages/XH3xabCB01FiCRal.htm)|Proficiency Without Level|Maîtrise sans niveau|changé|
|[Y3DFBCWiM9GBIlfl.htm](journals-pages/Y3DFBCWiM9GBIlfl.htm)|Moon Domain|Domaine de la Lune|changé|
|[yaR8E2drX6pFUFCK.htm](journals-pages/yaR8E2drX6pFUFCK.htm)|Familiar Master|Maître familier|changé|
|[ydbCjJ9PPmRzZhDN.htm](journals-pages/ydbCjJ9PPmRzZhDN.htm)|Creation Domain|Domaine de la Création|changé|
|[ydELQFQq0lGLTvrA.htm](journals-pages/ydELQFQq0lGLTvrA.htm)|Time Mage|Mage du temps|changé|
|[Yepac621abWWw5qj.htm](journals-pages/Yepac621abWWw5qj.htm)|Monster Abilities|Capacités des monstre|changé|
|[ygboVjCAFRpcysUb.htm](journals-pages/ygboVjCAFRpcysUb.htm)|Player Screen|Écran du joueur|changé|
|[yHqDPytVY9bxWo9I.htm](journals-pages/yHqDPytVY9bxWo9I.htm)|Item Quirks|Bizarreries des objets|changé|
|[YVLMgNxXTUQuDJgp.htm](journals-pages/YVLMgNxXTUQuDJgp.htm)|Travel Speed|Vitesse de voyage|changé|
|[Z2xXPHkxGLFTXzdI.htm](journals-pages/Z2xXPHkxGLFTXzdI.htm)|Creature Identification|Identification d'une créature|changé|
|[zVcfWP1ac0JhNEhY.htm](journals-pages/zVcfWP1ac0JhNEhY.htm)|Quick Adventure Groups|Groupes d'aventure rapide|changé|
|[zymZw8KQe0nkRf5L.htm](journals-pages/zymZw8KQe0nkRf5L.htm)|Victory Points|Points de victoire|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[09VwtSsYldMkVzU1.htm](journals-pages/09VwtSsYldMkVzU1.htm)|Runelord|Seigneur des runes (Archétype de classe)|libre|
|[0GwpYEjCHWyfQvgg.htm](journals-pages/0GwpYEjCHWyfQvgg.htm)|Knowledge Domain|Domaine du Savoir|libre|
|[0pPtVXH6Duitakbp.htm](journals-pages/0pPtVXH6Duitakbp.htm)|Folklorist|Folkloriste|libre|
|[0UrqPv7XLDDRwZ13.htm](journals-pages/0UrqPv7XLDDRwZ13.htm)|Pactbinder|Pactiseur|libre|
|[0wCEUwABKPdKPj8e.htm](journals-pages/0wCEUwABKPdKPj8e.htm)|Dreams Domain|Domaine des Rêves|libre|
|[1F84oh5hoG86mZIw.htm](journals-pages/1F84oh5hoG86mZIw.htm)|Marshal|Capitaine|libre|
|[3B1PqqSZYQj9YXac.htm](journals-pages/3B1PqqSZYQj9YXac.htm)|Hallowed Necromancer|Nécromant sacré|libre|
|[3P0NWwP3s7bIiidH.htm](journals-pages/3P0NWwP3s7bIiidH.htm)|Time Domain|Domaine du Temps|libre|
|[4gKrDFB1GlILn9la.htm](journals-pages/4gKrDFB1GlILn9la.htm)|Scroll Trickster|Usurpateur de parchemins|libre|
|[4ZPZqbe0Ai8ZIfcp.htm](journals-pages/4ZPZqbe0Ai8ZIfcp.htm)|Golden League Xun|Xun de la Ligue dorée|libre|
|[57rElfZ9sWKFQ8Ep.htm](journals-pages/57rElfZ9sWKFQ8Ep.htm)|Knight Vigilant|Chevalier vigilant|libre|
|[5MjSsuKOLBoiL8FB.htm](journals-pages/5MjSsuKOLBoiL8FB.htm)|Freedom Domain|Domaine de la Liberté|libre|
|[5nd8ON2kFGOsCdwQ.htm](journals-pages/5nd8ON2kFGOsCdwQ.htm)|Bullet Dancer|Danseur de balle|libre|
|[5RH1CkZQHYpsOium.htm](journals-pages/5RH1CkZQHYpsOium.htm)|Edgewatch Detective|Officier de la garde du précipice|libre|
|[5TqEbLR9QT3gJGe3.htm](journals-pages/5TqEbLR9QT3gJGe3.htm)|Sorrow Domain|Domaine du Chagrin|libre|
|[5v7k1XWQxaP0DoGX.htm](journals-pages/5v7k1XWQxaP0DoGX.htm)|Monk|Moine|libre|
|[6qTjtFWaBO5b60zJ.htm](journals-pages/6qTjtFWaBO5b60zJ.htm)|Dust Domain|Domaine de la poussière|libre|
|[798PFdS8FmefcOl0.htm](journals-pages/798PFdS8FmefcOl0.htm)|Death Domain|Domaine de la Mort|libre|
|[7FsXXkrOUXHtROnq.htm](journals-pages/7FsXXkrOUXHtROnq.htm)|Falling|Chuter|libre|
|[7jQ2x83KUFD8Wj6w.htm](journals-pages/7jQ2x83KUFD8Wj6w.htm)|Light Effects|Effets de lumière|libre|
|[7xrNAgAnBqBgE3yM.htm](journals-pages/7xrNAgAnBqBgE3yM.htm)|Change Domain|Domaine du Changement|libre|
|[8qYJAx4O97sSxbUF.htm](journals-pages/8qYJAx4O97sSxbUF.htm)|Basic Actions|Actions basiques|libre|
|[9g14DjBZNj27goTt.htm](journals-pages/9g14DjBZNj27goTt.htm)|Soul Warden|Gardien de l'âme|libre|
|[9g1dNytABTpmmGkG.htm](journals-pages/9g1dNytABTpmmGkG.htm)|Glyph Domain|Domaine des Glyphes|libre|
|[a3gPiGqSiWCZ6kGl.htm](journals-pages/a3gPiGqSiWCZ6kGl.htm)|Worldbuilding|Construction d'univers|libre|
|[A5HwvubNii1d2UND.htm](journals-pages/A5HwvubNii1d2UND.htm)|Sniping Duo|Duo de tireur d'élite|libre|
|[A7vErdGAweYsFcW8.htm](journals-pages/A7vErdGAweYsFcW8.htm)|Healing Domain|Domaine de la Guérison|libre|
|[aAm6jY2k5qBuWETd.htm](journals-pages/aAm6jY2k5qBuWETd.htm)|Inventor|Inventeur|libre|
|[aIpIUbupTjw2863C.htm](journals-pages/aIpIUbupTjw2863C.htm)|Ghost|Fantôme|libre|
|[ajCEExOaxuB4C1tY.htm](journals-pages/ajCEExOaxuB4C1tY.htm)|Passion Domain|Domaine de la Passion|libre|
|[AOQZjqgfafqqtHOB.htm](journals-pages/AOQZjqgfafqqtHOB.htm)|Destruction Domain|Domaine de la destruction|libre|
|[ARYrFIOsT3JxpjDY.htm](journals-pages/ARYrFIOsT3JxpjDY.htm)|Hellknight Armiger|Écuyer des chevaliers infernaux|libre|
|[b5VoSJNzoNuqbtvD.htm](journals-pages/b5VoSJNzoNuqbtvD.htm)|Summoner|Conjurateur|libre|
|[bApp2BZEMuYQCTDM.htm](journals-pages/bApp2BZEMuYQCTDM.htm)|Scout|Éclaireur|libre|
|[bKoV037XO3qiakGw.htm](journals-pages/bKoV037XO3qiakGw.htm)|Ghoul|Goule|libre|
|[bkTCYlTFNifrM3sh.htm](journals-pages/bkTCYlTFNifrM3sh.htm)|Artillerist|Artilleur|libre|
|[bL5cccuvnP5h7Bnn.htm](journals-pages/bL5cccuvnP5h7Bnn.htm)|Animal Trainer|Dompteur|libre|
|[bQeLZt29NnHEn6fh.htm](journals-pages/bQeLZt29NnHEn6fh.htm)|Juggler|Jongleur|libre|
|[BRxlFHXu8tN14TDI.htm](journals-pages/BRxlFHXu8tN14TDI.htm)|Staff Acrobat|Funambule|libre|
|[bTujFcUut9RX4GCy.htm](journals-pages/bTujFcUut9RX4GCy.htm)|Travel Domain|Domaine du voyage|libre|
|[cAxBEZsej32riaY5.htm](journals-pages/cAxBEZsej32riaY5.htm)|Decay Domain|Domaine de la décomposition|libre|
|[CB9YE4P7A2Wty1IX.htm](journals-pages/CB9YE4P7A2Wty1IX.htm)|Red Mantis Assassin|Assassin des mantes rouges|libre|
|[cBBwTnJrVmvaRMYq.htm](journals-pages/cBBwTnJrVmvaRMYq.htm)|Aldori Duelist|Duelliste Aldori|libre|
|[CbsAiY68e8n5vVVN.htm](journals-pages/CbsAiY68e8n5vVVN.htm)|Repose Domain|Domaine de la Quiétude|libre|
|[cdnkS3A8OpOnRjKu.htm](journals-pages/cdnkS3A8OpOnRjKu.htm)|Bellflower Tiller|Laboureur de la Campanule|libre|
|[CkBvj5y1lAm1jnsc.htm](journals-pages/CkBvj5y1lAm1jnsc.htm)|Sun Domain|Domaine du Soleil|libre|
|[CM9ZqWwl7myKn2X1.htm](journals-pages/CM9ZqWwl7myKn2X1.htm)|Darkness Domain|Domaine des Ténèbres|libre|
|[CMgYob7Cy4meoQKg.htm](journals-pages/CMgYob7Cy4meoQKg.htm)|Ranger|Rôdeur|libre|
|[COTDddn4psjIoWry.htm](journals-pages/COTDddn4psjIoWry.htm)|Lion Blade|Lame du lion|libre|
|[CSVoyUvynmM5LzPW.htm](journals-pages/CSVoyUvynmM5LzPW.htm)|Gunslinger|Franc tireur|libre|
|[Czi3XXuNOSE7ISpd.htm](journals-pages/Czi3XXuNOSE7ISpd.htm)|Perfection Domain|Domaine de la Perfection|libre|
|[DI3MYGIK8iEycanU.htm](journals-pages/DI3MYGIK8iEycanU.htm)|Zeal Domain|Domaine du Zèle|libre|
|[DS95vr2zmTsjsMhU.htm](journals-pages/DS95vr2zmTsjsMhU.htm)|Magic Domain|Domaine de la Magie|libre|
|[duqPy1VMQYNJrw7q.htm](journals-pages/duqPy1VMQYNJrw7q.htm)|Conditions|États|libre|
|[Dx47K8wpx8KZUa9S.htm](journals-pages/Dx47K8wpx8KZUa9S.htm)|Protection Domain|Domaine de la Protection|libre|
|[EC2eB0JglDG5j1gT.htm](journals-pages/EC2eB0JglDG5j1gT.htm)|Fate Domain|Domaine du Destin|libre|
|[egSErNozlL3HRK1y.htm](journals-pages/egSErNozlL3HRK1y.htm)|Fire Domain|Domaine du Feu|libre|
|[eLc6TSgWykjtSeuF.htm](journals-pages/eLc6TSgWykjtSeuF.htm)|Magaambyan Attendant|Gardien du Magaambya|libre|
|[eNTStXeuABNPSLjw.htm](journals-pages/eNTStXeuABNPSLjw.htm)|Witch|Sorcier|libre|
|[ePfibeHcnRcjO6lC.htm](journals-pages/ePfibeHcnRcjO6lC.htm)|Reanimator|Réanimateur|libre|
|[EQfZepZX6rxxBRqG.htm](journals-pages/EQfZepZX6rxxBRqG.htm)|Toil Domain|Domaine du Labeur|libre|
|[f2wr86966A0oUUA0.htm](journals-pages/f2wr86966A0oUUA0.htm)|Adjusting Treasure|Ajuster les trésors|libre|
|[f7tFLR7aFonXTLQa.htm](journals-pages/f7tFLR7aFonXTLQa.htm)|Undead Master|Maître des morts-vivants|libre|
|[F7W15pGOeSeNJD0C.htm](journals-pages/F7W15pGOeSeNJD0C.htm)|Firebrand Braggart|Agitateur vantard|libre|
|[flmxRzGxN2rRNyxZ.htm](journals-pages/flmxRzGxN2rRNyxZ.htm)|Confidence Domain|Domaine de la Confiance en soi|libre|
|[fPqSekmEm5byReOk.htm](journals-pages/fPqSekmEm5byReOk.htm)|Talisman Dabbler|Amateur de talismans|libre|
|[FtW1gtbHgO0KofPl.htm](journals-pages/FtW1gtbHgO0KofPl.htm)|Pain Domain|Domaine de la Souffrance|libre|
|[G1Rje9eYCFEByzjg.htm](journals-pages/G1Rje9eYCFEByzjg.htm)|Special Battles|Batailles spéciales (LdB)|libre|
|[gfhnRp2TEy9JCfHI.htm](journals-pages/gfhnRp2TEy9JCfHI.htm)|Gladiator|Gladiateur|libre|
|[GiuzDTtkQAgtGW6n.htm](journals-pages/GiuzDTtkQAgtGW6n.htm)|Indulgence Domain|Domaine des Petits plaisirs|libre|
|[h1zdUIWX1k5PKTnc.htm](journals-pages/h1zdUIWX1k5PKTnc.htm)|Celebrity|Célébrité|libre|
|[HDTW0vr8QL20M5ND.htm](journals-pages/HDTW0vr8QL20M5ND.htm)|Weapon Improviser|Improvisateur d'armes|libre|
|[hGoWOjdsUz16oJUm.htm](journals-pages/hGoWOjdsUz16oJUm.htm)|Plague Domain|Domaine du Fléau|libre|
|[hH8gPjc3GxFzgLHR.htm](journals-pages/hH8gPjc3GxFzgLHR.htm)|Variant Rules|Variantes de règles|libre|
|[I6FbkKYncDuu7eWq.htm](journals-pages/I6FbkKYncDuu7eWq.htm)|Geomancer|Géomancien|libre|
|[I6lHRH3mWjfvhJrR.htm](journals-pages/I6lHRH3mWjfvhJrR.htm)|Resting|Repos|libre|
|[IliVJaEHaOklcg86.htm](journals-pages/IliVJaEHaOklcg86.htm)|Stamina|Endurance|libre|
|[inxtk4rYj2UZaytg.htm](journals-pages/inxtk4rYj2UZaytg.htm)|Fighter|Guerrier|officielle|
|[JBPXisA4IdXI9gVn.htm](journals-pages/JBPXisA4IdXI9gVn.htm)|Eldritch Researcher|Chercheur mystique|libre|
|[jEf21wTIEXzGtPU6.htm](journals-pages/jEf21wTIEXzGtPU6.htm)|Detecting with Other Senses|Détecter avec d'autres sens|libre|
|[jFhTp57zO3ej6HDt.htm](journals-pages/jFhTp57zO3ej6HDt.htm)|Corpse Tender|Éleveur de cadavres|libre|
|[jq9O1tl76g2AzLOh.htm](journals-pages/jq9O1tl76g2AzLOh.htm)|Cold Domain|Domaine du Froid|libre|
|[jTiXRtNNvMOnsg98.htm](journals-pages/jTiXRtNNvMOnsg98.htm)|Beast Gunner|Bestioléro|libre|
|[k9Ebp52kt0ZLHtMl.htm](journals-pages/k9Ebp52kt0ZLHtMl.htm)|Swashbuckler|Bretteur|libre|
|[K9Krytj8OtUvQxoc.htm](journals-pages/K9Krytj8OtUvQxoc.htm)|Thaumaturge|Thaumaturge|libre|
|[Kca7UPuMm44tOo9n.htm](journals-pages/Kca7UPuMm44tOo9n.htm)|Lightning Domain|Domaine de la Foudre|libre|
|[kL8yx8vz9FMCIYC1.htm](journals-pages/kL8yx8vz9FMCIYC1.htm)|Treat Wounds|Soigner les blessures|libre|
|[KORSADviZaSccs2W.htm](journals-pages/KORSADviZaSccs2W.htm)|Zephyr Guard|Garde zéphyr|libre|
|[l0rlB52Pzy6Vt1Ic.htm](journals-pages/l0rlB52Pzy6Vt1Ic.htm)|Scroll Prices|Prix des parchemins|libre|
|[L11XsA5G89xVKlDw.htm](journals-pages/L11XsA5G89xVKlDw.htm)|Luck Domain|Domaine de la Chance|libre|
|[lc3iBCamQ8jvr9dp.htm](journals-pages/lc3iBCamQ8jvr9dp.htm)|Trapsmith|Fabricant de pièges|libre|
|[lgsJz7mZ1OTe340e.htm](journals-pages/lgsJz7mZ1OTe340e.htm)|Truth Domain|Domaine de la Vérité|libre|
|[LL4YD16kPqcGibKG.htm](journals-pages/LL4YD16kPqcGibKG.htm)|Ghost Hunter|Chasseur de fantômes|libre|
|[LWgcWM8B85HHVCtZ.htm](journals-pages/LWgcWM8B85HHVCtZ.htm)|Soulforger|Mentallurgiste|libre|
|[mJBp4KIszuqrmnp5.htm](journals-pages/mJBp4KIszuqrmnp5.htm)|Wealth Domain|Domaine de la Richesse|libre|
|[Mk7ECwe1a971WyWl.htm](journals-pages/Mk7ECwe1a971WyWl.htm)|Demolitionist|Démolisseur|libre|
|[MOVMHZU1SfkhNN1K.htm](journals-pages/MOVMHZU1SfkhNN1K.htm)|Might Domain|Domaine de la Puissance|libre|
|[mtc5frNxUHORGscz.htm](journals-pages/mtc5frNxUHORGscz.htm)|Linguist|Linguiste|libre|
|[mV0K9sxD3TWnZDcy.htm](journals-pages/mV0K9sxD3TWnZDcy.htm)|Undead Slayer|Tueur de mort-vivant|libre|
|[mXywYJJCM9IVItZz.htm](journals-pages/mXywYJJCM9IVItZz.htm)|Mind Smith|Façonneur spirituel|libre|
|[NaG4fy33coUdSFtH.htm](journals-pages/NaG4fy33coUdSFtH.htm)|Jalmeri Heavenseeker|Chercheur de paradis du Jalmeray|libre|
|[nmcEiM2gjqjGWp2c.htm](journals-pages/nmcEiM2gjqjGWp2c.htm)|Cavalier|Cavalier|libre|
|[nPcma8UOqWo7xw0P.htm](journals-pages/nPcma8UOqWo7xw0P.htm)|Trick Driver|Conducteur astucieux|libre|
|[nuywscaiVGXLQpZ1.htm](journals-pages/nuywscaiVGXLQpZ1.htm)|Wyrmkin Domain|Domaine de la Parenté dracosire|libre|
|[nVfhX1aisz6jY8qf.htm](journals-pages/nVfhX1aisz6jY8qf.htm)|Unexpected Sharpshooter|Tireur d'élite inattendu|libre|
|[O7fKfFxCx3e1YasX.htm](journals-pages/O7fKfFxCx3e1YasX.htm)|Spellmaster|Maître des sorts|libre|
|[O9P1YgtiCgHlPNp5.htm](journals-pages/O9P1YgtiCgHlPNp5.htm)|Pirate|Pirate|libre|
|[oBlKgVYRup5ORqx1.htm](journals-pages/oBlKgVYRup5ORqx1.htm)|Bard|Barde|libre|
|[OBTmo8rD2x8kFzeH.htm](journals-pages/OBTmo8rD2x8kFzeH.htm)|Sleepwalker|Somnambule|libre|
|[OcioDiLTdgzvT8VX.htm](journals-pages/OcioDiLTdgzvT8VX.htm)|Knight Reclaimant|Chevalier reconquérant|libre|
|[ok2IPQGIrxj8h1vs.htm](journals-pages/ok2IPQGIrxj8h1vs.htm)|Three-Dimensional Combat|Combat en trois dimensions|libre|
|[pDmUITVao0FDMnVf.htm](journals-pages/pDmUITVao0FDMnVf.htm)|Halcyon Speaker|Orateur syncrétique|libre|
|[pEjd65isUfXKSB18.htm](journals-pages/pEjd65isUfXKSB18.htm)|Hero Points|Points d'héroïsme|libre|
|[PFBBmN58KtjXo79k.htm](journals-pages/PFBBmN58KtjXo79k.htm)|Basic Rules|Règles basiques|libre|
|[PfoKLAsQeXHsDgzf.htm](journals-pages/PfoKLAsQeXHsDgzf.htm)|Lifestyle Expenses|Dépenses liées au mode de vie|libre|
|[pg9fG1ikcgJZRWlk.htm](journals-pages/pg9fG1ikcgJZRWlk.htm)|Lastwall Sentry|Sentinelle du Dernier-Rempart|officielle|
|[Ph7yWRR376aYh12T.htm](journals-pages/Ph7yWRR376aYh12T.htm)|Ritualist|Ritualiste|libre|
|[PpoPRKuwanrnhd0Y.htm](journals-pages/PpoPRKuwanrnhd0Y.htm)|Living Monolith|Monolithe vivant|officielle|
|[PVwU1LMWcSgD7Q0m.htm](journals-pages/PVwU1LMWcSgD7Q0m.htm)|Oatia Skysage|Sagecéleste d'Oatie|libre|
|[Q72YDLPh0urLfWPm.htm](journals-pages/Q72YDLPh0urLfWPm.htm)|Vehicle Mechanic|Mécanicien de véhicule|libre|
|[qlQSYjCwnoCzfto2.htm](journals-pages/qlQSYjCwnoCzfto2.htm)|Golem Grafter|Greffeur de golem|libre|
|[qMS6QepvY7UQQjcr.htm](journals-pages/qMS6QepvY7UQQjcr.htm)|Abomination Domain|Domaine de l'abomination|libre|
|[QzsUe3Rt3SifTQvb.htm](journals-pages/QzsUe3Rt3SifTQvb.htm)|Naga Domain|Domaine Naga|libre|
|[R20JXF43vU5RQyUj.htm](journals-pages/R20JXF43vU5RQyUj.htm)|Nightmares Domain|Domaine des Cauchemars|libre|
|[r43lPEEL7WHOZjHL.htm](journals-pages/r43lPEEL7WHOZjHL.htm)|Lich|Liche|libre|
|[r79jab1XgOdcgvKJ.htm](journals-pages/r79jab1XgOdcgvKJ.htm)|Magus|Magus|libre|
|[rd0jQwvTK4jpv95o.htm](journals-pages/rd0jQwvTK4jpv95o.htm)|Swarm Domain|Domaine des Nuées|libre|
|[RIlgBuWGfHC1rzYu.htm](journals-pages/RIlgBuWGfHC1rzYu.htm)|Undeath Domain|Domaine de la Non-mort|libre|
|[rlKxNuq1obXe3m5J.htm](journals-pages/rlKxNuq1obXe3m5J.htm)|Scrounger|Bricoleur|libre|
|[RoF5NOFBefXAPftS.htm](journals-pages/RoF5NOFBefXAPftS.htm)|Mauler|Cogneur|libre|
|[Rrjz5tMJtyVEQnh8.htm](journals-pages/Rrjz5tMJtyVEQnh8.htm)|Herbalist|Herboriste|libre|
|[rtobUemb6vF2Yu3Y.htm](journals-pages/rtobUemb6vF2Yu3Y.htm)|Soul Domain|Domaine de l'Âme|libre|
|[RxDsPgPCCxEdjcVQ.htm](journals-pages/RxDsPgPCCxEdjcVQ.htm)|Hellknight Signifer|Signifer|libre|
|[rxyJUbQosVfgRjLr.htm](journals-pages/rxyJUbQosVfgRjLr.htm)|Crystal Keeper|Gardien des cristaux|officielle|
|[S1gyomjojgtCdxc3.htm](journals-pages/S1gyomjojgtCdxc3.htm)|Secrecy Domain|Domaine du Secret|libre|
|[S4BZW9c5n6CctDxl.htm](journals-pages/S4BZW9c5n6CctDxl.htm)|Firework Technician|Pyrotechnicien|libre|
|[s9kLzXOCl2GNT6TY.htm](journals-pages/s9kLzXOCl2GNT6TY.htm)|Dandy|Dandy|libre|
|[SO9d7RSf1zqmTlmW.htm](journals-pages/SO9d7RSf1zqmTlmW.htm)|Dragon Disciple|Disciple draconique|libre|
|[StXN6IHR6evRaeXF.htm](journals-pages/StXN6IHR6evRaeXF.htm)|Vigil Domain|Domaine de la Veille|libre|
|[SUDV5hFZ9WocxWqv.htm](journals-pages/SUDV5hFZ9WocxWqv.htm)|Blessed One|Élu divin|libre|
|[sY25SoDaHBPIG5Jw.htm](journals-pages/sY25SoDaHBPIG5Jw.htm)|Nantambu Chime-Ringer|Sonneur de carillon de Nantambu|libre|
|[T0JHj79aGphlZ4Mt.htm](journals-pages/T0JHj79aGphlZ4Mt.htm)|Tyranny Domain|Domaine de la Tyrannie|libre|
|[T2y0vuYibZCL7CH0.htm](journals-pages/T2y0vuYibZCL7CH0.htm)|Air Domain|Domaine de l'Air|libre|
|[tJllU1E8g7W1QxYe.htm](journals-pages/tJllU1E8g7W1QxYe.htm)|Cover|Abri|libre|
|[tJQ5f8C8m5gpZsF1.htm](journals-pages/tJQ5f8C8m5gpZsF1.htm)|Oozemorph|Vasemorphe|libre|
|[TnD2hTWTyjGKlw9b.htm](journals-pages/TnD2hTWTyjGKlw9b.htm)|Sentinel|Sentinelle|libre|
|[tuThzOCvMLbRVba8.htm](journals-pages/tuThzOCvMLbRVba8.htm)|Delirium Domain|Domaine du Délirium|libre|
|[U8WVR6EDfmUaMCbu.htm](journals-pages/U8WVR6EDfmUaMCbu.htm)|Water Domain|Domaine de l'eau|libre|
|[uA6XIArPfGSwBvZi.htm](journals-pages/uA6XIArPfGSwBvZi.htm)|Psychic Duelist|Duelliste psychique|libre|
|[uGQKjk2w4whzomky.htm](journals-pages/uGQKjk2w4whzomky.htm)|Duty Domain|Domaine du Devoir|libre|
|[uGsGn5sgR7wn0TQD.htm](journals-pages/uGsGn5sgR7wn0TQD.htm)|Turpin Rowe Lumberjack|Bûcheron de Turpin Rowe|libre|
|[Ux0sa5SUBu616i5k.htm](journals-pages/Ux0sa5SUBu616i5k.htm)|Alchemist|Alchimiste|libre|
|[vKuJVojicBmNsL1C.htm](journals-pages/vKuJVojicBmNsL1C.htm)|Butterfly Blade|Lame papillon|libre|
|[vS9iMDapZN9uW33Q.htm](journals-pages/vS9iMDapZN9uW33Q.htm)|Bright Lion|Lion radieux|libre|
|[wBhgIgt47v9uspp3.htm](journals-pages/wBhgIgt47v9uspp3.htm)|Nature Domain|Domaine de la Nature|libre|
|[wHs6IGCMaXsgFNop.htm](journals-pages/wHs6IGCMaXsgFNop.htm)|Shadowcaster|Incantateur de l'ombre|libre|
|[xgk1Qc6nzGNJ7LSz.htm](journals-pages/xgk1Qc6nzGNJ7LSz.htm)|Chronoskimmer|Écumeur du temps|libre|
|[xJtbGqoz3BcCjUik.htm](journals-pages/xJtbGqoz3BcCjUik.htm)|Trickery Domain|Domaine de la Tromperie|libre|
|[XLug8rxIe1KPX6Nf.htm](journals-pages/XLug8rxIe1KPX6Nf.htm)|Provocator|Provocator|libre|
|[xLxrtbsj4acqgsyC.htm](journals-pages/xLxrtbsj4acqgsyC.htm)|Void Domain|Domaine du Vide|libre|
|[xMdIENo4w9bEzZuK.htm](journals-pages/xMdIENo4w9bEzZuK.htm)|Ghost Eater|Mangeur de fantômes|libre|
|[XszmLWy174esRrlg.htm](journals-pages/XszmLWy174esRrlg.htm)|Wizard|Magicien|libre|
|[XWkyCVISmVtJ0ZY3.htm](journals-pages/XWkyCVISmVtJ0ZY3.htm)|Medic|Médecin|libre|
|[Y4DiXz5I8krCh1fC.htm](journals-pages/Y4DiXz5I8krCh1fC.htm)|Swordmaster|Maître épéiste|libre|
|[yaMJsfYZmWJLqbFE.htm](journals-pages/yaMJsfYZmWJLqbFE.htm)|Ambition Domain|Domaine de l'Ambition|libre|
|[ygzv72IJNmjh0SPB.htm](journals-pages/ygzv72IJNmjh0SPB.htm)|Psychic|Psychiste|libre|
|[YODf1eNWi9jnR93y.htm](journals-pages/YODf1eNWi9jnR93y.htm)|Pactbound Initiate|Initié lié par un Pacte|libre|
|[yOEiNjHLQ6XuOruq.htm](journals-pages/yOEiNjHLQ6XuOruq.htm)|Overwatch|Observateur|libre|
|[YQXGtBHVoiSYUjtl.htm](journals-pages/YQXGtBHVoiSYUjtl.htm)|Twilight Speaker|Orateur du crépuscule|libre|
|[yWtwNUkGyj79Q04W.htm](journals-pages/yWtwNUkGyj79Q04W.htm)|Hellknight|Chevalier infernal|libre|
|[zB1mqE9IeyfyQDnn.htm](journals-pages/zB1mqE9IeyfyQDnn.htm)|Exploration Activities|Activités d'exploration|libre|
|[ZewC2i5YdZPsWO8X.htm](journals-pages/ZewC2i5YdZPsWO8X.htm)|Mummy|Momie|libre|
|[zj9z2BwVX6TLHdx3.htm](journals-pages/zj9z2BwVX6TLHdx3.htm)|Wellspring Mage (Class Archetype)|Mage de la source (Archétype de classe)|libre|
|[ZJHhPFjLnizAaUM1.htm](journals-pages/ZJHhPFjLnizAaUM1.htm)|Cleric|Prêtre|officielle|
|[zkiLWWYzzqoxmN2J.htm](journals-pages/zkiLWWYzzqoxmN2J.htm)|Earth Domain|Domaine de la Terre|libre|
|[zxY04QNfB90gjLoh.htm](journals-pages/zxY04QNfB90gjLoh.htm)|Magic Warrior|Guerrier magique|libre|
