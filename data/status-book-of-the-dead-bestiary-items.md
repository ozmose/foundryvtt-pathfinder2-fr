# État de la traduction (book-of-the-dead-bestiary-items)

 * **libre**: 854
 * **changé**: 36
 * **officielle**: 33


Dernière mise à jour: 2023-07-12 20:28 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0aqvd9pAwLkOwZeo.htm](book-of-the-dead-bestiary-items/0aqvd9pAwLkOwZeo.htm)|Tortured Gaze|Regard torturé|changé|
|[14ufiu26cnai5yer.htm](book-of-the-dead-bestiary-items/14ufiu26cnai5yer.htm)|Demolition|Démolition|changé|
|[3kzbeblthgly6zkx.htm](book-of-the-dead-bestiary-items/3kzbeblthgly6zkx.htm)|Time Shift|Décalage temporel|changé|
|[4z31tccif8f7qaq7.htm](book-of-the-dead-bestiary-items/4z31tccif8f7qaq7.htm)|Spawn Hunter Wight|Rejeton de chasseur nécrophage|changé|
|[581dy5lnt449p866.htm](book-of-the-dead-bestiary-items/581dy5lnt449p866.htm)|Spawn Wight Soldier|Rejeton de commandant nécrophage|changé|
|[5k5e5m4xanv71qep.htm](book-of-the-dead-bestiary-items/5k5e5m4xanv71qep.htm)|Smoldering Fist|Poing de braises|changé|
|[5q62z2h5hdchg2s2.htm](book-of-the-dead-bestiary-items/5q62z2h5hdchg2s2.htm)|Awful Approach|Approche affreuse|changé|
|[7499bjv6f655br8n.htm](book-of-the-dead-bestiary-items/7499bjv6f655br8n.htm)|Spawn Prowler Wight|Rejeton de nécrophage rôdeur|changé|
|[9QFD9WobEytvbbov.htm](book-of-the-dead-bestiary-items/9QFD9WobEytvbbov.htm)|Negative Healing|Guérison négative|changé|
|[bftdv71wnr4786h4.htm](book-of-the-dead-bestiary-items/bftdv71wnr4786h4.htm)|Call of the Damned|Appel des damnés|changé|
|[bh3hivovyu8ym9ay.htm](book-of-the-dead-bestiary-items/bh3hivovyu8ym9ay.htm)|Painful Touch|Contact douloureux|changé|
|[cVXdT14wxKimZClN.htm](book-of-the-dead-bestiary-items/cVXdT14wxKimZClN.htm)|Jiang-Shi Vulnerabilities|Vulnérabilités jiang-shi|changé|
|[dizkg5865hh5zv35.htm](book-of-the-dead-bestiary-items/dizkg5865hh5zv35.htm)|Wake the Dead|Réveiller les morts|changé|
|[e68t84jixmehqnur.htm](book-of-the-dead-bestiary-items/e68t84jixmehqnur.htm)|Final Spite|Malveillance ultime|changé|
|[ekMIs1v4IBJG2qEg.htm](book-of-the-dead-bestiary-items/ekMIs1v4IBJG2qEg.htm)|Create Zombies|Création de zombies|changé|
|[hK1tPnyMOt0g3STo.htm](book-of-the-dead-bestiary-items/hK1tPnyMOt0g3STo.htm)|Lifesense 60 feet|Perception de la vie 18 m|changé|
|[ijsqkg9v5y89e7nf.htm](book-of-the-dead-bestiary-items/ijsqkg9v5y89e7nf.htm)|Scream in Agony|Hurler à l'agonie|changé|
|[L4ewcxHDWUZozOIs.htm](book-of-the-dead-bestiary-items/L4ewcxHDWUZozOIs.htm)|Jiang-Shi Vulnerabilities|Vulnérabilités Jiang-Shi|changé|
|[obhnbsaze5pkt16e.htm](book-of-the-dead-bestiary-items/obhnbsaze5pkt16e.htm)|Osseous Defense|Défense osseuse|changé|
|[ohpwpe4hf1byw99z.htm](book-of-the-dead-bestiary-items/ohpwpe4hf1byw99z.htm)|Rejuvenation|Reconstruction|changé|
|[qViybdc6hq6JwJNP.htm](book-of-the-dead-bestiary-items/qViybdc6hq6JwJNP.htm)|Warped Fulu|Fulu corrompu|changé|
|[r7kfGxLnvo6gvY8J.htm](book-of-the-dead-bestiary-items/r7kfGxLnvo6gvY8J.htm)|Rejuvenation|Reconstruction|changé|
|[sDG67LzJ4UKhFyfv.htm](book-of-the-dead-bestiary-items/sDG67LzJ4UKhFyfv.htm)|Carve in Flesh|Tailler dans la chair|changé|
|[SeDr8IsuA0cd6pKi.htm](book-of-the-dead-bestiary-items/SeDr8IsuA0cd6pKi.htm)|Rejuvenation|Reconstruction|changé|
|[SQt7rET9Z2MgoK8E.htm](book-of-the-dead-bestiary-items/SQt7rET9Z2MgoK8E.htm)|Rejuvenation|Reconstruction|changé|
|[ssmbr5eqwybx9xne.htm](book-of-the-dead-bestiary-items/ssmbr5eqwybx9xne.htm)|Endless Suffering|Souffrances éternelles|changé|
|[t4dd2bxdturlg45u.htm](book-of-the-dead-bestiary-items/t4dd2bxdturlg45u.htm)|Flaying Flurry|Rafale de dépeçage|changé|
|[tOSvo39Ofu2f5ufu.htm](book-of-the-dead-bestiary-items/tOSvo39Ofu2f5ufu.htm)|Rejuvenation|Reconstruction|changé|
|[tvvsxwr3fxjxf5ho.htm](book-of-the-dead-bestiary-items/tvvsxwr3fxjxf5ho.htm)|Broken Barb|Ardillon cassé|changé|
|[v006gzbsc924nhz4.htm](book-of-the-dead-bestiary-items/v006gzbsc924nhz4.htm)|Exhale|Exhalation|changé|
|[vvxq8pxshmt9yd29.htm](book-of-the-dead-bestiary-items/vvxq8pxshmt9yd29.htm)|Frozen Breath|Souffle glacé|changé|
|[wv50pg4p8i3xb12d.htm](book-of-the-dead-bestiary-items/wv50pg4p8i3xb12d.htm)|Putrid Assault|Assaut putride|changé|
|[XyO3dvZ3nK3xea9x.htm](book-of-the-dead-bestiary-items/XyO3dvZ3nK3xea9x.htm)|Breathsense (Precise) 60 feet|Perception du souffle 18 m (précis)|changé|
|[yarojxhc6r3jba98.htm](book-of-the-dead-bestiary-items/yarojxhc6r3jba98.htm)|Pallid Plague|Peste livide|changé|
|[Z47yWqdpdND95LJp.htm](book-of-the-dead-bestiary-items/Z47yWqdpdND95LJp.htm)|Regeneration 10 (Deactivated by Piercing)|Régénération 10 (Désactivée par Froid)|changé|
|[zy1MsGurtjxz9LuB.htm](book-of-the-dead-bestiary-items/zy1MsGurtjxz9LuB.htm)|Warped Fulu|Fulu corrompu|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[048p1dvtj51r50vq.htm](book-of-the-dead-bestiary-items/048p1dvtj51r50vq.htm)|Lignify|Lignifier|libre|
|[05F3xO5TlXZ9n8cg.htm](book-of-the-dead-bestiary-items/05F3xO5TlXZ9n8cg.htm)|Negative Healing|Guérison négative|libre|
|[06rJwQpNQ1H9tOn3.htm](book-of-the-dead-bestiary-items/06rJwQpNQ1H9tOn3.htm)|Darkvision|Vision dans le noir|libre|
|[09d8l407352ep4z4.htm](book-of-the-dead-bestiary-items/09d8l407352ep4z4.htm)|Claw|Griffe|libre|
|[0a0R5L9eHRL770lk.htm](book-of-the-dead-bestiary-items/0a0R5L9eHRL770lk.htm)|Breathsense (Precise) 60 feet|Perception du souffle 18 m (précis)|libre|
|[0AkVpzKeetsxnaM0.htm](book-of-the-dead-bestiary-items/0AkVpzKeetsxnaM0.htm)|Negative Healing|Guérison négative|libre|
|[0cJtxKvPeAJC8lOc.htm](book-of-the-dead-bestiary-items/0cJtxKvPeAJC8lOc.htm)|Improved Grab|Empoignade/Aggripement améliorés|libre|
|[0cQO5TZGYHxwoqee.htm](book-of-the-dead-bestiary-items/0cQO5TZGYHxwoqee.htm)|Lifesense 30 feet|Perception de la vie 9 m|libre|
|[0e0uF4QWaN68KIlV.htm](book-of-the-dead-bestiary-items/0e0uF4QWaN68KIlV.htm)|Paralyzing Claws|Griffes paralysantes|libre|
|[0FiwKIU8kAmjUnyd.htm](book-of-the-dead-bestiary-items/0FiwKIU8kAmjUnyd.htm)|Rapier|+1,striking|Rapière de frappe +1|officielle|
|[0fo5e4nnx624my4t.htm](book-of-the-dead-bestiary-items/0fo5e4nnx624my4t.htm)|Weathering Aura|Aura d'altération climatique|libre|
|[0gikfhrrftrwkz2g.htm](book-of-the-dead-bestiary-items/0gikfhrrftrwkz2g.htm)|Intimidation|Intimidation|libre|
|[0jGEzNSvC3mj1gAv.htm](book-of-the-dead-bestiary-items/0jGEzNSvC3mj1gAv.htm)|Swallow Whole|Gober|libre|
|[0oBoPpJaPEder5Uj.htm](book-of-the-dead-bestiary-items/0oBoPpJaPEder5Uj.htm)|Scythe|Faux|libre|
|[0PYT6BjBxO2OCUr4.htm](book-of-the-dead-bestiary-items/0PYT6BjBxO2OCUr4.htm)|Battle Axe|+2,greaterStriking,grievous|Hache d'armes douloureuse de frappe supérieure +2|libre|
|[0VnHFn3wn3nNZ2Tp.htm](book-of-the-dead-bestiary-items/0VnHFn3wn3nNZ2Tp.htm)|Swallow Whole|Gober|libre|
|[0xaci6r10kc1q6hz.htm](book-of-the-dead-bestiary-items/0xaci6r10kc1q6hz.htm)|Stunning Flurry|Déluge étourdissant|libre|
|[0yqQhSvO3kn23ESU.htm](book-of-the-dead-bestiary-items/0yqQhSvO3kn23ESU.htm)|Devastating Blast|Déflagration dévastatrice|libre|
|[0zj1icya9xc6t4m7.htm](book-of-the-dead-bestiary-items/0zj1icya9xc6t4m7.htm)|Consecration Vulnerability|Vulnérable à la consécration|libre|
|[0zs0is6k2nndmc5h.htm](book-of-the-dead-bestiary-items/0zs0is6k2nndmc5h.htm)|Nerve Ending|Terminaison nerveuse|libre|
|[10pagf9v2vm39x4i.htm](book-of-the-dead-bestiary-items/10pagf9v2vm39x4i.htm)|Fist|Poing|libre|
|[13h23z1r0zoaru7r.htm](book-of-the-dead-bestiary-items/13h23z1r0zoaru7r.htm)|Snatch|Saisir|libre|
|[15ji0gz9dw3xxfog.htm](book-of-the-dead-bestiary-items/15ji0gz9dw3xxfog.htm)|Spray Black Bile|Éclatement de bile noire|libre|
|[15qn60fnmbbmi7qd.htm](book-of-the-dead-bestiary-items/15qn60fnmbbmi7qd.htm)|Bone|Os|libre|
|[164uke5yf2ifcf8e.htm](book-of-the-dead-bestiary-items/164uke5yf2ifcf8e.htm)|Reap|Faucher|libre|
|[1a7SbMUxtcojgqLl.htm](book-of-the-dead-bestiary-items/1a7SbMUxtcojgqLl.htm)|At-Will Spells|Sorts à volonté|libre|
|[1cs7olyf6afhvxbc.htm](book-of-the-dead-bestiary-items/1cs7olyf6afhvxbc.htm)|Onryo's Rancor|Rancoeur Onryo|libre|
|[1f5q331o2mt1wcdv.htm](book-of-the-dead-bestiary-items/1f5q331o2mt1wcdv.htm)|Soulbound Gallop|Galop d'âme lié|libre|
|[1fa8oveae7es6ajn.htm](book-of-the-dead-bestiary-items/1fa8oveae7es6ajn.htm)|Fiddlestick|Archet|libre|
|[1gmv4iqur5scrtnw.htm](book-of-the-dead-bestiary-items/1gmv4iqur5scrtnw.htm)|Soulscent (Imprecise) 200 feet|Perception des âmes 60 mètres (imprécis)|libre|
|[1HdblgdIZkc4dN3e.htm](book-of-the-dead-bestiary-items/1HdblgdIZkc4dN3e.htm)|Vetalarana Vulnerabilities|Vulnérabilités d'un vétalarana|libre|
|[1lbqFqVnwC2oNkep.htm](book-of-the-dead-bestiary-items/1lbqFqVnwC2oNkep.htm)|Darkvision|Vision dans le noir|libre|
|[1mqppqclycua1e1x.htm](book-of-the-dead-bestiary-items/1mqppqclycua1e1x.htm)|Power of the Haunt|Puissance de l'apparition|libre|
|[1NWUcNdniZkhKfCb.htm](book-of-the-dead-bestiary-items/1NWUcNdniZkhKfCb.htm)|Darkvision|Vision dans le noir|libre|
|[1otpt2d2gyuzglcy.htm](book-of-the-dead-bestiary-items/1otpt2d2gyuzglcy.htm)|Claw|Griffe|libre|
|[1rzixvxa00esxhop.htm](book-of-the-dead-bestiary-items/1rzixvxa00esxhop.htm)|Filth Fever|Fièvre de la fange|libre|
|[1T3U4mJBzZ7Nt8uF.htm](book-of-the-dead-bestiary-items/1T3U4mJBzZ7Nt8uF.htm)|Improved Grab|Empoignade/Agrippement améliorés|libre|
|[1t9Hd470CiyR1VXm.htm](book-of-the-dead-bestiary-items/1t9Hd470CiyR1VXm.htm)|Command (Animals Only)|Injonction (animaux seulement)|libre|
|[1U8XEv359HH8seeK.htm](book-of-the-dead-bestiary-items/1U8XEv359HH8seeK.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[1VGFADh8jJIfVH94.htm](book-of-the-dead-bestiary-items/1VGFADh8jJIfVH94.htm)|At-Will Spells|Sorts à volonté|libre|
|[1vq2jy6pqbxkwfde.htm](book-of-the-dead-bestiary-items/1vq2jy6pqbxkwfde.htm)|Servitor Realignment|Serviteur réaligné|libre|
|[1y7qe4eff55izld8.htm](book-of-the-dead-bestiary-items/1y7qe4eff55izld8.htm)|Stinger|Dard|libre|
|[1zQfinUzk9ky6EKF.htm](book-of-the-dead-bestiary-items/1zQfinUzk9ky6EKF.htm)|Weapon Master|Maître d'armes|libre|
|[20w584tt1vgcah4n.htm](book-of-the-dead-bestiary-items/20w584tt1vgcah4n.htm)|Claw|Griffe|libre|
|[21j0rnxdp8gf5zsf.htm](book-of-the-dead-bestiary-items/21j0rnxdp8gf5zsf.htm)|Jaws|Mâchoires|libre|
|[21sp3kwgg8uh9p13.htm](book-of-the-dead-bestiary-items/21sp3kwgg8uh9p13.htm)|Dagger|Dague|libre|
|[23gdXY5uSfl0Kp3I.htm](book-of-the-dead-bestiary-items/23gdXY5uSfl0Kp3I.htm)|Shifting Earth|Déplacement de terre|libre|
|[24C4VbeTjw3Ovvdn.htm](book-of-the-dead-bestiary-items/24C4VbeTjw3Ovvdn.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[25v45nqumj44pd4b.htm](book-of-the-dead-bestiary-items/25v45nqumj44pd4b.htm)|Wear Skin|Porter la peau|libre|
|[266u1f6h9sli518d.htm](book-of-the-dead-bestiary-items/266u1f6h9sli518d.htm)|Urveth Venom|Venin d'urveth|libre|
|[26yzjYUTRsWSJIeP.htm](book-of-the-dead-bestiary-items/26yzjYUTRsWSJIeP.htm)|At-Will Spells|Sorts à volonté|libre|
|[26zb7felsd7fhdyg.htm](book-of-the-dead-bestiary-items/26zb7felsd7fhdyg.htm)|Float|Flotte|libre|
|[272bll9hrxa49hmf.htm](book-of-the-dead-bestiary-items/272bll9hrxa49hmf.htm)|Ghostly Swoop|Descente fantomatique|libre|
|[2ahqZc1dTw700QB2.htm](book-of-the-dead-bestiary-items/2ahqZc1dTw700QB2.htm)|Darkvision|Vision dans le noir|libre|
|[2ASu2r4ddMMqE7Y8.htm](book-of-the-dead-bestiary-items/2ASu2r4ddMMqE7Y8.htm)|Religious Symbol of Urgathoa|Symbole religieux d'Urgathoa|libre|
|[2aUUvMiYk0FR7Cfr.htm](book-of-the-dead-bestiary-items/2aUUvMiYk0FR7Cfr.htm)|Negative Healing|Guérison négative|libre|
|[2axamykyrsqyw3y1.htm](book-of-the-dead-bestiary-items/2axamykyrsqyw3y1.htm)|Undying Vendetta|Vendetta immortelle|libre|
|[2ccxczu33z7f67dc.htm](book-of-the-dead-bestiary-items/2ccxczu33z7f67dc.htm)|Premonition of Death|Prémonition de mort|libre|
|[2d552nmuu8dx669n.htm](book-of-the-dead-bestiary-items/2d552nmuu8dx669n.htm)|Dagger|Dague|libre|
|[2fv011dfxayfhevz.htm](book-of-the-dead-bestiary-items/2fv011dfxayfhevz.htm)|Water Vulnerability|Vulnérabilité à l'eau|libre|
|[2h8eb5k9fufiveqm.htm](book-of-the-dead-bestiary-items/2h8eb5k9fufiveqm.htm)|Sand Rot|Pourriture de sable|libre|
|[2haw98dime2bu3qi.htm](book-of-the-dead-bestiary-items/2haw98dime2bu3qi.htm)|Shatter Block|Blocage d'éclats|libre|
|[2mNv4HeeYz6pnwor.htm](book-of-the-dead-bestiary-items/2mNv4HeeYz6pnwor.htm)|Negative Healing|Guérison négative|libre|
|[2RyJ9XZZMEWMa6zK.htm](book-of-the-dead-bestiary-items/2RyJ9XZZMEWMa6zK.htm)|Darkvision|Vision dans le noir|libre|
|[2s8zOCsruWILWVA9.htm](book-of-the-dead-bestiary-items/2s8zOCsruWILWVA9.htm)|Darkvision|Vision dans le noir|libre|
|[2tfn09ud7vyhndbb.htm](book-of-the-dead-bestiary-items/2tfn09ud7vyhndbb.htm)|Fossil Fury|Furie fossilisé|libre|
|[2ujjbxpmiiodb9o5.htm](book-of-the-dead-bestiary-items/2ujjbxpmiiodb9o5.htm)|Slow|Lent|libre|
|[2xqc5getycsdpc49.htm](book-of-the-dead-bestiary-items/2xqc5getycsdpc49.htm)|Talon|Serre|libre|
|[2xvk1ovgd1lsv1ze.htm](book-of-the-dead-bestiary-items/2xvk1ovgd1lsv1ze.htm)|Vital Transfusion|Transfusion vitale|libre|
|[2ynj848nvp9c3igy.htm](book-of-the-dead-bestiary-items/2ynj848nvp9c3igy.htm)|Betray the Pack|Trahison de la meute|libre|
|[2yp36AhxUGVlWXgq.htm](book-of-the-dead-bestiary-items/2yp36AhxUGVlWXgq.htm)|Darkvision|Vision dans le noir|libre|
|[324ljVcWLv8U3PwG.htm](book-of-the-dead-bestiary-items/324ljVcWLv8U3PwG.htm)|Negative Healing|Guérison négative|libre|
|[32Oayck2ePyYYgli.htm](book-of-the-dead-bestiary-items/32Oayck2ePyYYgli.htm)|Negative Healing|Guérison négative|libre|
|[32onun6vxolii6qa.htm](book-of-the-dead-bestiary-items/32onun6vxolii6qa.htm)|Jaws|Mâchoires|libre|
|[35tm9k6kvybbcwkv.htm](book-of-the-dead-bestiary-items/35tm9k6kvybbcwkv.htm)|Aura of Whispers|Aura de murmures|libre|
|[35ytvmm1cwqr4dbu.htm](book-of-the-dead-bestiary-items/35ytvmm1cwqr4dbu.htm)|Sudden Surge|Accélération soudaine|libre|
|[36cdvdo5yvfoxz7s.htm](book-of-the-dead-bestiary-items/36cdvdo5yvfoxz7s.htm)|Composite Longbow|Arc long composite|libre|
|[3BLhMeYajV3Rxi3L.htm](book-of-the-dead-bestiary-items/3BLhMeYajV3Rxi3L.htm)|Grab|Empoignade/Agrippement|libre|
|[3DAs1iaOfMM4FE4c.htm](book-of-the-dead-bestiary-items/3DAs1iaOfMM4FE4c.htm)|Fiddle|Violon|libre|
|[3G8FEWYsLeMf6cAv.htm](book-of-the-dead-bestiary-items/3G8FEWYsLeMf6cAv.htm)|Swallow Whole|Gober|libre|
|[3gvb9wq37oq21be3.htm](book-of-the-dead-bestiary-items/3gvb9wq37oq21be3.htm)|Drain Life|Drain de vie|libre|
|[3hac6i5gkgu40340.htm](book-of-the-dead-bestiary-items/3hac6i5gkgu40340.htm)|Jaws|Mâchoires|libre|
|[3j8u75qe0rn2330n.htm](book-of-the-dead-bestiary-items/3j8u75qe0rn2330n.htm)|Flintlock Pistol|Pistolet silex|libre|
|[3mhiog9le9ovojoy.htm](book-of-the-dead-bestiary-items/3mhiog9le9ovojoy.htm)|Soothsaying Lore|Connaissance des devinettes|libre|
|[3p1n4in9wahzixgz.htm](book-of-the-dead-bestiary-items/3p1n4in9wahzixgz.htm)|Ghost Lore|Connaissance des fantômes|libre|
|[3qz1msK40lJzzqhO.htm](book-of-the-dead-bestiary-items/3qz1msK40lJzzqhO.htm)|Grab|Empoignade/Agrippement|libre|
|[3rKCigKvwBeinC4a.htm](book-of-the-dead-bestiary-items/3rKCigKvwBeinC4a.htm)|Plane Shift (to Material Plane, Negative Energy Plane, or Shadow Plane only)|Changement de plan (vers le plan Matériel, de l' Énergie négative et de l'Ombre uniquement)|libre|
|[3spjg2szu482ix18.htm](book-of-the-dead-bestiary-items/3spjg2szu482ix18.htm)|Drain Magic|Drain de magie|libre|
|[3t7zznqikrwn9cyb.htm](book-of-the-dead-bestiary-items/3t7zznqikrwn9cyb.htm)|Heavy Crossbow|Arbalète lourde|libre|
|[3whr45ahot8zh9nf.htm](book-of-the-dead-bestiary-items/3whr45ahot8zh9nf.htm)|Abdomen Cache|Abdomen creux|libre|
|[3ybyexad76n3ge2g.htm](book-of-the-dead-bestiary-items/3ybyexad76n3ge2g.htm)|Servitor Assembly|Assemblage de serviteurs|libre|
|[40McFjcRX4SifzsC.htm](book-of-the-dead-bestiary-items/40McFjcRX4SifzsC.htm)|Negative Healing|Guérison négative|libre|
|[40tw7u6c1rckcl0u.htm](book-of-the-dead-bestiary-items/40tw7u6c1rckcl0u.htm)|Bone Javelin|Javeline d'os|libre|
|[41Dv5UzszJFoJotn.htm](book-of-the-dead-bestiary-items/41Dv5UzszJFoJotn.htm)|Negative Healing|Guérison négative|libre|
|[42bls3q0amg2osjy.htm](book-of-the-dead-bestiary-items/42bls3q0amg2osjy.htm)|Jaws|Mâchoires|libre|
|[42s7jyacejyvhl2u.htm](book-of-the-dead-bestiary-items/42s7jyacejyvhl2u.htm)|Infectious Visions|Vision infectieuse|libre|
|[43aba9wneEhm4kYI.htm](book-of-the-dead-bestiary-items/43aba9wneEhm4kYI.htm)|Darkvision|Vision dans le noir|libre|
|[43rtpaawc0hyjxs2.htm](book-of-the-dead-bestiary-items/43rtpaawc0hyjxs2.htm)|Sunlight Powerlessness|Impuissance solaire|libre|
|[44AaXfBWgXV0uMpL.htm](book-of-the-dead-bestiary-items/44AaXfBWgXV0uMpL.htm)|Darkvision|Vision dans le noir|libre|
|[479508pjo4uznsty.htm](book-of-the-dead-bestiary-items/479508pjo4uznsty.htm)|Final Snare|Piège ultime|libre|
|[47dj3m4gnycp9ovy.htm](book-of-the-dead-bestiary-items/47dj3m4gnycp9ovy.htm)|Vein Walker|Marche sur les veines|libre|
|[4DPXEcfNdnVqHXz2.htm](book-of-the-dead-bestiary-items/4DPXEcfNdnVqHXz2.htm)|Charm (Animals Only)|Charme (animaux uniquement)|officielle|
|[4fpg4t23cn5sl7dn.htm](book-of-the-dead-bestiary-items/4fpg4t23cn5sl7dn.htm)|Flail|Fléau d'arme|libre|
|[4i9LmwMyEPvNox7y.htm](book-of-the-dead-bestiary-items/4i9LmwMyEPvNox7y.htm)|Scythe|+1|Faux +1|libre|
|[4KyzQNfgO9JMN6RH.htm](book-of-the-dead-bestiary-items/4KyzQNfgO9JMN6RH.htm)|Ghoul Fever|Fièvre des goules|libre|
|[4nn1859fvesmfedg.htm](book-of-the-dead-bestiary-items/4nn1859fvesmfedg.htm)|Shortsword|Épée courte|libre|
|[4p83bgih48iq1inb.htm](book-of-the-dead-bestiary-items/4p83bgih48iq1inb.htm)|Claw|Griffe|libre|
|[4pMFJruJiXAxRCo4.htm](book-of-the-dead-bestiary-items/4pMFJruJiXAxRCo4.htm)|Air Walk (Constant)|Marche dans les airs (constant)|officielle|
|[4pt21hdckfee43y3.htm](book-of-the-dead-bestiary-items/4pt21hdckfee43y3.htm)|Mountain Sword|Épée de montagne|libre|
|[4qk5llfpop3z00fj.htm](book-of-the-dead-bestiary-items/4qk5llfpop3z00fj.htm)|Fist|Poing|libre|
|[4r9QDElzXGCRVfhh.htm](book-of-the-dead-bestiary-items/4r9QDElzXGCRVfhh.htm)|At-Will Spells|Sorts à volonté|libre|
|[4syq1lo2m1mx4330.htm](book-of-the-dead-bestiary-items/4syq1lo2m1mx4330.htm)|Agent of Despair|Agent de la désespérance|libre|
|[4uc0r15oqcogaudt.htm](book-of-the-dead-bestiary-items/4uc0r15oqcogaudt.htm)|Voice Imitation|Imitation de voix|libre|
|[4zbcbp0ersjqi242.htm](book-of-the-dead-bestiary-items/4zbcbp0ersjqi242.htm)|Curse Ye Scallywags!|Soyez damné scélérats !|libre|
|[517tUMEgs6I4camL.htm](book-of-the-dead-bestiary-items/517tUMEgs6I4camL.htm)|Negative Healing|Guérison négative|libre|
|[53r0jhgz2tlvtcn9.htm](book-of-the-dead-bestiary-items/53r0jhgz2tlvtcn9.htm)|Accelerating Inquest|Enquête d'accélération|libre|
|[55sqmv4kf6twrsh0.htm](book-of-the-dead-bestiary-items/55sqmv4kf6twrsh0.htm)|Foot|Pied|libre|
|[56PK7jsheqUjQAKo.htm](book-of-the-dead-bestiary-items/56PK7jsheqUjQAKo.htm)|Sneak Attack|Attaque sournoise|libre|
|[56vkqrstijfjdh6c.htm](book-of-the-dead-bestiary-items/56vkqrstijfjdh6c.htm)|Ground Slam|Éclatement au sol|libre|
|[57b0x2wmtxjbx2ww.htm](book-of-the-dead-bestiary-items/57b0x2wmtxjbx2ww.htm)|Psychic Superiority|Supériorité psychique|libre|
|[58ox9o6ljire079b.htm](book-of-the-dead-bestiary-items/58ox9o6ljire079b.htm)|Wrath of the Haunt|Colère d'apparition|libre|
|[5aGqEha0KK3U7pFi.htm](book-of-the-dead-bestiary-items/5aGqEha0KK3U7pFi.htm)|-2 to All Saves vs. Curses|Malus de -2 à tous les jets de sauvegarde contre les malédictions|libre|
|[5cP06WMcn0WgZIXu.htm](book-of-the-dead-bestiary-items/5cP06WMcn0WgZIXu.htm)|Darkvision|Vision dans le noir|libre|
|[5CQVRA4OFks4NZna.htm](book-of-the-dead-bestiary-items/5CQVRA4OFks4NZna.htm)|Negative Healing|Guérison négative|libre|
|[5dpwpqolmjl4a6sn.htm](book-of-the-dead-bestiary-items/5dpwpqolmjl4a6sn.htm)|Collect Soul|Collecter une âme|libre|
|[5gjnv61c3nc5vbt6.htm](book-of-the-dead-bestiary-items/5gjnv61c3nc5vbt6.htm)|Spiked Chain|Chaîne cloutée|libre|
|[5l8z97yi0fc0gqoq.htm](book-of-the-dead-bestiary-items/5l8z97yi0fc0gqoq.htm)|Root|Racine|libre|
|[5lklhb6x3xhg0lri.htm](book-of-the-dead-bestiary-items/5lklhb6x3xhg0lri.htm)|Fist|Poing|libre|
|[5mxcdpdeibtg3cig.htm](book-of-the-dead-bestiary-items/5mxcdpdeibtg3cig.htm)|Ghostly Blades|Lames fantomatiques|libre|
|[5n8sgtk9eiwslacu.htm](book-of-the-dead-bestiary-items/5n8sgtk9eiwslacu.htm)|Sense Visitors|Perception des visiteurs|libre|
|[5q1vmg0m797bxfoz.htm](book-of-the-dead-bestiary-items/5q1vmg0m797bxfoz.htm)|Claw|Griffe|libre|
|[5qil88xw16heonrg.htm](book-of-the-dead-bestiary-items/5qil88xw16heonrg.htm)|Smoke Vision|Vision malgré la fumée|libre|
|[5S4ZcAlTU2l4OVrx.htm](book-of-the-dead-bestiary-items/5S4ZcAlTU2l4OVrx.htm)|Wavesense (Imprecise) 100 feet|Perception des ondes (imprécis) 30 m|libre|
|[5tla19582bslvej5.htm](book-of-the-dead-bestiary-items/5tla19582bslvej5.htm)|Death Grip|Saisie mortelle|libre|
|[5wKokTxNYOw3NX8Y.htm](book-of-the-dead-bestiary-items/5wKokTxNYOw3NX8Y.htm)|Enthrall (At Will)|Discours captivant (À volonté)|officielle|
|[62rba0y5m9752v4s.htm](book-of-the-dead-bestiary-items/62rba0y5m9752v4s.htm)|Shadow Plane Lore|Connaissance du plan de l'ombre|officielle|
|[6312irfg8l2y8wqs.htm](book-of-the-dead-bestiary-items/6312irfg8l2y8wqs.htm)|Foot|Patte|libre|
|[68hcqX4h4YdgWr9o.htm](book-of-the-dead-bestiary-items/68hcqX4h4YdgWr9o.htm)|Fist|Poing|libre|
|[6c9470dhaj9941f5.htm](book-of-the-dead-bestiary-items/6c9470dhaj9941f5.htm)|Warfare Lore|Connaissance de la guerre|officielle|
|[6FVQCXo7u4wEwCcA.htm](book-of-the-dead-bestiary-items/6FVQCXo7u4wEwCcA.htm)|Constrict|Constriction|libre|
|[6h26o4rywcidme60.htm](book-of-the-dead-bestiary-items/6h26o4rywcidme60.htm)|Time-shifting Touch|Contact de décalage du temps|libre|
|[6Ho5G5qDA1od6BFM.htm](book-of-the-dead-bestiary-items/6Ho5G5qDA1od6BFM.htm)|Fast Healing 10|Guérison accélérée 10|libre|
|[6iygpHZZveJtpY3U.htm](book-of-the-dead-bestiary-items/6iygpHZZveJtpY3U.htm)|Feast|Festin|libre|
|[6leqktn7qesbdoh1.htm](book-of-the-dead-bestiary-items/6leqktn7qesbdoh1.htm)|Desiccation Aura|Aura dessicatrice|libre|
|[6litfyrvhbcm9pup.htm](book-of-the-dead-bestiary-items/6litfyrvhbcm9pup.htm)|Warfare Lore|Connaissance de la guerre|officielle|
|[6MBpOAXA2v4U8Ojh.htm](book-of-the-dead-bestiary-items/6MBpOAXA2v4U8Ojh.htm)|Constrict|Constriction|libre|
|[6qjxhm2zgc9z1z0o.htm](book-of-the-dead-bestiary-items/6qjxhm2zgc9z1z0o.htm)|Negative Energy Plane Lore|Connaissance du Plan de l'énergie négative|libre|
|[6qp9o1qevi1znh6x.htm](book-of-the-dead-bestiary-items/6qp9o1qevi1znh6x.htm)|Twilight Spirit|Esprit de la pénombre|libre|
|[6sig7ubrfqb7uiei.htm](book-of-the-dead-bestiary-items/6sig7ubrfqb7uiei.htm)|Channel Rot|Canalisation de la pourriture|libre|
|[6tism8ur5hq61x7v.htm](book-of-the-dead-bestiary-items/6tism8ur5hq61x7v.htm)|Steal Soul|Voleur d'âme|libre|
|[6U0JQZ8ztbguo44B.htm](book-of-the-dead-bestiary-items/6U0JQZ8ztbguo44B.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[6UUahVCN4rUQxrW0.htm](book-of-the-dead-bestiary-items/6UUahVCN4rUQxrW0.htm)|At-Will Spells|Sorts à volonté|libre|
|[6yud3tv7zitkw9k9.htm](book-of-the-dead-bestiary-items/6yud3tv7zitkw9k9.htm)|Slithering Strike|Frappe rampante|libre|
|[79lxx093izwrpuih.htm](book-of-the-dead-bestiary-items/79lxx093izwrpuih.htm)|Tusk|Défense|libre|
|[7at7cp1369l1wlf5.htm](book-of-the-dead-bestiary-items/7at7cp1369l1wlf5.htm)|Warrior's Mask|Masque du guerrier|libre|
|[7avv9wov0dsg3srf.htm](book-of-the-dead-bestiary-items/7avv9wov0dsg3srf.htm)|Ravenous Void|Néant affamé|libre|
|[7hy3wqu9oefod2vm.htm](book-of-the-dead-bestiary-items/7hy3wqu9oefod2vm.htm)|Mountain Slam|Claque de montagne|libre|
|[7Iep6kSfoCj1UNVm.htm](book-of-the-dead-bestiary-items/7Iep6kSfoCj1UNVm.htm)|Rejuvenation|Reconstruction|libre|
|[7ko9o8gxy2rnu262.htm](book-of-the-dead-bestiary-items/7ko9o8gxy2rnu262.htm)|Fist|Poing|libre|
|[7lt4725ubnitaecg.htm](book-of-the-dead-bestiary-items/7lt4725ubnitaecg.htm)|Jaws|Mâchoires|libre|
|[7m7b3dwyednyhqtu.htm](book-of-the-dead-bestiary-items/7m7b3dwyednyhqtu.htm)|Denounce Heretic|Dénoncer l'hérétique|libre|
|[7qQmNwIMFsY4kIjN.htm](book-of-the-dead-bestiary-items/7qQmNwIMFsY4kIjN.htm)|Defend Territory|Défense du territoire|libre|
|[7rpcLcKUwgjQVUST.htm](book-of-the-dead-bestiary-items/7rpcLcKUwgjQVUST.htm)|Sneak Attack|Attaque sournoise|libre|
|[7spg3bnxup4hapkm.htm](book-of-the-dead-bestiary-items/7spg3bnxup4hapkm.htm)|Branch|Branche|libre|
|[7wp3tf6myiuh5zf6.htm](book-of-the-dead-bestiary-items/7wp3tf6myiuh5zf6.htm)|Mandible|Mandibules|libre|
|[80uvreq1zdclqkeo.htm](book-of-the-dead-bestiary-items/80uvreq1zdclqkeo.htm)|Tumultuous Flash|Éclat tumultueux|libre|
|[81Htx72275BlwBKF.htm](book-of-the-dead-bestiary-items/81Htx72275BlwBKF.htm)|Weep Blood|Pleurs de sang|libre|
|[83bRw8wnX0fjOtf2.htm](book-of-the-dead-bestiary-items/83bRw8wnX0fjOtf2.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[83mbwwys64b7tvby.htm](book-of-the-dead-bestiary-items/83mbwwys64b7tvby.htm)|Sense Companion|Perception de compagnon|libre|
|[88vGaWxuqoaedx3J.htm](book-of-the-dead-bestiary-items/88vGaWxuqoaedx3J.htm)|Arcane Innate Spells|Sorts arcaniques innés|libre|
|[88ymu96vgm67d1yi.htm](book-of-the-dead-bestiary-items/88ymu96vgm67d1yi.htm)|Vengeful Suffocation|Suffocation vengeresse|libre|
|[8BdsrDdI55FRLHOf.htm](book-of-the-dead-bestiary-items/8BdsrDdI55FRLHOf.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[8cgtf6p2b39hnee5.htm](book-of-the-dead-bestiary-items/8cgtf6p2b39hnee5.htm)|Fragile Wings|Ailes fragiles|libre|
|[8dmnhjn5baw0ic6c.htm](book-of-the-dead-bestiary-items/8dmnhjn5baw0ic6c.htm)|Breath of Sand|Souffle de sable|libre|
|[8dn13bwe1ei77nhq.htm](book-of-the-dead-bestiary-items/8dn13bwe1ei77nhq.htm)|Shortbow|Arc court|libre|
|[8e4yMrXnRhkqRrxJ.htm](book-of-the-dead-bestiary-items/8e4yMrXnRhkqRrxJ.htm)|Darkvision|Vision dans le noir|libre|
|[8GAYsczHou1vWlDL.htm](book-of-the-dead-bestiary-items/8GAYsczHou1vWlDL.htm)|Negative Healing|Guérison négative|libre|
|[8h5yhlk6yupldtxp.htm](book-of-the-dead-bestiary-items/8h5yhlk6yupldtxp.htm)|Resurrection Vulnerability|Vulnérabilité à la résurrection|libre|
|[8jhirjg4ivj0abbm.htm](book-of-the-dead-bestiary-items/8jhirjg4ivj0abbm.htm)|Crumbling Form|Effritement de forme|libre|
|[8ki7j1n996www1th.htm](book-of-the-dead-bestiary-items/8ki7j1n996www1th.htm)|Shadowless|Sans ombres|libre|
|[8lahe1p6o2mjwdip.htm](book-of-the-dead-bestiary-items/8lahe1p6o2mjwdip.htm)|Temporal Fracturing Ray|Rayon de fracture du temps|libre|
|[8O9kWQZrwnaEV85K.htm](book-of-the-dead-bestiary-items/8O9kWQZrwnaEV85K.htm)|Improved Grab|Empoignade/Agrippement amélioré|libre|
|[8paxHAcU5Do4oAI1.htm](book-of-the-dead-bestiary-items/8paxHAcU5Do4oAI1.htm)|At-Will Spells|Sorts à volonté|libre|
|[8q0pqsiwe57oc0lw.htm](book-of-the-dead-bestiary-items/8q0pqsiwe57oc0lw.htm)|Sandstorm|Tempête de sable|libre|
|[8QQmzAl2OQsH6V6D.htm](book-of-the-dead-bestiary-items/8QQmzAl2OQsH6V6D.htm)|Constant Spells|Sorts constants|libre|
|[8sm7vCUGufEzQ4YW.htm](book-of-the-dead-bestiary-items/8sm7vCUGufEzQ4YW.htm)|Defiled Religious Symbol of Kazutal|Symbole religieux de Kazutal profané|libre|
|[8t3kp7gfpp7qfl7q.htm](book-of-the-dead-bestiary-items/8t3kp7gfpp7qfl7q.htm)|Robe Tangle|Entortillement d'écharpe|libre|
|[8ub8dfxzqtxj93eq.htm](book-of-the-dead-bestiary-items/8ub8dfxzqtxj93eq.htm)|Charge Chain|Chaîne chargée|libre|
|[90hmtfy65sjru41o.htm](book-of-the-dead-bestiary-items/90hmtfy65sjru41o.htm)|Dissonant Chord|Accord dissonnant|libre|
|[90j5smnm3ber9gfp.htm](book-of-the-dead-bestiary-items/90j5smnm3ber9gfp.htm)|Ghostly Hand|Main spectrale|libre|
|[92BrN8oQFaOLJ91I.htm](book-of-the-dead-bestiary-items/92BrN8oQFaOLJ91I.htm)|Darkvision|Vision dans le noir|libre|
|[942tW0398ldkqoyk.htm](book-of-the-dead-bestiary-items/942tW0398ldkqoyk.htm)|Fast Healing 10|Guérison accélérée 10|libre|
|[95en1txwwqbdjtdr.htm](book-of-the-dead-bestiary-items/95en1txwwqbdjtdr.htm)|Implacable Advance|Avancée implacable|libre|
|[9br1r4rn2iiosza8.htm](book-of-the-dead-bestiary-items/9br1r4rn2iiosza8.htm)|Battlefield Bound|Lié au champ de bataille|libre|
|[9bucmn723g63yhg1.htm](book-of-the-dead-bestiary-items/9bucmn723g63yhg1.htm)|Jaws|Mâchoires|libre|
|[9BZlVPtKWPrqj2a3.htm](book-of-the-dead-bestiary-items/9BZlVPtKWPrqj2a3.htm)|Negative Healing|Guérison négative|libre|
|[9cot9pecrjfwekp6.htm](book-of-the-dead-bestiary-items/9cot9pecrjfwekp6.htm)|Vomit Blood|Vomissement de sang|libre|
|[9lqmxwvb8gf3pki2.htm](book-of-the-dead-bestiary-items/9lqmxwvb8gf3pki2.htm)|Siphon Vitality|Aspiration de la vitalité|libre|
|[9nhauoark4hd73ic.htm](book-of-the-dead-bestiary-items/9nhauoark4hd73ic.htm)|Thoughtsense 60 feet|Perception des pensées 18 m|libre|
|[9Osaf1jaTvEKFmUM.htm](book-of-the-dead-bestiary-items/9Osaf1jaTvEKFmUM.htm)|Ruinous Weapons|Arme du désastre|libre|
|[9Qeobioo0LmSzjQg.htm](book-of-the-dead-bestiary-items/9Qeobioo0LmSzjQg.htm)|Darkvision|Vision dans le noir|libre|
|[9RsQKHAZSLUD8OaG.htm](book-of-the-dead-bestiary-items/9RsQKHAZSLUD8OaG.htm)|Darkvision|Vision dans le noir|libre|
|[9uwteydd6ypzaa2e.htm](book-of-the-dead-bestiary-items/9uwteydd6ypzaa2e.htm)|Tactical Direction|Ordre tactique|libre|
|[9X9LnWN67AI3Wyjr.htm](book-of-the-dead-bestiary-items/9X9LnWN67AI3Wyjr.htm)|Rejuvenation|Reconstruction|libre|
|[9xtciwtearfnw9g0.htm](book-of-the-dead-bestiary-items/9xtciwtearfnw9g0.htm)|Undying Vendetta|Vendetta immortelle|libre|
|[9y280jhc7njt1nez.htm](book-of-the-dead-bestiary-items/9y280jhc7njt1nez.htm)|Lair Sense|Perception du repaire|libre|
|[a0e3JLzKlammUKQN.htm](book-of-the-dead-bestiary-items/a0e3JLzKlammUKQN.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[a34aowwyzlmdtpg4.htm](book-of-the-dead-bestiary-items/a34aowwyzlmdtpg4.htm)|Ghostly Grasp|Étreinte fantomatique|libre|
|[a380z0ervv71womf.htm](book-of-the-dead-bestiary-items/a380z0ervv71womf.htm)|Wing|Aile|libre|
|[a3wMuTNkVSHLbp4T.htm](book-of-the-dead-bestiary-items/a3wMuTNkVSHLbp4T.htm)|Darkvision|Vision dans le noir|libre|
|[a7gh1zpyblagcti4.htm](book-of-the-dead-bestiary-items/a7gh1zpyblagcti4.htm)|Death Gasp|Soupir de mort|libre|
|[a7w43julclvnfw63.htm](book-of-the-dead-bestiary-items/a7w43julclvnfw63.htm)|Drain Spell Tome|Drain de grimoire|libre|
|[a9ir7xv426ririzq.htm](book-of-the-dead-bestiary-items/a9ir7xv426ririzq.htm)|Surge of Speed|Déferlement de vitesse|libre|
|[a9iRrDBwDQtFML4a.htm](book-of-the-dead-bestiary-items/a9iRrDBwDQtFML4a.htm)|Darkvision|Vision dans le noir|libre|
|[AbEjyP0syA7mzlA8.htm](book-of-the-dead-bestiary-items/AbEjyP0syA7mzlA8.htm)|Air Walk (Constant)|Marche dans les airs (constant)|officielle|
|[abi4luruyxmz1ht3.htm](book-of-the-dead-bestiary-items/abi4luruyxmz1ht3.htm)|Survival|Survie|libre|
|[ad1bef6ocdexoep9.htm](book-of-the-dead-bestiary-items/ad1bef6ocdexoep9.htm)|Fangs|Crocs|libre|
|[aDtuEVLZvPSFSJTZ.htm](book-of-the-dead-bestiary-items/aDtuEVLZvPSFSJTZ.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[ai6gr48jowwpnc0m.htm](book-of-the-dead-bestiary-items/ai6gr48jowwpnc0m.htm)|Ravenous Undoing|Voracité de survie|libre|
|[AjjzMtoQOxAQvqf9.htm](book-of-the-dead-bestiary-items/AjjzMtoQOxAQvqf9.htm)|Negative Healing|Guérison négative|libre|
|[al020n2uibvv8exp.htm](book-of-the-dead-bestiary-items/al020n2uibvv8exp.htm)|Fist|Poing|libre|
|[am4u24k7o7diqo0t.htm](book-of-the-dead-bestiary-items/am4u24k7o7diqo0t.htm)|Woodland Dependent|Lié à la forêt|libre|
|[AmOslrFKvYuv61w1.htm](book-of-the-dead-bestiary-items/AmOslrFKvYuv61w1.htm)|Control Comatose|Contrôler les comateux|libre|
|[AnBTVVvzCtNwFDSJ.htm](book-of-the-dead-bestiary-items/AnBTVVvzCtNwFDSJ.htm)|Grab|Empoignade/Agrippement|libre|
|[aph86y82rdqgcn4k.htm](book-of-the-dead-bestiary-items/aph86y82rdqgcn4k.htm)|Control Body|Contrôle de corps|libre|
|[APlEPXARsPqzLzte.htm](book-of-the-dead-bestiary-items/APlEPXARsPqzLzte.htm)|Shuriken|+1,striking,returning|Shuriken boomerang de frappe +1|libre|
|[apx5l6z5p09xanhv.htm](book-of-the-dead-bestiary-items/apx5l6z5p09xanhv.htm)|Feign Death|Feindre la mort|libre|
|[apXBdiwzInrE6nhf.htm](book-of-the-dead-bestiary-items/apXBdiwzInrE6nhf.htm)|Negative Healing|Guérison négative|libre|
|[aqutya2asmrp8wxy.htm](book-of-the-dead-bestiary-items/aqutya2asmrp8wxy.htm)|Soul Theft|Soustraction d'âme|libre|
|[ars8cn2115cvrmwo.htm](book-of-the-dead-bestiary-items/ars8cn2115cvrmwo.htm)|Ghostly Swoop|Descente fantomatique|libre|
|[awU8eYrcBF8rib52.htm](book-of-the-dead-bestiary-items/awU8eYrcBF8rib52.htm)|Invisibility (At Will)|Invisibilité (À volonté)|officielle|
|[b146le3z824z27kz.htm](book-of-the-dead-bestiary-items/b146le3z824z27kz.htm)|Jaws|Mâchoires|libre|
|[b2dqnLa8hK88Qb7g.htm](book-of-the-dead-bestiary-items/b2dqnLa8hK88Qb7g.htm)|Déjà Vu (At Will)|Déjà vu (À volonté)|libre|
|[B2lJQF6Y6o4hnBeL.htm](book-of-the-dead-bestiary-items/B2lJQF6Y6o4hnBeL.htm)|Negative Healing|Guérison négative|libre|
|[b44nuiFLtAaurkMZ.htm](book-of-the-dead-bestiary-items/b44nuiFLtAaurkMZ.htm)|Divine Prepared Spells|Sorts divins préparés|libre|
|[b4tdi1u14a8xwxkz.htm](book-of-the-dead-bestiary-items/b4tdi1u14a8xwxkz.htm)|Change of Luck|Chance changeante|libre|
|[b4ti2yqe9ipiuxz2.htm](book-of-the-dead-bestiary-items/b4ti2yqe9ipiuxz2.htm)|Sudden Surge|Accélération soudaine|libre|
|[B4uyQyWXUec4RybT.htm](book-of-the-dead-bestiary-items/B4uyQyWXUec4RybT.htm)|Ghostly Touch|Contact fantomatique|libre|
|[b6SNymlSdtUpqtv0.htm](book-of-the-dead-bestiary-items/b6SNymlSdtUpqtv0.htm)|+2 Resilient Full Plate|Harnois de résilience +2|officielle|
|[b9qjt3e0bvq6d5wx.htm](book-of-the-dead-bestiary-items/b9qjt3e0bvq6d5wx.htm)|Trunk|Trompe|libre|
|[BcSNcGhcDxvtI9k5.htm](book-of-the-dead-bestiary-items/BcSNcGhcDxvtI9k5.htm)|Divine Prepared Spells|Sorts divins préparés|libre|
|[bCT8pcPOyNG51I6r.htm](book-of-the-dead-bestiary-items/bCT8pcPOyNG51I6r.htm)|Darkvision|Vision dans le noir|libre|
|[bd5fpgu3trmh7764.htm](book-of-the-dead-bestiary-items/bd5fpgu3trmh7764.htm)|Eat Soul|Dévorer l'âme|libre|
|[bfVGbnQelJe4aMTO.htm](book-of-the-dead-bestiary-items/bfVGbnQelJe4aMTO.htm)|Lifesense 30 feet|Perception de la vie 9 m|libre|
|[bhmy3f5zgxplyfru.htm](book-of-the-dead-bestiary-items/bhmy3f5zgxplyfru.htm)|Demesne Confinement|Confinement de domaine|libre|
|[bj3PlHtgo2gfPTmO.htm](book-of-the-dead-bestiary-items/bj3PlHtgo2gfPTmO.htm)|Darkvision|Vision dans le noir|libre|
|[bKHEkuv4wRPctK0n.htm](book-of-the-dead-bestiary-items/bKHEkuv4wRPctK0n.htm)|+26 to Initiative|+26 à l'Initiative|libre|
|[blRax867OLdVUMwk.htm](book-of-the-dead-bestiary-items/blRax867OLdVUMwk.htm)|Nature's Enmity (Only While Within the Area to Which the Bhuta is Bound)|Hostilité naturelle (seulement dans la zone à laquelle le bhuta est lié)|libre|
|[BMogxn5MHTe3lvyK.htm](book-of-the-dead-bestiary-items/BMogxn5MHTe3lvyK.htm)|Darkvision|Vision dans le noir|libre|
|[bnaNGdsV5TrxiHNp.htm](book-of-the-dead-bestiary-items/bnaNGdsV5TrxiHNp.htm)|Constant Spells|Sorts constants|libre|
|[bnge6fye2orz7wv1.htm](book-of-the-dead-bestiary-items/bnge6fye2orz7wv1.htm)|Luring Laugh|Rire séducteur|libre|
|[bnqnkgeuejbypq0u.htm](book-of-the-dead-bestiary-items/bnqnkgeuejbypq0u.htm)|Horned Rush|Charge cornue|libre|
|[bnQpqP6hIv1ISjen.htm](book-of-the-dead-bestiary-items/bnQpqP6hIv1ISjen.htm)|Negative Healing|Guérison négative|libre|
|[bpw486wu7j66tqyz.htm](book-of-the-dead-bestiary-items/bpw486wu7j66tqyz.htm)|Bloody Handprint|Empreinte de la main sanglante|libre|
|[brzonbw2amez88xj.htm](book-of-the-dead-bestiary-items/brzonbw2amez88xj.htm)|Possess Tree|Possession d'arbre|libre|
|[bT4K3qMjcsV7JMaV.htm](book-of-the-dead-bestiary-items/bT4K3qMjcsV7JMaV.htm)|Lifesense 60 feet|Perception de la vie 18 m|libre|
|[bwdkr5yl7ye33lip.htm](book-of-the-dead-bestiary-items/bwdkr5yl7ye33lip.htm)|Dagger|Dague|libre|
|[bx92ttr6lo474e66.htm](book-of-the-dead-bestiary-items/bx92ttr6lo474e66.htm)|Heretic's Armaments|Armes hérétiques|libre|
|[bXrBIlfFPcGYTa2l.htm](book-of-the-dead-bestiary-items/bXrBIlfFPcGYTa2l.htm)|Mental Rebirth|Renaissance mentale|libre|
|[byft8BkxplVfvQqd.htm](book-of-the-dead-bestiary-items/byft8BkxplVfvQqd.htm)|Speak with Plants (Constant)|Communication avec les plantes (Constant)|libre|
|[bywAbicCfjMxO2MY.htm](book-of-the-dead-bestiary-items/bywAbicCfjMxO2MY.htm)|Negative Healing|Guérison négative|libre|
|[c0cED5WAyOCC6wwV.htm](book-of-the-dead-bestiary-items/c0cED5WAyOCC6wwV.htm)|Constant Spells|Sorts constants|libre|
|[c2iKZhgPad7G2R6c.htm](book-of-the-dead-bestiary-items/c2iKZhgPad7G2R6c.htm)|Darkvision|Vision dans le noir|libre|
|[c2kagtputtatmo83.htm](book-of-the-dead-bestiary-items/c2kagtputtatmo83.htm)|Longbow|Arc long|libre|
|[c4od3wbgqhmgyp28.htm](book-of-the-dead-bestiary-items/c4od3wbgqhmgyp28.htm)|Voice of the Soul|Voix de l'âme|libre|
|[C6FyNCCwhcZO6sQX.htm](book-of-the-dead-bestiary-items/C6FyNCCwhcZO6sQX.htm)|Sneak Attack|Attaque sournoise|libre|
|[cb4gc90xqcsm6n37.htm](book-of-the-dead-bestiary-items/cb4gc90xqcsm6n37.htm)|Sense Murderer|Perception du meurtrier|libre|
|[cbmmwaxrz6a1z435.htm](book-of-the-dead-bestiary-items/cbmmwaxrz6a1z435.htm)|Divine Guardian|Gardien divin|libre|
|[CDMyFeuLiSZFWpoj.htm](book-of-the-dead-bestiary-items/CDMyFeuLiSZFWpoj.htm)|Darkvision|Vision dans le noir|libre|
|[cg2m9if702p5jhb2.htm](book-of-the-dead-bestiary-items/cg2m9if702p5jhb2.htm)|Totems of the Past|Totems du passé|libre|
|[cg7mnEAt7MBa8UwC.htm](book-of-the-dead-bestiary-items/cg7mnEAt7MBa8UwC.htm)|Vetalarana Vulnerabilities|Vulnérabilités d'un vétalaranas|libre|
|[chmnd5AocqCFMCGT.htm](book-of-the-dead-bestiary-items/chmnd5AocqCFMCGT.htm)|Frightful Presence|Présence terrifiante|libre|
|[chqupAH7uMySkbAy.htm](book-of-the-dead-bestiary-items/chqupAH7uMySkbAy.htm)|Mental Rebirth|Renaissance mentale|libre|
|[cifv4nupbufci3x9.htm](book-of-the-dead-bestiary-items/cifv4nupbufci3x9.htm)|Fangs|Crocs|libre|
|[cIJp4XSjGySMlIFb.htm](book-of-the-dead-bestiary-items/cIJp4XSjGySMlIFb.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[CItCjF3JMpPDPOGb.htm](book-of-the-dead-bestiary-items/CItCjF3JMpPDPOGb.htm)|Shamble Forth|Anvancez au combat !|libre|
|[cMH2fN0W5f937Chd.htm](book-of-the-dead-bestiary-items/cMH2fN0W5f937Chd.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[cPUJUW23ZXZ9W7Wr.htm](book-of-the-dead-bestiary-items/cPUJUW23ZXZ9W7Wr.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[csccddk7b6n1491u.htm](book-of-the-dead-bestiary-items/csccddk7b6n1491u.htm)|Field of Undeath|Champ de morts-vivants|libre|
|[csqmqowngauhnll2.htm](book-of-the-dead-bestiary-items/csqmqowngauhnll2.htm)|Agonized Howl|Hurlement d'agonisant|libre|
|[ctcrjw6hzakchqgm.htm](book-of-the-dead-bestiary-items/ctcrjw6hzakchqgm.htm)|Athletics|Athlétisme|libre|
|[cuI5TrIQCtm5H6Bt.htm](book-of-the-dead-bestiary-items/cuI5TrIQCtm5H6Bt.htm)|Scythe|Faux|libre|
|[cuvl6Xxa053PF1xX.htm](book-of-the-dead-bestiary-items/cuvl6Xxa053PF1xX.htm)|Negative Healing|Guérison négative|libre|
|[Cvmy7uikSlZ9VNtN.htm](book-of-the-dead-bestiary-items/Cvmy7uikSlZ9VNtN.htm)|Spider Climb (Constant)|Pattes d'araignées (constant)|officielle|
|[CwXMyTDGPy4bVI0z.htm](book-of-the-dead-bestiary-items/CwXMyTDGPy4bVI0z.htm)|Negative Healing|Guérison négative|libre|
|[cxlq6wpb9in9z1s2.htm](book-of-the-dead-bestiary-items/cxlq6wpb9in9z1s2.htm)|Ghostly Hand Crossbow|Arbalète de poing fantomatique|libre|
|[CZFYOk40GBWGe5U5.htm](book-of-the-dead-bestiary-items/CZFYOk40GBWGe5U5.htm)|Change Shape|Changement de forme|libre|
|[D0GXxbH8jAosuw4k.htm](book-of-the-dead-bestiary-items/D0GXxbH8jAosuw4k.htm)|Sneak Attack|Attaque sournoise|libre|
|[D1dtdbWicwawxBM3.htm](book-of-the-dead-bestiary-items/D1dtdbWicwawxBM3.htm)|Constrict|Constriction|libre|
|[d1u87ahqlsegvjml.htm](book-of-the-dead-bestiary-items/d1u87ahqlsegvjml.htm)|Javelin|Javeline|libre|
|[D2cPnadhWWwabreZ.htm](book-of-the-dead-bestiary-items/D2cPnadhWWwabreZ.htm)|Greater Constrict|Constriction supérieure|libre|
|[D5XHOv8oLEJdcY0i.htm](book-of-the-dead-bestiary-items/D5XHOv8oLEJdcY0i.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[d607zui6g37uqefz.htm](book-of-the-dead-bestiary-items/d607zui6g37uqefz.htm)|Pounce|Bond|libre|
|[da71u2jzo19jfmuf.htm](book-of-the-dead-bestiary-items/da71u2jzo19jfmuf.htm)|Channel Rot|Canalisation de la pourriture|libre|
|[daAC9eNxuMlrDoZG.htm](book-of-the-dead-bestiary-items/daAC9eNxuMlrDoZG.htm)|Grab|Empoignade/Agrippement|libre|
|[db4votcoqinbjj2s.htm](book-of-the-dead-bestiary-items/db4votcoqinbjj2s.htm)|Scythe|Faux|libre|
|[dc6VyUkQp6pOJtSn.htm](book-of-the-dead-bestiary-items/dc6VyUkQp6pOJtSn.htm)|Constrict|Constriction|libre|
|[df2mf1pstfhw330a.htm](book-of-the-dead-bestiary-items/df2mf1pstfhw330a.htm)|Tear Skin|Déchirer la peau|libre|
|[df79dbzoljgbj0mq.htm](book-of-the-dead-bestiary-items/df79dbzoljgbj0mq.htm)|Self-Loathing|Auto-dénigrement|libre|
|[dFiZIcxqM6qYIs6g.htm](book-of-the-dead-bestiary-items/dFiZIcxqM6qYIs6g.htm)|Darkvision|Vision dans le noir|libre|
|[DHn9EnlQKZ1hypSV.htm](book-of-the-dead-bestiary-items/DHn9EnlQKZ1hypSV.htm)|Darkvision|Vision dans le noir|libre|
|[dIHvHkEhLKpQPvHh.htm](book-of-the-dead-bestiary-items/dIHvHkEhLKpQPvHh.htm)|Mercantile Lore|Connaissance commerciale|libre|
|[dIjjf4Tqor8DWWHu.htm](book-of-the-dead-bestiary-items/dIjjf4Tqor8DWWHu.htm)|Swallow Whole|Gober|libre|
|[dj6gpzc5rcds6hc4.htm](book-of-the-dead-bestiary-items/dj6gpzc5rcds6hc4.htm)|Irori Lore|Connaissance d'Irori|libre|
|[Dj9dMn2OXHtfdyMz.htm](book-of-the-dead-bestiary-items/Dj9dMn2OXHtfdyMz.htm)|Whisper|Murmure|libre|
|[djU8N0rJuOlO9qqb.htm](book-of-the-dead-bestiary-items/djU8N0rJuOlO9qqb.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[dKbCkV7uzuluzoo8.htm](book-of-the-dead-bestiary-items/dKbCkV7uzuluzoo8.htm)|Darkvision|Vision dans le noir|libre|
|[dkckp2z4bbmspgcy.htm](book-of-the-dead-bestiary-items/dkckp2z4bbmspgcy.htm)|Beak|Bec|libre|
|[DLfsz7D9pLNSqIlT.htm](book-of-the-dead-bestiary-items/DLfsz7D9pLNSqIlT.htm)|Stony Shards|Éclats de pierre|libre|
|[dnu6ev9agkecmex6.htm](book-of-the-dead-bestiary-items/dnu6ev9agkecmex6.htm)|Share Blessings|Partager la bénédiction|libre|
|[dOeCzneUCh2n8Vq5.htm](book-of-the-dead-bestiary-items/dOeCzneUCh2n8Vq5.htm)|Aura of Repose|Aura de repos|libre|
|[dp0m2eqjh44d7v4a.htm](book-of-the-dead-bestiary-items/dp0m2eqjh44d7v4a.htm)|Entropy's Shadow|Ombre d'entropie|libre|
|[dpxt817prps7vv63.htm](book-of-the-dead-bestiary-items/dpxt817prps7vv63.htm)|Negative Energy Plane Lore|Connaissance du Plan de l'énergie négative|libre|
|[dqeg5ywheav84nwd.htm](book-of-the-dead-bestiary-items/dqeg5ywheav84nwd.htm)|Stench|Puanteur|libre|
|[drin3tt0epvjjsr9.htm](book-of-the-dead-bestiary-items/drin3tt0epvjjsr9.htm)|Steady Spellcasting|Incantation fiable|libre|
|[dSlveRiUSvMYcg8P.htm](book-of-the-dead-bestiary-items/dSlveRiUSvMYcg8P.htm)|Negative Healing|Guérison négative|libre|
|[DSp7AdbdrabM470N.htm](book-of-the-dead-bestiary-items/DSp7AdbdrabM470N.htm)|Surge Through|Deferlement d'intangible|libre|
|[dtbhcid8fzmfjtp7.htm](book-of-the-dead-bestiary-items/dtbhcid8fzmfjtp7.htm)|Paralyzing Spew|Vomi paralysant|libre|
|[duqnb28a7oppci1f.htm](book-of-the-dead-bestiary-items/duqnb28a7oppci1f.htm)|Tail|Queue|libre|
|[dWDxhvABpzr9Iubs.htm](book-of-the-dead-bestiary-items/dWDxhvABpzr9Iubs.htm)|Mace|+1,striking|Masse d'armes de frappe +1|libre|
|[dXqGPFgT8WHpl72i.htm](book-of-the-dead-bestiary-items/dXqGPFgT8WHpl72i.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[dYKKjbhn2q7r853p.htm](book-of-the-dead-bestiary-items/dYKKjbhn2q7r853p.htm)|Negative Healing|Guérison négative|libre|
|[DYQmHncbK51f0Cup.htm](book-of-the-dead-bestiary-items/DYQmHncbK51f0Cup.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[dz1w9j5ipev8z1v8.htm](book-of-the-dead-bestiary-items/dz1w9j5ipev8z1v8.htm)|Savvy Joinin' Me Crew?|Tu rejoins mon équipage moussaillon ?|libre|
|[DzKCI3ALLE9qQq79.htm](book-of-the-dead-bestiary-items/DzKCI3ALLE9qQq79.htm)|Shortbow|+1|Arc court +1|libre|
|[e0vfxgxi4fovzldn.htm](book-of-the-dead-bestiary-items/e0vfxgxi4fovzldn.htm)|Proboscis|Trompe|libre|
|[e7nXVy8HD6KkhPGf.htm](book-of-the-dead-bestiary-items/e7nXVy8HD6KkhPGf.htm)|One More Breath|Un souffle de plus|libre|
|[e7v5oimcd7h2wmj8.htm](book-of-the-dead-bestiary-items/e7v5oimcd7h2wmj8.htm)|Sand Form|Forme de sable|libre|
|[ecn8ua35xgwp7abu.htm](book-of-the-dead-bestiary-items/ecn8ua35xgwp7abu.htm)|Corpse Consumption|Consommation de cadavres|libre|
|[EcZYs4KlcnQ09I0F.htm](book-of-the-dead-bestiary-items/EcZYs4KlcnQ09I0F.htm)|Darkvision|Vision dans le noir|libre|
|[Ee5DMlbADQLHFvnV.htm](book-of-the-dead-bestiary-items/Ee5DMlbADQLHFvnV.htm)|Darkvision|Vision dans le noir|libre|
|[eHad4TakoeYMeM8g.htm](book-of-the-dead-bestiary-items/eHad4TakoeYMeM8g.htm)|Sneak Attack|Attaque sournoise|libre|
|[eih755f7z0quy1bu.htm](book-of-the-dead-bestiary-items/eih755f7z0quy1bu.htm)|Scythe|Faux|libre|
|[ej3i6scbur8axq00.htm](book-of-the-dead-bestiary-items/ej3i6scbur8axq00.htm)|Ghostly Cutlass|Sabre fantomatique|libre|
|[ej7vkiygh97ejpx4.htm](book-of-the-dead-bestiary-items/ej7vkiygh97ejpx4.htm)|Woodland Lore (Applies Only to the Woodland the Waldgeist is Bonded To)|Connaissance des forêts (S'applique seulement dans la forêt à laquelle est lié le waldgeist)|libre|
|[ejx371hd10a4rhfs.htm](book-of-the-dead-bestiary-items/ejx371hd10a4rhfs.htm)|Jaws|Mâchoires|libre|
|[ekhnzl2vg8r8raia.htm](book-of-the-dead-bestiary-items/ekhnzl2vg8r8raia.htm)|Revenant Reload|Recharge du revenant|libre|
|[enVGATk5fYLDSoEA.htm](book-of-the-dead-bestiary-items/enVGATk5fYLDSoEA.htm)|Negative Healing|Guérison négative|libre|
|[eO23hQJnoj5vQUSL.htm](book-of-the-dead-bestiary-items/eO23hQJnoj5vQUSL.htm)|Negative Healing|Guérison négative|libre|
|[EPaipnmixMdtaUeW.htm](book-of-the-dead-bestiary-items/EPaipnmixMdtaUeW.htm)|Negative Healing|Guérison négative|libre|
|[eq0chkkfcwtghv3v.htm](book-of-the-dead-bestiary-items/eq0chkkfcwtghv3v.htm)|Horn|Corne|libre|
|[EqqrJxTxtfnQL9mm.htm](book-of-the-dead-bestiary-items/EqqrJxTxtfnQL9mm.htm)|Sudden Siphon|Siphon soudain|libre|
|[equod2o7os2wz0wr.htm](book-of-the-dead-bestiary-items/equod2o7os2wz0wr.htm)|Claw|Griffe|libre|
|[es9ibq62fqcl30pn.htm](book-of-the-dead-bestiary-items/es9ibq62fqcl30pn.htm)|Jaws|Mâchoires|libre|
|[eu6xmtw27h5dt4bt.htm](book-of-the-dead-bestiary-items/eu6xmtw27h5dt4bt.htm)|Rapier|Rapière|libre|
|[EUiVBFw40fh2AfU0.htm](book-of-the-dead-bestiary-items/EUiVBFw40fh2AfU0.htm)|Negative Healing|Guérison négative|libre|
|[EuLz7D8KQPASHSbM.htm](book-of-the-dead-bestiary-items/EuLz7D8KQPASHSbM.htm)|Darkvision|Vision dans le noir|libre|
|[EXqKm6CdVt3ufYyT.htm](book-of-the-dead-bestiary-items/EXqKm6CdVt3ufYyT.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[eyfrpq3bvxnh075k.htm](book-of-the-dead-bestiary-items/eyfrpq3bvxnh075k.htm)|Silent Aura|Aura de silence|libre|
|[eymmc9qbqyp94ywu.htm](book-of-the-dead-bestiary-items/eymmc9qbqyp94ywu.htm)|Inspire the Faithless|Inspiration des hérétiques|libre|
|[ezj7d0ajdqts6n00.htm](book-of-the-dead-bestiary-items/ezj7d0ajdqts6n00.htm)|Tail|Queue|libre|
|[f006egqkkbtjuqgv.htm](book-of-the-dead-bestiary-items/f006egqkkbtjuqgv.htm)|Blinding Breath|Souffle aveuglant|libre|
|[f303t130pkbopzk0.htm](book-of-the-dead-bestiary-items/f303t130pkbopzk0.htm)|Jaws|Mâchoires|libre|
|[f3HhM7YpmtEJwP4y.htm](book-of-the-dead-bestiary-items/f3HhM7YpmtEJwP4y.htm)|Negative Healing|Guérison négative|libre|
|[f55now7bu1g173vg.htm](book-of-the-dead-bestiary-items/f55now7bu1g173vg.htm)|Entropy's Shadow|Ombre d'entropie|libre|
|[f84cxpe3e8us7653.htm](book-of-the-dead-bestiary-items/f84cxpe3e8us7653.htm)|Claw|Griffe|libre|
|[fba14HId1lw0Kex9.htm](book-of-the-dead-bestiary-items/fba14HId1lw0Kex9.htm)|Darkvision|Vision dans le noir|libre|
|[FByIi64kp8hJjz6z.htm](book-of-the-dead-bestiary-items/FByIi64kp8hJjz6z.htm)|Tremorsense (Imprecise) 60 feet|Perception des vibrations 18 m (imprécis)|libre|
|[fc9jut8qh45m7eoj.htm](book-of-the-dead-bestiary-items/fc9jut8qh45m7eoj.htm)|Gladiatorial Lore|Connaissance des gladiateurs|libre|
|[fczuoyvm5wqij2to.htm](book-of-the-dead-bestiary-items/fczuoyvm5wqij2to.htm)|Intense Heat|Chaleur intense|libre|
|[Fdv00C87iITzPRVA.htm](book-of-the-dead-bestiary-items/Fdv00C87iITzPRVA.htm)|Staff|+1|Bâton +1|libre|
|[fegyOi6VDq8sD5vj.htm](book-of-the-dead-bestiary-items/fegyOi6VDq8sD5vj.htm)|Wand of Dispel Magic (Level 6)|Baguette de Dissipation de la magie (Niveau 6)|libre|
|[fet1zrdx8aapum04.htm](book-of-the-dead-bestiary-items/fet1zrdx8aapum04.htm)|Compression|Compression|libre|
|[Fg4PN7JgMiI91eH5.htm](book-of-the-dead-bestiary-items/Fg4PN7JgMiI91eH5.htm)|Negative Healing|Guérison négative|libre|
|[fHKSsTCMOscHoG6D.htm](book-of-the-dead-bestiary-items/fHKSsTCMOscHoG6D.htm)|Speak with Animals (Constant)|Communication avec les animaux (constant)|officielle|
|[FHXomEYheB5tF9Et.htm](book-of-the-dead-bestiary-items/FHXomEYheB5tF9Et.htm)|Aquatic Ambush|Embuscade aquatique|libre|
|[fIhgzWWiqFCcA6nV.htm](book-of-the-dead-bestiary-items/fIhgzWWiqFCcA6nV.htm)|Conjure Instruments|Invocation d'instruments|libre|
|[fk6qtcr37ue5mbxw.htm](book-of-the-dead-bestiary-items/fk6qtcr37ue5mbxw.htm)|Terrible Foresight|Préscience terrifiante|libre|
|[FKLyRZ60trNAaU7a.htm](book-of-the-dead-bestiary-items/FKLyRZ60trNAaU7a.htm)|Negative Healing|Guérison négative|libre|
|[fN1JV4sjRyAtaSwd.htm](book-of-the-dead-bestiary-items/fN1JV4sjRyAtaSwd.htm)|Crossbow|+1,striking|Arbalète de frappe +1|libre|
|[fn7qja3kw9dw1kox.htm](book-of-the-dead-bestiary-items/fn7qja3kw9dw1kox.htm)|Incorporeal Wheel|Roues intangibles|libre|
|[fp4p44l0wgn5altu.htm](book-of-the-dead-bestiary-items/fp4p44l0wgn5altu.htm)|Consecration Vulnerability|Vulnérable à la consécration|libre|
|[FRqXWcdzhlxU8sMh.htm](book-of-the-dead-bestiary-items/FRqXWcdzhlxU8sMh.htm)|Darkvision|Vision dans le noir|libre|
|[FRWKAh9vjYaNCqdT.htm](book-of-the-dead-bestiary-items/FRWKAh9vjYaNCqdT.htm)|Negative Healing|Guérison négative|libre|
|[fSXafbSBzAADlGcn.htm](book-of-the-dead-bestiary-items/fSXafbSBzAADlGcn.htm)|Cleric Domain Spells|Sorts de domaine de prêtre|libre|
|[FTPKGpv0QRKEpmnY.htm](book-of-the-dead-bestiary-items/FTPKGpv0QRKEpmnY.htm)|Cleric Domain Spells|Sorts de domaine de prêtre|libre|
|[FUCaE5JHQFv1Gc0a.htm](book-of-the-dead-bestiary-items/FUCaE5JHQFv1Gc0a.htm)|Cleric Domain Spells|Sorts de domaine de prêtre|libre|
|[fv0690n3y8ghmvd9.htm](book-of-the-dead-bestiary-items/fv0690n3y8ghmvd9.htm)|Claw|Griffe|libre|
|[fxc64qicfzi9l9lx.htm](book-of-the-dead-bestiary-items/fxc64qicfzi9l9lx.htm)|Rotten Fruit|Fruits pourris|libre|
|[FzDSsJ65EnkMhwSx.htm](book-of-the-dead-bestiary-items/FzDSsJ65EnkMhwSx.htm)|Negative Healing|Guérison négative|libre|
|[fzpmews5ufzi1k6e.htm](book-of-the-dead-bestiary-items/fzpmews5ufzi1k6e.htm)|Strangle|Étranglement|libre|
|[g3oCwdk0aWScgLBq.htm](book-of-the-dead-bestiary-items/g3oCwdk0aWScgLBq.htm)|Tongues (Constant)|Don des langues (constant)|officielle|
|[g3UMdUkuFL5RdKPP.htm](book-of-the-dead-bestiary-items/g3UMdUkuFL5RdKPP.htm)|Grab|Empoignade/Agrippement|libre|
|[g9hvfpvweahguxgh.htm](book-of-the-dead-bestiary-items/g9hvfpvweahguxgh.htm)|Create Zombies|Création de zombies|libre|
|[ga6hycubofuo1zyf.htm](book-of-the-dead-bestiary-items/ga6hycubofuo1zyf.htm)|Claw|Griffe|libre|
|[gB214qjiG2XEJPBR.htm](book-of-the-dead-bestiary-items/gB214qjiG2XEJPBR.htm)|Darkvision|Vision dans le noir|libre|
|[gB36aH5yDgmXH6xc.htm](book-of-the-dead-bestiary-items/gB36aH5yDgmXH6xc.htm)|Squeeze|Serrer|libre|
|[gb96b0rpdnd1o31t.htm](book-of-the-dead-bestiary-items/gb96b0rpdnd1o31t.htm)|Shadow Plane Lore|Connaissance du plan de l'ombre|officielle|
|[gdfTOvhcUidHz9fM.htm](book-of-the-dead-bestiary-items/gdfTOvhcUidHz9fM.htm)|Defiled Religious Symbol of Nethys|Symbole religieux de Néthys profané|libre|
|[gDkkvEZZSC6HPnDm.htm](book-of-the-dead-bestiary-items/gDkkvEZZSC6HPnDm.htm)|Command Zombie|Commandement de zombie|libre|
|[gE5SOeGR6RJsEJJ1.htm](book-of-the-dead-bestiary-items/gE5SOeGR6RJsEJJ1.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[GFbwZSSnTHj0YPuv.htm](book-of-the-dead-bestiary-items/GFbwZSSnTHj0YPuv.htm)|Darkvision|Vision dans le noir|libre|
|[ggz12biwbgr3mv1o.htm](book-of-the-dead-bestiary-items/ggz12biwbgr3mv1o.htm)|Lesser Mummy Rot|Putréfaction de la momie inférieure|libre|
|[ghtfq7ukz7gwxwsb.htm](book-of-the-dead-bestiary-items/ghtfq7ukz7gwxwsb.htm)|Draconic Frenzy|Frénésie draconique|libre|
|[gi4s0lkv3mfb9loi.htm](book-of-the-dead-bestiary-items/gi4s0lkv3mfb9loi.htm)|Fist|Poing|libre|
|[girs49ehz5bzw3df.htm](book-of-the-dead-bestiary-items/girs49ehz5bzw3df.htm)|Anticipatory Attack|Attaque anticipée|libre|
|[gizkkpi42rqcq7rz.htm](book-of-the-dead-bestiary-items/gizkkpi42rqcq7rz.htm)|Jaws|Mâchoires|libre|
|[GJBlX4Lhocg26ELX.htm](book-of-the-dead-bestiary-items/GJBlX4Lhocg26ELX.htm)|Negative Healing|Guérison négative|libre|
|[gkW3gWYUJOzFKerr.htm](book-of-the-dead-bestiary-items/gkW3gWYUJOzFKerr.htm)|Lifesense 60 feet|Perception de la vie 18 m|libre|
|[gmcd6ukh14nzdcez.htm](book-of-the-dead-bestiary-items/gmcd6ukh14nzdcez.htm)|Intimidation|Intimidation|libre|
|[gocrv4xvdtl8n7hl.htm](book-of-the-dead-bestiary-items/gocrv4xvdtl8n7hl.htm)|Blazing Howl|Hurlement embrasé|libre|
|[gomtgkod7rqvcpxq.htm](book-of-the-dead-bestiary-items/gomtgkod7rqvcpxq.htm)|Stamping Foot|Taper du pied|libre|
|[GrurNCdRZ7stOOB7.htm](book-of-the-dead-bestiary-items/GrurNCdRZ7stOOB7.htm)|Grab|Empoignade/Agrippement|libre|
|[gsUuAStZr4pb7zis.htm](book-of-the-dead-bestiary-items/gsUuAStZr4pb7zis.htm)|Improved Grab|Empoignade/Agrippement amélioré|libre|
|[GtDhQviwk4x4rmKZ.htm](book-of-the-dead-bestiary-items/GtDhQviwk4x4rmKZ.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[gUJbIPYPpxPHfMRO.htm](book-of-the-dead-bestiary-items/gUJbIPYPpxPHfMRO.htm)|+1 Scale Mail|Armure d'écailles +1|libre|
|[gw4pcdfvx6elv0g3.htm](book-of-the-dead-bestiary-items/gw4pcdfvx6elv0g3.htm)|Siphon Faith|Aspiration de la foi|libre|
|[gwzami23yywnrl99.htm](book-of-the-dead-bestiary-items/gwzami23yywnrl99.htm)|Slow|Lent|libre|
|[GX7ZYuLk1vIcTVZQ.htm](book-of-the-dead-bestiary-items/GX7ZYuLk1vIcTVZQ.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[gy0jf8etpgdnoddm.htm](book-of-the-dead-bestiary-items/gy0jf8etpgdnoddm.htm)|Natural Invisibility|Invisibilité naturelle|libre|
|[H0qYKDck9uqk3ZiC.htm](book-of-the-dead-bestiary-items/H0qYKDck9uqk3ZiC.htm)|Capture|Capture|libre|
|[h189cx7txluockka.htm](book-of-the-dead-bestiary-items/h189cx7txluockka.htm)|Shadow Rapier|Rapière des ombres|libre|
|[H1YR8lmJxjQ8GKTf.htm](book-of-the-dead-bestiary-items/H1YR8lmJxjQ8GKTf.htm)|Plane Shift (to Material Plane, Negative Energy Plane, or Shadow Plane only)|Changement de plan (vers le plan Matériel, de l' Énergie négative et de l'Ombre uniquement)|libre|
|[h305evdhewgfdv8w.htm](book-of-the-dead-bestiary-items/h305evdhewgfdv8w.htm)|Field of Undeath|Champ de morts-vivants|libre|
|[H3cDChRjDRA71MNI.htm](book-of-the-dead-bestiary-items/H3cDChRjDRA71MNI.htm)|Lifesense (Imprecise) 60 feet|Perception de la vie 18 m (imprécis)|libre|
|[h4i7zh8y17d35lou.htm](book-of-the-dead-bestiary-items/h4i7zh8y17d35lou.htm)|Jaws|Mâchoires|libre|
|[h4iw6qojb9gael8e.htm](book-of-the-dead-bestiary-items/h4iw6qojb9gael8e.htm)|Talon|Serre|libre|
|[h6ms55ripngjxrjb.htm](book-of-the-dead-bestiary-items/h6ms55ripngjxrjb.htm)|Eroding Touch|Contact érosif|libre|
|[h7zcdyu7ovmxa2ya.htm](book-of-the-dead-bestiary-items/h7zcdyu7ovmxa2ya.htm)|Claw|Griffe|libre|
|[h9Qhc5vy2FgrUSrb.htm](book-of-the-dead-bestiary-items/h9Qhc5vy2FgrUSrb.htm)|Darkvision|Vision dans le noir|libre|
|[H9XeL3LQ06KgkEE1.htm](book-of-the-dead-bestiary-items/H9XeL3LQ06KgkEE1.htm)|Frenzy|Frénésie|libre|
|[HdJVXKg1EjumVlC0.htm](book-of-the-dead-bestiary-items/HdJVXKg1EjumVlC0.htm)|Dagger|+1,striking|Dague de frappe +1|libre|
|[HeQVQ64EF8HjzyyO.htm](book-of-the-dead-bestiary-items/HeQVQ64EF8HjzyyO.htm)|Knockdown|Renversement|libre|
|[hG2ocnUMjZCucqB2.htm](book-of-the-dead-bestiary-items/hG2ocnUMjZCucqB2.htm)|Horn|Corne|libre|
|[hGLTi7qRn9Hbn8VM.htm](book-of-the-dead-bestiary-items/hGLTi7qRn9Hbn8VM.htm)|Rigor Mortis|Rigor mortis|libre|
|[HgX8EdBcuekHoaYB.htm](book-of-the-dead-bestiary-items/HgX8EdBcuekHoaYB.htm)|Constrict|Constriction|libre|
|[hjg7z96c9j7w5prb.htm](book-of-the-dead-bestiary-items/hjg7z96c9j7w5prb.htm)|Warfare Lore|Connaissance de la guerre|officielle|
|[hJqwMBdBQecpuWU8.htm](book-of-the-dead-bestiary-items/hJqwMBdBQecpuWU8.htm)|Negative Healing|Guérison négative|libre|
|[hma3kv808zk32z1k.htm](book-of-the-dead-bestiary-items/hma3kv808zk32z1k.htm)|Spear|Lance|libre|
|[hndwksut71ohgybj.htm](book-of-the-dead-bestiary-items/hndwksut71ohgybj.htm)|Midnight Depths|Profondeurs de minuit|libre|
|[HPESx2nabKEHNgX2.htm](book-of-the-dead-bestiary-items/HPESx2nabKEHNgX2.htm)|Air Walk (Constant)|Marche dans les airs (constant)|officielle|
|[hrwH6CDrMAcUOAA4.htm](book-of-the-dead-bestiary-items/hrwH6CDrMAcUOAA4.htm)|Constant Spells|Sorts constants|libre|
|[htqhazfmsy64d5iz.htm](book-of-the-dead-bestiary-items/htqhazfmsy64d5iz.htm)|Entropy's Shadow|Ombre d'entropie|libre|
|[HUgw20uZ51R7a9a4.htm](book-of-the-dead-bestiary-items/HUgw20uZ51R7a9a4.htm)|Topple Furniture|Renverser le meuble|libre|
|[HUIxZptlwIab6WRD.htm](book-of-the-dead-bestiary-items/HUIxZptlwIab6WRD.htm)|Darkvision|Vision dans le noir|libre|
|[hvmeWsfKjMqgYhBi.htm](book-of-the-dead-bestiary-items/hvmeWsfKjMqgYhBi.htm)|Negative Healing|Guérison négative|libre|
|[HWy6SULhipZ1fnRK.htm](book-of-the-dead-bestiary-items/HWy6SULhipZ1fnRK.htm)|Darkvision|Vision dans le noir|libre|
|[hz33scglipei6wyh.htm](book-of-the-dead-bestiary-items/hz33scglipei6wyh.htm)|Claw|Griffe|libre|
|[hz9E9TnkviNuCCnq.htm](book-of-the-dead-bestiary-items/hz9E9TnkviNuCCnq.htm)|Gasp|Halètement|libre|
|[HZCvfp07Tr6TQA6s.htm](book-of-the-dead-bestiary-items/HZCvfp07Tr6TQA6s.htm)|Darkvision|Vision dans le noir|libre|
|[i2zp6vcnxl3idh5a.htm](book-of-the-dead-bestiary-items/i2zp6vcnxl3idh5a.htm)|Tail|Queue|libre|
|[i3b5R0drNc9jYAxQ.htm](book-of-the-dead-bestiary-items/i3b5R0drNc9jYAxQ.htm)|Monk Ki Spells|Sorts de ki de moine|libre|
|[i45nUr4U6ijZvV0X.htm](book-of-the-dead-bestiary-items/i45nUr4U6ijZvV0X.htm)|Negative Healing|Guérison négative|libre|
|[i5QEKnRQOxdbucFa.htm](book-of-the-dead-bestiary-items/i5QEKnRQOxdbucFa.htm)|Grab|Empoignade/Agrippement|libre|
|[i7d6smiJzgZMN4eM.htm](book-of-the-dead-bestiary-items/i7d6smiJzgZMN4eM.htm)|Negative Healing|Guérison négative|libre|
|[IAkmYPAen89nAVOl.htm](book-of-the-dead-bestiary-items/IAkmYPAen89nAVOl.htm)|Constant Spells|Sorts constants|libre|
|[ibAx8T7dlGLGUNfN.htm](book-of-the-dead-bestiary-items/ibAx8T7dlGLGUNfN.htm)|Negative Healing|Guérison négative|libre|
|[Icnp6B80vyyxB58y.htm](book-of-the-dead-bestiary-items/Icnp6B80vyyxB58y.htm)|Lifesense 30 feet|Perception de la vie 9 m|libre|
|[ie93zawg4aydi426.htm](book-of-the-dead-bestiary-items/ie93zawg4aydi426.htm)|Longsword|Épée longue|libre|
|[IeIZXKmchA7BgVL3.htm](book-of-the-dead-bestiary-items/IeIZXKmchA7BgVL3.htm)|Lifesense 60 feet|Perception de la vie 18 m|libre|
|[ifsi8iYNheUHJYSo.htm](book-of-the-dead-bestiary-items/ifsi8iYNheUHJYSo.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[igdqv17bti9ke6yv.htm](book-of-the-dead-bestiary-items/igdqv17bti9ke6yv.htm)|Dead Shot|Tireur d'élite|libre|
|[IgpRQqNu2wC1srux.htm](book-of-the-dead-bestiary-items/IgpRQqNu2wC1srux.htm)|Negative Healing|Guérison négative|libre|
|[igSuS5pwnNIqzoiZ.htm](book-of-the-dead-bestiary-items/igSuS5pwnNIqzoiZ.htm)|Negative Healing|Guérison négative|libre|
|[IhOPZKCB3vAG0fWj.htm](book-of-the-dead-bestiary-items/IhOPZKCB3vAG0fWj.htm)|Darkvision|Vision dans le noir|libre|
|[IiHW4mzCpj9P0zUh.htm](book-of-the-dead-bestiary-items/IiHW4mzCpj9P0zUh.htm)|Negative Healing|Guérison négative|libre|
|[IMlXx0dxH2pyVYkZ.htm](book-of-the-dead-bestiary-items/IMlXx0dxH2pyVYkZ.htm)|Negative Healing|Guérison négative|libre|
|[ioRiyfYOVSBwJU1I.htm](book-of-the-dead-bestiary-items/ioRiyfYOVSBwJU1I.htm)|Divine Prepared Spells|Sorts divins préparés|libre|
|[ipzqyff0deghe3l9.htm](book-of-the-dead-bestiary-items/ipzqyff0deghe3l9.htm)|Funereal Touch|Touché funéraire|libre|
|[iqh47015p1pztflo.htm](book-of-the-dead-bestiary-items/iqh47015p1pztflo.htm)|Force Feed|Nourrir de force|libre|
|[irqrb4xjw4fid7ki.htm](book-of-the-dead-bestiary-items/irqrb4xjw4fid7ki.htm)|Fist|Poing|libre|
|[isudkibiz4ctaaai.htm](book-of-the-dead-bestiary-items/isudkibiz4ctaaai.htm)|Mace|Masse d'armes|libre|
|[IvNNdk1lrY4Xyd5o.htm](book-of-the-dead-bestiary-items/IvNNdk1lrY4Xyd5o.htm)|Troop Movement|Mouvement de troupe|libre|
|[IWjuKGNq99Q9FpWw.htm](book-of-the-dead-bestiary-items/IWjuKGNq99Q9FpWw.htm)|Negative Healing|Guérison négative|libre|
|[j0godn9ynpr21ye8.htm](book-of-the-dead-bestiary-items/j0godn9ynpr21ye8.htm)|Possess Animal|Possession d'un animal|libre|
|[j0tNYHtR2k2ufUJL.htm](book-of-the-dead-bestiary-items/j0tNYHtR2k2ufUJL.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[jajq24ckigdi8yla.htm](book-of-the-dead-bestiary-items/jajq24ckigdi8yla.htm)|Rosebriar Lash|Fouet d'épines de rose|libre|
|[jdhshd6p6earqvr9.htm](book-of-the-dead-bestiary-items/jdhshd6p6earqvr9.htm)|Waves of Sorrow|Vagues de tristesse|libre|
|[je6z12w9bwlsz6kk.htm](book-of-the-dead-bestiary-items/je6z12w9bwlsz6kk.htm)|Incessant Goading|Provocations incessantes|libre|
|[jeo96qnICvlBhjJ0.htm](book-of-the-dead-bestiary-items/jeo96qnICvlBhjJ0.htm)|+1 Status to All Saves vs. Magic|bonus de statut de +1 aux JdS contre la magie|libre|
|[jfbv6zyvbqlc7a83.htm](book-of-the-dead-bestiary-items/jfbv6zyvbqlc7a83.htm)|Shadow Plane Lore|Connaissance du plan de l'ombre|officielle|
|[jfD7HOJyLqdUeAsO.htm](book-of-the-dead-bestiary-items/jfD7HOJyLqdUeAsO.htm)|Negative Healing|Guérison négative|libre|
|[jjm9aum1hxm0xf4c.htm](book-of-the-dead-bestiary-items/jjm9aum1hxm0xf4c.htm)|Command Zombie|Commandement de zombie|libre|
|[JKpaLr9UX648Hgix.htm](book-of-the-dead-bestiary-items/JKpaLr9UX648Hgix.htm)|Infused Reagents|Réactifs imprégnés|officielle|
|[JLuKV6PpCsAJSsQD.htm](book-of-the-dead-bestiary-items/JLuKV6PpCsAJSsQD.htm)|Champion Devotion Spells|Sorts de dévotion de champion|libre|
|[jmhsum5l0sk7d9ru.htm](book-of-the-dead-bestiary-items/jmhsum5l0sk7d9ru.htm)|Polong Possession|Possession polong|libre|
|[JNoGBCYpptNAJ1M0.htm](book-of-the-dead-bestiary-items/JNoGBCYpptNAJ1M0.htm)|Shatter|Brisure|libre|
|[jQEBpMjZVXEPDHQy.htm](book-of-the-dead-bestiary-items/jQEBpMjZVXEPDHQy.htm)|Drain Thoughts|Drain de pensées|libre|
|[jVvenC8VtGesd99v.htm](book-of-the-dead-bestiary-items/jVvenC8VtGesd99v.htm)|Graveknight's Curse|Malédiction du chevalier sépulcre|libre|
|[jWKQduPw868WuVOj.htm](book-of-the-dead-bestiary-items/jWKQduPw868WuVOj.htm)|Negative Healing|Guérison négative|libre|
|[JxEmM6BuyP2WDNYs.htm](book-of-the-dead-bestiary-items/JxEmM6BuyP2WDNYs.htm)|Grab|Empoignade/Agrippement|libre|
|[k55F7rzQpnXtBEJV.htm](book-of-the-dead-bestiary-items/k55F7rzQpnXtBEJV.htm)|Rend|Éventration|libre|
|[k7dcbgnjv5bmyic2.htm](book-of-the-dead-bestiary-items/k7dcbgnjv5bmyic2.htm)|Targeted Collapse|Effondrement ciblé|libre|
|[KB3dAuCJuP1fvJj1.htm](book-of-the-dead-bestiary-items/KB3dAuCJuP1fvJj1.htm)|Darkvision|Vision dans le noir|libre|
|[kDcWkpiVlvQCKNaQ.htm](book-of-the-dead-bestiary-items/kDcWkpiVlvQCKNaQ.htm)|Religious Symbol of Set|Symbole religieux de Seth|libre|
|[KfMljjVvrcltFSTJ.htm](book-of-the-dead-bestiary-items/KfMljjVvrcltFSTJ.htm)|Negative Healing|Guérison négative|libre|
|[kfufm3rf9agn7hj4.htm](book-of-the-dead-bestiary-items/kfufm3rf9agn7hj4.htm)|Hand|Main|libre|
|[kg8pmvmx5h75yhea.htm](book-of-the-dead-bestiary-items/kg8pmvmx5h75yhea.htm)|Warfare Lore|Connaissance de la guerre|officielle|
|[kgbHU2XWV7rcVSRL.htm](book-of-the-dead-bestiary-items/kgbHU2XWV7rcVSRL.htm)|Flute|Flûte|libre|
|[KGi4YZQdbv764frU.htm](book-of-the-dead-bestiary-items/KGi4YZQdbv764frU.htm)|Trample|Piétinement|libre|
|[khs8jsznmljq10no.htm](book-of-the-dead-bestiary-items/khs8jsznmljq10no.htm)|Talon|Serre|libre|
|[ki62sc8x79ypbv1d.htm](book-of-the-dead-bestiary-items/ki62sc8x79ypbv1d.htm)|Foot|Pied|libre|
|[kijygqC36kPqtu7H.htm](book-of-the-dead-bestiary-items/kijygqC36kPqtu7H.htm)|Darkvision|Vision dans le noir|libre|
|[KKDdzLVqG8oTmkVx.htm](book-of-the-dead-bestiary-items/KKDdzLVqG8oTmkVx.htm)|Shield Block|Blocage au bouclier|libre|
|[kl1jb22cgwiuukv9.htm](book-of-the-dead-bestiary-items/kl1jb22cgwiuukv9.htm)|Draining Gaze|Regard fixe drainant|libre|
|[kpogcinxfxdtnd5j.htm](book-of-the-dead-bestiary-items/kpogcinxfxdtnd5j.htm)|Gallow Curse|Malédiction de potence|libre|
|[kPpdhRglPpemLa6O.htm](book-of-the-dead-bestiary-items/kPpdhRglPpemLa6O.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[krx5EeEB0stlZv3X.htm](book-of-the-dead-bestiary-items/krx5EeEB0stlZv3X.htm)|Lifesense 60 feet|Perception de la vie 18 m|libre|
|[KSMsQZQvHWveKzzQ.htm](book-of-the-dead-bestiary-items/KSMsQZQvHWveKzzQ.htm)|Entangle (At-Will)|Enchevêtrement (À volonté)|libre|
|[KY0t4zPgTeWUaAUM.htm](book-of-the-dead-bestiary-items/KY0t4zPgTeWUaAUM.htm)|Swift Leap|Bond rapide|libre|
|[kYeCwvGuYQYBi0jb.htm](book-of-the-dead-bestiary-items/kYeCwvGuYQYBi0jb.htm)|Tongues (Constant)|Don des langues (constant)|officielle|
|[kyvbc251jzpofo6c.htm](book-of-the-dead-bestiary-items/kyvbc251jzpofo6c.htm)|Lore|Connaissance|officielle|
|[L0BqA6ioP9NTqswd.htm](book-of-the-dead-bestiary-items/L0BqA6ioP9NTqswd.htm)|Negative Healing|Guérison négative|libre|
|[l13as6h1cnru8t59.htm](book-of-the-dead-bestiary-items/l13as6h1cnru8t59.htm)|Nethys Lore|Connaissance de Néthys|libre|
|[l525qc20u09lsupe.htm](book-of-the-dead-bestiary-items/l525qc20u09lsupe.htm)|Rhapsodic Flourish|Frioriture rhapsodique|libre|
|[l7w2peurzgbblvcv.htm](book-of-the-dead-bestiary-items/l7w2peurzgbblvcv.htm)|Bottle Bound|Lié à une bouteille|libre|
|[L95LvvfAZVKzDqz8.htm](book-of-the-dead-bestiary-items/L95LvvfAZVKzDqz8.htm)|Occult Innate Spells|Sorts occultes innés|libre|
|[ldn7og162ohnumw1.htm](book-of-the-dead-bestiary-items/ldn7og162ohnumw1.htm)|Lignifying Hand|Main lignifiante|libre|
|[LiEmzWVKW20OJoEm.htm](book-of-the-dead-bestiary-items/LiEmzWVKW20OJoEm.htm)|Darkvision|Vision dans le noir|libre|
|[lig2yfl497n7dn8f.htm](book-of-the-dead-bestiary-items/lig2yfl497n7dn8f.htm)|Theater Lore|Connaissance du théâtre|libre|
|[LigXwWUfSvwcEKm4.htm](book-of-the-dead-bestiary-items/LigXwWUfSvwcEKm4.htm)|Thoughtsense (Precise) 100 feet|Perception des pensées (précis) 30 m|libre|
|[lj7qJagCUYgoGe76.htm](book-of-the-dead-bestiary-items/lj7qJagCUYgoGe76.htm)|Divine Prepared Spells|Sorts divins préparés|libre|
|[lLCa9xf3PhXLdbdq.htm](book-of-the-dead-bestiary-items/lLCa9xf3PhXLdbdq.htm)|Darkvision|Vision dans le noir|libre|
|[llsd8tyjckikjmsy.htm](book-of-the-dead-bestiary-items/llsd8tyjckikjmsy.htm)|Fist|Poing|libre|
|[lo0krhkeahlcxl4k.htm](book-of-the-dead-bestiary-items/lo0krhkeahlcxl4k.htm)|Servitor Lunge|Plongeon du serviteur|libre|
|[loe7wunkt2aw95fw.htm](book-of-the-dead-bestiary-items/loe7wunkt2aw95fw.htm)|Blight|Rouille|libre|
|[lrHbm9UI1nZEYYkV.htm](book-of-the-dead-bestiary-items/lrHbm9UI1nZEYYkV.htm)|Enthrall (At Will)|Discours captivant (À volonté)|officielle|
|[ls7l1xs99s4qiknb.htm](book-of-the-dead-bestiary-items/ls7l1xs99s4qiknb.htm)|Pallid Touch|Toucher livide|libre|
|[lsjXlNKiGE8mRABo.htm](book-of-the-dead-bestiary-items/lsjXlNKiGE8mRABo.htm)|Darkvision|Vision dans le noir|libre|
|[m3t78jldn1s4cro7.htm](book-of-the-dead-bestiary-items/m3t78jldn1s4cro7.htm)|Brain Rot|Pourriture du cerveau|libre|
|[M5lSM9eRGm6vSvNd.htm](book-of-the-dead-bestiary-items/M5lSM9eRGm6vSvNd.htm)|Negative Healing|Guérison négative|libre|
|[M8YAKZOpPsPwvANR.htm](book-of-the-dead-bestiary-items/M8YAKZOpPsPwvANR.htm)|Negative Healing|Guérison négative|libre|
|[mAgSJlfDWhy4ZhCH.htm](book-of-the-dead-bestiary-items/mAgSJlfDWhy4ZhCH.htm)|Drain Thoughts|Drain de pensées|libre|
|[mc334q1nfka7sngl.htm](book-of-the-dead-bestiary-items/mc334q1nfka7sngl.htm)|Fire Fist|Poing de feu|libre|
|[mCgB8TMnu1WQOtQJ.htm](book-of-the-dead-bestiary-items/mCgB8TMnu1WQOtQJ.htm)|Darkvision|Vision dans le noir|libre|
|[md6wPL6GWR80z2Z6.htm](book-of-the-dead-bestiary-items/md6wPL6GWR80z2Z6.htm)|Darkvision|Vision dans le noir|libre|
|[ME1twBqe6m8JvEtv.htm](book-of-the-dead-bestiary-items/ME1twBqe6m8JvEtv.htm)|Darkvision|Vision dans le noir|libre|
|[MFbENdEsj2e5TGOd.htm](book-of-the-dead-bestiary-items/MFbENdEsj2e5TGOd.htm)|Darkvision|Vision dans le noir|libre|
|[MFUrmw36LIlgw5UX.htm](book-of-the-dead-bestiary-items/MFUrmw36LIlgw5UX.htm)|Lifesense 60 feet|Perception de la vie 18 m|libre|
|[mgzsxl5d5chh9f17.htm](book-of-the-dead-bestiary-items/mgzsxl5d5chh9f17.htm)|Brew Tomb Juice|Fabrication de jus de tombe|libre|
|[mHzpQhYPnyDQgnCL.htm](book-of-the-dead-bestiary-items/mHzpQhYPnyDQgnCL.htm)|Chains of the Dead|Chaîne des morts|libre|
|[MIBK76pXVm7hAU66.htm](book-of-the-dead-bestiary-items/MIBK76pXVm7hAU66.htm)|Thoughtsense (Precise) 100 feet|Perception des pensées (précis) 30 m|libre|
|[mij1k2qxzdjs4q2z.htm](book-of-the-dead-bestiary-items/mij1k2qxzdjs4q2z.htm)|Athletics|Athlétisme|libre|
|[mjju0x3kql87c8d3.htm](book-of-the-dead-bestiary-items/mjju0x3kql87c8d3.htm)|Sacrilegious Aura|Aura sacrilège|libre|
|[mjX6AVWz9h31QQlj.htm](book-of-the-dead-bestiary-items/mjX6AVWz9h31QQlj.htm)|Darkvision|Vision dans le noir|libre|
|[mkg6oo3hwvllmvw3.htm](book-of-the-dead-bestiary-items/mkg6oo3hwvllmvw3.htm)|Victory Celebration|Célébration de la victoire|libre|
|[mkicpd4bhua50de0.htm](book-of-the-dead-bestiary-items/mkicpd4bhua50de0.htm)|Claw|Griffe|libre|
|[mlnuhck97ts6m986.htm](book-of-the-dead-bestiary-items/mlnuhck97ts6m986.htm)|Tail|Queue|libre|
|[mlytlp3vwdiv4xev.htm](book-of-the-dead-bestiary-items/mlytlp3vwdiv4xev.htm)|Rise Again|Se relève|libre|
|[mmLtzGT7Xi5ll4Cd.htm](book-of-the-dead-bestiary-items/mmLtzGT7Xi5ll4Cd.htm)|Attack of Opportunity|Attaque d'opportunité|libre|
|[mn56u513jsfshjfz.htm](book-of-the-dead-bestiary-items/mn56u513jsfshjfz.htm)|Machete|Machette|libre|
|[mo4zs1e4rvsueimw.htm](book-of-the-dead-bestiary-items/mo4zs1e4rvsueimw.htm)|Jaws|Mâchoires|libre|
|[mp4tj1xv12frp13e.htm](book-of-the-dead-bestiary-items/mp4tj1xv12frp13e.htm)|Final Blasphemy|Blasphème final|libre|
|[MRiU0bmGLDherRXE.htm](book-of-the-dead-bestiary-items/MRiU0bmGLDherRXE.htm)|Grab|Empoigner/Agripper|libre|
|[msrjzpke2pxhz0yk.htm](book-of-the-dead-bestiary-items/msrjzpke2pxhz0yk.htm)|Sand Vision|Vision malgré le sable|libre|
|[msy9ijw3hmhu6ppi.htm](book-of-the-dead-bestiary-items/msy9ijw3hmhu6ppi.htm)|Staff|Bâton|libre|
|[MuhMVZGSGDdlWbDo.htm](book-of-the-dead-bestiary-items/MuhMVZGSGDdlWbDo.htm)|Shove|Poussée|libre|
|[mvCicXdmNRrAJwTJ.htm](book-of-the-dead-bestiary-items/mvCicXdmNRrAJwTJ.htm)|Rigor Mortis|Rigor mortis|libre|
|[mW6gZ5jbhxJRUo7Y.htm](book-of-the-dead-bestiary-items/mW6gZ5jbhxJRUo7Y.htm)|Negative Healing|Guérison négative|libre|
|[mx6sscnvhtaiutid.htm](book-of-the-dead-bestiary-items/mx6sscnvhtaiutid.htm)|Meant to Live|Supposé vivre|libre|
|[MYPGBIYRPHho61zG.htm](book-of-the-dead-bestiary-items/MYPGBIYRPHho61zG.htm)|Ghoul Fever|Fièvre des goules|libre|
|[n1l2a2abx1ek0d09.htm](book-of-the-dead-bestiary-items/n1l2a2abx1ek0d09.htm)|Kazutal Lore|Connaissance de Kazutal|libre|
|[n2cq3t3kbwnbsjkm.htm](book-of-the-dead-bestiary-items/n2cq3t3kbwnbsjkm.htm)|Sunlight Powerlessness|Impuissance solaire|libre|
|[n3kan97tvbssofit.htm](book-of-the-dead-bestiary-items/n3kan97tvbssofit.htm)|Servitor Attack|Attaque du serviteur|libre|
|[n468FPlhvooypdYU.htm](book-of-the-dead-bestiary-items/n468FPlhvooypdYU.htm)|Darkvision|Vision dans le noir|libre|
|[n53sw596l0uo2mmy.htm](book-of-the-dead-bestiary-items/n53sw596l0uo2mmy.htm)|Sunlight Powerlessness|Impuissance solaire|libre|
|[n5oh9urk0vokvmfv.htm](book-of-the-dead-bestiary-items/n5oh9urk0vokvmfv.htm)|Claw|Griffe|libre|
|[N5PKTuC1nPKbFmU9.htm](book-of-the-dead-bestiary-items/N5PKTuC1nPKbFmU9.htm)|Primal Prepared Spells|Sorts primordiaux préparés|libre|
|[n7PNu3mHe1gYDzMO.htm](book-of-the-dead-bestiary-items/n7PNu3mHe1gYDzMO.htm)|Fast Healing 10|Guérison rapide 10|libre|
|[n8a2azRJSqQCGbOL.htm](book-of-the-dead-bestiary-items/n8a2azRJSqQCGbOL.htm)|Negative Healing|Guérison négative|libre|
|[Nca0UI8dL6FmQNm5.htm](book-of-the-dead-bestiary-items/Nca0UI8dL6FmQNm5.htm)|Darkvision|Vision dans le noir|libre|
|[ncmh5gpidrqgeqj5.htm](book-of-the-dead-bestiary-items/ncmh5gpidrqgeqj5.htm)|Negative Ray|Rayon d'énergie négative|libre|
|[NcuughOSy9EI249D.htm](book-of-the-dead-bestiary-items/NcuughOSy9EI249D.htm)|Darkvision|Vision dans le noir|libre|
|[ndmnLmpNGYMdIz2H.htm](book-of-the-dead-bestiary-items/ndmnLmpNGYMdIz2H.htm)|Jaws|Mâchoires|libre|
|[ngnwinz7rvkh1cvw.htm](book-of-the-dead-bestiary-items/ngnwinz7rvkh1cvw.htm)|Bound|Lié|libre|
|[NIIRwklz4cR5uYjX.htm](book-of-the-dead-bestiary-items/NIIRwklz4cR5uYjX.htm)|Negative Healing|Guérison négative|libre|
|[NjGL58OXlG8alhDr.htm](book-of-the-dead-bestiary-items/NjGL58OXlG8alhDr.htm)|Fast Healing 10|Guérison accélérée 10|libre|
|[NkZfjGZXJvvzsjuj.htm](book-of-the-dead-bestiary-items/NkZfjGZXJvvzsjuj.htm)|Primal Focus Spells|Sorts primordiaux focalisés|libre|
|[nltFl2TPZ3VCCV0z.htm](book-of-the-dead-bestiary-items/nltFl2TPZ3VCCV0z.htm)|Darkvision|Vision dans le noir|libre|
|[NrspwQc7xJGzHSfc.htm](book-of-the-dead-bestiary-items/NrspwQc7xJGzHSfc.htm)|Phantom Mount|Monture fantôme|libre|
|[nua4eplxdmlfifpv.htm](book-of-the-dead-bestiary-items/nua4eplxdmlfifpv.htm)|Dance with Death|Danse avec la mort|libre|
|[nvu6RAdwRrg2nEJ4.htm](book-of-the-dead-bestiary-items/nvu6RAdwRrg2nEJ4.htm)|+1 Resilient Full Plate|Harnois de résilience +1|officielle|
|[nwwxqa7o7t95veoj.htm](book-of-the-dead-bestiary-items/nwwxqa7o7t95veoj.htm)|Living Visage|Visage vivant|libre|
|[nXUOVxkr2qzcOOP6.htm](book-of-the-dead-bestiary-items/nXUOVxkr2qzcOOP6.htm)|Negative Healing|Guérison négative|libre|
|[nYA9w4d89kX8zkyS.htm](book-of-the-dead-bestiary-items/nYA9w4d89kX8zkyS.htm)|Swallow Whole|Gober|libre|
|[nYgcNR6KtQIwzTTP.htm](book-of-the-dead-bestiary-items/nYgcNR6KtQIwzTTP.htm)|Shield Block|Blocage au bouclier|libre|
|[O1MGe9IusxQ8m5IK.htm](book-of-the-dead-bestiary-items/O1MGe9IusxQ8m5IK.htm)|Scent (Imprecise) 60 feet|Odorat 18 m (imprécis)|libre|
|[o31uzfafqxg6retc.htm](book-of-the-dead-bestiary-items/o31uzfafqxg6retc.htm)|Jaws|Mâchoires|libre|
|[o50ct5atowqid2nk.htm](book-of-the-dead-bestiary-items/o50ct5atowqid2nk.htm)|Claw|Griffe|libre|
|[o5vtcl40ekp9qv6n.htm](book-of-the-dead-bestiary-items/o5vtcl40ekp9qv6n.htm)|Starvation Aura|Aura de famine|libre|
|[o6I8Uh0qPM5ozyMW.htm](book-of-the-dead-bestiary-items/o6I8Uh0qPM5ozyMW.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[o7u2o263mumslgsb.htm](book-of-the-dead-bestiary-items/o7u2o263mumslgsb.htm)|War Flail|Fléau de guerre|libre|
|[o82ozylu3ctjujip.htm](book-of-the-dead-bestiary-items/o82ozylu3ctjujip.htm)|Jaws|Mâchoires|libre|
|[o8cbkspx01eskehx.htm](book-of-the-dead-bestiary-items/o8cbkspx01eskehx.htm)|Terrifying Laugh|Rire terrifiant|libre|
|[objWEl4wPiQkNnho.htm](book-of-the-dead-bestiary-items/objWEl4wPiQkNnho.htm)|Negative Healing|Guérison négative|libre|
|[oc46s540bthxaecp.htm](book-of-the-dead-bestiary-items/oc46s540bthxaecp.htm)|Combat Current|Fluide du combat|libre|
|[oer9h35wciqfjj68.htm](book-of-the-dead-bestiary-items/oer9h35wciqfjj68.htm)|Claw|Griffe|libre|
|[OFQw1wCzWACEpFPh.htm](book-of-the-dead-bestiary-items/OFQw1wCzWACEpFPh.htm)|Negative Healing|Guérison négative|libre|
|[Oh9OzqtV1OnzNNN3.htm](book-of-the-dead-bestiary-items/Oh9OzqtV1OnzNNN3.htm)|Haste (Self Only)|Rapidité (soi seulement)|libre|
|[olrz72ebabiexxj8.htm](book-of-the-dead-bestiary-items/olrz72ebabiexxj8.htm)|Synaptic Overload|Surcharge synaptique|libre|
|[om967vglnfob32of.htm](book-of-the-dead-bestiary-items/om967vglnfob32of.htm)|Frightful Battle Cry|Cri de guerre effroyable|libre|
|[ombjfkyxjmwbb9xc.htm](book-of-the-dead-bestiary-items/ombjfkyxjmwbb9xc.htm)|Drain Life|Drain de vie|libre|
|[onemB0KzXTHPiaTQ.htm](book-of-the-dead-bestiary-items/onemB0KzXTHPiaTQ.htm)|Darkvision|Vision dans le noir|libre|
|[onsl5qalhafgwyhe.htm](book-of-the-dead-bestiary-items/onsl5qalhafgwyhe.htm)|Compression|Compression|libre|
|[oramesvya495nwdm.htm](book-of-the-dead-bestiary-items/oramesvya495nwdm.htm)|Shuriken|Shuriken|libre|
|[OrGqVpvK9Ri1osng.htm](book-of-the-dead-bestiary-items/OrGqVpvK9Ri1osng.htm)|+1 Status to All Saves vs. Positive|bonus de statut de +1 aux JdS contre contre les effets positifs|libre|
|[ovW00fb5RQ0fss2D.htm](book-of-the-dead-bestiary-items/ovW00fb5RQ0fss2D.htm)|Darkvision|Vision dans le noir|libre|
|[oz8ouz3zgukkg8ga.htm](book-of-the-dead-bestiary-items/oz8ouz3zgukkg8ga.htm)|Driving Lore|Connaissance de la conduite|libre|
|[p1j4vk8x9pqsovd6.htm](book-of-the-dead-bestiary-items/p1j4vk8x9pqsovd6.htm)|Sense Companion|Perception du compagnon|libre|
|[p2homhgdk4xiy7ve.htm](book-of-the-dead-bestiary-items/p2homhgdk4xiy7ve.htm)|Breath Weapon|Arme de souffle|libre|
|[p2rb04uhqbleqtpy.htm](book-of-the-dead-bestiary-items/p2rb04uhqbleqtpy.htm)|Claw|Griffe|libre|
|[p3fyg7e66y8gq61g.htm](book-of-the-dead-bestiary-items/p3fyg7e66y8gq61g.htm)|Claw|Griffe|libre|
|[p44HhukqlJ7IVvti.htm](book-of-the-dead-bestiary-items/p44HhukqlJ7IVvti.htm)|Negative Healing|Guérison négative|libre|
|[P5HJ6L1BVQCeHoJr.htm](book-of-the-dead-bestiary-items/P5HJ6L1BVQCeHoJr.htm)|+18 to Initiative|+18 à l'Initiative|libre|
|[p5ohcfvydik0i2mz.htm](book-of-the-dead-bestiary-items/p5ohcfvydik0i2mz.htm)|Hungry Armor|Armure affamée|libre|
|[p7BEvZc5WuJeZIJf.htm](book-of-the-dead-bestiary-items/p7BEvZc5WuJeZIJf.htm)|Constrict|Constriction|libre|
|[P7FUCg0CYhhrSYXp.htm](book-of-the-dead-bestiary-items/P7FUCg0CYhhrSYXp.htm)|Negative Healing|Guérison négative|libre|
|[p8d8xn0icyozjet4.htm](book-of-the-dead-bestiary-items/p8d8xn0icyozjet4.htm)|Negative Energy Plane Lore|Connaissance du Plan de l'énergie négative|libre|
|[P9uhzDYGuPtmPcTt.htm](book-of-the-dead-bestiary-items/P9uhzDYGuPtmPcTt.htm)|Darkvision 60 feet|Vision dans le noir 18 m|libre|
|[pbwnkebF8Azr64pn.htm](book-of-the-dead-bestiary-items/pbwnkebF8Azr64pn.htm)|Grab|Empoigner/Agripper|libre|
|[pcv2wjgq3skfdwr1.htm](book-of-the-dead-bestiary-items/pcv2wjgq3skfdwr1.htm)|Stench|Puanteur|libre|
|[Pd2a0LCF9zAkHizr.htm](book-of-the-dead-bestiary-items/Pd2a0LCF9zAkHizr.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[pdt8bn3rsl7rxryb.htm](book-of-the-dead-bestiary-items/pdt8bn3rsl7rxryb.htm)|Summon Weapon|Convocation d'arme|libre|
|[pdtNmk8Zc7lN4BFF.htm](book-of-the-dead-bestiary-items/pdtNmk8Zc7lN4BFF.htm)|Grab|Empoignade/Agrippement|libre|
|[piqtoxtcbhe4b1a1.htm](book-of-the-dead-bestiary-items/piqtoxtcbhe4b1a1.htm)|Great Despair|Grand désespoir|libre|
|[pJdmn8co20iXokx1.htm](book-of-the-dead-bestiary-items/pJdmn8co20iXokx1.htm)|Negative Healing|Guérison négative|libre|
|[pKH0djjdIbsZYSyj.htm](book-of-the-dead-bestiary-items/pKH0djjdIbsZYSyj.htm)|Darkvision|Vision dans le noir|libre|
|[plczq6dno7lmsveb.htm](book-of-the-dead-bestiary-items/plczq6dno7lmsveb.htm)|Adopt Guise|Adopter une attidude|libre|
|[pm509slp2godzutv.htm](book-of-the-dead-bestiary-items/pm509slp2godzutv.htm)|Roll the Bones|Lance les osselets|libre|
|[pQh6aSqjZQoy6EAa.htm](book-of-the-dead-bestiary-items/pQh6aSqjZQoy6EAa.htm)|The Upper Hand|Avoir le dessus|libre|
|[PQnmOAwTpUMykmR6.htm](book-of-the-dead-bestiary-items/PQnmOAwTpUMykmR6.htm)|Darkvision|Vision dans le noir|libre|
|[pst72akzo45b7a0z.htm](book-of-the-dead-bestiary-items/pst72akzo45b7a0z.htm)|Parry Dance|Danse de parade|libre|
|[PUNivyqo7fsbOn4U.htm](book-of-the-dead-bestiary-items/PUNivyqo7fsbOn4U.htm)|Blink (Constant)|Clignotement (constant)|libre|
|[pvl5y6qz89i11vc5.htm](book-of-the-dead-bestiary-items/pvl5y6qz89i11vc5.htm)|Sense Murderer|Perception du meurtrier|libre|
|[pxfvn6ngmzu1k7ed.htm](book-of-the-dead-bestiary-items/pxfvn6ngmzu1k7ed.htm)|Crafting|Artisanat|libre|
|[pYCh0bsqXGiKu08p.htm](book-of-the-dead-bestiary-items/pYCh0bsqXGiKu08p.htm)|Darkvision|Vision dans le noir|libre|
|[q6iakq314is9gj9c.htm](book-of-the-dead-bestiary-items/q6iakq314is9gj9c.htm)|Ecstatic Ululation|Hurlement extatique|libre|
|[qbupjn2tlljiraow.htm](book-of-the-dead-bestiary-items/qbupjn2tlljiraow.htm)|Stone Antler|Corne de pierre|libre|
|[qd0w6wglev50gug2.htm](book-of-the-dead-bestiary-items/qd0w6wglev50gug2.htm)|Shadow Plane Lore|Connaissance du plan de l'ombre|officielle|
|[qDKNtEMBCrIlKW6A.htm](book-of-the-dead-bestiary-items/qDKNtEMBCrIlKW6A.htm)|Blight Mastery|Maître du fléau|libre|
|[qmkVNjGJoAddHkl7.htm](book-of-the-dead-bestiary-items/qmkVNjGJoAddHkl7.htm)|Greater Darkvision|Vision dans le noir supérieur|libre|
|[qmXjR6UjkPsa5Yrc.htm](book-of-the-dead-bestiary-items/qmXjR6UjkPsa5Yrc.htm)|Improved Grab|Empoignade/Agrippement amélioré|libre|
|[QNACTpWxBNKzhVKH.htm](book-of-the-dead-bestiary-items/QNACTpWxBNKzhVKH.htm)|Bag of Holding (Type 1)|Sac sans fond (Type I)|libre|
|[qo8ae1c96drn6owz.htm](book-of-the-dead-bestiary-items/qo8ae1c96drn6owz.htm)|Claw|Griffe|libre|
|[QOcBBYQLYIHAXHeX.htm](book-of-the-dead-bestiary-items/QOcBBYQLYIHAXHeX.htm)|Scroll of Teleport (Level 6)|Parchemin de Téléportation (Niveau 6)|libre|
|[qqbbtn1cc3yyrpdv.htm](book-of-the-dead-bestiary-items/qqbbtn1cc3yyrpdv.htm)|Slow|Lent|libre|
|[QQXKnrwMluEorlEb.htm](book-of-the-dead-bestiary-items/QQXKnrwMluEorlEb.htm)|Darkvision|Vision dans le noir|libre|
|[qrob277923ccvj5f.htm](book-of-the-dead-bestiary-items/qrob277923ccvj5f.htm)|Flicker|Vaciller|libre|
|[QRR9beKx0noS0RfN.htm](book-of-the-dead-bestiary-items/QRR9beKx0noS0RfN.htm)|Lifesense 60 feet|Perception de la vie 30 m|libre|
|[QTvQkFjvczOYx7t8.htm](book-of-the-dead-bestiary-items/QTvQkFjvczOYx7t8.htm)|One More Breath|Un souffle de plus|libre|
|[qU8ergQ43LOW1kIH.htm](book-of-the-dead-bestiary-items/qU8ergQ43LOW1kIH.htm)|Plane Shift (to Material Plane, Negative Energy Plane, or Shadow Plane only)|Changement de plan (vers le plan Matériel, de l' Énergie négative et de l'Ombre uniquement)|libre|
|[qvgyve9ubbhtjhlb.htm](book-of-the-dead-bestiary-items/qvgyve9ubbhtjhlb.htm)|Consecration Vulnerability|Vulnérabilité à la consécration|libre|
|[qVmgf9hKv2Wt0x9f.htm](book-of-the-dead-bestiary-items/qVmgf9hKv2Wt0x9f.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[qWb2hnLxcNgrwqy7.htm](book-of-the-dead-bestiary-items/qWb2hnLxcNgrwqy7.htm)|Darkvision|Vision dans le noir|libre|
|[qxv9k072qjlqqrmv.htm](book-of-the-dead-bestiary-items/qxv9k072qjlqqrmv.htm)|Bite Back|Retour de morsure|libre|
|[r1hdbf8k93avvj7u.htm](book-of-the-dead-bestiary-items/r1hdbf8k93avvj7u.htm)|Forest Guardian|Gardien de la forêt|libre|
|[r2m3ybfe440pmnlu.htm](book-of-the-dead-bestiary-items/r2m3ybfe440pmnlu.htm)|Terrain Advantage|Avantage du terrain|libre|
|[r5xddqaz12nw9vmw.htm](book-of-the-dead-bestiary-items/r5xddqaz12nw9vmw.htm)|Muscular Leap|Saut musclé|libre|
|[R7D68innrOxLHJAm.htm](book-of-the-dead-bestiary-items/R7D68innrOxLHJAm.htm)|Musical Assault|Assaut musical|libre|
|[r867EAQMTLMUJiHC.htm](book-of-the-dead-bestiary-items/r867EAQMTLMUJiHC.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[r9tJVChsQaE6jEtS.htm](book-of-the-dead-bestiary-items/r9tJVChsQaE6jEtS.htm)|Darkvision|Vision dans le noir|libre|
|[RaJiotddHOyL70ol.htm](book-of-the-dead-bestiary-items/RaJiotddHOyL70ol.htm)|Negative Healing|Guérison négative|libre|
|[rat6z2dx8o2ocr0x.htm](book-of-the-dead-bestiary-items/rat6z2dx8o2ocr0x.htm)|Aura of Peace|Aura de paix|libre|
|[rfeIYZBHpVkv6qJZ.htm](book-of-the-dead-bestiary-items/rfeIYZBHpVkv6qJZ.htm)|Negative Healing|Guérison négative|libre|
|[rfqiqYvSTPZWOWVq.htm](book-of-the-dead-bestiary-items/rfqiqYvSTPZWOWVq.htm)|Negative Healing|Guérison négative|libre|
|[rfs2498hafjirg6i.htm](book-of-the-dead-bestiary-items/rfs2498hafjirg6i.htm)|Claw|Griffe|libre|
|[rh6570cau7s1l659.htm](book-of-the-dead-bestiary-items/rh6570cau7s1l659.htm)|Arm Spike|Bras pointu|libre|
|[ri32o76yg6cyuq05.htm](book-of-the-dead-bestiary-items/ri32o76yg6cyuq05.htm)|Glaive|Glaive|libre|
|[rIOKUxnAXsQC39t6.htm](book-of-the-dead-bestiary-items/rIOKUxnAXsQC39t6.htm)|Darkvision|Vision dans le noir|libre|
|[Rk6vaUzdBA05Ipr4.htm](book-of-the-dead-bestiary-items/Rk6vaUzdBA05Ipr4.htm)|Negative Healing|Guérison négative|libre|
|[RKJRHH3TlsIfcjRw.htm](book-of-the-dead-bestiary-items/RKJRHH3TlsIfcjRw.htm)|Negative Healing|Guérison négative|libre|
|[RM4jD8UptCfao2mw.htm](book-of-the-dead-bestiary-items/RM4jD8UptCfao2mw.htm)|Vampiric Exsanguination (Drawing in Moisture Rather than Blood)|Saignée du vampire (tirant de l'humidité plutôt que du sang)|libre|
|[rNWhoVEyE4tbJ7UF.htm](book-of-the-dead-bestiary-items/rNWhoVEyE4tbJ7UF.htm)|Darkvision|Vision dans le noir|libre|
|[ROMNXYjyp9b5wU7t.htm](book-of-the-dead-bestiary-items/ROMNXYjyp9b5wU7t.htm)|Darkvision|Vision dans le noir|libre|
|[rotlnf6iwle4td73.htm](book-of-the-dead-bestiary-items/rotlnf6iwle4td73.htm)|Acrobatics|Acrobaties|libre|
|[RpSDL9pRSQiu3lu9.htm](book-of-the-dead-bestiary-items/RpSDL9pRSQiu3lu9.htm)|Cleric Domain Spells|Sorts de domaine de prêtre|libre|
|[rpzk7dfngnq6h4fj.htm](book-of-the-dead-bestiary-items/rpzk7dfngnq6h4fj.htm)|Crossbow|Arc long|libre|
|[rsHuZ4AV9kbemHYN.htm](book-of-the-dead-bestiary-items/rsHuZ4AV9kbemHYN.htm)|Negative Healing|Guérison négative|libre|
|[RsT9PJggBJ9E7yXV.htm](book-of-the-dead-bestiary-items/RsT9PJggBJ9E7yXV.htm)|Shut In|Enfermer|libre|
|[rt3cft7ln3rvinzi.htm](book-of-the-dead-bestiary-items/rt3cft7ln3rvinzi.htm)|Teleporting Clothesline|Coup de la téléportation|libre|
|[RvTQTNsY192UMAWY.htm](book-of-the-dead-bestiary-items/RvTQTNsY192UMAWY.htm)|Drain Qi|Drain de Qi|libre|
|[Rw4TQAjoro6E9MvZ.htm](book-of-the-dead-bestiary-items/Rw4TQAjoro6E9MvZ.htm)|Composite Longbow|+1|Arc long composite +1|officielle|
|[RZ8nmKkTFCoNYpw1.htm](book-of-the-dead-bestiary-items/RZ8nmKkTFCoNYpw1.htm)|Negative Healing|Guérison négative|libre|
|[RZU7C9rxkfFtWjAK.htm](book-of-the-dead-bestiary-items/RZU7C9rxkfFtWjAK.htm)|Negative Healing|Guérison négative|libre|
|[S03qKeYKeO3g8bMB.htm](book-of-the-dead-bestiary-items/S03qKeYKeO3g8bMB.htm)|Darkvision|Vision dans le noir|libre|
|[s0ilcnpw3egzb7jx.htm](book-of-the-dead-bestiary-items/s0ilcnpw3egzb7jx.htm)|Jaws|Mâchoires|libre|
|[s4bb8j40iolivf58.htm](book-of-the-dead-bestiary-items/s4bb8j40iolivf58.htm)|Martial Arts Lore|Connaissance des arts martiaux|libre|
|[S5HKepuRcLZyCgzt.htm](book-of-the-dead-bestiary-items/S5HKepuRcLZyCgzt.htm)|Paralysis|Paralysie|libre|
|[s6celq1ixxucp8ro.htm](book-of-the-dead-bestiary-items/s6celq1ixxucp8ro.htm)|Death Gasp|Soupir de mort|libre|
|[SAnGJYM6pCkTSTFE.htm](book-of-the-dead-bestiary-items/SAnGJYM6pCkTSTFE.htm)|Chomp|Morsure profonde|libre|
|[sb2b4fq0wls5slr7.htm](book-of-the-dead-bestiary-items/sb2b4fq0wls5slr7.htm)|Negative Energy Plane Lore|Connaissance du Plan de l'énergie négative|libre|
|[sbuu4mk2ssfmalco.htm](book-of-the-dead-bestiary-items/sbuu4mk2ssfmalco.htm)|Claw|Griffe|libre|
|[sbw8eejalnwiv9pt.htm](book-of-the-dead-bestiary-items/sbw8eejalnwiv9pt.htm)|Soulscent (Imprecise) 100 feet|Perception des âmes 30 mètres (imprécis)|libre|
|[sE93R44GXNoE5W3Y.htm](book-of-the-dead-bestiary-items/sE93R44GXNoE5W3Y.htm)|Drain Qi|Drain de Qi|libre|
|[sfOhU9Ljw1nJ9Vw8.htm](book-of-the-dead-bestiary-items/sfOhU9Ljw1nJ9Vw8.htm)|Negative Healing|Guérison négative|libre|
|[SfwWBcVieSEQcUwE.htm](book-of-the-dead-bestiary-items/SfwWBcVieSEQcUwE.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[sg28ok3v73dn8dvi.htm](book-of-the-dead-bestiary-items/sg28ok3v73dn8dvi.htm)|Snatch|Saisir|libre|
|[sgrD5tLKLJMdGbYH.htm](book-of-the-dead-bestiary-items/sgrD5tLKLJMdGbYH.htm)|Improved Grab|Empoignade/Agrippement amélioré|libre|
|[ShZ2CAIzf05F41B2.htm](book-of-the-dead-bestiary-items/ShZ2CAIzf05F41B2.htm)|Stop Heart|Arrêt cardiaque|libre|
|[sjrL9SViifyZvj1G.htm](book-of-the-dead-bestiary-items/sjrL9SViifyZvj1G.htm)|Negative Healing|Guérison négative|libre|
|[SNMKCPwnj2TgJsVv.htm](book-of-the-dead-bestiary-items/SNMKCPwnj2TgJsVv.htm)|Plane Shift (to Material Plane, Negative Energy Plane, or Shadow Plane only)|Changement de plan (vers le plan Matériel, de l' Énergie négative et de l'Ombre uniquement)|libre|
|[so0db4mc0q95h2lm.htm](book-of-the-dead-bestiary-items/so0db4mc0q95h2lm.htm)|Enshroud|Envelopper|libre|
|[ssocwemmugpprhuc.htm](book-of-the-dead-bestiary-items/ssocwemmugpprhuc.htm)|Sunlight Powerlessness|Impuissance au soleil|libre|
|[suMVXLkMmzCGv2fJ.htm](book-of-the-dead-bestiary-items/suMVXLkMmzCGv2fJ.htm)|Fist|Poing|libre|
|[supp5wizpe79eqi9.htm](book-of-the-dead-bestiary-items/supp5wizpe79eqi9.htm)|Bloody Spew|Crachat de sang|libre|
|[sUsdrW2eEA7nzp6P.htm](book-of-the-dead-bestiary-items/sUsdrW2eEA7nzp6P.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[SwCl0ytYvoRbPKKM.htm](book-of-the-dead-bestiary-items/SwCl0ytYvoRbPKKM.htm)|Darkvision|Vision dans le noir|libre|
|[sWtjwSeVA2Yi9URn.htm](book-of-the-dead-bestiary-items/sWtjwSeVA2Yi9URn.htm)|Jaws|Mâchoires|libre|
|[SXOUzgOVcsJXmJtF.htm](book-of-the-dead-bestiary-items/SXOUzgOVcsJXmJtF.htm)|Gather Spirit|Rassembler les âmes|libre|
|[syR8yFxw3sNEGkUO.htm](book-of-the-dead-bestiary-items/syR8yFxw3sNEGkUO.htm)|Negative Healing|Guérison négative|libre|
|[SZNvFrnofD8dddxU.htm](book-of-the-dead-bestiary-items/SZNvFrnofD8dddxU.htm)|Divine Prepared Spells|Sorts divins préparés|libre|
|[t1csyjtz5anowgkm.htm](book-of-the-dead-bestiary-items/t1csyjtz5anowgkm.htm)|Athletics|Athlétisme|libre|
|[t1x1x6iwe8eqpsxc.htm](book-of-the-dead-bestiary-items/t1x1x6iwe8eqpsxc.htm)|Staff|Bâton|libre|
|[t2vkqgxwqps1lc2c.htm](book-of-the-dead-bestiary-items/t2vkqgxwqps1lc2c.htm)|Plant|Plante|libre|
|[t31wSSg27UyZRToz.htm](book-of-the-dead-bestiary-items/t31wSSg27UyZRToz.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[t57awjzu5thorizx.htm](book-of-the-dead-bestiary-items/t57awjzu5thorizx.htm)|Pistol Whip|Pistolet fouet|libre|
|[t6tofj5bxmm53nld.htm](book-of-the-dead-bestiary-items/t6tofj5bxmm53nld.htm)|Death Gasp|Soupir de mort|libre|
|[TA1sQhusAvQaJjnY.htm](book-of-the-dead-bestiary-items/TA1sQhusAvQaJjnY.htm)|Change Shape|Changement de forme|libre|
|[tabg0vo3elkeri2r.htm](book-of-the-dead-bestiary-items/tabg0vo3elkeri2r.htm)|Horrifying Screech|Hurlement horrifiant|libre|
|[TCn4mhscYuvLcXc1.htm](book-of-the-dead-bestiary-items/TCn4mhscYuvLcXc1.htm)|Darkvision|Vision dans le noir|libre|
|[TDyZOrJxLY1hcPKJ.htm](book-of-the-dead-bestiary-items/TDyZOrJxLY1hcPKJ.htm)|Drain Life|Drain de vie|libre|
|[thmdq28mehsfvjtu.htm](book-of-the-dead-bestiary-items/thmdq28mehsfvjtu.htm)|Severed Trunk|Trompe sectionnée|libre|
|[thmuyoj3asul9w8q.htm](book-of-the-dead-bestiary-items/thmuyoj3asul9w8q.htm)|Claw|Griffe|libre|
|[TkNnlQ5Y9tP89lcL.htm](book-of-the-dead-bestiary-items/TkNnlQ5Y9tP89lcL.htm)|Desperate Meal|Repas désespérant|libre|
|[tlp3kuw1xc6wf30n.htm](book-of-the-dead-bestiary-items/tlp3kuw1xc6wf30n.htm)|Acrobatics|Acrobaties|libre|
|[TN9YVnHaKRk3uwTv.htm](book-of-the-dead-bestiary-items/TN9YVnHaKRk3uwTv.htm)|Negative Healing|Guérison négative|libre|
|[tskaoz4x2uhz0ss3.htm](book-of-the-dead-bestiary-items/tskaoz4x2uhz0ss3.htm)|Bone Debris|Débris d'os|libre|
|[TtXUPuadvachP5x2.htm](book-of-the-dead-bestiary-items/TtXUPuadvachP5x2.htm)|Spiked Chain|+2,striking|Chaine cloutée de frappe +2|libre|
|[tult7kj40ygqn8tz.htm](book-of-the-dead-bestiary-items/tult7kj40ygqn8tz.htm)|Ghostly Grasp|Étreinte fantomatique|libre|
|[U0g3auuSZ4pwJU82.htm](book-of-the-dead-bestiary-items/U0g3auuSZ4pwJU82.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[U34PxMHZ0fS0k12A.htm](book-of-the-dead-bestiary-items/U34PxMHZ0fS0k12A.htm)|Form Up|Se reformer|libre|
|[u35onovz82ogkm7o.htm](book-of-the-dead-bestiary-items/u35onovz82ogkm7o.htm)|Take Root|Prendre racine|libre|
|[u3b2rv8mv9k9rb23.htm](book-of-the-dead-bestiary-items/u3b2rv8mv9k9rb23.htm)|Great Despair|Grand désespoir|libre|
|[u5fvma01emd3ped8.htm](book-of-the-dead-bestiary-items/u5fvma01emd3ped8.htm)|Jaws|Mâchoires|libre|
|[u5q8d8r0yfjwcdoj.htm](book-of-the-dead-bestiary-items/u5q8d8r0yfjwcdoj.htm)|Ship Bound|Lié au navire|libre|
|[u7gqkauw2hdfde3u.htm](book-of-the-dead-bestiary-items/u7gqkauw2hdfde3u.htm)|Involuntary Reaction|Réaction involontaire|libre|
|[uAiU3U0R80wN24HZ.htm](book-of-the-dead-bestiary-items/uAiU3U0R80wN24HZ.htm)|Visions of Danger (A Swarm of Sluagh Reapers)|Dangereuses vision (une nuée de faucheurs sluagh)|libre|
|[uB6EJzJpTE3PRvS8.htm](book-of-the-dead-bestiary-items/uB6EJzJpTE3PRvS8.htm)|Negative Healing|Guérison négative|libre|
|[ubqhrgnv2m0l1100.htm](book-of-the-dead-bestiary-items/ubqhrgnv2m0l1100.htm)|Claw|Griffe|libre|
|[ubsx377x3tktofot.htm](book-of-the-dead-bestiary-items/ubsx377x3tktofot.htm)|Claw|Griffe|libre|
|[UBzBGEYk2sLnPlbQ.htm](book-of-the-dead-bestiary-items/UBzBGEYk2sLnPlbQ.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|libre|
|[Uc4DijFU9ApvpWFZ.htm](book-of-the-dead-bestiary-items/Uc4DijFU9ApvpWFZ.htm)|Consume Flesh|Dévorer la chair|libre|
|[ucz1Sz93jpBvRWbp.htm](book-of-the-dead-bestiary-items/ucz1Sz93jpBvRWbp.htm)|Darkvision|Vision dans le noir|libre|
|[Ud7fmRAenQnd8Zxa.htm](book-of-the-dead-bestiary-items/Ud7fmRAenQnd8Zxa.htm)|Negative Healing|Guérison négative|libre|
|[udo3xq7mjc590h1k.htm](book-of-the-dead-bestiary-items/udo3xq7mjc590h1k.htm)|Fire Mote|Particule de feu|libre|
|[ugPuUFMgsKh55RQn.htm](book-of-the-dead-bestiary-items/ugPuUFMgsKh55RQn.htm)|Divine Focus Spells|Sorts divins focalisés|libre|
|[uh3gw9uok4g0fuzy.htm](book-of-the-dead-bestiary-items/uh3gw9uok4g0fuzy.htm)|Reap Faith|Récolte de foi|libre|
|[UHjOHKWLHa1xT8ID.htm](book-of-the-dead-bestiary-items/UHjOHKWLHa1xT8ID.htm)|Slow|Lent|libre|
|[uhTWUFxW0j3Yev1T.htm](book-of-the-dead-bestiary-items/uhTWUFxW0j3Yev1T.htm)|Negative Healing|Guérison négative|libre|
|[UK03LEy2NrVO5Ir3.htm](book-of-the-dead-bestiary-items/UK03LEy2NrVO5Ir3.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[ukiq9bqccfjpoc72.htm](book-of-the-dead-bestiary-items/ukiq9bqccfjpoc72.htm)|Submission Lock|Clé de soumission|libre|
|[un97s9dr8tua2f9t.htm](book-of-the-dead-bestiary-items/un97s9dr8tua2f9t.htm)|Fangs|Crocs|libre|
|[uNwNfEbvOaEcyhSd.htm](book-of-the-dead-bestiary-items/uNwNfEbvOaEcyhSd.htm)|Machete|+1,striking|Machette de frappe +1|libre|
|[uovpjaklzm8wgqbt.htm](book-of-the-dead-bestiary-items/uovpjaklzm8wgqbt.htm)|Spectral Charge|Charge spectrale|libre|
|[uphuqzm8b7b57k4d.htm](book-of-the-dead-bestiary-items/uphuqzm8b7b57k4d.htm)|Final Blasphemy|Blasphème final|libre|
|[urw7slqqg18c2bqr.htm](book-of-the-dead-bestiary-items/urw7slqqg18c2bqr.htm)|Change Posture|Changer de posture|libre|
|[usz0U3YIXa4lehfb.htm](book-of-the-dead-bestiary-items/usz0U3YIXa4lehfb.htm)|Wand of Wall of Force (Level 6)|Baguette de Mur de force (Niveau 6)|libre|
|[UTArnl71MY7BVLQc.htm](book-of-the-dead-bestiary-items/UTArnl71MY7BVLQc.htm)|Negative Healing|Guérison négative|libre|
|[UuMVTRKxoumdKVH4.htm](book-of-the-dead-bestiary-items/UuMVTRKxoumdKVH4.htm)|At-Will Spells|Sorts à volonté|libre|
|[uUZ8FSW9h8LqYa6l.htm](book-of-the-dead-bestiary-items/uUZ8FSW9h8LqYa6l.htm)|Flintlock Pistol|+1,striking|Pistolet à silex de frappe +1|libre|
|[UVtZ0kwBh5CQGxkT.htm](book-of-the-dead-bestiary-items/UVtZ0kwBh5CQGxkT.htm)|Stalk|Suivre|libre|
|[UwqJzuhMBvtDiE1L.htm](book-of-the-dead-bestiary-items/UwqJzuhMBvtDiE1L.htm)|Darkvision|Vision dans le noir|libre|
|[uxhfyJ9MLgmRSKS9.htm](book-of-the-dead-bestiary-items/uxhfyJ9MLgmRSKS9.htm)|Darkness (At Will)|Ténèbres (À volonté)|officielle|
|[uyk9qJPvkuv0JU5M.htm](book-of-the-dead-bestiary-items/uyk9qJPvkuv0JU5M.htm)|Mental Bind|Chaîne mentale|libre|
|[uyym1abw28sm0iuq.htm](book-of-the-dead-bestiary-items/uyym1abw28sm0iuq.htm)|Crush Item|Broyer l'objet|libre|
|[v0q9b81a0anjrpfc.htm](book-of-the-dead-bestiary-items/v0q9b81a0anjrpfc.htm)|Chastise Heretic|Châtiment de l'hérétique|libre|
|[v1cjchmhqo6lvdjy.htm](book-of-the-dead-bestiary-items/v1cjchmhqo6lvdjy.htm)|Coordinated Strike|Frappe coordonnée|libre|
|[v1KNai0kJiubpLMO.htm](book-of-the-dead-bestiary-items/v1KNai0kJiubpLMO.htm)|Darkvision|Vision dans le noir|libre|
|[v3e78dwshaq5ch57.htm](book-of-the-dead-bestiary-items/v3e78dwshaq5ch57.htm)|Consult the Text|Consultation du texte|libre|
|[V5QNa5hnkOnSMJVv.htm](book-of-the-dead-bestiary-items/V5QNa5hnkOnSMJVv.htm)|Attack of Opportunity (Special)|Attaque d'opportunité (Spéciale)|libre|
|[v6OYQ9mA3KrJqI4Q.htm](book-of-the-dead-bestiary-items/v6OYQ9mA3KrJqI4Q.htm)|Negative Healing|Guérison négative|libre|
|[v7iYGiJ8yz2GeTHT.htm](book-of-the-dead-bestiary-items/v7iYGiJ8yz2GeTHT.htm)|Darkvision|Vision dans le noir|libre|
|[VaJj5noJcvvTyDKp.htm](book-of-the-dead-bestiary-items/VaJj5noJcvvTyDKp.htm)|Grab|Empoignade/Agrippement|libre|
|[vamz8Jb2yzFSh4rA.htm](book-of-the-dead-bestiary-items/vamz8Jb2yzFSh4rA.htm)|Dispel Magic (At Will)|Dissipation de la magie (À volonté)|officielle|
|[vb6es4dznn8764o4.htm](book-of-the-dead-bestiary-items/vb6es4dznn8764o4.htm)|Fist|Poing|libre|
|[vc4ulmssbysj8wam.htm](book-of-the-dead-bestiary-items/vc4ulmssbysj8wam.htm)|Catching Bite|Morsure capturante|libre|
|[vd5bzz1j83bmas2b.htm](book-of-the-dead-bestiary-items/vd5bzz1j83bmas2b.htm)|Spear|Lance|libre|
|[VdFWJZh6U3QHXfn7.htm](book-of-the-dead-bestiary-items/VdFWJZh6U3QHXfn7.htm)|Negative Healing|Guérison négative|libre|
|[vghi4lo4jsex6xvg.htm](book-of-the-dead-bestiary-items/vghi4lo4jsex6xvg.htm)|Spellstealing Counter|Vol de sort contré|libre|
|[vgpzwfs5atbe56al.htm](book-of-the-dead-bestiary-items/vgpzwfs5atbe56al.htm)|Cold Rot|Pourriture glacée|libre|
|[vj9p9alcrxuahtfn.htm](book-of-the-dead-bestiary-items/vj9p9alcrxuahtfn.htm)|Scythe Claw|Griffe-faux|libre|
|[VJtbJPj39575cPho.htm](book-of-the-dead-bestiary-items/VJtbJPj39575cPho.htm)|Change Shape|Changement de forme|libre|
|[vkuk04wj4sfihq3b.htm](book-of-the-dead-bestiary-items/vkuk04wj4sfihq3b.htm)|Corpse Scent (Imprecise) 1 mile|Odorat des cadavres (imprécis) 1,6 km|libre|
|[vlcr44dvupfosqoq.htm](book-of-the-dead-bestiary-items/vlcr44dvupfosqoq.htm)|Steady Spellcasting|Incantation fiable|libre|
|[vlnnscd18jfqcdq9.htm](book-of-the-dead-bestiary-items/vlnnscd18jfqcdq9.htm)|Scything Blade|Lame faucheuse|libre|
|[vM9qUCcmjXuCKUvw.htm](book-of-the-dead-bestiary-items/vM9qUCcmjXuCKUvw.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[VqcQnwyahELeDBwK.htm](book-of-the-dead-bestiary-items/VqcQnwyahELeDBwK.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[vqtr9yyxp561y34u.htm](book-of-the-dead-bestiary-items/vqtr9yyxp561y34u.htm)|Ghostly Hand|Main spectrale|libre|
|[vrbzxalz7mler6oi.htm](book-of-the-dead-bestiary-items/vrbzxalz7mler6oi.htm)|Snow Vision|Vision malgré la neige|libre|
|[VuchmKXLlqk2wO9c.htm](book-of-the-dead-bestiary-items/VuchmKXLlqk2wO9c.htm)|Grab|Empoignade/Agrippement|libre|
|[vW68sTmazYmk9G6Q.htm](book-of-the-dead-bestiary-items/vW68sTmazYmk9G6Q.htm)|Feral Leap|Bond féral|libre|
|[vwxfS6QSBbdY3B7C.htm](book-of-the-dead-bestiary-items/vwxfS6QSBbdY3B7C.htm)|Longsword|+1,striking|Épée longue de frappe +1|libre|
|[vX9cmoHPlrRAHeYs.htm](book-of-the-dead-bestiary-items/vX9cmoHPlrRAHeYs.htm)|Bullets of Vengeance|Balles de la vengeance|libre|
|[VZR6D452W8R3BDqd.htm](book-of-the-dead-bestiary-items/VZR6D452W8R3BDqd.htm)|Darkvision|Vision dans le noir|libre|
|[W0TicentuZL4V7qS.htm](book-of-the-dead-bestiary-items/W0TicentuZL4V7qS.htm)|Darkvision|Vision dans le noir|libre|
|[w4cvlv6bdzp5wcxx.htm](book-of-the-dead-bestiary-items/w4cvlv6bdzp5wcxx.htm)|Hand|Main|libre|
|[w8nbobeosq4s3a35.htm](book-of-the-dead-bestiary-items/w8nbobeosq4s3a35.htm)|Stance of Death|Posture de la mort|libre|
|[wauesfz491hsnkk4.htm](book-of-the-dead-bestiary-items/wauesfz491hsnkk4.htm)|Destructive Finale|Finale destructrice|libre|
|[wbmhtkddtggv5bgk.htm](book-of-the-dead-bestiary-items/wbmhtkddtggv5bgk.htm)|Fist|Poing|libre|
|[wbqipaniohqje45s.htm](book-of-the-dead-bestiary-items/wbqipaniohqje45s.htm)|Devour Flesh|Dévore la chair|libre|
|[WciUktftb9XKpzo5.htm](book-of-the-dead-bestiary-items/WciUktftb9XKpzo5.htm)|Religious Symbol of Urgathoa|Symbole religieux d'Urgathoa|libre|
|[WI1TgZGTFtOzaOax.htm](book-of-the-dead-bestiary-items/WI1TgZGTFtOzaOax.htm)|Water Walk (Constant)|Marche sur l'eau (constant)|officielle|
|[WI4DJQA8o5zmepx3.htm](book-of-the-dead-bestiary-items/WI4DJQA8o5zmepx3.htm)|Spear|+1,striking|Lance de frappe +1|officielle|
|[wjdkro5cizi5vre7.htm](book-of-the-dead-bestiary-items/wjdkro5cizi5vre7.htm)|Eyes of the Tyrant|Yeux du tyran|libre|
|[WK7lEwsMK9Kmei2J.htm](book-of-the-dead-bestiary-items/WK7lEwsMK9Kmei2J.htm)|Freedom of Movement (Constant)|Liberté de mouvement (constant)|officielle|
|[WkoSPIMoXbCHFRZj.htm](book-of-the-dead-bestiary-items/WkoSPIMoXbCHFRZj.htm)|Grab|Empoignade/Agrippement|libre|
|[wln6jtro52i3xd9c.htm](book-of-the-dead-bestiary-items/wln6jtro52i3xd9c.htm)|Exhume|Déterrer|libre|
|[WnQDVEnbKG81zOIf.htm](book-of-the-dead-bestiary-items/WnQDVEnbKG81zOIf.htm)|Occult Prepared Spells|Sorts occultes préparés|libre|
|[wo29s0y8jcz4isnc.htm](book-of-the-dead-bestiary-items/wo29s0y8jcz4isnc.htm)|Claw|Griffe|libre|
|[woLgXjw2p6sRigIz.htm](book-of-the-dead-bestiary-items/woLgXjw2p6sRigIz.htm)|Grab|Empoignade/Agrippement|libre|
|[woUqNS1copPPro5x.htm](book-of-the-dead-bestiary-items/woUqNS1copPPro5x.htm)|Negative Healing|Guérison négative|libre|
|[WPhT2IXNFPoANk1z.htm](book-of-the-dead-bestiary-items/WPhT2IXNFPoANk1z.htm)|Grab|Empoignade/Agrippement|libre|
|[WPV9eNOze72jDJV4.htm](book-of-the-dead-bestiary-items/WPV9eNOze72jDJV4.htm)|Frightful Presence|Présence terrifiante|libre|
|[wq8dsdznit8qtxcv.htm](book-of-the-dead-bestiary-items/wq8dsdznit8qtxcv.htm)|Fist|Poing|libre|
|[wRVqT1QO1O1ru1SY.htm](book-of-the-dead-bestiary-items/wRVqT1QO1O1ru1SY.htm)|Trample|Piétinement|libre|
|[wspi8nMzAlqEekgs.htm](book-of-the-dead-bestiary-items/wspi8nMzAlqEekgs.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[WthNDt9DjtNzlPes.htm](book-of-the-dead-bestiary-items/WthNDt9DjtNzlPes.htm)|Negative Healing|Guérison négative|libre|
|[Wunpn2wRMKV7JLZr.htm](book-of-the-dead-bestiary-items/Wunpn2wRMKV7JLZr.htm)|Darkvision|Vision dans le noir|libre|
|[WUYJjrYb5HbQgPNA.htm](book-of-the-dead-bestiary-items/WUYJjrYb5HbQgPNA.htm)|Negative Healing|Guérison négative|libre|
|[wvs0vnvwndnjqq6u.htm](book-of-the-dead-bestiary-items/wvs0vnvwndnjqq6u.htm)|Chain Capture|Chaîne de capture|libre|
|[ww6g31ronygtiit4.htm](book-of-the-dead-bestiary-items/ww6g31ronygtiit4.htm)|Primal Corruption|Corruption primordiale|libre|
|[X0l5lixcXAtLUYYZ.htm](book-of-the-dead-bestiary-items/X0l5lixcXAtLUYYZ.htm)|Chain Mail (Broken)|Cotte de mailles brisée|libre|
|[X1pEJ8DPmm00nq2l.htm](book-of-the-dead-bestiary-items/X1pEJ8DPmm00nq2l.htm)|Constant Spells|Sorts constants|libre|
|[X22kke8lGbonOyuE.htm](book-of-the-dead-bestiary-items/X22kke8lGbonOyuE.htm)|Lifesense 60 feet|Perception de la vie 18 m|libre|
|[x8nenxl6un8manoy.htm](book-of-the-dead-bestiary-items/x8nenxl6un8manoy.htm)|Acrobatics|Acrobaties|libre|
|[x9iTyqF9YiSbKaz7.htm](book-of-the-dead-bestiary-items/x9iTyqF9YiSbKaz7.htm)|Frightful Presence|Présence terrifiante|libre|
|[xat38aa9u2gh8xpz.htm](book-of-the-dead-bestiary-items/xat38aa9u2gh8xpz.htm)|Curse of Eternal Sleep|Malédiction de repos éternel|libre|
|[xcdtl5hp12cdn0au.htm](book-of-the-dead-bestiary-items/xcdtl5hp12cdn0au.htm)|Necrotic Runoff|Écoulement nécrotique|libre|
|[xcxndcu0v1o1sayq.htm](book-of-the-dead-bestiary-items/xcxndcu0v1o1sayq.htm)|Fist|Poing|libre|
|[xf1472qm9rkgerh8.htm](book-of-the-dead-bestiary-items/xf1472qm9rkgerh8.htm)|Aura of Doom|Aura funeste|libre|
|[xicsjt4e3glrfz9g.htm](book-of-the-dead-bestiary-items/xicsjt4e3glrfz9g.htm)|Final Spite|Malveillance ultime|libre|
|[XlVJgIVvBqc5DGGe.htm](book-of-the-dead-bestiary-items/XlVJgIVvBqc5DGGe.htm)|Darkvision|Vision dans le noir|libre|
|[Xoqie6AhgsKZ7wJ3.htm](book-of-the-dead-bestiary-items/Xoqie6AhgsKZ7wJ3.htm)|Sudden Chill|Froid soudain|libre|
|[xOyNtZslnRcM2xBf.htm](book-of-the-dead-bestiary-items/xOyNtZslnRcM2xBf.htm)|Staff|+1,striking|Bâton de frappe +1|libre|
|[xrhhvmqzd4qp9cx9.htm](book-of-the-dead-bestiary-items/xrhhvmqzd4qp9cx9.htm)|Steady Spellcasting|Incantation fiable|libre|
|[xt70n62im3owz0y2.htm](book-of-the-dead-bestiary-items/xt70n62im3owz0y2.htm)|Crumble|Effondrement|libre|
|[xukqweotozm6rcvt.htm](book-of-the-dead-bestiary-items/xukqweotozm6rcvt.htm)|Claw|Griffe|libre|
|[xw3lqxe7mwht4qcu.htm](book-of-the-dead-bestiary-items/xw3lqxe7mwht4qcu.htm)|Claw|Griffe|libre|
|[xw3tnwsyuflmy8kr.htm](book-of-the-dead-bestiary-items/xw3tnwsyuflmy8kr.htm)|Stored Items|Stockage d'objet|libre|
|[XxTC7GYQhX1vxMLs.htm](book-of-the-dead-bestiary-items/XxTC7GYQhX1vxMLs.htm)|At-Will Spells|Sorts à volonté|libre|
|[XyqShEf9KyXNmA9C.htm](book-of-the-dead-bestiary-items/XyqShEf9KyXNmA9C.htm)|Rejuvenation|Reconstruction|libre|
|[y2p2f22o6cstzezq.htm](book-of-the-dead-bestiary-items/y2p2f22o6cstzezq.htm)|Wail|Plainte|libre|
|[Y4U4NccPky46RHKa.htm](book-of-the-dead-bestiary-items/Y4U4NccPky46RHKa.htm)|Darkvision|Vision dans le noir|libre|
|[Y5HEEWSpdfvhVsRo.htm](book-of-the-dead-bestiary-items/Y5HEEWSpdfvhVsRo.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[y75gjuac91xio97i.htm](book-of-the-dead-bestiary-items/y75gjuac91xio97i.htm)|Shortsword|Épée courte|libre|
|[y7o3rimaf8qdtr7r.htm](book-of-the-dead-bestiary-items/y7o3rimaf8qdtr7r.htm)|Mystery Ingredients|Ingrédients mystères|libre|
|[ydi6eab8julbghyg.htm](book-of-the-dead-bestiary-items/ydi6eab8julbghyg.htm)|Heretic's Smite|Châtiment de l'hérétique|libre|
|[yfqqqxd6hvhoxdj9.htm](book-of-the-dead-bestiary-items/yfqqqxd6hvhoxdj9.htm)|Athletics|Athlétisme|libre|
|[YG1rDVP0SPT5NVZR.htm](book-of-the-dead-bestiary-items/YG1rDVP0SPT5NVZR.htm)|Negative Healing|Guérison négative|libre|
|[YgBTeOvZE7Aq4UvH.htm](book-of-the-dead-bestiary-items/YgBTeOvZE7Aq4UvH.htm)|Shambling Trample|Piétinement du désastre|libre|
|[yl83ga21pc0ii43n.htm](book-of-the-dead-bestiary-items/yl83ga21pc0ii43n.htm)|Fist|Poing|libre|
|[yM9uk6VXyMHmaP4c.htm](book-of-the-dead-bestiary-items/yM9uk6VXyMHmaP4c.htm)|Seep Blood|Suinte le sang|libre|
|[yn0676ooolga6aov.htm](book-of-the-dead-bestiary-items/yn0676ooolga6aov.htm)|Claw|Griffe|libre|
|[yof80btzc6ifgcl4.htm](book-of-the-dead-bestiary-items/yof80btzc6ifgcl4.htm)|Battle Axe|Hache d'armes|libre|
|[ypbtlxics2321ox1.htm](book-of-the-dead-bestiary-items/ypbtlxics2321ox1.htm)|Regenerating Bond|Lien revitalisant|libre|
|[YPP8yxLHuW6dBk3A.htm](book-of-the-dead-bestiary-items/YPP8yxLHuW6dBk3A.htm)|Darkness (At Will)|Ténèbres (À volonté)|officielle|
|[yQiPlzwxHHKQB3D2.htm](book-of-the-dead-bestiary-items/yQiPlzwxHHKQB3D2.htm)|Scroll of True Seeing (Level 6)|Parchemin de Vision lucide (Niveau 6)|libre|
|[yqze7ouayizxybn1.htm](book-of-the-dead-bestiary-items/yqze7ouayizxybn1.htm)|Aura of Charm|Aura de charme|libre|
|[yRj5K09fzH4SzWgC.htm](book-of-the-dead-bestiary-items/yRj5K09fzH4SzWgC.htm)|Darkvision|Vision dans le noir|libre|
|[yrk0vsss2csfbcxn.htm](book-of-the-dead-bestiary-items/yrk0vsss2csfbcxn.htm)|Self-Loathing|Auto-dénigrement|libre|
|[ytyjg7p3zpuhfcl3.htm](book-of-the-dead-bestiary-items/ytyjg7p3zpuhfcl3.htm)|Games Lore|Connaissance ludique|officielle|
|[Yv49fZ7FOxFBIwPP.htm](book-of-the-dead-bestiary-items/Yv49fZ7FOxFBIwPP.htm)|Divine Innate Spells|Sorts divins innés|libre|
|[yWkcNDsv41WwuTaJ.htm](book-of-the-dead-bestiary-items/yWkcNDsv41WwuTaJ.htm)|Primal Innate Spells|Sorts primordiaux innés|libre|
|[YWKIM8D6A5hTxCGj.htm](book-of-the-dead-bestiary-items/YWKIM8D6A5hTxCGj.htm)|Darkvision|Vision dans le noir|libre|
|[yXCq63vokzp9doFR.htm](book-of-the-dead-bestiary-items/yXCq63vokzp9doFR.htm)|Negative Healing|Guérison négative|libre|
|[YXtsCv8FMKUxjjDY.htm](book-of-the-dead-bestiary-items/YXtsCv8FMKUxjjDY.htm)|Telepathy 100 feet|Télépathie 30 m|libre|
|[YykK3Rlmno0gXfji.htm](book-of-the-dead-bestiary-items/YykK3Rlmno0gXfji.htm)|Darkvision|Vision dans le noir|libre|
|[yZSthxhBNrgEZuJQ.htm](book-of-the-dead-bestiary-items/yZSthxhBNrgEZuJQ.htm)|Grab|Empoignade/Agrippement|libre|
|[z0m4exlsxdjib6e2.htm](book-of-the-dead-bestiary-items/z0m4exlsxdjib6e2.htm)|Shamble Forth!|Avancez au combat !|libre|
|[z3ckvu9q74atbpee.htm](book-of-the-dead-bestiary-items/z3ckvu9q74atbpee.htm)|Feed on the Living|Se nourrir des vivants|libre|
|[z4v0g409xiuvao4u.htm](book-of-the-dead-bestiary-items/z4v0g409xiuvao4u.htm)|Baleful Gaze|Regard funeste|libre|
|[z7svy61gizn3hcsn.htm](book-of-the-dead-bestiary-items/z7svy61gizn3hcsn.htm)|Jaws|Mâchoires|libre|
|[Zb4v92oZHr85fHnO.htm](book-of-the-dead-bestiary-items/Zb4v92oZHr85fHnO.htm)|Darkvision|Vision dans le noir|libre|
|[zboaidBufdR4dpax.htm](book-of-the-dead-bestiary-items/zboaidBufdR4dpax.htm)|Whispering Scythe|Faux murmurante|libre|
|[zdy9kx6yjmJEYk7V.htm](book-of-the-dead-bestiary-items/zdy9kx6yjmJEYk7V.htm)|Arcane Prepared Spells|Sorts arcaniques préparés|libre|
|[ZeX1y7UFISFL1frx.htm](book-of-the-dead-bestiary-items/ZeX1y7UFISFL1frx.htm)|Dwarven War Axe (Broken)|Hache de guerre naire brisée|libre|
|[zFCU1ww4cUWHDUWa.htm](book-of-the-dead-bestiary-items/zFCU1ww4cUWHDUWa.htm)|Constant Spells|Sorts constants|libre|
|[zg9njsvehofdrf4r.htm](book-of-the-dead-bestiary-items/zg9njsvehofdrf4r.htm)|Claw|Griffe|libre|
|[zh8ndbyu6spjabea.htm](book-of-the-dead-bestiary-items/zh8ndbyu6spjabea.htm)|Sailing Lore|Connaissance de la navigation|officielle|
|[zhuzs5628vicodv5.htm](book-of-the-dead-bestiary-items/zhuzs5628vicodv5.htm)|Set Defense|Position de défense|libre|
|[ZIt2oLTwffPTkqT4.htm](book-of-the-dead-bestiary-items/ZIt2oLTwffPTkqT4.htm)|+1 Status to All Saves vs. Positive|bonus de statut aux JdS contre les effets positifs|libre|
|[zl5zkuozrgn1ttrm.htm](book-of-the-dead-bestiary-items/zl5zkuozrgn1ttrm.htm)|Crafting|Artisanat|libre|
|[ZmcRFvC9pG00P8gI.htm](book-of-the-dead-bestiary-items/ZmcRFvC9pG00P8gI.htm)|Greater Darkvision|Vision dans le noir supérieure|libre|
|[ZMfdgAIrZqi3yhIl.htm](book-of-the-dead-bestiary-items/ZMfdgAIrZqi3yhIl.htm)|Darkvision|Vision dans le noir|libre|
|[zmsoavgcearhti47.htm](book-of-the-dead-bestiary-items/zmsoavgcearhti47.htm)|Slow|Lent|libre|
|[znu8ybij44epq6vh.htm](book-of-the-dead-bestiary-items/znu8ybij44epq6vh.htm)|Exemplar of Violence|Exemple de violence|libre|
|[ZoUf0bRzsx7T7fTw.htm](book-of-the-dead-bestiary-items/ZoUf0bRzsx7T7fTw.htm)|Negative Healing|Guérison négative|libre|
|[zp0eidljzesefa24.htm](book-of-the-dead-bestiary-items/zp0eidljzesefa24.htm)|Repartee Riposte|Riposte qui a du répondant|libre|
|[zpLChqdAZDg9x9Lg.htm](book-of-the-dead-bestiary-items/zpLChqdAZDg9x9Lg.htm)|Grab|Empoignade/Agrippement|libre|
|[ZPZh59h8oJbpPBr5.htm](book-of-the-dead-bestiary-items/ZPZh59h8oJbpPBr5.htm)|Darkvision|Vision dans le noir|libre|
|[zqe60ky12wasdnz6.htm](book-of-the-dead-bestiary-items/zqe60ky12wasdnz6.htm)|Drowning Grasp|Noyade agrippée|libre|
|[ZRadUe9v3qyF3o2k.htm](book-of-the-dead-bestiary-items/ZRadUe9v3qyF3o2k.htm)|Troop Defenses|Défense de troupe|libre|
|[zrycs1fp75o2l8h0.htm](book-of-the-dead-bestiary-items/zrycs1fp75o2l8h0.htm)|Bite|Morsure|libre|
|[zu5kwhnrxafjmru7.htm](book-of-the-dead-bestiary-items/zu5kwhnrxafjmru7.htm)|Axe Vulnerability|Vulnérabilité à la hache|libre|
|[ZuLQYlg9AglGAJp6.htm](book-of-the-dead-bestiary-items/ZuLQYlg9AglGAJp6.htm)|Scent (Imprecise) 30 feet|Odorat 9 m (imprécis)|libre|
|[zwnm8cclbjl2zo8c.htm](book-of-the-dead-bestiary-items/zwnm8cclbjl2zo8c.htm)|Heretic's Smite|Châtiment de l'hérétique|libre|
|[Zyy2H5wK3HtqNuuy.htm](book-of-the-dead-bestiary-items/Zyy2H5wK3HtqNuuy.htm)|Negative Healing|Guérison négative|libre|
