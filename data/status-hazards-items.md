# État de la traduction (hazards-items)

 * **officielle**: 73
 * **libre**: 6


Dernière mise à jour: 2023-07-12 20:28 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0fjRyLkdj1a2kfGM.htm](hazards-items/0fjRyLkdj1a2kfGM.htm)|Falling Scythes|Chute de lames de faux|officielle|
|[0FpzSygcs7G688GT.htm](hazards-items/0FpzSygcs7G688GT.htm)|Quietus|Coup de grâce|officielle|
|[0KrOaCLFSEcIdcej.htm](hazards-items/0KrOaCLFSEcIdcej.htm)|Spear|Lance-épieu|officielle|
|[17gJOHIRtWXfYMKa.htm](hazards-items/17gJOHIRtWXfYMKa.htm)|Sap Vitality|Absorption de vitalité|officielle|
|[1bvXbELRBoJTBqsR.htm](hazards-items/1bvXbELRBoJTBqsR.htm)|Spring|Ressort|officielle|
|[1M9OasbKWAPME9IC.htm](hazards-items/1M9OasbKWAPME9IC.htm)|Summon Monster|Convocation de monstre|officielle|
|[34y07VryqAVeJhfG.htm](hazards-items/34y07VryqAVeJhfG.htm)|Slam Shut|Chute du bloc|officielle|
|[3oSIIlS5PZwciZ3k.htm](hazards-items/3oSIIlS5PZwciZ3k.htm)|Poisoned Dart|Fléchette empoisonnée|officielle|
|[4QF1yNgj2FX7rO4C.htm](hazards-items/4QF1yNgj2FX7rO4C.htm)|Total Decapitation|Décapitation totale|officielle|
|[6wV9i41Jd4HThaAi.htm](hazards-items/6wV9i41Jd4HThaAi.htm)|Speed|Vitesse|officielle|
|[8yNeU3TLczTIlFjP.htm](hazards-items/8yNeU3TLczTIlFjP.htm)|Lunging Dead|Fente des morts|libre|
|[aJouqXgwCGFTWYHD.htm](hazards-items/aJouqXgwCGFTWYHD.htm)|Objects|Objets|officielle|
|[aNYeaG5N3o5PQ9IB.htm](hazards-items/aNYeaG5N3o5PQ9IB.htm)|Dissolving Ambush|Embuscade caustique|officielle|
|[B486xwl3ZYYCqNyw.htm](hazards-items/B486xwl3ZYYCqNyw.htm)|Spear|Lance|officielle|
|[Bov9feGVYpn5nmUl.htm](hazards-items/Bov9feGVYpn5nmUl.htm)|Prelude|Prélude|officielle|
|[caUgV11OydLsLw1j.htm](hazards-items/caUgV11OydLsLw1j.htm)|Forbid Entry|Interdiction d'entrer|officielle|
|[Cc30YjKZ7qSxVjSC.htm](hazards-items/Cc30YjKZ7qSxVjSC.htm)|Curse the Intruders|Maudits soient les intrus|officielle|
|[CHZ0RRKgpq6eI2AS.htm](hazards-items/CHZ0RRKgpq6eI2AS.htm)|Ensnare|Prise au piège|officielle|
|[ddB24NYRgK5YbbcR.htm](hazards-items/ddB24NYRgK5YbbcR.htm)|In the Beginning|Au commencement|officielle|
|[DE7tTstJaGz9oAi4.htm](hazards-items/DE7tTstJaGz9oAi4.htm)|Rockslide|Glissement de terrain|officielle|
|[E8X8ETVNwXuUF08C.htm](hazards-items/E8X8ETVNwXuUF08C.htm)|Searing Agony|Agonie calcinante|officielle|
|[e90G0Brsf6uhlGp6.htm](hazards-items/e90G0Brsf6uhlGp6.htm)|Rend Magic|Déchirure magique|officielle|
|[eauYGISAGprcOwH4.htm](hazards-items/eauYGISAGprcOwH4.htm)|Devour|Engloutissement|officielle|
|[eLyrxG4My2vDsmHg.htm](hazards-items/eLyrxG4My2vDsmHg.htm)|No MAP|Pas de PAM|libre|
|[eTpo3PPNbe4sFYhD.htm](hazards-items/eTpo3PPNbe4sFYhD.htm)|Spore Explosion|Explosion de spores|officielle|
|[eY731rvsYJZ2o5Sh.htm](hazards-items/eY731rvsYJZ2o5Sh.htm)|Flesset Poison|Poison flesset|officielle|
|[Ey8CknllSG5csxdG.htm](hazards-items/Ey8CknllSG5csxdG.htm)|Submerge|Engloutissement|officielle|
|[F0I1pLJQm1AQtXPH.htm](hazards-items/F0I1pLJQm1AQtXPH.htm)|Rising Pillar|Surgissement de la barre|officielle|
|[f9aVsy30iGDJTtb4.htm](hazards-items/f9aVsy30iGDJTtb4.htm)|Snap Shut|Occlusion rapide|officielle|
|[FzfgLXTt6R8bYmTP.htm](hazards-items/FzfgLXTt6R8bYmTP.htm)|Electrocution|Électrocution|officielle|
|[gxjS3APksgflCuua.htm](hazards-items/gxjS3APksgflCuua.htm)|Burst Free|Rupture|officielle|
|[h1kUAmChVAo2xByF.htm](hazards-items/h1kUAmChVAo2xByF.htm)|Clockwork Fist|Poing mécanique|officielle|
|[hEtVGo9EpKcXlhPI.htm](hazards-items/hEtVGo9EpKcXlhPI.htm)|Spectral Impale|Empalement spectral|officielle|
|[HhzDfHd26GkdqhME.htm](hazards-items/HhzDfHd26GkdqhME.htm)|Shock|Choc|libre|
|[hK2a0NJwqQgjs15q.htm](hazards-items/hK2a0NJwqQgjs15q.htm)|Mark for Damnation|Marque de damnation|officielle|
|[hkSWLZaKG41zef8w.htm](hazards-items/hkSWLZaKG41zef8w.htm)|Continuous Barrage|Tir de barrage ininterrompu|officielle|
|[hoSnbd89PgLy85mP.htm](hazards-items/hoSnbd89PgLy85mP.htm)|Snowdrop|Chute de neige|officielle|
|[HoWRGPkBlJjsL9zl.htm](hazards-items/HoWRGPkBlJjsL9zl.htm)|Cladis Poison|Poison cladis|officielle|
|[i9qsvHuq9IdeRohO.htm](hazards-items/i9qsvHuq9IdeRohO.htm)|Call of the Ground|Appel du vide|officielle|
|[iji2yCS44gpqc5p3.htm](hazards-items/iji2yCS44gpqc5p3.htm)|Pitfall|Fosse|officielle|
|[j3QvHDCU1gvOInG4.htm](hazards-items/j3QvHDCU1gvOInG4.htm)|Deadfall|Assommoir|officielle|
|[JEXj6WkaqB0iscnL.htm](hazards-items/JEXj6WkaqB0iscnL.htm)|Reflection of Evil|Reflet maléfique|officielle|
|[jSBfyDGUXAPbIJTT.htm](hazards-items/jSBfyDGUXAPbIJTT.htm)|Whirling Blades|Lames tourbillonnantes|officielle|
|[K7Ya0U5jerfvH0vw.htm](hazards-items/K7Ya0U5jerfvH0vw.htm)|Into the Great Beyond|Vers le Grand Au-Delà|officielle|
|[KaYeYfefttO5X9OF.htm](hazards-items/KaYeYfefttO5X9OF.htm)|Burn It All|Tout doit brûler|officielle|
|[KFCwJINs8Uu4riRs.htm](hazards-items/KFCwJINs8Uu4riRs.htm)|Dart Volley|Volée de fléchettes|officielle|
|[lY83oUjx0DLxDByK.htm](hazards-items/lY83oUjx0DLxDByK.htm)|Pitfall|Fosse|officielle|
|[LZTjCUaLv54zwB8W.htm](hazards-items/LZTjCUaLv54zwB8W.htm)|Web Noose|Noeud coulant|officielle|
|[mPVesRXUMN4Ws9IR.htm](hazards-items/mPVesRXUMN4Ws9IR.htm)|Decapitation|Décapitation|officielle|
|[MQ3CKYIdF6zrMg0x.htm](hazards-items/MQ3CKYIdF6zrMg0x.htm)|Leech Warmth|Absorption de chaleur|officielle|
|[nCcx1LK7XrbDnVXI.htm](hazards-items/nCcx1LK7XrbDnVXI.htm)|Sportlebore Infestation|Infestation d'imitapéros|officielle|
|[NcyiM0WihYrncbSF.htm](hazards-items/NcyiM0WihYrncbSF.htm)|Emit Cold|Émission de froid|officielle|
|[NjmHPcx6IEA14Yul.htm](hazards-items/NjmHPcx6IEA14Yul.htm)|Powder Burst|Nuage de poudre|officielle|
|[nqcgUfOiRWy6Kti7.htm](hazards-items/nqcgUfOiRWy6Kti7.htm)|Scream|Cri|officielle|
|[oaNkRH35bkjdSm6P.htm](hazards-items/oaNkRH35bkjdSm6P.htm)|Scythe|Faux|officielle|
|[oZGdTS4jvoAj1myH.htm](hazards-items/oZGdTS4jvoAj1myH.htm)|Profane Chant|Chant impie|officielle|
|[pLuxXYKq1Wi28Czn.htm](hazards-items/pLuxXYKq1Wi28Czn.htm)|Hammer|Marteau|officielle|
|[Pz2JfI4HEqH5iEwA.htm](hazards-items/Pz2JfI4HEqH5iEwA.htm)|Filth Fever|Fièvre de la fange|libre|
|[Q2074Gj6y8XUUgAk.htm](hazards-items/Q2074Gj6y8XUUgAk.htm)|Awaken|Éveil|libre|
|[qBQpsAk64L9c0sJ2.htm](hazards-items/qBQpsAk64L9c0sJ2.htm)|Titanic Flytrap Toxin|Toxine d'attrape-mouche titanesque|officielle|
|[QrgnumULVnz8pWfK.htm](hazards-items/QrgnumULVnz8pWfK.htm)|Unmask|Démasquer|officielle|
|[sBSa8YzXeDYM0ODl.htm](hazards-items/sBSa8YzXeDYM0ODl.htm)|Shriek|Hurlement|officielle|
|[slQb54KbvJ8vJpRb.htm](hazards-items/slQb54KbvJ8vJpRb.htm)|Flume Activation|Activation du déverseur|officielle|
|[SoObyOgEreU0KLDs.htm](hazards-items/SoObyOgEreU0KLDs.htm)|Adrift in Time|Dérive temporelle|officielle|
|[SvqJNCRH2eI1jZk1.htm](hazards-items/SvqJNCRH2eI1jZk1.htm)|Steam Blast|Explosion de vapeur|libre|
|[SvVSBExFgbjDrBvW.htm](hazards-items/SvVSBExFgbjDrBvW.htm)|Agitate|Agitation|officielle|
|[T0k2Rnyz49qG1Gkv.htm](hazards-items/T0k2Rnyz49qG1Gkv.htm)|Saw Blade|Lame de scie|officielle|
|[TxhvkFafggYWv5D7.htm](hazards-items/TxhvkFafggYWv5D7.htm)|Baleful Polymorph|Métamorphose funeste|officielle|
|[tyAeSBnHmP9Nuh2k.htm](hazards-items/tyAeSBnHmP9Nuh2k.htm)|Yellow Mold Spores|Spores de moisissure jaune|officielle|
|[uJpg7vUJ1qag7R3t.htm](hazards-items/uJpg7vUJ1qag7R3t.htm)|Fireball|Boule de feu|officielle|
|[V7tWWuB9NSEsMEae.htm](hazards-items/V7tWWuB9NSEsMEae.htm)|Special|Spécial|officielle|
|[vFBoAHwlrVTu9wKm.htm](hazards-items/vFBoAHwlrVTu9wKm.htm)|Spine|Pointe|officielle|
|[VNaaW87S24zzxXZh.htm](hazards-items/VNaaW87S24zzxXZh.htm)|Noose|Noeud coulant|officielle|
|[vS5xxWVCLY1BNE4O.htm](hazards-items/vS5xxWVCLY1BNE4O.htm)|Spinning Blade|Lame tournoyante|officielle|
|[wdA0nAhiSTY7znrs.htm](hazards-items/wdA0nAhiSTY7znrs.htm)|Shadow Barbs|Ardillons d'ombre|officielle|
|[WJtDPJzNrJO19H6k.htm](hazards-items/WJtDPJzNrJO19H6k.htm)|Infinite Pitfall|Fosse sans fond|officielle|
|[WleycA2DC7Pz8RYR.htm](hazards-items/WleycA2DC7Pz8RYR.htm)|Mimic Food|Imitation de nourriture|officielle|
|[Yw05wWhZB9fLdatc.htm](hazards-items/Yw05wWhZB9fLdatc.htm)|Jaws|Mâchoires|officielle|
|[ZS4b3rYv9jOgbMzm.htm](hazards-items/ZS4b3rYv9jOgbMzm.htm)|Wheel Spin|La roue tourne|officielle|
