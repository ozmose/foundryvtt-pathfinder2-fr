# État de la traduction (gmg-srd)

 * **changé**: 49
 * **officielle**: 49


Dernière mise à jour: 2023-07-12 20:28 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[2cyhXgGkmoFe3Phw.htm](gmg-srd/2cyhXgGkmoFe3Phw.htm)|Trained by a Mentor|Formé par un Mentor|changé|
|[652cIWAXc8zAxkac.htm](gmg-srd/652cIWAXc8zAxkac.htm)|Matter of Might|Une question de force|changé|
|[7qsQBWD5sSGz7Fik.htm](gmg-srd/7qsQBWD5sSGz7Fik.htm)|Mercantile Expertise|Expertise marchande|changé|
|[a0mJO7io1hcwMAjJ.htm](gmg-srd/a0mJO7io1hcwMAjJ.htm)|Lost in the Wilderness|Perdu dans la nature|changé|
|[BbgtR5LmB9ZdPpx1.htm](gmg-srd/BbgtR5LmB9ZdPpx1.htm)|Timely Cure|Remède opportun|changé|
|[bNmSwKSFk6SWWrlj.htm](gmg-srd/bNmSwKSFk6SWWrlj.htm)|Abandoned in a Distant Land|Abandonné dans un pays lointain|changé|
|[bRQz8uVlYtQ1cwwI.htm](gmg-srd/bRQz8uVlYtQ1cwwI.htm)|Wasteland Survivors|Survivants d'une région sauvage|changé|
|[c8bBOo2RPxStZozM.htm](gmg-srd/c8bBOo2RPxStZozM.htm)|Moon|Lune|changé|
|[cD51eAUtbGlp5UKr.htm](gmg-srd/cD51eAUtbGlp5UKr.htm)|Slander|Calomnies|changé|
|[CfD81LilQAtGuvyx.htm](gmg-srd/CfD81LilQAtGuvyx.htm)|The Void|Le néant|changé|
|[dcP3oqjsl85p6R2Z.htm](gmg-srd/dcP3oqjsl85p6R2Z.htm)|Kidnapped|Enlevé|changé|
|[e4nkDjB1Y6nqaK2G.htm](gmg-srd/e4nkDjB1Y6nqaK2G.htm)|Betrayed|Trahi|changé|
|[eADadfrHPkGNTjEB.htm](gmg-srd/eADadfrHPkGNTjEB.htm)|Accidental Fall|Chute accidentelle|changé|
|[FOs2eLbehUzPh9p9.htm](gmg-srd/FOs2eLbehUzPh9p9.htm)|Attained a Magical Gift|Dépositaire d'un talent magique|changé|
|[G9058881e1ci9X1C.htm](gmg-srd/G9058881e1ci9X1C.htm)|Desperate Intimidation|Intimidation désespérée|changé|
|[iA9QWuYATBPB6thI.htm](gmg-srd/iA9QWuYATBPB6thI.htm)|Knight|Chevalier|changé|
|[idZxjeEv8B3x44r1.htm](gmg-srd/idZxjeEv8B3x44r1.htm)|Captured by Giants|Capturé par des géants|changé|
|[IJ3nFDu2YcoP6ynd.htm](gmg-srd/IJ3nFDu2YcoP6ynd.htm)|Throne|Trône|changé|
|[Ip6kSKGhl3XHQZ93.htm](gmg-srd/Ip6kSKGhl3XHQZ93.htm)|Robbed|Dépouillé|changé|
|[j9kx5CzfulaFTMOE.htm](gmg-srd/j9kx5CzfulaFTMOE.htm)|Academy Trained|Formation universitaire|changé|
|[JHQI2skcksaLMPqA.htm](gmg-srd/JHQI2skcksaLMPqA.htm)|Kindly Witch|Sorcier bienveillant|changé|
|[jtgfTYpcv4FFpWsL.htm](gmg-srd/jtgfTYpcv4FFpWsL.htm)|Died|Mort|changé|
|[jVh90bfsC6w6iVyy.htm](gmg-srd/jVh90bfsC6w6iVyy.htm)|Euryale (curse)|Euryale (malédiction)|changé|
|[LekxXsOgEwMODqCS.htm](gmg-srd/LekxXsOgEwMODqCS.htm)|Animal Helpers|Amis des animaux|changé|
|[llfG8iMTFMzVIF9s.htm](gmg-srd/llfG8iMTFMzVIF9s.htm)|Donjon|Donjon|changé|
|[niZPxqupDMfIXxJs.htm](gmg-srd/niZPxqupDMfIXxJs.htm)|Homelessness|Vagabondage|changé|
|[nsZ93vUEFucsLwRz.htm](gmg-srd/nsZ93vUEFucsLwRz.htm)|Privileged Position|Statut privilégié|changé|
|[nzOpdMVLPhoNTxzR.htm](gmg-srd/nzOpdMVLPhoNTxzR.htm)|Comrade-in-Arms|Frères d'armes|changé|
|[oDhTOO1WjEZk1qfD.htm](gmg-srd/oDhTOO1WjEZk1qfD.htm)|Witnessed War|Témoin d'une guerre|changé|
|[OHKKQ7zTf61vz35h.htm](gmg-srd/OHKKQ7zTf61vz35h.htm)|Liberators|Libérateurs|changé|
|[PVdcBHjBVySBchG1.htm](gmg-srd/PVdcBHjBVySBchG1.htm)|Religious Students|Séminaristes|changé|
|[pwn7lPcwqyGNZyOo.htm](gmg-srd/pwn7lPcwqyGNZyOo.htm)|Survived a Disaster|Survivant d'un désastre|changé|
|[rDbdifseYxfEWgKg.htm](gmg-srd/rDbdifseYxfEWgKg.htm)|Patron of the Arts|Bienfaiteur des arts|changé|
|[Runwg9xhNrN79JBN.htm](gmg-srd/Runwg9xhNrN79JBN.htm)|Had an Ordinary Childhood|Enfance ordinaire|changé|
|[sHC4quiLAzdUe2uT.htm](gmg-srd/sHC4quiLAzdUe2uT.htm)|Accusation of Theft|Accusation de vol|changé|
|[UGxZ6vhfq7vQFXTt.htm](gmg-srd/UGxZ6vhfq7vQFXTt.htm)|Magician|Magicien|changé|
|[upnKpiEflAhhFLNQ.htm](gmg-srd/upnKpiEflAhhFLNQ.htm)|Missing Child|Enfant perdu|changé|
|[VEFcfA4NnWdJSRtp.htm](gmg-srd/VEFcfA4NnWdJSRtp.htm)|Spy|Espion|changé|
|[vMTOwBTqs8JeE8bH.htm](gmg-srd/vMTOwBTqs8JeE8bH.htm)|Rival Trackers|Pisteurs rivaux|changé|
|[VnrGXdVthTceOlXV.htm](gmg-srd/VnrGXdVthTceOlXV.htm)|Another Ancestry's Settlement|Une communauté d'une autre ascendance|changé|
|[W2ccu0zeLNHDlCXe.htm](gmg-srd/W2ccu0zeLNHDlCXe.htm)|Bullied|Persécuté|changé|
|[wIGRRmXtzed74qQq.htm](gmg-srd/wIGRRmXtzed74qQq.htm)|Fell In with a Bad Crowd|Mauvaises fréquentations|changé|
|[WXQbdAmQM9HBjc0R.htm](gmg-srd/WXQbdAmQM9HBjc0R.htm)|Relationship Ender|Briseur de relations|changé|
|[xGkT6QZGfYE3kBqn.htm](gmg-srd/xGkT6QZGfYE3kBqn.htm)|Skull|Crâne|changé|
|[xmgvqyfwYPBXVELo.htm](gmg-srd/xmgvqyfwYPBXVELo.htm)|Met a Fantastic Creature|Rencontre avec une créature fantastique|changé|
|[y4quheEoo2bcmM6g.htm](gmg-srd/y4quheEoo2bcmM6g.htm)|Seeking Accolades|Chercheur d'attention|changé|
|[yqCZWcX9e3iwmvSS.htm](gmg-srd/yqCZWcX9e3iwmvSS.htm)|Social Maneuvering|Manipulations sociales|changé|
|[YsY9euHOGCGc3mTE.htm](gmg-srd/YsY9euHOGCGc3mTE.htm)|Won a Competition|Vainqueur d'une compétition|changé|
|[yZUZDw8TXX1MvZ4P.htm](gmg-srd/yZUZDw8TXX1MvZ4P.htm)|Called Before Judges|Convoqué devant les juges|changé|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[3g3uXFsHbfcad8eA.htm](gmg-srd/3g3uXFsHbfcad8eA.htm)|The Fool|Le bouffon|officielle|
|[3Uwfn0ui7tS277iE.htm](gmg-srd/3Uwfn0ui7tS277iE.htm)|Metropolis|Métropole|officielle|
|[5KDGPcdZpdZumUYy.htm](gmg-srd/5KDGPcdZpdZumUYy.htm)|Itinerant|Itinérant|officielle|
|[6NOZe9gUfVEedECW.htm](gmg-srd/6NOZe9gUfVEedECW.htm)|Star|Célébrité|officielle|
|[8NKe8N5NsULPShPX.htm](gmg-srd/8NKe8N5NsULPShPX.htm)|The Dead One|Le défunt|officielle|
|[AL562TPAhLCFX62S.htm](gmg-srd/AL562TPAhLCFX62S.htm)|Underground|Sous terre|officielle|
|[Alf16WU6qK8w1jmq.htm](gmg-srd/Alf16WU6qK8w1jmq.htm)|Sun|Soleil|officielle|
|[amEb5dGmSvvAJz7J.htm](gmg-srd/amEb5dGmSvvAJz7J.htm)|Balance|Balance|officielle|
|[BAs1fCaUTmvRjvVT.htm](gmg-srd/BAs1fCaUTmvRjvVT.htm)|Dullard|Idiot|officielle|
|[bvhckAGjcCJVs0jn.htm](gmg-srd/bvhckAGjcCJVs0jn.htm)|Claimed an Inheritance|Bénéficiaire d'un héritage|officielle|
|[C4KgMYzmyWVpe721.htm](gmg-srd/C4KgMYzmyWVpe721.htm)|Fool|Fou|officielle|
|[ccG9pxYQ9Gg6262b.htm](gmg-srd/ccG9pxYQ9Gg6262b.htm)|Key|Clé|officielle|
|[Cfd7ndT61VjR52OZ.htm](gmg-srd/Cfd7ndT61VjR52OZ.htm)|The Mercenary|Le mercenaire|officielle|
|[cIlNXxPJuy8MtaDM.htm](gmg-srd/cIlNXxPJuy8MtaDM.htm)|Vizier|Le vizir|officielle|
|[CW53y8R730XoNQSG.htm](gmg-srd/CW53y8R730XoNQSG.htm)|The Champion|Le champion|officielle|
|[CwyYDIAAuRll2i5R.htm](gmg-srd/CwyYDIAAuRll2i5R.htm)|The Seer|Le devin|officielle|
|[Dyg3vJnEnReKyYHU.htm](gmg-srd/Dyg3vJnEnReKyYHU.htm)|The Fates|Les Parques|officielle|
|[eCB10E0MSvD9s008.htm](gmg-srd/eCB10E0MSvD9s008.htm)|Frontier|Confins|officielle|
|[ejRPZwAlci3jTli9.htm](gmg-srd/ejRPZwAlci3jTli9.htm)|The Liege Lord|Le suzerain|officielle|
|[Erqgqv8WEtnzBtTu.htm](gmg-srd/Erqgqv8WEtnzBtTu.htm)|The Mystic|Le mystique|officielle|
|[F7Tf6APgUKjnCZgv.htm](gmg-srd/F7Tf6APgUKjnCZgv.htm)|The Crafter|L'artisan|officielle|
|[fojjq3VXShnNSOsZ.htm](gmg-srd/fojjq3VXShnNSOsZ.htm)|Academic Community|Communauté universitaire|officielle|
|[Gm4AtuFNRgOnzsmF.htm](gmg-srd/Gm4AtuFNRgOnzsmF.htm)|Simple Village|Simple village|officielle|
|[Gr4v8ROPCxi4hvAO.htm](gmg-srd/Gr4v8ROPCxi4hvAO.htm)|Religious Community|Communauté religieuse|officielle|
|[HkMzwjBTGgQgsOCW.htm](gmg-srd/HkMzwjBTGgQgsOCW.htm)|Cosmopolitan City|Cité cosmopolite|officielle|
|[KtkDaTrkBwwupKcE.htm](gmg-srd/KtkDaTrkBwwupKcE.htm)|The Wanderer|Le voyageur|officielle|
|[MCIp76IWZ2pk7dyz.htm](gmg-srd/MCIp76IWZ2pk7dyz.htm)|The Criminal|Le criminel|officielle|
|[mg7oMp0cNa0GJMCS.htm](gmg-srd/mg7oMp0cNa0GJMCS.htm)|The Lover|L'amant|officielle|
|[mHocbJQKBzbHM790.htm](gmg-srd/mHocbJQKBzbHM790.htm)|The Confidante|Le confident|officielle|
|[NSG1YxOYAtSowNJa.htm](gmg-srd/NSG1YxOYAtSowNJa.htm)|Comet|Comète|officielle|
|[OdD3NuxI5hNAEOVx.htm](gmg-srd/OdD3NuxI5hNAEOVx.htm)|Trade Town|Ville marchande|officielle|
|[pd6ZzGL5TA58Fa3p.htm](gmg-srd/pd6ZzGL5TA58Fa3p.htm)|The Hunter|Le chasseur|officielle|
|[qFgPiHDBVa3VSNgC.htm](gmg-srd/qFgPiHDBVa3VSNgC.htm)|Gem|Gemme|officielle|
|[QhNWhs0isjxE67lu.htm](gmg-srd/QhNWhs0isjxE67lu.htm)|The Fiend|Le fiélon|officielle|
|[QxQ0PheFSIJU9rfH.htm](gmg-srd/QxQ0PheFSIJU9rfH.htm)|Talons|Serres|officielle|
|[re8nmHRiQdU63AYk.htm](gmg-srd/re8nmHRiQdU63AYk.htm)|The Boss|Le patron|officielle|
|[Rh5XWscABJ43TLme.htm](gmg-srd/Rh5XWscABJ43TLme.htm)|The Mentor|Le mentor|officielle|
|[SbfCzPannCdZEB7c.htm](gmg-srd/SbfCzPannCdZEB7c.htm)|Jester|Bouffon|officielle|
|[SyEl079jcULQp7Ra.htm](gmg-srd/SyEl079jcULQp7Ra.htm)|Front Lines|Ligne de Front|officielle|
|[TG9L9JvKpomFQUfl.htm](gmg-srd/TG9L9JvKpomFQUfl.htm)|Flames|Flammes|officielle|
|[tmrnZkUzHVC0s6D9.htm](gmg-srd/tmrnZkUzHVC0s6D9.htm)|Had Your First Kill|Premier meurtre|officielle|
|[TNruMcXK4wizclQc.htm](gmg-srd/TNruMcXK4wizclQc.htm)|Raided|Pillage|officielle|
|[uK1fqqsUNk7FZsOk.htm](gmg-srd/uK1fqqsUNk7FZsOk.htm)|The Pariah|Le paria|officielle|
|[vD1y8mIHxxw7c8J1.htm](gmg-srd/vD1y8mIHxxw7c8J1.htm)|Coastal Community|Communauté côtière|officielle|
|[x0vU6WsHcXUNY20y.htm](gmg-srd/x0vU6WsHcXUNY20y.htm)|Ruin|Ruine|officielle|
|[xe0iqQ3XSnDV4tLp.htm](gmg-srd/xe0iqQ3XSnDV4tLp.htm)|Rogue|Traître|officielle|
|[Xi5nvMNJrzsy09ws.htm](gmg-srd/Xi5nvMNJrzsy09ws.htm)|The Well-Connected|L'ami aux nombreuses relations|officielle|
|[YR0uxB5CPPueFjBs.htm](gmg-srd/YR0uxB5CPPueFjBs.htm)|The Relative|Le parent|officielle|
|[ziWhyyygUG3Lemdu.htm](gmg-srd/ziWhyyygUG3Lemdu.htm)|The Academic|Le professeur|officielle|
