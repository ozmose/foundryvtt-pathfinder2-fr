# État de la traduction (feat-effects)

 * **libre**: 351


Dernière mise à jour: 2023-07-12 20:28 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0AD7BiKjT8a6Uh92.htm](feat-effects/0AD7BiKjT8a6Uh92.htm)|Effect: Energetic Meltdown|Effet : Effondrement énergétique|libre|
|[0JrHvdUgJBl631En.htm](feat-effects/0JrHvdUgJBl631En.htm)|Effect: Juvenile Flight|Effet : Vol juvénile|libre|
|[0pq3MPLH0C9z4tj3.htm](feat-effects/0pq3MPLH0C9z4tj3.htm)|Effect: Victorious Vigor|Effet : Vigueur dans la victoire|libre|
|[0r2V1nK5pV31IUPY.htm](feat-effects/0r2V1nK5pV31IUPY.htm)|Effect: Protective Mentor Boon (Revered) (PFS)|Effet : Récompense du mentor protecteur (Révéré) (PFS)|libre|
|[18FHJoazfEmgNkfk.htm](feat-effects/18FHJoazfEmgNkfk.htm)|Effect: Aura of Preservation|Effet : Aura de préservation|libre|
|[1Azz9TSWXrX4aNX4.htm](feat-effects/1Azz9TSWXrX4aNX4.htm)|Effect: Harrow-Chosen|Effet : Choisi par le Tourment|libre|
|[1dxD3xsTzak6GNj5.htm](feat-effects/1dxD3xsTzak6GNj5.htm)|Stance: Monastic Archer Stance|Posture : Posture de l'archer monastique|libre|
|[1nCwQErK6hpkNvfw.htm](feat-effects/1nCwQErK6hpkNvfw.htm)|Effect: Dueling Parry|Effet : Parade en duel|libre|
|[1XlJ9xLzL19GHoOL.htm](feat-effects/1XlJ9xLzL19GHoOL.htm)|Effect: Overdrive|Effet : Surrégime|libre|
|[263Cd5JMj8Lgc9yz.htm](feat-effects/263Cd5JMj8Lgc9yz.htm)|Effect: Radiant Circuitry|Effet : Circuiterie rayonnante|libre|
|[2c30Drdg84bWLcRn.htm](feat-effects/2c30Drdg84bWLcRn.htm)|Effect: Emblazon Energy (Weapon, Sonic)|Effet : Énergie blasonnée (Arme, Son)|libre|
|[2ca1ZuqQ7VkunAh3.htm](feat-effects/2ca1ZuqQ7VkunAh3.htm)|Effect: Accept Echo|Effet : Accepter l'écho|libre|
|[2gVP04ZWYbQdX3uS.htm](feat-effects/2gVP04ZWYbQdX3uS.htm)|Effect: Spiral Sworn|Effet : Spirale assermentée|libre|
|[2MIn8qyPTmz4ZyO1.htm](feat-effects/2MIn8qyPTmz4ZyO1.htm)|Effect: Smite Good|Effet : Châtiment du bien|libre|
|[2Qpt0CHuOMeL48rN.htm](feat-effects/2Qpt0CHuOMeL48rN.htm)|Stance: Cobra Stance (Cobra Envenom)|Posture : Posture du cobra (Cobra envenimé)|libre|
|[2RwhJ9fbJtcQjW6s.htm](feat-effects/2RwhJ9fbJtcQjW6s.htm)|Effect: Arctic Endemic Herb|Effet : Herbes endémiques Arctique|libre|
|[2XEYQNZTCGpdkyR6.htm](feat-effects/2XEYQNZTCGpdkyR6.htm)|Effect: Battle Medicine Immunity|Effet : Immunité à Médecine militaire|libre|
|[3eHMqVx30JGiJqtM.htm](feat-effects/3eHMqVx30JGiJqtM.htm)|Stance: Twinned Defense|Posture : Défense jumelée|libre|
|[3gGBZHcUFsHLJeQH.htm](feat-effects/3gGBZHcUFsHLJeQH.htm)|Effect: Elemental Blood Magic (Self)|Effet : Magie du sang élémentaire (soi-même)|libre|
|[3GPh6O3PJxORytAC.htm](feat-effects/3GPh6O3PJxORytAC.htm)|Effect: Shadow Sight|Effet : Vision d'ombre|libre|
|[3WzaQKb10AYLdTsQ.htm](feat-effects/3WzaQKb10AYLdTsQ.htm)|Effect: Corpse-Killer's Defiance|Effet : Défi du tueur de cadavre (niveau égal ou supérieur)|libre|
|[4alr9e8w9H0RCLwI.htm](feat-effects/4alr9e8w9H0RCLwI.htm)|Effect: Tiller's Drive|Effet : Conduite du Laboureur|libre|
|[4QWayYR3JSL9bk2T.htm](feat-effects/4QWayYR3JSL9bk2T.htm)|Effect: Weapon Tampered With (Success)|Effet : Arme trafiquée (succès)|libre|
|[4UNQfMrwfWirdwoV.htm](feat-effects/4UNQfMrwfWirdwoV.htm)|Effect: Masterful Hunter's Edge, Flurry|Effet : Spécialité Maître chasseur, Déluge|libre|
|[4xtHFRGI05SNe9rA.htm](feat-effects/4xtHFRGI05SNe9rA.htm)|Effect: Hungry Goblin|Effet : Gobelin affamé|libre|
|[4Zj71naHbY6O9ggP.htm](feat-effects/4Zj71naHbY6O9ggP.htm)|Effect: Bristle|Effet : Hirsute|libre|
|[5bEnBqVOgdp4gROP.htm](feat-effects/5bEnBqVOgdp4gROP.htm)|Effect: Catfolk Dance|Effet : Danse homme-félin|libre|
|[5IGz4iheaiUWm5KR.htm](feat-effects/5IGz4iheaiUWm5KR.htm)|Effect: Eye of the Arclords|Effet : Oeil des Seigneurs de l'Arc|libre|
|[5PIaLkys5ZqP2BUv.htm](feat-effects/5PIaLkys5ZqP2BUv.htm)|Effect: Primal Aegis|Effet : Égide primordiale|libre|
|[5TzKmEqFyLHBG2ua.htm](feat-effects/5TzKmEqFyLHBG2ua.htm)|Effect: Emblazon Energy (Weapon, Cold)|Effet : Énergie blasonnée (Arme, Froid)|libre|
|[5v0ndPPMfZwhiVZF.htm](feat-effects/5v0ndPPMfZwhiVZF.htm)|Effect: Predictable!|Effet : Présivisible ! (Succès)|libre|
|[5veOBmMYQxywTudd.htm](feat-effects/5veOBmMYQxywTudd.htm)|Goblin Song (Success)|Effet : Chant gobelin - Succès|libre|
|[6ACbQIpmmemxmoBJ.htm](feat-effects/6ACbQIpmmemxmoBJ.htm)|Effect: Saoc Astrology|Effet : Astrologie saoc|libre|
|[6ctQFQfSZ6o1uyyZ.htm](feat-effects/6ctQFQfSZ6o1uyyZ.htm)|Stance: Bullet Dancer Stance|Posture : Posture du danseur de balle|libre|
|[6EDoy3OSFZ4L3Vs7.htm](feat-effects/6EDoy3OSFZ4L3Vs7.htm)|Stance: Paragon's Guard|Posture : Protection du parangon|libre|
|[6fb15XuSV4TNuVAT.htm](feat-effects/6fb15XuSV4TNuVAT.htm)|Effect: Hag Blood Magic|Effet : Magie du sang guenaude|libre|
|[6fObd480rDBkFwZ3.htm](feat-effects/6fObd480rDBkFwZ3.htm)|Effect: Curse of Living Death|Effet : Malédiction de la mort vivante|libre|
|[6IsZQpwRJQWIzdGx.htm](feat-effects/6IsZQpwRJQWIzdGx.htm)|Stance: Masquerade of Seasons Stance|Posture : Posture de la mascarade des saisons|libre|
|[6VrKQ0PhRXuteusQ.htm](feat-effects/6VrKQ0PhRXuteusQ.htm)|Effect: Giant's Stature|Effet : Stature de géant|libre|
|[6YhbQmOmbmy84W1C.htm](feat-effects/6YhbQmOmbmy84W1C.htm)|Effect: Crimson Shroud|Effet : Voile pourpre|libre|
|[72THfaqak0F4XnON.htm](feat-effects/72THfaqak0F4XnON.htm)|Effect: Didactic Strike|Effet : Frappe didactique|libre|
|[7BFd8A9HFrmg6vwL.htm](feat-effects/7BFd8A9HFrmg6vwL.htm)|Effect: Psychopomp Blood Magic (Self)|Effet : Magie du sang psychopompe (soi-même)|libre|
|[7cG8kpQvh2oyBV8d.htm](feat-effects/7cG8kpQvh2oyBV8d.htm)|Effect: Stone Body|Effet : Corps de pierre|libre|
|[7hQnwwsixZmXzdIT.htm](feat-effects/7hQnwwsixZmXzdIT.htm)|Effect: Channel the Godmind|Effet : Canaliser l'esprit divin|libre|
|[7hRgBo0fRQBxMK7g.htm](feat-effects/7hRgBo0fRQBxMK7g.htm)|Effect: Distracting Feint|Effet : Feinte de diversion|libre|
|[7MQLLkQACZt8cspt.htm](feat-effects/7MQLLkQACZt8cspt.htm)|Effect: Purifying Breeze|Effet : Brise purifiante|libre|
|[7ogytOgDmh4h2g5d.htm](feat-effects/7ogytOgDmh4h2g5d.htm)|Effect: Heroic Recovery|Effet : Récupération héroïque|libre|
|[8E5SCmFndGAvgkTw.htm](feat-effects/8E5SCmFndGAvgkTw.htm)|Effect: Energize Wings|Effet : Énergiser les ailes|libre|
|[8rDbWcWmQL0N5FFG.htm](feat-effects/8rDbWcWmQL0N5FFG.htm)|Effect: Azarketi Purification|Effet : Purification azarketi|libre|
|[939OHjW9y8uCmDk3.htm](feat-effects/939OHjW9y8uCmDk3.htm)|Effect: Unleash Psyche|Effet : Déchaîner la psyché|libre|
|[94MzLpLykQIWKcA1.htm](feat-effects/94MzLpLykQIWKcA1.htm)|Effect: Deteriorated|Effet : Détérioré|libre|
|[9AUcoY48H5LrVZiF.htm](feat-effects/9AUcoY48H5LrVZiF.htm)|Effect: Genie Blood Magic (Self)|Effet : Magie du sang génie (soi-même)|libre|
|[9dCt0asv0kt7DR4q.htm](feat-effects/9dCt0asv0kt7DR4q.htm)|Effect: Liberating Step (vs. Fiend)|Effet : Pas libérateur (Fiélon)|libre|
|[9HPxAKpP3WJmICBx.htm](feat-effects/9HPxAKpP3WJmICBx.htm)|Stance: Point-Blank Shot|Posture : Tir à bout portant|libre|
|[9kNbiZPOM2wy60ao.htm](feat-effects/9kNbiZPOM2wy60ao.htm)|Effect: Ceremony of Protection|Effet : Cérémonie de protection|libre|
|[a7qiSYdlaIRPe57i.htm](feat-effects/a7qiSYdlaIRPe57i.htm)|Effect: Watchful Gaze|Effet : Regard vigilant|libre|
|[AAgoUuwMvHzqNhIN.htm](feat-effects/AAgoUuwMvHzqNhIN.htm)|Effect: Assisting Shot (Critical Hit)|Effet : Tir de soutien (coup critique)|libre|
|[AclYG5JuBFrjCY3I.htm](feat-effects/AclYG5JuBFrjCY3I.htm)|Effect: Divine Weapon (Evil)|Effet : Arme Divine (Mauvais)|libre|
|[aEuDaQY1GnrrnDRA.htm](feat-effects/aEuDaQY1GnrrnDRA.htm)|Effect: Aldori Parry|Effet : Parade aldorie|libre|
|[AJlunjfAIOq2Sg0p.htm](feat-effects/AJlunjfAIOq2Sg0p.htm)|Effect: Underground Endemic Herbs|Effet : Herbes endémiques Souterrains|libre|
|[AKKHagjg5bL1fMG5.htm](feat-effects/AKKHagjg5bL1fMG5.htm)|Effect: Overwatch Field|Effet : Champ d'observation|libre|
|[aKRo5TIhUtu0kyEr.htm](feat-effects/aKRo5TIhUtu0kyEr.htm)|Effect: Demonic Blood Magic (Self)|Effet : Magie du sang démoniaque (Soi-même)|libre|
|[AlnxieIRjqNqsdVu.htm](feat-effects/AlnxieIRjqNqsdVu.htm)|Effect: Smite Evil|Effet : Châtiment du mal|libre|
|[aqnx6IDcB7ARLxS5.htm](feat-effects/aqnx6IDcB7ARLxS5.htm)|Effect: Wyrmblessed Blood Magic (Status Penalty - Target)|Effet : Magie du sang béni du ver (pénalité de statut - Cible)|libre|
|[aUpcWqaLBlmpnJgW.htm](feat-effects/aUpcWqaLBlmpnJgW.htm)|Effect: Legendary Monster Warden|Effet : Garde-monsres légendaire|libre|
|[aWOvmdaTK1jS3H72.htm](feat-effects/aWOvmdaTK1jS3H72.htm)|Effect: Lost in the Crowd (10 Creatures)|Effet : Se perdre dans la foule (10 créatures)|libre|
|[b2kWJuCPj1rDMdwz.htm](feat-effects/b2kWJuCPj1rDMdwz.htm)|Stance: Wolf Stance|Posture : Posture du loup|libre|
|[BC92TyFzRCWq8fu0.htm](feat-effects/BC92TyFzRCWq8fu0.htm)|Effect: Great Tengu Form|Effet : Forme du grand tengu|libre|
|[BCyGDKcplkJiSAKJ.htm](feat-effects/BCyGDKcplkJiSAKJ.htm)|Stance: Stumbling Stance|Posture : Posture chancelante|libre|
|[BHnunYPROBG5lxv4.htm](feat-effects/BHnunYPROBG5lxv4.htm)|Effect: Heroes' Call|Effet : Appel des héros|libre|
|[bIRIS6mnynr72RDw.htm](feat-effects/bIRIS6mnynr72RDw.htm)|Goblin Song (Critical Success)|Effet : Chant gobelin - Succès critique)|libre|
|[bIU1q05vzkKBtFj2.htm](feat-effects/bIU1q05vzkKBtFj2.htm)|Effect: Necrotic Infusion|Effet : Injection nécrotique|libre|
|[bl4HXm1e4NQ0iJs5.htm](feat-effects/bl4HXm1e4NQ0iJs5.htm)|Effect: Align Armament (Good)|Effet : Arsenal aligné (Bon)|libre|
|[bliWctLi7jlKUTUe.htm](feat-effects/bliWctLi7jlKUTUe.htm)|Effect: Forest Endemic Herbs|Effet : Herbes endémiques Forêt|libre|
|[bmVwaN0C4e9fE2Sz.htm](feat-effects/bmVwaN0C4e9fE2Sz.htm)|Effect: Bolera's Interrogation (failure)|Effet : Interrogatoire de Boléra (échec)|libre|
|[bvk5rwYSoTtz8QGf.htm](feat-effects/bvk5rwYSoTtz8QGf.htm)|Effect: Accursed Clay Fist|Effet: Poing de glaise maudit|libre|
|[C6H3gF5HTdsIKpOC.htm](feat-effects/C6H3gF5HTdsIKpOC.htm)|Effect: Improved Poison Weapon|Effet: Arme empoisonnée améliorée|libre|
|[cA6ps8RKE0gysEWr.htm](feat-effects/cA6ps8RKE0gysEWr.htm)|Effect: Prayer-Touched Weapon|Effet : Arme touchée par la grâce|libre|
|[cGwFYusGTsJR3x3P.htm](feat-effects/cGwFYusGTsJR3x3P.htm)|Effect: Under Control|Effet : Sous contrôle|libre|
|[CgxYa0lrLUjS2ZhI.htm](feat-effects/CgxYa0lrLUjS2ZhI.htm)|Stance: Cobra Stance|Posture : Posture du cobra|libre|
|[cH8JD3ub4eEKuIAD.htm](feat-effects/cH8JD3ub4eEKuIAD.htm)|Effect: Radiant Infusion|Effet : Imprégnation irradiante|libre|
|[CNnIS8jWVj00nPwF.htm](feat-effects/CNnIS8jWVj00nPwF.htm)|Effect: Lost in the Crowd (100 Creatures)|Effet : Se perdre dans la foule (100 créatures)|libre|
|[COsdMolZraFRTdP8.htm](feat-effects/COsdMolZraFRTdP8.htm)|Effect: Prevailing Position|Effet : Position prédominante|libre|
|[CQfkyJkRHw4IHWhv.htm](feat-effects/CQfkyJkRHw4IHWhv.htm)|Stance: Sky and Heaven Stance|Posture : Posture du Ciel et du Paradis|libre|
|[cqgbTZCvqaSvtQdz.htm](feat-effects/cqgbTZCvqaSvtQdz.htm)|Effect: Encroaching Presence|Effet : Présence envahissante|libre|
|[ctiTtuRWFjAnWdYQ.htm](feat-effects/ctiTtuRWFjAnWdYQ.htm)|Effect: Corpse-Killer's Defiance (Lower Level)|Effet : Défi du tueur de cadavre (niveau inférieur)|libre|
|[CtrZFI3RV0yPNzTv.htm](feat-effects/CtrZFI3RV0yPNzTv.htm)|Effect: Bon Mot (Critical Success)|Effet : Bon Mot (Succès critique)|libre|
|[Cumdy84uIkUHG9zF.htm](feat-effects/Cumdy84uIkUHG9zF.htm)|Effect: Resounding Bravery (vs. Fear)|Effet : Bravoure retentissante (contre la peur)|libre|
|[CUtvkuGSxq1raBIB.htm](feat-effects/CUtvkuGSxq1raBIB.htm)|Effect: Shared Clarity|Effet : Clarté partagée|libre|
|[CW4zphOOpeaLJIWc.htm](feat-effects/CW4zphOOpeaLJIWc.htm)|Effect: Recall Under Pressure|Effet : Souvenir sous pression|libre|
|[DawVHfoPKbPJsz4k.htm](feat-effects/DawVHfoPKbPJsz4k.htm)|Effect: Champion's Resistance|Effet : Résistance du champion|libre|
|[Dbr5hInQXH904Ca7.htm](feat-effects/Dbr5hInQXH904Ca7.htm)|Effect: Psychic Rapport|Effet : Rapport psychique|libre|
|[DhvSMIFs6xifgQHX.htm](feat-effects/DhvSMIFs6xifgQHX.htm)|Effect: Current Spell (Water)|Effet : Courant de sort (Eau)|libre|
|[DjxZpQ4xJWWvYQqY.htm](feat-effects/DjxZpQ4xJWWvYQqY.htm)|Effect: Repair Module|Effet: Module de réparation|libre|
|[dTymNXgTtnjqgYP9.htm](feat-effects/dTymNXgTtnjqgYP9.htm)|Effect: Emotional Surge|Effet : Surcharge émotionnelle|libre|
|[dvOfGUuvG8ihcN8d.htm](feat-effects/dvOfGUuvG8ihcN8d.htm)|Effect: Divine Weapon (Good)|Effet : Arme Divine (Bon)|libre|
|[DvyyA11a63FBwV7x.htm](feat-effects/DvyyA11a63FBwV7x.htm)|Effect: Known Weakness|Effet : Faiblesses connues|libre|
|[DWrsDJte9sez0Ppi.htm](feat-effects/DWrsDJte9sez0Ppi.htm)|Effect: Rampaging Form|Effet : Forme destructrice|libre|
|[DyX4E7KDkzRnDxzc.htm](feat-effects/DyX4E7KDkzRnDxzc.htm)|Effect: Perfect Resistance|Effet : Résistance parfaite|libre|
|[e6mv68aarIbQ3tXL.htm](feat-effects/e6mv68aarIbQ3tXL.htm)|Effect: Undying Conviction|Effet : Conviction immortelle|libre|
|[E8MiV00QEhH5n18L.htm](feat-effects/E8MiV00QEhH5n18L.htm)|Effect: Bespell Weapon|Effet : Arme enchantée|libre|
|[eA14bUF7xhNCzw2v.htm](feat-effects/eA14bUF7xhNCzw2v.htm)|Effect: Align Armament (Evil)|Effet : Arsenal aligné (Mal)|libre|
|[ed9iJxdHuft6bDFF.htm](feat-effects/ed9iJxdHuft6bDFF.htm)|Effect: Deadly Poison Weapon|Effet : Arme empoisonnée mortelle|libre|
|[EDpjey6SCdvIYqEc.htm](feat-effects/EDpjey6SCdvIYqEc.htm)|Effect: Twin Parry (Parry Trait)|Effet : Parade jumelée (Trait parade)|libre|
|[eeAlh6edygcZIz9c.htm](feat-effects/eeAlh6edygcZIz9c.htm)|Stance: Wild Winds Stance|Posture : Posture des vents violents|libre|
|[EfMaI6AnROP4X9lN.htm](feat-effects/EfMaI6AnROP4X9lN.htm)|Effect: Mountain Stronghold|Effet : Bastion de la montagne|libre|
|[emSh1VxHVtTmt925.htm](feat-effects/emSh1VxHVtTmt925.htm)|Effect: Methodical Debilitations|Effet : Handicaps méthodiques|libre|
|[eMsI1lR0SuJBCYjn.htm](feat-effects/eMsI1lR0SuJBCYjn.htm)|Effect: Consume Energy (Augment Strike)|Effet : Consommer l'énergie (Augmenter la Frappe)|libre|
|[EQCnu8DGHDDNXch0.htm](feat-effects/EQCnu8DGHDDNXch0.htm)|Effect: Reanimator Dedication|Effet : Dévouement : Réanimateur|libre|
|[er5tvDNvpbcnlbHQ.htm](feat-effects/er5tvDNvpbcnlbHQ.htm)|Stance: Inspiring Marshal Stance|Posture : Posture du capitaine inspirant|libre|
|[ErLweSmVAN57QIpp.htm](feat-effects/ErLweSmVAN57QIpp.htm)|Effect: Nanite Surge|Effet : Poussée nanite|libre|
|[ESnzqtwSgahLcxg2.htm](feat-effects/ESnzqtwSgahLcxg2.htm)|Effect: Hamstringing Strike|Effet : Frappe aux ischio-jambiers|libre|
|[EtFMN1ZLkL7sUk01.htm](feat-effects/EtFMN1ZLkL7sUk01.htm)|Effect: Curse of Outpouring Life|Effet : Malédiction de la vie déversée|libre|
|[eu2HidLHaGKe4MPn.htm](feat-effects/eu2HidLHaGKe4MPn.htm)|Effect: Twin Parry|Effet : Parade jumelée}|libre|
|[EVRcdGt4awWPgXla.htm](feat-effects/EVRcdGt4awWPgXla.htm)|Effect: Arcane Propulsion|Effet : Propulsion arcanique|libre|
|[EzgW32MCOGov9h5C.htm](feat-effects/EzgW32MCOGov9h5C.htm)|Effect: Striking Retribution|Effet : Rétribution frappante|libre|
|[fh8TgCfiifVk0eqU.htm](feat-effects/fh8TgCfiifVk0eqU.htm)|Effect: Magical Mentor Boon (PFS)|Effet : Récompense de mentor magique (PFS)|libre|
|[FIgud5jqZgIjwkRE.htm](feat-effects/FIgud5jqZgIjwkRE.htm)|Effect: Maiden's Mending|Effet : Guérison de la vierge}|libre|
|[fILVhS5NuCtGXfri.htm](feat-effects/fILVhS5NuCtGXfri.htm)|Effect: Wyrmblessed Blood Magic (Status Bonus - Self)|Effet : Magie du sang Béni du ver (bonus de statut - soi-même)|libre|
|[FNTTeJHiK6iOjrSq.htm](feat-effects/FNTTeJHiK6iOjrSq.htm)|Effect: Draconic Blood Magic|Effet : Magie du sang draconique|libre|
|[FPuICuxBLiDaEbDX.htm](feat-effects/FPuICuxBLiDaEbDX.htm)|Effect: Aura of Life|Effet : Aura de Vie|libre|
|[fsjO5oTKttsbpaKl.htm](feat-effects/fsjO5oTKttsbpaKl.htm)|Stance: Arcane Cascade|Posture : Cascade arcanique|libre|
|[FyaekbWsazkJhJda.htm](feat-effects/FyaekbWsazkJhJda.htm)|Effect: Decry Thief (Success)|Effet : Fléau des voleurs (Succès)|libre|
|[G1IRkppxJCYdfqXo.htm](feat-effects/G1IRkppxJCYdfqXo.htm)|Effect: Bespell Weapon (Negative)|Effet : Arme enchantée (Négatif)|libre|
|[G4L49aMxHqO2yqxi.htm](feat-effects/G4L49aMxHqO2yqxi.htm)|Effect: Curse of Creeping Ashes|Effet : Malédiction des cendres rampantes|libre|
|[GbbwJhwSNLw06XpO.htm](feat-effects/GbbwJhwSNLw06XpO.htm)|Effect: Bespell Weapon (Force)|Effet : Arme enchantée (Force)|libre|
|[GCEOngH5zL0rRyle.htm](feat-effects/GCEOngH5zL0rRyle.htm)|Effect: Emblazon Energy (Weapon, Fire)|Effet : Énergie blasonnée (Arme, Feu)|libre|
|[GGebXpRPyONZB3eS.htm](feat-effects/GGebXpRPyONZB3eS.htm)|Stance: Everstand Stance|Posture : Posture toujours en position|libre|
|[ghZFZWUh5Z20vOlR.htm](feat-effects/ghZFZWUh5Z20vOlR.htm)|Effect: Fortify Shield|Effet : Bouclier fortifié|libre|
|[GlpZyxAGhy5QNqkm.htm](feat-effects/GlpZyxAGhy5QNqkm.htm)|Effect: Divine Weapon (Lawful)|Effet : Arme Divine (Loyal)|libre|
|[gN1LbKYQgi8Fx98V.htm](feat-effects/gN1LbKYQgi8Fx98V.htm)|Effect: Anadi Venom|Effet : Venin anadi|libre|
|[GoSls6SKCFmSoDxT.htm](feat-effects/GoSls6SKCFmSoDxT.htm)|Effect: Bon Mot|Effet : Bon Mot (Succès)|libre|
|[GvqB4M8LrHpzYEvl.htm](feat-effects/GvqB4M8LrHpzYEvl.htm)|Stance: Fane's Fourberie|Posture : Fourberie de Fane|libre|
|[gWwG7MNAesJgpmRW.htm](feat-effects/gWwG7MNAesJgpmRW.htm)|Effect: Cut from the Air|Effet : Découper en l'air|libre|
|[gYpy9XBPScIlY93p.htm](feat-effects/gYpy9XBPScIlY93p.htm)|Stance: Mountain Stance|Posture : Posture de la montagne|libre|
|[h45sUZFs5jhuQdCE.htm](feat-effects/h45sUZFs5jhuQdCE.htm)|Stance: Vitality-Manipulation Stance|Posture : Posture de manipulation de la vitalité|libre|
|[HfXGhXc9D120gvl5.htm](feat-effects/HfXGhXc9D120gvl5.htm)|Effect: Celestial Wings|Effet : Ailes célestes|libre|
|[HjCXHDZT6GkCyiuG.htm](feat-effects/HjCXHDZT6GkCyiuG.htm)|Effect: Plains Endemic Herbs|Effet : Herbes endémiques Plaine|libre|
|[HKPmrxkZwHRND5Um.htm](feat-effects/HKPmrxkZwHRND5Um.htm)|Favored Terrain (Increase Swim Speed)|Effet : Environnement de prédilection (Augmenter la vitesse de nage)|libre|
|[hqeR9faxHj0NDFFP.htm](feat-effects/hqeR9faxHj0NDFFP.htm)|Effect: Curse of Engulfing Flames|Effet : Malédiction du linceul de flammes|libre|
|[I0g5oaSwaqZ8fFAV.htm](feat-effects/I0g5oaSwaqZ8fFAV.htm)|Effect: Curse of the Perpetual Storm|Effet : Malédiction de la tempête perpétuelle|libre|
|[I4Ozf6mTnd3X0Oax.htm](feat-effects/I4Ozf6mTnd3X0Oax.htm)|Effect: Predictable! (Critical Success)|Effet : Présivisible ! (Succès critique)|libre|
|[iaZ6P59YVhdnFIN8.htm](feat-effects/iaZ6P59YVhdnFIN8.htm)|Effect: Guided Skill|Effet : Compétence guidée|libre|
|[IfRkgjyh0JzGalIy.htm](feat-effects/IfRkgjyh0JzGalIy.htm)|Effect: Armor Tampered With (Success)|Effet : Armure trafiquée avec succès|libre|
|[IfsglZ7fdegwem0E.htm](feat-effects/IfsglZ7fdegwem0E.htm)|Effect: Hydraulic Deflection|Effet : Manoeuvres hydrauliques|libre|
|[Im5JBInybWFbHEYS.htm](feat-effects/Im5JBInybWFbHEYS.htm)|Stance: Rain of Embers Stance|Posture : Posture de la pluie de charbons ardents|libre|
|[IpRfT9lL3YR6MH6w.htm](feat-effects/IpRfT9lL3YR6MH6w.htm)|Favored Terrain (Increase Climb Speed)|Effet : Environnement de prédilection (Augmenter la vitesse d'escalade)|libre|
|[iqvurepX0zyu9OlI.htm](feat-effects/iqvurepX0zyu9OlI.htm)|Effect: Masterful Hunter's Edge, Outwit|Effet : Spécialité Maître chasseur, Ruse|libre|
|[ITvyvbB234bxceRK.htm](feat-effects/ITvyvbB234bxceRK.htm)|Effect: Mutate Weapon|Effet : Arme mutante|libre|
|[ivGiUp0EC5nWT9Hb.htm](feat-effects/ivGiUp0EC5nWT9Hb.htm)|Effect: Read Shibboleths|Effet : Lire les signes distinctifs|libre|
|[iyONT1qgeRgoYHsZ.htm](feat-effects/iyONT1qgeRgoYHsZ.htm)|Effect: Liberating Step (vs. Dragon)|Effet : Pas libérateur (Dragon)|libre|
|[jACKRmVfr9ATsmwg.htm](feat-effects/jACKRmVfr9ATsmwg.htm)|Effect: Devrin's Cunning Stance|Effet : Posture astucieuse de Devrin|libre|
|[JefXqvhzUeBArkAP.htm](feat-effects/JefXqvhzUeBArkAP.htm)|Stance: Whirling Blade Stance|Posture : Posture de la lame tournoyante|libre|
|[JF2xCqL6t4UJZtUi.htm](feat-effects/JF2xCqL6t4UJZtUi.htm)|Effect: Blizzard Evasion|Effet : Évasion du blizzard|libre|
|[jlZjUtrfcfIWumSe.htm](feat-effects/jlZjUtrfcfIWumSe.htm)|Effect: Renewed Vigor|Effet : Regain de vigueur|libre|
|[JQUoBlZKT5N5zO5k.htm](feat-effects/JQUoBlZKT5N5zO5k.htm)|Effect: Avenge in Glory|Effet : Revanche dans la gloire|libre|
|[JUgx48XHMz4QM4Ir.htm](feat-effects/JUgx48XHMz4QM4Ir.htm)|Effect: Tactical Debilitations (No Flanking)|Effet : Handicaps tactiques (Pas de tenaille)|libre|
|[JwDCoBIwyhOFnDGZ.htm](feat-effects/JwDCoBIwyhOFnDGZ.htm)|Effect: Augment Senses|Effet : Sens augmentés|libre|
|[jwxurN6JPQm9wXug.htm](feat-effects/jwxurN6JPQm9wXug.htm)|Effect: Defensive Recovery|Effet : Récupération défensive|libre|
|[JysvElDwGZ5ABQ6x.htm](feat-effects/JysvElDwGZ5ABQ6x.htm)|Effect: Emotional Fervor|Effet : Ferveur émotionnelle|libre|
|[jZYRxGHyArCci6AF.htm](feat-effects/jZYRxGHyArCci6AF.htm)|Effect: Desert Endemic Herbs|Effet : Herbes endémiques Désert|libre|
|[K0Sv9AHgq245hSLC.htm](feat-effects/K0Sv9AHgq245hSLC.htm)|Effect: Inspired Stratagem|Effet : Stratagème inspiré|libre|
|[K1IgNCf3Hh2EJwQ9.htm](feat-effects/K1IgNCf3Hh2EJwQ9.htm)|Effect: Divine Aegis|Effet : Égide divine|libre|
|[k1J2SaHPwZb2Y6Bp.htm](feat-effects/k1J2SaHPwZb2Y6Bp.htm)|Effect: Wings of Air|Effet : Ailes d'air|libre|
|[k8gB0eDuAlGRoeQj.htm](feat-effects/k8gB0eDuAlGRoeQj.htm)|Effect: Benevolent Spirit Deck|Effet : Jeu spirituel bienveillant|libre|
|[kAgUld9PcI4XkHbq.htm](feat-effects/kAgUld9PcI4XkHbq.htm)|Effect: Decry Thief (Critical Success)|Effet : Fléau des voleurs (Succès critique)|libre|
|[KBEJVRrie2JTHWIK.htm](feat-effects/KBEJVRrie2JTHWIK.htm)|Effect: Dread Marshal Stance|Posture : Posture du terrible Capitaine|libre|
|[KceTcamIZ4ZrQJLD.htm](feat-effects/KceTcamIZ4ZrQJLD.htm)|Effect: Educate Allies (Self)|Effet : Alliés instruits (Vous-même)|libre|
|[kDTiRg9vVOYNnTyr.htm](feat-effects/kDTiRg9vVOYNnTyr.htm)|Stance: Powder Punch Stance|Posture : Posture du coup de poing à poudre|libre|
|[KgR1myc4OLzVxfxn.htm](feat-effects/KgR1myc4OLzVxfxn.htm)|Effect: Predictable! (Critical Failure)|Effet : Présivisible ! (Échec critique)|libre|
|[KiuBRoMFxL2Npt51.htm](feat-effects/KiuBRoMFxL2Npt51.htm)|Stance: Dueling Dance|Posture : Danse en duel|libre|
|[KkbFlNfcQQUfSVXd.htm](feat-effects/KkbFlNfcQQUfSVXd.htm)|Effect: Align Armament (Lawful)|Effet : Arsenal aligné (Loyal)|libre|
|[kui8yKIVsxfJnrYe.htm](feat-effects/kui8yKIVsxfJnrYe.htm)|Effect: Walking the Cardinal Paths|Effet : Arpenter les chemins cardinaux|libre|
|[KVbS7AbhQdeuA0J6.htm](feat-effects/KVbS7AbhQdeuA0J6.htm)|Effect: Genie Blood Magic (Target)|Effet : Magie du sang génie (Cible)|libre|
|[kyrvZfZfzKK1vx9b.htm](feat-effects/kyrvZfZfzKK1vx9b.htm)|Stance: Devrin's Cunning Stance|Posture : Posture astucieuse de Devrin|libre|
|[kzEPq4aczYb6OD2h.htm](feat-effects/kzEPq4aczYb6OD2h.htm)|Effect: Inspiring Marshal Stance|Effet : Posture du Capitaine inspirant|libre|
|[kzSjzK72CQ67wfBH.htm](feat-effects/kzSjzK72CQ67wfBH.htm)|Effect: Protective Spirit Mask|Effet : Masque de l'esprit protecteur|libre|
|[L0hDj8vFk1IWh01L.htm](feat-effects/L0hDj8vFk1IWh01L.htm)|Effect: Aura of Righteousness|Effet : Aura de vertu|libre|
|[l3S9i2UWGhSO58YX.htm](feat-effects/l3S9i2UWGhSO58YX.htm)|Effect: Cat Nap|Effet : Sieste féline|libre|
|[l4QUaedYofnfXig0.htm](feat-effects/l4QUaedYofnfXig0.htm)|Stance: Multishot Stance|Posture : Posture de tirs multiples|libre|
|[L9g3EMCT3imX650b.htm](feat-effects/L9g3EMCT3imX650b.htm)|Effect: Heaven's Thunder|Effet : Tonnerre du Paradis|libre|
|[LB0PTV5yqMlBmRFj.htm](feat-effects/LB0PTV5yqMlBmRFj.htm)|Effect: Legendary Monster Hunter|Effet : Chasseur de monstres légendaire|libre|
|[Lb4q2bBAgxamtix5.htm](feat-effects/Lb4q2bBAgxamtix5.htm)|Effect: Treat Wounds Immunity|Effet : Immunité à Soigner les blessures|libre|
|[lbe8XDSZB8gwyg90.htm](feat-effects/lbe8XDSZB8gwyg90.htm)|Effect: Protective Mentor Boon (Admired) (PFS)|Effet: Récompense du mentor protecteur (admiré) (PFS)|libre|
|[LF8xzzFsFJKxejqv.htm](feat-effects/LF8xzzFsFJKxejqv.htm)|Effect: Enforce Oath|Effet : Serment renforcé|libre|
|[Ljrx4N5XACKSk1Ks.htm](feat-effects/Ljrx4N5XACKSk1Ks.htm)|Effect: Core Cannon|Effet : Noyau canon|libre|
|[Lt5iSfx8fxHSdYXz.htm](feat-effects/Lt5iSfx8fxHSdYXz.htm)|Effect: Masterful Hunter's Edge, Precision|Effet : Spécialité Maître chasseur, Précision|libre|
|[ltIvO9ZQlmqGD89Y.htm](feat-effects/ltIvO9ZQlmqGD89Y.htm)|Effect: Hunter's Edge, Outwit|Effet : Spécialité du chasseur, Ruse|libre|
|[LVPodfYEWKtK3fUW.htm](feat-effects/LVPodfYEWKtK3fUW.htm)|Effect: Formation Training|Effet : Combat en formation|libre|
|[LxSev4GNKv26DbZw.htm](feat-effects/LxSev4GNKv26DbZw.htm)|Stance: Disarming Stance|Posture : Posture désarmante|libre|
|[lZPbv3nBRWmfbs3z.htm](feat-effects/lZPbv3nBRWmfbs3z.htm)|Effect: Strained Metabolism|Effet : Métabolisme sous tension|libre|
|[m5xWMaDfV0PiTE6u.htm](feat-effects/m5xWMaDfV0PiTE6u.htm)|Effect: Ursine Avenger Form|Effet : Forme de vengeur ursin|libre|
|[mark4VEQoynfYNBF.htm](feat-effects/mark4VEQoynfYNBF.htm)|Stance: Graceful Poise|Posture : Aisance gracieuse|libre|
|[mkIamZGtQaSsUWLk.htm](feat-effects/mkIamZGtQaSsUWLk.htm)|Effect: Control Tower|Effet : Tour de contrôle|libre|
|[mNk0KxsZMFnDjUA0.htm](feat-effects/mNk0KxsZMFnDjUA0.htm)|Effect: Hunter's Edge, Precision|Effet : Spécialité du chasseur, Précision|libre|
|[MNkIxAishE22TqL3.htm](feat-effects/MNkIxAishE22TqL3.htm)|Effect: Aura of Despair|Effet : Aura de Désespoir|libre|
|[MrdT7LiOZMN8J4GK.htm](feat-effects/MrdT7LiOZMN8J4GK.htm)|Effect: Fiendish Wings|Effet : Ailes fiélones|libre|
|[MRo1SI1Y5PgdNf8r.htm](feat-effects/MRo1SI1Y5PgdNf8r.htm)|Effect: Deep Freeze (Critical Failure)|Effet : Congélation profonde(Échec critique)|libre|
|[Ms6WPXRWfXb2KpG2.htm](feat-effects/Ms6WPXRWfXb2KpG2.htm)|Stance: Tenacious Stance|Posture : Posture tenace|libre|
|[MSkspeBsbXm6LQ19.htm](feat-effects/MSkspeBsbXm6LQ19.htm)|Effect: Harrow the Fiend|Effet : tourmenter le fiélon|libre|
|[MZDh3170EFIfOwTO.htm](feat-effects/MZDh3170EFIfOwTO.htm)|Effect: Overdrive (Success)|Effet : Surrégime (Succès)|libre|
|[n1vhmOd7aNiuR3nk.htm](feat-effects/n1vhmOd7aNiuR3nk.htm)|Effect: Diabolic Blood Magic (Self)|Effet : Magie du sang diabolique (soi-même)|libre|
|[N2CSGvtPXloOEPrK.htm](feat-effects/N2CSGvtPXloOEPrK.htm)|Effect: Giant's Lunge|Effet : Fente de géant|libre|
|[ngwcN8u7f7CnqGXp.htm](feat-effects/ngwcN8u7f7CnqGXp.htm)|Effect: Distant Wandering|Effet : Errance à distance|libre|
|[nlaxROgSSLVHZ1hx.htm](feat-effects/nlaxROgSSLVHZ1hx.htm)|Effect: Monster Warden|Effet : Garde-monstre|libre|
|[nlMZCi8xi9YSvlYR.htm](feat-effects/nlMZCi8xi9YSvlYR.htm)|Effect: Engine of Destruction|Effet : Machine de destruction|libre|
|[NMmsJyeMTawpgLVR.htm](feat-effects/NMmsJyeMTawpgLVR.htm)|Effect: Resounding Bravery|Effet : Bravoure retentissante|libre|
|[nnF7RSVlC6swbSw8.htm](feat-effects/nnF7RSVlC6swbSw8.htm)|Effect: Anoint Ally|Effet : Oindre un allié|libre|
|[Np3OSqKxuB9rTbij.htm](feat-effects/Np3OSqKxuB9rTbij.htm)|Effect: Harden Flesh|Effet : Peau durcie|libre|
|[Nv70aqcQgCBpDYp8.htm](feat-effects/Nv70aqcQgCBpDYp8.htm)|Effect: Shadow Blood Magic (Perception)|Effet : Magie du sang ombre (Perception)|libre|
|[NviQYIVZbPCSWLqT.htm](feat-effects/NviQYIVZbPCSWLqT.htm)|Effect: Aquatic Endemic Herbs|Effet : Herbes endémiques Aquatique|libre|
|[nwkYZs6YwXYAJ4ps.htm](feat-effects/nwkYZs6YwXYAJ4ps.htm)|Stance: Crane Stance|Posture : Posture de la grue|libre|
|[NWOmJ6WJFheaGhho.htm](feat-effects/NWOmJ6WJFheaGhho.htm)|Stance: Mobile Shot Stance|Posture : Posture de tir mobile|libre|
|[o7qm13OmaYOMwgib.htm](feat-effects/o7qm13OmaYOMwgib.htm)|Effect: Weapon Tampered With (Critical Success)|Effet : Arme trafiquée avec succès critique|libre|
|[O8qithYQCv3e7DUQ.htm](feat-effects/O8qithYQCv3e7DUQ.htm)|Effect: Elementalist Dedication|Effet : Dévouement : Élémentaliste|libre|
|[OeZ0E1oUKyGPxPy0.htm](feat-effects/OeZ0E1oUKyGPxPy0.htm)|Effect: Push Back the Dead!|Effet : Repousser les morts-vivants|libre|
|[OhLcaJeQy4Nf5Mwo.htm](feat-effects/OhLcaJeQy4Nf5Mwo.htm)|Favored Terrain (Gain Swim Speed)|Effet : Environnement de prédilection (Obtenir une vitesse de nage)|libre|
|[OK7zMlYy25JciBp6.htm](feat-effects/OK7zMlYy25JciBp6.htm)|Effect: Shed Tail|Effet : Autotomie caudale|libre|
|[OkcblqWj4aHVAkrp.htm](feat-effects/OkcblqWj4aHVAkrp.htm)|Effect: Divine Weapon (Chaotic)|Effet : Arme Divine (Chaotique)|libre|
|[oKJr59FYdDORxLcR.htm](feat-effects/oKJr59FYdDORxLcR.htm)|Effect: Worldly Mentor Boon (PFS)|Effet : Récompense de mentor expérimenté (PFS)|libre|
|[OKOqC1wswrh9jXqP.htm](feat-effects/OKOqC1wswrh9jXqP.htm)|Effect: Protective Mentor Boon (Liked) (PFS)|Effet : Récompense de mentor protecteur (Aimé)|libre|
|[OqH6IaeOwRWkGPrk.htm](feat-effects/OqH6IaeOwRWkGPrk.htm)|Effect: Shadow Blood Magic (Stealth)|Effet : Magie du sang ombre (Discrétion)|libre|
|[oSzUv21eN9VS9TC1.htm](feat-effects/oSzUv21eN9VS9TC1.htm)|Effect: Curse of Turbulent Moments|Effet : Malédiction des moments de turbulence|libre|
|[oX51Db6IxnUI64dT.htm](feat-effects/oX51Db6IxnUI64dT.htm)|Effect: Emblazon Energy (Weapon, Electricity)|Effet : Énergie blasonnée (Arme, Électricité)|libre|
|[oXG7eX26FmePmwUF.htm](feat-effects/oXG7eX26FmePmwUF.htm)|Effect: Discordant Voice|Effet : Voix discordante|libre|
|[p0S3VHkRgMye7RSI.htm](feat-effects/p0S3VHkRgMye7RSI.htm)|Effect: Gathering Moss|Effet : Recouvert de mousse|libre|
|[P6druSuWIVoLrXJR.htm](feat-effects/P6druSuWIVoLrXJR.htm)|Effect: Calculate Threats|Effet : Calculer les menaces|libre|
|[P80mwvCAEncR2snK.htm](feat-effects/P80mwvCAEncR2snK.htm)|Stance: Six Pillars Stance|Posture : Posture des six piliers|libre|
|[pf9yvKNg6jZLrE30.htm](feat-effects/pf9yvKNg6jZLrE30.htm)|Stance: Tiger Stance|Posture : Posture du tigre|libre|
|[pkcr9w5x6bKzl3om.htm](feat-effects/pkcr9w5x6bKzl3om.htm)|Stance: Jellyfish Stance|Posture : Posture de la méduse|libre|
|[PMHwCrnh9W4sMu5b.htm](feat-effects/PMHwCrnh9W4sMu5b.htm)|Stance: Tangled Forest Stance|Posture : Posture de la forêt enchevêtrée|libre|
|[pQ3EjUm1lZW9t3el.htm](feat-effects/pQ3EjUm1lZW9t3el.htm)|Effect: Curse of the Hero's Burden|Effet : Malédiction du fardeau du héros|libre|
|[pQ9e5njvIOe5QzFa.htm](feat-effects/pQ9e5njvIOe5QzFa.htm)|Effect: Fleet Tempo|Effet : Tempo rapide|libre|
|[PS17dsXkTdQmOv7w.htm](feat-effects/PS17dsXkTdQmOv7w.htm)|Stance: Buckler Dance|Posture : Danse de la targe|libre|
|[pTYTanMHMwSgJ8TN.htm](feat-effects/pTYTanMHMwSgJ8TN.htm)|Effect: Defensive Instincts|Effet : Instinct défensif|libre|
|[pwbFFD6NzDooobKo.htm](feat-effects/pwbFFD6NzDooobKo.htm)|Effect: Reflexive Shield|Effet : Bouclier instinctif|libre|
|[PX6WdrpzEdUzKRHx.htm](feat-effects/PX6WdrpzEdUzKRHx.htm)|Effect: Enduring Debilitating Strike|Effet : Frappe incapacitante persistante|libre|
|[Q0DKJRnDuuUnLpvn.htm](feat-effects/Q0DKJRnDuuUnLpvn.htm)|Effect: Tail Toxin|Effet : Queue à toxine|libre|
|[q2kY0TzXloJ8HLNO.htm](feat-effects/q2kY0TzXloJ8HLNO.htm)|Effect: Combat Mentor Boon (PFS)|Effet : Récompense de mentor de combat (PFS)|libre|
|[Q5FUu7yhWPJlcXei.htm](feat-effects/Q5FUu7yhWPJlcXei.htm)|Effect: Hydration|Effet : Hydratation|libre|
|[q6UokHWSEcEYWmvh.htm](feat-effects/q6UokHWSEcEYWmvh.htm)|Stance: Whirlwind Stance|Posture : Posture tourbillonnante|libre|
|[qBR3kqGCeKp3T2Be.htm](feat-effects/qBR3kqGCeKp3T2Be.htm)|Stance: Disruptive Stance|Posture : Posture perturbatrice|libre|
|[QcReJp7kgURdQCGz.htm](feat-effects/QcReJp7kgURdQCGz.htm)|Effect: Disruptive Stare|Effet : Regard perturbateur|libre|
|[QDQwHxNowRwzUx9R.htm](feat-effects/QDQwHxNowRwzUx9R.htm)|Stance: Reflective Ripple Stance|Effet : Posture de l'onde réfléchissante|libre|
|[qIOEe4kUN7FOBifb.htm](feat-effects/qIOEe4kUN7FOBifb.htm)|Effect: Hybrid Shape (Beastkin)|Effet : Forme hybride (animanthrope)|libre|
|[qM4bQfcwZ0EOS2M9.htm](feat-effects/qM4bQfcwZ0EOS2M9.htm)|Effect: Inspiring Resilience|Effet : Résistance inspirante|libre|
|[qSKVcw6brzrvfhUM.htm](feat-effects/qSKVcw6brzrvfhUM.htm)|Effect: Supercharge Prosthetic Eyes|Effet : Surcharge des yeux prothétiques|libre|
|[QTG73gxKSNkiEWdY.htm](feat-effects/QTG73gxKSNkiEWdY.htm)|Effect: Mountain Endemic Herbs|Effet : Herbes endémiques Montagnes|libre|
|[qUowHpn79Dpt1hVn.htm](feat-effects/qUowHpn79Dpt1hVn.htm)|Stance: Dragon Stance|Posture : Posture du dragon|libre|
|[qX62wJzDYtNxDbFv.htm](feat-effects/qX62wJzDYtNxDbFv.htm)|Stance: Dread Marshal Stance|Posture : Posture du terrible capitaine|libre|
|[r4kb2zDepFeczMsl.htm](feat-effects/r4kb2zDepFeczMsl.htm)|Effect: Bone Swarm|Effet : Nuée d'os|libre|
|[R6mx6EfLxSrQlrRa.htm](feat-effects/R6mx6EfLxSrQlrRa.htm)|Effect: Anchored|Effet : Ancré|libre|
|[raLQ458uiyd3lI2K.htm](feat-effects/raLQ458uiyd3lI2K.htm)|Effect: Guided by the Stars|Effet : Guidé par les étoiles|libre|
|[raoz523QRsj5WjcF.htm](feat-effects/raoz523QRsj5WjcF.htm)|Effect: Harsh Judgement|Effet : Jugement sévère|libre|
|[RATDyLyxXN3qmOas.htm](feat-effects/RATDyLyxXN3qmOas.htm)|Effect: Daydream Trance|Effet : Transe du rêve éveillé|libre|
|[RcxDIOa68SUGyMun.htm](feat-effects/RcxDIOa68SUGyMun.htm)|Effect: Titan's Stature|Effet : Stature de titan|libre|
|[Rgt9hH3W1oh9dvku.htm](feat-effects/Rgt9hH3W1oh9dvku.htm)|Effect: Vicious Debilitations|Effet : Handicaps cruels|libre|
|[rJpkKaPRGaH0pLse.htm](feat-effects/rJpkKaPRGaH0pLse.htm)|Effect: Fey Blood Magic|Effet : Magie du sang féerique|libre|
|[RoGEt7lrCdfaueB9.htm](feat-effects/RoGEt7lrCdfaueB9.htm)|Effect: Share Rage|Effet : Rage partagée|libre|
|[RozqjLocahvQWERr.htm](feat-effects/RozqjLocahvQWERr.htm)|Stance: Gorilla Stance|Posture : Posture du gorille|libre|
|[rp1YauUSULuqW8rs.htm](feat-effects/rp1YauUSULuqW8rs.htm)|Stance: Stoked Flame Stance|Posture : Posture de la flamme ravivée|libre|
|[rp6hA52dWVwtuu5F.htm](feat-effects/rp6hA52dWVwtuu5F.htm)|Effect: Harrow Omen|Effet : Présage du Tourment|libre|
|[Ru4BNABCZ0hUbX7S.htm](feat-effects/Ru4BNABCZ0hUbX7S.htm)|Effect: Marshal's Aura|Effet : Aura de capitaine|libre|
|[RU6D7pNQSBt1zSuK.htm](feat-effects/RU6D7pNQSBt1zSuK.htm)|Effect: Propulsive Leap|Effet : Bond propulsif|libre|
|[rUKtp4q8y73AvCbo.htm](feat-effects/rUKtp4q8y73AvCbo.htm)|Effect: Clue In (Detective's Readiness)|Effet : Partager les indices (Vivacité du détective)|libre|
|[ruRAfGJnik7lRavk.htm](feat-effects/ruRAfGJnik7lRavk.htm)|Effect: Nymph Blood Magic (Target)|Effet : Magie du sang nymphe (Cible)|libre|
|[rvyeOU7TQTLnKj03.htm](feat-effects/rvyeOU7TQTLnKj03.htm)|Effect: Reckless Abandon (Goblin)|Effet : Dangereux abandon (Gobelin)|libre|
|[rwDsr5XsrYcH7oFT.htm](feat-effects/rwDsr5XsrYcH7oFT.htm)|Effect: Curse of the Sky's Call|Effet : Malédiction de l'appel des cieux|libre|
|[RXbfq6oqzVnW6xOV.htm](feat-effects/RXbfq6oqzVnW6xOV.htm)|Stance: Shooting Stars Stance|Posture : Posture des étoiles lancées|libre|
|[RxDDXK52lwyHXl7v.htm](feat-effects/RxDDXK52lwyHXl7v.htm)|Effect: Scout's Warning|Effet : Avertissement de l'éclaireur|libre|
|[RyGaB5hDRcOeb34Q.htm](feat-effects/RyGaB5hDRcOeb34Q.htm)|Effect: Emblazon Antimagic (Weapon)|Effet : Antimagie blasonnée (Arme)|libre|
|[rzcpTJU9MvW1x1gz.htm](feat-effects/rzcpTJU9MvW1x1gz.htm)|Effect: Armor Tampered With (Critical Success)|Effet : Armure trafiquée avec succès critique|libre|
|[s1tulrmW6teTFjVd.htm](feat-effects/s1tulrmW6teTFjVd.htm)|Effect: Angelic Blood Magic|Effet : Magie du sang angélique|libre|
|[s3Te8waFP3KEb2dN.htm](feat-effects/s3Te8waFP3KEb2dN.htm)|Effect: Shield Ally|Effet : Bouclier allié|libre|
|[sCxi8lOH8tWQjLh0.htm](feat-effects/sCxi8lOH8tWQjLh0.htm)|Effect: Blade Ally|Effet : Allié lame|libre|
|[sDftJWPPSUeSZD3A.htm](feat-effects/sDftJWPPSUeSZD3A.htm)|Favored Terrain (Gain Climb Speed)|Effet : Environnement de prédilection (Obtenir une vitesse d'escalade)|libre|
|[sfUsodcGb4atcSyN.htm](feat-effects/sfUsodcGb4atcSyN.htm)|Effect: Reckless Abandon (Barbarian)|Effet : Dangereux abandon (Barbare)|libre|
|[SiegLMJpVOGuoyWJ.htm](feat-effects/SiegLMJpVOGuoyWJ.htm)|Effect: Ghost Wrangler|Effet : Dresseur de fantôme|libre|
|[SKjVvQcRQmnDoouw.htm](feat-effects/SKjVvQcRQmnDoouw.htm)|Effect: Skillful Mentor Boon (PFS)|Effet : Récompense de mentor talentueux (PFS)|libre|
|[SVGW8CLKwixFlnTv.htm](feat-effects/SVGW8CLKwixFlnTv.htm)|Effect: Nymph Blood Magic (Self)|Effet : Magie du sang nymphe (Soi-même)|libre|
|[svVczVV174KfJRDf.htm](feat-effects/svVczVV174KfJRDf.htm)|Effect: Shared Avoidance|Effet : Évitement partagé|libre|
|[SXYcrnGzWCuj8zq7.htm](feat-effects/SXYcrnGzWCuj8zq7.htm)|Effect: Poison Weapon|Effet : Arme empoisonnée|libre|
|[T5rsLTqS274B9Mly.htm](feat-effects/T5rsLTqS274B9Mly.htm)|Effect: Current Spell (Air)|Effet : Courant de sort (air)|libre|
|[T7AJQbfmlA57y625.htm](feat-effects/T7AJQbfmlA57y625.htm)|Effect: Vivacious Bravado|Effet: Bravade vivifiante|libre|
|[tAsFXMzNkpj964X4.htm](feat-effects/tAsFXMzNkpj964X4.htm)|Effect: Liberating Step (vs. Aberration)|Effet : Pas libérateur (Aberration)|libre|
|[Tju9kpQlwcKkyKor.htm](feat-effects/Tju9kpQlwcKkyKor.htm)|Effect: Curse of Torrential Knowledge|Effet : Malédiction  de l'afflux de connaissances|libre|
|[tl94WHJ2Hg0akK2o.htm](feat-effects/tl94WHJ2Hg0akK2o.htm)|Effect: Invigorating Breath|Effet : Souffle revigorant|libre|
|[tlft5vzk66iWCVRq.htm](feat-effects/tlft5vzk66iWCVRq.htm)|Effect: Safeguard Soul|Effet : Âme à l'abri|libre|
|[tPKXLtDJ3bzJcXlv.htm](feat-effects/tPKXLtDJ3bzJcXlv.htm)|Stance: Ironblood Stance|Posture : Posture du sang-de-fer|libre|
|[tx0S0fnfZ6Q2o80H.htm](feat-effects/tx0S0fnfZ6Q2o80H.htm)|Effect: High-Speed Regeneration Speed Boost|Effet : Régénération à haute vitesse augmentation de vitesse|libre|
|[U1MpMtRnFqEDBJwd.htm](feat-effects/U1MpMtRnFqEDBJwd.htm)|Effect: Emblazon Armament (Weapon)|Effet : Arsenal blasonné (Arme)|libre|
|[U2Pgm6B4nmdQ2Gpd.htm](feat-effects/U2Pgm6B4nmdQ2Gpd.htm)|Effect: Holy Castigation|Effet : Punition sacrée|libre|
|[uA1Ofqoyi0UiZIPk.htm](feat-effects/uA1Ofqoyi0UiZIPk.htm)|Effect: Clue In (Expertise)|Effet : Partager les indices (Expertise de l'enquêteur)|libre|
|[UBC6HbfqbfPQYlMq.htm](feat-effects/UBC6HbfqbfPQYlMq.htm)|Effect: Tidal Shield|Effet : Bouclier de la marée|libre|
|[uBJsxCzNhje8m8jj.htm](feat-effects/uBJsxCzNhje8m8jj.htm)|Effect: Panache|Effet : Panache|libre|
|[UE0yky6aW0WCF0Qg.htm](feat-effects/UE0yky6aW0WCF0Qg.htm)|Effect: Fulminating Shot|Effet : Tir fulminant|libre|
|[uFYvW3kFP9iyNfVX.htm](feat-effects/uFYvW3kFP9iyNfVX.htm)|Stance: Clinging Shadows Stance|Posture : Posture des ombres tenaces|libre|
|[ugeStF0Rj8phBPWL.htm](feat-effects/ugeStF0Rj8phBPWL.htm)|Effect: Witch's Charge|Effet : Fardeau du sorcier|libre|
|[Unfl4QQURWaX2zfd.htm](feat-effects/Unfl4QQURWaX2zfd.htm)|Stance: Ricochet Stance|Posture : Posture du ricochet|libre|
|[UQ7vZgmfK0VSFS8A.htm](feat-effects/UQ7vZgmfK0VSFS8A.htm)|Effect: Aberrant Blood Magic|Effet : Magie du sang aberrant|libre|
|[uXCU8GgriUjuj5FV.htm](feat-effects/uXCU8GgriUjuj5FV.htm)|Effect: Hunter's Edge, Flurry|Effet : Spécialisation du chasseur, Déluge|libre|
|[UzIamWcEJTOjwfoA.htm](feat-effects/UzIamWcEJTOjwfoA.htm)|Effect: Spin Tale|Effet : Broder un conte|libre|
|[UZKIKLuwpQu47feK.htm](feat-effects/UZKIKLuwpQu47feK.htm)|Stance: Gorilla Stance (Gorilla Pound)|Posture : Posture du gorille (Martèlement du gorille)|libre|
|[v2HDcrxQF2Dncjbs.htm](feat-effects/v2HDcrxQF2Dncjbs.htm)|Effect: Flamboyant Cruelty|Effet : Cruauté flamboyante|libre|
|[V6lnFOq998B76Rr0.htm](feat-effects/V6lnFOq998B76Rr0.htm)|Effect: Curse of Ancestral Meddling|Effet : Malédiction de l'incursion ancestrale|libre|
|[vguxP8ukwVTWWWaA.htm](feat-effects/vguxP8ukwVTWWWaA.htm)|Effect: Imperial Blood Magic|Effet : Magie du sang impérial|libre|
|[vhSYlQiAQMLuXqoc.htm](feat-effects/vhSYlQiAQMLuXqoc.htm)|Effect: Clue In|Effet : Partager les indices|libre|
|[VIScVb6Hl7KwoWfH.htm](feat-effects/VIScVb6Hl7KwoWfH.htm)|Effect: Bolera's Interrogation (Critical Failure)|Effet : Interrogatoire de Boléra (échec critique)|libre|
|[vjvcccAwdkOLA1Fc.htm](feat-effects/vjvcccAwdkOLA1Fc.htm)|Stance: Peafowl Stance|Posture : Posture du paon|libre|
|[VOOShYoB4gTopZtg.htm](feat-effects/VOOShYoB4gTopZtg.htm)|Effect: Aura of Faith|Effet : Aura de foi|libre|
|[vQj9I3FShzx3lNoG.htm](feat-effects/vQj9I3FShzx3lNoG.htm)|Effect: Deep Freeze (Failure)|Effet : Congélation profonde(Échec)|libre|
|[W2tWq0gdAcnoz2MO.htm](feat-effects/W2tWq0gdAcnoz2MO.htm)|Effect: Monster Hunter|Effet : Chasseur de monstres|libre|
|[w6X7io56B2HHTOvs.htm](feat-effects/w6X7io56B2HHTOvs.htm)|Effect: Guardian's Deflection|Effet : Déviation du Gardien|libre|
|[W8CKuADdbODpBh6O.htm](feat-effects/W8CKuADdbODpBh6O.htm)|Stance: Lunging Stance|Posture : Posture de fente|libre|
|[W8HWQ47YNHWB8kj6.htm](feat-effects/W8HWQ47YNHWB8kj6.htm)|Effect: Topple Giants|Effet : Renverser les géants|libre|
|[wjNNHgX6ceKLbn8Q.htm](feat-effects/wjNNHgX6ceKLbn8Q.htm)|Effect: Rallying Charge|Effet : Charge de ralliement|libre|
|[WKbsjhjYIMv59JQg.htm](feat-effects/WKbsjhjYIMv59JQg.htm)|Effect: Deep Freeze (Success)|Effet : Congélation profonde (Succès)|libre|
|[wmBSuZPqiDyUNwXH.htm](feat-effects/wmBSuZPqiDyUNwXH.htm)|Effect: Dragon's Rage Wings|Effet : Ailes de rage du dragon|libre|
|[wQDHpOKY3GZqvS2v.htm](feat-effects/wQDHpOKY3GZqvS2v.htm)|Effect: Seedpod|Effet : Cosse|libre|
|[WRe8qbemruWxkN8d.htm](feat-effects/WRe8qbemruWxkN8d.htm)|Effect: Rampaging Form (Frozen Winds Kitsune)|Effet : Forme destructrice (Kitsune des vents glacés)|libre|
|[WrWSieH9Acy6XuzV.htm](feat-effects/WrWSieH9Acy6XuzV.htm)|Effect: Educate Allies (Ally)|Effet : Alliés instruits (Allié)|libre|
|[X19XgqqItqZ4tfmq.htm](feat-effects/X19XgqqItqZ4tfmq.htm)|Effect: Guardian's Embrace|Effet : Étreinte du gardien|libre|
|[X1pGyhMKrCTvHB0q.htm](feat-effects/X1pGyhMKrCTvHB0q.htm)|Effect: Favorable Winds|Effet : Vents favorables|libre|
|[x4Sb3qaMJo8x1r3X.htm](feat-effects/x4Sb3qaMJo8x1r3X.htm)|Effect: Emblazon Energy (Weapon, Acid)|Effet : Énergie blasonnée (Arme, Acide)|libre|
|[XaZdQHF9GvaJINqH.htm](feat-effects/XaZdQHF9GvaJINqH.htm)|Effect: Elemental Assault|Effet : Assaut élémentaire|libre|
|[XC3dRbwfu35vuvmM.htm](feat-effects/XC3dRbwfu35vuvmM.htm)|Effect: Align Armament (Chaotic)|Effet : Arsenal aligné (Chaotique)|libre|
|[xDT10fUWp8UStSZR.htm](feat-effects/xDT10fUWp8UStSZR.htm)|Effect: Cavalier's Banner|Effet : Bannière du Cavalier|libre|
|[XJtlvaqAHseq1yoz.htm](feat-effects/XJtlvaqAHseq1yoz.htm)|Effect: Towering Presence|Effet : Présence imposante|libre|
|[XM1AA8z5cHm8sJXM.htm](feat-effects/XM1AA8z5cHm8sJXM.htm)|Effect: Enlightened Presence|Effet : Présence éclairée|libre|
|[xPg5wzzKNxJy18rU.htm](feat-effects/xPg5wzzKNxJy18rU.htm)|Effect: Brightness Seeker|Effet : Aspirant à l'illumination|libre|
|[Y96a1OedsU8PVf7z.htm](feat-effects/Y96a1OedsU8PVf7z.htm)|Effect: Starlight Armor|Effet : Armure de lumière stellaire|libre|
|[ybc7tZwByenCzow8.htm](feat-effects/ybc7tZwByenCzow8.htm)|Effect: Creeping Ashes|Effet : Malédiction des cendres rampantes|libre|
|[yBTASi3FvnReAwHy.htm](feat-effects/yBTASi3FvnReAwHy.htm)|Effect: Debilitating Strike|Effet : Frappe incapacitante|libre|
|[yfbP64r4a9e5oyli.htm](feat-effects/yfbP64r4a9e5oyli.htm)|Effect: Demonic Blood Magic (Target)|Effet : Magie du sang démoniaque (Cible)|libre|
|[YkiTA74FrUUu5IvI.htm](feat-effects/YkiTA74FrUUu5IvI.htm)|Stance: Rough Terrain Stance|Posture : Posture du terrain accidenté|libre|
|[YKJhjkerCW0Jl6HP.htm](feat-effects/YKJhjkerCW0Jl6HP.htm)|Effect: Life-Giving Magic|Effet : Magie donneuse de vie|libre|
|[YMdRmEcOlM3uU9Em.htm](feat-effects/YMdRmEcOlM3uU9Em.htm)|Effect: Living for the Applause|Effet : Vivre pour les applaudissements|libre|
|[yr5ey5qC8dXH749T.htm](feat-effects/yr5ey5qC8dXH749T.htm)|Effect: Entity's Resurgence|Effet : Résurgence de l'entité|libre|
|[ytG5XJmkOnDOTjNN.htm](feat-effects/ytG5XJmkOnDOTjNN.htm)|Effect: Soaring Flight|Effet : Voler haut|libre|
|[z3uyCMBddrPK5umr.htm](feat-effects/z3uyCMBddrPK5umr.htm)|Effect: Rage|Effet : Rage|libre|
|[z4pnE8KyUdEkJmru.htm](feat-effects/z4pnE8KyUdEkJmru.htm)|Effect: Clue In (Detective's Readiness, Expertise)|Effet : Partager les indices (Vivacité du détective et expertise de l'enquêteur)|libre|
|[zcJii1XyOne9EvMr.htm](feat-effects/zcJii1XyOne9EvMr.htm)|Effect: Assisting Shot|Effet : Tir de soutien|libre|
|[ZMFgz4GYSsFeaKKK.htm](feat-effects/ZMFgz4GYSsFeaKKK.htm)|Effect: Rugged Mentor Boon (PFS)|Effet : Récompense de mentor robuste (PFS)|libre|
|[ZnKnOPPq3cG54PlG.htm](feat-effects/ZnKnOPPq3cG54PlG.htm)|Effect: Liberating Step (vs. Undead)|Effet : Pas libérateur (morts-vivants)|libre|
|[zocU4IYIlWwRKUuE.htm](feat-effects/zocU4IYIlWwRKUuE.htm)|Effect: Energy Shot|Effet : Tir énergétique|libre|
|[zQHF2kkhZRAcrQvR.htm](feat-effects/zQHF2kkhZRAcrQvR.htm)|Effect: Sniping Duo Dedication|Effet : Dévouement : Duo de tieur d'élite|libre|
|[ZSgB3imGveukWUxs.htm](feat-effects/ZSgB3imGveukWUxs.htm)|Effect: Bespell Weapon (Mental)|Effet : Arme enchantée (Mental)|libre|
|[ZsO5juyylVoxUkXh.htm](feat-effects/ZsO5juyylVoxUkXh.htm)|Effect: Bone Spikes|Effet : Pointe d'os|libre|
|[zUvicEXd4OgCZ1cO.htm](feat-effects/zUvicEXd4OgCZ1cO.htm)|Effect: You're an Embarrassment|Effet : Tu es gênant !|libre|
|[zZ25N1zpXA8GNhFL.htm](feat-effects/zZ25N1zpXA8GNhFL.htm)|Effect: Divine Weapon (Force)|Effet : Arme Divine (Force)|libre|
|[zzC2qZwEKf4Ja3xD.htm](feat-effects/zzC2qZwEKf4Ja3xD.htm)|Stance: Impassable Wall Stance|Posture : Posture du mur infranchissable|libre|
