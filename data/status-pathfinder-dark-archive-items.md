# État de la traduction (pathfinder-dark-archive-items)

 * **aucune**: 43
 * **libre**: 19
 * **vide**: 9


Dernière mise à jour: 2023-07-12 20:28 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[0bSHLXIeRbwxpKtv.htm](pathfinder-dark-archive-items/0bSHLXIeRbwxpKtv.htm)|Drop|
|[0l35fQJcnzJb7vMi.htm](pathfinder-dark-archive-items/0l35fQJcnzJb7vMi.htm)|Gust|
|[1MnFDLD3UqAsLnmn.htm](pathfinder-dark-archive-items/1MnFDLD3UqAsLnmn.htm)|Tempt|
|[2LrlEIwWcp7z9jL4.htm](pathfinder-dark-archive-items/2LrlEIwWcp7z9jL4.htm)|Attack of Opportunity (Special)|
|[2WfGqYcBTEQfIJje.htm](pathfinder-dark-archive-items/2WfGqYcBTEQfIJje.htm)|Siphon Soul|
|[4A7cNLcKMBz6WJiy.htm](pathfinder-dark-archive-items/4A7cNLcKMBz6WJiy.htm)|String Astral Body|
|[5n8EB4Rkx3GH7H9F.htm](pathfinder-dark-archive-items/5n8EB4Rkx3GH7H9F.htm)|Ascension is Near|
|[5ziv4KKVkKAcChTU.htm](pathfinder-dark-archive-items/5ziv4KKVkKAcChTU.htm)|Stalker in Dreams|
|[9AePbujsZky8xUjK.htm](pathfinder-dark-archive-items/9AePbujsZky8xUjK.htm)|Astral Spindel Implement|
|[C5zKC61h7GcivnD5.htm](pathfinder-dark-archive-items/C5zKC61h7GcivnD5.htm)|Disgusted by Food|
|[crjSzGgHUw6zPIVX.htm](pathfinder-dark-archive-items/crjSzGgHUw6zPIVX.htm)|Reconstitute from Thought|
|[dXHZxCKOYXB0IsAL.htm](pathfinder-dark-archive-items/dXHZxCKOYXB0IsAL.htm)|Appear|
|[G4i0tp7lYajN7KwD.htm](pathfinder-dark-archive-items/G4i0tp7lYajN7KwD.htm)|Dreams Unraveling|
|[g86YWdXagoWqk0OQ.htm](pathfinder-dark-archive-items/g86YWdXagoWqk0OQ.htm)|Destabilized Form|
|[GGttpO7eWtif6ZON.htm](pathfinder-dark-archive-items/GGttpO7eWtif6ZON.htm)|Nameless Nightmare|
|[gjGhslPt9ZQSI0yX.htm](pathfinder-dark-archive-items/gjGhslPt9ZQSI0yX.htm)|Contingency Oathday-Nine-Rova|
|[GTidGY6vXi9cj9yz.htm](pathfinder-dark-archive-items/GTidGY6vXi9cj9yz.htm)|Crystal Vitality Link|
|[GufPL7fXAz1R4tFA.htm](pathfinder-dark-archive-items/GufPL7fXAz1R4tFA.htm)|Endless|
|[H5No3yLHugKzgNQr.htm](pathfinder-dark-archive-items/H5No3yLHugKzgNQr.htm)|Anchored in Silk|
|[Hc9RMNJy8VQgbgt8.htm](pathfinder-dark-archive-items/Hc9RMNJy8VQgbgt8.htm)|In There Somewhere|
|[HHFw3W3cOgIJv8xE.htm](pathfinder-dark-archive-items/HHFw3W3cOgIJv8xE.htm)|Give of Oneself|
|[K7zv6GnWMFnvcN2z.htm](pathfinder-dark-archive-items/K7zv6GnWMFnvcN2z.htm)|Confounding Misdirection|
|[ljtEKeEjWZpLfCLL.htm](pathfinder-dark-archive-items/ljtEKeEjWZpLfCLL.htm)|Don Shroud|
|[lY8P3VlUAyOoBnVJ.htm](pathfinder-dark-archive-items/lY8P3VlUAyOoBnVJ.htm)|Bay|
|[M6pVTXxAJ26CuNx5.htm](pathfinder-dark-archive-items/M6pVTXxAJ26CuNx5.htm)|Spawn Reflection|
|[mF3Y5psHWHrkB81u.htm](pathfinder-dark-archive-items/mF3Y5psHWHrkB81u.htm)|Face Your Fear|
|[mLNLTTi0YhxCWKWq.htm](pathfinder-dark-archive-items/mLNLTTi0YhxCWKWq.htm)|Flip|
|[pAa1q5ts9oP6qdky.htm](pathfinder-dark-archive-items/pAa1q5ts9oP6qdky.htm)|Tomorrow's Fury|
|[PFLlpNCLFoMvvzjF.htm](pathfinder-dark-archive-items/PFLlpNCLFoMvvzjF.htm)|Separate Astral Body|
|[pqQB2JJ1Z8S2K9ay.htm](pathfinder-dark-archive-items/pqQB2JJ1Z8S2K9ay.htm)|Of Same Essence|
|[PSN0OsnaXCyM9qu3.htm](pathfinder-dark-archive-items/PSN0OsnaXCyM9qu3.htm)|Astral Thread Control|
|[Q0howoIf0TfR9YuR.htm](pathfinder-dark-archive-items/Q0howoIf0TfR9YuR.htm)|Lose Form|
|[qTHgALlk6FGnqd7W.htm](pathfinder-dark-archive-items/qTHgALlk6FGnqd7W.htm)|Reflection Routine|
|[rKdbdgqCvS4u8s4L.htm](pathfinder-dark-archive-items/rKdbdgqCvS4u8s4L.htm)|Painful Transformation|
|[s0uQRVp56Th5lEsd.htm](pathfinder-dark-archive-items/s0uQRVp56Th5lEsd.htm)|Stitch Into the Web|
|[sxD01nPswudJsjzS.htm](pathfinder-dark-archive-items/sxD01nPswudJsjzS.htm)|Swallow Future|
|[SzPTvuxOEAOJsUZN.htm](pathfinder-dark-archive-items/SzPTvuxOEAOJsUZN.htm)|Crystal Flower Crown|
|[V4xSx1Z7pl5RTlY8.htm](pathfinder-dark-archive-items/V4xSx1Z7pl5RTlY8.htm)|Take a Seat!|
|[Vcb2fzxMkjOhWNPz.htm](pathfinder-dark-archive-items/Vcb2fzxMkjOhWNPz.htm)|Drain Potential|
|[vt810lsvoNHLaCNF.htm](pathfinder-dark-archive-items/vt810lsvoNHLaCNF.htm)|Spindle's Web|
|[WYeWa4hUK5Lu9dLb.htm](pathfinder-dark-archive-items/WYeWa4hUK5Lu9dLb.htm)|False Step|
|[XcNlseVC4Ik7ETIX.htm](pathfinder-dark-archive-items/XcNlseVC4Ik7ETIX.htm)|Sever Astral Threads|
|[ZJEa0ERwBrbsozjF.htm](pathfinder-dark-archive-items/ZJEa0ERwBrbsozjF.htm)|Disorient|

## Liste des éléments vides dont le nom doit être traduit

| Fichier   | Nom (EN)    | État |
|-----------|-------------|:----:|
|[HOnJVrqXXtc3EGEu.htm](pathfinder-dark-archive-items/HOnJVrqXXtc3EGEu.htm)|Shrouded Touch|vide|
|[jHHE5GG2biaCiVYC.htm](pathfinder-dark-archive-items/jHHE5GG2biaCiVYC.htm)|Psychic Scream|vide|
|[JzkjkxNi7EFlzsmZ.htm](pathfinder-dark-archive-items/JzkjkxNi7EFlzsmZ.htm)|Stealth|vide|
|[nvyRpGOfcTh5qgcI.htm](pathfinder-dark-archive-items/nvyRpGOfcTh5qgcI.htm)|Primal Prepared Spells|vide|
|[spFa0equ7phkEcc9.htm](pathfinder-dark-archive-items/spFa0equ7phkEcc9.htm)|Ludovica Lore|vide|
|[TpYNcJKE1v9ktu1H.htm](pathfinder-dark-archive-items/TpYNcJKE1v9ktu1H.htm)|Reflected Weapon|vide|
|[UPEC8mJThmuoRkKJ.htm](pathfinder-dark-archive-items/UPEC8mJThmuoRkKJ.htm)|Astral Silk|vide|
|[URApeqv0vXz0Usnr.htm](pathfinder-dark-archive-items/URApeqv0vXz0Usnr.htm)|Wall|vide|
|[VdAYDEzY4gulPTVL.htm](pathfinder-dark-archive-items/VdAYDEzY4gulPTVL.htm)|Esoteric Lore|vide|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0D7DkzCzV4iv9Q0r.htm](pathfinder-dark-archive-items/0D7DkzCzV4iv9Q0r.htm)|Grab|Empoignade/Agrippement|libre|
|[0QWVJ7Of8cQNKp0N.htm](pathfinder-dark-archive-items/0QWVJ7Of8cQNKp0N.htm)|Slink in Shadows|Se fondre dans les ombres|libre|
|[41MourwRyz9uDnd8.htm](pathfinder-dark-archive-items/41MourwRyz9uDnd8.htm)|Claw|Griffe|libre|
|[6Psn5RSYm8UGDpwM.htm](pathfinder-dark-archive-items/6Psn5RSYm8UGDpwM.htm)|Pod Prison|Prison de cosse|libre|
|[7pxsxbazh1PpK8A4.htm](pathfinder-dark-archive-items/7pxsxbazh1PpK8A4.htm)|Fast Healing 10|Guérison accélérée 10|libre|
|[ACHlnYzXVxizzqPw.htm](pathfinder-dark-archive-items/ACHlnYzXVxizzqPw.htm)|Light Vulnerability|Vulnérabilité à la lumière|libre|
|[cOno50AugBRsfxaZ.htm](pathfinder-dark-archive-items/cOno50AugBRsfxaZ.htm)|Ceremonial Robes|Robes cérémonielles|libre|
|[Dj3szMxIZ2hwkpbQ.htm](pathfinder-dark-archive-items/Dj3szMxIZ2hwkpbQ.htm)|Dagger|+1,striking|Dague de frappe +1|libre|
|[eA8cwxuJWpDq8jgk.htm](pathfinder-dark-archive-items/eA8cwxuJWpDq8jgk.htm)|Leap into the Unknown|Sauter dans l'inconnu|libre|
|[EC0YgI9NRxD1ISHc.htm](pathfinder-dark-archive-items/EC0YgI9NRxD1ISHc.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|libre|
|[kjaeBRLTBCSOl9cS.htm](pathfinder-dark-archive-items/kjaeBRLTBCSOl9cS.htm)|Swallow Whole|Gober|libre|
|[LkqCkG3YFw362zdS.htm](pathfinder-dark-archive-items/LkqCkG3YFw362zdS.htm)|Crystal Siphon|Siphon de cristal|libre|
|[lMprNG8h8WUaezg1.htm](pathfinder-dark-archive-items/lMprNG8h8WUaezg1.htm)|Claw|Griffe|libre|
|[MvwGFLtsnf8ZFqhG.htm](pathfinder-dark-archive-items/MvwGFLtsnf8ZFqhG.htm)|Root|Racine|libre|
|[SDF2zWv6zGkmcQH9.htm](pathfinder-dark-archive-items/SDF2zWv6zGkmcQH9.htm)|Bite|Morsure|libre|
|[SlJZiCxXuBW155d5.htm](pathfinder-dark-archive-items/SlJZiCxXuBW155d5.htm)|Athletics|Athlétisme|libre|
|[uOpDNZG0ysnNMkxM.htm](pathfinder-dark-archive-items/uOpDNZG0ysnNMkxM.htm)|Spray Blossoms|Diffuser des fleurs|libre|
|[Uoxf8sSuS6pG7QQF.htm](pathfinder-dark-archive-items/Uoxf8sSuS6pG7QQF.htm)|Pod Spawn|Rejeton cosse|libre|
|[x47iDfIE9albveGl.htm](pathfinder-dark-archive-items/x47iDfIE9albveGl.htm)|Shadow Hand|Main d'ombre|libre|
