# État de la traduction (npc-gallery-items)

 * **officielle**: 611
 * **libre**: 126


Dernière mise à jour: 2023-07-12 20:28 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[06QUpYDzeWLuHjVF.htm](npc-gallery-items/06QUpYDzeWLuHjVF.htm)|Shortsword|Épée courte|officielle|
|[087PGXgVjLNGs7lG.htm](npc-gallery-items/087PGXgVjLNGs7lG.htm)|Javelin|Javeline|officielle|
|[0fWZn3xodpxwco8r.htm](npc-gallery-items/0fWZn3xodpxwco8r.htm)|Swear Vengeance|Jurer vengeance|officielle|
|[0hxn526bxpU9psxJ.htm](npc-gallery-items/0hxn526bxpU9psxJ.htm)|Shiv|Surin|libre|
|[0mY6UXKCPuoJcVKz.htm](npc-gallery-items/0mY6UXKCPuoJcVKz.htm)|Rapier|Rapière|officielle|
|[0osYGMTj1AfagtgG.htm](npc-gallery-items/0osYGMTj1AfagtgG.htm)|Hand Crossbow|Arbalète de poing|officielle|
|[0OzSGNJ8fmj9Z2dh.htm](npc-gallery-items/0OzSGNJ8fmj9Z2dh.htm)|Scimitar|Cimeterre|officielle|
|[0PlCgdUO4JNfJMKr.htm](npc-gallery-items/0PlCgdUO4JNfJMKr.htm)|Hand Crossbow|Arbalète de poing|officielle|
|[0WoLHxcEDfHig6LB.htm](npc-gallery-items/0WoLHxcEDfHig6LB.htm)|Scoundrel's Feint|Feinte du scélérat|officielle|
|[0yKbSsTBHJzUb4J9.htm](npc-gallery-items/0yKbSsTBHJzUb4J9.htm)|Pitchfork|Fourche|officielle|
|[0Z0tfKCoRaKagvVT.htm](npc-gallery-items/0Z0tfKCoRaKagvVT.htm)|Pike and Strike|Pique et frappe|officielle|
|[18XOpkL2M0BH3sDN.htm](npc-gallery-items/18XOpkL2M0BH3sDN.htm)|Morningstar|+1,striking|Morgenstern de frappe +1|libre|
|[1caXZSSxHAApzo4g.htm](npc-gallery-items/1caXZSSxHAApzo4g.htm)|Law and Rhetoric Book|Livre de la loi et de la rhétorique|libre|
|[1dkm6dgNqE06TZD0.htm](npc-gallery-items/1dkm6dgNqE06TZD0.htm)|Pewter Mug|Gobelin en étain|officielle|
|[1gLrx7OTMtRPbK9N.htm](npc-gallery-items/1gLrx7OTMtRPbK9N.htm)|Brutal Beating|Raclée brutale|officielle|
|[1kzfqcU4yMR8Evgt.htm](npc-gallery-items/1kzfqcU4yMR8Evgt.htm)|Frost Vial (Lesser) (Infused)|Fiole de givre inférieure (imprégnée)|libre|
|[1VRZfyweUQjfqW3F.htm](npc-gallery-items/1VRZfyweUQjfqW3F.htm)|Steady Spellcasting|Incantation fiable|officielle|
|[1Z1VM6NR01OCRpFf.htm](npc-gallery-items/1Z1VM6NR01OCRpFf.htm)|Cleric Domain Spells|Sorts de domaine de prêtre|officielle|
|[22gFqjuhKcKMgJKN.htm](npc-gallery-items/22gFqjuhKcKMgJKN.htm)|Warden's Protection|Protection du gardien|officielle|
|[22PsMMGp8JINhVoi.htm](npc-gallery-items/22PsMMGp8JINhVoi.htm)|Rapier|Rapière|officielle|
|[27l65icQwiqUX7yl.htm](npc-gallery-items/27l65icQwiqUX7yl.htm)|Trident|+1|Trident +1|libre|
|[28SnHCyal79ByXz4.htm](npc-gallery-items/28SnHCyal79ByXz4.htm)|Composite Longbow|Arc long composite|officielle|
|[2B42tVGi3YO1t4rU.htm](npc-gallery-items/2B42tVGi3YO1t4rU.htm)|Leather Apron|Tablier en cuir|libre|
|[2HONqJ3KSR0h25We.htm](npc-gallery-items/2HONqJ3KSR0h25We.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|officielle|
|[2L7yj9lThk67SmMC.htm](npc-gallery-items/2L7yj9lThk67SmMC.htm)|Performance|Représentation|officielle|
|[2LYwFejQFhQ3id2V.htm](npc-gallery-items/2LYwFejQFhQ3id2V.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[2m3sOYaFmlCapCZm.htm](npc-gallery-items/2m3sOYaFmlCapCZm.htm)|Graveyard Lore|Connaissance du cimetière|officielle|
|[2pNLHufgvxymiXGj.htm](npc-gallery-items/2pNLHufgvxymiXGj.htm)|Rock|Pierre|officielle|
|[2PrqruymHWvMXHjI.htm](npc-gallery-items/2PrqruymHWvMXHjI.htm)|Air of Authority|Posture autoritaire|officielle|
|[2uSSFi0lqFoWgzYI.htm](npc-gallery-items/2uSSFi0lqFoWgzYI.htm)|Home Turf|À la maison|officielle|
|[2w1WQr635JVnsORb.htm](npc-gallery-items/2w1WQr635JVnsORb.htm)|Swift Sneak|Furtivité rapide|officielle|
|[2WL0bJlVq6JxInsp.htm](npc-gallery-items/2WL0bJlVq6JxInsp.htm)|Warfare Lore|Connaissance de la guerre|officielle|
|[2xwVAGUh92PRESt7.htm](npc-gallery-items/2xwVAGUh92PRESt7.htm)|Wizard School Spell|Sorts d'école de magie|officielle|
|[3CHnQSHfFAroFmAn.htm](npc-gallery-items/3CHnQSHfFAroFmAn.htm)|Light Hammer|Marteau de guerre léger|officielle|
|[3Cmc9ZLwNvRhmJBB.htm](npc-gallery-items/3Cmc9ZLwNvRhmJBB.htm)|Mobility|Mobilité|officielle|
|[3CtPYZLeE2j7fIME.htm](npc-gallery-items/3CtPYZLeE2j7fIME.htm)|Shiv|Surin|officielle|
|[3EX2JaX17gPKVhcb.htm](npc-gallery-items/3EX2JaX17gPKVhcb.htm)|Main-Gauche|Main-gauche|officielle|
|[3FUfdj3U5vEftIPg.htm](npc-gallery-items/3FUfdj3U5vEftIPg.htm)|Primal Prepared Spells|Sorts primordiaux préparés|officielle|
|[3Gsk5abTPsMK7L90.htm](npc-gallery-items/3Gsk5abTPsMK7L90.htm)|Dagger|Dague|officielle|
|[3Hp4zP3MQ5RzRFcu.htm](npc-gallery-items/3Hp4zP3MQ5RzRFcu.htm)|Fist|Poing|officielle|
|[3i4w75rSKQiwZzwD.htm](npc-gallery-items/3i4w75rSKQiwZzwD.htm)|Fated Doom|Destin tragique|officielle|
|[3k43BvFaLIDzoL5a.htm](npc-gallery-items/3k43BvFaLIDzoL5a.htm)|Crossbow|Arbalète|officielle|
|[3LcqeSNm2BGnGC1G.htm](npc-gallery-items/3LcqeSNm2BGnGC1G.htm)|Placate|Amadouer|officielle|
|[3lmMOkUuWRYkYOfO.htm](npc-gallery-items/3lmMOkUuWRYkYOfO.htm)|Divine Prepared Spells|Sorts divins préparés|officielle|
|[3opCHH7my1xVzJSY.htm](npc-gallery-items/3opCHH7my1xVzJSY.htm)|Underwater Lore|Connaissance sous-marine|officielle|
|[3pDy4fG7Gyy7veGs.htm](npc-gallery-items/3pDy4fG7Gyy7veGs.htm)|Warhammer|+1,striking|Marteau de guerre de frappe +1|officielle|
|[3YP4WRM7J9c4OkFT.htm](npc-gallery-items/3YP4WRM7J9c4OkFT.htm)|Sailing Lore|Connaissance de la navigation|officielle|
|[486JeF49AVqoD8d0.htm](npc-gallery-items/486JeF49AVqoD8d0.htm)|Composite Longbow|Arc long composite|officielle|
|[49sfbmwEvN94dGhS.htm](npc-gallery-items/49sfbmwEvN94dGhS.htm)|Hand Crossbow|Arbalète de poing|officielle|
|[4baqnRP77X4lxpxU.htm](npc-gallery-items/4baqnRP77X4lxpxU.htm)|Wild Empathy|Empathie sauvage|officielle|
|[4bQvbouLvfbJf5ml.htm](npc-gallery-items/4bQvbouLvfbJf5ml.htm)|Farming Lore|Connaissance agricole|officielle|
|[4E5jd0bmls7oDWHu.htm](npc-gallery-items/4E5jd0bmls7oDWHu.htm)|Trident|Trident|officielle|
|[4fVWXHO7ivGD8UMb.htm](npc-gallery-items/4fVWXHO7ivGD8UMb.htm)|Gavel|Maillet du juge|libre|
|[4l9I2HyaE5ptxZSF.htm](npc-gallery-items/4l9I2HyaE5ptxZSF.htm)|Drain Bonded Item|Drain d'objet lié|officielle|
|[4mQUcJRfogdhtD4e.htm](npc-gallery-items/4mQUcJRfogdhtD4e.htm)|Religious Symbol (Wooden) of Pharasma|Symbole religieux en bois de Pharasma|libre|
|[4NNvg2SM3AoqAWLt.htm](npc-gallery-items/4NNvg2SM3AoqAWLt.htm)|Cult Lore (applies to cultist's own cult)|Connaissance du culte (son propre culte)|officielle|
|[4SfHayudG99XPwJP.htm](npc-gallery-items/4SfHayudG99XPwJP.htm)|Shortsword|+1|Épée courte +1|officielle|
|[4sLSH6LuZKxvs4Qo.htm](npc-gallery-items/4sLSH6LuZKxvs4Qo.htm)|Bandolier|Bandoulière|officielle|
|[4VbQq13CfCCwqfYt.htm](npc-gallery-items/4VbQq13CfCCwqfYt.htm)|Foot|Pied|officielle|
|[4ZB9khLqIwNxe4j2.htm](npc-gallery-items/4ZB9khLqIwNxe4j2.htm)|Sailing Lore|Connaissance de la navigation maritime|officielle|
|[4zzNfH7d9409n3cP.htm](npc-gallery-items/4zzNfH7d9409n3cP.htm)|Robes|Robes|libre|
|[51OPbUesBItU8rTj.htm](npc-gallery-items/51OPbUesBItU8rTj.htm)|Forest Lore|Connaissance des forêts|officielle|
|[598qUP8bkS1TdMEY.htm](npc-gallery-items/598qUP8bkS1TdMEY.htm)|Fist|Poing|officielle|
|[5as8LbntWBRwhdy8.htm](npc-gallery-items/5as8LbntWBRwhdy8.htm)|Sentry's Aim|Visée de la sentinelle|officielle|
|[5DPI512dyzb4L4kR.htm](npc-gallery-items/5DPI512dyzb4L4kR.htm)|Shield Block|Blocage au bouclier|officielle|
|[5dZchzh220BWuYbG.htm](npc-gallery-items/5dZchzh220BWuYbG.htm)|Theatre Lore|Connaissance théâtrale|officielle|
|[5h9TgMqtTUK56q1p.htm](npc-gallery-items/5h9TgMqtTUK56q1p.htm)|Divine Focus Spells|Sorts divins focalisés|officielle|
|[5iTqDt4fDSnYuJIg.htm](npc-gallery-items/5iTqDt4fDSnYuJIg.htm)|Dual Disarm|Double désarmement|officielle|
|[5MyYDCSSRvOAcJCe.htm](npc-gallery-items/5MyYDCSSRvOAcJCe.htm)|Theatre Lore|Connaissance théâtrale|officielle|
|[5oQ0ag3YkEb6hYwX.htm](npc-gallery-items/5oQ0ag3YkEb6hYwX.htm)|Academia Lore|Connaissance universitaire|officielle|
|[5qtQA0th9HWkfecU.htm](npc-gallery-items/5qtQA0th9HWkfecU.htm)|Hunt Prey|Chasser une proie|officielle|
|[6048zx9AI7OaIsjX.htm](npc-gallery-items/6048zx9AI7OaIsjX.htm)|Reach Spell|Sort éloigné|officielle|
|[64r9RgELkr6OXrX7.htm](npc-gallery-items/64r9RgELkr6OXrX7.htm)|Alchemist's Tools (Used as "Blessed Items" to Fool Marks)|Outils d'alchimiste (utilisé comme des "objets sacrés")|libre|
|[6bZGX49gZGcVxwBY.htm](npc-gallery-items/6bZGX49gZGcVxwBY.htm)|Primal Focus Spells|Sorts primordiaux focalisés|officielle|
|[6FKPyy0QIzlrgYay.htm](npc-gallery-items/6FKPyy0QIzlrgYay.htm)|Scroll of Spectral Hand (Level 2)|Parchemin de Main spectrale (Niveau 2)|libre|
|[6HFhSXAXGd9TV4GQ.htm](npc-gallery-items/6HFhSXAXGd9TV4GQ.htm)|Shovel|Pelle|libre|
|[6KJ0ua534rpJ3q5Q.htm](npc-gallery-items/6KJ0ua534rpJ3q5Q.htm)|Shortsword|Épée courte|officielle|
|[6kOiyngYv38Ec0lB.htm](npc-gallery-items/6kOiyngYv38Ec0lB.htm)|Divine Prepared Spells|Sorts divins préparés|officielle|
|[6r31Osvx8aSYtCts.htm](npc-gallery-items/6r31Osvx8aSYtCts.htm)|Athletics|Athlétisme|officielle|
|[6SS8rVfwSj7Oy7WY.htm](npc-gallery-items/6SS8rVfwSj7Oy7WY.htm)|Healing Hands|Mains guérisseuses|officielle|
|[6UQShM6aleyfYDgH.htm](npc-gallery-items/6UQShM6aleyfYDgH.htm)|Big Swing|Coup violent|officielle|
|[6XSEC6evi0cGuYXY.htm](npc-gallery-items/6XSEC6evi0cGuYXY.htm)|Formula Book|Livre de formules|officielle|
|[6YMPTiP6aT0CgYpJ.htm](npc-gallery-items/6YMPTiP6aT0CgYpJ.htm)|Spellbook|Grimoire|officielle|
|[78xUeN4lAiJy7HB8.htm](npc-gallery-items/78xUeN4lAiJy7HB8.htm)|Paragon's Guard|Protection du parangon|officielle|
|[7bwgpaZGG2Bs6DSx.htm](npc-gallery-items/7bwgpaZGG2Bs6DSx.htm)|Apple|Pomme|officielle|
|[7d9Cb3ugSc4NMuC1.htm](npc-gallery-items/7d9Cb3ugSc4NMuC1.htm)|Geography Lore|Connaissance de la géographie|officielle|
|[7JKronzMIeoVdmyI.htm](npc-gallery-items/7JKronzMIeoVdmyI.htm)|Bard Composition Spells|Sorts de composition de barde|officielle|
|[7JSOtd0bGE4q9bHf.htm](npc-gallery-items/7JSOtd0bGE4q9bHf.htm)|Legal Lore|Connaissance juridique|officielle|
|[7RDjgYcmwKNdhNZc.htm](npc-gallery-items/7RDjgYcmwKNdhNZc.htm)|Performance|Représentation|officielle|
|[7STN0PrV0AblMmmM.htm](npc-gallery-items/7STN0PrV0AblMmmM.htm)|Primal Spontaneous Spells|Sorts primordiaux spontanés|officielle|
|[7uL4FBIFV9kMEQKe.htm](npc-gallery-items/7uL4FBIFV9kMEQKe.htm)|Underworld Lore|Connaissance de la pègre|officielle|
|[7uuo0yXpPiywEFNQ.htm](npc-gallery-items/7uuo0yXpPiywEFNQ.htm)|Counterspell|Contresort|officielle|
|[7uXdmkkS4JVZOIyN.htm](npc-gallery-items/7uXdmkkS4JVZOIyN.htm)|Composite Shortbow|Arc court composite|officielle|
|[7ydMyqCO1oh7cHUR.htm](npc-gallery-items/7ydMyqCO1oh7cHUR.htm)|Fist|Poing|officielle|
|[80iH1mCHZeW5rWYE.htm](npc-gallery-items/80iH1mCHZeW5rWYE.htm)|Nimble Dodge|Esquive agile|officielle|
|[85x1pisleQwObtMf.htm](npc-gallery-items/85x1pisleQwObtMf.htm)|Mark for Death|Condamné à mort|officielle|
|[86ELyZJGF5f8M7ew.htm](npc-gallery-items/86ELyZJGF5f8M7ew.htm)|Bravery|Bravoure|officielle|
|[86tScORfgOX4zuix.htm](npc-gallery-items/86tScORfgOX4zuix.htm)|Sneak Attack|Attaque sournoise|officielle|
|[8812wH4PteCqa89F.htm](npc-gallery-items/8812wH4PteCqa89F.htm)|Dagger|Dague|officielle|
|[8DylMvT2Yh5SC6gF.htm](npc-gallery-items/8DylMvT2Yh5SC6gF.htm)|Sailing Lore|Connaissance de la navigation maritime|officielle|
|[8EfthxjczAzYkpvD.htm](npc-gallery-items/8EfthxjczAzYkpvD.htm)|Dagger|Dague|officielle|
|[8fAOSxgE0FgWpvsE.htm](npc-gallery-items/8fAOSxgE0FgWpvsE.htm)|Composite Shortbow|Arc court composite|officielle|
|[8l8fkQuORmfdmuFO.htm](npc-gallery-items/8l8fkQuORmfdmuFO.htm)|Divine Spontaneous Spells|Sorts divins spontanés|officielle|
|[8sKYzAJzGo9HTaC9.htm](npc-gallery-items/8sKYzAJzGo9HTaC9.htm)|Group Impression|Bonne impression de groupe|officielle|
|[9bruXwALVDFhxs9a.htm](npc-gallery-items/9bruXwALVDFhxs9a.htm)|Broom|Balai|libre|
|[9CtaEDgxqQK3AGrO.htm](npc-gallery-items/9CtaEDgxqQK3AGrO.htm)|Dagger|Dague|officielle|
|[9dDjsrPAI4FmqCth.htm](npc-gallery-items/9dDjsrPAI4FmqCth.htm)|Bomber's Eye Elixir (Lesser) (Infused)|Élixir d'oeil de mitrailleur inférieur (imprégné)|libre|
|[9hxxkHZZGdkhbZvg.htm](npc-gallery-items/9hxxkHZZGdkhbZvg.htm)|Shield Warden|Gardien au bouclier|officielle|
|[9KFKSYQInYw7RRFw.htm](npc-gallery-items/9KFKSYQInYw7RRFw.htm)|Greataxe|Grande hache|officielle|
|[9MoDarqE5LK66jA5.htm](npc-gallery-items/9MoDarqE5LK66jA5.htm)|Magaambya Lore|Connaissance du Magaambya|officielle|
|[9OxsjzXYOnydXvcb.htm](npc-gallery-items/9OxsjzXYOnydXvcb.htm)|Fist|Poing|officielle|
|[9PJJ2Aqeorbh7BnP.htm](npc-gallery-items/9PJJ2Aqeorbh7BnP.htm)|Throw Rock|Projection de rochers|officielle|
|[9UbRDyf3EovxiABp.htm](npc-gallery-items/9UbRDyf3EovxiABp.htm)|Fickle Prophecy|Prophétie capricieuse|officielle|
|[9vYQdc1M4NTXvorr.htm](npc-gallery-items/9vYQdc1M4NTXvorr.htm)|Map Lore|Connaissance de la cartographie|officielle|
|[9xXxlNwXM90sw8zk.htm](npc-gallery-items/9xXxlNwXM90sw8zk.htm)|Doctor's Hand|Main du médecin|officielle|
|[9xyeARIhxCpAj8BU.htm](npc-gallery-items/9xyeARIhxCpAj8BU.htm)|Greatclub|Massue|officielle|
|[a6VT956imq0UeyhM.htm](npc-gallery-items/a6VT956imq0UeyhM.htm)|Fist|Poing|officielle|
|[aAGb6LDoY8EcYRdE.htm](npc-gallery-items/aAGb6LDoY8EcYRdE.htm)|Pouch of Rocks|Sac contenant des pierres|libre|
|[aAgpROGXTToyiZfx.htm](npc-gallery-items/aAgpROGXTToyiZfx.htm)|Foot|Pied|officielle|
|[agaJs5LEhgmy0sPO.htm](npc-gallery-items/agaJs5LEhgmy0sPO.htm)|Scout's Warning|Avertissement de l'éclaireur|officielle|
|[aMJGSTnE6Wgp9MBr.htm](npc-gallery-items/aMJGSTnE6Wgp9MBr.htm)|Guiding Words|Indiquer la faiblesse|officielle|
|[ammC8wH7C165yxrp.htm](npc-gallery-items/ammC8wH7C165yxrp.htm)|Light in the Dark|Lumière dans les ténèbres|officielle|
|[Ant0lStzSYo01Zhd.htm](npc-gallery-items/Ant0lStzSYo01Zhd.htm)|Halberd|Hallebarde|officielle|
|[Ao7VlJhCpiWg1j18.htm](npc-gallery-items/Ao7VlJhCpiWg1j18.htm)|Court Garb|Accoutrement de la cour de justice|libre|
|[AtMtAwu4qZIQZq8E.htm](npc-gallery-items/AtMtAwu4qZIQZq8E.htm)|Dagger|Dague|officielle|
|[AuocSDVewXxiFzuz.htm](npc-gallery-items/AuocSDVewXxiFzuz.htm)|Fist|Poing|officielle|
|[Aw1KbwX8Q6IJWqjz.htm](npc-gallery-items/Aw1KbwX8Q6IJWqjz.htm)|Underworld Lore|Connaissance de la pègre|officielle|
|[AWKTmRlR5ZVNFq1A.htm](npc-gallery-items/AWKTmRlR5ZVNFq1A.htm)|Dagger|Dague|officielle|
|[axdkeSbnyjnpjO2h.htm](npc-gallery-items/axdkeSbnyjnpjO2h.htm)|Speaker of the Oceans|Traducteur d'océan|libre|
|[ayPb7Lg7AFYqIfzk.htm](npc-gallery-items/ayPb7Lg7AFYqIfzk.htm)|Sneak Attack|Attaque sournoise|officielle|
|[aYsvRHgZ6n5NX68c.htm](npc-gallery-items/aYsvRHgZ6n5NX68c.htm)|Trap Finder|Dénicheur de pièges|officielle|
|[Azm5VrtnHNvlTD1i.htm](npc-gallery-items/Azm5VrtnHNvlTD1i.htm)|Inspirational Presence|Présence inspirante|officielle|
|[b65fCKLd7xxZXc5w.htm](npc-gallery-items/b65fCKLd7xxZXc5w.htm)|Aquatic Predator|Prédateur aquatique|libre|
|[b6M0e68kzny77o5h.htm](npc-gallery-items/b6M0e68kzny77o5h.htm)|Underworld Lore|Connaissance de la pègre|officielle|
|[b8lQp0O8fNE08x0h.htm](npc-gallery-items/b8lQp0O8fNE08x0h.htm)|Kukri|Kukri|officielle|
|[BbtsWaLbyjDHtboq.htm](npc-gallery-items/BbtsWaLbyjDHtboq.htm)|Cleric Domain Spells|Sorts de domaine de prêtre|officielle|
|[BDewONWVQqYhfwRL.htm](npc-gallery-items/BDewONWVQqYhfwRL.htm)|Gavel|Maillet|officielle|
|[BE4Ax06mjpPipwKW.htm](npc-gallery-items/BE4Ax06mjpPipwKW.htm)|Sailing Lore|Connaissance de la navigation|officielle|
|[Bg7DMaLAy2rtjxkb.htm](npc-gallery-items/Bg7DMaLAy2rtjxkb.htm)|Composite Shortbow|Arc court composite|officielle|
|[BHcpl6Nlf6nVff26.htm](npc-gallery-items/BHcpl6Nlf6nVff26.htm)|Deadly Simplicity|Simplicité mortelle|officielle|
|[BIreghuAVYAZ0WoG.htm](npc-gallery-items/BIreghuAVYAZ0WoG.htm)|Medical Textbook|Livre médical|libre|
|[Bk9v9oLDZmh7ITsB.htm](npc-gallery-items/Bk9v9oLDZmh7ITsB.htm)|Construction Schematics|Plans de construction|libre|
|[blC0ZLHL8ZjzOyui.htm](npc-gallery-items/blC0ZLHL8ZjzOyui.htm)|Elixir of Life (Lesser) (Infused)|Élixir de vie inférieur (imprégné)|libre|
|[blPj44XSQdjiu1MS.htm](npc-gallery-items/blPj44XSQdjiu1MS.htm)|Fence's Eye|OEil du receleur|officielle|
|[BmfnMuD7K7AAnAI6.htm](npc-gallery-items/BmfnMuD7K7AAnAI6.htm)|Map|Carte|libre|
|[BpD2byfrbMItSgfy.htm](npc-gallery-items/BpD2byfrbMItSgfy.htm)|+15 to Sense Motive|+15 pour Deviner les Intentions|officielle|
|[bqpvez0u4Rw93KXo.htm](npc-gallery-items/bqpvez0u4Rw93KXo.htm)|Font of Knowledge|Source de connaissance|officielle|
|[BR4ZECJ9kO4oph8g.htm](npc-gallery-items/BR4ZECJ9kO4oph8g.htm)|Bloodline Magic|Magie de lignage|officielle|
|[Bre79vI4roX7eGSB.htm](npc-gallery-items/Bre79vI4roX7eGSB.htm)|Fist|Poing|officielle|
|[BRt0eTPvm8Gdvdd5.htm](npc-gallery-items/BRt0eTPvm8Gdvdd5.htm)|Bureaucracy Lore|Connaissance de la bureaucracie|officielle|
|[Bz5e5nzwVDHVigQ0.htm](npc-gallery-items/Bz5e5nzwVDHVigQ0.htm)|Dagger|Dague|officielle|
|[bzK8nvv2KV60Xe8s.htm](npc-gallery-items/bzK8nvv2KV60Xe8s.htm)|Scroll of Heal (Level 1)|Parchemin de Guérison (Niveau 1)|libre|
|[C0mzP5eUVDpwg5rI.htm](npc-gallery-items/C0mzP5eUVDpwg5rI.htm)|Legal Lore|Connaissance juridique|officielle|
|[C7PdppuFM1J5AfZc.htm](npc-gallery-items/C7PdppuFM1J5AfZc.htm)|Shiv|Surin|officielle|
|[C9BvM6sI4AN0SssQ.htm](npc-gallery-items/C9BvM6sI4AN0SssQ.htm)|Shovel|Pelle|officielle|
|[cah7DtpiMX62b2rT.htm](npc-gallery-items/cah7DtpiMX62b2rT.htm)|Longspear|Pique|officielle|
|[Cdbi86SZ0ydTRO91.htm](npc-gallery-items/Cdbi86SZ0ydTRO91.htm)|Underwater Lore|Connaissance sous-marine|officielle|
|[cHWzyBoiaRRRBq99.htm](npc-gallery-items/cHWzyBoiaRRRBq99.htm)|-2 to Will Saves vs. Higher Ranking Cult Members|-2 aux JdS de Volonté contre des membres du culte de rang supérieur|officielle|
|[CJp7VcMyI9Wp4QX0.htm](npc-gallery-items/CJp7VcMyI9Wp4QX0.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[cjVbggOxvEzXmIQm.htm](npc-gallery-items/cjVbggOxvEzXmIQm.htm)|Piton Pin|Planter de piton|officielle|
|[CK3FzTPn8aAFEwnB.htm](npc-gallery-items/CK3FzTPn8aAFEwnB.htm)|Scroll of Speak with Animals (Level 2)|Parchemin de Communication avec les animaux (Niveau 2)|libre|
|[CKzyNTs3y8kKsSFY.htm](npc-gallery-items/CKzyNTs3y8kKsSFY.htm)|Efficient Capture|Capture efficace|officielle|
|[CLyzEL754Aofobtz.htm](npc-gallery-items/CLyzEL754Aofobtz.htm)|Sailing Lore|Connaissance de la navigation|officielle|
|[Cmu4PgaVIciVRKDC.htm](npc-gallery-items/Cmu4PgaVIciVRKDC.htm)|Dagger|Dague|officielle|
|[CmUq3q1BL7VoIvyM.htm](npc-gallery-items/CmUq3q1BL7VoIvyM.htm)|Cutlass|Sabre d'abordage|officielle|
|[CmXRYDHVFD9zOTaa.htm](npc-gallery-items/CmXRYDHVFD9zOTaa.htm)|Dagger|Dague|officielle|
|[cMz56RGF3H0a0ZQi.htm](npc-gallery-items/cMz56RGF3H0a0ZQi.htm)|Quick Rummage|Bric-à-brac rapide|officielle|
|[CN4GwNgOryN6LNFo.htm](npc-gallery-items/CN4GwNgOryN6LNFo.htm)|Fist|Poing|officielle|
|[cnmmUfSvnBHbPJ3N.htm](npc-gallery-items/cnmmUfSvnBHbPJ3N.htm)|Bard Composition Spells|Sorts de composition de barde|officielle|
|[coq6krhuSNTUKuyB.htm](npc-gallery-items/coq6krhuSNTUKuyB.htm)|Deny Advantage|Refus d'avantage|officielle|
|[CqwxqwQxgNIp6UdX.htm](npc-gallery-items/CqwxqwQxgNIp6UdX.htm)|Staff Of Abjuration|Bâton d'abjuration|officielle|
|[CRo6e5ADHqXxRyBr.htm](npc-gallery-items/CRo6e5ADHqXxRyBr.htm)|Occult Spells Known|Sorts occultes connus|officielle|
|[csVpBw7aFwkZ7W2a.htm](npc-gallery-items/csVpBw7aFwkZ7W2a.htm)|Robes|Robes|libre|
|[Cu79oDJClJ9C9Reu.htm](npc-gallery-items/Cu79oDJClJ9C9Reu.htm)|Demon Lore|Connaissance des démons|officielle|
|[CUkJIaXcAVXlBYoH.htm](npc-gallery-items/CUkJIaXcAVXlBYoH.htm)|Smith's Fury|Fureur du forgeron|officielle|
|[cviTvCcO5isNFaU7.htm](npc-gallery-items/cviTvCcO5isNFaU7.htm)|Sickle|Serpe|officielle|
|[cxWmbPgxDBajNjqz.htm](npc-gallery-items/cxWmbPgxDBajNjqz.htm)|Scimitar|Cimeterre|officielle|
|[CY3VwzlwEsLmLp7q.htm](npc-gallery-items/CY3VwzlwEsLmLp7q.htm)|Dagger|Dague|officielle|
|[cYpzRpaINHeW2XxC.htm](npc-gallery-items/cYpzRpaINHeW2XxC.htm)|Poison Weapon|Arme empoisonnée|officielle|
|[cz2QOg6QSmJNA3YL.htm](npc-gallery-items/cz2QOg6QSmJNA3YL.htm)|Sway the Judge and Jury|Influencer le juge et le jury|officielle|
|[d1CLaCtFlghOslm4.htm](npc-gallery-items/d1CLaCtFlghOslm4.htm)|Religious Symbol of Nethys|Symbole religieux de Néthys|libre|
|[D91SDgzufetsXtHT.htm](npc-gallery-items/D91SDgzufetsXtHT.htm)|Acid Flask|Fiole d'acide|officielle|
|[d9TOZvyquMo6o18K.htm](npc-gallery-items/d9TOZvyquMo6o18K.htm)|Quick Bomber|Artificier rapide|officielle|
|[db0QJhDLMLOerNa6.htm](npc-gallery-items/db0QJhDLMLOerNa6.htm)|Channel Smite|Châtiment canalisé|officielle|
|[dbDMr7uWS78nPenZ.htm](npc-gallery-items/dbDMr7uWS78nPenZ.htm)|Dagger|Dague|officielle|
|[dCBZN8wCmO7ipEHO.htm](npc-gallery-items/dCBZN8wCmO7ipEHO.htm)|Dagger|Dague|officielle|
|[ddMOXx1g84tLNThI.htm](npc-gallery-items/ddMOXx1g84tLNThI.htm)|+1 Status to All Saves vs. Poison|bonus de statut de +1 aux JdS contre le poison|officielle|
|[DDRbAS5r8U0XMma7.htm](npc-gallery-items/DDRbAS5r8U0XMma7.htm)|Greataxe|Grande hache|officielle|
|[DgGWQZ2UfWzKedET.htm](npc-gallery-items/DgGWQZ2UfWzKedET.htm)|Composite Longbow|Arc long composite|officielle|
|[DipxFEdiddYHWirp.htm](npc-gallery-items/DipxFEdiddYHWirp.htm)|Bard Composition Spells|Sorts de composition de barde|officielle|
|[DJcssfmvEXxJU37q.htm](npc-gallery-items/DJcssfmvEXxJU37q.htm)|Poetry Book|Livre de poésie|libre|
|[DjeP7DBDv1Ue87eW.htm](npc-gallery-items/DjeP7DBDv1Ue87eW.htm)|Whip|Fouet|officielle|
|[dlhm2yJ4EUGym0CG.htm](npc-gallery-items/dlhm2yJ4EUGym0CG.htm)|Gang Up|Attaque groupée|officielle|
|[DNrmjXUT1oMnmx43.htm](npc-gallery-items/DNrmjXUT1oMnmx43.htm)|Pewter Mug|Gobelet en étain|libre|
|[drNAUPLqWo7QSh3l.htm](npc-gallery-items/drNAUPLqWo7QSh3l.htm)|Shielded Advance|Avancée au bouclier|officielle|
|[dSa8OQfOz63lYKQ2.htm](npc-gallery-items/dSa8OQfOz63lYKQ2.htm)|Versatile Performance|Polyvalence artistique|officielle|
|[DsXup6n3udnCKVoX.htm](npc-gallery-items/DsXup6n3udnCKVoX.htm)|Serving Tray|Plateau de service|libre|
|[dTwii5UOFOwMyY0C.htm](npc-gallery-items/dTwii5UOFOwMyY0C.htm)|Font of Gossip|Source de rumeur|officielle|
|[duBrf89grnmT2Qdn.htm](npc-gallery-items/duBrf89grnmT2Qdn.htm)|Engineering Lore|Connaissance de l'ingénierie|officielle|
|[DV9NQLrI4FouRCpe.htm](npc-gallery-items/DV9NQLrI4FouRCpe.htm)|Fist|Poing|officielle|
|[dYLk3rxvErYoaP9G.htm](npc-gallery-items/dYLk3rxvErYoaP9G.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[dYlvFC5F1daX6NF8.htm](npc-gallery-items/dYlvFC5F1daX6NF8.htm)|Shortsword|Épée courte|officielle|
|[DzrwPVv4yZGfqMNf.htm](npc-gallery-items/DzrwPVv4yZGfqMNf.htm)|Improved Communal Healing|Guérison collective améliorée|officielle|
|[e3i9M78EsoeT9Mu1.htm](npc-gallery-items/e3i9M78EsoeT9Mu1.htm)|Plague Lore|Connaissance de la peste|officielle|
|[E4oBlqhBaVqkShlq.htm](npc-gallery-items/E4oBlqhBaVqkShlq.htm)|Stealth|Discrétion|officielle|
|[eAaIdI0UJUrcmPzC.htm](npc-gallery-items/eAaIdI0UJUrcmPzC.htm)|Bar Brawler|Bagarreur de comptoir|officielle|
|[EbHva548irAoyT4c.htm](npc-gallery-items/EbHva548irAoyT4c.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[ebZ4RtDXFWOGqY2i.htm](npc-gallery-items/ebZ4RtDXFWOGqY2i.htm)|Rapier|Rapière|officielle|
|[ECBmark63yV59X2C.htm](npc-gallery-items/ECBmark63yV59X2C.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|officielle|
|[edMKJZQVDeLF48ZV.htm](npc-gallery-items/edMKJZQVDeLF48ZV.htm)|Reach Spell|Sort éloigné|officielle|
|[egP992KxbiFYLiUr.htm](npc-gallery-items/egP992KxbiFYLiUr.htm)|Fortune-Telling Lore|Connaissance de la voyance|officielle|
|[EHw3qV7vYGDQdfPB.htm](npc-gallery-items/EHw3qV7vYGDQdfPB.htm)|Sneak Attack|Attaque sournoise|officielle|
|[EMerfEpCLFxD8gAF.htm](npc-gallery-items/EMerfEpCLFxD8gAF.htm)|Staff|Bâton|officielle|
|[emTBNypG9xLkrGmv.htm](npc-gallery-items/emTBNypG9xLkrGmv.htm)|Greataxe|Grande hache|officielle|
|[EnayAcuVFebid6Yd.htm](npc-gallery-items/EnayAcuVFebid6Yd.htm)|Labor Lore|Connaissance du travail|officielle|
|[EozEU2EXtYF3roQx.htm](npc-gallery-items/EozEU2EXtYF3roQx.htm)|Spellbook|Grimoire|officielle|
|[eplixHfDeknEF97x.htm](npc-gallery-items/eplixHfDeknEF97x.htm)|Chameleon Step|Démarche du caméléon|officielle|
|[Eq3KajvZR0XWgw2w.htm](npc-gallery-items/Eq3KajvZR0XWgw2w.htm)|Rapier|Rapière|officielle|
|[erMgbyNlTUBjOhqo.htm](npc-gallery-items/erMgbyNlTUBjOhqo.htm)|Falchion|Cimeterre à deux mains|officielle|
|[EtuysfUGpQnBFAaU.htm](npc-gallery-items/EtuysfUGpQnBFAaU.htm)|Rapier|Rapière|officielle|
|[Ew9YdRtlJy7JUSHa.htm](npc-gallery-items/Ew9YdRtlJy7JUSHa.htm)|Fist|Poing|officielle|
|[EYXqnzW1WGw1aWCa.htm](npc-gallery-items/EYXqnzW1WGw1aWCa.htm)|Longsword|+1|Épée longue +1|libre|
|[ezgpbQSl4o3vFJaU.htm](npc-gallery-items/ezgpbQSl4o3vFJaU.htm)|Arcane Prepared Spells|Sorts arcaniques préparés|officielle|
|[F0Yrpyr0NczIzhGY.htm](npc-gallery-items/F0Yrpyr0NczIzhGY.htm)|Steady Balance|Équilibre stable|officielle|
|[f6BZlUvuAkJjyx2T.htm](npc-gallery-items/f6BZlUvuAkJjyx2T.htm)|Vengeful Edge|Avantage vindicatif|officielle|
|[F7IHKiavOdLoatuo.htm](npc-gallery-items/F7IHKiavOdLoatuo.htm)|Doctor's Hand|Main du médecin|officielle|
|[f9317vgvLL2FaL7N.htm](npc-gallery-items/f9317vgvLL2FaL7N.htm)|Fortitude Saves|Jets de Vigueur|officielle|
|[F9lM0H0vX25rlQiB.htm](npc-gallery-items/F9lM0H0vX25rlQiB.htm)|Sworn Duty|Dévoué|officielle|
|[f9Y5nPNe31oqVE1V.htm](npc-gallery-items/f9Y5nPNe31oqVE1V.htm)|Kukri|+1|Kukri +1|libre|
|[FDgG3VBojXvvTpd3.htm](npc-gallery-items/FDgG3VBojXvvTpd3.htm)|Darkvision|Vision dans le noir|officielle|
|[FFYi0YoImNuoninN.htm](npc-gallery-items/FFYi0YoImNuoninN.htm)|Underworld Lore|Connaissance de la pègre|officielle|
|[fGw7sipW1eI40byQ.htm](npc-gallery-items/fGw7sipW1eI40byQ.htm)|Dagger|Dague|officielle|
|[FkNhevhHp4HfH05a.htm](npc-gallery-items/FkNhevhHp4HfH05a.htm)|Low-Light Vision|Vision nocturne|officielle|
|[flLLoUGHSVJire4L.htm](npc-gallery-items/flLLoUGHSVJire4L.htm)|Sorcerer Bloodline Spells|Sorts de lignage d'ensorceleur|officielle|
|[FMRFRcuXn2QHFX2v.htm](npc-gallery-items/FMRFRcuXn2QHFX2v.htm)|Bravery|Bravoure|officielle|
|[FmriI9fZfj3I2QS1.htm](npc-gallery-items/FmriI9fZfj3I2QS1.htm)|Swig|Rasade|officielle|
|[FMXvHmfCarIbLAk5.htm](npc-gallery-items/FMXvHmfCarIbLAk5.htm)|Broom|Balais|officielle|
|[fp3XmyB0pcpjCBex.htm](npc-gallery-items/fp3XmyB0pcpjCBex.htm)|Nimble Dodge|Esquive agile|officielle|
|[FQO3E18nwnNURI9p.htm](npc-gallery-items/FQO3E18nwnNURI9p.htm)|Universal Obedience|Obéissance universelle|officielle|
|[fSa7yGc6AlAoFW23.htm](npc-gallery-items/fSa7yGc6AlAoFW23.htm)|Composite Longbow|Arc long composite|officielle|
|[fTKnuO5HCLkBEtPH.htm](npc-gallery-items/fTKnuO5HCLkBEtPH.htm)|Performance|Représentation|officielle|
|[FuOqFNDYZSFUVtPi.htm](npc-gallery-items/FuOqFNDYZSFUVtPi.htm)|Cutlery|Couverts|officielle|
|[fwJUsC1cyxsHPo94.htm](npc-gallery-items/fwJUsC1cyxsHPo94.htm)|Architecture Lore|Connaissance de l'architecture|officielle|
|[fWy0XIMLXO8QjFJn.htm](npc-gallery-items/fWy0XIMLXO8QjFJn.htm)|Divine Prepared Spells|Sorts divins préparés|officielle|
|[FXduVQD4iGJxgXX2.htm](npc-gallery-items/FXduVQD4iGJxgXX2.htm)|Naval Pike|Pique d'abordage|officielle|
|[G4RkhwmVzzHUmIqk.htm](npc-gallery-items/G4RkhwmVzzHUmIqk.htm)|Club|Gourdin|officielle|
|[G5iBcxxzOecJApLi.htm](npc-gallery-items/G5iBcxxzOecJApLi.htm)|Crossbow|Arbalète|officielle|
|[G7t10rKHu8TabnQz.htm](npc-gallery-items/G7t10rKHu8TabnQz.htm)|Theatre Lore|Connaissance théâtrale|officielle|
|[geD4pfraSGQRXXvE.htm](npc-gallery-items/geD4pfraSGQRXXvE.htm)|+1 Status to All Saves vs. Fear|bonus de statut de +1 aux JdS contre la peur|officielle|
|[Gel19EmMnTqnNt87.htm](npc-gallery-items/Gel19EmMnTqnNt87.htm)|Legal Lore|Connaissance juridique|officielle|
|[Gf4bCH3UInm7pjOu.htm](npc-gallery-items/Gf4bCH3UInm7pjOu.htm)|Intimidating Strike|Frappe intimidante|officielle|
|[ggCUTswEGOjGvjj0.htm](npc-gallery-items/ggCUTswEGOjGvjj0.htm)|Sudden Charge|Charge soudaine|officielle|
|[GhbdiBXGiH9AN1pw.htm](npc-gallery-items/GhbdiBXGiH9AN1pw.htm)|Staff|Bâton|officielle|
|[ghFGvItVlZL7x9SC.htm](npc-gallery-items/ghFGvItVlZL7x9SC.htm)|Hand Crossbow|Arbalète de poing|officielle|
|[gHkZ5EkRCbXagaxl.htm](npc-gallery-items/gHkZ5EkRCbXagaxl.htm)|Drunken Rage|Rage d'ivrogne|officielle|
|[GICVztYSUDv0IIkz.htm](npc-gallery-items/GICVztYSUDv0IIkz.htm)|Cat Fall|Chute féline|officielle|
|[GmvgnOPx8qubo9xm.htm](npc-gallery-items/GmvgnOPx8qubo9xm.htm)|Advancing Flourish|Avancée et moulinets|officielle|
|[groFZoIcMCSJcNZO.htm](npc-gallery-items/groFZoIcMCSJcNZO.htm)|+2 Circumstance to All Saves vs. Dream and Sleep|Bonus de circonstances de +2 aux JdS contre le sommeil et le poison|officielle|
|[gUoaHaSoU57Z7C4z.htm](npc-gallery-items/gUoaHaSoU57Z7C4z.htm)|Underworld Lore|Connaissance de la pègre|officielle|
|[GvFkJmDJIJRneI79.htm](npc-gallery-items/GvFkJmDJIJRneI79.htm)|Primal Prepared Spells|Sorts primordiaux préparés|officielle|
|[gwyxM0numx6kL7On.htm](npc-gallery-items/gwyxM0numx6kL7On.htm)|Mercantile Lore|Connaissance commerciale|officielle|
|[GxVV9OrtL5eoSx2b.htm](npc-gallery-items/GxVV9OrtL5eoSx2b.htm)|Forager|Glaneur|officielle|
|[gzBR3N7gO6KPegiv.htm](npc-gallery-items/gzBR3N7gO6KPegiv.htm)|Mask Bond|Masque lié|officielle|
|[h0OhlZfKQJOrUwr1.htm](npc-gallery-items/h0OhlZfKQJOrUwr1.htm)|Fishing Lore|Connaissance de la pêche|officielle|
|[h0oO1nc9v9yl5OgB.htm](npc-gallery-items/h0oO1nc9v9yl5OgB.htm)|Lip Reader|Lecture labiale|officielle|
|[h15HeWZxI5S8VAFk.htm](npc-gallery-items/h15HeWZxI5S8VAFk.htm)|Law Book|Livre de loi|libre|
|[h1SgfPkIaJazPwYz.htm](npc-gallery-items/h1SgfPkIaJazPwYz.htm)|Glittering Distraction|Distraction monétaire|officielle|
|[h3tZcwvLExRJno4u.htm](npc-gallery-items/h3tZcwvLExRJno4u.htm)|Scroll of Acid Arrow (Level 2)|Parchemin de flèche d'acide (Niveau 2)|libre|
|[H8PMiZFsy2MQfLN2.htm](npc-gallery-items/H8PMiZFsy2MQfLN2.htm)|Gather Converts|Rassembler les fidèles|officielle|
|[H91fpqLmPSX7S8ow.htm](npc-gallery-items/H91fpqLmPSX7S8ow.htm)|Longspear|+1|Pique +1|libre|
|[HBgAXRPFxVlqiDZ8.htm](npc-gallery-items/HBgAXRPFxVlqiDZ8.htm)|Shortbow|Arc court|officielle|
|[hc2576PgevK5i1qc.htm](npc-gallery-items/hc2576PgevK5i1qc.htm)|Pitch Bale|Enfourcher|officielle|
|[HDAF4Nls9WJAWOeR.htm](npc-gallery-items/HDAF4Nls9WJAWOeR.htm)|Submerged Stealth|Immersion discrète|libre|
|[hF5pPWi7gLBNBaO0.htm](npc-gallery-items/hF5pPWi7gLBNBaO0.htm)|Warfare Lore|Connaissance de la guerre|officielle|
|[hgkI7oRfyfC00JbV.htm](npc-gallery-items/hgkI7oRfyfC00JbV.htm)|Keyring|Anneau de clés|libre|
|[hHCP08kAOR4XgVJL.htm](npc-gallery-items/hHCP08kAOR4XgVJL.htm)|Hood|Capuchon|libre|
|[hHng07DmszKk5pNt.htm](npc-gallery-items/hHng07DmszKk5pNt.htm)|Crossbow|Arbalète|officielle|
|[hjekwptdXb14cWSj.htm](npc-gallery-items/hjekwptdXb14cWSj.htm)|Bravery|Bravoure|officielle|
|[HnDaKsWEBEBi5eJX.htm](npc-gallery-items/HnDaKsWEBEBi5eJX.htm)|Sneak Attack|Attaque sournoise|officielle|
|[hoNnL8BsPzPuGERZ.htm](npc-gallery-items/hoNnL8BsPzPuGERZ.htm)|Swinging Strike|Frappe en balancier|officielle|
|[HpLKljvtfXk5Em41.htm](npc-gallery-items/HpLKljvtfXk5Em41.htm)|Book|Livre|officielle|
|[hpPQrfATIi4fXq3L.htm](npc-gallery-items/hpPQrfATIi4fXq3L.htm)|Cane|Canne|officielle|
|[Hrp3jkhyAGyCrwfW.htm](npc-gallery-items/Hrp3jkhyAGyCrwfW.htm)|Force Body|Corps de force|libre|
|[Ht7knXtXeyTkKi2n.htm](npc-gallery-items/Ht7knXtXeyTkKi2n.htm)|Evasion|Évasion|officielle|
|[HuqsExeC8DG7dt5Z.htm](npc-gallery-items/HuqsExeC8DG7dt5Z.htm)|Barkeep's Apron|Tablier de barman|libre|
|[Hw1KYT6hIaAHcXkI.htm](npc-gallery-items/Hw1KYT6hIaAHcXkI.htm)|Navigator's Edge|Avantage du navigateur|officielle|
|[HXCEPfKkb2TP6NVy.htm](npc-gallery-items/HXCEPfKkb2TP6NVy.htm)|Bravery|Bravoure|officielle|
|[hYh1jMda40LahOdv.htm](npc-gallery-items/hYh1jMda40LahOdv.htm)|Sickle|Serpe|officielle|
|[hyLN8l8Z2WSxFAXC.htm](npc-gallery-items/hyLN8l8Z2WSxFAXC.htm)|Running Reload|Rechargement en courant|officielle|
|[i3KBgRUcT11VAT2c.htm](npc-gallery-items/i3KBgRUcT11VAT2c.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[I6H2KqzFAWfPj0ZV.htm](npc-gallery-items/I6H2KqzFAWfPj0ZV.htm)|Cane|Canne|libre|
|[i6RGiBt0UOTBg3go.htm](npc-gallery-items/i6RGiBt0UOTBg3go.htm)|Club|Gourdin|officielle|
|[IAFE4UVaYTfZgp3J.htm](npc-gallery-items/IAFE4UVaYTfZgp3J.htm)|Fist|Poing|officielle|
|[IbjfdrjbLUv06vFD.htm](npc-gallery-items/IbjfdrjbLUv06vFD.htm)|Rapier|+1|Rapière +1|libre|
|[iCIavuhoO4Uam2Vj.htm](npc-gallery-items/iCIavuhoO4Uam2Vj.htm)|Shovel|Pelle|libre|
|[icyjQtX7NGtgTdJj.htm](npc-gallery-items/icyjQtX7NGtgTdJj.htm)|Claw|Griffe|officielle|
|[iDsCIJ7Gzk9ENl2h.htm](npc-gallery-items/iDsCIJ7Gzk9ENl2h.htm)|Legal Lore|Connaissance juridique|officielle|
|[IEibCXh97z4dVqH9.htm](npc-gallery-items/IEibCXh97z4dVqH9.htm)|Bandit's Ambush|Embuscade du bandit|officielle|
|[ifnnMl3PxViiITbo.htm](npc-gallery-items/ifnnMl3PxViiITbo.htm)|Local Court Lore|Connaissance de la cour locale|officielle|
|[iGam6RcKFAl2V9TI.htm](npc-gallery-items/iGam6RcKFAl2V9TI.htm)|Silver Flask|Fiole en argent|libre|
|[ilIEY861r4bkrSfL.htm](npc-gallery-items/ilIEY861r4bkrSfL.htm)|Miner's Harness|Baudrier de mineur|libre|
|[ImjzyAwR4eEDXf0q.htm](npc-gallery-items/ImjzyAwR4eEDXf0q.htm)|Cultist Garb|Tenue de cultiste|libre|
|[ImzpAPq1SgMcf7t3.htm](npc-gallery-items/ImzpAPq1SgMcf7t3.htm)|Hunt Prey|Chasser une proie|officielle|
|[InRtCxbhS6HJi1kR.htm](npc-gallery-items/InRtCxbhS6HJi1kR.htm)|Greataxe|+1|Grande hache +1|libre|
|[ioBhBqoLt4XX6CZA.htm](npc-gallery-items/ioBhBqoLt4XX6CZA.htm)|Naval Pike|Pique d'abordage|libre|
|[ipz89wB16JTSERLW.htm](npc-gallery-items/ipz89wB16JTSERLW.htm)|Journal|Journal|libre|
|[iQWAwtvFxRVGqW0G.htm](npc-gallery-items/iQWAwtvFxRVGqW0G.htm)|Journal|Journal|officielle|
|[ItJWnelnPHXRTb86.htm](npc-gallery-items/ItJWnelnPHXRTb86.htm)|Cleric Domain Spells|Sorts de domaine de prêtre|officielle|
|[iUTM9WOzUZIkVt1D.htm](npc-gallery-items/iUTM9WOzUZIkVt1D.htm)|Hand Crossbow|Arbalète de poing|officielle|
|[IWfm1LwDDnDCkgUi.htm](npc-gallery-items/IWfm1LwDDnDCkgUi.htm)|Fist|Poing|officielle|
|[iWiQjH6UZGPhap8G.htm](npc-gallery-items/iWiQjH6UZGPhap8G.htm)|Intimidating Strike|Frappe intimidante|officielle|
|[Iwy3TBA4Dhrb3256.htm](npc-gallery-items/Iwy3TBA4Dhrb3256.htm)|Composite Longbow|Arc long composite|officielle|
|[IWzZhxjRQbZJM9bU.htm](npc-gallery-items/IWzZhxjRQbZJM9bU.htm)|+3 Status to Reflex vs. Damaging Effects|+3 de statut aux JdS de Réflexes contre les effets de dégât|officielle|
|[iYZVob3Hya2x788S.htm](npc-gallery-items/iYZVob3Hya2x788S.htm)|Spell Component Pouch|Bourse de composantes de sorts|libre|
|[J0gZtGInMyuCyCuB.htm](npc-gallery-items/J0gZtGInMyuCyCuB.htm)|Longsword|Épée longue|officielle|
|[j1wf4dSXkGypJjRs.htm](npc-gallery-items/j1wf4dSXkGypJjRs.htm)|Sneak Attack|Attaque sournoise|officielle|
|[J2jz17JAQtHVQs5I.htm](npc-gallery-items/J2jz17JAQtHVQs5I.htm)|Sneak Attack|Attaque sournoise|officielle|
|[J6LhLBjnU1kyUhwK.htm](npc-gallery-items/J6LhLBjnU1kyUhwK.htm)|Shield Ally|Allié bouclier|officielle|
|[Ja5JnCCeUp0qanDJ.htm](npc-gallery-items/Ja5JnCCeUp0qanDJ.htm)|Apple|Pomme|libre|
|[Ja5QiBYnwlN8lKXg.htm](npc-gallery-items/Ja5QiBYnwlN8lKXg.htm)|Composite Shortbow|Arc court composite|officielle|
|[jbBEFIXHqLEFkuxj.htm](npc-gallery-items/jbBEFIXHqLEFkuxj.htm)|Sneak Attack|Attaque sournoise|officielle|
|[JbbyThxk68mjKgkw.htm](npc-gallery-items/JbbyThxk68mjKgkw.htm)|Fist|Poing|officielle|
|[JbPgkPBw6I7dZFcQ.htm](npc-gallery-items/JbPgkPBw6I7dZFcQ.htm)|Scalpel|Scalpel|officielle|
|[JCFQaCMK8sKMFgL2.htm](npc-gallery-items/JCFQaCMK8sKMFgL2.htm)|Cleric Domain Spells|Sorts de domaine de prêtre|officielle|
|[Jd088q8hRZquUT6t.htm](npc-gallery-items/Jd088q8hRZquUT6t.htm)|Scribing Lore|Connaissance du métier de scribe|officielle|
|[jHMJPUtmDcCC20ND.htm](npc-gallery-items/jHMJPUtmDcCC20ND.htm)|Crossbow|Arbalète|officielle|
|[jhzTyAeLv0K1sK7c.htm](npc-gallery-items/jhzTyAeLv0K1sK7c.htm)|Book|Livre|officielle|
|[JLeDsdQFNsR2XCIX.htm](npc-gallery-items/JLeDsdQFNsR2XCIX.htm)|Torch|Torche|officielle|
|[jMBs4PyC1EahZHlA.htm](npc-gallery-items/jMBs4PyC1EahZHlA.htm)|Sack with 5 Rocks|Sac contenant 5 rochers|libre|
|[jMsBYZOEyBIRK1en.htm](npc-gallery-items/jMsBYZOEyBIRK1en.htm)|Sickle|+1|Serpe +1|libre|
|[jr5nhXsMkIwlhAOa.htm](npc-gallery-items/jr5nhXsMkIwlhAOa.htm)|Moderate Frost Vial|Fioles de givre modérées|officielle|
|[JtEtXdQY26QW7Yh7.htm](npc-gallery-items/JtEtXdQY26QW7Yh7.htm)|Pewter Mug|Gobelet en étain|libre|
|[JUbm5Xf0IURQGSTX.htm](npc-gallery-items/JUbm5Xf0IURQGSTX.htm)|Ledger|Registre|officielle|
|[JVQnk3tWMv7fuQax.htm](npc-gallery-items/JVQnk3tWMv7fuQax.htm)|Fist|Poing|officielle|
|[KbYGZddUNQGhrhEf.htm](npc-gallery-items/KbYGZddUNQGhrhEf.htm)|Composite Longbow|Arc long composite|officielle|
|[kcbRLFttvvktolJn.htm](npc-gallery-items/kcbRLFttvvktolJn.htm)|Hydration|Hydratation|libre|
|[kcXoIhefcYSfr7CL.htm](npc-gallery-items/kcXoIhefcYSfr7CL.htm)|Guide's Warning|Avertissement du guide|officielle|
|[KdjtCPQTvuJzzmNN.htm](npc-gallery-items/KdjtCPQTvuJzzmNN.htm)|Crossbow|Arbalète|officielle|
|[KDV6e31ms8EioLrR.htm](npc-gallery-items/KDV6e31ms8EioLrR.htm)|Dagger|Dague|officielle|
|[keHqnY2IJOfiVKRj.htm](npc-gallery-items/keHqnY2IJOfiVKRj.htm)|Rapier|Rapière|officielle|
|[KhEHpoNP1iXOpths.htm](npc-gallery-items/KhEHpoNP1iXOpths.htm)|Club|Gourdin|officielle|
|[kotwyraYf9vVKqEY.htm](npc-gallery-items/kotwyraYf9vVKqEY.htm)|Fascinating Dance|Danse fascinante|officielle|
|[kPjRjJyV9Xs4FsAB.htm](npc-gallery-items/kPjRjJyV9Xs4FsAB.htm)|Arcane Prepared Spells|Sorts arcaniques préparés|officielle|
|[krDLK9NYOYjDsMUZ.htm](npc-gallery-items/krDLK9NYOYjDsMUZ.htm)|Underworld Lore|Connaissance de la pègre|officielle|
|[KRLpKVZ3AZwkE4cj.htm](npc-gallery-items/KRLpKVZ3AZwkE4cj.htm)|+2 Status to Perception to Find Traps|Bonus de statut de +2 à la Perception pour trouver des pièges|officielle|
|[kSd8rIlnXiVczYg0.htm](npc-gallery-items/kSd8rIlnXiVczYg0.htm)|Deny Advantage|Refus d'avantage|officielle|
|[KT37rRaRxFL95Tl3.htm](npc-gallery-items/KT37rRaRxFL95Tl3.htm)|Work Coat|Tablier de travail|libre|
|[KuVkUyVf2udJz4xG.htm](npc-gallery-items/KuVkUyVf2udJz4xG.htm)|Bardic Lore|Connaissance bardique|officielle|
|[kx6tL1KbenrQWtCB.htm](npc-gallery-items/kx6tL1KbenrQWtCB.htm)|Composite Longbow|Arc long composite|officielle|
|[kzNiR7bLBxL4aWfR.htm](npc-gallery-items/kzNiR7bLBxL4aWfR.htm)|Quick Draw|Arme en main|officielle|
|[L2NMWuwie5fk4t5D.htm](npc-gallery-items/L2NMWuwie5fk4t5D.htm)|Beat a Retreat|Battre en retraite|officielle|
|[l57vO8xlVLGunBbi.htm](npc-gallery-items/l57vO8xlVLGunBbi.htm)|Composite Longbow|+1|Arc long composite +1|officielle|
|[Laex9xerP0tAva7M.htm](npc-gallery-items/Laex9xerP0tAva7M.htm)|Surprise Attack|Attaque surprise|officielle|
|[LbKXY3iqGS2l93BZ.htm](npc-gallery-items/LbKXY3iqGS2l93BZ.htm)|Fist|Poing|officielle|
|[lC7KFntiTwd1FRo7.htm](npc-gallery-items/lC7KFntiTwd1FRo7.htm)|Pickpocket|Vol à la tire|officielle|
|[ld6AdRlIeDnctV9m.htm](npc-gallery-items/ld6AdRlIeDnctV9m.htm)|Sneak Attack|Attaque sournoise|officielle|
|[ldL413k00pgvZnif.htm](npc-gallery-items/ldL413k00pgvZnif.htm)|Spellbook (Fiendish Hypotheses and Protections from Same)|Grimoire (Hypothèses fiélonnes et Protections des mêmes)|libre|
|[LEi8MMikbUZD1avz.htm](npc-gallery-items/LEi8MMikbUZD1avz.htm)|Shield Block|Blocage au bouclier|officielle|
|[LGdgJYiYY4gmLKOx.htm](npc-gallery-items/LGdgJYiYY4gmLKOx.htm)|Surprise Attack|Attaque surprise|officielle|
|[lh7skf3SdSowZIzD.htm](npc-gallery-items/lh7skf3SdSowZIzD.htm)|Empty Bottle|Bouteille vide|libre|
|[ljSnticrrCWLH49B.htm](npc-gallery-items/ljSnticrrCWLH49B.htm)|Ceremonial Robes|Robes de cérémonie|libre|
|[lKG5nFZzi5NYvRU3.htm](npc-gallery-items/lKG5nFZzi5NYvRU3.htm)|Dagger|Dague|officielle|
|[ll3LaxvkACnszsxH.htm](npc-gallery-items/ll3LaxvkACnszsxH.htm)|Live to Tell the Tale|Survivre pour tout raconter|officielle|
|[LMckIZmrAQt7HWW3.htm](npc-gallery-items/LMckIZmrAQt7HWW3.htm)|Unstable Compounds|Matières instables|officielle|
|[lo1kjTX2r4gJJ3gT.htm](npc-gallery-items/lo1kjTX2r4gJJ3gT.htm)|Light Hammer|+1,striking|Marteau de guerre léger de frappe +1|libre|
|[lO3CMWjs9XkP1KVX.htm](npc-gallery-items/lO3CMWjs9XkP1KVX.htm)|Crafting|Artisanat|officielle|
|[lpMiICSGm7J1NIMh.htm](npc-gallery-items/lpMiICSGm7J1NIMh.htm)|Snare Crafting|Fabrication de pièges artisanaux|officielle|
|[lqVfEZu6MeCHDuky.htm](npc-gallery-items/lqVfEZu6MeCHDuky.htm)|Lore (any one related to their trade)|Connaissance (une en rapport avec sa profession)|officielle|
|[lrHNxw2aet6y4AAg.htm](npc-gallery-items/lrHNxw2aet6y4AAg.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[LtLpVRWe7Qsb7Y8A.htm](npc-gallery-items/LtLpVRWe7Qsb7Y8A.htm)|Bodyguard's Defense|Défense du garde du corps|officielle|
|[lTt5I810cFQrBGBC.htm](npc-gallery-items/lTt5I810cFQrBGBC.htm)|Arcane Focus Spells|Sorts arcaniques focalisés|officielle|
|[LvkfpNccs65oqyIV.htm](npc-gallery-items/LvkfpNccs65oqyIV.htm)|Precision Edge|Précision avantageuse|officielle|
|[lWk7EJdUr0IOisEA.htm](npc-gallery-items/lWk7EJdUr0IOisEA.htm)|Composite Longbow|+1,striking|Arc long composite de frappe +1|officielle|
|[lY3x1rCfroxrjphs.htm](npc-gallery-items/lY3x1rCfroxrjphs.htm)|Thievery|Vol|officielle|
|[LzDRsXeV3aAZe1AF.htm](npc-gallery-items/LzDRsXeV3aAZe1AF.htm)|Cleric Domain Spells|Sorts de domaine de prêtre|officielle|
|[M6mU2n4cS5QZRHvV.htm](npc-gallery-items/M6mU2n4cS5QZRHvV.htm)|Steady Spellcasting|Incantation fiable|officielle|
|[m9he0rG2ytGNLtE9.htm](npc-gallery-items/m9he0rG2ytGNLtE9.htm)|Apprentice's Ambition|Ambition de l'apprenti|officielle|
|[ma0kNlN1eN2YPKXO.htm](npc-gallery-items/ma0kNlN1eN2YPKXO.htm)|Heft Crate|Soulever une caisse|officielle|
|[McH1gJqQMQRVSagF.htm](npc-gallery-items/McH1gJqQMQRVSagF.htm)|Pewter Mug|Gobelet en étain|officielle|
|[Mcqi9fIHy5a1ffpL.htm](npc-gallery-items/Mcqi9fIHy5a1ffpL.htm)|Rapier|+1|Rapière +1|libre|
|[mCWRIeeB6PFExTmR.htm](npc-gallery-items/mCWRIeeB6PFExTmR.htm)|Hunt Prey|Chasser une proie|officielle|
|[MePUvMSpXtRD7gNJ.htm](npc-gallery-items/MePUvMSpXtRD7gNJ.htm)|Flail|Fléau d'armes|officielle|
|[MGSxc2vgOsjBpioT.htm](npc-gallery-items/MGSxc2vgOsjBpioT.htm)|Guildmaster's Uniform|Uniforme de Maître de guilde|libre|
|[mgVHJtmU3oAVM8wL.htm](npc-gallery-items/mgVHJtmU3oAVM8wL.htm)|Wizard School Spells|Sorts d'école de magicien|officielle|
|[Mhb7rNWIF3ipsvBr.htm](npc-gallery-items/Mhb7rNWIF3ipsvBr.htm)|Scroll Case|Étui à parchemins|officielle|
|[mHiTwtQ3th4qU0Um.htm](npc-gallery-items/mHiTwtQ3th4qU0Um.htm)|Hell Lore|Connaissance de l'Enfer|officielle|
|[Mi0QstLGjxyJGIa5.htm](npc-gallery-items/Mi0QstLGjxyJGIa5.htm)|+1 Clothing (Explorer's)|Vêtements d'explorateur +1|officielle|
|[mjTbb3MZMBpSS8o5.htm](npc-gallery-items/mjTbb3MZMBpSS8o5.htm)|Bastard Sword|+1,striking|Épée bâtarde de frappe +1|libre|
|[MKdjcAmWOXZkk2Pu.htm](npc-gallery-items/MKdjcAmWOXZkk2Pu.htm)|Catch Rock|Interception de rochers|officielle|
|[mKFp9wl5UZg1QAXE.htm](npc-gallery-items/mKFp9wl5UZg1QAXE.htm)|Staff|+1|Bâton +1|libre|
|[MLCrUvDlugE5BKqe.htm](npc-gallery-items/MLCrUvDlugE5BKqe.htm)|Staff|Bâton|officielle|
|[Mlr3n6w8aB9t1a5K.htm](npc-gallery-items/Mlr3n6w8aB9t1a5K.htm)|Infused Items|Objets imprégnés|officielle|
|[MlZNOt3aiHAeXqiD.htm](npc-gallery-items/MlZNOt3aiHAeXqiD.htm)|Sap|Matraque|officielle|
|[MO84pY6jx8r53gTb.htm](npc-gallery-items/MO84pY6jx8r53gTb.htm)|Serving Platter|Plateau de service|libre|
|[mOaqXByjpJOQroyH.htm](npc-gallery-items/mOaqXByjpJOQroyH.htm)|Timely Advice|Conseil opportun|officielle|
|[mOC9pnzLIeqAQ8i0.htm](npc-gallery-items/mOC9pnzLIeqAQ8i0.htm)|Religious Symbol of Pharasma|Symbole religieux de Pharasma|libre|
|[MpZVk32vk8f54dFz.htm](npc-gallery-items/MpZVk32vk8f54dFz.htm)|Crossbow|Arbalète|officielle|
|[mtGgMNRGtkuPRFCN.htm](npc-gallery-items/mtGgMNRGtkuPRFCN.htm)|Longsword|Épée longue|officielle|
|[muJs58oNvKAXYuXV.htm](npc-gallery-items/muJs58oNvKAXYuXV.htm)|Hatchet|Hachette|officielle|
|[MW2HziWk52XbjMte.htm](npc-gallery-items/MW2HziWk52XbjMte.htm)|Cutlass|Sabre d'abordage|libre|
|[Mz1hKhGITzmi33Pu.htm](npc-gallery-items/Mz1hKhGITzmi33Pu.htm)|Assorted Maps|Diverses cartes|libre|
|[n1RVqltrSIC94q8w.htm](npc-gallery-items/n1RVqltrSIC94q8w.htm)|Engineering Lore|Connaissance de l'ingénierie|officielle|
|[N2HV6ZqTWrwK3PxJ.htm](npc-gallery-items/N2HV6ZqTWrwK3PxJ.htm)|Gavel|Maillet|officielle|
|[N6AEPmgihlaE1PZT.htm](npc-gallery-items/N6AEPmgihlaE1PZT.htm)|Astronomy Lore|Connaissance de l'astronomie|officielle|
|[n6mv4Y0jGccZlwmK.htm](npc-gallery-items/n6mv4Y0jGccZlwmK.htm)|Signet Ring|Chevalière|libre|
|[N85njvyWLxpngIQ4.htm](npc-gallery-items/N85njvyWLxpngIQ4.htm)|Druid Order Spells|Sorts d'ordre de druide|officielle|
|[naPxWIBQxbkOgD8M.htm](npc-gallery-items/naPxWIBQxbkOgD8M.htm)|Breach the Abyss|Brèche dans l'Abysse|officielle|
|[NaQSwHuqBJmhgld8.htm](npc-gallery-items/NaQSwHuqBJmhgld8.htm)|Primal Prepared Spells|Sorts primordiaux préparés|officielle|
|[NfP2dKgjJXtvbtmr.htm](npc-gallery-items/NfP2dKgjJXtvbtmr.htm)|Mining Lore|Connaissance minière|officielle|
|[nFxNDxHPYmc47Mlx.htm](npc-gallery-items/nFxNDxHPYmc47Mlx.htm)|Dagger|Dague|officielle|
|[nFYahNWZUkU0gnJ9.htm](npc-gallery-items/nFYahNWZUkU0gnJ9.htm)|Hatchet|Hachette|officielle|
|[nhdJ9mXQ9NXj6n7V.htm](npc-gallery-items/nhdJ9mXQ9NXj6n7V.htm)|+2 Status to Reflex Saves vs. Traps|Bonus de statut de +2 aux jets de Réflexes contre les pièges|officielle|
|[NjMo70hHW4elTqEW.htm](npc-gallery-items/NjMo70hHW4elTqEW.htm)|Rapier|Rapière|officielle|
|[NKmPaEo138UaRnmO.htm](npc-gallery-items/NKmPaEo138UaRnmO.htm)|Experienced Hand|Avantage de l'expérience|officielle|
|[NLeDblkhTEvLGjwy.htm](npc-gallery-items/NLeDblkhTEvLGjwy.htm)|Reflex Saves|Jet de sauvegarde Réflexes|officielle|
|[nN82jRXcz7hgkXiE.htm](npc-gallery-items/nN82jRXcz7hgkXiE.htm)|Dagger|Dague|officielle|
|[NQR9pSqVCQqJO0b7.htm](npc-gallery-items/NQR9pSqVCQqJO0b7.htm)|Bottle|Bouteille|officielle|
|[nu42JIeBWdi2MnrN.htm](npc-gallery-items/nu42JIeBWdi2MnrN.htm)|Divine Prepared Spells|Sorts divins préparés|officielle|
|[NUzbNVXkKMop7XIE.htm](npc-gallery-items/NUzbNVXkKMop7XIE.htm)|Dagger|Dague|officielle|
|[nV2KEb0hxYozbDOL.htm](npc-gallery-items/nV2KEb0hxYozbDOL.htm)|Weapon Mastery|Maîtrise martiale|officielle|
|[Nv6NVuyp91007LQt.htm](npc-gallery-items/Nv6NVuyp91007LQt.htm)|Loaded Dice|Dé lesté|libre|
|[nwfcLkf39CUdTMCe.htm](npc-gallery-items/nwfcLkf39CUdTMCe.htm)|Manifesto|Manifeste|libre|
|[nXKjhXyXe4UIUDqy.htm](npc-gallery-items/nXKjhXyXe4UIUDqy.htm)|Crossbow|+1|Arbalète +1|libre|
|[O3WnGGds29lL8Ajd.htm](npc-gallery-items/O3WnGGds29lL8Ajd.htm)|Small Harp|Petite harpe|libre|
|[O4GUtfCmOYOhNgf2.htm](npc-gallery-items/O4GUtfCmOYOhNgf2.htm)|Dagger|Dague|officielle|
|[o4oz661UQlAOVWiz.htm](npc-gallery-items/o4oz661UQlAOVWiz.htm)|Bottled Lightning|Foudre en bouteille|officielle|
|[O4SjTzND1x4DfkNb.htm](npc-gallery-items/O4SjTzND1x4DfkNb.htm)|Athletics|Athlétisme|officielle|
|[o5K5Jo1qZTIda54i.htm](npc-gallery-items/o5K5Jo1qZTIda54i.htm)|Wizard Prepared Spells|Sorts préparés de magicien|officielle|
|[O6gOcXC1xkeICXwL.htm](npc-gallery-items/O6gOcXC1xkeICXwL.htm)|Indecipherable Book of Sigils|Livre indéchiffrable de Symboles|libre|
|[o82vOevA36JTvcnU.htm](npc-gallery-items/o82vOevA36JTvcnU.htm)|Forest Lore|Connaissance des forêts|officielle|
|[o8GJ3AVTDFtS1dFG.htm](npc-gallery-items/o8GJ3AVTDFtS1dFG.htm)|Sickle|Serpe|officielle|
|[O8PG0gBxUuoMxwH2.htm](npc-gallery-items/O8PG0gBxUuoMxwH2.htm)|Retributive Strike|Frappe punitive|officielle|
|[o8WBtfQQ8hDZ7qe7.htm](npc-gallery-items/o8WBtfQQ8hDZ7qe7.htm)|Judge's Robes|Robe de juge|libre|
|[oAhc8baMBkq1f865.htm](npc-gallery-items/oAhc8baMBkq1f865.htm)|Medical Textbook|Manuel médical|officielle|
|[oc10jg3Rm2uXz5eg.htm](npc-gallery-items/oc10jg3Rm2uXz5eg.htm)|Crossbow|Arbalète|officielle|
|[oc2k5FQjYXACg6rP.htm](npc-gallery-items/oc2k5FQjYXACg6rP.htm)|Stone Pestle|Pilon en pierre|officielle|
|[OcO2C4FT46HfzihZ.htm](npc-gallery-items/OcO2C4FT46HfzihZ.htm)|Shovel|Pelle|officielle|
|[OjePIuXOLgAEb4RG.htm](npc-gallery-items/OjePIuXOLgAEb4RG.htm)|No Quarter!|Pas de quartier !|officielle|
|[OkdkQX03m4gr7M3e.htm](npc-gallery-items/OkdkQX03m4gr7M3e.htm)|Shield Block|Blocage au bouclier|officielle|
|[OkuUkkH9XVGKJSre.htm](npc-gallery-items/OkuUkkH9XVGKJSre.htm)|Composite Longbow|+1,striking|Arc long composite de frappe +1|officielle|
|[OmOeBNnG1vyNMitv.htm](npc-gallery-items/OmOeBNnG1vyNMitv.htm)|Legal Lore|Connaissance juridique|officielle|
|[OmZUGABSf4Zc4qb6.htm](npc-gallery-items/OmZUGABSf4Zc4qb6.htm)|Sailing Lore|Connaissance de la navigation|officielle|
|[Oq7EJED40zeLYJUJ.htm](npc-gallery-items/Oq7EJED40zeLYJUJ.htm)|Deceiver's Surprise|Surprise du dupeur|officielle|
|[OqsCqhHLnN6naAWU.htm](npc-gallery-items/OqsCqhHLnN6naAWU.htm)|Scroll Mastery|Maîtrise des parchemins|officielle|
|[OsAKupyZJwzBcBcb.htm](npc-gallery-items/OsAKupyZJwzBcBcb.htm)|Legal Lore|Connaissance juridique|officielle|
|[OSqvPT6Kb8Od7g2m.htm](npc-gallery-items/OSqvPT6Kb8Od7g2m.htm)|Shortsword|Épée courte|officielle|
|[oSXltTfrdndgGVfN.htm](npc-gallery-items/oSXltTfrdndgGVfN.htm)|Swinging Strike|Frappe en balancier|libre|
|[oSzvqCM5pgm1hIvZ.htm](npc-gallery-items/oSzvqCM5pgm1hIvZ.htm)|Smithy Lore|Connaissance en forge|officielle|
|[OTCwjYQZzd5T0upd.htm](npc-gallery-items/OTCwjYQZzd5T0upd.htm)|Arcane Prepared Spells|Sorts arcaniques préparés|officielle|
|[Ow0z6F2Xw9PbQnD7.htm](npc-gallery-items/Ow0z6F2Xw9PbQnD7.htm)|Spellbook (Abominable Missives of the Atrophied)|Grimoire (Missives abominables de l'Atrophié)|libre|
|[owBd74YHegCqxDhR.htm](npc-gallery-items/owBd74YHegCqxDhR.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|officielle|
|[owq1vQqeFtTypbuU.htm](npc-gallery-items/owq1vQqeFtTypbuU.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[OzTxpPbEz9oZ7Oxz.htm](npc-gallery-items/OzTxpPbEz9oZ7Oxz.htm)|Mobility|Mobilité|officielle|
|[p0JGAWaRJxrH24fB.htm](npc-gallery-items/p0JGAWaRJxrH24fB.htm)|Bardic Lore|Connaissance bardique|officielle|
|[p0JJ5Z6nze1w1CXi.htm](npc-gallery-items/p0JJ5Z6nze1w1CXi.htm)|Trained Animal|Dressage animalier|officielle|
|[P0WSBDjQC4AcYXXj.htm](npc-gallery-items/P0WSBDjQC4AcYXXj.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[P2ky9f2YijdmoVk9.htm](npc-gallery-items/P2ky9f2YijdmoVk9.htm)|Sap|Matraque|officielle|
|[P3HCHCil1R8QALJv.htm](npc-gallery-items/P3HCHCil1R8QALJv.htm)|Fist|Poing|officielle|
|[p6fuDAVu9s3zeg9e.htm](npc-gallery-items/p6fuDAVu9s3zeg9e.htm)|Ledger|Registre|libre|
|[Pa0KJtbEU8gMH1Qh.htm](npc-gallery-items/Pa0KJtbEU8gMH1Qh.htm)|Torch Combatant|Combat à la torche|officielle|
|[pb2Ck8v2MCrK8gzf.htm](npc-gallery-items/pb2Ck8v2MCrK8gzf.htm)|Crossbow|+1|Arbalète +1|libre|
|[pCgvFzMbb4y6tLfW.htm](npc-gallery-items/pCgvFzMbb4y6tLfW.htm)|Sage's Analysis|Analyse du sage|officielle|
|[pcW9etyXWo6vgBDy.htm](npc-gallery-items/pcW9etyXWo6vgBDy.htm)|Shortsword|Épée courte|officielle|
|[pCZpELfahQYNki30.htm](npc-gallery-items/pCZpELfahQYNki30.htm)|Cutlery|Couverts|officielle|
|[PDidp7qMSw5qRoXU.htm](npc-gallery-items/PDidp7qMSw5qRoXU.htm)|Quick Draw|Arme en main|officielle|
|[pHbOsoRctK7yp0Uj.htm](npc-gallery-items/pHbOsoRctK7yp0Uj.htm)|Stealth|Discrétion|officielle|
|[phnggMzXOyEB9A1S.htm](npc-gallery-items/phnggMzXOyEB9A1S.htm)|Club|Gourdin|officielle|
|[PlvlB09wLGdgmyo3.htm](npc-gallery-items/PlvlB09wLGdgmyo3.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[pmz2Md1ezhwMIY3D.htm](npc-gallery-items/pmz2Md1ezhwMIY3D.htm)|Underworld Lore|Connaissance de la pègre|officielle|
|[PqeleLSAo4wtVlzd.htm](npc-gallery-items/PqeleLSAo4wtVlzd.htm)|Composite Longbow|Arc long composite|officielle|
|[ptceztheMpvUcPpL.htm](npc-gallery-items/ptceztheMpvUcPpL.htm)|Scribing Lore|Connaissance du métier de scribe|officielle|
|[pV1Zg9nyy2H7ISQ6.htm](npc-gallery-items/pV1Zg9nyy2H7ISQ6.htm)|Innkeeper's Apron|Tablier d'aubergiste|libre|
|[PvAkl60zgsMhzuXb.htm](npc-gallery-items/PvAkl60zgsMhzuXb.htm)|Acid Flask (Moderate) (Infused)|Fiole d'acide modérée (imprégnée)|officielle|
|[pvLUpob2OzcBpXiY.htm](npc-gallery-items/pvLUpob2OzcBpXiY.htm)|Cite Precedent|Citer un précédent|officielle|
|[pZ0qTSs1z35vx57T.htm](npc-gallery-items/pZ0qTSs1z35vx57T.htm)|Hand Crossbow|Arbalète de poing|officielle|
|[pZfnSGB52sgqtEKM.htm](npc-gallery-items/pZfnSGB52sgqtEKM.htm)|Crossbow|Arbalète|officielle|
|[q0BbteyMJiNAGthF.htm](npc-gallery-items/q0BbteyMJiNAGthF.htm)|Frost Vial|Fiole de givre|officielle|
|[Q0NhoV8dgyILx0Gd.htm](npc-gallery-items/Q0NhoV8dgyILx0Gd.htm)|Underworld Lore|Connaissance de la pègre|officielle|
|[q2DuiV2Er7pxJDvH.htm](npc-gallery-items/q2DuiV2Er7pxJDvH.htm)|Sneak Attack|Attaque sournoise|officielle|
|[Q3RfoNXTp5dIw2Mx.htm](npc-gallery-items/Q3RfoNXTp5dIw2Mx.htm)|Behead|Décapitation|officielle|
|[q7PNw0PeVpAbdvGg.htm](npc-gallery-items/q7PNw0PeVpAbdvGg.htm)|Cloak|Cape|libre|
|[q8RnxJPpBW9f5Ie5.htm](npc-gallery-items/q8RnxJPpBW9f5Ie5.htm)|Forager|Glaneur|officielle|
|[Q95qJwP5SCa0IlGu.htm](npc-gallery-items/Q95qJwP5SCa0IlGu.htm)|Medical Malpractice|Erreur médicale|officielle|
|[Q9h6X4WFBsWo4KlV.htm](npc-gallery-items/Q9h6X4WFBsWo4KlV.htm)|Rock|Pierre|officielle|
|[qaGKeUrnOaPLyPKv.htm](npc-gallery-items/qaGKeUrnOaPLyPKv.htm)|Bardic Lore|Connaissance bardique|officielle|
|[qfzrHQkkZcEFrhFT.htm](npc-gallery-items/qfzrHQkkZcEFrhFT.htm)|You're Next|Tu es le suivant|officielle|
|[qIRZdfRLhqBnYMWt.htm](npc-gallery-items/qIRZdfRLhqBnYMWt.htm)|Chart a Course|Suivre le cap|officielle|
|[qmCssarLjtwLY11r.htm](npc-gallery-items/qmCssarLjtwLY11r.htm)|Darkvision|Vision dans le noir|officielle|
|[qNEPB3RKC3SlV4YE.htm](npc-gallery-items/qNEPB3RKC3SlV4YE.htm)|Surprise Attack|Attaque surprise|officielle|
|[qOhRxpDdOg4c6aNs.htm](npc-gallery-items/qOhRxpDdOg4c6aNs.htm)|Bedside Manner|Au chevet des malades|officielle|
|[qQfETAyjFjGSYQV3.htm](npc-gallery-items/qQfETAyjFjGSYQV3.htm)|Shield Block|Blocage au bouclier|officielle|
|[QQXMLJd5WLldP21P.htm](npc-gallery-items/QQXMLJd5WLldP21P.htm)|Hooded Robe|Robe à capuche|libre|
|[qu7Gla2a7DxXInkc.htm](npc-gallery-items/qu7Gla2a7DxXInkc.htm)|Warhammer|Marteau de guerre|officielle|
|[qw0aIOiOvQLo7bUz.htm](npc-gallery-items/qw0aIOiOvQLo7bUz.htm)|Crossbow|Arbalète|officielle|
|[Qw88JvXNINA3GNOT.htm](npc-gallery-items/Qw88JvXNINA3GNOT.htm)|Moderate Alchemist's Fire|Feux grégeois modérés|officielle|
|[QW9oExZljGxszfI0.htm](npc-gallery-items/QW9oExZljGxszfI0.htm)|Crossbow|Arbalète|officielle|
|[qX24xR2DF5OwD6iF.htm](npc-gallery-items/qX24xR2DF5OwD6iF.htm)|Pewter Mug|Gobelet en étain|libre|
|[qy60SQR1jiyW4CXc.htm](npc-gallery-items/qy60SQR1jiyW4CXc.htm)|Snagging Strike|Frappe déconcertante|officielle|
|[qz40qujYeMWWRnDt.htm](npc-gallery-items/qz40qujYeMWWRnDt.htm)|Scroll of Remove Fear (Level 2)|Parchemin de Délivrance de la peur (Niveau 2)|libre|
|[r19Dz88pBsPS4lU2.htm](npc-gallery-items/r19Dz88pBsPS4lU2.htm)|Medical Wisdom|Sagesse médicale|officielle|
|[R3Qe1BmMhgp4UHSo.htm](npc-gallery-items/R3Qe1BmMhgp4UHSo.htm)|Moderate Acid Flask|Fioles d’acide modérées|officielle|
|[R4Ai5HX7MjgLGEyG.htm](npc-gallery-items/R4Ai5HX7MjgLGEyG.htm)|Dangerous Sorcery|Sorcellerie dangereuse|officielle|
|[R5pKOSIyZ8tVn26G.htm](npc-gallery-items/R5pKOSIyZ8tVn26G.htm)|Dagger|Dague|officielle|
|[Rb6YoSdQpTTq789g.htm](npc-gallery-items/Rb6YoSdQpTTq789g.htm)|Textbook|Manuel scolaire|libre|
|[rBpjP1zIiLXuu1XU.htm](npc-gallery-items/rBpjP1zIiLXuu1XU.htm)|Gambling Lore|Connaissance des jeux d'argent|officielle|
|[RcwOpnDVjeLBkRqr.htm](npc-gallery-items/RcwOpnDVjeLBkRqr.htm)|Academia Lore|Connaissance universitaire|officielle|
|[RDzTak37QN2P8E5h.htm](npc-gallery-items/RDzTak37QN2P8E5h.htm)|Staff|+1|Bâton +1|libre|
|[Rgd5z8pZXoiA9cop.htm](npc-gallery-items/Rgd5z8pZXoiA9cop.htm)|Staff|Bâton|officielle|
|[rJH4Sq14VL3sMhLc.htm](npc-gallery-items/rJH4Sq14VL3sMhLc.htm)|+1 Hellknight Plate|Harnois de Chevalier infernal +1|libre|
|[RKa2PsbkATet8Zb9.htm](npc-gallery-items/RKa2PsbkATet8Zb9.htm)|Drunkard's Outfit|Vêtements d'ivrogne|libre|
|[RlKi9kBotypUiYQS.htm](npc-gallery-items/RlKi9kBotypUiYQS.htm)|Mercantile Lore|Connaissance commerciale|officielle|
|[RlqNIctK3YtpYMow.htm](npc-gallery-items/RlqNIctK3YtpYMow.htm)|Simple Injury Poison|Poison de blessure simple|libre|
|[RnJieV2KgJwl8PgM.htm](npc-gallery-items/RnJieV2KgJwl8PgM.htm)|Intimidating Strike|Frappe intimidante|officielle|
|[ro3cy6ShC6y96iaD.htm](npc-gallery-items/ro3cy6ShC6y96iaD.htm)|Rapier|+1|Rapière +1|libre|
|[RoENPeDeCIhLDS5H.htm](npc-gallery-items/RoENPeDeCIhLDS5H.htm)|Trident|Trident|officielle|
|[Rpv1wLy0PVmx5FE6.htm](npc-gallery-items/Rpv1wLy0PVmx5FE6.htm)|Alcohol Lore|Connaissance des alcools|officielle|
|[Rr4yoc0Caa777jb7.htm](npc-gallery-items/Rr4yoc0Caa777jb7.htm)|Dagger|+1|Dague +1|officielle|
|[rskoGYiEEGDi0ngN.htm](npc-gallery-items/rskoGYiEEGDi0ngN.htm)|Academia Lore|Connaissance universitaire|officielle|
|[rTeH6wIQdfQpy5CH.htm](npc-gallery-items/rTeH6wIQdfQpy5CH.htm)|Hunt Prey|Chasser une proie|officielle|
|[RtyfDIKkjX6sanHn.htm](npc-gallery-items/RtyfDIKkjX6sanHn.htm)|Architecture Lore|Connaissance de l'architecture|officielle|
|[rWb3x7u63QvEkLfQ.htm](npc-gallery-items/rWb3x7u63QvEkLfQ.htm)|Divine Prepared Spells|Sorts divins préparés|officielle|
|[rwsjmSGbF6Nuxl2R.htm](npc-gallery-items/rwsjmSGbF6Nuxl2R.htm)|Darkvision|Vision dans le noir|officielle|
|[rxhsNPSPQ1q9Zs6z.htm](npc-gallery-items/rxhsNPSPQ1q9Zs6z.htm)|Living Sextant|Sextant vivant|officielle|
|[ryVaqSZq9cQ7wPix.htm](npc-gallery-items/ryVaqSZq9cQ7wPix.htm)|Scholarly Robes|Robes d'érudit|libre|
|[RzSdb5m56ixW5tfw.htm](npc-gallery-items/RzSdb5m56ixW5tfw.htm)|Mace|Masse d'armes|officielle|
|[s0IuVvw3zTYVeGYV.htm](npc-gallery-items/s0IuVvw3zTYVeGYV.htm)|Bastard Sword|Épée bâtarde|officielle|
|[S0MOf3jyFwTZYtAP.htm](npc-gallery-items/S0MOf3jyFwTZYtAP.htm)|Naval Pike|Pique d'abordage|officielle|
|[s4BkXBiBAgyWmBrU.htm](npc-gallery-items/s4BkXBiBAgyWmBrU.htm)|Deny Advantage|Refus d'avantage|officielle|
|[S4syqUe0hjyn6BQn.htm](npc-gallery-items/S4syqUe0hjyn6BQn.htm)|Crossbow|Arbalète|officielle|
|[S5Yokvt6N9kVghRk.htm](npc-gallery-items/S5Yokvt6N9kVghRk.htm)|Composite Shortbow|+1,striking|Arc court composite de frappe +1|libre|
|[S6XXdREi3Cb7Xh3V.htm](npc-gallery-items/S6XXdREi3Cb7Xh3V.htm)|Servant's Uniform|Uniforme de serviteur|libre|
|[sAXGGq06HDnJjuhX.htm](npc-gallery-items/sAXGGq06HDnJjuhX.htm)|Circus Lore|Connaissance du cirque|officielle|
|[sDwLsrlMceRxlzIo.htm](npc-gallery-items/sDwLsrlMceRxlzIo.htm)|Deny Advantage|Refus d'avantage|officielle|
|[sDz2pTgrdn7OJC6y.htm](npc-gallery-items/sDz2pTgrdn7OJC6y.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|officielle|
|[SeHfXdvpM8XZT2ue.htm](npc-gallery-items/SeHfXdvpM8XZT2ue.htm)|Library Lore|Connaissance des bibliothèques|officielle|
|[SHCWDZKcHBeqUqMh.htm](npc-gallery-items/SHCWDZKcHBeqUqMh.htm)|Tax Ledgers|Registre de taxes|libre|
|[ShvU5c3eNvpu7NSs.htm](npc-gallery-items/ShvU5c3eNvpu7NSs.htm)|Shortsword|Épée courte|officielle|
|[SJ7vgCLVP54MCqjB.htm](npc-gallery-items/SJ7vgCLVP54MCqjB.htm)|Sleep Poison|Poison de sommeil|libre|
|[SjfWr7t0YSZQdt8c.htm](npc-gallery-items/SjfWr7t0YSZQdt8c.htm)|Snare Crafting|Fabrication de pièges artisanaux|officielle|
|[sJmK79CMVyreBL50.htm](npc-gallery-items/sJmK79CMVyreBL50.htm)|Forest Lore|Connaissance des forêts|officielle|
|[skjl8IwD4XG0LeLw.htm](npc-gallery-items/skjl8IwD4XG0LeLw.htm)|Sink or Swim|Coule ou nage|officielle|
|[SLVD3NahEqkF3ybu.htm](npc-gallery-items/SLVD3NahEqkF3ybu.htm)|Magaambya Mask|Masque du Magaambya|libre|
|[SMJAMYebYv4bS1yT.htm](npc-gallery-items/SMJAMYebYv4bS1yT.htm)|Performance|Représentation|officielle|
|[SPtYCE6lLVtQjkar.htm](npc-gallery-items/SPtYCE6lLVtQjkar.htm)|Divine Spontaneous Spells|Sorts divins spontanés|officielle|
|[ssNhLs1uWAzMgArR.htm](npc-gallery-items/ssNhLs1uWAzMgArR.htm)|Alcohol Lore|Connaissance des alcools|officielle|
|[SvCtowtaw8hmMZKz.htm](npc-gallery-items/SvCtowtaw8hmMZKz.htm)|Hatchet|Hachette|officielle|
|[SvKEuXIi5eUaqkLA.htm](npc-gallery-items/SvKEuXIi5eUaqkLA.htm)|Warhammer|Marteau de guerre|officielle|
|[sw2mWVFaQb93J9el.htm](npc-gallery-items/sw2mWVFaQb93J9el.htm)|Forager|Glaneur|officielle|
|[sWmESzQJUt1c3Xuu.htm](npc-gallery-items/sWmESzQJUt1c3Xuu.htm)|Sea Legs|Pied marin|libre|
|[SXTvovlQ2R6oXYos.htm](npc-gallery-items/SXTvovlQ2R6oXYos.htm)|Rapier|+1,striking|Rapière de frappe +1|officielle|
|[T0cpCJqKGkWPXW4L.htm](npc-gallery-items/T0cpCJqKGkWPXW4L.htm)|Royal Defender|Protecteur royal|officielle|
|[T3ZpHsIT64RGyhT0.htm](npc-gallery-items/T3ZpHsIT64RGyhT0.htm)|Power of the Mob|Pouvoir de la foule|officielle|
|[t5skSqnTS4aKuuan.htm](npc-gallery-items/t5skSqnTS4aKuuan.htm)|Surprise Attack|Attaque surprise|officielle|
|[t6XJOXNfa5XsgeQT.htm](npc-gallery-items/t6XJOXNfa5XsgeQT.htm)|Chain Mail with Palace Insignia|Cotte de maille avec l'insigne du Palais|libre|
|[t7aKwycZ4ehsFtFB.htm](npc-gallery-items/t7aKwycZ4ehsFtFB.htm)|Hatchet|Hachette|officielle|
|[T7oZcH7OKDm1O4cw.htm](npc-gallery-items/T7oZcH7OKDm1O4cw.htm)|Aura of Command|Aura du commandant|officielle|
|[t8D9jbYaoXXUxjCe.htm](npc-gallery-items/t8D9jbYaoXXUxjCe.htm)|Versatile Performance|Polyvalence artistique|officielle|
|[TAnxvIqFKgGFyBhv.htm](npc-gallery-items/TAnxvIqFKgGFyBhv.htm)|Cooking Lore|Connaissance culinaire|officielle|
|[taX3Q0r1h37NNYFO.htm](npc-gallery-items/taX3Q0r1h37NNYFO.htm)|Hazard Spotter|Détection des dangers|officielle|
|[tCqd5uhyOKVs0yco.htm](npc-gallery-items/tCqd5uhyOKVs0yco.htm)|Protect the Master!|Protégez le maître !|officielle|
|[tDia9PqZk9lPblrF.htm](npc-gallery-items/tDia9PqZk9lPblrF.htm)|Appraising Eye|OEil d'expert|officielle|
|[TEKRQ6lf1xZxEPIz.htm](npc-gallery-items/TEKRQ6lf1xZxEPIz.htm)|Fist|Poing|officielle|
|[tMYrLjnqdp2h4CWk.htm](npc-gallery-items/tMYrLjnqdp2h4CWk.htm)|Swim Away|Nage tactique|libre|
|[ttkiwONkYUn5piHI.htm](npc-gallery-items/ttkiwONkYUn5piHI.htm)|War Flail|Fléau d'armes|officielle|
|[ttSdjINAYV0VxPZk.htm](npc-gallery-items/ttSdjINAYV0VxPZk.htm)|Sneak Attack|Attaque sournoise|officielle|
|[TvnwhUBwaljk26uo.htm](npc-gallery-items/TvnwhUBwaljk26uo.htm)|Dagger|Dague|officielle|
|[tW8uP6mKqhtlwmPP.htm](npc-gallery-items/tW8uP6mKqhtlwmPP.htm)|+1 Half Plate|Armure de plate +1|libre|
|[tWDvrIy7tMRedOAa.htm](npc-gallery-items/tWDvrIy7tMRedOAa.htm)|Master Tracker|Maître pisteur|officielle|
|[twFQitk8O2yJII8R.htm](npc-gallery-items/twFQitk8O2yJII8R.htm)|Household Lore|Connaissance domestique|officielle|
|[tYBrwLh84hOMoQb4.htm](npc-gallery-items/tYBrwLh84hOMoQb4.htm)|Bastard Sword|Épée bâtarde|officielle|
|[TyYuupAf39PFsWJO.htm](npc-gallery-items/TyYuupAf39PFsWJO.htm)|Spell Component Pouch|Bourse de composantes de sorts|libre|
|[TztjddoEZphVsSDa.htm](npc-gallery-items/TztjddoEZphVsSDa.htm)|Focused Thinker|Penseur concentré|officielle|
|[U1pVgJkNIeXvdXNP.htm](npc-gallery-items/U1pVgJkNIeXvdXNP.htm)|Underworld Lore|Connaissance de la pègre|officielle|
|[u3RFdo6uA3yPenzC.htm](npc-gallery-items/u3RFdo6uA3yPenzC.htm)|Light Hammer|Marteau de guerre léger|officielle|
|[u6QEft9DxfittKI2.htm](npc-gallery-items/u6QEft9DxfittKI2.htm)|Bosun's Command|Injonction de bosco|officielle|
|[u7AfocCKAnj4LGQO.htm](npc-gallery-items/u7AfocCKAnj4LGQO.htm)|Staff|Bâton|officielle|
|[uA4DNYAmIuhSQAgl.htm](npc-gallery-items/uA4DNYAmIuhSQAgl.htm)|Lute|Luth|libre|
|[uaIlnacoQ95VuVMM.htm](npc-gallery-items/uaIlnacoQ95VuVMM.htm)|Sailing Lore|Connaissance de la navigation|officielle|
|[uBrl3zYakAsOXqbM.htm](npc-gallery-items/uBrl3zYakAsOXqbM.htm)|Clothing with Jewelry|Habits avec des bijoux|libre|
|[ue4Y2G3CCizn6XEG.htm](npc-gallery-items/ue4Y2G3CCizn6XEG.htm)|Sneak Attack|Attaque sournoise|officielle|
|[uE9nqIQImKOG8Vr0.htm](npc-gallery-items/uE9nqIQImKOG8Vr0.htm)|Hidden Blade|Lame dissimulée|officielle|
|[Uee48lu9JRn91tRp.htm](npc-gallery-items/Uee48lu9JRn91tRp.htm)|Pick|Pic|officielle|
|[uf3cFRfFs23gJ4eW.htm](npc-gallery-items/uf3cFRfFs23gJ4eW.htm)|Religious Text of Nethys|Livre religieux de Néthys|libre|
|[Ufe5H0f9R4O2VBNV.htm](npc-gallery-items/Ufe5H0f9R4O2VBNV.htm)|Nature's Edge|Avantage naturel|officielle|
|[ufJVnIjBde8zNbAj.htm](npc-gallery-items/ufJVnIjBde8zNbAj.htm)|Dagger|Dague|officielle|
|[ug2TQxIeBOWQren2.htm](npc-gallery-items/ug2TQxIeBOWQren2.htm)|Sorcerer Bloodline Spells|Sorts de lignage d'ensorceleur|officielle|
|[uglCIhXIhI66IGsk.htm](npc-gallery-items/uglCIhXIhI66IGsk.htm)|Crimson Vengeance|Vengeance écarlate|officielle|
|[UhDwKtMaA99YE2vC.htm](npc-gallery-items/UhDwKtMaA99YE2vC.htm)|Sudden Charge|Charge soudaine|officielle|
|[ukfbB1s02nAdmdwQ.htm](npc-gallery-items/ukfbB1s02nAdmdwQ.htm)|Scroll Case with Ship's Charts|Étui de parchemins avec des cartes de navigations|libre|
|[uLm4nxop4jQv6iwA.htm](npc-gallery-items/uLm4nxop4jQv6iwA.htm)|Circus Lore|Connaissance du cirque|officielle|
|[uMvy7NgRGRADz6zB.htm](npc-gallery-items/uMvy7NgRGRADz6zB.htm)|Staff|Bâton|officielle|
|[un2PSYOK7aGBLat2.htm](npc-gallery-items/un2PSYOK7aGBLat2.htm)|Flask of Whiskey|Flasque de whisky|libre|
|[uozTPO1slJcKBEax.htm](npc-gallery-items/uozTPO1slJcKBEax.htm)|Dagger|Dague|officielle|
|[UpuwyeFRhKKkWeGm.htm](npc-gallery-items/UpuwyeFRhKKkWeGm.htm)|Accounting Lore|Connaissance de la comptabilité|officielle|
|[Uq9fVOOj1180ESoY.htm](npc-gallery-items/Uq9fVOOj1180ESoY.htm)|Diplomacy|Diplomatie|officielle|
|[UQoNtmlZuh2NnKd0.htm](npc-gallery-items/UQoNtmlZuh2NnKd0.htm)|Noble's Ruse|Ruse du noble|officielle|
|[UticIspMVAd6QGkp.htm](npc-gallery-items/UticIspMVAd6QGkp.htm)|Divine Focus Spells|Sorts divins focalisés|officielle|
|[UTr5XgqL7eUu8pyH.htm](npc-gallery-items/UTr5XgqL7eUu8pyH.htm)|Low-Light Vision|Vision nocturne|officielle|
|[uUISfTBeVRNhnN9u.htm](npc-gallery-items/uUISfTBeVRNhnN9u.htm)|Fist|Poing|officielle|
|[UWagoDkGXzULZ0i4.htm](npc-gallery-items/UWagoDkGXzULZ0i4.htm)|Sap|Matraque|officielle|
|[UwdvjmLBy5IaWIWn.htm](npc-gallery-items/UwdvjmLBy5IaWIWn.htm)|Sneak Attack|Attaque sournoise|officielle|
|[uYklkDltD5DuFVB7.htm](npc-gallery-items/uYklkDltD5DuFVB7.htm)|Astrolabe|Astrolabe|libre|
|[UZZJYmev69dOUgKu.htm](npc-gallery-items/UZZJYmev69dOUgKu.htm)|Composite Longbow|Arc long composite|officielle|
|[v2DAXboOXPMsm6x0.htm](npc-gallery-items/v2DAXboOXPMsm6x0.htm)|Crafting|Artisanat|officielle|
|[v3VQfpzwMQ3vAQCU.htm](npc-gallery-items/v3VQfpzwMQ3vAQCU.htm)|Scoundrel's Feint|Feinte du scélérat|officielle|
|[V3WALYE32ZPtfiRt.htm](npc-gallery-items/V3WALYE32ZPtfiRt.htm)|Sap|Matraque|officielle|
|[VCHM4U1i2BqAQJdz.htm](npc-gallery-items/VCHM4U1i2BqAQJdz.htm)|Staff|Bâton|officielle|
|[VcNmSmQQR4qxCCxK.htm](npc-gallery-items/VcNmSmQQR4qxCCxK.htm)|Alchemist's Fire (Moderate) (Infused)|Feu grégeois modéré (imprégné)|officielle|
|[vDou2pKWxCBzmvqr.htm](npc-gallery-items/vDou2pKWxCBzmvqr.htm)|Gravedigger's Garb|Accoutrement de fossoyeur|libre|
|[vfhKr3Ly9XjE7PhV.htm](npc-gallery-items/vfhKr3Ly9XjE7PhV.htm)|Occult Focus Spells|Sorts occultes focalisés|officielle|
|[VfIDgw7C87841Ehg.htm](npc-gallery-items/VfIDgw7C87841Ehg.htm)|Unshakable|Inébranlable|officielle|
|[vgPpoUOgBxWLd4Ne.htm](npc-gallery-items/vgPpoUOgBxWLd4Ne.htm)|Steady Balance|Équilibre stable|officielle|
|[VJxxeLCAgKz1s9HI.htm](npc-gallery-items/VJxxeLCAgKz1s9HI.htm)|Abyssal Temptation|Tentations abyssales|officielle|
|[VKwW4ylsv5KU7Nhr.htm](npc-gallery-items/VKwW4ylsv5KU7Nhr.htm)|Dread Striker|Frappeur d'effroi|officielle|
|[vmjC8rwYO5CA0CjY.htm](npc-gallery-items/vmjC8rwYO5CA0CjY.htm)|Bardic Lore|Connaissance bardique|officielle|
|[vnh6CQV0lhDgm0I8.htm](npc-gallery-items/vnh6CQV0lhDgm0I8.htm)|Shining Crusade Lore|Connaissance de la Croisade étincelante|officielle|
|[vOk7ftvRMJXLkNWY.htm](npc-gallery-items/vOk7ftvRMJXLkNWY.htm)|Hand Crossbow|Arbalète de poing|officielle|
|[vprpkjJKcOBq6m7X.htm](npc-gallery-items/vprpkjJKcOBq6m7X.htm)|Rock|Pierre|officielle|
|[Vqjb5Zoa5sSTgnDg.htm](npc-gallery-items/Vqjb5Zoa5sSTgnDg.htm)|Composite Longbow|Arc long composite|officielle|
|[VQPPHF2M9xHf5XD3.htm](npc-gallery-items/VQPPHF2M9xHf5XD3.htm)|Sneak Attack|Attaque sournoise|officielle|
|[vrubZ4BfdgD4AmTp.htm](npc-gallery-items/vrubZ4BfdgD4AmTp.htm)|Shortsword|Épée courte|officielle|
|[vtgkJ23YrCFVTyTP.htm](npc-gallery-items/vtgkJ23YrCFVTyTP.htm)|Warding Strike|Frappe du gardien|officielle|
|[VtGXx6gZ4Ezl8Mcp.htm](npc-gallery-items/VtGXx6gZ4Ezl8Mcp.htm)|Pewter Mug|Gobelet en étain|officielle|
|[vyJ3imlXVxUHGVq3.htm](npc-gallery-items/vyJ3imlXVxUHGVq3.htm)|Weapon Mastery (Hammer)|Maîtrise martiale (Marteau)|officielle|
|[Vz4DYAJUl9kelFni.htm](npc-gallery-items/Vz4DYAJUl9kelFni.htm)|Staff|Bâton|officielle|
|[vZlRJzB7o08xfLuI.htm](npc-gallery-items/vZlRJzB7o08xfLuI.htm)|Light Hammer|Marteau de guerre léger|officielle|
|[VzPHszVrqprSLuY7.htm](npc-gallery-items/VzPHszVrqprSLuY7.htm)|Primal Focus Spells|Sorts primordiaux focalisés|officielle|
|[w021SRzRisqhAnKU.htm](npc-gallery-items/w021SRzRisqhAnKU.htm)|Darkvision|Vision dans le noir|officielle|
|[W3jbnf0RJFBtiRto.htm](npc-gallery-items/W3jbnf0RJFBtiRto.htm)|Halberd|Hallebarde|officielle|
|[W5ngvSMX8dRsptvJ.htm](npc-gallery-items/W5ngvSMX8dRsptvJ.htm)|Tax Documents|Documents d'impôts|libre|
|[wDgSQGJhEyvpYeZE.htm](npc-gallery-items/wDgSQGJhEyvpYeZE.htm)|Persistent Lies|Mensonges persistants|officielle|
|[we5UuMRtG5niRCBP.htm](npc-gallery-items/we5UuMRtG5niRCBP.htm)|Thunderstone|Pierre à tonnerre|officielle|
|[weCX0QLmEtlkJj6e.htm](npc-gallery-items/weCX0QLmEtlkJj6e.htm)|Pitchfork|Fourche|libre|
|[WFByucZQz0Q9slLk.htm](npc-gallery-items/WFByucZQz0Q9slLk.htm)|Sling|Fronde|officielle|
|[wfNSTwrVHkHF5YLS.htm](npc-gallery-items/wfNSTwrVHkHF5YLS.htm)|Hand Crossbow|Arbalète de poing|officielle|
|[WGBYwG2C4eElCrPd.htm](npc-gallery-items/WGBYwG2C4eElCrPd.htm)|Collaborative Thievery|Vol collaboratif|officielle|
|[WHOLkEuYaCAMaFl1.htm](npc-gallery-items/WHOLkEuYaCAMaFl1.htm)|Noble's Ally|Allié de la noblesse|officielle|
|[wiKF84BoxIlLdggN.htm](npc-gallery-items/wiKF84BoxIlLdggN.htm)|Surprise Attack|Attaque surprise|officielle|
|[wJwF7vhkxxkiLdKt.htm](npc-gallery-items/wJwF7vhkxxkiLdKt.htm)|Lute|Luth|libre|
|[WKPSQhPteRbqC14q.htm](npc-gallery-items/WKPSQhPteRbqC14q.htm)|Mortar and Pestle|Mortier et pilon|libre|
|[wlmOlag2TUOgMUOB.htm](npc-gallery-items/wlmOlag2TUOgMUOB.htm)|Sailing Lore|Connaissance de la navigation|officielle|
|[wLxqjBiRMADtMrlV.htm](npc-gallery-items/wLxqjBiRMADtMrlV.htm)|Light Hammer|Marteau de guerre léger|officielle|
|[wnR3RwCNys0DRqxB.htm](npc-gallery-items/wnR3RwCNys0DRqxB.htm)|Sap|Matraque|officielle|
|[wOWNuciIMk1EpXis.htm](npc-gallery-items/wOWNuciIMk1EpXis.htm)|Holy Water|Eau bénite|officielle|
|[wPucfhAZe8Uqamyu.htm](npc-gallery-items/wPucfhAZe8Uqamyu.htm)|Crossbow|Arbalète|officielle|
|[WQEUzXA436GuObJu.htm](npc-gallery-items/WQEUzXA436GuObJu.htm)|Greataxe|+1|Grande hache +1|libre|
|[WRQQun80G8om7SIp.htm](npc-gallery-items/WRQQun80G8om7SIp.htm)|Morningstar|Morgenstern|officielle|
|[WrTvPVOo3UOYpMWC.htm](npc-gallery-items/WrTvPVOo3UOYpMWC.htm)|Boarding Action|Action d’embarquement|officielle|
|[wSGmAT3ciB7uLXoe.htm](npc-gallery-items/wSGmAT3ciB7uLXoe.htm)|Bottled Lightning (Lesser) (Infused)|Foudre en bouteille inférieur (imprégné)|libre|
|[wtkxx3QceWjfCcym.htm](npc-gallery-items/wtkxx3QceWjfCcym.htm)|Frost Vial (Moderate) (Infused)|Fiole de givre modérée (imprégnée)|libre|
|[wvyx4nEskJ9Imojt.htm](npc-gallery-items/wvyx4nEskJ9Imojt.htm)|Barkeep's Advice|Conseil du tavernier|officielle|
|[wwLWKH0F1mtPQ2lE.htm](npc-gallery-items/wwLWKH0F1mtPQ2lE.htm)|+1 to Sense Motive|+1 pour Deviner les intentions|officielle|
|[wXgvnztT7r9jO7Px.htm](npc-gallery-items/wXgvnztT7r9jO7Px.htm)|Brutal Rally|Mobilisation par la violence|officielle|
|[wxpvY7YmCPTSk3p9.htm](npc-gallery-items/wxpvY7YmCPTSk3p9.htm)|Raise a Shield|Lever un bouclier|officielle|
|[wYUI9xidJaJLYynz.htm](npc-gallery-items/wYUI9xidJaJLYynz.htm)|The Jig Is Up|La fête est terminée|officielle|
|[WZzoPHc7f4dTn3iC.htm](npc-gallery-items/WZzoPHc7f4dTn3iC.htm)|Shortsword|Épée courte|officielle|
|[X7QR8UnnfPH6vOWK.htm](npc-gallery-items/X7QR8UnnfPH6vOWK.htm)|Subdue Prisoners|Maîtriser les prisonniers|officielle|
|[X8NQcsCgEiQHiZj6.htm](npc-gallery-items/X8NQcsCgEiQHiZj6.htm)|Arcane Prepared Spells|Sorts arcaniques préparés|officielle|
|[x8s1JWo1Jcm4elFK.htm](npc-gallery-items/x8s1JWo1Jcm4elFK.htm)|+1 Status to All Saves vs. Poison|bonus de statut de +1 aux JdS contre le poison|officielle|
|[xBb6evQkhB3uYAvn.htm](npc-gallery-items/xBb6evQkhB3uYAvn.htm)|Rage|Rage|officielle|
|[Xbpr8x90JdxlOqrV.htm](npc-gallery-items/Xbpr8x90JdxlOqrV.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|officielle|
|[XCCNdWgAwDmYJNUr.htm](npc-gallery-items/XCCNdWgAwDmYJNUr.htm)|Occult Spontaneous Spells|Sorts occultes spontanés|officielle|
|[XCtQzAvxzCKT6NIl.htm](npc-gallery-items/XCtQzAvxzCKT6NIl.htm)|Stench of Decay|Odeur de décomposition|officielle|
|[XDBnPMR5BrHQO9Pj.htm](npc-gallery-items/XDBnPMR5BrHQO9Pj.htm)|Ledger|Registre|libre|
|[XdNbqGPaLBs6RCOX.htm](npc-gallery-items/XdNbqGPaLBs6RCOX.htm)|Dagger|Dague|officielle|
|[xeEI4dqDE4PmxOeA.htm](npc-gallery-items/xeEI4dqDE4PmxOeA.htm)|Forest Walker|Arpenteur des forêts|officielle|
|[XFo44PTU8F1rJam8.htm](npc-gallery-items/XFo44PTU8F1rJam8.htm)|Destructive Vengeance|Vindicte destructrice|officielle|
|[XHVHeWTU0zqmjJWZ.htm](npc-gallery-items/XHVHeWTU0zqmjJWZ.htm)|Whip|Fouet|officielle|
|[xhZKk4oQRgs08i0T.htm](npc-gallery-items/xhZKk4oQRgs08i0T.htm)|Nimble Dodge|Esquive agile|officielle|
|[XhzT06iOtcqrirxO.htm](npc-gallery-items/XhzT06iOtcqrirxO.htm)|Pathfinder Society Lore|Connaissance de la Société des éclaireurs|officielle|
|[xI5EGbyi2XbV0z6e.htm](npc-gallery-items/xI5EGbyi2XbV0z6e.htm)|Fist|Poing|officielle|
|[Xja3Z5ROmRPkUeeO.htm](npc-gallery-items/Xja3Z5ROmRPkUeeO.htm)|Leather Apron|Tablier en cuir|libre|
|[xkwTvTqSPNbk86Un.htm](npc-gallery-items/xkwTvTqSPNbk86Un.htm)|Crossbow|Arbalète|officielle|
|[xLUuOeUKIbtUPL9R.htm](npc-gallery-items/xLUuOeUKIbtUPL9R.htm)|Athletics|Athlétisme|officielle|
|[XnFDooIYZ58vGk4b.htm](npc-gallery-items/XnFDooIYZ58vGk4b.htm)|Crab Cage|Cage à crabes|libre|
|[Xo7vz0G7GjTFg4yo.htm](npc-gallery-items/Xo7vz0G7GjTFg4yo.htm)|Light Mace|Masse d'armes légère|officielle|
|[XPYr9hMESgVNjbVp.htm](npc-gallery-items/XPYr9hMESgVNjbVp.htm)|Trick Attack|Ruse d'attaque|officielle|
|[XqNEuTQGsReodN8h.htm](npc-gallery-items/XqNEuTQGsReodN8h.htm)|Sap|Matraque|officielle|
|[xS8f6u8VDcBepVGM.htm](npc-gallery-items/xS8f6u8VDcBepVGM.htm)|Demon Summoning|Convocation de démon|officielle|
|[xuyjTEDoDAmb5acN.htm](npc-gallery-items/xuyjTEDoDAmb5acN.htm)|Sap|Matraque|officielle|
|[XVoLXwXlo8BPWO8P.htm](npc-gallery-items/XVoLXwXlo8BPWO8P.htm)|Spiked Gauntlet|Gantelet à pointes|officielle|
|[XxRBSksVZSF2n79w.htm](npc-gallery-items/XxRBSksVZSF2n79w.htm)|Dagger|Dague|officielle|
|[XXu57gmSKMHk38fa.htm](npc-gallery-items/XXu57gmSKMHk38fa.htm)|Rock|Rocher|officielle|
|[Y4ZPmVCbnUAexfGS.htm](npc-gallery-items/Y4ZPmVCbnUAexfGS.htm)|+1 Circumstance to All Saves vs. Traps|Bonus de circonstances +1 aux JdS contre les pièges|officielle|
|[y5n9AfgPzGvGtS69.htm](npc-gallery-items/y5n9AfgPzGvGtS69.htm)|Rock|Pierre|officielle|
|[y6XpXIXPKaxphOrA.htm](npc-gallery-items/y6XpXIXPKaxphOrA.htm)|Architecture Lore|Connaissance de l'architecture|officielle|
|[y77GcQxQjHDAiISd.htm](npc-gallery-items/y77GcQxQjHDAiISd.htm)|Sling|Fronde|officielle|
|[y9nL7kklEoVqLV06.htm](npc-gallery-items/y9nL7kklEoVqLV06.htm)|Favored Terrain|Environnement de prédilection|officielle|
|[yAk7aiu08rTizR5c.htm](npc-gallery-items/yAk7aiu08rTizR5c.htm)|Rapier|Rapière|officielle|
|[YB9T72k74oWO9L6c.htm](npc-gallery-items/YB9T72k74oWO9L6c.htm)|Low-Light Vision|Vision nocturne|officielle|
|[YCf4WMESlKy1zF6l.htm](npc-gallery-items/YCf4WMESlKy1zF6l.htm)|Sneak Attack|Attaque sournoise|officielle|
|[ydSwmhudPEes84G7.htm](npc-gallery-items/ydSwmhudPEes84G7.htm)|Legal Lore|Connaissance juridique|officielle|
|[Ye6jF7F3YdRigibJ.htm](npc-gallery-items/Ye6jF7F3YdRigibJ.htm)|Methodical Research|Recherche méthodique|officielle|
|[yEcvRwueCdGqs9GC.htm](npc-gallery-items/yEcvRwueCdGqs9GC.htm)|Dagger|Dague|officielle|
|[YEsqZ1pmCAbdEHfI.htm](npc-gallery-items/YEsqZ1pmCAbdEHfI.htm)|Champion Devotion Spells|Sorts de dévotion de champion|officielle|
|[YFx6uVJW2kREU7me.htm](npc-gallery-items/YFx6uVJW2kREU7me.htm)|One Additional Lore|Une connaissance supplémentaire|officielle|
|[YgMj9CaUadOgAwPl.htm](npc-gallery-items/YgMj9CaUadOgAwPl.htm)|Scalpel|Scalpel|officielle|
|[yJqGrkv3kpN6caAd.htm](npc-gallery-items/yJqGrkv3kpN6caAd.htm)|Weapon Mastery|Maîtrise martiale|officielle|
|[yjTVn5zGiibr7UH8.htm](npc-gallery-items/yjTVn5zGiibr7UH8.htm)|Crossbow|+1,striking|Arbalète de frappe +1|libre|
|[Yk9MTrG2nXJYu38f.htm](npc-gallery-items/Yk9MTrG2nXJYu38f.htm)|Thunderstone (Lesser) (Infused)|Pierre à tonnerre inférieure (imprégnée)|libre|
|[yM3snMjrKoj8gXaq.htm](npc-gallery-items/yM3snMjrKoj8gXaq.htm)|Club|Gourdin|officielle|
|[YMK6yeIqUwKCrXpF.htm](npc-gallery-items/YMK6yeIqUwKCrXpF.htm)|Composite Shortbow|Arc court composite|officielle|
|[ynKm4RDwMaA0vdpv.htm](npc-gallery-items/ynKm4RDwMaA0vdpv.htm)|Journal|Journal intime|libre|
|[yqiy77WfCwOSI2ti.htm](npc-gallery-items/yqiy77WfCwOSI2ti.htm)|Alcohol Lore|Connaissance des alcools|officielle|
|[yqwZZgxhcWcjshla.htm](npc-gallery-items/yqwZZgxhcWcjshla.htm)|Accounting Lore|Connaissance de la comptabilité|officielle|
|[YT49F2uCWIqKs6Ct.htm](npc-gallery-items/YT49F2uCWIqKs6Ct.htm)|Divine Spontaneous Spells|Sorts divins spontanés|officielle|
|[YTWYrskTTtVm4yze.htm](npc-gallery-items/YTWYrskTTtVm4yze.htm)|Composite Longbow|Arc long composite|officielle|
|[YVRMs40wzsiQC7Si.htm](npc-gallery-items/YVRMs40wzsiQC7Si.htm)|Collection of Expired Documents with Intact Seals|Collection de documents périmés avec sceau intact|libre|
|[YW1vBoCGfZcVfMiP.htm](npc-gallery-items/YW1vBoCGfZcVfMiP.htm)|Composite Longbow|+1|Arc long composite +1|officielle|
|[Yxoxo2Zh5olZHceb.htm](npc-gallery-items/Yxoxo2Zh5olZHceb.htm)|Shield Block|Blocage au bouclier|officielle|
|[yYnHoV3tJvO6D3bB.htm](npc-gallery-items/yYnHoV3tJvO6D3bB.htm)|Legal Lore|Connaissance juridique|officielle|
|[YZqHMPt8ZBbo3FbI.htm](npc-gallery-items/YZqHMPt8ZBbo3FbI.htm)|Underworld Lore|Connaissance de la pègre|officielle|
|[Yzt7iAgbGNFAn1NW.htm](npc-gallery-items/Yzt7iAgbGNFAn1NW.htm)|+1 Circumstance Bonus on Saves vs. Poisons|+1 de circonstances aux JdS contre le poison|officielle|
|[z1TGYF24OzHmgdjp.htm](npc-gallery-items/z1TGYF24OzHmgdjp.htm)|Whiskey|Whisky|libre|
|[Z6BNfng94z1xdzn3.htm](npc-gallery-items/Z6BNfng94z1xdzn3.htm)|Scalpel|Scalpel|libre|
|[ZBaDNdF5HlekM2y5.htm](npc-gallery-items/ZBaDNdF5HlekM2y5.htm)|Monster Lore|Connaissance des monstres|officielle|
|[ZcVfcVHFVcJZhIQI.htm](npc-gallery-items/ZcVfcVHFVcJZhIQI.htm)|Greataxe|Grande hache|officielle|
|[zecbW6qIhXuKa8Yu.htm](npc-gallery-items/zecbW6qIhXuKa8Yu.htm)|Fist|Poing|officielle|
|[zeH3OrBLioIRoYb5.htm](npc-gallery-items/zeH3OrBLioIRoYb5.htm)|Deny Advantage|Refus d'avantage|officielle|
|[zewypHqdhJynMyRj.htm](npc-gallery-items/zewypHqdhJynMyRj.htm)|Engineering Lore|Connaissance de l'ingénierie|officielle|
|[zEyJJyZtRmrTwf2n.htm](npc-gallery-items/zEyJJyZtRmrTwf2n.htm)|Collection of Fake Relics|Collection de faux reliques|libre|
|[zg7ntU3VEOypwaws.htm](npc-gallery-items/zg7ntU3VEOypwaws.htm)|Attack of Opportunity|Attaque d'opportunité|officielle|
|[zICeN9ExAbhXEjzA.htm](npc-gallery-items/zICeN9ExAbhXEjzA.htm)|Infused Items|Objets imprégnés|officielle|
|[ziPz3z2a6Db63N9r.htm](npc-gallery-items/ziPz3z2a6Db63N9r.htm)|Reckless Alchemy|Alchimie imprudente|officielle|
|[zKVUU94IzSXLEDLT.htm](npc-gallery-items/zKVUU94IzSXLEDLT.htm)|Crossbow|Arbalète|officielle|
|[ZMcT1sMmOY8rwe3w.htm](npc-gallery-items/ZMcT1sMmOY8rwe3w.htm)|Shiv|Surin|libre|
|[zmdPdUEf9SjhHnTL.htm](npc-gallery-items/zmdPdUEf9SjhHnTL.htm)|Sneak Attack|Attaque sournoise|officielle|
|[ZmfzXnEaPSxGh2J4.htm](npc-gallery-items/ZmfzXnEaPSxGh2J4.htm)|Quick Catch|Rattrapé au vol|officielle|
|[zMkrKhyfRWFrFfuv.htm](npc-gallery-items/zMkrKhyfRWFrFfuv.htm)|Fanatical Frenzy|Frénésie fanatique|officielle|
|[zn2RGfRS6HoLPjTb.htm](npc-gallery-items/zn2RGfRS6HoLPjTb.htm)|Scouting Lore|Connaissance du métier d'éclaireur|officielle|
|[ZraokHXxg7pBRXud.htm](npc-gallery-items/ZraokHXxg7pBRXud.htm)|Rugged Clothes with Tool Belt|Vêtements rugueux avec une ceinture d'outils|libre|
|[zS04tb23VRw9X9kX.htm](npc-gallery-items/zS04tb23VRw9X9kX.htm)|Nimble Dodge|Esquive agile|officielle|
|[ZtFKTs16yHUY48WF.htm](npc-gallery-items/ZtFKTs16yHUY48WF.htm)|Healing Hands|Mains guérisseuses|officielle|
|[ZtSXv2TmdsOEyaIw.htm](npc-gallery-items/ZtSXv2TmdsOEyaIw.htm)|Cult Lore (Applies to the Leader's own cult)|Connaissance du culte (s'applique à son propre culte)|officielle|
|[ZuDGBQWUf1cSV69U.htm](npc-gallery-items/ZuDGBQWUf1cSV69U.htm)|Call to Action|Appel à la mobilisation|officielle|
|[ZuiIsrJudzF67zHr.htm](npc-gallery-items/ZuiIsrJudzF67zHr.htm)|+1 Chain Shirt|Chemise de mailles +1|officielle|
|[zYZvGGQfgJZyoNMQ.htm](npc-gallery-items/zYZvGGQfgJZyoNMQ.htm)|Dagger|Dague|officielle|
