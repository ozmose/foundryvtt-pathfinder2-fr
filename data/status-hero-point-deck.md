# État de la traduction (hero-point-deck)

 * **libre**: 52


Dernière mise à jour: 2023-06-07 06:38 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0CPZ672t3F462vTh.htm](hero-point-deck/0CPZ672t3F462vTh.htm)|Rage and Fury|Rage et Furie|libre|
|[0MLuHRkOADrmm7Fa.htm](hero-point-deck/0MLuHRkOADrmm7Fa.htm)|I Hope This Works|J'espère que ça fonctionne|libre|
|[1VbuhRzlKWPfcBpq.htm](hero-point-deck/1VbuhRzlKWPfcBpq.htm)|Endure the Onslaught|Endurer l'Assaut|libre|
|[1We96Iyv6MkFvEN8.htm](hero-point-deck/1We96Iyv6MkFvEN8.htm)|Battle Cry|Cri de guerre|libre|
|[2zFd5Huu1Yf8RDFE.htm](hero-point-deck/2zFd5Huu1Yf8RDFE.htm)|Catch your Breath|Reprends ton souffle|libre|
|[4Lvb1JdWFgDyp58P.htm](hero-point-deck/4Lvb1JdWFgDyp58P.htm)|Last Stand|Dernier combat|libre|
|[6JieVtNOk4ZVclcP.htm](hero-point-deck/6JieVtNOk4ZVclcP.htm)|Shoot Through|Tir à travers|libre|
|[6Vnr4v3SspJH87lQ.htm](hero-point-deck/6Vnr4v3SspJH87lQ.htm)|Reckless Charge|Charge téméraire|libre|
|[764VCnwWLKmpPsBj.htm](hero-point-deck/764VCnwWLKmpPsBj.htm)|Roll Back|Roulade arrière|libre|
|[7Nk3WehdHLrgwnBZ.htm](hero-point-deck/7Nk3WehdHLrgwnBZ.htm)|Warding Sign|Signe de protection|libre|
|[8d7KQtHz1kYxvRIa.htm](hero-point-deck/8d7KQtHz1kYxvRIa.htm)|Ancestral Might|Puissance ancestral|libre|
|[atIE8EmiN38wh8wx.htm](hero-point-deck/atIE8EmiN38wh8wx.htm)|Last Second Sidestep|Pas de côté de dernière minute|libre|
|[cgoOcef3miRd16bt.htm](hero-point-deck/cgoOcef3miRd16bt.htm)|Reverse Strike|Frappe à revers|libre|
|[DGoCx0xl47vL2oOY.htm](hero-point-deck/DGoCx0xl47vL2oOY.htm)|Called Foe|Ennemi désigné|libre|
|[F39gVP1gj2NVWgLD.htm](hero-point-deck/F39gVP1gj2NVWgLD.htm)|Class Might|Puissance de classe|libre|
|[FaCIC09UZubaw2LO.htm](hero-point-deck/FaCIC09UZubaw2LO.htm)|Tumble Through|Déplacement acrobatique|libre|
|[FB7WRjQkNnCROiH8.htm](hero-point-deck/FB7WRjQkNnCROiH8.htm)|Hold the Line|Tenez la ligne|libre|
|[ftJgbv638qcidZk0.htm](hero-point-deck/ftJgbv638qcidZk0.htm)|Critical Moment|Instant Critique|libre|
|[GOafyVMF9ZkCuB3G.htm](hero-point-deck/GOafyVMF9ZkCuB3G.htm)|Fluid Motion|Mouvement fluide|libre|
|[HEbxDXXFXDTWAitt.htm](hero-point-deck/HEbxDXXFXDTWAitt.htm)|Magical Reverberation|Réverberation magique|libre|
|[HfHDVaEEtG2wUmOZ.htm](hero-point-deck/HfHDVaEEtG2wUmOZ.htm)|Spark of Courage|Étincelle de courage|libre|
|[hhzrxLk2HBc2RYj3.htm](hero-point-deck/hhzrxLk2HBc2RYj3.htm)|Channel Life Force|Canalisation de force vitale|libre|
|[hjD9eWrqowr7zxBU.htm](hero-point-deck/hjD9eWrqowr7zxBU.htm)|Stoke the Magical Flame|Attiser la flamme magique|libre|
|[im7c5Vs4dXSN7383.htm](hero-point-deck/im7c5Vs4dXSN7383.htm)|Desperate Swing|Balancement désespéré|libre|
|[jzsxn0vnoWD41AkQ.htm](hero-point-deck/jzsxn0vnoWD41AkQ.htm)|Cut Through the Fog|Traverser le brouillard|libre|
|[kuIJqFgeJyQcCfqc.htm](hero-point-deck/kuIJqFgeJyQcCfqc.htm)|Surge of Speed|Poussée de vitesse|libre|
|[L8W1qzHJgRor97RC.htm](hero-point-deck/L8W1qzHJgRor97RC.htm)|Pierce Resistance|Perce résistance|libre|
|[lF8Zm7y4U3nT1q3w.htm](hero-point-deck/lF8Zm7y4U3nT1q3w.htm)|Opportune Distraction|Distraction opportune|libre|
|[MMOfg6GP9fhCCbep.htm](hero-point-deck/MMOfg6GP9fhCCbep.htm)|Press On|Poursuivre|libre|
|[mpiUTv0lciVJkdqr.htm](hero-point-deck/mpiUTv0lciVJkdqr.htm)|Flash of Insight|Éclair de lucidité|libre|
|[mRJIxYZNnhNShgfj.htm](hero-point-deck/mRJIxYZNnhNShgfj.htm)|Run and Shoot|Courir et tirer|libre|
|[MSza9gUg7xQfGQnk.htm](hero-point-deck/MSza9gUg7xQfGQnk.htm)|Impossible Shot|Tir impossible|libre|
|[O2oSUauTcoBXcv42.htm](hero-point-deck/O2oSUauTcoBXcv42.htm)|Protect the Innocent|Protéger l'innocent|libre|
|[PdE41wSjGDbIrGzG.htm](hero-point-deck/PdE41wSjGDbIrGzG.htm)|Make Way!|Faites Place !|libre|
|[PPViJTXbCAKWSl0X.htm](hero-point-deck/PPViJTXbCAKWSl0X.htm)|Healing Prayer|Prière de guérison|libre|
|[PqRUEduLDwObHntx.htm](hero-point-deck/PqRUEduLDwObHntx.htm)|Rending Swipe|Coup éventreur|libre|
|[R5MsKeC8KHGBHawz.htm](hero-point-deck/R5MsKeC8KHGBHawz.htm)|Stay in the Fight|Reste dans le combat|libre|
|[rB2othihPTaTf57h.htm](hero-point-deck/rB2othihPTaTf57h.htm)|Rampage|Carnage|libre|
|[RLrz4nP5DqwZye4g.htm](hero-point-deck/RLrz4nP5DqwZye4g.htm)|Strike True|Frappe ultime|libre|
|[RulRBISL4S1xVyVg.htm](hero-point-deck/RulRBISL4S1xVyVg.htm)|Drain Power|Drainer la puissance|libre|
|[rvfUqrWM1zFzPBUk.htm](hero-point-deck/rvfUqrWM1zFzPBUk.htm)|Misdirected Attack|Attaque mal dirigée|libre|
|[SS70TEfozroKA9G1.htm](hero-point-deck/SS70TEfozroKA9G1.htm)|Shake it Off|Secoue toi|libre|
|[u3l38345DtCb1zrg.htm](hero-point-deck/u3l38345DtCb1zrg.htm)|Grazing Blow|Coup égratignant|libre|
|[UUDBSBzK4W5FP8je.htm](hero-point-deck/UUDBSBzK4W5FP8je.htm)|Dive Out of Danger|Se mettre hors de danger|libre|
|[v2kZImaT1rE6Y1aN.htm](hero-point-deck/v2kZImaT1rE6Y1aN.htm)|Last Ounce of Strength|Dernière once de force|libre|
|[vWyxxVDHSBWMtsJN.htm](hero-point-deck/vWyxxVDHSBWMtsJN.htm)|Aura of Protection|Aura de Protection|libre|
|[W07rvt5FRZQ7Uwl7.htm](hero-point-deck/W07rvt5FRZQ7Uwl7.htm)|Push Through the Pain|Surmonter la douleur|libre|
|[WfrKztGAK8Xthk02.htm](hero-point-deck/WfrKztGAK8Xthk02.htm)|Hasty Block|Blocage précipité|libre|
|[XREJxXH5jWesYqoO.htm](hero-point-deck/XREJxXH5jWesYqoO.htm)|Distract Foe|Distraire l'ennemi|libre|
|[xVyXTjsG8iIe6wLj.htm](hero-point-deck/xVyXTjsG8iIe6wLj.htm)|Tuck and Roll|Se plier et rouler|libre|
|[yUi0ENJP5sgnyPyp.htm](hero-point-deck/yUi0ENJP5sgnyPyp.htm)|Surge of Magic|Afflux de magie|libre|
|[zNcR1B6R6clmiFLM.htm](hero-point-deck/zNcR1B6R6clmiFLM.htm)|Daring Attempt|Tentative audacieuse|libre|
