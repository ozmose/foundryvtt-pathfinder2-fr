# État de la traduction (macros)

 * **libre**: 9


Dernière mise à jour: 2023-07-12 20:28 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[084MOWWSEVNwpHVG.htm](macros/084MOWWSEVNwpHVG.htm)|Perception for Selected Tokens|Perception pour les jetons sélectionnés|libre|
|[0GU2sdy3r2MeC56x.htm](macros/0GU2sdy3r2MeC56x.htm)|Rest for the Night|Se reposer pour la nuit|libre|
|[6duZj0Ygiqv712rq.htm](macros/6duZj0Ygiqv712rq.htm)|Treat Wounds|Soigner les blessures|libre|
|[aS6F7PSUlS9JM5jr.htm](macros/aS6F7PSUlS9JM5jr.htm)|Take a Breather|Reprendre son souffle|libre|
|[MAHxEeGf31wqv3jp.htm](macros/MAHxEeGf31wqv3jp.htm)|XP|PX|libre|
|[mxHKWibjPrgfJTDg.htm](macros/mxHKWibjPrgfJTDg.htm)|Earn Income|Gagner de l'argent|libre|
|[NQkc5rKoeFemdVHr.htm](macros/NQkc5rKoeFemdVHr.htm)|Travel Duration|Durée du voyage|libre|
|[s2sa8lo9dcIA6UGe.htm](macros/s2sa8lo9dcIA6UGe.htm)|Toggle Compendium Browser|Activer le navigateur de compendium|libre|
|[yBuEphSaJJ7V9Yw3.htm](macros/yBuEphSaJJ7V9Yw3.htm)|Stealth for Selected Tokens|Discrétion pour les jetons choisis|libre|
