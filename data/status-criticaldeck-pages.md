# État de la traduction (criticaldeck-pages)

 * **libre**: 24
 * **aucune**: 82


Dernière mise à jour: 2023-07-12 20:28 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[14krStxw1Eirik24.htm](criticaldeck-pages/14krStxw1Eirik24.htm)|Critical Hit Deck #21|
|[4YX173FiGFHEW0YY.htm](criticaldeck-pages/4YX173FiGFHEW0YY.htm)|Critical Hit Deck #53|
|[61SADGhFPc7Ra59P.htm](criticaldeck-pages/61SADGhFPc7Ra59P.htm)|Critical Hit Deck #33|
|[7shwLAHvCpn9nNyP.htm](criticaldeck-pages/7shwLAHvCpn9nNyP.htm)|Critical Hit Deck #18|
|[8catEKrHLlFFcnKK.htm](criticaldeck-pages/8catEKrHLlFFcnKK.htm)|Critical Hit Deck #15|
|[8Qpc1cbM8YliNeWt.htm](criticaldeck-pages/8Qpc1cbM8YliNeWt.htm)|Critical Hit Deck #24|
|[aAQ9VezKVsEVvSZP.htm](criticaldeck-pages/aAQ9VezKVsEVvSZP.htm)|Critical Hit Deck #44|
|[aJziqJXJbAHFMnJi.htm](criticaldeck-pages/aJziqJXJbAHFMnJi.htm)|Critical Hit Deck #9|
|[bLrBNA83wIHBP2Y0.htm](criticaldeck-pages/bLrBNA83wIHBP2Y0.htm)|Critical Hit Deck #30|
|[cAf08LJb1eMwAUR9.htm](criticaldeck-pages/cAf08LJb1eMwAUR9.htm)|Critical Hit Deck #27|
|[Clcu2H0uVyozvbI1.htm](criticaldeck-pages/Clcu2H0uVyozvbI1.htm)|Critical Hit Deck #17|
|[d0sh4NZCQ5cKu4ae.htm](criticaldeck-pages/d0sh4NZCQ5cKu4ae.htm)|Critical Hit Deck #7|
|[DlZqvNFlAdXu740m.htm](criticaldeck-pages/DlZqvNFlAdXu740m.htm)|Critical Hit Deck #31|
|[E06pVpFdVXuRKgLT.htm](criticaldeck-pages/E06pVpFdVXuRKgLT.htm)|Critical Hit Deck #52|
|[EAYQXMZmvqXhDVKX.htm](criticaldeck-pages/EAYQXMZmvqXhDVKX.htm)|Critical Hit Deck #48|
|[EkWqEdr5GVmYQOKx.htm](criticaldeck-pages/EkWqEdr5GVmYQOKx.htm)|Critical Hit Deck #45|
|[ftmugY4kI4X2OHtJ.htm](criticaldeck-pages/ftmugY4kI4X2OHtJ.htm)|Critical Hit Deck #14|
|[hfnqwRiA1F8yFxO2.htm](criticaldeck-pages/hfnqwRiA1F8yFxO2.htm)|Critical Hit Deck #38|
|[hRnoJuHZgvnm0y4Y.htm](criticaldeck-pages/hRnoJuHZgvnm0y4Y.htm)|Critical Fumble Deck #14|
|[jE2DwXGkwsJABkA1.htm](criticaldeck-pages/jE2DwXGkwsJABkA1.htm)|Critical Fumble Deck #45|
|[jON5ZXJSqO78xFNT.htm](criticaldeck-pages/jON5ZXJSqO78xFNT.htm)|Critical Hit Deck #10|
|[jU2mfBSZkV0Ek5gE.htm](criticaldeck-pages/jU2mfBSZkV0Ek5gE.htm)|Critical Hit Deck #20|
|[jy3B3GW2BfOzG0AZ.htm](criticaldeck-pages/jy3B3GW2BfOzG0AZ.htm)|Critical Hit Deck #22|
|[k7BzBti00xCRMnZq.htm](criticaldeck-pages/k7BzBti00xCRMnZq.htm)|Critical Fumble Deck #35|
|[KAlZA2xLfbcGEm4E.htm](criticaldeck-pages/KAlZA2xLfbcGEm4E.htm)|Critical Hit Deck #40|
|[KAnlflMVazFiPclF.htm](criticaldeck-pages/KAnlflMVazFiPclF.htm)|Critical Fumble Deck #26|
|[KAsdzGsp84DphbcZ.htm](criticaldeck-pages/KAsdzGsp84DphbcZ.htm)|Critical Hit Deck #23|
|[klRXj66DUAy1d1V5.htm](criticaldeck-pages/klRXj66DUAy1d1V5.htm)|Critical Fumble Deck #39|
|[kQHLnBkfwg7mgZAD.htm](criticaldeck-pages/kQHLnBkfwg7mgZAD.htm)|Critical Hit Deck #13|
|[Lqeke3pTNSHcyAlS.htm](criticaldeck-pages/Lqeke3pTNSHcyAlS.htm)|Critical Hit Deck #16|
|[LXVN3PrglHFeUUQI.htm](criticaldeck-pages/LXVN3PrglHFeUUQI.htm)|Critical Fumble Deck #5|
|[MAjmNAKgJd4UqEfQ.htm](criticaldeck-pages/MAjmNAKgJd4UqEfQ.htm)|Critical Fumble Deck #34|
|[MuU2oRlPMEGf5SKT.htm](criticaldeck-pages/MuU2oRlPMEGf5SKT.htm)|Critical Hit Deck #43|
|[nAQt4JAeqobILZOF.htm](criticaldeck-pages/nAQt4JAeqobILZOF.htm)|Critical Fumble Deck #53|
|[O54GgNHACD44rViW.htm](criticaldeck-pages/O54GgNHACD44rViW.htm)|Critical Fumble Deck #13|
|[OhyEcqPV8lo4hCwQ.htm](criticaldeck-pages/OhyEcqPV8lo4hCwQ.htm)|Critical Hit Deck #25|
|[Okqxk51eZT2JWJnw.htm](criticaldeck-pages/Okqxk51eZT2JWJnw.htm)|Critical Hit Deck #41|
|[OSj1vOhjm71h9Km6.htm](criticaldeck-pages/OSj1vOhjm71h9Km6.htm)|Critical Hit Deck #8|
|[pdKVSF828hMb0ruS.htm](criticaldeck-pages/pdKVSF828hMb0ruS.htm)|Critical Fumble Deck #8|
|[pLB1M1oHTnFnzSCR.htm](criticaldeck-pages/pLB1M1oHTnFnzSCR.htm)|Critical Hit Deck #5|
|[q3wbDpopfALf7tJr.htm](criticaldeck-pages/q3wbDpopfALf7tJr.htm)|Critical Fumble Deck #6|
|[QeRwmIUgBtzxElzk.htm](criticaldeck-pages/QeRwmIUgBtzxElzk.htm)|Critical Fumble Deck #49|
|[qFcMTp2HirNwCf5g.htm](criticaldeck-pages/qFcMTp2HirNwCf5g.htm)|Critical Fumble Deck #38|
|[qJaP33VzLsKefpSc.htm](criticaldeck-pages/qJaP33VzLsKefpSc.htm)|Critical Hit Deck #36|
|[qJgQ0vw4mcqDyYVT.htm](criticaldeck-pages/qJgQ0vw4mcqDyYVT.htm)|Critical Fumble Deck #19|
|[qsF4BXJlET5zW5n6.htm](criticaldeck-pages/qsF4BXJlET5zW5n6.htm)|Critical Fumble Deck #33|
|[r9iBZ8NsIFSvgStR.htm](criticaldeck-pages/r9iBZ8NsIFSvgStR.htm)|Critical Hit Deck #1|
|[R9KUWluXi5sFMfii.htm](criticaldeck-pages/R9KUWluXi5sFMfii.htm)|Critical Fumble Deck #52|
|[RksHFLAckI3WzZjx.htm](criticaldeck-pages/RksHFLAckI3WzZjx.htm)|Critical Hit Deck #4|
|[RMK1WoNdESOAfpUW.htm](criticaldeck-pages/RMK1WoNdESOAfpUW.htm)|Critical Fumble Deck #21|
|[RwmPxeyYxpqhNpKj.htm](criticaldeck-pages/RwmPxeyYxpqhNpKj.htm)|Critical Fumble Deck #10|
|[RYUmc3RaviKhGkaI.htm](criticaldeck-pages/RYUmc3RaviKhGkaI.htm)|Critical Hit Deck #47|
|[rZHd27zct5D3dN6w.htm](criticaldeck-pages/rZHd27zct5D3dN6w.htm)|Critical Fumble Deck #18|
|[SVt7DN4eRBljHvWX.htm](criticaldeck-pages/SVt7DN4eRBljHvWX.htm)|Critical Hit Deck #51|
|[TLsZved7H5lQtKWl.htm](criticaldeck-pages/TLsZved7H5lQtKWl.htm)|Critical Fumble Deck #24|
|[tn6geVSwpDWKhMFC.htm](criticaldeck-pages/tn6geVSwpDWKhMFC.htm)|Critical Hit Deck #26|
|[TQtz26fCkZNSwPf2.htm](criticaldeck-pages/TQtz26fCkZNSwPf2.htm)|Critical Fumble Deck #28|
|[tUgtvtXyPyK1XHIt.htm](criticaldeck-pages/tUgtvtXyPyK1XHIt.htm)|Critical Hit Deck #32|
|[UnOv3yIG2BFWBvOo.htm](criticaldeck-pages/UnOv3yIG2BFWBvOo.htm)|Critical Hit Deck #34|
|[UQ3MU5ltnZlq1Ljs.htm](criticaldeck-pages/UQ3MU5ltnZlq1Ljs.htm)|Critical Hit Deck #37|
|[URrgkZ8uRkzt2Zhf.htm](criticaldeck-pages/URrgkZ8uRkzt2Zhf.htm)|Critical Hit Deck #11|
|[uxrtM7Zg7HSzLogm.htm](criticaldeck-pages/uxrtM7Zg7HSzLogm.htm)|Critical Fumble Deck #46|
|[uYcfVZMr3ykQgRXN.htm](criticaldeck-pages/uYcfVZMr3ykQgRXN.htm)|Critical Hit Deck #35|
|[vbCWhbKx24g0Q40U.htm](criticaldeck-pages/vbCWhbKx24g0Q40U.htm)|Critical Hit Deck #46|
|[vFjWSlbJ9kWr7dhQ.htm](criticaldeck-pages/vFjWSlbJ9kWr7dhQ.htm)|Critical Fumble Deck #17|
|[VWlXcYzq9XEBOA3u.htm](criticaldeck-pages/VWlXcYzq9XEBOA3u.htm)|Critical Hit Deck #28|
|[WBUwp076Zb1XZceP.htm](criticaldeck-pages/WBUwp076Zb1XZceP.htm)|Critical Fumble Deck #42|
|[WheD4j70ekVgpFb7.htm](criticaldeck-pages/WheD4j70ekVgpFb7.htm)|Critical Hit Deck #50|
|[wVneA33rSDvAtNPZ.htm](criticaldeck-pages/wVneA33rSDvAtNPZ.htm)|Critical Fumble Deck #29|
|[XTAKgmqj1ZFepfHx.htm](criticaldeck-pages/XTAKgmqj1ZFepfHx.htm)|Critical Hit Deck #49|
|[XW36TQZYvQ9S7T4v.htm](criticaldeck-pages/XW36TQZYvQ9S7T4v.htm)|Critical Hit Deck #3|
|[XX7shh3pqdG5v6Lm.htm](criticaldeck-pages/XX7shh3pqdG5v6Lm.htm)|Critical Fumble Deck #25|
|[Y3etmaTpdsMT3kKp.htm](criticaldeck-pages/Y3etmaTpdsMT3kKp.htm)|Critical Hit Deck #2|
|[ymqdYEJyGiqAOijp.htm](criticaldeck-pages/ymqdYEJyGiqAOijp.htm)|Critical Hit Deck #19|
|[YrNP8Havb1BQ6akO.htm](criticaldeck-pages/YrNP8Havb1BQ6akO.htm)|Critical Hit Deck #29|
|[Yw5SzkTazJSsn3db.htm](criticaldeck-pages/Yw5SzkTazJSsn3db.htm)|Critical Hit Deck #12|
|[yy6uVnmPsQNaxks9.htm](criticaldeck-pages/yy6uVnmPsQNaxks9.htm)|Critical Hit Deck #6|
|[z1dQl3xXyLg6xakc.htm](criticaldeck-pages/z1dQl3xXyLg6xakc.htm)|Critical Fumble Deck #32|
|[Z6uHNA2pcuTdNc4L.htm](criticaldeck-pages/Z6uHNA2pcuTdNc4L.htm)|Critical Fumble Deck #23|
|[znDpd5Z0k6bq6WCr.htm](criticaldeck-pages/znDpd5Z0k6bq6WCr.htm)|Critical Hit Deck #39|
|[ZNh4PvHcmQEOa65W.htm](criticaldeck-pages/ZNh4PvHcmQEOa65W.htm)|Critical Hit Deck #42|
|[zW0b5OCcFi5TPYp5.htm](criticaldeck-pages/zW0b5OCcFi5TPYp5.htm)|Critical Fumble Deck #7|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0UfzLjrNTkWSDZKb.htm](criticaldeck-pages/0UfzLjrNTkWSDZKb.htm)|Critical Fumble Deck #50|Carte d'échec critique #50|libre|
|[1twrhbUvK9iTGLkw.htm](criticaldeck-pages/1twrhbUvK9iTGLkw.htm)|Critical Fumble Deck #15|Carte d'échec critique #15|libre|
|[1Xh3F6JINcq67Avv.htm](criticaldeck-pages/1Xh3F6JINcq67Avv.htm)|Critical Fumble Deck #16|Carte d'échec critique #16|libre|
|[26fg43UMXWHaV5Ee.htm](criticaldeck-pages/26fg43UMXWHaV5Ee.htm)|Critical Fumble Deck #37|Carte d'échec critique #37|libre|
|[5UIGH23EhwTfoNSM.htm](criticaldeck-pages/5UIGH23EhwTfoNSM.htm)|Critical Fumble Deck #40|Carte d'échec critique #40|libre|
|[97Rw9jJrOJa7NM0s.htm](criticaldeck-pages/97Rw9jJrOJa7NM0s.htm)|Critical Fumble Deck #1|Carte d'échec critique #1|libre|
|[AHnMn8nDzBU8XiDZ.htm](criticaldeck-pages/AHnMn8nDzBU8XiDZ.htm)|Critical Fumble Deck #31|Carté d'échec critique #31|libre|
|[bZJiyaGJWhgfmrqF.htm](criticaldeck-pages/bZJiyaGJWhgfmrqF.htm)|Critical Fumble Deck #27|Carté d'échec critique #27|libre|
|[C0t1CYEnkINf6jhr.htm](criticaldeck-pages/C0t1CYEnkINf6jhr.htm)|Critical Fumble Deck #41|Carte d'échec critique #41|libre|
|[ChyeVeGrXiP83SMI.htm](criticaldeck-pages/ChyeVeGrXiP83SMI.htm)|Critical Fumble Deck #22|Carté d'échec critique #22|libre|
|[CRAeRs5IsNZ6UvY1.htm](criticaldeck-pages/CRAeRs5IsNZ6UvY1.htm)|Critical Fumble Deck #20|Carte d'échec critique #20|libre|
|[CvQ1EVSIxYPukyF5.htm](criticaldeck-pages/CvQ1EVSIxYPukyF5.htm)|Critical Fumble Deck #43|Carte d'échec critique #43|libre|
|[d0OhvD2MJiKwDp6d.htm](criticaldeck-pages/d0OhvD2MJiKwDp6d.htm)|Critical Fumble Deck #30|Carte d'échec critique #30|libre|
|[DbGY1HRle0RP8yQZ.htm](criticaldeck-pages/DbGY1HRle0RP8yQZ.htm)|Critical Fumble Deck #11|Carte d'échec critique #11|libre|
|[dg5t9bt48deLA8IH.htm](criticaldeck-pages/dg5t9bt48deLA8IH.htm)|Critical Fumble Deck #44|Carte d'échec critique #44|libre|
|[dXEWEkV4N8QgAodD.htm](criticaldeck-pages/dXEWEkV4N8QgAodD.htm)|Critical Fumble Deck #4|Carte d'échec critique #4|libre|
|[e9OAyablRyqsYAaP.htm](criticaldeck-pages/e9OAyablRyqsYAaP.htm)|Critical Fumble Deck #36|Carte d'échec critique #36|libre|
|[GHS4SvuvWoDaWa0x.htm](criticaldeck-pages/GHS4SvuvWoDaWa0x.htm)|Critical Fumble Deck #12|Carte d'échec critique #12|libre|
|[GM8KhEJHweIyOXpQ.htm](criticaldeck-pages/GM8KhEJHweIyOXpQ.htm)|Critical Fumble Deck #47|Carte d'échec critique #47|libre|
|[hbbqXuMIjP1O5ivM.htm](criticaldeck-pages/hbbqXuMIjP1O5ivM.htm)|Critical Fumble Deck #48|Carte d'échec critique #48|libre|
|[HJNQmrXcBXqxpU8d.htm](criticaldeck-pages/HJNQmrXcBXqxpU8d.htm)|Critical Fumble Deck #51|Carte d'échec critique #51|libre|
|[IKTn2I00FjQ8CFT5.htm](criticaldeck-pages/IKTn2I00FjQ8CFT5.htm)|Critical Fumble Deck #9|Carte d'échec critique #9|libre|
|[jCcFyy09k0lf4wEl.htm](criticaldeck-pages/jCcFyy09k0lf4wEl.htm)|Critical Fumble Deck #3||libre|
|[jvnfW76s6YjUo71n.htm](criticaldeck-pages/jvnfW76s6YjUo71n.htm)|Critical Fumble Deck #2|Carté d'échec critique #2|libre|
