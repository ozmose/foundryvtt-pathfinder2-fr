# État de la traduction (pathfinder-dark-archive)

 * **changé**: 19
 * **vide**: 6
 * **libre**: 2


Dernière mise à jour: 2023-07-12 20:28 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions à faire

| Fichier   | Nom (EN)    |
|-----------|-------------|
|[3RL07afKtflSq5Ff.htm](pathfinder-dark-archive/3RL07afKtflSq5Ff.htm)|Entrapping Chair|
|[6EELzUwaHp1oMfCO.htm](pathfinder-dark-archive/6EELzUwaHp1oMfCO.htm)|False Step Floor|
|[71Dv5WjrNOzy1Jtr.htm](pathfinder-dark-archive/71Dv5WjrNOzy1Jtr.htm)|Reflected Desires|
|[92rwV7OmBQyY4ZV4.htm](pathfinder-dark-archive/92rwV7OmBQyY4ZV4.htm)|Sigil of Deepest Fears|
|[9rQvZZQbn1IUfr77.htm](pathfinder-dark-archive/9rQvZZQbn1IUfr77.htm)|Mirror Door|
|[cZwTVO2KF85Ak4o6.htm](pathfinder-dark-archive/cZwTVO2KF85Ak4o6.htm)|Confounding Portal|
|[dbbWRZqE509Ac4bH.htm](pathfinder-dark-archive/dbbWRZqE509Ac4bH.htm)|Shrinking Hall|
|[eX1vJnbUagKPdSbU.htm](pathfinder-dark-archive/eX1vJnbUagKPdSbU.htm)|Spirit Window|
|[IyE7jYfltfPSYzNy.htm](pathfinder-dark-archive/IyE7jYfltfPSYzNy.htm)|False Floor|
|[Ldw3d0TYwtMywbM0.htm](pathfinder-dark-archive/Ldw3d0TYwtMywbM0.htm)|Clone Mirrors|
|[mu0khG3glMMREcnD.htm](pathfinder-dark-archive/mu0khG3glMMREcnD.htm)|Distortion Mirror|
|[NGV18Lovi4HsjJOc.htm](pathfinder-dark-archive/NGV18Lovi4HsjJOc.htm)|Shuffling Hall|
|[ObeSl1UJMrCxsVJ6.htm](pathfinder-dark-archive/ObeSl1UJMrCxsVJ6.htm)|Bounding Hounds|
|[P2fNCqJsR0gazMzM.htm](pathfinder-dark-archive/P2fNCqJsR0gazMzM.htm)|Exhaling Portal|
|[RHjdD47Lr9JrXWes.htm](pathfinder-dark-archive/RHjdD47Lr9JrXWes.htm)|Disorienting Illusions|
|[tGhJpdNRItMZTpG7.htm](pathfinder-dark-archive/tGhJpdNRItMZTpG7.htm)|Constricting Hall|
|[wjWedZ12f3t8ixE1.htm](pathfinder-dark-archive/wjWedZ12f3t8ixE1.htm)|Shrouded Assailant|
|[Z9iID4f6DoEuVrLo.htm](pathfinder-dark-archive/Z9iID4f6DoEuVrLo.htm)|Thalassophobic Pool|

## Liste des éléments changés en VO et devant être vérifiés

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0AdtolhMWrPlzVZY.htm](pathfinder-dark-archive/0AdtolhMWrPlzVZY.htm)|Call of the Void|Appel du néant|changé|

## Liste des éléments vides dont le nom doit être traduit

| Fichier   | Nom (EN)    | État |
|-----------|-------------|:----:|
|[34gooaLEuQh0Cu9n.htm](pathfinder-dark-archive/34gooaLEuQh0Cu9n.htm)|Cocoon of Lucid Potential|vide|
|[8Lo4sys48l7YaK5J.htm](pathfinder-dark-archive/8Lo4sys48l7YaK5J.htm)|The Morrowkin|vide|
|[ICdVjs6JEs0YAefl.htm](pathfinder-dark-archive/ICdVjs6JEs0YAefl.htm)|Reflection|vide|
|[spP95evfvogIuclj.htm](pathfinder-dark-archive/spP95evfvogIuclj.htm)|The Weaver in Dreams|vide|
|[WiFzLguamOR69IcD.htm](pathfinder-dark-archive/WiFzLguamOR69IcD.htm)|Mother Mitera|vide|
|[wmKvVsfc6Hg1Bj8I.htm](pathfinder-dark-archive/wmKvVsfc6Hg1Bj8I.htm)|K. H. W.'s Echo|vide|

## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[q9q6ag5sSEEefgUs.htm](pathfinder-dark-archive/q9q6ag5sSEEefgUs.htm)|Verdure's Moonflower|Fleur de lune de verdure|libre|
|[q9R4hKHeErDOaFJu.htm](pathfinder-dark-archive/q9R4hKHeErDOaFJu.htm)|Quartz-Spawned Shadow|Ombre rejeton de quartz|libre|
