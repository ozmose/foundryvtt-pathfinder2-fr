# État de la traduction (familiar-abilities)

 * **officielle**: 16
 * **libre**: 49


Dernière mise à jour: 2023-07-12 20:28 *(heure de Canada/Montréal)*

Ce fichier est généré automatiquement. NE PAS MODIFIER!
## Liste des traductions complétées

| Fichier   | Nom (EN)    | Nom (FR)    | État |
|-----------|-------------|-------------|:----:|
|[0Xrkk46IM43iI1Fv.htm](familiar-abilities/0Xrkk46IM43iI1Fv.htm)|Darkvision|Vision dans le noir|officielle|
|[2fiQzoEKu6YUnrU9.htm](familiar-abilities/2fiQzoEKu6YUnrU9.htm)|Independent|Indépendant|libre|
|[3y6GGXQgyJ4Hq4Yt.htm](familiar-abilities/3y6GGXQgyJ4Hq4Yt.htm)|Radiant|Radieux|libre|
|[43xB5UnexISlfRa5.htm](familiar-abilities/43xB5UnexISlfRa5.htm)|Purify Air|Purificateur d'air|libre|
|[57b8u8s3fV0UCrgJ.htm](familiar-abilities/57b8u8s3fV0UCrgJ.htm)|Plant Form|Forme de plante|libre|
|[5gwqSpkRqWzrbDDU.htm](familiar-abilities/5gwqSpkRqWzrbDDU.htm)|Damage Avoidance: Will|Évitement des dégâts : Volonté|libre|
|[7QosmRHlyLLhU1hX.htm](familiar-abilities/7QosmRHlyLLhU1hX.htm)|Lab Assistant|Assistant de laboratoire|officielle|
|[7ZxPS0UU7pf7wjp0.htm](familiar-abilities/7ZxPS0UU7pf7wjp0.htm)|Ambassador|Ambassadeur|libre|
|[8Z1UkLEWkFWIjOF8.htm](familiar-abilities/8Z1UkLEWkFWIjOF8.htm)|Poison Reservoir|Réservoir de poison|libre|
|[92lgSEPFIDLvKOCF.htm](familiar-abilities/92lgSEPFIDLvKOCF.htm)|Accompanist|Accompagnateur|libre|
|[9PsptrEoCC4QdM23.htm](familiar-abilities/9PsptrEoCC4QdM23.htm)|Valet|Valet|libre|
|[A0C86V3MUECpX5jd.htm](familiar-abilities/A0C86V3MUECpX5jd.htm)|Amphibious|Amphibie|libre|
|[aEKA3YWekLhEhuV8.htm](familiar-abilities/aEKA3YWekLhEhuV8.htm)|Threat Display|Démonstration menaçante|libre|
|[Amzezp93MZckBYRZ.htm](familiar-abilities/Amzezp93MZckBYRZ.htm)|Wavesense|Perception des ondes|libre|
|[asOhEdyF8CWFbR96.htm](familiar-abilities/asOhEdyF8CWFbR96.htm)|Spellcasting|Incantateur|libre|
|[BXssJhTJjKrfojwG.htm](familiar-abilities/BXssJhTJjKrfojwG.htm)|Fast Movement: Land|Déplacement rapide : Au sol|libre|
|[C16JgmeJJG249WXz.htm](familiar-abilities/C16JgmeJJG249WXz.htm)|Mask Freeze|Masque figé|officielle|
|[cT5octWchU4gjrhP.htm](familiar-abilities/cT5octWchU4gjrhP.htm)|Manual Dexterity|Dextérité manuelle|officielle|
|[cy7bdQqVANipyljS.htm](familiar-abilities/cy7bdQqVANipyljS.htm)|Erudite|Érudit|libre|
|[D0ltNUJnN7UjJpA1.htm](familiar-abilities/D0ltNUJnN7UjJpA1.htm)|Innate Surge|Déferlante innée|officielle|
|[deC1yIM2S5szGdzT.htm](familiar-abilities/deC1yIM2S5szGdzT.htm)|Lifelink|Lien vital|officielle|
|[dpjf1CyMEILpJWyp.htm](familiar-abilities/dpjf1CyMEILpJWyp.htm)|Darkeater|Mangeur noir|libre|
|[dWTfO5WbLkD5mw2H.htm](familiar-abilities/dWTfO5WbLkD5mw2H.htm)|Climber|Grimpeur|officielle|
|[FcQQLMAJMgOLjnSv.htm](familiar-abilities/FcQQLMAJMgOLjnSv.htm)|Resistance|Résistance|libre|
|[FlRUb8U13Crj3NaA.htm](familiar-abilities/FlRUb8U13Crj3NaA.htm)|Scent|Odorat|officielle|
|[fmsVItn94FeY5Q3X.htm](familiar-abilities/fmsVItn94FeY5Q3X.htm)|Snoop|Fouineur|libre|
|[gPceRQqO847lvSnb.htm](familiar-abilities/gPceRQqO847lvSnb.htm)|Share Senses|Partage des sens|officielle|
|[hMrxiUPHXKpKu1Ha.htm](familiar-abilities/hMrxiUPHXKpKu1Ha.htm)|Major Resistance|Résistance majeure|libre|
|[j1qZiH50Bl2SJ8vT.htm](familiar-abilities/j1qZiH50Bl2SJ8vT.htm)|Shadow Step|Pas d'ombre|libre|
|[j9vOSbF9kLibhSIf.htm](familiar-abilities/j9vOSbF9kLibhSIf.htm)|Second Opinion|Seconde opinion|libre|
|[jdlefpPcSCIe27vO.htm](familiar-abilities/jdlefpPcSCIe27vO.htm)|Familiar Focus|Focalisation du familier|officielle|
|[jevzf9JbJJibpqaI.htm](familiar-abilities/jevzf9JbJJibpqaI.htm)|Skilled|Compétent|libre|
|[JRP2bdkdCdj2JDrq.htm](familiar-abilities/JRP2bdkdCdj2JDrq.htm)|Master's Form|Forme du maître|libre|
|[K5OLRDsGCfPZ6mO6.htm](familiar-abilities/K5OLRDsGCfPZ6mO6.htm)|Damage Avoidance: Reflex|Évitement des dégâts : Réflexes|libre|
|[Le8UWr5BU8rV3iBf.htm](familiar-abilities/Le8UWr5BU8rV3iBf.htm)|Tough|Robuste|libre|
|[lpyJAl5B4j2DC7mc.htm](familiar-abilities/lpyJAl5B4j2DC7mc.htm)|Gills|Branchies|libre|
|[LrDnat1DsGJoAiKv.htm](familiar-abilities/LrDnat1DsGJoAiKv.htm)|Tremorsense|Perception des vibrations|libre|
|[LUBS9csNNgRTui4p.htm](familiar-abilities/LUBS9csNNgRTui4p.htm)|Grasping Tendrils|Vrilles agrippantes|libre|
|[mK3mAUWiRLZZYNdz.htm](familiar-abilities/mK3mAUWiRLZZYNdz.htm)|Damage Avoidance: Fortitude|Évitement des dégâts : Vigueur|libre|
|[mKmBgQmPmiLOlEvw.htm](familiar-abilities/mKmBgQmPmiLOlEvw.htm)|Augury|Augure|libre|
|[nrPl3Dz7fbnmas7T.htm](familiar-abilities/nrPl3Dz7fbnmas7T.htm)|Spirit Touch|Contact spirituel|libre|
|[o0fxDDUu2ZSWYDTr.htm](familiar-abilities/o0fxDDUu2ZSWYDTr.htm)|Soul Sight|Vision de l'âme|libre|
|[O5TIjqXAuta8iVSz.htm](familiar-abilities/O5TIjqXAuta8iVSz.htm)|Focused Rejuvenation|Récupération concentrée|libre|
|[ou91pzf9TlOnIjYn.htm](familiar-abilities/ou91pzf9TlOnIjYn.htm)|Luminous|Lumineux|libre|
|[Q42nrq9LwDdbeZM5.htm](familiar-abilities/Q42nrq9LwDdbeZM5.htm)|Restorative Familiar|Familier curatif|libre|
|[qTxH8mSOvc4PMzrP.htm](familiar-abilities/qTxH8mSOvc4PMzrP.htm)|Kinspeech|Parole des semblables|officielle|
|[ReIgBsaM95BTvHpN.htm](familiar-abilities/ReIgBsaM95BTvHpN.htm)|Medic|Médecin|libre|
|[REJfFezELjc5Gzsy.htm](familiar-abilities/REJfFezELjc5Gzsy.htm)|Recall Familiar|Rappel de familier|libre|
|[rs4Awf4k1e0Mj797.htm](familiar-abilities/rs4Awf4k1e0Mj797.htm)|Cantrip Connection|Lien avec les tours de magie|officielle|
|[Rv1KTRioaZOWyQI6.htm](familiar-abilities/Rv1KTRioaZOWyQI6.htm)|Alchemical Gut|Entrailles alchimiques|libre|
|[SKIS1xexaOvrecdV.htm](familiar-abilities/SKIS1xexaOvrecdV.htm)|Verdant Burst|Explosion verdoyante|libre|
|[SxWYVgqNMsq0OijU.htm](familiar-abilities/SxWYVgqNMsq0OijU.htm)|Fast Movement: Climb|Déplacement rapide : Escalade|libre|
|[tEWAHPPZULvPgHnT.htm](familiar-abilities/tEWAHPPZULvPgHnT.htm)|Tattoo Transformation|Transformation en tatouage|libre|
|[uUrsZ4WvhjKjFjnt.htm](familiar-abilities/uUrsZ4WvhjKjFjnt.htm)|Toolbearer|Porte outils|libre|
|[uy15sDBuYNK48N3v.htm](familiar-abilities/uy15sDBuYNK48N3v.htm)|Burrower|Fouisseur|officielle|
|[v7zE3tKQb9G6PaYU.htm](familiar-abilities/v7zE3tKQb9G6PaYU.htm)|Partner in Crime|Associés dans le crime|libre|
|[VHQUZcjUxfC3GcJ9.htm](familiar-abilities/VHQUZcjUxfC3GcJ9.htm)|Fast Movement: Fly|Déplacement rapide : Vol|libre|
|[vpw2ReYdcyQBpdqn.htm](familiar-abilities/vpw2ReYdcyQBpdqn.htm)|Fast Movement: Swim|Déplacement rapide : Nage|libre|
|[wOgvBymJOVQDSm1Q.htm](familiar-abilities/wOgvBymJOVQDSm1Q.htm)|Spell Delivery|Transfert de sort|libre|
|[Xanjwv4YU0CBnsMw.htm](familiar-abilities/Xanjwv4YU0CBnsMw.htm)|Spell Battery|Accumulateur de sorts|officielle|
|[XCqYnlVbLGqEGPeX.htm](familiar-abilities/XCqYnlVbLGqEGPeX.htm)|Touch Telepathy|Télépathie de contact|libre|
|[ZHSzNt3NxkXbj1mI.htm](familiar-abilities/ZHSzNt3NxkXbj1mI.htm)|Flier|Volant|libre|
|[zKTL9y9et0oTHEYS.htm](familiar-abilities/zKTL9y9et0oTHEYS.htm)|Greater Resistance|Résistance supérieure|libre|
|[ZtKb89o1PPhwJ3Lx.htm](familiar-abilities/ZtKb89o1PPhwJ3Lx.htm)|Extra Reagents|Réactifs supplémentaires|officielle|
|[zyMRLQnFCQVpltiR.htm](familiar-abilities/zyMRLQnFCQVpltiR.htm)|Speech|Parole|officielle|
