class Translator {
    static get() {
        if (!Translator.instance) {
            Translator.instance = new Translator();
        }
        return Translator.instance;
    }

    // Initialize translator
    async initialize() {
        // Signalize translator is ready
        Hooks.callAll("pf2FR.ready");
    }

    constructor() {
        this.initialize();
    }

    translateActorItems(data) {
        data.forEach((entry, index, arr) => {
            let nameEN = entry.name;
            let specificTranslation;
            let pack = game.babele.packs.find(pack => pack.translated && pack.hasTranslation(entry));
            if (pack) {
                 specificTranslation = pack.translate(entry, true);
            }

            if (entry.flags?.core?.sourceId && entry.flags.core.sourceId.startsWith("Compendium")) {
                const itemCompendium = entry.flags.core.sourceId.slice(
                    entry.flags.core.sourceId.indexOf(".") + 1,
                    entry.flags.core.sourceId.lastIndexOf(".Item.")
                );
                const originalName = fromUuidSync(entry.flags.core.sourceId)?.flags?.babele?.originalName;
                if (originalName) {
                    entry.name = originalName;
                    arr[index] = game.babele.packs.get(itemCompendium).translate(entry);
                }
            }

            if (specificTranslation) {
                // Merge specific translation into Compendium translation
                mergeObject(arr[index], specificTranslation, { override: true });
            } else {
                // Put back name in English if no specific translation was provided to keep specificities
                arr[index].name = nameEN;
            }
        });

        return data;
    }

    translateEquipmentName(data, translation, dataObject) {
        if (dataObject?.type === "weapon" && game.settings.get('pf2-fr', 'weapon-name-generation') && translation) {
            if (game.settings.get('pf2-fr', 'name-display') === "vf-vo") {
                return translation.replace(" (" + data + ")", "");
            }
            else if (game.settings.get('pf2-fr', 'name-display') === "vo-vf") {
                return translation.replace(data + " (", "").slice(0, -1);
            }
            else {
                return translation;
            }
        }
        else {
            return translation;
        }
    }
}

Hooks.once("init", () => {
    game.langFRPf2e = Translator.get();

    game.settings.register("pf2-fr", "name-display", {
        name: "Affichage des noms",
        hint: "Vous pouvez choisir ici la manière dont les noms des acteurs, objets et journaux issus des compendiums seront traduits et affichés",
        scope: "world",
        type: String,
        choices: {
            "vf-vo": "VF (VO)",
            "vo-vf": "VO (VF)",
            "vf": "VF",
            "vo": "VO"
        },
        default: "vf-vo",
        config: true,
        onChange: foundry.utils.debouncedReload
    });

    game.settings.register("pf2-fr", "weapon-name-generation", {
        name: "Garder les noms des armes en VF",
        hint: "Le système implémente une génération automatique des noms des armes en fonction de leurs propriétés (matériaux, runes), mais pour cela les noms doivent être en VF uniquement. Incompatible avec les noms en VO pure.",
        scope: "world",
        type: Boolean,
        default: false,
        config: true,
        onChange: foundry.utils.debouncedReload
    });

    if (typeof Babele !== "undefined") {
        Babele.get().register({
            module: "pf2-fr",
            lang: 'fr',
            dir: "babele/"+game.settings.get('pf2-fr', 'name-display')+"/fr/"
        });

        Babele.get().registerConverters({
            "translateActorItems": (data) => {
                return game.langFRPf2e.translateActorItems(data);
            },
            "translateEquipmentName": (data, translation, dataObject) => {
                return game.langFRPf2e.translateEquipmentName(data, translation, dataObject);
            }
        });
    }
});

Hooks.once("babele.ready", () => {
    game.pf2e.ConditionManager.initialize();

    if (game.modules.get("lang-fr-pf2e")?.active){
        ui.notifications.error("Le package \"Système PF2 Français\" est encore installé sur cette partie ; il n'est plus utile et peut donc être désinstallé.")
    }
});
